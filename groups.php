<?php
include "session.php";

include_once __DIR__ . '/models/tour_membership.php';
include_once __DIR__ . '/models/contacts.php';

// { Acl Condition
$loggedUserId = AclPermission::userId() ;
$PageTitle =  "Group List" ;
if( AclPermission::activeRoleId() == AclRole::$AGENT ||
    AclPermission::activeRoleId() == AclRole::$LEADER ) {
    $contact = (new Contacts())->get(['id' => $loggedUserId]) ;
    $name = GUtils::contactName($contact) ;
    if( strlen(trim($name)) > 0 ) {
        $PageTitle =  "$name's Group List" ;
    }
} 
if(AclPermission::activeRoleId() == AclRole::$LEADER) {
    if( ! isset($_REQUEST['status']) ) {
        header( 'location:' . 'groups.php?status=both') ;
        die;
    }
}
include "header.php"; 

$ACL_CONDITION = "" ;
if( AclPermission::activeRoleId() == AclRole::$AGENT ) {
    $ACL_CONDITION = " AND CONCAT(',', agent, ',') LIKE '%,$loggedUserId,%' " ;
}
else if( AclPermission::activeRoleId() == AclRole::$LEADER ) {
    $ACL_CONDITION = " AND CONCAT(',', groupleader, ',') LIKE '%,$loggedUserId,%' " ;
}
else if( AclPermission::activeRoleId() == AclRole::$SUPPLIER ) {
    $ACL_CONDITION = " AND tourid IN(
        SELECT tourid FROM groups g
            JOIN products p ON (g.airline_productid=p.id OR g.land_productid=p.id)
        WHERE p.supplierid='$loggedUserId' ) " ;
}
else if( AclPermission::activeRoleId() == AclRole::$CUSTOMER) {
    $ACL_CONDITION = " AND tourid IN( SELECT tourid FROM customer_account ca WHERE contactid='$loggedUserId' ) " ;
}
// }

//To show the Update success notification with the url
if (!empty($_GET['action']))
{
	if ($_GET['action'] == "created") {
		$fullname = $_GET["fullname"];
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The contact ".$fullname." has been created successfully!</strong></div>";
	}
	if ($_GET['action'] == "update") {
		$fullname = $_GET["fullname"];
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The contact ".$fullname." has been updated successfully!</strong></div>";
	}
	if ($_GET['action'] == "delete") {
		$fullname = $_GET["fullname"];
		$contactid = $_GET["cid"];
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The contact ".$fullname." (id# ".$contactid.") has been deleted!</strong></div>";
	}
}
?>
<!--<script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>	 -->
<style type="text/css">
#basic-btn2_filter { display:none;}
</style>

<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
		<div class="card">
            <div class="row">
				<div class="col-md-2" style="margin-top:15px;padding-left:45px;">
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
					</div>
				</div>
				<div class="col-md-8" style="margin-top:15px;">
				<form id="AdvancedFilter" class="col-12 row" action="groups.php" method="GET">
					<div class="col-md-2 text-right pt-1">
						<b>Filter:</b>
					</div>
					<div class="col-md-2">
						<select id="month" name="month" class="form-control form-control-default fill">
							<option value="">Month</option>
							<option value="1" <?php if(!empty($_GET['month']) AND $_GET['month'] == 1) {echo "selected";} ?>>January</option>
							<option value="2" <?php if(!empty($_GET['month']) AND $_GET['month'] == 2) {echo "selected";} ?>>February</option>
							<option value="3" <?php if(!empty($_GET['month']) AND $_GET['month'] == 3) {echo "selected";} ?>>March</option>
							<option value="4" <?php if(!empty($_GET['month']) AND $_GET['month'] == 4) {echo "selected";} ?>>April</option>
							<option value="5" <?php if(!empty($_GET['month']) AND $_GET['month'] == 5) {echo "selected";} ?>>May</option>
							<option value="6" <?php if(!empty($_GET['month']) AND $_GET['month'] == 6) {echo "selected";} ?>>June</option>
							<option value="7" <?php if(!empty($_GET['month']) AND $_GET['month'] == 7) {echo "selected";} ?>>July</option>
							<option value="8" <?php if(!empty($_GET['month']) AND $_GET['month'] == 8) {echo "selected";} ?>>August</option>
							<option value="9" <?php if(!empty($_GET['month']) AND $_GET['month'] == 9) {echo "selected";} ?>>September</option>
							<option value="10" <?php if(!empty($_GET['month']) AND $_GET['month'] == 10) {echo "selected";} ?>>October</option>
							<option value="11" <?php if(!empty($_GET['month']) AND $_GET['month'] == 11) {echo "selected";} ?>>November</option>
							<option value="12" <?php if(!empty($_GET['month']) AND $_GET['month'] == 12) {echo "selected";} ?>>December</option>
						</select>
					</div>
					<div class="col-md-2">
						<select id="year" name="year" class="form-control form-control-default fill">
							<option value="">Year</option>
							<option value="2018" <?php if(!empty($_GET['year']) AND $_GET['year'] == 2018) {echo "selected";} ?>>2018</option>
							<option value="2019" <?php if(!empty($_GET['year']) AND $_GET['year'] == 2019) {echo "selected";} ?>>2019</option>
							<option value="2020" <?php if(!empty($_GET['year']) AND $_GET['year'] == 2020) {echo "selected";} ?>>2020</option>
							<option value="2021" <?php if(!empty($_GET['year']) AND $_GET['year'] == 2021) {echo "selected";} ?>>2021</option>
							<option value="2022" <?php if(!empty($_GET['year']) AND $_GET['year'] == 2022) {echo "selected";} ?>>2022</option>
							<option value="2023" <?php if(!empty($_GET['year']) AND $_GET['year'] == 2023) {echo "selected";} ?>>2023</option>
							<option value="2024" <?php if(!empty($_GET['year']) AND $_GET['year'] == 2024) {echo "selected";} ?>>2024</option>
							<option value="2025" <?php if(!empty($_GET['year']) AND $_GET['year'] == 2025) {echo "selected";} ?>>2025</option>
						</select>
					</div>
					<div class="col-md-2">
						<select id="staff" name="staff" class="form-control form-control-default fill">
							<option value="">Staff</option>
							<?php 
							$StaffSelectResult = mysqli_query($db,"SELECT * FROM users WHERE active=1 AND Role_ID=1");
							while($StaffListData = mysqli_fetch_array($StaffSelectResult))
							{
								$selected = "";
								if($StaffListData['fname'] != "" OR $StaffListData['lname'] != "") {
									$FullName = $StaffListData['fname']." ".$StaffListData['lname'];
								} else {
									$FullName = $StaffListData['Username'];
								}
								if($_GET['staff'] == $StaffListData['UserID']) { $selected = "selected";} else { $selected = ""; }
								echo "<option value='".$StaffListData['UserID']."' ".$selected.">".$FullName."</option>";
							}
							?>
						</select>
					</div>
					<div class="col-md-2">
						<select id="status" name="status" class="form-control form-control-default fill">
							<option value="">Status</option>
							<option value="both" <?php if(!empty($_GET['status']) AND $_GET['status'] == "both") {echo "selected";} ?>>All</option>
							<option value="active" <?php if(!empty($_GET['status']) AND $_GET['status'] == "active") {echo "selected";} ?>>Active</option>
							<option value="expired" <?php if(!empty($_GET['status']) AND $_GET['status'] == "expired") {echo "selected";} ?>>Expired</option>
							<option value="inactive" <?php if(!empty($_GET['status']) AND $_GET['status'] == "inactive") {echo "selected";} ?>>Disabled</option>
						</select>
					</div>
					<div class="col-md-1">
						<button type="submit" class="btn btn-info" style="padding: 5px 13px;">Filter</button>
					</div>
				</form>
				</div>
				<div class="col-md-2" style="margin-top:15px;">
					<?php if( AclPermission::actionAllowed('Add') ) { ?>
					<a href="groups-add.php" class="btn  btn-mat waves-effect waves-light btn-success btn-block" style="margin-right: 30px;float:right; padding: 6px 13px;"><i class="far fa-check-circle"></i>Add New Group</a>
					<?php }?>
				</div>
			</div>
            <div class="card-block">
				<div class="dt-responsive table-responsive">
                    <table id="basic-btn2" class="table table-hover table-striped table-bordered nowrap" data-page-length="20">
                        <thead>
                            <tr>
                                <th>Group Ref. #</th>
                                <th>Group's Name</th>
                                <th>Starting Date</th>
                                <th>Ending Date</th>
                                <th>Group Price</th>
                                <th># of Pax</th>
                            </tr>
                        </thead>
                        <tbody>
						<?php
						$displaynone = "";
						$leadergroups = "";
						$statuss = "WHERE status=1 AND enddate >= CURDATE() ";
						$FilterInput = "";
						
						if(!empty($_GET['leader'])){
							$leaderzzid = $_GET['leader'];
							$leadergroups = "AND groupleader LIKE '%$leaderzzid%'";
						}						
						if(!empty($_GET['staff'])){
							$GetStaff = $_GET['staff'];
							$FilterInput.= " AND Staff_ID='$GetStaff'";
						}
						if(!empty($_GET['month'])) {
							$GetMonth = $_GET['month'];
							$FilterInput.= " AND EXTRACT(MONTH FROM startdate)= '$GetMonth'";
						}
						if(!empty($_GET['year'])) {
							$GetYear = $_GET['year'];
							$FilterInput.= " AND EXTRACT(YEAR FROM startdate)= '$GetYear'";
						}
						if(!empty($_GET['status'])) {
							$statuss = "WHERE status=1 AND enddate >= CURDATE() $leadergroups";
						}
						if(!empty($_GET['status']) AND $_GET['status'] == "active") {
							$statuss = "WHERE status=1 AND enddate >= CURDATE() $leadergroups";
						}
						if(!empty($_GET['status']) AND $_GET['status'] == "both") {
							$statuss = "WHERE 1 ";
						}
						if(!empty($_GET['status']) AND $_GET['status'] == "expired") {
						    $displaynone = "style='display:none;'";
						    $statuss = "WHERE status=1 AND enddate < CURDATE() $leadergroups";
						}
						if(!empty($_GET['status']) AND $_GET['status'] == "inactive") {
						    $statuss = "WHERE status=0 $leadergroups";
						}
						
						$sql = "select * from groups $statuss $FilterInput
                                                            $ACL_CONDITION 
                                                            ORDER BY startdate asc";
						$result = $db->query($sql);

						if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { 
						
							//To show the Green Check or Red X before the title of the group
							if($row["status"]==1) { $groupstatus = "<i $displaynone class='far fa-check-circle' style='color: #198a10;'></i>"; }
							else { $groupstatus = "<i class='far fa-times-circle' style='color: red;'></i>"; }
							
							$tourzid = $row["tourid"];
							$LandProductID = $row["land_productid"];
							$AirlineProductID = $row["airline_productid"];

							$LandCostPriceSQL = "SELECT price FROM products WHERE id=$LandProductID";
							$LandCostPriceResult = $db->query($LandCostPriceSQL);
							$LandCostPrice = $LandCostPriceResult->fetch_assoc();

							$AirCostPriceSQL = "SELECT price FROM products WHERE id=$AirlineProductID";
							$AirCostPriceResult = $db->query($AirCostPriceSQL);
							$AirCostPrice = $AirCostPriceResult->fetch_assoc();
							
							$AirAndLandPrice = $LandCostPrice['price'] + $AirCostPrice['price'];
							if (empty($AirlineProductID) && empty($AirAndLandPrice))
							{
								$AirAndLandPrice = $row["listprice"];
							}
							$TotalGroupPriceUpdateSQL = "UPDATE groups SET listprice=$AirAndLandPrice WHERE tourid=$tourzid;";
							$db->query($TotalGroupPriceUpdateSQL);

							//To get the number of PAX in the group

                            $paxtotal = (new TourMembership())->paxTotal($tourzid) ;

							//To get the number of seats in the group
							$seatsql = "SELECT seats FROM products WHERE id=$AirlineProductID";
							$paxresult = $db->query($seatsql);
							$seatdata = $paxresult->fetch_assoc();
							
							if($seatdata["seats"] <= $paxtotal) {
								$paxwarning = "style='background: #ffc3c3;'";
								$warningsql = "UPDATE groups SET warnings=1 WHERE tourid=$tourzid AND status=1;";
								$db->query($warningsql);
							} else {
								$paxwarning ="";
								$warningsqldone = "UPDATE groups SET warnings=0 WHERE tourid=$tourzid AND status=1;";
								$db->query($warningsqldone);
							}
							?> 
                            <tr <?php echo $paxwarning; ?>>
                                <td><?php echo $row["tourdesc"]; ?></td>
                                <td><?php echo $groupstatus ?> <a href="groups-edit.php?id=<?php echo $row["tourid"]; ?>&action=edit" style="color: #0000EE;">
									<?php echo $row["tourname"]; ?></a>
								</td>
                                <td><?php echo date('m/d/Y', strtotime($row["startdate"])); ?></td>
                                <td><?php echo date('m/d/Y', strtotime($row["enddate"])); ?></td>
                                <td><?php echo GUtils::formatMoney($row["listprice"]); ?></td>
                                <td>
									<a href="groups-edit.php?id=<?php echo $row["tourid"]; ?>&action=edit&tab=participants" style="color: #0000EE;font-size:14px;">
									<?php
									echo $paxtotal; ?>
									</a> out of <?php if($seatdata["seats"] == "") {echo "0";} else {echo $seatdata["seats"];} ?>
								</td>
                                <?php /*<td>
									<?php //First, we should get the contact id from the agents table
									$agentcon = new mysqli($servername, $username, $password, $dbname);
									$agentid = $row["agent"];
									$getcontactid = mysqli_query($agentcon,"SELECT contactid FROM agents WHERE id='$agentid' ");
									$theagentid = mysqli_fetch_array($getcontactid); $thecontactid = $theagentid['contactid'];
									//After getting the contact id from the agent table, now we will get the name
									$AgentResult = mysqli_query($agentcon,"SELECT fname, mname, lname FROM contacts WHERE id='$thecontactid' ");
									$theagent = mysqli_fetch_array($AgentResult); echo $theagent['fname']." ".$theagent['mname']." ".$theagent['lname']; ?>
								</td>
                                
								<td>
									<?php //To get the name of the Group leader, which is already on Contacts Table
									$leadercon = new mysqli($servername, $username, $password, $dbname);
									$leaderid = $row["groupleader"];
									$LeaderResult = mysqli_query($leadercon,"SELECT fname, mname, lname FROM contacts WHERE id='$leaderid' ");
									$theleader = mysqli_fetch_array($LeaderResult); echo $theleader['fname']." ".$theleader['mname']." ".$theleader['lname']; ?>
								</td>*/ ?>
                            </tr>
						<?php }} ?>
						</tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<!-- [ page content ] end -->
<script>
//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#basic-btn2').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
});

$(document).ready(function() {
    $('#basic-btn2').DataTable( {
        "ordering": false,
		dom: 'Bfrtip',
        buttons: [ ]
    } );
} );
</script>
<?php include "footer.php"; ?>