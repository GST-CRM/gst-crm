<?php

require_once('session.php');
require_once 'inc/helpers.php';
ini_set('display_errors', false) ;

$invoiceId = isset($__REQUEST['invid']) ? $__REQUEST['invid'] : 0;

// Include the main TCPDF library (search for installation path).
require_once('files/pdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Element Media');
$pdf->SetTitle('GST Customer Invoice');
$pdf->SetSubject('GST Customer Invoice');
$pdf->SetKeywords('GST, CRM, Element, Media, Element.ps');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set margins
$pdf->SetMargins(15, 25, 15);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/../lang/eng.php')) {
	require_once(dirname(__FILE__).'/../lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('Helvetica', '', 11);

// add a page
$pdf->AddPage('P','LETTER');

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

$sqlg = "SELECT tourid, ca.contactid AS id FROM customer_account ca"
    . " INNER JOIN customer_invoice ci ON ci.id='$invoiceId'" ;
$invoiceData = GDb::fetchRow($sqlg) ;
$groupId = $invoiceData['tourid'] ;
$customerId = $invoiceData['id'] ;

//Invoice Data
if( $invoiceId ) {

    $SubmitAction = 'update';

    $editId = $__REQUEST['invid'] ;
    //invoice
    $sqle = "SELECT ci.*, ca.contactid, c.email FROM customer_invoice ci "
        . " INNER JOIN customer_account ca ON ca.id=ci.Customer_Account_Customer_ID "
        . " INNER JOIN contacts c ON c.id=ca.contactid "
        . " WHERE Customer_Invoice_Num='$editId'" ;
    $invoices = GDb::fetchRow($sqle) ;

    $editContactId = $invoices['contactid'] ;
    //invoice lines
    $sqll = "SELECT * FROM customer_invoice_line WHERE `Customer_Invoice_Num`='$editId'" ;
    $invoiceLines = GDb::fetchRowSet($sqll) ;

    $payments = "SELECT * FROM payments" ;
    $payments = GDb::fetchRowSet($payments) ;
}

if ( !empty($invoices) ) { 

    ob_start() ;
    include 'inc/sales-invoice-pdf-content.php';

    $html = ob_get_clean() ;

} else {
    $html = 'This customer account does not have any invoice records yet!.';
    return null ; 
}

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');


// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

$PDF_OUTPUT = isset($PDF_OUTPUT ) ? $PDF_OUTPUT : 'I' ;
//Close and output PDF document
return $pdf->Output('Customer-Invoice.pdf', $PDF_OUTPUT );

//============================================================+
// END OF FILE
//============================================================+
