<?php
/**
 * THIS FILE SHOULD NOT BE EDITED. IF MAKE ANY CHANGE, CONSIDER
 * sales-invoice-tab.php ALSO.
 */

include "session.php";
$PageTitle = "Customer Invoice";
if( ! defined('INVOICE_ADD_FRAGMENT_VIEW') ) {
    include "header.php";
}
include_once __DIR__ . '/models/customer_credit_note.php';
include_once __DIR__ . '/models/customer_invoice.php';
include_once __DIR__ . '/models/customer_invoice_line.php';
include_once __DIR__ . '/models/customer_payment.php';
include_once __DIR__ . '/models/contacts.php';
include_once __DIR__ . '/models/groups.php';

global $GLOBAL_SCRIPT;
$SubmitAction = 'create';

$invoiceId =$__GET['invid'] ;
    
$ADD_URL = 'sales-invoice-add.php?invid='.$invoiceId ;

if( basename($_SERVER['SCRIPT_NAME']) == 'contact-edit.php' ) {
    $contactIdForRedirectData = (new CustomerInvoice())->get(['Customer_Invoice_Num' => $invId]) ;
    $contactIdForRedirect = $contactIdForRedirectData['Customer_Account_Customer_ID'] ;
    $ADD_URL = "contacts-edit.php?invid=$invoiceId&id=$contactIdForRedirect&action=edit&usertype=sales#salestab" ;
}

//Actions are handled in seprate file.
if ( isset($__REQUEST['submit']) && isset($__POST['hidInvoiceSave']) && $__POST['hidInvoiceSave'] == '1') {
    include __DIR__ . '/inc/sales-invoice-save.php';
    if ($__POST['submit'] == "create_new" || $__POST['submit'] == "update_new")
    {
        GUtils::redirect('sales-invoice-add.php');
    } 
    else if (($__POST['submit'] == "create_only") || ($__POST['submit'] == "update_only"))
    {
        GUtils::redirect($ADD_URL);
    }
    else
    {
        GUtils::redirect('sales-invoices.php');
    }
}
/**  * Delete note action ?  * { */
if( isset($__GET['noteid']) && isset($__GET['invid']) && $__GET['action'] == 'deletenote' ) {
$noteId = $__GET['noteid'] ;
 $obj = new CustomerCreditNote() ;
 $obj->delete(['Note_ID' => $noteId]) ;
 
 GUtils::redirect('sales-invoice-add.php?invid=' . $__GET['invid']);
 return; 
}
/**  }  */    


//Edit Mode {
//Invoice Number
$invoices     = [];
$invoiceLines = [];
$editId        = 0;
$editContactId = 0;
$paymentList    = [] ;
if (isset($__GET['invid'])) {

    $SubmitAction = 'update';
    $editId   = $__GET['invid'];
    //get invoice data.
    $invoices = (new CustomerPayment())->invoiceDetails($editId) ;
    if (empty($invoices['invoiced_customer']))
    {
         $invoices['Refund_Amount']= 0;
         $invoices['Cancellation_Charge'] = 0;
         $invoices['paid'] = 0;
         $invoices['due'] = 0;
    }
    //format billing address
    $invoices['address'] = GUtils::buildGSTAddress( false, $invoices, ",", "\n" ) ;

    $editContactId = $invoices['contactid'];
    
    //get invoice lines
    $invoiceLines = null ;
    if ( ! empty($invoices['invoiced_customer']) )
    {
        $invoiceLines  =  (new CustomerInvoiceLine())->getInvoiceLines($editId) ;
    }
    //get payments made
    $paymentList = (new CustomerPayment())->invoicePaymentsList($editId) ;
} else {

    //initialize wiht null string for add page. otherwise might cause undefined 
    $invoices['email']    = '';
    $invoices['Due_Date'] = '';
    $invoices['Comments'] = '';
    $invoices['paid'] = '';
    $invoices['due'] = '';
    $invoices['address'] = '';
    $invoices['tourid'] = '' ;
    $invoices['Invoice_Status'] = 0 ;
    $invoices['RefundStatus'] = 0 ;
    //TODO; for add, enable it. confirm with jyo {
    $invoices['invoiced_customer'] = 1 ;
    //}
}

//Credit Notes
$creditNotes = [] ;
if( $editId ) {
$creditNotes = (new CustomerCreditNote())->getList(['Customer_Invoice_Num' => $editId, 'Status' => 1]);
}
$creditNoteTotal = 0 ;
if ($invoices['invoiced_customer'] == 1)
{
    foreach( $creditNotes as $one ) {
        $creditNoteTotal += $one['Credit_Amount'] ;
    }  
}

//if invalid due date in invoice table, recalculat the same
if( ! $invoices['Due_Date'] || strtotime($invoices['Due_Date']) == 0 ) {
    $tourId = $invoices['tourid'] ;
    $invoices['Due_Date'] = (new Groups())->getDueDate($tourId) ;
}

//} Edit Mode

//Fetch all customers for select box
$custRows = (new Contacts())->forInvoicedCustomersList($editContactId) ;

//Product Row
$qtyCondition = "" ;
$qtySel = "" ;
//get invoice line while editing
if( $editId ) {
    $qtySel = ", ci.Qty" ;
    //$qtyCondition = "LEFT JOIN customer_invoice_line ci ON ci.Product_Product_ID=p.id AND ci.Customer_Invoice_Num='$editId' "; 
	//I removed "LEFT" so we only list the products that are listed in that invoice only - For faster page loading speed
    $qtyCondition = "JOIN customer_invoice_line ci ON ci.Product_Product_ID=p.id AND ci.Customer_Invoice_Num='$editId' "; 
}
$templateLine = '';
$sql          = "SELECT p.* $qtySel FROM products p
                $qtyCondition 
                WHERE subproduct=0 ";
$result       = $db->query($sql);

//create html for customer list.
$select = "";
if ($result->num_rows > 0 && $invoices['invoiced_customer'] == 1) {
    $select .= "<select  onchange='invoiceLineSelected(this)' class='productSelect js-example-basic-multiple-limit col-sm-12' multiple='multiple' name='product[{rand}]'>";
    while ($row    = $result->fetch_assoc()) {
        $select .= "<option data-quantity='" . (isset($row['Qty']) ? $row['Qty'] : 1) . "' data-description='" . $row['description'] . "' data-price='" . $row['price'] . "' value ='" . $row["id"] . "' >" . $row["name"] . "</option>";
    }
    $select .= '</select>';
}

$ccCharges = 0.0 ;
$numCharges = 0 ;
if ($invoices['invoiced_customer'] == 1)
{
    foreach( $paymentList as $one ) {
        if( floatval($one['Charges']) > 0 ) {
            $numCharges ++ ;
            $ccCharges += $one['Charges'] ;
        }
    } 
}
//start printing invoice line template. the {rand} will be replaced with a uniq value to print each row.
ob_start();
?>
<tr class='invoice-line-tr row-{rand}'>
    <td class='slno'></td>
    <td width="25%"><?php echo $select; ?></td>
    <td class="description"></td>
    <td><input type="number" onchange="invoiceLineUpdatePrice(this)" class="quantity small-textbox" value="1" name="qty[{rand}]"/></td>
    <td class="price"></td>
    <td class="total"></td>
</tr>
<?php
$line = ob_get_clean();
?>
<style>
span.select2-selection.select2-selection--multiple {
    height: 34px;
    padding: 0px;
}
</style>
<script type="text/javascript">
    productLine = '<?php echo urlencode($line); ?>';
</script>
<input type="hidden" id="invoiceDataEx" class="invoice-data"
       data-invoice-status="<?php echo $invoices['Invoice_Status'];?>"
       data-invoice-revised="<?php echo $invoices['Invoice_Amount_Revised'];?>"
       data-balance="<?php echo $invoices['balance'];?>"
       data-invoice-Amount="<?php echo $invoices['Invoice_Amount'];?>"
       data-credit-note="<?php echo $creditNoteTotal ;?>"
       />

<form class="not-common-form" name="invoiceAddForm" action="" method="POST" id="contact1" enctype="multipart/form-data" >
            <div class="card">
                <div class="card-header">
                    <h5><?php echo ( ! isset($__GET['invid']) ? 'Create a new invoice' : 'Edit invoice' ) ;?></h5>
                </div>
                <div class="card-block pt-0 pb-0 row" style="flex-wrap: nowrap !important;">
                    <div class="form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Customer Name</label>
                        <select onchange="loadCustomerDetails(this.value)" required="required" name="CurrentCustomerSelected" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
						<?php
						foreach ($custRows as $row7) {
							$selected = '';
							if ($row7['id'] == $editContactId && $editContactId) {
								$selected = 'selected="selected"';
							}
							echo "<option $selected value='" . $row7['id'] . "'>" . $row7['fname'] . " " . $row7['mname'] . " " . $row7['lname'] . "</option>";
						}
						?>
                        </select>
                    </div>
                    <div class="form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Email Address</label>
                        <input value="<?php echo $invoices['email']; ?>" type="text" name="email" id='id_email' class="form-control" required="">
                        <input value="<?php echo $invoices['tourid']; ?>" type="text" name="GroupID" class="form-control" hidden>
                    </div>
                    <div class="form-default form-static-label col-sm-2" style="padding-top: 30px">
                        
                        <input type="checkbox" name="cbSend" style="vertical-align: middle" id="cbSendInvoice" />
                        <label for="cbSendInvoice" class="float-label gst-label"> &nbsp;&nbsp; Send Invoice
                        <?php if( $invoices['Invoice_Sent'] > 0 ) { echo ' (Already Sent)'; } ?>
                        </label>
                    </div>
                    <div class="form-default form-static-label col-sm-2 text-right">
					<?php if(isset($__GET['invid']) && $invoices['Invoice_Status'] == 0) { ?>
						<h1 class="text-center"><i class="text-danger fas fa-exclamation-triangle"></i></h1>
					<?php } ?>
                    </div>
                    <?php

                    if( $invoices['Invoice_Status'] == CustomerInvoice::$CANCELLED ) {
                        $balanceToPrint= $invoices['balance'] ;
                        $caption = 'Balance Due' ;
                        if( $balanceToPrint < 0 ) {
                            $caption = 'Refund Due' ;
                        }
                    }
                    else {
                        $caption = 'Balance Due' ;
                        $balanceToPrint= '';
                    }
                    //use invoice amount when canceled.
                    ?>
                    <div class="form-default form-static-label col-sm-2 text-right">
                    <h5 style="margin-bottom:10px;"><?php echo $caption;?></h5>
                    <h3 class="text-danger" id='id_balance_due' data-refund-status="<?php echo $invoices['RefundStatus'];?>" 
                        data-paid="<?php echo $invoices['paid'];?>" data-balance="<?php echo $balanceToPrint ?>">
                            <?php echo GUtils::formatMoney($balanceToPrint); ?>
                    </h3>
                    
                        
                    </div>
                    
                </div>
 
                <div class="card-block pt-0 row">
                    <div class="form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Billing Address</label>
                        <textarea name="billing_address" id='id_billing_address' class="form-control" rows="3" required="required" ><?php echo $invoices['address'] ;?></textarea>
                    </div>
                    <div class="form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Invoice Date</label>
                        <input type="date" required="required" name="invoice_date" id='id_invoice_date' value='<?php echo GUtils::clientDate(Date('Y-m-d H:i:s'), 'Y-m-d'); ?>' class="form-control date-picker" required="">
                    </div>
                    <div class="form-default form-static-label col-sm-2">
                        <label class="float-label gst-label" >Due Date</label>
                        <input type="date" required="required" name="due_date" id="id_due_date" class="form-control" value="<?php echo $invoices['Due_Date']; ?>">
                    </div>
                    <div class="form-default form-static-label col-sm-2">
					<?php if(isset($__GET['invid']) && $invoices['Invoice_Status'] == 0) { ?>
						<h5 class="text-center text-danger">The invoice is</h5>
						<h3 class="text-center text-danger">Voided</h3>
					<?php } else if(isset($__GET['invid']) && $invoices['Invoice_Status'] == CustomerInvoice::$CANCELLED) { ?>
						<h5 class="text-center text-danger">The invoice is</h5>
						<h3 class="text-center text-danger">Canceled</h3>
					<?php } ?>
                    </div>
                    <div class="form-default form-static-label col-sm-2">
					<?php if(isset($__GET['invid']) && $invoices['Invoice_Status'] == 0) { ?>
                        <a href="javascript:void(0);" onclick = 'deleteConfirm( "sales-invoices.php?action=unvoid&InvoiceID=<?php echo $editId; ?>", "Unvoid")'>
                            <input <?php echo (!$editId) ? 'disabled="disabled"' : ''; ?> class="btn waves-effect waves-light <?php echo (!$editId) ? 'btn-disabled' : ''; ?> btn-danger float-right" type="button" value="Un-void" />
                        </a>
					<?php } else { ?>
                        <?php if( $editId && $invoices['Invoice_Status'] != 4 ) { ?>
                        <a href="payments-add.php?invoice=<?php echo $invoices['Customer_Invoice_Num']; ?>">
                            <input <?php echo (!$editId) ? 'disabled="disabled"' : ''; ?> class="btn waves-effect waves-light <?php echo (!$editId) ? 'btn-disabled' : ''; ?> btn-info float-right" type="button" value="Receive Payment" />
                        </a>
                        <div class="clearfix"></div>
                        <a href="credit_notes_add.php?invid=<?php echo $invoices['Customer_Invoice_Num']; ?>">
                            <input <?php echo (!$editId) ? 'disabled="disabled"' : ''; ?> class="btn m-t-5 waves-effect waves-light <?php echo (!$editId) ? 'btn-disabled' : ''; ?> btn-success float-right" type="button" value="Credit Note" />
                        </a>
                        <?php } else if( $editId && $invoices['Invoice_Status'] == 4 && $invoices['RefundStatus'] == 1 && $invoices['Refund_Amount'] != 0.0 ) { ?>
                        <a href="refund_receipt_add.php?invid=<?php echo $invoices['Customer_Invoice_Num']; ?>">
                            <input <?php echo (!$editId) ? 'disabled="disabled"' : ''; ?> class="btn waves-effect waves-light <?php echo (!$editId) ? 'btn-disabled' : ''; ?> btn-info float-right" type="button" value="Issue A Refund" />
                        </a>
                        <?php } ?>
					<?php } ?>
                    </div>
                </div>
				<?php
				if($editContactId > 0) {
					$TheTourIDx = $invoices['tourid'];
					$JointCustomersSQL = "SELECT * FROM customer_groups WHERE Primary_Customer_ID=$editContactId AND Type='Group Invoice' AND Group_ID=$TheTourIDx;";
					$JointCustomersData = GDb::fetchRow($JointCustomersSQL);
					$AdditionalTravelers = "";
					if(count($JointCustomersData) > 0) {
					
					if($JointCustomersData['Additional_Traveler_ID_1'] > 0) {
						$JCustomer_ID = $JointCustomersData['Additional_Traveler_ID_1'];
						$JCsql = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
						$JCData = GDb::fetchRow($JCsql);
						$AdditionalTravelers .= "<a href='contacts-edit.php?id=".$JCData['id']."&action=edit&usertype=customer'>".$JCData['fname']." ".$JCData['lname']."</a>, ";
					}
					if($JointCustomersData['Additional_Traveler_ID_2'] > 0) {
						$JCustomer_ID = $JointCustomersData['Additional_Traveler_ID_2'];
						$JCsql = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
						$JCData = GDb::fetchRow($JCsql);
						$AdditionalTravelers .= "<a href='contacts-edit.php?id=".$JCData['id']."&action=edit&usertype=customer'>".$JCData['fname']." ".$JCData['lname']."</a>, ";
					}
					if($JointCustomersData['Additional_Traveler_ID_3'] > 0) {
						$JCustomer_ID = $JointCustomersData['Additional_Traveler_ID_3'];
						$JCsql = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
						$JCData = GDb::fetchRow($JCsql);
						$AdditionalTravelers .= "<a href='contacts-edit.php?id=".$JCData['id']."&action=edit&usertype=customer'>".$JCData['fname']." ".$JCData['lname']."</a>, ";
					}
					if($JointCustomersData['Additional_Traveler_ID_4'] > 0) {
						$JCustomer_ID = $JointCustomersData['Additional_Traveler_ID_4'];
						$JCsql = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
						$JCData = GDb::fetchRow($JCsql);
						$AdditionalTravelers .= "<a href='contacts-edit.php?id=".$JCData['id']."&action=edit&usertype=customer'>".$JCData['fname']." ".$JCData['lname']."</a>, ";
					}
					if($JointCustomersData['Additional_Traveler_ID_5'] > 0) {
						$JCustomer_ID = $JointCustomersData['Additional_Traveler_ID_5'];
						$JCsql = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
						$JCData = GDb::fetchRow($JCsql);
						$AdditionalTravelers .= "<a href='contacts-edit.php?id=".$JCData['id']."&action=edit&usertype=customer'>".$JCData['fname']." ".$JCData['lname']."</a>, ";
					}
					if($JointCustomersData['Additional_Traveler_ID_6'] > 0) {
						$JCustomer_ID = $JointCustomersData['Additional_Traveler_ID_6'];
						$JCsql = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
						$JCData = GDb::fetchRow($JCsql);
						$AdditionalTravelers .= "<a href='contacts-edit.php?id=".$JCData['id']."&action=edit&usertype=customer'>".$JCData['fname']." ".$JCData['lname']."</a>, ";
					}
					if($JointCustomersData['Additional_Traveler_ID_7'] > 0) {
						$JCustomer_ID = $JointCustomersData['Additional_Traveler_ID_7'];
						$JCsql = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
						$JCData = GDb::fetchRow($JCsql);
						$AdditionalTravelers .= "<a href='contacts-edit.php?id=".$JCData['id']."&action=edit&usertype=customer'>".$JCData['fname']." ".$JCData['lname']."</a>, ";
					}
					if($JointCustomersData['Additional_Traveler_ID_8'] > 0) {
						$JCustomer_ID = $JointCustomersData['Additional_Traveler_ID_8'];
						$JCsql = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
						$JCData = GDb::fetchRow($JCsql);
						$AdditionalTravelers .= "<a href='contacts-edit.php?id=".$JCData['id']."&action=edit&usertype=customer'>".$JCData['fname']." ".$JCData['lname']."</a>, ";
					}
				?>
				<div class="card-block pt-0 col-12">
					<label style="font-weight:bold;">Additional travelers: <?php echo substr($AdditionalTravelers,0,-2); ?>.</label>
				</div>
				<?php }} ?>

                <div class="card-block gst-block table-responsive">
                        <table id="basic-btnzz" class="table table-hover table-striped table-bordered nowrap gst-table-margin" style="border: 0px;" data-page-length="20">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Product / Service</th>
                                    <th>Description</th>
                                    <th>Quantity</th>
                                    <th>Amount</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody id='idInvoiceLineSection'>
                                <!-- Your data here -->
<?php
foreach ($invoiceLines as $v) {
    echo str_replace('{rand}', $v['Customer_Invoice_Line'], $line);
    $GLOBAL_SCRIPT .= "setInvoiceProductSelection(" . $v['Customer_Invoice_Line'] . ',' . $v['Product_Product_ID'] . ");";
}
?>
                            </tbody>
                            <tfoot class="gst-all-borderless gst-all-bold">
                                <?php 
                                
                                if( $ccCharges != 0 ) { ?>
                                    <tr>
                                        <td align="right" colspan="5">Credit Card Surcharge (4%) </td>
                                        <td align="left" ><?php echo GUtils::formatMoney($ccCharges); ?></td>
                                    </tr>
                                <?php }
                                
                                 if( $invoices['Invoice_Status'] == CustomerInvoice::$CANCELLED ) { ?>
                                <tr>
                                    <td  align="right" colspan="5">Customer Cancellation</td>
                                    <td class="invoice-cancelled" align="left"><?php echo GUtils::formatMoney($invoices['Invoice_Amount_Reverted']) ;?></td>
                                </tr>
                                <tr>
                                    <td  align="right" colspan="5">Cancellation Charge</td>
                                    <td align="left" class="cancellation_charge" 
                                        data-cancellation-charge="<?php echo floatval($invoices['Cancellation_Charge']) ;?>" ><?php echo GUtils::formatMoney($invoices['Cancellation_Charge']) ;?></td>
                                </tr>
                                <?php
                                    }
                                    
                                    if( $invoices['Payee_Cancellation_Charge'] > 0 ) { ?>
                                <tr>
                                    <td  align="right" colspan="5">Additional Customer Cancellation</td>
                                    <td align="left">
                                        <span class="hidden payee_cancellation_charge" data-cancellation-charge="<?php echo floatval($invoices['Payee_Cancellation_Charge']) ;?>"/></span>
                                        <?php echo GUtils::formatMoney($invoices['Payee_Cancellation_Charge']) ;?>
                                    </td>
                                </tr>
                                <?php
                                    }
                                ?>
                                <tr>
                                    <td colspan="2" class="align-right pl-0">
                                        <?php if( $invoices['Invoice_Status'] != 4 ) { ?>
                                        <input onclick="addInvoiceLine()" type="button" value="Add Line" class="<?php if( $editId ) { echo "d-none"; } ?> btn float-left waves-effect waves-light btn-info ml-0 mr-2 mt-2" />
                                        <input onclick="clearInvoiceLines()" type="button" value="Clear All Lines" class="<?php if( $editId ) { echo "d-none"; } ?> btn float-left waves-effect waves-light btn-info mt-2" />
                                        <?php } ?>
                                    </td>
                                    <td colspan="3" class="align-right">Total</td>
                                    <td class="invoice-total" style="background: none;" >$0.00</td>
                                </tr>
                            </tfoot>
                        </table>
                </div>
                                
                <?php if( count($paymentList) > 0 || ( is_array($creditNotes) && count($creditNotes) > 0) ) { ?>
				<br />
                <div class="card-block pt-0 pb-0">
                        <table id="basic-btnxx" class="table table-hover table-striped table-bordered nowrap gst-table-margin" style="border: 0px;" data-page-length="20">
                            <thead>
                                <?php if(is_array($paymentList) && count($paymentList) > 0 ) { ?>
                                <tr>
                                    <th width="15%">Payment Date</th>
                                    <th>Payment Type</th>
                                    <th>Description</th>
                                    <th width="10%" class="text-center" >Files</th>
                                    <th width="10%">Amount</th>
                                </tr>
                                <?php } ?>
                            </thead>
                            <tbody id='idInvoiceLineSection'>
                                <?php 
                                $paid = 0.0 ;
                                $charges = 0.0 ;
                                if(is_array($paymentList) && count($paymentList) > 0 ) { 
                                foreach( $paymentList as $one ) {
                                    $paid += floatval( $one['Customer_Payment_Amount'] ) ;
                                    $charges = $one['Charges'] ;
                                    ?>
                                <tr>
                                    <td><?php echo GUtils::clientDate($one['Customer_Payment_Date']);?></td>
                                    <td><?php echo $one['Customer_Payment_Method'];?></td>
                                    <td><?php echo $one['Customer_Payment_Comments'];?></td>
                                    <td class="text-center" >
                                        <?php if( $one['Attachment'] ) { ?>
                                        <a href="<?php echo GUtils::doDownload($one['Attachment'], 'invattach/');?>" target="_blank"><img src="files/assets/images/view-file.png" style="width:25px;height:auto;" alt="download"></a>
                                <?php } ?>
                                    </td>
                                    <td><?php echo GUtils::formatMoney( $one['Customer_Payment_Amount'] );?></td>
                                </tr>
                                <?php } 
                                }
                                
                                if(is_array($creditNotes) && count($creditNotes) > 0 ) { ?>
                                <tr>
                                    <th width="15%">Credit Note Date</th>
                                    <th colspan="3">Description</th>
                                    <th width="10%">Amount</th>
                                </tr>
                                <?php
                                }
                                $totalCredit = 0 ; 
                                foreach($creditNotes as $one ) {
                                if( $one['Credit_Amount'] != 0 ) {
                                    $totalCredit += $one['Credit_Amount'] ;
                                    ?>
                                    <tr>
                                        <td align="left" ><?php echo GUtils::clientDate($one['Credit_Note_Date']); ?></td>
                                        <td colspan="3" align="left" ><?php echo CustomerCreditNote::printCreditNoteLine($one);?></td>
                                        <td align="left" >
                                            <?php echo GUtils::formatMoney($one['Credit_Amount']); 
                                            if( $invoices['Invoice_Status'] == CustomerInvoice::$ACTIVE ) { 
                                            ?> 
                                            <a class="float-right" onclick = 'deleteConfirm( "sales-invoice-add.php?noteid=<?php echo $one['Note_ID'];?>&action=deletenote&invid=<?php echo $one['Customer_Invoice_Num'];?>")'
                                                href="javascript:void(0);">
                                                <i class="fas fa-trash"> </i>
                                            </a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php 
                                }
                                }
                                if( $invoices['RefundStatus'] == 2 ) /* Refund issued */ { ?>
                                <tr>
                                    <td><?php echo GUtils::clientDate($invoices['Refund_Date']);?></td>
                                    <td><?php echo $invoices['Cancellation_Outcome'];?></th>
                                    <td><?php echo GUtils::formatMoney( 0- $invoices['Refund_Amount'] );?></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot class="gst-all-borderless gst-all-bold">                               
                                <?php if( $invoices['Invoice_Status'] != 4 ) { ?>
                                <tr>
                                    <td colspan="4" class="align-right">Total Paid</td>
                                    <td><?php echo GUtils::formatMoney( floatval($paid) );?></td>
                                </tr>
                                <?php }
                                $balCaption = 'Balance Due' ;
                                if( $invoices['Invoice_Status'] == CustomerInvoice::$CANCELLED && $invoices['Refund_Amount'] > 0 && $invoices['Refund_Status'] != 2 ) {
                                    $balCaption = 'Refund Due' ;
                                }
                                ?>
                                <tr>
                                    <td colspan="4" class="align-right"><?php echo $balCaption;?></td>
                                    <td id="idInvoiceBalanceElement" data-charges="<?php echo $ccCharges;?>" class="invoice-balance"></td>
                                </tr>
                            </tfoot>
                        </table>
                </div>
                <?php } ?>
                
                <div class="card-block gst-block  row">
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label gst-label">Message Displayed On Invoice</label>
                        <textarea class="form-control" name="comments" ><?php echo $invoices['Comments']; ?></textarea>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label gst-label">Attachment</label>
                        <input type="file" name="filAttachment" class="form-control" >
<?php if ( strlen($invoices['Attachment']) > 0) { ?>
                        
                            <a href='<?php echo GUtils::doDownload($invoices['Attachment'], 'invattach/'); ?>' target='_blank' download>
                                <h5><i class='fas fa-cloud-download-alt'></i> <small>Download the attachment</small></h5>
                            </a>
                        
<?php } ?>
                    </div>
                </div>
                
                <div class="card-block gst-block row">
                    <div class="col-sm-12">
                        <br />
                        <input type="hidden" name="hidInvoiceSave" value="1" />
                        <?php if( $invoices['Invoice_Status'] == 4 ) { ?>
                        <button type="submit" onclick="formmodified = 0;onSaveInvoice();" value="update_message_only" name="submit" class="btn btn-success"><i class="far fa-check-circle"></i> Update Message</button>
                        <?php } else { ?>
						<div class="btn-group mr-2">
							<button type="submit" onclick="formmodified = 0;onSaveInvoice();" value="<?php echo $SubmitAction; ?>_only" name="submit" class="btn btn-success"><i class="far fa-check-circle"></i> Save & Stay</button>
							<?php if( ! defined('INVOICE_ADD_FRAGMENT_VIEW') ) { ?>
                            <button type="button" class="btn btn-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<div class="dropdown-menu">
								<button type="submit" onclick="formmodified = 0;onSaveInvoice();" value="<?php echo $SubmitAction; ?>_new" name="submit" class="dropdown-item">Save & New</button>
								<button type="submit" onclick="formmodified = 0;onSaveInvoice();" value="<?php echo $SubmitAction; ?>_close" name="submit" class="dropdown-item" href="#">Save & Close</button>
							</div>
                            <?php } ?>
						</div>
                        <?php } ?>
                        <?php if( $editId) { ?>
						<button type="button" onclick="window.open('sales-invoice-print.php?print=payment&invid=<?php echo $invoices['Customer_Invoice_Num']; ?>','_blank');" class="btn waves-effect waves-light btn-info mr-2"><i class="fas fa-print"></i>Print</button>
                        <?php } 

                        if( ! defined('INVOICE_ADD_FRAGMENT_VIEW') ) {
                        ?>
						<button type="button" onclick="window.location.href = 'sales-invoices.php'" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Cancel</button>
                        <?php } ?>
                    </div>
                </div>
                <div class="card-block gst-block row">
                    <div class="gst-spacer-10"></div>
                </div>

            </div>
</form>


<?php include 'inc/notificiations.php'; ?>

<script type="text/javascript">
    function onSaveInvoice() {
        $('#contact1').submit(function (e) {


            $('#resultsmodal #results').html('Saving ...');
            $('#resultsmodal').modal('show');
            return true ;
        });
    }
    
    //Notification if the form isnt saved before leaving the page
    $(document).ready(function () {
        formmodified = 0;
        $('form *').change(function () {
            formmodified = 1;
        });
        window.onbeforeunload = confirmExit;
        function confirmExit() {
            if (formmodified == 1) {
                return "New information not saved. Do you wish to leave the page?";
            }
        }
    });

</script>

<!-- sweet alert js -->
<script type="text/javascript" src="files/bower_components/sweetalert/js/sweetalert.min.js"></script>
<!-- modalEffects js nifty modal window effects -->
<script type="text/javascript" src="files/assets/js/modalEffects.js"></script>

<script type="text/javascript">
    //do ajax and print customer details.
    function loadCustomerDetails(val) {
        //reset first
        $('#id_email').val('');
        $('#id_billing_address').val('');
        $('#id_due_date').val('');
        $('#id_balance_due').html(formatMoney(0.0));
        $('#id_balance_due').data('balance', '0.0');

        if (val !== undefined) {
            ajaxCall('inc/ajax.php', {'action': 'customer-details', 'id': val}, function (data) {
                if (typeof data === 'object') {
                    $('#id_email').val(data.email);
                    $('#id_billing_address').val(data.address);
                    $('#id_due_date').val(data.due_date);
                    $('#id_balance_due').html(formatMoney(data.balance));
                    $('#id_balance_due').data('balance', data.balance);
                }
            });
        }
    }
    ;
    //clear invoice lines in html table.
    function clearInvoiceLines() {
        $('#idInvoiceLineSection').html('');
        updateTotal();
    }
    //render template and add a new invoice line
    function addInvoiceLine() {
        var rand = Math.random().toString(36).substr(2, 7);
        var productLineProcessed = urldecode(productLine);
        productLineProcessed = productLineProcessed.replace(/\{rand\}/g, rand);
        $('#idInvoiceLineSection').append(productLineProcessed);
        $('#idInvoiceLineSection .invoice-line-tr').find('.slno').each(function (i) {
            $(this).html(i + 1);
        });
        
        dynamicSelect2('#idInvoiceLineSection');
    }
    //update the selected product details
    function invoiceLineSelected(obj) {
        var val = $(obj).val();
        var description = $(obj).find("option[value='" + val + "']").data('description');
        var quantity = $(obj).find("option[value='" + val + "']").data('quantity');
        $(obj).closest('.invoice-line-tr').find('.description').html(description);
        $(obj).closest('.invoice-line-tr').find('.quantity').val(quantity);

        invoiceLineUpdatePrice($(obj).closest('.invoice-line-tr').find('.quantity'));
    }
    function invoiceLineUpdatePrice(textobj) {
        var qty = $(textobj).val();
        var sel = $(textobj).closest('.invoice-line-tr').find('.productSelect');
        var val = $(sel).val();

        var price = $(sel).find("option[value='" + val + "']").data('price');
        $(sel).closest('.invoice-line-tr').find('.price').html(formatMoney(price));
        var newprice = price * qty;

        $(sel).closest('.invoice-line-tr').find('.total').html(formatMoney(newprice));
        $(sel).closest('.invoice-line-tr').find('.total').data('price', newprice);

        updateTotal();
    }
    function updateTotal() {
        var listedTotal = 0 ;
        $('.invoice-line-tr .total').each(function () {
            listedTotal += parseFloat($(this).data('price')) || 0 ;
        });

debugger;

        var invoiceAmount = $('#invoiceDataEx').data('invoice-amount') ;
        var Invoice_Status = $('#invoiceDataEx').data('invoice-status') ;
        var Invoice_Revised = $('#invoiceDataEx').data('invoice-revised') ;
        var balance = $('#invoiceDataEx').data('balance') ;

        var newBal = balance ;
        if( Invoice_Status == 1 ) {
            if( listedTotal != invoiceAmount ) {
                newBal += (listedTotal - invoiceAmount); 
            }
        }
        
        $('#id_balance_due').data('balance', newBal);
        $('#id_balance_due').html(formatMoney(newBal));
        $('.invoice-balance').html(formatMoney(newBal));
        $('.invoice-total').html(formatMoney(Invoice_Revised));

    }
    //calculate balance and total
    function updateTotalOld() {
        var total = 0;
        debugger;

        $('.invoice-line-tr .total').each(function () {
            total += parseFloat($(this).data('price')) || 0 ;
        });

        var creditNote = parseFloat( $('.invoice-refund-status').data('credit-note') || 0 ) ;
        var othersCancellationCharge = parseFloat( $('.payee_cancellation_charge').data('cancellation-charge') || 0 ) ;
        var cancellationCharge = parseFloat($('.invoice-refund-status').data('cancellation') || 0) ;
        var fCharges = parseFloat( $('#idInvoiceBalanceElement').data('charges') || 0 ) ;
        var paid = parseFloat($('#id_balance_due').data('paid')) || 0;
		var charges = fCharges || 0.0 ;
        
        
        $('.invoice-cancelled').html(formatMoney( 0 - (total + fCharges) ));
        
        var cancelled = $('.invoice-refund-status').val() || 0 ;
        
        var invoiceTotal = 0 ;
        if( parseFloat(cancelled) != 0 ) {
            total = $('.invoice-refund-status').data('cancellation') ;
            invoiceTotal = parseFloat(total) ;
        }
        else {
            invoiceTotal = parseFloat(total) + parseFloat(fCharges) + parseFloat(othersCancellationCharge) ;
        }

        $('.invoice-total').html(formatMoney(invoiceTotal));
        
        var bal = parseFloat( (total+charges) - (paid+creditNote) ) ;
        
        var newBal = 0 ;
        var refundStatus = $('#id_balance_due').data('refund-status') ;
        if( refundStatus == '1') {
            newBal = $('#id_balance_due').data('balance') ;
        }
        else if( refundStatus == '2') {
            newBal = 0.0 ;
        }
        else {
            newBal = bal ;
        }
        
        $('#id_balance_due').data('balance', newBal);
        $('#id_balance_due').html(formatMoney(newBal));
        $('.invoice-balance').html(formatMoney(newBal));

    }

    function setInvoiceProductSelection(rowid, prodid) {
        $('.row-' + rowid + " select option[value='" + prodid + "']").prop("selected", true);
        $('.row-' + rowid + " select").change();
    }
</script>
<?php 
if( ! defined('INVOICE_ADD_FRAGMENT_VIEW') ) {
    include "footer.php"; 
}
?>
