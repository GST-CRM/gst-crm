<?php include "session.php";
$PageTitle = "Create A New User";
include "header.php";

?>
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->

<form name="contact1" action="inc/user-functions.php" method="POST" id="contact1" class="row">
    <input type="hidden" id="submitBtn" name="submitBtn" value="">
    <div class="col-md-12 row">
		<div class="col-md-2">
		</div>
		<div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5>Create a new user account</h5>
                </div>
				<div class="card-block  row">
                    <div class="form-group form-default form-static-label col-sm-6">
                        <label class="float-label">Username</label>
                        <input type="text" name="username" class="form-control" required="">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-6">
                        <label class="float-label">Password</label>
                        <input type="password" name="password" class="form-control" required="">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">First Name</label>
                        <input type="text" name="fname" class="form-control">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">Last Name</label>
                        <input type="text" name="lname" class="form-control">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">Email</label>
                        <input type="text" name="email" class="form-control">
                    </div>
                    <div class="col-sm-12"><br />
                    <button type="submit" name="submit" class="btn waves-effect waves-light btn-success mr-1 mt-2" data-toggle="modal" data-target="#resultsmodal" data-val="save_new"><i class="far fa-check-circle"></i>Save & New</button>
                    <button type="submit" name="submit" class="btn waves-effect waves-light btn-info mr-1 mt-2" data-toggle="modal" data-target="#resultsmodal" data-val="save_close"><i class="far fa-check-circle"></i>Save & Close</button>
                    <button type="reset" class="btn waves-effect waves-light btn-inverse mt-2"><i class="fas fa-ban"></i>Clear</button>
					</div>
				</div>
            </div>
        </div>
		<div class="col-md-2">
		</div>
	</div>
</form>

<?php include 'inc/notificiations.php'; ?>

<script type="text/javascript">
$(document).ready(function() {
	$("form").submit(function() {
		// Getting the form ID
		var  formID = $(this).attr('id');
		var formDetails = $('#'+formID);
		//To not let the form of uploading attachments included in this
		if (formID != 'contact6') {
			$.ajax({
				type: "POST",
				url: 'inc/user-functions.php',
				data: formDetails.serialize(),
				success: function (data) {	
					// Inserting html into the result div
					$('#results').html(data);
					//$("form")[0].reset();
					formmodified = 0;
				},
				error: function(jqXHR, text, error){
				// Displaying if there are any errors
					$('#result').html(error);           
				}
			});
			return false;
		}
	});
});

//Notification if the form isnt saved before leaving the page
$(document).ready(function() {
    formmodified=0;
    $('form *').change(function(){
        formmodified=1;
    });
    window.onbeforeunload = confirmExit;
    function confirmExit() {
        if (formmodified == 1) {
            return "New information not saved. Do you wish to leave the page?";
        }
    }
    
    $('.btn').click(function() {
          var buttonval    = $(this).attr('data-val');
          $("#submitBtn").val(buttonval);
    })
});

</script>

    <!-- sweet alert js -->
    <script type="text/javascript" src="files/bower_components/sweetalert/js/sweetalert.min.js"></script>
    <!-- modalEffects js nifty modal window effects -->
    <script type="text/javascript" src="files/assets/js/modalEffects.js"></script>

<?php include "footer.php"; ?>