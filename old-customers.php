<?php
include "session.php";
$PageTitle = "Users";
include "header.php"; 

//To delete the user
if ($_GET['action'] == "delete") {
	$userid = $_GET["id"];
	$con4 = new mysqli($servername, $username, $password, $dbname);
	if ($con4->connect_error) {die("Connection failed: " . $con4->connect_error);} 
	$sql2 = "DELETE FROM users WHERE UserID=$userid;";
	if ($con4->multi_query($sql2) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The user account has been removed successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting record: " . $con4->error . "</strong></div>";
	}
	$con4->close();
}
?>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
		<div class="card">
            <div class="row">
				<div class="col-md-3">
					<div class="card-header table-card-header">
						<h5>List of users</h5>
					</div>
				</div>
				<div class="col-md-6" style="margin-top:15px;">
				</div>
				<div class="col-md-3" style="margin-top:15px;">
					<a href="users-add.php" class="btn waves-effect waves-light btn-success" style="margin-right: 30px;float:right; padding: 3px 13px;"><i class="far fa-check-circle"></i>Add</a>
				</div>
			</div>
            <div class="card-block">
				<div class="dt-responsive table-responsive">
                    <table id="basic-btn" class="table table-hover table-striped table-bordered nowrap" data-page-length="20">
                        <thead>
                            <tr>
                                <th>Tour ID</th>
                                <th>Username</th>
                                <th>Full Name</th>
                                <th>Room Type</th>
                                <th>Roommate #1</th>
                                <th>Roommate #2</th>
                            </tr>
                        </thead>
                        <tbody>
						<?php
						$sql = "SELECT gro.tourid,gro.tourname,co.fname,co.mname,co.lname,ca.roomtype,(SELECT CONCAT(fname,' ', mname,' ', lname) FROM contacts WHERE id=ca.travelingwith) AS Roommate1,(SELECT CONCAT(fname,' ', mname,' ', lname) FROM contacts WHERE id=ca.travelingwith2) AS Roommate2 FROM customer_account2 ca
JOIN contacts co 
ON co.id=ca.contactid
JOIN groups gro 
ON gro.tourid=ca.tourid
WHERE ca.status=1"; $result = $db->query($sql);
						if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
                            <tr>
                                <td><?php echo $row["tourid"]; ?></td>
                                <td><?php echo $row["tourname"]; ?></td>
                                <td><?php echo $row["fname"]. " ".$row["mname"]. " ".$row["lname"]; ?></td>
                                <td><?php echo $row["roomtype"]; ?></td>
                                <td><?php echo $row["Roommate1"]; ?></td>
                                <td><?php echo $row["Roommate2"]; ?></td>
                            </tr>
						<?php }} ?>
						</tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<!-- [ page content ] end -->
<?php include "footer.php"; ?>