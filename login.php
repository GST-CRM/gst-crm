<?php
include "config.php";
session_start();
define('LOGIN_TEMPLATE', true);
$PageTitle = "Login";
include "header.php";

?>
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->
    <section class="login-block">
        <!-- Container-fluid starts -->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Authentication card start -->
                    <form name="contact3" id="contact3" class="md-float-material form-material" action="inc/login-functions.php" method="post">
                        <div class="text-center">
                            <img src="files/assets/images/auth/logo-dark.png" alt="logo.png">
                        </div>
                        <div class="auth-box card">
                            <div class="card-block">
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <h3 class="text-center">Sign In</h3>
                                    </div>
                                </div>
								<div id="results"><?php echo GUtils::flashMessage() ; ?></div>
                                
                                <div class="form-group form-primary">
                                    <input type="text" name="login_username" class="form-control" required="" autocomplete="new-password">
                                    <span class="form-bar"></span>
                                    <label class="float-label">Your Username</label>
                                </div>
                                <div class="form-group form-primary">
                                    <input type="password" name="login_password" class="form-control" required="" autocomplete="new-password">
                                    <span class="form-bar"></span>
                                    <label class="float-label">Password</label>
                                </div>
                                <div class="row m-t-25 text-left" >
                                    <div class="col-12">
                                        <div class="checkbox-fade fade-in-primary d-" style="display: none;" >
                                            <label>
                                                <input type="checkbox" value="">
                                                <span class="cr"><i class="cr-icon fas fa-check txt-primary"></i></span>
                                                <span class="text-inverse">Remember me</span>
                                            </label>
                                        </div>
                                        <div class="forgot-phone text-right float-right">
                                            <a href="forgot_password.php" class="text-right f-w-600"> Forgot Password?</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row m-t-30">
                                    <div class="col-md-12">
                                        <button type="submit" name="submit" class="btn btn-primary btn-md btn-block waves-effect waves-light text-center m-b-20">Sign in</button>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-8">
                                        <p class="text-inverse text-left m-b-0">Thank you.</p>
                                        <p class="text-inverse text-left"><a href="https://www.tourtheholylands.com/"><b>Back to website</b></a></p>
                                    </div>
                                    <div class="col-md-4">
                                        <img src="files/assets/images/auth/Logo-small-bottom.png" alt="small-logo.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- end of form -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container-fluid -->
    </section>

<script type="text/javascript">
$(document).ready(function() {
	$("form").submit(function() {
		// Getting the form ID
		var  formID = $(this).attr('id');
		var formDetails = $('#'+formID);
		//To not let the form of uploading attachments included in this
		if (formID != 'contact6') {
			$.ajax({
				type: "POST",
				url: 'inc/login-functions.php',
				data: formDetails.serialize(),
				success: function (data) {	
					// Inserting html into the result div
					$('#results').html(data);
					//$("form")[0].reset();
					formmodified = 0;
				},
				error: function(jqXHR, text, error){
				// Displaying if there are any errors
					$('#result').html(error);           
				}
			});
			return false;
		}
	});
});
</script>
	
<?php include "footer.php"; ?>
