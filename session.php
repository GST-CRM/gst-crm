<?php
   include('config.php');
   require_once __DIR__ . "/models/acl_permission.php";
   session_start();
   
   $user_check = $_SESSION['login_user'];
   
   $ses_sql = mysqli_query($db,"select * from users where Username = '$user_check' ");
   
   $row = mysqli_fetch_array($ses_sql,MYSQLI_ASSOC);
   
   $login_session = $row['Username'];
   $adminname = $row['fname']." ".$row['lname'];
   $UserProfile = $row['Profile_Path'];
   $UserAdminID = $row['UserID'];
    
   global $G_ROLE_ID ;
   $G_ROLE_ID = $row['Role_ID'] ;
   
   if(!isset($_SESSION['login_user'])){
      header("location:login.php");
	  exit;
   }
   
    //ACL Check {
    $page = basename($_SERVER['PHP_SELF']);

    $acl = new AclPermission();
    if ( ! $acl->isPageAllowed($page)) {
        header("location:login.php");
        exit;
    }
    //}
   
	/*$RemoteResponse = file_get_contents('https://element.ps/Projects/License/index.php?domain=goodshepherdtravel.net');
	$LocalResponse = md5('Success');
	if($RemoteResponse!=$LocalResponse) {}*/

	//If the HTTPS is not found to be "on"
	if(!isset($_SERVER["HTTPS"]) || $_SERVER["HTTPS"] != "on")
	{
		//Tell the browser to redirect to the HTTPS URL.
		header("Location: https://" . $_SERVER["HTTP_HOST"] . $_SERVER["REQUEST_URI"], true, 301);
		//Prevent the rest of the script from executing.
		exit;
	}

?>