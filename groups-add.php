<?php include "session.php";
$PageTitle = "Create A New Group";
include "header.php";

//Get the last ID in the products table
$GetLastProductID = "SELECT MAX(id) FROM products;" ;
$LastProductID = GDb::fetchScalar($GetLastProductID);
$AirlineProductID = $LastProductID+1;
$LandProductID = $LastProductID+2;


$TourName		= mysqli_real_escape_string($db, $_POST["tourname"]);
$TourDesc		= mysqli_real_escape_string($db, $_POST["tourdesc"]);
$Destination	= mysqli_real_escape_string($db, $_POST["Destination"]);
$StartDate		= mysqli_real_escape_string($db, $_POST["startdate"]);
$EndDate		= mysqli_real_escape_string($db, $_POST["enddate"]);
$ListPrice		= mysqli_real_escape_string($db, $_POST["listprice"]);
$SplitPrice = $ListPrice/2; //To add it for the airline and land products
$TripCost		= mysqli_real_escape_string($db, $_POST["tripcost"]);
$SplitCost = $TripCost/2; //To add it for the airline and land products
$status			= mysqli_real_escape_string($db, $_POST["status"]);

$theagent		= "";
if(isset($_POST['submit'])) {
	foreach ($_POST['theagent'] as $select2) {
		$theagent .= $select2 . ",";
	}
	$theagent = substr($theagent,0,-1);  //to remove the last comma
}

$thegroupleader	= "";
if(isset($_POST['submit'])) {
	foreach ($_POST['thegroupleader'] as $select3) {
		$thegroupleader .= $select3 . ",";
	}
	$thegroupleader = substr($thegroupleader,0,-1);  //to remove the last comma
}

//Create the group and add it to the Groups Table
if(isset($_POST['submit'])) {
	$sql = "INSERT INTO groups (tourname, tourdesc, destination, startdate, enddate, listprice, tripcost, agent, groupleader,airline_productid,land_productid,status)
	VALUES ('$TourName', '$TourDesc', '$Destination', '$StartDate', '$EndDate', '$ListPrice', '$TripCost', '$theagent', '$thegroupleader','$AirlineProductID','$LandProductID','$status')";

	if ($db->query($sql) === TRUE) {
		//Get the group ID and create a group_purhase_order
		$GroupID 	= $db->insert_id;
		date_default_timezone_set('America/Chicago');
		$NowDate 	= date("Y-m-d");
		$sql2 		= "INSERT INTO group_purchase_order (Group_Group_ID,PO_Date,Group_PO_Stop,Total_Order_Amount) VALUES ('$GroupID', '$NowDate','0','0');";
		//Add the product for the Airline
		$sql2.= "INSERT INTO products(name, description, supplierid, categoryid, subproduct, cost, price) VALUES ('Airline for #$GroupID','$TourName - Draft Airline Product','14745','1','0','$SplitCost','$SplitPrice');";
		//Add the product for the Land
		$sql2.= "INSERT INTO products(name, description, supplierid, categoryid, subproduct, cost, price) VALUES ('Land for #$GroupID','$TourName - Draft Land Product','14744','2','0','$SplitCost','$SplitPrice');";
		if ($db->multi_query($sql2) === TRUE) {
			//Show the success message and either stay on the same page or redirect to the groups list page
		    if ($_POST['submitBtn'] == 'save_close') {
    			GUtils::setSuccess("New Group has been created successfully! <a style='color: white;font-weight: bold;' href='groups-edit.php?id=$GroupID&action=edit'>Click here to edit the group.</a>");
		        GUtils::redirect('groups.php');
		    } else if ($_POST['submitBtn'] == 'save_only') {
		    	GUtils::redirect('groups-edit.php?id='.$GroupID.'&action=edit');
		    } else {
    			GUtils::setSuccess("New Group has been created successfully!");
		        GUtils::redirect('groups-add.php');
			}
		} else {
			echo $sql2 . "<br>" . $db->error."<br> error";
		}
	} else {
		echo $sql . "<br>" . $db->error."<br> error";
	}
	$db->close();
}
?>
<form  id="main" method="post" action="" class="form-material row">
	<input type="hidden" id="submitBtn" name="submitBtn" value="" >
    <div class="row">
		<div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5>Group's Basic Information</h5>
                </div>
				<div class="card-block  row">
                    <div class="form-group form-default form-static-label col-sm-4" style="display:none;">
                        <input type="text" name="tourid" class="disabled form-control" value="Auto Generated" disabled>
                        <span class="form-bar"></span>
                        <label class="float-label">Group's Code</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-12">
                        <input type="text" name="tourname" class="form-control" required="">
                        <span class="form-bar"></span>
                        <label class="float-label">Group's Name</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-12">
                        <input type="text" name="tourdesc" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">Group's Description</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <input type="text" name="Destination" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">Destination</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <input type="date" name="startdate" class="form-control" required>
                        <span class="form-bar"></span>
                        <label class="float-label">Group's Start Date</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <input type="date" name="enddate" class="form-control" required>
                        <span class="form-bar"></span>
                        <label class="float-label">Group's End Date</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-6">
                        <input type="text" name="tripcost" class="form-control" required>
                        <span class="form-bar"></span>
                        <label class="float-label">Trip Cost</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-6">
                        <input type="text" name="listprice" class="form-control" required>
                        <span class="form-bar"></span>
                        <label class="float-label">Trip Price</label>
                    </div>
                    <div class="col-sm-12"><br />
                    <button type="submit" name="submit" class="btn waves-effect waves-light btn-success mr-1" data-val="save_only"><i class="far fa-check-circle"></i>Save</button>
					<button type="submit" name="submit" class="btn waves-effect waves-light btn-success mr-1" data-val="save_new"><i class="far fa-check-circle"></i>Save & New</button>
					<button type="submit" name="submit" class="btn waves-effect waves-light btn-info mr-1" data-val="save_close"><i class="far fa-check-circle"></i>Save & Close</button>
                    <button type="reset" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Clear</button>
					</div>
				</div>
            </div>
        </div>
		<div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h5>Group's required info</h5>
                </div>
                <div class="card-block  row">
                    <div class="form-group col-sm-12">
                        <h4 class="sub-title">Group Status</h4>
						<div class="can-toggle">
							<input id="e" name="status" value="1" type="checkbox" checked>
							<label for="e">
								<div class="can-toggle__switch" data-checked="Active" data-unchecked="Inactive"></div>
							</label>
						</div>
                    </div>
                    <div class="col-sm-12">
                        <h4 class="sub-title">The Agent <span style="color:red;">*</span></h4>
                        <select name="theagent[]" class="js-example2-basic2-multiple-limit col-sm-12" multiple="multiple" required>
							<?php $con5 = new mysqli($servername, $username, $password, $dbname);
							$result5 = mysqli_query($con5,"SELECT * FROM agents WHERE status=1");
							while($row5 = mysqli_fetch_array($result5))
							{
								//here, we are checking the Agent ID with the contact column list to show their names
								$con6 = new mysqli($servername, $username, $password, $dbname);
								$result6 = mysqli_query($con6,"SELECT id, fname, mname, lname FROM contacts WHERE `usertype` LIKE '%1%'");
								while($row6 = mysqli_fetch_array($result6))
								{
									if($row5['contactid'] == $row6['id']) {
										echo "<option value='".$row5['contactid']."'>".$row6['fname']." ".$row6['lname']."</option>";
									}
								}
							} ?>
                        </select>
                    </div>
                    <div class="col-sm-12">
						<br />
						<h4 class="sub-title">The Group Leader</h4>
						<select name="thegroupleader[]" class="js-example2-basic2-multiple-limit col-sm-12" multiple="multiple">
							<?php //here, we are checking the Agent ID with the contact column list to show their names
							$con7 = new mysqli($servername, $username, $password, $dbname);
							$result7 = mysqli_query($con7,"SELECT id, fname, mname, lname FROM contacts WHERE `usertype` LIKE '%3%'");
							while($row7 = mysqli_fetch_array($result7))
							{
								echo "<option value='".$row7['id']."'>".$row7['fname']." ".$row7['lname']."</option>";
							} ?>
						</select>
                    </div>
				</div>
			</div>
		</div>
	</div>
</form>
<?php include "footer.php"; ?>

<script type="text/javascript">
	$(document).ready(function() {
	    $('.btn').click(function() {
	          var buttonval    = $(this).attr('data-val');
	          $("#submitBtn").val(buttonval);
	    })
	});
</script>