<!-- [ breadcrumb ] start -->
<div class="page-header">
    <div class="page-block">
        <style>
			.FavoriteButton { width:15px;height:15px;background: transparent url('files/assets/images/bookmark-alt.png') center top;float: left; margin-right: 5px; margin-left: 5px; margin-top: 3px; }
			.FavoriteButton:hover { background: transparent url('files/assets/images/bookmark-alt.png') center bottom; }
			.breadcrumb-item+.breadcrumb-item::before { display:none !important; }
			.breadcrumb-item { margin-left: 20px; }
		</style>
        <nav aria-label="breadcrumb" class="float-right">
            <ul class="breadcrumb breadcrumb-dot bg-default">
                <?php
                $index = 0 ;
                $favorites = (new UserFavorites())->getFavoriteList();
                if( count($favorites) > 0 ) {
                    $curUri = UserFavorites::requestUriToStore() ;
                    foreach( $favorites as $one ) {
                        $selected = "text-dark" ;
                        if( $one['URI'] == $curUri ) {
                            $selected = "text-info" ;
                        }
                        
                ?>
                <li class="breadcrumb-item limited-text p-0 " >
                    <a title="Remove Favorite" class="FavoriteButton float " href="<?php echo UserFavorites::requestUriToPrint() . $symbol ;?>favorite-action=remove&favorite-uri=<?php echo urlencode($one['URI']);?>&favorite-redirect=<?php echo urlencode( UserFavorites::requestUriToPrint() );?>">
						<span></span>
                    </a>
                    <a title="<?php echo $one['Title'];?>" href="<?php echo $one['URI_PRINT'];?>" class="<?php echo $selected;?> breadcrumb-item-text"><?php echo GUtils::excerpt($one['Title'], 30);?></a>
                </li>
                <?php 
                        if( ++ $index >= 6 ){ 
                            break ;
                        }
                    }
                }
                if( count($favorites) > 6  ) {
                ?>                
                <li class="nav-item dropdown ">
                    <a class="nav-link dropdown-toggl" href="#" id="navbarDropdownMenu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <span class="feather icon-arrow-down"></span>
                    </a>
                    <ul class="dropdown-menu text-dark dropdown-menu-right" aria-labelledby="navbarDropdownMenu">
                        <?php
                        $index = 0 ;                            
                        foreach( $favorites as $one ) {
                            if( ++$index <= 6 ){
                                continue;
                            }
                            ?>
                            <li>
                                <a title="Remove Favorite" class="FavoriteButton float " href="<?php echo UserFavorites::requestUriToPrint() . $symbol ;?>favorite-action=remove&favorite-uri=<?php echo urlencode($one['URI']);?>&favorite-redirect=<?php echo urlencode( UserFavorites::requestUriToPrint() );?>">
                                    <span></span>
                                </a>
                                <a title="<?php echo $one['Title'];?>" href="<?php echo $one['URI_PRINT'];?>" class="<?php echo $selected;?> breadcrumb-item-text"><?php echo GUtils::excerpt($one['Title'], 30);?></a>
                            </li>
                        <?php } ?>        
                    </ul>
                </li>
                <?php
                }
                ?>
            </ul>
        </nav>

        <div class="row align-items-center">
            <div class="col-md-10">
                <div class="page-header-title">
                    <h4 class="m-b-10"><?php echo $PageTitle; ?></h4>
                </div>
                <ul class="breadcrumb d-none">
                    <li class="breadcrumb-item">
                        <a href="dashboard.php">
                            <i class="feather icon-home"></i>
                        </a>
                    </li>
                    <li class="breadcrumb-item">
                        <a href=""><?php echo $PageTitle; ?></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- [ breadcrumb ] end -->
