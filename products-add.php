<?php include "session.php";
$PageTitle = "Add New Product";
include "header.php";

?>
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->

<form  id="main" method="post" action="" class="row">
    <input type="hidden" id="submitBtn" name="submitBtn" value="" >
    <div class="row">
		<div class="col-md-9">
            <div class="card">
				<div class="card-block  row">
                    <div class="form-group form-default form-static-label col-sm-9">
                        <label class="float-label">Product Name</label>
                        <input type="text" name="productname" class="form-control" required="">
                    </div>
					<div class="form-group form-default form-static-label col-sm-3">
                        <div class="checkbox-fade fade-in-success">
						<label class="float-label">Sub-Product?</label><br />
                            <label>
                                <input type="checkbox" name="subproduct" id="subproduct" value="1">
                                <span class="cr">
                                    <i class="cr-icon fas fa-check txt-success"></i>
                                </span>
                                <span>Yes</span>
                            </label>
                        </div>
					</div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label">Product Discription</label>
                        <input type="text" name="description" class="form-control">
                    </div>
					<div id="parentcategory" class="col-sm-3" style="display:none;">
						<label class="float-label">Parent Product</label>
						<select name="parentid" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<?php $con10 = new mysqli($servername, $username, $password, $dbname);
							$result10 = mysqli_query($con10,"SELECT * FROM products WHERE subproduct=0");
							while($row10 = mysqli_fetch_array($result10))
							{
								echo "<option value='".$row10['id']."'>".$row10['name']."</option>";
							} ?>
						</select>
					</div>
					<div id="producttypediv" class="form-group form-default form-static-label col-sm-3" style="display:none;">
						<label class="float-label">Product Type</label>
						<select name="producttype" id="producttype" class="form-control form-control-default">
							<option value="0" disabled selected></option>
							<option id="Airfare" value="Airfare">Airfare</option>
							<option id="Hotels" value="Hotels">Hotels</option>
							<option id="Restaurants" value="Restaurants">Restaurants</option>
							<option id="Guides" value="Tour Guides">Tour Guides</option>
							<option id="Insurance" value="Insurance">Insurance</option>
						</select>
					</div>
                    <div id="productcost" class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label">Cost $</label>
                        <div class="dollar"><input type="text" name="cost" class="form-control"></div>
                    </div>
                    <div id="productprice" class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label">Price $</label>
                        <div class="dollar"><input type="text" name="price" class="form-control"></div>
                    </div>
					<div id="productterms" class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Payment Term</label>
						<select name="paymentterm" class="form-control form-control-default">
							<option value="0" disabled selected> </option>
							<option value="1">Lump Sum</option>
							<option value="2">Per Person</option>
							<option value="3">Per Day</option>
						</select>
					</div>
                    <div id="hotelcity" class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label">Hotel's City</label>
                        <input type="text" name="hotelcity" class="form-control">
                    </div>
                    <div id="checkindate" class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label">Check in Date</label>
                        <input type="date" name="checkindate" class="form-control">
                    </div>
                    <div id="checkoutdate" class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label">Check out Date</label>
                        <input type="date" name="checkoutdate" class="form-control">
                    </div>
                    <div id="departurecity" class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label">Departure City</label>
                        <input type="text" name="departurecity" class="form-control">
                    </div>
                    <div id="departurestate" class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label">Departure State</label>
                        <input type="text" name="departurestate" class="form-control">
                    </div>
                    <div id="GuidePhone" class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label">Tour Guide Phone #</label>
                        <input type="text" name="GuidePhone" class="form-control">
                    </div>
                    <div id="GuideCity" class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label">Tour Guide City</label>
                        <input type="text" name="GuideCity" class="form-control">
                    </div>
                    <div class="col-sm-12">
                        <br />
                        <button type="submit" name="submit" class="btn waves-effect waves-light btn-success mr-1" data-toggle="modal" data-target="#resultsmodal" data-val="save_new"><i class="far fa-check-circle"></i>Save & New</button>
    					<button type="submit" name="submit" class="btn waves-effect waves-light btn-info mr-1" data-toggle="modal" data-target="#resultsmodal" data-val="save_close"><i class="far fa-check-circle"></i>Save & Close</button>
                        <button type="reset" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Clear</button>
					</div>
				</div>
            </div>
        </div>
		<div class="col-md-3">
            <div class="card">
                <div class="card-header">
                    <h5>Required Information</h5>
                </div>
                <div class="card-block  row">
                    <div class="form-group col-sm-12">
                        <h4 class="sub-title">Product Status</h4>
						<div class="can-toggle">
							<input id="e" name="status" value="1" type="checkbox" checked>
							<label for="e">
								<div class="can-toggle__switch" data-checked="Active" data-unchecked="Inactive"></div>
							</label>
						</div>
                    </div>
                    <div class="col-sm-12">
						<h4 class="sub-title">Product's Supplier</h4>
						<select name="thesupplier" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<?php //here, we are checking the Agent ID with the contact column list to show their names
							$con7 = new mysqli($servername, $username, $password, $dbname);
							$result7 = mysqli_query($con7,"SELECT id, fname, mname, lname FROM contacts WHERE `usertype` LIKE '%2%'");
							while($row7 = mysqli_fetch_array($result7))
							{
								echo "<option value='".$row7['id']."'>".$row7['fname']." ".$row7['mname']." ".$row7['lname']."</option>";
							} ?>
						</select>
                    </div>
                    <div class="col-sm-12"><br />
						<h4 class="sub-title">Product's Category</h4>
						<select name="thecategory" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<?php //here, we are checking the Agent ID with the contact column list to show their names
							$con8 = new mysqli($servername, $username, $password, $dbname);
							$result8 = mysqli_query($con8,"SELECT * FROM products_category");
							while($row8 = mysqli_fetch_array($result8))
							{
								echo "<option value='".$row8['id']."'>".$row8['name']."</option>";
							} ?>
						</select>
                    </div>
				</div>
			</div>
		</div>
	</div>
</form>

<?php include 'inc/notificiations.php'; ?>

<script type="text/javascript">

//This is so it can update the data without refresh from the inc folder
$(document).ready(function() {
	$("form").submit(function() {
        $('#results').html("Saving...");
		// Getting the form ID
		var  formID = $(this).attr('id');
		var formDetails = $('#'+formID);
		//To not let the form of uploading attachments included in this
		if (formID != 'contact6') {
			$.ajax({
				type: "POST",
				url: 'inc/group-functions.php',
				data: formDetails.serialize(),
				success: function (data) {	
					// Inserting html into the result div
					$('#results').html(data);
					//$("form")[0].reset();
					formmodified = 0;
				},
				error: function(jqXHR, text, error){
				// Displaying if there are any errors
					$('#result').html(error);           
				}
			});
			return false;
		}
	});
});


//This is so it can show the new field where you can create a new product type
function admSelectCheck(nameSelect)
{
    if(nameSelect){
        admOptionValue = document.getElementById("admOption").value;
        if(admOptionValue == nameSelect.value){
            document.getElementById("producttypenew").style.display = "block";
            document.getElementById("producttype").style.display = "none";
        }
        else{
            document.getElementById("producttypenew").style.display = "none";
        }
    }
    else{
        document.getElementById("producttypenew").style.display = "none";
    }
}


//This is so it can show or hide fields depending on the field of is it subcategory or not
$(function () {
        $("#subproduct").click(function () {
            if ($(this).is(":checked")) {
                $("#parentcategory").show();
                $("#producttypediv").show();
                $("#productcost").hide();
                $("#productprice").hide();
                $("#productterms").hide();
            } else {
                $("#parentcategory").hide();
                $("#producttypediv").hide();
                $("#productcost").show();
                $("#productprice").show();
                $("#productterms").show();
      $("#departurestate").hide();
      $("#departurecity").hide();
      $("#hotelcity").hide();
      $("#checkindate").hide();
      $("#checkoutdate").hide();
            }
        });
    });
	
//This is so it can show or hide fields when the product type is selected, like hotels, restaurants, ...
$(function() {
  $("#producttype").change(function() {
    if ($("#Hotels").is(":selected")) {
      $("#hotelcity").show();
      $("#checkindate").show();
      $("#checkoutdate").show();
    } else {
      $("#hotelcity").hide();
      $("#checkindate").hide();
      $("#checkoutdate").hide();
    }
    if ($("#Airfare").is(":selected")) {
      $("#departurestate").show();
      $("#departurecity").show();
    } else {
      $("#departurestate").hide();
      $("#departurecity").hide();
    }
    if ($("#Guides").is(":selected")) {
      $("#GuidePhone").show();
      $("#GuideCity").show();
    } else {
      $("#GuideCity").hide();
      $("#GuidePhone").hide();
    }
  }).trigger('change');
});


//This is for not leaving the page without saving your data
$(document).ready(function() {
    formmodified=0;
    $('form *').change(function(){
        formmodified=1;
    });
    window.onbeforeunload = confirmExit;
    function confirmExit() {
        if (formmodified == 1) {
            return "New information not saved. Do you wish to leave the page?";
        }
    }
    
    $('.btn').click(function() {
          var buttonval    = $(this).attr('data-val');
          $("#submitBtn").val(buttonval);
    })
});

</script>

<!-- sweet alert js -->
<script type="text/javascript" src="files/bower_components/sweetalert/js/sweetalert.min.js"></script>
<!-- modalEffects js nifty modal window effects -->
<script type="text/javascript" src="files/assets/js/modalEffects.js"></script>


<?php include "footer.php"; ?>