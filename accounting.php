<?php include "session.php";
$PageTitle = "Accounting";
include "header.php";
?>
    <div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <h5>Create</h5>
                </div>
				<div class="card-block row">
					<div class="col-md-4">
						<h5 style="border-left: solid 3px #17a2b8;padding-left:10px;">Customers</h5>
						<br />
						<ul class="list-group">
							<li class="list-group-item list-group-item-action"><a href="sales-invoice-add.php">Invoice</a></li>
                            <li class="list-group-item list-group-item-action"><a href="payments-add.php">Recieve Payment</a></li>
							<li class="list-group-item list-group-item-action"><a href="sales-add.php">Sales Order</a></li>
							<li class="list-group-item list-group-item-action"><a href="sales-receipts.php">Sales Receipt</a></li>
                            <li class="list-group-item list-group-item-action"><a href="refund_receipt_add.php">Refund Receipt</a></li>
                            <li class="list-group-item list-group-item-action"><a href="credit_notes_add.php">Credit Notes</a></li>
						</ul>
					</div>
					<div class="col-md-4">
						<h5 style="border-left: solid 3px #17a2b8;padding-left:10px;">Suppliers and Expenses</h5>
						<br />
						<ul class="list-group">
							<li class="list-group-item list-group-item-action"><a href="bills-add.php">Supplier Bill</a></li>
                            <li class="list-group-item list-group-item-action"><a href="suppliers-payments.php">Supplier Payments</a></li>
							<li class="list-group-item list-group-item-action">Supplier Credit</li>
							<li class="list-group-item list-group-item-action">Purchase Order</li>
							<li class="list-group-item list-group-item-action"><a href="expenses-add.php">Expense Bill</a></li>
							<li class="list-group-item list-group-item-action"><a href="expense-payment-add.php">Expense Payment</a></li>
						</ul>
					</div>
					<div class="col-md-4">
						<h5 style="border-left: solid 3px #17a2b8;padding-left:10px;">Other</h5>
						<br />
						<ul class="list-group">
							<li class="list-group-item list-group-item-action">Bank Deposit</li>
							<li class="list-group-item list-group-item-action">Transfer</li>
							<li class="list-group-item list-group-item-action">Payroll</li>
							<li class="list-group-item list-group-item-action">Journal Entry</li>
							<li class="list-group-item list-group-item-action">Estimate\Quotes</li>
						</ul>
					</div>
				</div>
				<br />
            </div>
        </div>
		<div class="col-md-1"></div>
	</div>
<?php include "footer.php"; ?>