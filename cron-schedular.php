<?php
include "session.php";
$PageTitle = "Email Schedular";
include "header.php";

if ($_GET['action'] == "delete") {
	$cron_id = $_GET['cron_id'];
	$sql = "DELETE FROM cron_schedular WHERE Cron_ID=$cron_id";
	if ($db->multi_query($sql) === TRUE) {
		GUtils::setSuccess("<strong>The selected cron record has been deleted successfully.</strong>");
	} else {
		GUtils::setSuccess("<strong>Error deleting record: " . $db->error . "</strong>");
	}
}

if (($_GET['action'] == "disable") || ($_GET['action'] == "enable")) {
	$cron_id = $_GET['cron_id'];
	if ($_GET['action'] == "disable") {
		$setStatus = 0;
	}
	if ($_GET['action'] == "enable") {
		$setStatus = 1;
	}
	$sql = "UPDATE cron_schedular SET Cron_Status = $setStatus WHERE Cron_ID=$cron_id";
	if ($db->multi_query($sql) === TRUE) {
		if ($_GET['action'] == "disable") {
			GUtils::setSuccess("<strong>The selected cron record has been disabled successfully.</strong>");
		}
		if ($_GET['action'] == "enable") {
			GUtils::setSuccess("<strong>The selected cron record has been enabled successfully.</strong>");
		}
	} else {
		GUtils::setSuccess("<strong>Error Disable/Enable record: " . $db->error . "</strong>");
	}
}

?>
<?php echo GUtils::flashMessage() ; ?>
<div class="row">
	<div class="col-sm-12">
		<div class="card">
            <div class="row">
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
					</div>
				</div>
				<div class="col-md-5" style="margin-top:15px;"></div>
				<div class="col-md-2" style="margin-top:15px;">
					<select id="status" name="status" class="form-control form-control-default fill" onChange="doReload(this.value);">
						<option value="">Status</option>
						<option value="active" <?php if(!empty($_GET['status']) AND $_GET['status'] == "active") {echo "selected";} ?>>Active Jobs</option>
						<option value="disabled" <?php if(!empty($_GET['status']) AND $_GET['status'] == "disabled") {echo "selected";} ?>>Disabled Jobs</option>
					</select>
				</div>
				<div class="col-md-2" style="margin-top:15px;padding-right:45px;">
					<a href="cron-schedular-add.php" class="btn waves-effect float-right waves-light btn-success" style="padding: 3px 13px;"><i class="fas fa-plus-circle"></i>Add New Job</a>
				</div>
				<div class="col-md-12">
				</div>
			</div>
            <div class="card-block">
				<div class="dt-responsive table-responsive">
                    <table id="templates_grid" class="table table-hover table-striped table-bordered" data-page-length="10">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Job Name</th>
                                <th>Template Name</th>
                                <th>Scheduled For</th>
                                <th>Created On</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                        		if ($_GET['status'] == "disabled") {$cronStatusWhere="WHERE cs.Cron_Status=0";} elseif($_GET['status'] == "active") {$cronStatusWhere="WHERE cs.Cron_Status=1";} else {$cronStatusWhere="";}

                        		$CronJobListsql = "SELECT cs.*,et.template_name FROM cron_schedular cs JOIN email_templates_new et ON cs.Email_Template_ID=et.template_id $cronStatusWhere ORDER BY cs.cron_id DESC";
                        		$CronJobLists = GDb::fetchRowSet($CronJobListsql);
								
								foreach ($CronJobLists as $CronJob) {
									$statusDisabled = ($CronJob['Cron_Status'] == 0) ? "style='background: #ffc3c3'" : '';
									if($CronJob['Cron_Loop'] == 1) { $scheduled_for = $CronJob['Cron_Schedule_Date'];}
									if($CronJob['Cron_Loop'] == 2) { $scheduled_for = "Hourly";}
									if($CronJob['Cron_Loop'] == 3) { $scheduled_for = "Daily";}
									if($CronJob['Cron_Loop'] == 4) { $scheduled_for = "Weekly";}
									if($CronJob['Cron_Loop'] == 5) { $scheduled_for = "Monthly";}
									if($CronJob['Cron_Loop'] == 99) { $scheduled_for = $CronJob['Cron_Period']." ".$CronJob['Cron_Days']." days";} ?>
									<tr <?php echo $statusDisabled; ?>>
										<td><?php echo $CronJob['Cron_ID'] ?></td>
										<td><a href="cron-schedular-edit.php?id=<?php echo $CronJob["Cron_ID"]; ?>&action=edit" style="color: #0000EE;"><?php echo wordwrap($CronJob['Cron_Name'],55,"<br>\n") ?></a></td>
										<td><a href="email-templates-edit.php?id=<?php echo $CronJob["Email_Template_ID"]; ?>&action=edit" style="color: #0000EE;" title="<?php echo $CronJob['template_name']; ?>"><?php echo substr($CronJob['template_name'],0,50)."..."; ?></a></td>
										<td><?php echo $scheduled_for; ?></td>
										<td><?php echo date('m/d/Y', strtotime($CronJob['Cron_Created_On'])); ?></td>
										<td>
											<div class="btn-group">
												<a class="btn btn-primary btn-sm" href="javascript:void(0)" role="button" id="dropdownMenuLink">Actions</a>
												<button type="button" class="btn btn-sm btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
													<span class="sr-only">Toggle Dropdown</span>
												</button>
												<div class="dropdown-menu">
													<a class="dropdown-item" href="cron-schedular-edit.php?id=<?php echo $CronJob["Cron_ID"]; ?>&action=edit" >Edit</a>
													<?php if ($CronJob['Cron_Status'] == 1) { ?>
														<a class="dropdown-item" href="javascript:void(0);" onclick = 'deleteConfirm( "cron-schedular.php?action=disable&cron_id=<?php echo $CronJob["Cron_ID"]; ?>", "Disable", "to Disable this Cron")'>Disable</a>
														<?php } else { ?>
														<a class="dropdown-item" href="javascript:void(0);" onclick = 'deleteConfirm( "cron-schedular.php?action=enable&cron_id=<?php echo $CronJob["Cron_ID"]; ?>", "Enable", "to Enable this Cron")'>Enable</a>
														<?php } ?>
														<a class="dropdown-item" href="javascript:void(0);" onclick = 'deleteConfirm( "cron-schedular.php?action=delete&cron_id=<?php echo $CronJob["Cron_ID"]; ?>", "Delete")'>Delete</a>
												</div>
											</div>
										</td>
									</tr>
								<?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
	</div>
</div>
<script>
//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#templates_grid').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
});

$(document).ready(function() {
    $('#templates_grid').DataTable( {
        "ordering": false,
		"bLengthChange": false, //To hide the Show X entries dropdown
		dom: 'Bfrtip',
		sDom: 'lrtip', //To Hide the search box
        buttons: []
    } );
} );

function doReload(status){
	document.location = 'cron-schedular.php?status=' + status;
}

</script>
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>