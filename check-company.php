<?php
include "session.php";
$PageTitle = "Edit the Company Details";

if(isset($_GET['id']) && is_numeric($_GET['id'])) {
    //$CompanyID = mysqli_real_escape_string($db, $_GET["id"]);
    $CompanyID = 1;
} else {
    header("location: checks.php");die('Please go back to the main page.');
}

include "header.php";


$GetCompanyDataSQL = 'SELECT `Company_Name`, `Company_Address`, `Company_Address2`, `Company_Phone`, `Company_Email`, `Company_Logo` FROM `checks_company` WHERE Company_ID='.$CompanyID;
$CompanyData = GDb::fetchRow($GetCompanyDataSQL);

if(isset($_POST["AddNewBankSubmit"])) {
 
    $CompanyName = mysqli_real_escape_string($db, $_POST["CompanyName"]);
    $CompanyAddress1 = mysqli_real_escape_string($db, $_POST["CompanyAddress1"]);
    $CompanyAddress2 = mysqli_real_escape_string($db, $_POST["CompanyAddress2"]);
    $CompanyPhone = mysqli_real_escape_string($db, $_POST["CompanyPhone"]);
    $CompanyEmail = mysqli_real_escape_string($db, $_POST["CompanyEmail"]);
    $CompanyLogo = mysqli_real_escape_string($db, $_POST["fileToUploadCurrent"]);
    
    $target_dir = "uploads/";
    $target_file_name_only = basename($_FILES["fileToUpload"]["name"]);
    $target_file = $target_dir . $target_file_name_only;
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    $temp = explode(".", $_FILES["fileToUpload"]["name"]);
    $filename = time() . '.' . end($temp);
    $fileurl = $target_dir . $filename;
    
    if($target_file_name_only != "") {
        $CanUpload = 1;
        $CompanyLogo = $filename;
    } else {
        $CanUpload = 0;
    }

    $UploadMessageText = "";

    if ($_FILES["fileToUpload"]["name"] != "" AND $CanUpload == 1) {
        // Check if file already exists
        if (file_exists($target_file)) {
            $UploadMessageText .= "Sorry, file already exists. ";
            $uploadOk = 0;
        }
        // Check file size
        if ($_FILES["fileToUpload"]["size"] > 50000000) {
            $UploadMessageText .= "Sorry, your file is too large. ";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
            $UploadMessageText .= "Sorry, only JPG, JPEG, PNG, GIF & PDF & DOCX & DOC files are allowed. ";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            $UploadMessageText .= "Sorry, your file was not uploaded.";
            echo $UploadMessageText;

        // if everything is ok, try to upload file
        } else {
            move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $fileurl);
        }
    }
        $UpdateCompanySQL = "UPDATE checks_company SET Company_Name='$CompanyName',Company_Address='$CompanyAddress1',Company_Address2='$CompanyAddress2',Company_Phone='$CompanyPhone',Company_Email='$CompanyEmail',Company_Logo='$CompanyLogo' WHERE Company_ID='$CompanyID'";
        $UpdateCompanyLogSQL = "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','company','$CompanyID','updated','$CompanyName');";

        if (GDb::execute($UpdateCompanySQL) && GDb::execute($UpdateCompanyLogSQL)) {
            echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The company ($CompanyName) has been updated successfully.</strong></div>";
            echo "<meta http-equiv='refresh' content='2;check-company.php?id=$CompanyID'>";
        } else {
            echo $db->error;
        }  
}

?>

<style>
@font-face {
  font-family: MICRfont;
  src: url(files/assets/fonts/micrenc.ttf);
}
.select2-container--default .select2-selection--single .select2-selection__rendered {background:white;color:#333;padding:2px 30px 3px 20px;border-radius:4px;}
.select2-container--default .select2-selection--single .select2-selection__arrow {top:4px;}
.select2-container--default .select2-selection--single .select2-selection__arrow b {border-color:black transparent transparent transparent;}
.select2-selection {height:35px;padding:0px !important;}
</style>

<form name="contact6" id="contact6" action="" method="POST" enctype="multipart/form-data">
<div class="row">
    <div class="col-sm-5">
        <div class="card">
            <div class="card-header pb-3">
                <h5>Company Information</h5>
            </div>
            <div class="card-block row pt-0">
                <div class="form-group form-default form-static-label col-sm-12">
                    <label class="float-label">Company Name</label>
                    <input type="text" name="CompanyName" id="CompanyName" value="<?php echo $CompanyData['Company_Name']; ?>" oninput="CompanyNameFunc()" class="form-control">
                </div>
                <div class="form-group form-default form-static-label col-sm-6">
                    <label class="float-label">Address #1</label>
                    <input type="text" name="CompanyAddress1" id="CompanyAddress1" value="<?php echo $CompanyData['Company_Address']; ?>" oninput="CompanyAddress1Func()" class="form-control">
                </div>
                <div class="form-group form-default form-static-label col-sm-6">
                    <label class="float-label">Address #2</label>
                    <input type="text" name="CompanyAddress2" id="CompanyAddress2" value="<?php echo $CompanyData['Company_Address2']; ?>" oninput="CompanyAddress2Func()" class="form-control">
                </div>
                <div class="form-group form-default form-static-label col-sm-6">
                    <label class="float-label">Phone Number</label>
                    <input type="text" name="CompanyPhone" id="CompanyPhone" value="<?php echo $CompanyData['Company_Phone']; ?>" oninput="CompanyPhoneFunc()" class="form-control">
                </div>
                <div class="form-group form-default form-static-label col-sm-6">
                    <label class="float-label">Email Address</label>
                    <input type="text" name="CompanyEmail" id="CompanyEmail" value="<?php echo $CompanyData['Company_Email']; ?>" oninput="CompanyEmailFunc()" class="form-control">
                </div>
                <div class="form-group form-default form-static-label col-sm-8">
                    <label class="float-label" for="CompanyLogo">Company Logo</label>
                    <div class="Attachment_Box">
                        <input type="text" name="fileToUploadCurrent" value="<?php echo $CompanyData['Company_Logo']; ?>" hidden>
                        <input type="file" name="fileToUpload" id="CompanyLogo" value="" class="Attachment_Input">
                        <p id="CompanyLogo_Text" class="Attachment_Text">Drag the file here or click in this area.</p>
                    </div>
                </div>
                <div class="form-group form-default form-static-label col-sm-4">
                    <label class="float-label">Logo Preview</label>
                     <?php if($CompanyData['Company_Logo'] != '') {
                         $LogoPath = "uploads/".$CompanyData['Company_Logo'];
                     } else { 
                            $LogoPath = $crm_images."/logo-Placeholder.png";
                     } ?>
                    <img src="<?php echo $LogoPath; ?>" alt="Company Logo" class="img-fluid" />
                </div>
                <div class="col-sm-12">
                    <hr />
                    <button type="submit" name="AddNewBankSubmit" value="yes" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal" style="margin-right:20px;" ><i class="far fa-check-circle"></i>Save</button>
                    <button type="button" onclick="window.location.href = 'check-banks.php'" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"> </i>Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-7">
        <div class="card">
            <div class="card-header">
                <h5>Preview</h5>
            </div>
            <div class="card-block row pt-0 no-gutters text-center">
                <div class="col-12 row no-gutters p-3" style="background:#f9f9f9 url(files/assets/images/cream-pixels.png);border: solid 1px #333; border-radius: 15px;">
                    <div class="col-2" style="line-height:13px;">
                        <img src="<?php echo $LogoPath; ?>" class="img-fluid" style="height:60px;" alt="Bank Check Company Photo" />
                    </div>
                    <div class="col-4" style="line-height:13px;">
                        <b id="CompanyNameText"><?php echo $CompanyData['Company_Name']; ?></b>
                        <br />
                        <small>
                            <span id="CompanyAddress1Text"><?php echo $CompanyData['Company_Address']; ?></span>
                            <br />
                            <span id="CompanyAddress2Text"><?php echo $CompanyData['Company_Address2']; ?></span>
                            <br />
                            <span id="CompanyPhoneText"><?php echo $CompanyData['Company_Phone']; ?></span>
                            <br />
                            <span id="CompanyEmailText"><?php echo $CompanyData['Company_Email']; ?></span>
                        </small>
                    </div>
                    <div class="col-4">
                        <b id="BankNameText">Your Bank Name</b><br /><small id="BankAddressText">Bank City, State, Zip Code</small>
                    </div>
                    <div class="col-2 text-center">
                        <h4 class="mb-0" id="CheckNumText">No. 1001</h4>
                        <h6 id="TransitCodeText"></h6>
                    </div>
                    <div class="col-10"></div>
                    <div class="col-2 text-center pb-2"><p style="color:black">
                        <b>Date:</b> <span id="CheckDateText">__ / __ / ____</span></p></div>
                    <div class="col-10 text-left" style="border-bottom:solid 2px #333;">
                        <b>Pay To The Order Of:</b> <span id="CustomerNameText">-</span>
                    </div>
                    <div class="col-2 text-center">
                        <h5 id="CheckAmountText">$ _____ . __</h5>
                    </div>
                    <div class="col-12 pt-2 text-left" id="CustomerAddressText"></div>
                    <div class="col-12 pt-2 text-left" style="border-bottom:solid 2px #333;" id="CheckAmountWords">
                        <b>Amount in words</b>
                    </div>
                    
                    <div class="col-7 pt-3 text-left">
                        <b>Memo:</b> <u id="CheckMemoText"></u>
                    </div>
                    <div class="col-5 pt-3" style="border-bottom:solid 2px #333;"></div>
                    <div class="col-12 text-center pt-2 pb-0"><h4 class="mb-0" style="font-family:MICRfont;">C00<span id="CheckNum2Text">1001</span>C A<span id="RoutingNumText">67-76890</span>A <span id="AccountNumText">111000025</span>C</h4></div>
                </div>
                <br/>
                <label class="mt-2" style="margin:auto;font-size:12px;color:#333;">*The info on this check is for sample purposes only.*</label>
            </div>
        </div>
    </div>
</div>
</form>


<div class="modal fade" id="resultsmodal" tabindex="-1" role="dialog" style="top:50px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
                <div id="results" class="confirmation-message-result-area">
					<div class="upload-loader"></div>
					<p>Prcoessing...</p>
				</div>
            </div>
            <div class="modal-footer2">
                <button type="button" class="btn btn-success waves-effect waves-light" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
function CompanyNameFunc() {
    var x = document.getElementById("CompanyName").value;
    document.getElementById("CompanyNameText").innerHTML = x;
}
function CompanyAddress1Func() {
    var x = document.getElementById("CompanyAddress1").value;
    document.getElementById("CompanyAddress1Text").innerHTML = x;
}
function CompanyAddress2Func() {
    var x = document.getElementById("CompanyAddress2").value;
    document.getElementById("CompanyAddress2Text").innerHTML = x;
}
function CompanyPhoneFunc() {
    var x = document.getElementById("CompanyPhone").value;
    document.getElementById("CompanyPhoneText").innerHTML = x;
}
function CompanyEmailFunc() {
    var x = document.getElementById("CompanyEmail").value;
    document.getElementById("CompanyEmailText").innerHTML = x;
}

//This is to make the attachment box update its content when a file is selected
$(document).ready(function(){
  $('#CompanyLogo').change(function () {
    $('#CompanyLogo_Text').text("A file has been selected");
  });
});

    
$(document).ready(function() {
	$("form").submit(function() {
		// Getting the form ID
		var  formID = $(this).attr('id');
		var formDetails = $('#'+formID);
		//To not let the form of uploading attachments included in this
		if (formID != 'contact6' && formID != 'idGroupEmailForm') {
			$.ajax({
				type: "POST",
				url: 'inc/checks-functions.php',
				data: formDetails.serialize(),
				success: function (data) {	
					// Inserting html into the result div
					$('#results').html(data);
					//$("form")[0].reset();
				},
				error: function(jqXHR, text, error){
				// Displaying if there are any errors
					$('#result').html(error);           
				}
			});
			return false;
		}
	});
});
</script>


<?php //include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>
