<?php

include "session.php";
include_once "inc/helpers.php";


$printTitle = 'Sales Receipt' ;

if( isset($__REQUEST['batch-invaction']) ) {
    if( $__REQUEST['batch-invaction'] == 'P' ) {
        include 'inc/sales-receipts-pdf.php';
    }
    else if( $__REQUEST['batch-invaction'] == 'S' ) {
        $FileTitlePrefix = 'receipt' ;
        include 'inc/sales-invoice-mail.php';
        echo '<script type="text/javascript">window.close();</script>' ;
        die;
    }
}

$PageTitle = "Sales Receipts ";
include "header.php";

?>

    <form action="sales-receipts.php" method="POST" name="customerPaymentsListForm" >
        <!-- [ page content ] start -->
        <div class="row">
            <div class="col-sm-12">
                <!-- HTML5 Export Buttons table start -->
                <div class="card">
					<div class="row">
						<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
							<div class="input-group input-group-sm mb-0">
								<span class="input-group-prepend mt-0">
									<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
								</span>
								<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
							</div>
						</div>
						<div class="col-md-7" style="margin-top:15px;"></div>
						<div class="col-md-2" style="margin-top:15px; padding-right:45px;">
							<span>
								<select name="batch-invaction" onchange="return batchSubmit(this);" class="form-control form-control-default fill gst-invoice-batchaction" disabled="disabled">
									<option value="">Batch Action</option>
									<option value="S">Send Receipts</option>
									<option value="P">Print Receipts</option>
								</select>
							</span>
						</div>
					</div>
                    <div class="row">
                        <div class="col-md-3">
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="dt-responsive table-responsive">
                            <table id="idPaymentReceivedTable" class="table gst-table table-hover table-striped table-bordered nowrap"
                                   data-page-length="10">
                                <thead>
                                <tr>
                                    <th class="gst-no-sort gst-has-events">
                                        <input type="checkbox" class="gst-invoice-cb-parent" />
                                    </th>
                                    <th width="1%">Invoice #</th>
                                    <th>Invoice Date</th>
                                    <th>Customer Name</th>
                                    <th>Group Reference</th>
                                    <th>Invoice Amount</th>
                                    <th>Paid Amount</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- HTML5 Export Buttons end -->
            </div>
        </div>
    </form>

<script type="text/javascript">
    
    function sendReminderHandler(obj) {
        
        $("#showSendReminder").modal("show");
        
        $('.gst-reminder-id').val( $(obj).data('id') ) ;
        $('.gst-reminder-class').html( $(obj).data('name') ) ;
        $('.gst-reminder-email').val( $(obj).data('email') ) ;
        $('.gst-reminder-subject').val( '' ) ;
        $('.gst-reminder-message').val( '' ) ;
    }
    
    function batchSubmit(obj) {
        var count = $('.gst-invoice-cb:checked').length ;
        if( count > 0 ) {
            
            obj.form.target = '_blank' ;
            if( obj.value == 'S' ) {
                msg = "Are you sure you want to sent " + count + " receipt(s) ?" ;
            }
            else if( obj.value == 'P' ) {
                msg = "Are you sure you want to print " + count + " receipt(s) ?" ;
            }
            else {
                return ;
            }
            showConfirm( "javascript:customerPaymentsListForm.submit();", "Yes", msg, "Please Confirm", "modal-md") ;
//            obj.form.submit();
        }
    }

    document.addEventListener('DOMContentLoaded', function () {
    $('#idPaymentReceivedTable').DataTable({
		 "bProcessing": true,
         "serverSide": true,
		 dom: 'Bfrtip',
		 sDom: 'lrtip', //To Hide the search box
		 "bLengthChange": false, //To hide the Show X entries dropdown
		 "columnDefs": [
                     {
                        "targets": 0,
                        "orderable": false
                        } 
		   ],
		 buttons: [ ],
         ajax : {
            url :"inc/datatable-sales-receipts.php", // json datasource
            complete : function(){
                delayedBindingCheckbox() ;

              }
          }
        }); 
    } ) ;
	
	
//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#idPaymentReceivedTable').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
});
</script>

<?php
global $GLOBAL_SCRIPT ;
$GLOBAL_SCRIPT .= "bindCheckAll('.gst-invoice-cb-parent', '.gst-invoice-cb');" ;
?>

<?php include 'inc/notificiations.php'; ?>
    <!-- [ page content ] end -->
<?php include "footer.php"; ?>