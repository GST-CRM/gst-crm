<?php
include "session.php";
$PageTitle = "Supplier Payment";
include "header.php";

global $GLOBAL_SCRIPT;
$SubmitAction = 'create';


$sqlCust  = "SELECT c.id,fname,mname,lname FROM contacts c
			INNER JOIN suppliers supp ON supp.contactid=c.id
			WHERE supp.status=1
			ORDER BY c.fname, c.mname, c.lname";
$custRows = GDb::fetchRowSet($sqlCust);

$invoiceId         = '';
$customerId        = '';
$where             = ' AND 0';
$paymentData       = [];
$editPaymentAmount = 0;
$isEditMode        = false;
$paymentMethodEdit = '' ;
if (isset($_GET['payment'])) {
    $isEditMode = true;
    $paymentId  = $_GET['payment'];

    $sql         = "SELECT * FROM suppliers_payments WHERE Supplier_Payment_ID='$paymentId'";
    $paymentData = GDb::fetchRow($sql);

    $_GET['invoice']   = $paymentData['Supplier_Bill_Num'];
    $invoiceId   = $paymentData['Supplier_Bill_Num'];
    $customerId = $paymentData['Supplier_ID'];
    $editPaymentAmount = $paymentData['Supplier_Payment_Amount'];

//    var_dump($paymentData); die;Customer_Payment_Amount
}
if (isset($_GET['bill'])) {
    $customerId = '';
    $invoiceId  = $_GET['bill'];
    $where      = " AND sb.Supplier_Bill_Num='$invoiceId' ";

    $tripref = "SELECT Supplier_ID AS contactid FROM suppliers_bill WHERE Supplier_Bill_Num='$invoiceId' ";

    $trips = GDb::fetchRowSet($tripref);
    //find customer id 
    foreach ($trips as $trip) {
        $customerId = $trip['contactid'];
        break;
    }
} else if (isset($_GET['supplier'])) {
    $customerId = $_GET['supplier'];
    $where      = " AND sb.Supplier_ID ='$customerId' ";

    $tripref = "SELECT g.tourid, g.tourname FROM customer_account AS ca 
        LEFT JOIN `groups` AS g ON g.tourid=ca.tourid WHERE ca.contactid='$customerId' ";

    $trips = GDb::fetchRowSet($tripref);
	if (isset($_GET['GroupID'])) {
		$GroupIDget = $_GET['GroupID'];
		$where      .= " AND po.Group_ID ='$GroupIDget' ";
	}
}
//overwrite where 
if (isset($_GET['payment'])) {
    $where = " AND sp.Supplier_Payment_ID='$paymentId' ";
}

$groupby = ' GROUP BY sb.Supplier_Bill_Num';

$sql     = "SELECT SUM(sp.Charges) AS charges, SUM(IFNULL(sp.Supplier_Payment_Amount,0)) AS payments, sp.Supplier_Payment_Comments, c.fname, c.lname, c.mname, sup.contactid, g.tourname, sb.Supplier_Bill_Num, sb.Due_Date, sb.Bill_Amount
			FROM suppliers_bill AS sb
			LEFT JOIN suppliers AS sup ON sup.contactid = sb.Supplier_ID AND sup.status=1
			LEFT JOIN contacts AS c ON c.id = sup.contactid
			LEFT JOIN purchase_order AS po ON po.Purchase_Order_Num=sb.Purchase_Order_Num
			LEFT JOIN `groups` AS g ON g.tourid=po.Group_ID
			LEFT JOIN suppliers_payments sp ON sb.Supplier_Bill_Num = sp.Supplier_Bill_Num AND sp.Status=1
			WHERE sb.Status IN (1,2) $where $groupby";
$records = GDb::fetchRowSet($sql);



//Save and Close -- Save and New buttons
if (isset($_POST['hidPaymentSave']) && $_POST['hidPaymentSave'] == '1') {

    include 'inc/suppliers-bill-payments-save.php';

	//Save & Close Button
    if ($_POST['submit'] == 'save') {
        $str = '';
		if($invoiceId) {
			$str = '?bill=' . $invoiceId;
		}
        GUtils::redirect('bills-edit.php?id='.$invoiceId.'&action=edit');
		
    //Save & New Button
	} else {
        $str = '';
		if($invoiceId) {
			$str = '?bill=' . $invoiceId;
		}
        GUtils::redirect('suppliers-bill-payments-add.php' . $str);
    }
}

?>
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5>
                    <?php echo (isset($_GET['payment']) ? 'Edit payment #' . $_GET['payment'] : 'Make Payment'); ?>
                </h5>
            </div>
            <form name="invoiceAddForm" action="" method="POST" id="contact1" class="card-block gst-block  row gst-no-right-p" enctype="multipart/form-data">

                <div class="col-md-12 row">
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Supplier Name</label>
                        <select onclick="formmodified=0;" onchange="onCustomerChange(this)" required="required" name="CurrentCustomerSelected" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
                            <?php foreach ($custRows as $row7) {
                                $selected = '';
                                if ($row7['id'] == $customerId && $customerId) {
                                    $selected = 'selected="selected"';
                                }
                                echo "<option $selected value='" . $row7['id'] . "'>" . $row7['fname'] . " " . $row7['mname'] . " " . $row7['lname'] . "</option>";
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Find By Bill ID</label>

                        <div class="input-group  col-centered gst-input-group">
                            <input type="text" name="invoice" id="idFindByInvoice" class="form-control" value="<?php echo $invoiceId; ?>" style="padding-left: 10px">
                            <span class="input-group-btn">
                                <button onclick="formmodified = 0; loadByInvoice() " type="submit" class="btn btn-default">
                                    <div class="fa-search fas"></div>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-6 text-right">
                        <h5 class="">Amount Paid</h5> <br />
                        <h3 class="text-danger" id='id_received_amount' data-paid="0" data-balance="0"><?php echo GUtils::formatMoney($editPaymentAmount); ?></h3>
                    </div>
                </div>
                <div class="col-md-12 row">
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Payment Date</label>
                        <input type="date" required="required" name="invoice_date" id='id_invoice_date' value='<?php if($paymentData["Supplier_Payment_Date"] > 0) {echo $paymentData["Supplier_Payment_Date"];} else {echo GUtils::clientDate(Date('Y-m-d H:i:s'), 'Y-m-d');}?>' class="form-control date-picker" required="">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Supplier Balance Account</label>
                        <?php
                        $$GetSupplierAccount = "SELECT `Bank_ID` FROM `checks_bank` WHERE `Bank_Type` = 'supplier' AND `Bank_Name` = '$customerId'";
                        $SupplierAccount = GDb::fetchRow($$GetSupplierAccount); 
                        if(count($SupplierAccount) > 0) { 
                            $Supplier_Account = $SupplierAccount['Bank_ID'];
                            $GetSupplierCredit = "SELECT SUM(Trans_Credit - Trans_Debit) AS Supplier_Balance FROM (
                                                    (SELECT
                                                        'Deposit' AS Trans_Type,
                                                        de.Deposit_Amount AS Trans_Credit,
                                                        '0.00' AS Trans_Debit
                                                    FROM checks_bank_deposits de
                                                    WHERE de.Deposit_To=$Supplier_Account)
                                                    UNION ALL
                                                    (SELECT
                                                        'Withdrawal' AS Trans_Type,
                                                        '0.00' AS Trans_Credit,
                                                        de.Deposit_Amount AS Trans_Debit
                                                    FROM checks_bank_deposits de
                                                    WHERE de.Deposit_From=$Supplier_Account)
                                                    UNION ALL
                                                    (SELECT
                                                        'Payment' AS Trans_Type,
                                                        '0.00' AS Trans_Credit,
                                                        sp.Supplier_Payment_Amount AS Trans_Debit
                                                    FROM suppliers_payments sp
                                                    WHERE sp.Supplier_Payment_Financial_Account=$Supplier_Account)
                                                 ) t1";
					        $SupplierCredit = GDb::fetchRow($GetSupplierCredit); ?>
                            <input type="text" value="<?php echo GUtils::formatMoney($SupplierCredit['Supplier_Balance']); ?>" class="form-control" readonly>
                        <?php } else { ?>
                            <a href="contacts-edit.php?id=<?php echo $customerId; ?>&action=edit&usertype=supplierBalance" target="_blank"><input type="text" value="Click here to create a supplier balance first" style="cursor:pointer;" class="form-control" readonly></a>
                        <?php } ?>
                    </div>
                </div>

                <div class="col-md-12 row">

                    <div class="dt-responsive table-responsive col-sm-12">

                        <b style="padding-bottom: 10px;float: left;width: 100%;">Outstanding Transaction</b>
                        <table id="basic-btnX" class="table table-hover table-striped table-bordered nowrap gst-table-margin" data-page-length="20">
                            <thead>
                                <tr>
                                    <th style="display: none" width="1%"></th>
                                    <th width="10%">Bill #</th>
                                    <th>Product Name</th>
                                    <th width="10%">Due Date</th>
                                    <th>Total</th>
                                    <th>Remaining</th>
                                    <th width="15%">Payment Amount</th>
                                    <th>Payment Notes</th>
                                </tr>
                            </thead>
                            <tbody id='idInvoiceLineSection'>
                                <?php foreach ($records as $row) { ?>
                                <tr>
                                    <td style="display: none">
                                        <?php if (($row["Bill_Amount"] - $row['payments']) > 0.0) { ?>
                                        <input name="cbPayment[<?php echo $row["Supplier_Bill_Num"]; ?>]" value="<?php echo $row["Supplier_Bill_Num"]; ?>" type="checkbox" class="payment-checkbox" onclick="togglePaymentInput(this)" checked="checked" />
                                        <?php } ?>
                                    </td>
                                    <td><a href="bills-edit.php?id=<?php echo $row["Supplier_Bill_Num"]; ?>&action=edit" style="color: #0000EE;"><?php echo $row["Supplier_Bill_Num"]; ?></a></td>
                                    <td><?php echo $row["tourname"]; ?></td>
                                    <td><?php echo GUtils::clientDate($row["Due_Date"]); ?></td>
                                    <td><?php echo GUtils::formatMoney($row["Bill_Amount"]); ?></td>
                                    <td><?php echo GUtils::formatMoney(($row["Bill_Amount"] + $row["charges"]) - $row['payments']); ?></td>
                                    <td onclick="autoshowPayment(this)">
                                        <div class="dollar"><input data-balance="<?php echo ($row["Bill_Amount"] - $row['payments']); ?>" type="text" name="payment[<?php echo $row["Supplier_Bill_Num"]; ?>]" class="form-control payment-input" style=" <?php /*echo ( ($isEditMode) ? '' : ': hidden');*/ ?>" onchange="onChangePayment(this)" value="<?php echo $editPaymentAmount; ?>" /></div>
                                    </td>
                                    <td><input type="text" name="description[<?php echo $row["Supplier_Bill_Num"]; ?>]" value="<?php echo (($isEditMode) ? $row['Supplier_Payment_Comments'] : '') ;?>" /></td>
                                </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr class="payment-charges">
                                    <td class="text-right" colspan="6">Credit Card Charges (4%)</td>
                                    <td id="idCreditCardCharges"></td>
                                </tr>
                            </tfoot>
                        </table>

                        <input type="hidden" name="hidPaymentSave" value="1" />
                    </div>

                </div>

                <div class="col-md-12 row">
                    <div class="form-group form-default form-static-label col-sm-12">
                        <button type="submit" onclick="formmodified = 0;window.onbeforeunload = null;" value="save-new" name="submit" class="btn waves-effect waves-light btn-success mt-2 mr-1">
                            <i class="far fa-check-circle"></i> Save & New
                        </button>
                        <button type="submit" onclick="formmodified = 0; window.onbeforeunload = null;" value="save" name="submit" class="btn waves-effect waves-light btn-info mt-2 mr-1">
                            <i class="far fa-check-circle"></i> Save & Close
                        </button>
                        <button type="button" onclick="window.onbeforeunload = null; window.location.href = 'suppliers-payments.php'" class="btn waves-effect waves-light btn-inverse mt-2 mr-1"><i class="fas fa-ban"></i>Cancel</button>
                        <?php if( isset($row["Customer_Invoice_Num"]) ) { ?>
                        <a target="_blank" href="sales-invoice-print.php?print=payment&invid=<?php echo $row["Customer_Invoice_Num"] ; ?>">
                            <button type="button" class="btn waves-effect waves-light btn-info float-right"><i class="fas fa-print"></i>Print</button>
                        </a>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-md-12 row">
                    <div class="gst-spacer-10"></div>
                </div>
            </form>
        </div>
    </div>
</div>




<?php include 'inc/notificiations.php'; ?>

<script type="text/javascript">
    function loadByInvoice() {
        window.location.href = 'suppliers-bill-payments-add.php?bill=' + $('#idFindByInvoice').val();
    }

    function autoshowPayment(obj) {
        if (!$(obj).parent().find('.payment-checkbox').prop('checked')) {
            $(obj).parent().find('.payment-checkbox').trigger('click');
        }
    }

    function togglePaymentInput(obj) {

        //        if ($(obj).prop('checked')) {
        //            $(obj).parent().parent().find('.payment-input').css('visibility', 'visible');
        //            $(obj).parent().parent().find('.payment-input').focus();
        //        }
        //        else {
        //            $(obj).parent().parent().find('.payment-input').css('visibility', 'hidden');
        //        }
    }

    function onSaveInvoice() {
        $('#contact1').submit(function(e) {


            $('#resultsmodal #results').html('Saving ...');
            $('#resultsmodal').modal('show');
            return true;
        });
    }
    onSaveInvoice();

    //Notification if the form isnt saved before leaving the page
    $(document).ready(function() {
        formmodified = 0;
        $('form *').change(function() {
            formmodified = 1;
        });
        window.onbeforeunload = confirmExit;

        function confirmExit() {
            if (formmodified == 1) {
                return "New information not saved. Do you wish to leave the page?";
            }
        }
    });

</script>

<!-- sweet alert js -->
<script type="text/javascript" src="files/bower_components/sweetalert/js/sweetalert.min.js"></script>
<!-- modalEffects js nifty modal window effects -->
<script type="text/javascript" src="files/assets/js/modalEffects.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#idSelectPaymentMethod').on('change', function() {
            if (this.value == 'Cash') {
                $("#Accounts_cash").show('slow');
                $("#Accounts_None").hide('slow');
                $("#Accounts_cards").hide('slow');
                $("#Accounts_bank").hide('slow');
                $("#Accounts_paypal").hide('slow');
                $("#CheckNumberDiv").hide('slow');
            } else if (this.value == 'Bank Transfer') {
                $("#Accounts_cash").hide('slow');
                $("#Accounts_None").hide('slow');
                $("#Accounts_cards").hide('slow');
                $("#Accounts_bank").show('slow');
                $("#Accounts_paypal").hide('slow');
                $("#CheckNumberDiv").hide('slow');
            } else if (this.value == 'Check') {
                $("#Accounts_cash").hide('slow');
                $("#Accounts_None").hide('slow');
                $("#Accounts_cards").hide('slow');
                $("#Accounts_bank").show('slow');
                $("#Accounts_paypal").hide('slow');
                $("#CheckNumberDiv").show('slow');
            } else if (this.value == 'Credit Card') {
                $("#Accounts_cash").hide('slow');
                $("#Accounts_None").hide('slow');
                $("#Accounts_cards").show('slow');
                $("#Accounts_bank").hide('slow');
                $("#Accounts_paypal").hide('slow');
                $("#CheckNumberDiv").hide('slow');
            } else if (this.value == 'Online Payment') {
                $("#Accounts_cash").hide('slow');
                $("#Accounts_None").hide('slow');
                $("#Accounts_cards").hide('slow');
                $("#Accounts_bank").hide('slow');
                $("#Accounts_paypal").show('slow');
                $("#CheckNumberDiv").hide('slow');
            }
        });
    });


    var selection = document.getElementById("Accounts_bank");
    selection.onchange = function(event) {
        var checknumber = event.target.options[event.target.selectedIndex].dataset.checknumber;
        document.getElementById("Check_Number").value = checknumber;
    };

    function onChangePayment(obj) {

        var amount = $(obj).val();
        var bal = $(obj).data('balance');

        if (bal - amount < 0) {
            //            $(obj).val(bal);
            $(obj).addClass('text-danger');
        } else {
            $(obj).removeClass('text-danger');
        }

        updateTotal();
    }

    function updateTotal() {
        var total = 0.0;
        $('.payment-input').each(function() {
            total += parseFloat($(this).val()) || 0.0;
        });

        var ccharge = 0;
        if ($('#idSelectPaymentMethod').val() === 'Credit Card') {
            ccharge = total * 4 / 100;
        }
        //        total = total + ccharge ; //Comment this line if you dont want to update total invoice amount.
        $('#id_received_amount').data('balance', total);
        $('#id_received_amount').html(formatMoney(total));
        $('#idCreditCardCharges').html(formatMoney(ccharge));
    }

    function paymentMethodChange() {
        if ($('#idSelectPaymentMethod').val() === 'Credit Card') {
            $('.payment-charges').show();
        } else {
            $('.payment-charges').hide();
        }
        updateTotal();
    }
    paymentMethodChange();
    <?php
        global $GLOBAL_SCRIPT ;
        $GLOBAL_SCRIPT .= 'updateTotal();' ;
    ?>

    function loadCustomerDetails(val) {
        //reset first
        $('#id_email').val('');
        $('#id_billing_address').val('');
        $('#id_due_date').val('');
        $('#id_received_amount').html(formatMoney(0.0));
        $('#id_received_amount').data('balance', '0.0');

        if (val !== undefined) {
            ajaxCall('inc/ajax.php', {
                'action': 'customer-details',
                'id': val
            }, function(data) {
                if (typeof data === 'object') {
                    $('#id_email').val(data.email);
                    $('#id_billing_address').val(data.address);
                    $('#id_due_date').val(data.due_date);
                    $('#id_received_amount').html(formatMoney(data.balance));
                    $('#id_received_amount').data('balance', data.balance);
                }
            });
        }
    }

    function invoiceLineSelected(obj) {
        var val = $(obj).val();
        var description = $(obj).find("option[value='" + val + "']").data('description');
        var quantity = $(obj).find("option[value='" + val + "']").data('quantity');
        $(obj).closest('.invoice-line-tr').find('.description').html(description);
        $(obj).closest('.invoice-line-tr').find('.quantity').val(quantity);

        invoiceLineUpdatePrice($(obj).closest('.invoice-line-tr').find('.quantity'));
    }

    function invoiceLineUpdatePrice(textobj) {
        var qty = $(textobj).val();
        var sel = $(textobj).closest('.invoice-line-tr').find('.productSelect');
        var val = $(sel).val();

        var price = $(sel).find("option[value='" + val + "']").data('price');
        $(sel).closest('.invoice-line-tr').find('.price').html(formatMoney(price));
        var newprice = price * qty;

        $(sel).closest('.invoice-line-tr').find('.total').html(formatMoney(newprice));
        $(sel).closest('.invoice-line-tr').find('.total').data('price', newprice);

        updateTotal();
    }

    function setInvoiceProductSelection(rowid, prodid) {
        $('.row-' + rowid + " select option[value='" + prodid + "']").prop("selected", true);
        $('.row-' + rowid + " select").change();
    }

    function onCustomerChange(obj) {
        formmodified = 0;
        window.onbeforeunload = null;
        if (obj.value) {
            window.location.href = 'suppliers-bill-payments-add.php?supplier=' + obj.value;
        }
    }

</script>
<?php include "footer.php"; ?>