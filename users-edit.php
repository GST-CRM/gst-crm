<?php include "session.php";
$PageTitle = "Edit the user";
include "header.php";

$userid = $_GET["id"];

//To collect the data of the above user id, and show it in the fields below
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {die("Connection failed: " . $conn->connect_error);} 
$sql = "SELECT * FROM users WHERE UserID=$userid";
$result = $conn->query($sql);
$data = $result->fetch_assoc();
$conn->close();


?>
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->

<form name="contact6" id="contact6" action="inc/user-edit-functions.php" method="POST" class="row" enctype="multipart/form-data">
    <div class="col-md-12 row">
		<div class="col-md-2">
		</div>
		<div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5>Edit the user - <?php echo $data['Username']; ?></h5>
                </div>
				<div class="card-block row">
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">Profile Picture (Click on the photo to change it)</label><br />
						<?php if($data['Profile_Path'] == "") { ?>
						<img id="ProfileThumb" src="files/assets/images/default-user-thumb.jpg" alt="" width="150" />
						<input type="file" id="fileToUpload" name="fileToUpload" class="form-control" style="display: none;" />
						<?php } else { ?>
						<img id="ProfileThumb" src="uploads/profile/<?php echo $data['Profile_Path']; ?>" alt="" width="150" />
						<input type="file" id="fileToUpload" name="fileToUpload" class="form-control" style="display: none;" />
						<?php } ?>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">Username</label>
                        <input type="text" name="currentusername" class="form-control" value="<?php echo $data['Username']; ?>" readonly>
                        <input type="text" name="userid" class="form-control" value="<?php echo $data['UserID']; ?>" hidden>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">New Password? Type it down.</label>
                        <input type="password" name="NewPassword" class="form-control" value="Password">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">First Name</label>
                        <input type="text" name="fname" class="form-control" value="<?php echo $data['fname']; ?>">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">Last Name</label>
                        <input type="text" name="lname" class="form-control" value="<?php echo $data['lname']; ?>">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">Email</label>
                        <input type="text" name="email" class="form-control" value="<?php echo $data['EmailAddress']; ?>">
                    </div>
                    <div class="col-sm-12"><br />
					<button type="submit" name="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#uploadingmodal" onclick="uploaddisablealert()"><i class="far fa-check-circle"></i>Update</button>
                    <button type="reset" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Reset</button>
                    <input type="hidden" name="ConfirmNewPassword" class="form-control" value="<?php echo $data['Password']; ?>">
					</div>
				</div>
            </div>
        </div>
		<div class="col-md-2">
		</div>
	</div>
</form>

<?php include 'inc/notificiations.php'; ?>

<script type="text/javascript">
//Notification if the form isnt saved before leaving the page
$(document).ready(function() {
    formmodified=0;
    $('form *').change(function(){
        formmodified=1;
    });
    window.onbeforeunload = confirmExit;
    function confirmExit() {
        if (formmodified == 1) {
            return "New information not saved. Do you wish to leave the page?";
        }
    }
});

function uploaddisablealert() {
  formmodified=0;
}

$("#ProfileThumb").click(function() {
    $("input[id='fileToUpload']").click();
});
</script>

    <!-- sweet alert js -->
    <script type="text/javascript" src="files/bower_components/sweetalert/js/sweetalert.min.js"></script>
    <!-- modalEffects js nifty modal window effects -->
    <script type="text/javascript" src="files/assets/js/modalEffects.js"></script>

<?php include "footer.php"; ?>