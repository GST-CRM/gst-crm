<?php include "session.php";
$PageTitle = "Add A Supplier Bill";
include "header.php";

include 'inc/bills-functions.php';

$WhereSupplier = "";
$SupplierSelectedID = mysqli_real_escape_string($db, $_POST['SupplierID']);
$SuppID = mysqli_real_escape_string($db, $_GET['SuppID']);
if ($SuppID != "" AND $SuppID > 0) {
	$WhereSupplier = "AND (LandPro.supplierid=".$SuppID." OR AirPro.supplierid=".$SuppID.")";
	$SupplierSelectedID = $SuppID;
}

?>
  <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />-->
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-block">
				<form name="contact5" action="" method="POST" class="row" id="contact5" enctype="multipart/form-data">
                    <div class="form-default form-static-label col-sm-4">
						<input type="hidden" id="submitBtn" name="submitBtn" value="" >
                        <label id="SupplierNameLabel" class="float-label">Supplier Company</label>
						<input value="NEW" name="NewBill" type="hidden">
						<select name="SupplierID" id="SupplierID" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple" onChange="doReload(this.value);">
						<option value="0">Select</option>
						<?php
							$SupplierResult = mysqli_query($db,"SELECT con.id,con.company FROM suppliers sup JOIN contacts con ON con.id=sup.contactid WHERE sup.status=1");
							while($SupplierData = mysqli_fetch_array($SupplierResult))
							{
								if($SupplierData['id'] == $SupplierSelectedID) { $SelectedSupplier = "selected";} else {$SelectedSupplier = ""; }
								echo "<option value='".$SupplierData['id']."' $SelectedSupplier>".$SupplierData['company']."</option>";
							}
						?>
						</select>
						<div class="invalid-feedback">
							Please choose a supplier.
						</div>
                    </div>
                    <div class="form-default form-static-label col-sm-5">
                        <label class="float-label gst-label">Group Reference</label>
						<select name="GroupID" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple" onchange="GroupBillSelected(this)">
						<option value="0">Select</option>
						<?php
							$GroupResult = mysqli_query($db,"SELECT gro.tourid,gro.tourname,gro.enddate FROM groups gro
															LEFT JOIN products AirPro
															ON AirPro.id = gro.airline_productid
															LEFT JOIN products LandPro
															ON LandPro.id = gro.land_productid
															WHERE gro.status=1 $WhereSupplier
															ORDER BY gro.startdate DESC");
							while($GroupData = mysqli_fetch_array($GroupResult))
							{
								if($GroupData['tourid'] == $_POST['GroupID']) { $SelectedGroup = "selected";} else {$SelectedGroup = ""; }
								$GroupEndDate = $GroupData['enddate'];
								$GroupDueDate = date('Y-m-d',strtotime("-30 days",strtotime($GroupEndDate)));
								echo "<option value='".$GroupData['tourid']."' data-duedate='".$GroupDueDate."' $SelectedGroup>".$GroupData['tourname']."</option>";
							}
						?>
						</select>
                    </div>
                    <div class="form-default form-static-label col-sm-1">
                    </div>
                    <div class="form-default form-static-label col-sm-2 text-center">
                        <h5>Amount</h5>
                        <h3 class="text-danger" id="SupplierBillTotal">$0.00</h3>
						<input name="BillTotal" type="hidden" value="" class="HiddenTotal">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Bill Date</label>
                        <input type="date" name="Bill_Date" value="<?php if($_POST['Bill_Date']==""){ echo date("Y-m-d"); } else { echo $_POST['Bill_Date']; } ?>" class="form-control date-picker">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label" >Due Date</label>
                        <input type="date" id="Due_Date" name="Due_Date" value="<?php echo $_POST['Due_Date']; ?>" class="form-control">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
					<style>.select2-selection {height:35px;padding:0px !important;}</style>
                        <label class="float-label gst-label">Terms</label>
                        <select name="Terms" class="form-control">
                            <option value="">Select</option>
                                <option value="Bank Transfer" <?php if($_POST['Terms'] == "Bank Transfer") { echo "selected";} ?>>Net 7</option>
                                <option value="Cash" <?php if($_POST['Terms'] == "Cash") { echo "selected";} ?>>Net 30</option>
                                <option value="Check" <?php if($_POST['Terms'] == "Check") { echo "selected";} ?>>Net 90</option>
                                <option value="Card" <?php if($_POST['Terms'] == "Card") { echo "selected";} ?>>Cash on Delivery</option>
                        </select>
                    </div>
                    <div class="col-sm-12"><br /></div>
                    <div class="col-sm-12 table-responsive">
					<h4 class="sub-title">Supplier Bill Details</h4>
						<table class="table table-hover table-striped table-bordered nowrap" id="item_table">
							<thead>
								<tr>
									<th width="30" class="text-center">#</th>
									<th width="20%">Product / Service</th>
									<th>Description</th>
									<th width="100">Quantity</th>
									<th width="150">Amount</th>
									<th width="150">Total</th>
									<th width="50"><button type="button" name="add" class="btn btn-success btn-sm add"><i class="fas fa-plus mr-0"></i></button></th>
								</tr>
							</thead>
							<?php
							//To make the list of products in PHP Variable
							$ProductSelect = '';
							$ProductSelect .= '<option value="0">Select a Product</option>';
							$SupplierID = $data['contactid'];
							$ProductsListResult = mysqli_query($db,"SELECT * FROM products WHERE status=1");
							while($SupplierProducts = mysqli_fetch_array($ProductsListResult)) {
								$ProductName = str_replace("'"," ",$SupplierProducts['name']);
								$ProductDescription = str_replace("'"," ",$SupplierProducts['description']);
								$ProductSelect .='<option value="'.$SupplierProducts["id"].'" data-description="' . $ProductDescription . '" data-cost="' . $SupplierProducts['cost'] . '" data-quantity="0" data-category="' . $SupplierProducts['categoryid'] . '" data-name="' .$ProductName. '">'.$ProductName.'</option>';
							} ?>
							<tfoot>
								<tr>
									<th colspan="5" class="pt-2 pb-2 text-right">Total</th>
									<th colspan="2" class="pt-2 pb-2 FooterTotal">$0.00</th>
								</tr>
							</tfoot>
						</table>
					</div>
                    <div class="col-sm-12"><br /></div>
					<div class="form-group form-default form-static-label col-md-4">
                        <label class="float-label">Supplier Bill Comments</label>
                        <textarea name="Bill_Comments" class="form-control" rows="6"><?php echo $_POST["Bill_Comments"]; ?></textarea>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label gst-label" for="SupplierBill_File">Attachment</label>
                        <div class="Attachment_Box">
                            <input <?php echo $DisabledFlag; ?> type="file" id="SupplierBill_File" name="fileToUpload" class="Attachment_Input" >
                            <p id="SupplierBill_File_Text" class="Attachment_Text">Drag the file here or click in this area.</p>
                        </div>
                    </div>
                    <div class="col-sm-12"><br /><br />
						<button type="submit" name="addcontact" value="submit" class="btn waves-effect waves-light btn-success mr-1 mt-2" data-toggle="modal" data-target="#resultsmodal" data-val="save_new"><i class="far fa-check-circle"></i>Save & New</button>
						<button type="submit" name="addcontact" value="submit" class="btn waves-effect waves-light btn-info mr-1 mt-2" data-toggle="modal" data-target="#resultsmodal" data-val="save_close"><i class="far fa-check-circle"></i>Save & Close</button>
                        <button type="reset" class="btn waves-effect waves-light btn-inverse mt-2"><i class="fas fa-ban"></i>Clear</button>
						<!-- <button type="submit" name="submit" value="save_new" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Save & New</button>
						<button type="submit" name="submit" value="save_close" class="btn waves-effect waves-light btn-info" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Save & Close</button>
                        <button type="reset" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Clear</button> -->
					</div>
				</form>
			</div>
        </div>
    </div>
</div>
<script type="text/javascript">
//To make the numbers as a currency with two decimels
const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 2
})

//This is to make the attachment box update its content when a file is selected
$(document).ready(function(){
  $('#SupplierBill_File').change(function () {
    $('#SupplierBill_File_Text').text("A file has been selected");
  });
});

//This is to get the details of each product and show it in that line
function BillLineSelected(obj) {
	var val = $(obj).val();
	var description = $(obj).find("option[value='" + val + "']").data('description');
	var cost = $(obj).find("option[value='" + val + "']").data('cost');
	var quantity = $(obj).find("option[value='" + val + "']").data('quantity');
	var name = $(obj).find("option[value='" + val + "']").data('name');
	var category = $(obj).find("option[value='" + val + "']").data('category');
	$(obj).closest('.bill-line-tr').find('.product_desc').html(description);
	$(obj).closest('.bill-line-tr').find('.product_cost').html(formatMoney(cost));
	$(obj).closest('.bill-line-tr').find('.product_cost').val(cost);
	$(obj).closest('.bill-line-tr').find('.product_name').val(name);
	$(obj).closest('.bill-line-tr').find('.product_category').val(category);
	$(obj).closest('.bill-line-tr').find('.bill_line_quantity').val(quantity);
	var BillLineTotal = quantity * cost;
	$(obj).closest('.bill-line-tr').find('.bill_line_total').html(formatMoney(BillLineTotal));
	$(sel).closest('.bill-line-tr').find('.bill_line_total').data('cost', BillLineTotal);

	Bill_Line_Qty_Change($(obj).closest('.bill-line-tr').find('.bill_line_quantity'));
}

//This is to get the due date minus 30 days for the supplier bill if they choose a group
function GroupBillSelected(obj) {
	var val = $(obj).val();
	var duedate = $(obj).find("option[value='" + val + "']");

	if (duedate.length > 0) {
	  var id = duedate.data("duedate");
	  document.getElementById("Due_Date").value = id;
	}
}

function Bill_Line_Qty_Change(textobj) {
	var qty = $(textobj).val();
	var sel = $(textobj).closest('.bill-line-tr').find('.Bill_Line_Product');
	var val = $(sel).val();

	var cost = $(sel).find("option[value='" + val + "']").data('cost');
	$(sel).closest('.bill-line-tr').find('.product_cost').html(formatMoney(cost));
	var TotalLineCost = cost * qty;

	$(sel).closest('.bill-line-tr').find('.bill_line_total').html(formatMoney(TotalLineCost));
	$(sel).closest('.bill-line-tr').find('.bill_line_total').data('cost', TotalLineCost);

	updateTotal();
}

function updateTotal() {
	var total = 0;

	$('.bill-line-tr .bill_line_total').each(function () {
		total += parseFloat($(this).data('cost'));
	});
	var PaidSoFar = 0;
	var FinalAmount = total - PaidSoFar;
	$('#SupplierBillTotal').html(formatMoney(FinalAmount));
	$('.FooterTotal').html(formatMoney(total));
	$('.HiddenTotal').val(total);

}

//To be able to add new rows or remove old ones
$(document).ready(function(){
	var TDnum = 1;
	$(document).on('click', '.add', function(){
	if (document.getElementById('SupplierID').options.length == 0) 
		alert('The selectbox contains 0 items');
		if ($("#SupplierID option").is(":selected")) {
		var html = '';
		html += '<tr class="bill-line-tr">';
		html += '<td class="text-center">'+ TDnum +'<input type="hidden" name="exp_id[]" class="form-control exp_id" value="'+ TDnum +'" /></td>';
		html += '<td><select name="Bill_Product[]" class="form-control Bill_Line_Product" onchange="BillLineSelected(this)">';
		html += '<?php echo $ProductSelect; ?>';
		html += '</select></td>';
		html += '<td><span class="product_desc"></span></td>';
		html += '<td><input type="number" onchange="Bill_Line_Qty_Change(this)" name="Bill_Line_Quantity[]" class="form-control bill_line_quantity" value="" /></td>';
		html += '<td><span class="product_cost"></td>';
		html += '<td><span class="bill_line_total"></span></td>';
		html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm remove"><i class="fas fa-trash-alt mr-0"></i></button></td>';
		html += '<td style="display:none;"><input type="text" class="product_name" name="product_name[]" value="">';
		html += '<input type="text" class="product_cost" name="product_cost[]" value="">';
		html += '<input type="text" class="product_category" name="product_category[]" value="">';
		html += '</td></tr>';
		$('#item_table').append(html);
		TDnum++;
		
			$(".invalid-feedback").css("display", "none");
			
		} else {
			$(".invalid-feedback").css("display", "block");
			var element = document.getElementById("SupplierNameLabel");
			element.classList.add("text-danger");
		}
	});

	$(document).on('click', '.remove', function(){
		$(this).closest('tr').remove();
	});

    $('.btn').click(function() {
          var buttonval    = $(this).attr('data-val');
          $("#submitBtn").val(buttonval);
    }) 
});

function doReload(SuppID){
	document.location = 'bills-add.php?SuppID=' + SuppID;
}
</script>
<?php include "footer.php"; ?>