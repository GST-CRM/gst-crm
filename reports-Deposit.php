<?php
include "session.php";
$PageTitle = "Customers Cash & Checks Deposits Report";
include "header.php"; 

// Get the values from the main page first
if(isset($_GET['CustomerSelected'])) {$CustomerSelected = mysqli_real_escape_string($db, $_GET['CustomerSelected']);}
if(isset($_GET['GroupSelected'])) {$GroupSelected = mysqli_real_escape_string($db, $_GET['GroupSelected']);}
if(isset($_GET['FromDate'])) {$FromDateFilter = mysqli_real_escape_string($db, $_GET['FromDate']);}
if(isset($_GET['UntilDate'])) {$UntilDateFilter = mysqli_real_escape_string($db, $_GET['UntilDate']);}
if(isset($_GET['type'])) {$type = mysqli_real_escape_string($db, $_GET['type']);}

// Show the Table with the results if the filter is active
if (!empty($CustomerSelected) OR !empty($GroupSelected) OR !empty($FromDateFilter) OR !empty($UntilDateFilter) OR !empty($type)) {
	$ShowTheResults = true;
} else {
	$ShowTheResults = false;
}

?>
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {background:white;color:#333;padding:5px 30px 4px 20px;border-radius:4px;}
.select2-container--default .select2-selection--single .select2-selection__arrow {top:6px;}
.select2-container--default .select2-selection--single .select2-selection__arrow b {border-color:black transparent transparent transparent;}
div.dt-buttons { float:right !important; }
div.dataTables_wrapper div.dataTables_filter { text-align:left !important; }
</style>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
		<div class="card">
            <form class="row mb-3" action="reports-Deposit.php" method="GET">
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;display:none;">
						<label class="float-label" for="cbBIRTH">Search for a specific Customer</label>
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
					</div>
				</div>
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
						<label class="float-label" for="SpecificCustomer">Search for a specific Customer</label>
						<select id="SpecificCustomer" name="CustomerSelected" class="col-sm-12">
							<?php
							//To show the selected customer if it was selected in the GET variable
							if(!empty($CustomerSelected)) { 
								$CustomerzData = explode(':', $CustomerSelected);
								$CustomerzDataID = $CustomerzData[0];
								$CustomerzDataName = $CustomerzData[1];
								echo "<option value='".$CustomerzDataID.":".$CustomerzDataName."' selected>".$CustomerzDataName."</option>";
							} else { 
								$CustomerzDataID = 0;
								echo "<option value='0'>-</option>";
							}
							?>
						</select>
				</div>
				<div class="col-md-3" style="margin-top:15px;">
						<label class="float-label" for="SpecificGroup">Search for a specific Group</label>
						<select id="SpecificGroup" name="GroupSelected" class="col-sm-12">
							<option value='0'>-</option>
							<?php
							//To show the selected customer if it was selected in the GET variable
							if(!empty($GroupSelected)) { 
								$GroupzData = explode(':', $GroupSelected);
								$GroupzDataID = $GroupzData[0];
								$GroupzDataName = $GroupzData[1];
								echo "<option value='".$GroupzDataID.":".$GroupzDataName."' selected>".$GroupzDataName."</option>";
							} else { 
								$GroupzDataID = 0;
								echo "<option value='0'>-</option>";
							}
							?>
						</select>
				</div>
				<div class="col-md-6" style="margin-top:15px;">
					<div class="col-12 row">
						<div class="form-group form-default form-static-label col-md-3">
							<label class="float-label" for="FromDate">From</label>
							<input class="form-control" id="FromDate" type="date" name="FromDate" value="<?php if(!empty($FromDateFilter)) {echo $FromDateFilter;} ?>">
						</div>
						<div class="form-group form-default form-static-label col-md-3">
							<label class="float-label" for="UntilDate">Until</label>
							<input class="form-control" id="UntilDate" type="date" name="UntilDate" value="<?php if(!empty($UntilDateFilter)) {echo $UntilDateFilter;} ?>">
						</div>
						<div class="form-group form-default form-static-label col-md-2">
							<label class="float-label" for="type">Type</label>
							<select id="type" name="type" class="form-control form-control-default fill">
								<option value="all" <?php if(!empty($type) AND $type == "all") {echo "selected";} ?>>All</option>
								<option value="Checks" <?php if(!empty($type) AND $type == "Checks") {echo "selected";} ?>>Checks</option>
								<option value="Cash" <?php if(!empty($type) AND $type == "Cash") {echo "selected";} ?>>Cash</option>
							</select>
						</div>
						<div class="form-group form-default form-static-label col-md-1">
							<label class="float-label" for="ClearFilterOptions">Clear</label>
							<button id="ClearFilterOptions" type="button" class="btn btn-inverse btn-block waves-effect waves-light" style="width:100%; padding: 5px 2px;"><i class="fas fa-eraser"></i></button>
						</div>
						<div class="form-group form-default form-static-label col-md-3 pr-0">
							<hr style="border: 0;height: 1px;background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0));margin-top: 14px;margin-bottom: 13px;" />
							<button type="submit" class="btn btn-info btn-block waves-effect waves-light" style="padding: 5px 13px;">Filter</button>
						</div>
					</div>
				</div>
			</form>
<?php if($ShowTheResults === true) { ?>
            <div class="card-block">
				<div class="dt-responsive table-responsive">
                    <table id="customers_list" class="table table-hover table-striped table-bordered responsive nowrap" style="width:100%;" data-page-length="20">
                        <thead>
                            <tr>
                                <th>Payment Date</th>
                                <th>Full Name</th>
                                <th>Group Reference</th>
                                <th>Payment Method</th>
                                <th>Payment Amount</th>
                                <th width="30" class="text-center">Deposited?</th>
                            </tr>
                        </thead>
						<tbody>
<?php 
//Advanced Filters for the Deposit Report List Page
$DepositFilters = "";

//The Type of the payment: Check or Cash
if($type == "all") {
	$DepositFilters.= "";
}elseif($type == "Checks") {
	$DepositFilters.= " AND cp.Customer_Payment_Method='Check' ";
}elseif($type == "Cash") {
	$DepositFilters.= " AND cp.Customer_Payment_Method='Cash' ";
}

//The from date and the until date filters
if($FromDateFilter != 0) {
	$DepositFilters.= " AND cp.Customer_Payment_Date >= '$FromDateFilter' ";
}
if($UntilDateFilter != 0) {
	$DepositFilters.= " AND cp.Customer_Payment_Date <= '$UntilDateFilter' ";
}

//The customer ID filter
if($CustomerzDataID > 0) {
	$DepositFilters.= " AND co.id = $CustomerzDataID ";
}

//The Group ID filter
if($GroupzDataID > 0) {
	$DepositFilters.= " AND gr.tourid = $GroupzDataID ";
}

//The deposit get query
$GetDeposits = "SELECT
	cp.Customer_Payment_ID,
	cp.Customer_Payment_Date AS Old_Customer_Payment_Date,
	DATE_FORMAT(cp.Customer_Payment_Date, '%m/%d/%Y') AS Customer_Payment_Date,
    co.id,co.title,co.fname,co.mname,co.lname,
    if(cp.Customer_Payment_Method='Check',cp.Customer_Payment_Comments,cp.Customer_Payment_Method) AS Check_Cash,
    cp.Customer_Payment_Amount,
    gr.tourid,
    gr.tourdesc
FROM customer_payments cp 
JOIN contacts co 
ON cp.Customer_Account_Customer_ID=co.id
JOIN customer_invoice ci 
ON ci.Customer_Invoice_Num=cp.Customer_Invoice_Num
JOIN groups gr 
ON gr.tourid=ci.Group_ID
WHERE (cp.Customer_Payment_Method='Check' OR cp.Customer_Payment_Method='Cash')
$DepositFilters
ORDER BY Old_Customer_Payment_Date DESC";
$DepositRecords = GDb::fetchRowSet($GetDeposits);
$TotalPaymentsRecorded = 0;
foreach ($DepositRecords AS $Deposit) {
?>
<tr>
	<td><?php echo $Deposit['Customer_Payment_Date']; ?></td>
	<td><?php echo "<a href='contacts-edit.php?id=".$Deposit['id']."&action=edit&usertype=customer' style='color: #0000EE;'>".$Deposit['title']." ".$Deposit['fname']." ".$Deposit['mname']." ".$Deposit['lname']."</a>"; ?></td>
	<td><?php echo "<a href='groups-edit.php?id=".$Deposit['tourid']."&action=edit&tab=participants' style='color: #0000EE;'>".$Deposit['tourdesc']."</a>"; ?></td>
	<td><img src="<?php if($Deposit['Check_Cash'] == "Cash") { echo $crm_images."/cash-money-icon.png"; } else { echo $crm_images."/check-money-icon.png"; } ?>" alt="Payment Method" width="25" /> <?php echo $Deposit['Check_Cash']; ?></td>
	<td><?php echo GUtils::formatMoney($Deposit['Customer_Payment_Amount']); ?></td>
	<td class="text-center">
		<div class="checkbox-fade fade-in-success pt-2 m-0">
			<label class="m-0">
				<input id="PassportPending" name="PassportPending" type="checkbox" value="1">
				<span class="cr m-0">
					<i class="cr-icon fas fa-check txt-success"></i>
				</span>
			</label>
		</div>
	</td>
</tr>
<?php $TotalPaymentsRecorded += $Deposit['Customer_Payment_Amount']; } ?>
						</tbody>
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th style="text-align:right">Total:</th>
								<th><?php echo GUtils::formatMoney($TotalPaymentsRecorded); ?></th>
								<th></th>
							</tr>
						</tfoot>
					</table>
                </div>
            </div>
<?php } ?>
        </div>
	</div>
</div>

<script>
//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#customers_list').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
});

const formatter = new Intl.NumberFormat('en-US', {
	style: 'currency',
	currency: 'USD',
	minimumFractionDigits: 2
})


//To customize the datatables function
$( document ).ready(function() {
	$('#customers_list').DataTable({
		"lengthMenu": [ [10,20,50,100,1000,-1], [10,20,50,100,1000,"All"] ],
		//"order": [[ 1, "desc" ]],
		"ordering": false,
		dom: 'Bfrtip',
		buttons: ['csv',{
                    extend: 'excel',
                    footer: true,
                    title: 'Report',
                    filename: 'Report',
                }],
		"language": {
			"emptyTable": "There are no deposits available."
		},
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;
			// Remove the formatting to get integer data for summation
			var intVal = function ( i ) {
				return typeof i === 'string' ?
				i.replace(/[\$,]/g, '')*1 :
				typeof i === 'number' ?
				i : 0;
			};

			// Total over all pages
            total = api
                .column( 4 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 4, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 4 ).footer() ).html(
                formatter.format(pageTotal) +'<br /><small>(Total Records '+ formatter.format(total) +')</small>' 
            );
        },
		columnDefs: [
			{ width: 110, targets: 0 },
			{ width: 300, targets: 1 },
			{ width: 200, targets: 3 },
			{ width: 150, targets: 4 }
		]
	});   
});

//To create select2 function for the customers dropdown
$(document).ready(function(){
   $("#SpecificCustomer").select2({
      ajax: {
        url: "inc/select2-customers.php",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
           return {
              searchTerm: params.term
           };
        },
        processResults: function (response) {
           return {
              results: response
           };
        },
        cache: true
      }
   });
});

//To create select2 function for the groups dropdown
$(document).ready(function(){
   $("#SpecificGroup").select2({
      ajax: {
        url: "inc/select2-groups.php",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
           return {
              searchTerm: params.term
           };
        },
        processResults: function (response) {
           return {
              results: response
           };
        },
        cache: true
      }
   });
});

$('#ClearFilterOptions').click(function(){
	document.getElementById("FromDate").value = "0000-00-00";
	document.getElementById("UntilDate").value = "0000-00-00";
	$('#type').val('all');
	//$("#SpecificGroup").empty();
	$("#SpecificGroup").select2("val", "0");
	$("#SpecificCustomer").select2("val", "0");
});
</script>
<?php include "footer.php"; ?>