<?php
include "session.php";
$PageTitle = "Sales Order List";
include "header.php"; 

include_once __DIR__ . '/models/acl_permission.php';


//To get the URL parameters needed
if(isset($_GET['action'])) {$action = mysqli_real_escape_string($db, $_GET['action']);} else { $action = ""; }
if(isset($_GET['SalesID'])) {$SalesID = mysqli_real_escape_string($db, $_GET['SalesID']);}
if(isset($_GET['CustomerID'])) {$CustomerID = mysqli_real_escape_string($db, $_GET['CustomerID']);}
if(isset($_GET['status'])) {$Status = mysqli_real_escape_string($db, $_GET['status']);} else { $Status = ""; }
if(isset($_GET['FromDate'])) {$FromDateFilter = mysqli_real_escape_string($db, $_GET['FromDate']);}
if(isset($_GET['UntilDate'])) {$UntilDateFilter = mysqli_real_escape_string($db, $_GET['UntilDate']);}
if(isset($_GET['ShowGroups'])) {$ShowGroups = mysqli_real_escape_string($db, $_GET['ShowGroups']);}

$saledEdit = (new AclPermission())->isActionAllowed('Index', 'sales-edit.php')  ;
$contactEdit = (new AclPermission())->isActionAllowed('Index', 'contact-edit.php') ;


//To show the Void Message
if ($action == "void") {
	$SOquery = "UPDATE sales_order SET Sales_Order_Stop=1 WHERE Sales_Order_Num='$SalesID';";
	$SOquery .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','Sales Order','$SalesID','voided','');";
	if ($db->multi_query($SOquery) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected sales order record has been voided successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error voiding record: " . $db->error . "</strong></div>";
	}
}

//To show the Un-Void Message
if ($action == "unvoid") {
	$SOquery = "UPDATE sales_order SET Sales_Order_Stop=0 WHERE Sales_Order_Num='$SalesID';";
	$SOquery .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','Sales Order','$SalesID','unvoided','');";
	if ($db->multi_query($SOquery) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected sales order record has been un-voided successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error un-voiding record: " . $db->error . "</strong></div>";
	}
}

//To Create an invoice from this sales order
if ($action == "CreateInvoice") {
	//Copy the data of the sales order to a new invoice record
	$CreateInvoiceSQL = "INSERT INTO `customer_invoice`(`Customer_Invoice_Date`, `Customer_Order_Num`, `Group_ID`, `Invoice_Amount`, `Sales_Order_Reference`, `Customer_Account_Customer_ID`, `Transaction_Type_Transaction_ID`, `Due_Date`)
						SELECT
							Sales_Order_Date AS Customer_Invoice_Date,
							Sales_Order_Num AS Customer_Order_Num,
							'999' AS Group_ID,
							Total_Sale_Price AS Invoice_Amount,
							Sales_Order_Num AS Sales_Order_Reference,
							Customer_Account_Customer_ID,
							'0' AS Transaction_Type_Transaction_ID,
							Sales_Order_Due_Date AS Due_Date
						FROM `sales_order`
						WHERE Sales_Order_Num = $SalesID;";
	GDb::execute($CreateInvoiceSQL);
	$NewInvoiceID = GDb::db()->insert_id;
	
	//Set the counter to count the total rows will be added to the invoice lines
	$SetCounter = "SET @Counter = 0;";
	GDb::execute($SetCounter);
	
	//Copy the data of the sales order lines to the new invoice lines
	$CreateInvoiceLines = "INSERT INTO `customer_invoice_line`(`Customer_Invoice_Num`, `Customer_Invoice_Line`, `Line_Type`, `Product_Product_ID`, `Product_Name`, `Qty`, `Invoice_Line_Total`)
						SELECT
							'$NewInvoiceID' AS Customer_Invoice_Num,
							@Counter:=@Counter+1 AS Customer_Invoice_Line,
							if(Line_Type='Airline Package',1,if(Line_Type='Land Package',2,if(Line_Type='Ticket Discount',3,if(Line_Type='Single Supplement',4,if(Line_Type='Air Upgrade',5,if(Line_Type='Land Upgrade',6,if(Line_Type='Extension',8,if(Line_Type='Transfer',9,if(Line_Type='Insurance',10,0))))))))) AS Line_Type,
							Product_Product_ID,
							Product_Name,
							Sales_Quantity AS Qty,
							Sales_Amount AS Invoice_Line_Total
						FROM `sales_order_line` WHERE Sales_Order_Num = $SalesID;";
	GDb::execute($CreateInvoiceLines);
	
	//Record this process in the logs archive
	$SOquery = "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','Sales Order','$SalesID','convereted to invoice','$NewInvoiceID');";
	if ($db->multi_query($SOquery) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected sales order record has been converted to an invoice successfully. You will be redirected to the invoice shortly.</strong></div>";
		echo "<meta http-equiv='refresh' content='2;url=contacts-edit.php?invid=$NewInvoiceID&id=$CustomerID&action=edit&usertype=sales'>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error creative invoice record: Do not refersh the page! " . $db->error . "</strong></div>";
	}
}

?>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
		<div class="card">
            <form class="row" action="sales.php" method="GET">
				<div class="col-md-2" style="margin-top:15px;padding-left:45px;">
					<label class="float-label" for="searchbox">Custom Search</label>
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input type="text" id="searchbox" class="outsideBorderSearch form-control" placeholder="Search...">
					</div>
				</div>
				<div class="col-md-2" style="margin-top:15px;">
					<label class="float-label" for="FromDate">From Date</label>
					<input class="form-control" id="FromDate" type="date" name="FromDate" value="<?php if(!empty($FromDateFilter)) {echo $FromDateFilter;} ?>">
				</div>
				<div class="col-md-2" style="margin-top:15px;">
					<label class="float-label" for="UntilDate">Until Date</label>
					<input class="form-control" id="UntilDate" type="date" name="UntilDate" value="<?php if(!empty($UntilDateFilter)) {echo $UntilDateFilter;} ?>">
				</div>
				<div class="col-md-1" style="margin-top:15px;padding-right:20px;">
					<label class="float-label" for="ShowGroups">Type</label>
					<select id="ShowGroups" name="ShowGroups" class="form-control form-control-default fill">
						<option disabled value="all" <?php if(empty($ShowGroups)) {echo "selected";} ?>></option>
						<?php if($ShowGroups === "") { $ShowGroups = "show"; } ?>
						<option value="show" <?php if(!empty($ShowGroups) AND $ShowGroups == "show") {echo "selected";} ?>>Show group sales orders</option>
						<option value="hide" <?php if(!empty($ShowGroups) AND $ShowGroups == "hide") {echo "selected";} ?>>Hide group sales orders</option>
					</select>
				</div>
				<div class="col-md-1" style="margin-top:15px;padding-right:20px;">
					<label class="float-label" for="status">Status</label>
					<select id="status" name="status" class="form-control form-control-default fill">
						<option disabled value="all" <?php if(empty($Status)) {echo "selected";} ?>></option>
						<?php if($Status === "") { $Status = "all"; } ?>
						<option value="active" <?php if(!empty($Status) AND $Status == "active") {echo "selected";} ?>>Active Orders</option>
						<option value="voided" <?php if(!empty($Status) AND $Status == "voided") {echo "selected";} ?>>Voided Orders</option>
					</select>
				</div>
				<div class="col-md-1" style="margin-top:15px;">
					<label class="float-label">Show Results</label>
					<button type="submit" class="btn btn-info btn-block waves-effect waves-light" style="padding: 5px 13px;">Filter</button>
				</div>
				<div class="col-md-1" style="margin-top:15px;">
					<label class="float-label">Download</label><br />
					<a href="excel.php?type=sales&status=<?php echo !empty($Status) ? $Status : 'all';?>&FromDate=<?php echo !empty($FromDateFilter) ? $FromDateFilter : '0000-00-00';?>&UntilDate=<?php echo !empty($UntilDateFilter) ? $UntilDateFilter : '0000-00-00';?>&ShowGroups=<?php echo !empty($ShowGroups) ? $ShowGroups : 'show';?>" class="btn waves-effect float-right waves-light btn-inverse btn-block" style="padding: 5px 13px;" download><i class="fas fa-download mr-0"></i> Excel</a>
				</div>
                <?php if( $saledEdit ) {?>
				<div class="col-md-2" style="margin-top:15px;padding-right:45px;">
					<hr style="border: 0;height: 1px;background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0));margin-top: 14px;margin-bottom: 13px;" />
					<a href="sales-add.php" class="btn btn-block waves-effect float-right waves-light btn-success" style="padding: 5px 13px;"><i class="fas fa-plus-circle"></i>Add Sales Order</a>
				</div>
                <?php } ?>
			</form>
			<div class="row">
				<div class="col-md-11" style="margin-top:15px;padding-left:45px;"></div>
				<div class="col-md-1" style="margin-top:15px;padding-right:45px;">
					
				</div>
			</div>
            <div class="card-block">
				<div class="dt-responsive table-responsive">
                    <table id="sales_list" class="table table-hover table-striped table-bordered nowrap" style="width:100%;">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Sale Order Date</th>
                                <th>Customer Name</th>
                                <th>Has Invoice?</th>
                                <th>Sale Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<!-- [ page content ] end -->
<script>
    <?php 
    if( $saledEdit ) {
    
        echo 'var SALES_EDIT=true;' ;
    }
    else {
        echo 'var SALES_EDIT=false;' ;
    }
    if( $contactEdit ) {
        echo 'var CONTACT_EDIT=true;' ;
    }
    else {
        echo 'var CONTACT_EDIT=false;' ;
    }
    ?>
//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#sales_list').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
});

function doReload(status){
	document.location = 'sales.php?status=' + status;
}

$( document ).ready(function() {
	$('#sales_list').DataTable({
		 "bProcessing": true,
         "serverSide": true,
		 pageLength: 20,
		 dom: 'frtip',
		 sDom: 'rtip', //To Hide the search box
		 "order": [[ 1, "desc" ],[ 0, "desc" ]],
		 "columnDefs": [
			  {"targets": 0,
				 "render": function(data, type, row, meta){
					if(type === 'display'){
						if(row[10] > 0) {
                            if( ! SALES_EDIT ) { data = row[0] ; }
                            else {
                                data = '<a href="sales-edit.php?id=' + row[0] + '&action=edit&group=1" style="color: #0000EE;">'+ row[0] +'</a>';
                            }
						} else {
                            if( ! SALES_EDIT ) { data = row[0] ; }
                            else {
                                data = '<a href="sales-edit.php?id=' + row[0] + '&action=edit&group=0" style="color: #0000EE;">'+ row[0] +'</a>';
                            }
						}
					}
					return data;
				 }
			  },
			  {"targets": 2,
				 "render": function(data, type, row, meta){
					if(type === 'display'){
                        if( ! CONTACT_EDIT) { data = row[2] ; }
                        else {
                            data = '<a href="contacts-edit.php?id=' + row[2] + '&action=edit&usertype=customer" style="color: #0000EE;">' + row[4] + ' ' + row[5] + ' ' + row[6] + '</a>';
                        }
					}
					return data;
				 }
			  },
			  {"targets": 3,
				 "render": function(data, type, row, meta){
					if(type === 'display'){
						if(row[10] > 0) {
                            if( ! SALES_EDIT ) { data = row[0] ; }
                            else {
                                data = '<a href="sales-edit.php?id=' + row[0] + '&action=edit&group=1" style="color: #0000EE;">'+ row[3] +'</a>';
                            }
						} else {
                            if( ! SALES_EDIT ) { data = row[0] ; }
                            else {
                                data = '<a href="sales-edit.php?id=' + row[0] + '&action=edit&group=0" style="color: #0000EE;">'+ row[3] +'</a>';
                            }
						}
					}
					return data;
				 }
			  },
			  {"targets": 4,
				 "render": function(data, type, row, meta){
					if(type === 'display'){
						if(row[10] > 0) {
                            if( ! SALES_EDIT ) { data = row[0] ; }
                            else {
                                data = '<a href="sales-edit.php?id=' + row[0] + '&action=edit&group=1" style="color: #0000EE;">$' + row[7] +'</a>';
                            }
						} else {
                            if( ! SALES_EDIT ) { data = row[0] ; }
                            else {
                                data = '<a href="sales-edit.php?id=' + row[0] + '&action=edit&group=0" style="color: #0000EE;">$' + row[7] +'</a>';
                            }
						}
					}
					return data;
				 }
			  },
			  {"targets": 5,
				 "render": function(data, type, row, meta){
					if(type === 'display'){
						if(row[10] > 0) {
								data = '<a class="btn btn-primary btn-sm" href="sales-edit.php?id=' + row[0] + '&action=edit&group=1" role="button">View</a>';
						} else {
							if(row[9] == 1) {
								data = '<a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick = \'deleteConfirm( "sales.php?action=unvoid&SalesID=' + row[0] +'", "Unvoid")\' role="button">Unvoid</a>';
							} else { 
								data = '<div class="btn-group"><a class="btn btn-primary btn-sm" href="sales-edit.php?id=' + row[0] + '&action=edit" role="button" id="dropdownMenuLink">Edit</a><button type="button" class="btn btn-sm btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="sr-only">Toggle Dropdown</span></button><div class="dropdown-menu"><a class="dropdown-item" href="sales-edit.php?id=' + row[0] + '&action=edit" >Edit Record</a><a class="dropdown-item" href="javascript:void(0);" onclick = \'showConfirm( "sales.php?action=CreateInvoice&SalesID=' + row[0] + '&CustomerID=' + row[2] + '", "Create Invoice", "An invoice will be created for this sales order.", "Please Confirm")\'>Create invoice</a><a class="dropdown-item" href="javascript:void(0);" onclick = \'deleteConfirm( "sales.php?action=void&SalesID=' + row[0] + '", "Void")\'>Void Record</a></div></div>';
							}
						}
					}
					return data;
				 }
                <?php
                if( ! $saledEdit ) { 
                    echo ', "visible": false' ;
                }
                ?>

			  }
		   ],
		 //buttons: ['csv','excel'],
         "ajax":{
            //url :"inc/sales-order-list.php?status=<?php if($Status == '') { echo 'all'; } else { echo $Status; } ?>", // json datasource
            url :"inc/sales-order-list.php?status=<?php echo !empty($Status) ? $Status : 'all';?>&FromDate=<?php echo !empty($FromDateFilter) ? $FromDateFilter : '0000-00-00';?>&UntilDate=<?php echo !empty($UntilDateFilter) ? $UntilDateFilter : '0000-00-00';?>&ShowGroups=<?php echo !empty($ShowGroups) ? $ShowGroups : 'show';?>", // json datasource
            type: "GET",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#sales_list_processing").css("display","none");
            }
          }
        });   
});
</script>
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>