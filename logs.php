<?php
include "session.php";
$PageTitle = "CRM Logs";
include "header.php";


$RecordsPerPage = 100;
if(isset($_GET['page'])) {$CurrentPageNum = mysqli_real_escape_string($db, $_GET['page']);} else { $CurrentPageNum = 1; }
$RecordsOffset = ($CurrentPageNum - 1) * $RecordsPerPage;



?>
<!-- [ page content ] start -->
	<div class="row">
		<div class="col-sm-1">
		</div>
		<div class="col-sm-10">
			<div class="card exp-build-card">
				<div class="card-header">
				<h5>GST CRM Software Logs</h5>
				</div>
				<div class="card-block">
					<ul class="basic-list">
					<?php $sql = "select * from logs ORDER BY id DESC limit $RecordsOffset, $RecordsPerPage"; $logs = $db->query($sql);
					if ($logs->num_rows > 0) { while($row = $logs->fetch_assoc()) {
						
						$Username = $row["username"];
						$LogTime = date('m/d/Y - h:ia', strtotime($row["time"]));
						$LogAction = $row["action"];
						$LogType = $row["usertype"];
						$LogID = $row["contactid"];
						$LogDesc = $row["text"];
						
						if($LogType == "group note") {
							$LogMessage = $LogTime." | The user ".$Username." has <span style='font-weight:bold;'>".$LogAction."</span> a <span style='font-weight:bold;'>".$LogType."</span> for the group <span style='font-weight:bold;'><a style='font-size:13px;' href='groups-edit.php?id=".$LogID."&action=edit&tab=notes'>".$LogDesc."</a></span>";
						} elseif($LogType == "customer account" OR $LogType == "contact") {
							$LogMessage = $LogTime." | The user ".$Username." has <span style='font-weight:bold;'>".$LogAction."</span> the <span style='font-weight:bold;'>".$LogType."</span> <a href='contacts-edit.php?id=".$LogID."&action=edit'>".$LogDesc."</a>";
						} else {
							$LogMessage = $LogTime." | The user ".$Username." has <span style='font-weight:bold;'>".$LogAction."</span> the <span style='font-weight:bold;'>".$LogType."</span> ".$LogDesc."";
						}
						?>
						<li>
							<p><?php echo $LogMessage; ?></p>
						</li>
					<?php }} ?>
					</ul>
					<nav aria-label="Page navigation example">
					  <ul class="pagination">
						<li class="page-item">
						  <a class="page-link" href="logs.php?page=<?php if(($CurrentPageNum-1) == 0) { echo "1"; } else {($CurrentPageNum-1);} ?>" aria-label="Previous">
							<span aria-hidden="true">&laquo;</span>
							<span class="sr-only">Previous</span>
						  </a>
						</li>
						<li class="page-item <?php if($CurrentPageNum == 1) { echo "active"; } ?>"><a class="page-link" href="logs.php?page=1">1</a></li>
						<li class="page-item <?php if($CurrentPageNum == 2) { echo "active"; } ?>"><a class="page-link" href="logs.php?page=2">2</a></li>
						<li class="page-item <?php if($CurrentPageNum == 3) { echo "active"; } ?>"><a class="page-link" href="logs.php?page=3">3</a></li>
						<li class="page-item <?php if($CurrentPageNum == 4) { echo "active"; } ?>"><a class="page-link" href="logs.php?page=4">4</a></li>
						<li class="page-item <?php if($CurrentPageNum == 5) { echo "active"; } ?>"><a class="page-link" href="logs.php?page=5">5</a></li>
						<li class="page-item <?php if($CurrentPageNum == 6) { echo "active"; } ?>"><a class="page-link" href="logs.php?page=6">6</a></li>
						<li class="page-item <?php if($CurrentPageNum == 7) { echo "active"; } ?>"><a class="page-link" href="logs.php?page=7">7</a></li>
						<li class="page-item <?php if($CurrentPageNum == 8) { echo "active"; } ?>"><a class="page-link" href="logs.php?page=8">8</a></li>
						<li class="page-item <?php if($CurrentPageNum == 9) { echo "active"; } ?>"><a class="page-link" href="logs.php?page=9">9</a></li>
						<li class="page-item <?php if($CurrentPageNum == 10) { echo "active"; } ?>"><a class="page-link" href="logs.php?page=10">10</a></li>
						<li class="page-item">
						  <a class="page-link" href="logs.php?page=<?php echo ($CurrentPageNum+1); ?>" aria-label="Next">
							<span aria-hidden="true">&raquo;</span>
							<span class="sr-only">Next</span>
						  </a>
						</li>
					  </ul>
					</nav>
				</div>
			</div>
		</div>
		<div class="col-sm-1">
		</div>
	</div>
<!-- [ page content ] end -->
<?php include "footer.php"; ?>