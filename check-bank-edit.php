<?php
include "session.php";
$PageTitle = "Edit Financial Account";

if(isset($_GET['id']) && is_numeric($_GET['id'])) {
    $BankID = mysqli_real_escape_string($db, $_GET["id"]);
} else {
    header("location: check-banks.php");die('Please go back to the main page.');
}

include "header.php";

if(isset($_GET['action']) AND $_GET['action'] == 'delete') {
	$DepositID = mysqli_real_escape_string($db, $_GET['Deposit']);
	$DepositDelete = "DELETE FROM checks_bank_deposits WHERE Deposit_ID=$DepositID;";
	if(GDb::execute($DepositDelete)) {
		echo "<div class='alert alert-success background-success mt-4 mr-3 ml-3 mb-1'><button type='button' class='mt-1 close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected transaction record has been deleted successfully.</strong></div>";
	} else {
		echo "<div class='alert alert-danger background-danger mt-4 mr-3 ml-3 mb-1'><button type='button' class='mt-1 close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting record: " . $db->error . "</strong></div>";
	}
}

$GetBankDataSQL = 'SELECT Bank_Type, Bank_Name, Bank_Address, Bank_Transit, Bank_Routing, Bank_Account_Number, Bank_Check_Number, Bank_Notes, Bank_Add_Time, Bank_Mod_Time FROM checks_bank WHERE Bank_ID='.$BankID;
$BankData = GDb::fetchRow($GetBankDataSQL);

$GetAccountOverview = "(SELECT
    '4' AS Number, 'Refunds' AS Type, SUM(crl.Amount) AS Total
FROM customer_refund_line crl
WHERE crl.Payment_From_Account=$BankID)
UNION
(SELECT
    '7' AS Number, 'General_Expenses' AS Type, SUM(ep.Exp_Payment_Amount) AS Total
FROM expenses_payments ep
WHERE ep.Exp_Payment_Account=$BankID)
UNION
(SELECT
	'3' AS Number, 'Customer_Payments' AS Type, SUM(cp.Customer_Payment_Amount) AS Total
FROM customer_payments cp
WHERE cp.Customer_Payment_Deposit_To=$BankID)
UNION
(SELECT
	'1' AS Number, 'Deposits' AS Type, SUM(de.Deposit_Amount) AS Total
FROM checks_bank_deposits de
WHERE de.Deposit_To=$BankID)
UNION
(SELECT
	'2' AS Number, 'Withdraws' AS Type, SUM(de.Deposit_Amount) AS Total
FROM checks_bank_deposits de
WHERE de.Deposit_From=$BankID)
UNION
(SELECT
    '5' AS Number, 'Agents' AS Type, SUM(ap.Agent_Payment_Amount) AS Total
FROM agents_payments ap
WHERE ap.Agent_Payment_From_Account=$BankID)
UNION
(SELECT
    '6' AS Number, 'Suppliers' AS Type, SUM(sp.Supplier_Payment_Amount) AS Total
FROM suppliers_payments sp
WHERE sp.Supplier_Payment_From_Account=$BankID)
ORDER BY Number ASC;";
$AccountOverviewData = GDb::fetchRowSet($GetAccountOverview);
$AccountBalances = array();
foreach($AccountOverviewData AS $AccountBalance) {
    if($AccountBalance['Total'] > 0) { $BalanceTotal = $AccountBalance['Total']; } else { $BalanceTotal = 0.00; }
    $AccountBalances[$AccountBalance['Type']] = $BalanceTotal;
}
?>

<style>
    @font-face {
        font-family: MICRfont;
        src: url(files/assets/fonts/micrenc.ttf);
    }

</style>


<form name="CheckSetupForm" action="" method="POST" id="contact1" class="d-flex flex-row">
    <div class="card m-3" style="flex: 2;">
        <?php if($BankData['Bank_Type'] == 'cash') { ?>
        <div class="card-header pb-3">
            <h5>Cash Account Information</h5>
        </div>
        <div class="card-block row pt-0 pb-0">
            <div class="form-group form-default form-static-label col-sm-12">
                <label class="float-label">Account Name</label>
                <input type="hidden" name="EditCurrentBank" value="<?php echo $BankID; ?>">
                <input type="hidden" name="BankType" value="<?php echo $BankData['Bank_Type']; ?>">
                <input type="text" name="BankName" value="<?php echo $BankData['Bank_Name']; ?>" class="form-control" placeholder="The name of the bank">
            </div>
            <div class="form-group form-default form-static-label col-sm-12">
                <label class="float-label">Account Notes</label>
                <input type="text" name="BankNotes" value="<?php echo $BankData['Bank_Notes']; ?>" class="form-control">
            </div>
        </div>
        <?php } elseif($BankData['Bank_Type'] == 'bank') { ?>
        <div class="card-header pb-3">
            <h5>Bank Information</h5>
        </div>
        <div class="card-block row pt-0 pb-0">
            <div class="form-group form-default form-static-label col-sm-8">
                <label class="float-label">Bank Name</label>
                <input type="hidden" name="EditCurrentBank" value="<?php echo $BankID; ?>">
                <input type="hidden" name="BankType" value="<?php echo $BankData['Bank_Type']; ?>">
                <input type="text" name="BankName" value="<?php echo $BankData['Bank_Name']; ?>" class="form-control" placeholder="The name of the bank">
            </div>
            <div class="form-group form-default form-static-label col-sm-4">
                <label class="float-label">Check's Start Number</label>
                <input type="number" name="BankCheckNumber" value="<?php echo $BankData['Bank_Check_Number']; ?>" class="form-control" placeholder="ex. 1001">
            </div>
            <div class="form-group form-default form-static-label col-sm-12">
                <label class="float-label">Bank Address</label>
                <input type="text" name="BankAddress" value="<?php echo $BankData['Bank_Address']; ?>" class="form-control" placeholder="Bank City, State, Zip Code">
            </div>
            <div class="form-group form-default form-static-label col-sm-4">
                <label class="float-label">Transit Code (Optional)</label>
                <input type="number" name="TransitCode" value="<?php echo $BankData['Bank_Transit']; ?>" class="form-control">
            </div>
            <div class="form-group form-default form-static-label col-sm-4">
                <label class="float-label">Routing Number</label>
                <input type="number" name="RoutingNum" value="<?php echo $BankData['Bank_Routing']; ?>" class="form-control">
            </div>
            <div class="form-group form-default form-static-label col-sm-4">
                <label class="float-label">Account Number</label>
                <input type="text" name="AccountNum" value="<?php echo $BankData['Bank_Account_Number']; ?>" class="form-control">
            </div>
        </div>
        <?php } elseif($BankData['Bank_Type'] == 'cards') { ?>
        <div class="card-header pb-3">
            <h5>Credit Card Information</h5>
        </div>
        <div class="card-block row pt-0 pb-0">
            <div class="form-group form-default form-static-label col-sm-12">
                <label class="float-label">Account Name</label>
                <input type="hidden" name="EditCurrentBank" value="<?php echo $BankID; ?>">
                <input type="hidden" name="BankType" value="<?php echo $BankData['Bank_Type']; ?>">
                <input type="text" name="BankName" value="<?php echo $BankData['Bank_Name']; ?>" class="form-control" placeholder="The name of the bank">
            </div>
            <div class="form-group form-default form-static-label col-sm-12">
                <label class="float-label">Credit Card Number</label>
                <input type="text" name="AccountNum" maxlength="20" value="<?php echo $BankData['Bank_Account_Number']; ?>" class="form-control">
            </div>
        </div>
        <?php } elseif($BankData['Bank_Type'] == 'paypal') { ?>
        <div class="card-header pb-3">
            <h5>PayPal Information</h5>
        </div>
        <div class="card-block row pt-0 pb-0">
            <div class="form-group form-default form-static-label col-sm-6">
                <label class="float-label">Account Name</label>
                <input type="hidden" name="EditCurrentBank" value="<?php echo $BankID; ?>">
                <input type="hidden" name="BankType" value="<?php echo $BankData['Bank_Type']; ?>">
                <input type="text" name="BankName" value="<?php echo $BankData['Bank_Name']; ?>" class="form-control" placeholder="The name of the bank">
            </div>
            <div class="form-group form-default form-static-label col-sm-6">
                <label class="float-label">PayPal Email Address</label>
                <input type="text" name="BankAddress" value="<?php echo $BankData['Bank_Address']; ?>" class="form-control">
            </div>
            <div class="form-group form-default form-static-label col-sm-12">
                <label class="float-label">Account Notes</label>
                <input type="text" name="BankNotes" value="<?php echo $BankData['Bank_Notes']; ?>" class="form-control">
            </div>
        </div>
        <?php } ?>
        <div class="card-block col-sm-12 mt-0 pt-0">
            <button type="submit" name="UpdateBankSubmit" class="btn waves-effect waves-light btn-success mr-3" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Update</button>
            <button type="button" onclick="window.location.href = 'check-banks.php'" class="btn waves-effect waves-light btn-inverse mr-3"><i class="fas fa-ban"> </i>Cancel</button>
            <button type="button" class="btn waves-effect waves-light btn-primary" style="margin-right:20px;" data-toggle="modal" data-target="#AddDepositModal" style="margin-right:20px;"><i class="fa fa-plus-circle"></i>Add Deposit</button>
        </div>
    </div>
    <div class="card m-3" style="flex: 1;">
        <div class="card-header pb-3">
            <h5>Account Balance</h5>
        </div>
        <div class="card-block pt-0">
            <style>
                .table td {
                    padding-top: 5px;
                    padding-bottom: 5px;
                }

            </style>
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th scope="col">Category</th>
                        <th scope="col">Credit</th>
                        <th scope="col">Debit</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Deposits & Withdraws</td>
                        <td><?php echo GUtils::formatMoney($AccountBalances['Deposits']); ?></td>
                        <td><?php echo GUtils::formatMoney($AccountBalances['Withdraws']); ?></td>
                    </tr>
                    <tr>
                        <td>Customer's Payments</td>
                        <td><?php echo GUtils::formatMoney($AccountBalances['Customer_Payments']); ?></td>
                        <td><?php echo GUtils::formatMoney($AccountBalances['Refunds']); ?></td>
                    </tr>
                    <tr>
                        <td>Supplier's Payments</td>
                        <td>$0.00</td>
                        <td><?php echo GUtils::formatMoney($AccountBalances['Suppliers']); ?></td>
                    </tr>
                    <tr>
                        <td>Agent's Payments</td>
                        <td>$0.00</td>
                        <td><?php echo GUtils::formatMoney($AccountBalances['Agents']); ?></td>
                    </tr>
                    <tr>
                        <td>Expense's Payments</td>
                        <td>$0.00</td>
                        <td><?php echo GUtils::formatMoney($AccountBalances['General_Expenses']); ?></td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td class="text-right"><b>Total:</b></td>
                        <td><?php
                            $TotalCredit = $AccountBalances['Deposits']+$AccountBalances['Customer_Payments'];
                            echo GUtils::formatMoney($TotalCredit); ?></td>
                        <td><?php
                           $TotalDebit= $AccountBalances['Withdraws']+$AccountBalances['Refunds']+$AccountBalances['Suppliers']+$AccountBalances['Agents']+$AccountBalances['General_Expenses'];
                            echo GUtils::formatMoney($TotalDebit); ?></td>
                    </tr>
                    <tr>
                        <td class="text-right"><b>Balance:</b></td>
                        <td colspan="2"><?php echo GUtils::formatMoney($TotalCredit-$TotalDebit); ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</form>
<div class="d-flex">
    <div class="card m-3" style="flex:1;">
        <div class="card-header pb-3">
            <h5>Account Transactions - <?php echo $BankData['Bank_Name']; ?></h5>
        </div>
        <div class="card-block pt-0">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th scope="col" width="150">Date</th>
                        <th scope="col" width="230">Type</th>
                        <th scope="col" width="250">Account</th>
                        <th scope="col">Description</th>
                        <th scope="col" width="130">Credit</th>
                        <th scope="col" width="130">Debit</th>
                        <th scope="col" class="text-center" width="50"><i class="fa fa-cogs" aria-hidden="true"></i></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
$TotalCredit = 0;
$TotalDebit = 0;
                    
$TransactionsSQL = "(SELECT
	crl.Refund_ID AS Trans_ID,
    crl.Issue_Date AS Trans_Date,
    'Customer Refund Payment' AS Trans_Type,
    co.id AS Trans_Person_ID,
    CONCAT(co.fname,' ',co.mname,' ',co.lname) AS Trans_Person,
    crl.Description AS Trans_Details,
    crl.Payment_From_Account AS Trans_Reference_ID,
    cb.Bank_Name AS Trans_Reference,
    '0.00' AS Trans_Credit,
    crl.Amount AS Trans_Debit,
    crl.Add_Date AS Trans_Time
FROM customer_refund_line crl
JOIN checks_bank cb
ON cb.Bank_ID=crl.Payment_From_Account
JOIN customer_refund cr 
ON cr.Refund_ID=crl.Refund_ID
JOIN contacts co 
ON co.id=cr.Customer_Account_Customer_ID
WHERE crl.Payment_From_Account=$BankID)
UNION
(SELECT
	ep.Exp_Payment_ID AS Trans_ID,
    ep.Exp_Payment_Date AS Trans_Date,
    'General Expense Payment' AS Trans_Type,
    exp.Expense_Num AS Trans_Person_ID,
    payee.Payee_Name AS Trans_Person,
    ep.Exp_Payment_Comments AS Trans_Details,
    ep.Exp_Payment_Account AS Trans_Reference_ID,
    cb.Bank_Name AS Trans_Reference,
    '0.00' AS Trans_Credit,
    ep.Exp_Payment_Amount AS Trans_Debit,
    ep.Add_Date AS Trans_Time
FROM expenses_payments ep
JOIN expenses exp 
ON exp.Expense_Num=ep.Expense_Num
JOIN expenses_payee payee 
ON payee.Payee_ID=exp.Payee_ID
JOIN checks_bank cb
ON cb.Bank_ID=ep.Exp_Payment_Account
WHERE ep.Exp_Payment_Account=$BankID)
UNION
(SELECT
	cp.Customer_Payment_ID AS Trans_ID,
    cp.Customer_Payment_Date AS Trans_Date,
    'Customer Payment' AS Trans_Type,
    cp.Customer_Account_Customer_ID AS Trans_Person_ID,
    CONCAT(co.fname,' ',co.mname,' ',co.lname) AS Trans_Person,
    cp.Customer_Payment_Comments AS Trans_Details,
    cp.Customer_Payment_Deposit_To AS Trans_Reference_ID,
    cb.Bank_Name AS Trans_Reference,
    cp.Customer_Payment_Amount AS Trans_Credit,
    '0.00' AS Trans_Debit,
    cp.Add_Date AS Trans_Time
FROM customer_payments cp
JOIN checks_bank cb
ON cb.Bank_ID=cp.Customer_Payment_Deposit_To
JOIN contacts co 
ON co.id=cp.Customer_Account_Customer_ID
WHERE cp.Customer_Payment_Deposit_To=$BankID)
UNION
(SELECT
	de.Deposit_ID AS Trans_ID,
    de.Deposit_Date AS Trans_Date,
    'Deposit' AS Trans_Type,
    de.Deposit_Added_By AS Trans_Person_ID,
    CONCAT(us.fname,' ',us.lname) AS Trans_Person,
    de.Deposit_Notes AS Trans_Details,
    de.Deposit_To AS Trans_Reference_ID,
    cb.Bank_Name AS Trans_Reference,
    de.Deposit_Amount AS Trans_Credit,
    '0.00' AS Trans_Debit,
    de.Deposit_Add_Time AS Trans_Time
FROM checks_bank_deposits de
JOIN checks_bank cb
ON cb.Bank_ID=de.Deposit_To
JOIN users us 
ON us.UserID=de.Deposit_Added_By
WHERE de.Deposit_To=$BankID)
UNION
(SELECT
	de.Deposit_ID AS Trans_ID,
    de.Deposit_Date AS Trans_Date,
    'Withdrawal' AS Trans_Type,
    de.Deposit_Added_By AS Trans_Person_ID,
    CONCAT(us.fname,' ',us.lname) AS Trans_Person,
    de.Deposit_Notes AS Trans_Details,
    de.Deposit_From AS Trans_Reference_ID,
    cb.Bank_Name AS Trans_Reference,
    '0.00' AS Trans_Credit,
    de.Deposit_Amount AS Trans_Debit,
    de.Deposit_Add_Time AS Trans_Time
FROM checks_bank_deposits de
JOIN checks_bank cb
ON cb.Bank_ID=de.Deposit_From
JOIN users us 
ON us.UserID=de.Deposit_Added_By
WHERE de.Deposit_From=$BankID)
UNION
(SELECT 
	ap.Agent_Payment_ID AS Trans_ID,
    ap.Agent_Payment_Date AS Trans_Date,
    'Agent Payment' AS Trans_Type,
    ap.Agent_ID AS Trans_Person_ID,
    co.company AS Trans_Person,
    ap.Agent_Payment_Comments AS Trans_Details,
    ap.Agent_Payment_From_Account AS Trans_Reference_ID,
    cb.Bank_Name AS Trans_Reference,
    '0.00' AS Trans_Credit,
    ap.Agent_Payment_Amount AS Trans_Debit,
    ap.Add_Date AS Trans_Time
FROM agents_payments ap
JOIN checks_bank cb
ON cb.Bank_ID=ap.Agent_Payment_From_Account
JOIN contacts co 
ON co.id=ap.Agent_ID
WHERE ap.Agent_Payment_From_Account=$BankID)
UNION
(SELECT
	sp.Supplier_Payment_ID AS Trans_ID,
    sp.Supplier_Payment_Date AS Trans_Date,
    'Supplier Payment' AS Trans_Type,
    sp.Supplier_ID AS Trans_Person_ID,
    co.company AS Trans_Person,
    sp.Supplier_Payment_Comments AS Trans_Details,
    sp.Supplier_Payment_From_Account AS Trans_Reference_ID,
    cb.Bank_Name AS Trans_Reference,
    '0.00' AS Trans_Credit,
    sp.Supplier_Payment_Amount AS Trans_Debit,
    sp.Add_Date AS Trans_Time
FROM suppliers_payments sp
JOIN suppliers_bill sb
ON sb.Supplier_Bill_Num=sp.Supplier_Bill_Num
JOIN contacts co 
ON co.id=sb.Supplier_ID
JOIN checks_bank cb
ON cb.Bank_ID=sp.Supplier_Payment_From_Account
WHERE sp.Supplier_Payment_From_Account=$BankID)
ORDER BY Trans_Date ASC, Trans_Time ASC;";
                    
                    $TransactionsData = GDb::fetchRowSet($TransactionsSQL);
                    foreach($TransactionsData AS $Transaction) { 
                    
                    if($Transaction['Trans_Type'] == 'Supplier Payment') {
                        $TransPersonLink = "contacts-edit.php?id=".$Transaction['Trans_Person_ID']."&action=edit&usertype=supplierBalance";
                    } elseif($Transaction['Trans_Type'] == 'Agent Payment') {
                        $TransPersonLink = "contacts-edit.php?id=".$Transaction['Trans_Person_ID']."&action=edit&usertype=agentPayments";
                    } elseif($Transaction['Trans_Type'] == 'Customer Payment') {
                        $TransPersonLink = "contacts-edit.php?id=".$Transaction['Trans_Person_ID']."&action=edit&usertype=sales";
                    } elseif($Transaction['Trans_Type'] == 'Customer Refund Payment') {
                        $TransPersonLink = "contacts-edit.php?id=".$Transaction['Trans_Person_ID']."&action=edit&usertype=sales";
                    } elseif($Transaction['Trans_Type'] == 'General Expense Payment') {
                        $TransPersonLink = "expenses-edit.php?id=".$Transaction['Trans_Person_ID']."&action=edit";
                    } elseif($Transaction['Trans_Type'] == 'Deposit' OR $Transaction['Trans_Type'] == 'Withdrawal') {
                        $TransPersonLink = "users-edit.php?id=".$Transaction['Trans_Person_ID'];
                    } else {
                        $TransPersonLink = "";
                    }
                    ?>
                    <tr>
                        <td><?php echo GUtils::clientDate($Transaction['Trans_Date']); ?></td>
                        <td><?php echo $Transaction['Trans_Type']; ?></td>
                        <td><?php echo "<a href='".$TransPersonLink."' target='_blank'>".$Transaction['Trans_Person']."</a>"; ?></td>
                        <td><?php echo $Transaction['Trans_Details']; ?></td>
                        <td><?php if($Transaction['Trans_Credit'] > 0) {echo GUtils::formatMoney($Transaction['Trans_Credit']);} else { echo "--"; } $TotalCredit += $Transaction['Trans_Credit']; ?></td>
                        <td class="text-danger"><?php if($Transaction['Trans_Debit'] > 0) {echo GUtils::formatMoney($Transaction['Trans_Debit']);} else { echo "--"; }
                        $TotalDebit += $Transaction['Trans_Debit']; ?></td>
                        <td class="text-center"><?php if($Transaction['Trans_Type'] == 'Deposit' OR $Transaction['Trans_Type'] == 'Withdrawal') { ?>
                            <a  href="javascript:void(0);" onclick = 'deleteConfirm( "check-bank-edit.php?action=delete&id=<?php echo $BankID; ?>&Deposit=<?php echo $Transaction['Trans_ID']; ?>", "Delete")'><i class="fa fa-trash" aria-hidden="true"></i></a>
                        <?php } else { ?>
                            <i class="fa fa-trash" style="opacity:0.2" aria-hidden="true"></i>
                        <?php } ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr style="border-top: double 3px #999;">
                        <td class="text-right" colspan="4"><b>Sub Totals:</b></td>
                        <td><?php echo GUtils::formatMoney($TotalCredit); ?></td>
                        <td><?php echo GUtils::formatMoney($TotalDebit); ?></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right" colspan="4"><b>Balance:</b></td>
                        <td colspan="3"><?php echo GUtils::formatMoney($TotalCredit - $TotalDebit); ?></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
</div>

<script>
    $(document).ready(function() {
        $("form").submit(function() {
            // Getting the form ID
            var formID = $(this).attr('id');
            var formDetails = $('#' + formID);
            //To not let the form of uploading attachments included in this
            if (formID != 'contact6' && formID != 'idGroupEmailForm') {
                $.ajax({
                    type: "POST",
                    url: 'inc/checks-functions.php',
                    data: formDetails.serialize(),
                    success: function(data) {
                        // Inserting html into the result div
                        $('#results').html(data);
                        //$("form")[0].reset();
                    },
                    error: function(jqXHR, text, error) {
                        // Displaying if there are any errors
                        $('#result').html(error);
                    }
                });
                return false;
            }
        });
    });
</script>


<div class="modal fade" id="resultsmodal" tabindex="-1" role="dialog" style="top:90px;z-index:99999;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
                <div id="results" class="confirmation-message-result-area">
                    <div class="upload-loader"></div>
                    <p>Prcoessing...</p>
                </div>
            </div>
            <div class="modal-footer2">
                <button type="button" class="btn btn-success waves-effect waves-light" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="AddDepositModal" tabindex="-1" role="dialog" style="top:50px;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
                <form id="contact16" name="contact16" method="post" action="inc/checks-functions.php">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <h3>Add a Deposit</h3>
                        </div>
                        <div class="col-sm-4">
                            <label class="float-label">Date of Deposit</label>
                            <input type="text" name="Deposit_Add" value="ADD" class="form-control" hidden>
                            <input type="text" name="Deposit_To" value="<?php echo $BankID; ?>" class="form-control" hidden>
                            <input type="text" name="Deposit_Added_By" value="<?php echo $UserAdminID; ?>" class="form-control" hidden>
                            <input type="date" name="Deposit_Date" class="form-control" value="<?php echo date("Y-m-d"); ?>" required>
                            <input type="text" name="Deposit_Type" class="form-control" value="original" required>
                        </div>
                        <div class="col-sm-4">
                            <label class="float-label">Amount of Deposit</label>
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <label class="input-group-text">$</label>
                                </span>
                                <input name="Deposit_Amount" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label class="float-label">From Account</label>
                            <select name="Deposit_From" class="form-control">
                                <option value="">Select</option>
                                <?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name FROM checks_bank WHERE Bank_Status=1 AND Bank_Type != 'supplier' ";
                                    $BanksData = GDb::fetchRowSet($GetBankAccountSQL);
                                    foreach ($BanksData AS $BankData) { ?>
                                <option value="<?php echo $BankData['Bank_ID']; ?>"><?php echo $BankData['Bank_Name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <label class="float-label">Comments / Refrences</label>
                            <input type="text" name="Deposit_Notes" class="form-control">
                        </div>
                        <div class="col-sm-12"><br />
                            <button type="submit" name="AddDepositSubmit" class="btn waves-effect waves-light btn-success mr-3" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Add</button>
                            <button type="reset" class="btn waves-effect waves-light btn-inverse" data-dismiss="modal"><i class="fas fa-ban"></i>Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php //This pop up modal is for when trying to delete a record ?>
<div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" style="background:white;border-radius:10px;" role="document">
        <div class="sweet-alert showSweetAlert sweet-alert-customized" data-custom-class="" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: block;">
            <div class="sa-icon sa-warning pulseWarning" style="display: block;">
                <span class="sa-body pulseWarningIns"></span>
                <span class="sa-dot pulseWarningIns"></span>
            </div>
            <h2 class="delete-title">Are you sure?</h2>
            <p class="delete-msg">This action is irreversible!</p>
            <div class="sa-button-container"><br />
                <a type="button" class="btn btn-default waves-effect  waves-light" data-dismiss="modal">Close</a>
                <a type="submit" href="javascript:void(0)" class="delete-target btn btn-danger waves-effect waves-light" >Delete</a>
            </div>
        </div>
    </div>
</div>
<?php //include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>
