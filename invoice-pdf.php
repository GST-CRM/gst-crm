<?php

require_once('session.php');
$Customer_ID = $__GET['customerID'];
$Group_ID = $__GET['groupID'];

// Include the main TCPDF library (search for installation path).
require_once('files/pdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Element Media');
$pdf->SetTitle('GST Customer Invoice');
$pdf->SetSubject('GST Customer Invoice');
$pdf->SetKeywords('GST, CRM, Element, Media, Element.ps');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set margins
$pdf->SetMargins(15, 25, 15);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('Helvetica', '', 11);

// add a page
$pdf->AddPage('P','LETTER');

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

//To collect the data of the contacts and the customer invoice
$sql = "SELECT so.Sales_Order_Num,
		so.Sales_Order_Date,
		so.Sales_Order_Category,
		so.Sales_Order_Type,
		so.Sales_Order_Stop,
		so.Cancellation_Date,
		so.Customer_Account_Customer_ID,
		co.*,
		gr.tourid,
		gr.tourname,
		gr.listprice,
        inv.Customer_Invoice_Num,
        inv.Customer_Invoice_Date,
        inv.Due_Date,
		inv.Comments

		FROM sales_order so
		JOIN contacts co 
		ON so.Customer_Account_Customer_ID=co.id
		JOIN groups gr 
		ON so.Group_ID=gr.tourid
        JOIN customer_invoice inv 
        ON co.id=inv.Customer_Account_Customer_ID
		WHERE so.Group_ID=$Group_ID AND so.Customer_Account_Customer_ID=$Customer_ID";
$result = $db->query($sql);
$InvoiceData = $result->fetch_assoc();



if ($result->num_rows > 0) { 


// create some HTML content
$html = '
<table cellpadding="7" cellspacing="0" border="0">
	<tbody>
		<tr>
			<td width="65%">
				<img src="files/assets/images/logo-invoice.png" width="300">
			</td>
			<td width="35%">
				<b>Invoice #:</b> '.$InvoiceData["Customer_Invoice_Num"].'<br>
				<b>Created:</b> '.date("m/d/Y", strtotime($InvoiceData["Customer_Invoice_Date"])).'<br>
				<b>Due:</b> '.date("m/d/Y", strtotime($InvoiceData["Due_Date"])).'								
			</td>
		</tr>
		<tr>
			<td>
				<strong style="font-weight:bold;">'.$InvoiceData["title"].' '.$InvoiceData["fname"].' '.$InvoiceData["mname"].' '.$InvoiceData["lname"].'</strong><br>
				'.$InvoiceData["address1"].' '.$InvoiceData["address2"].'<br>
				'.$InvoiceData["city"].', '.$InvoiceData["state"].', '.$InvoiceData["zipcode"].'
			</td>
			<td>
				<strong style="font-weight:bold;">'.$Company_Name.'</strong><br>
				'.$Company_Address.'<br>
				'.$Company_Email.'
			</td>
		</tr>
		<tr>
			<td colspan="2" height="15">
			</td>
		</tr>
		<tr bgcolor="#eee">
			<td>
				<b>Item</b>
			</td>    
			<td align="right">
				<b>Price</b>
			</td>
		</tr>
		<tr>
			<td>Package Fee</td>
			<td align="right">'.$InvoiceData["listprice"].'</td>
		</tr>';
		$conn = new mysqli($servername, $username, $password, $dbname);
		if ($conn->connect_error) {die("Connection failed: " . $conn->connect_error);} 
		$SalesOrderID = $InvoiceData['Sales_Order_Num'];
		$sql2 = "SELECT sol.*,pro.name,con.fname,con.lname,con.company FROM sales_order_line sol
				JOIN products pro 
				ON sol.Product_Product_ID=pro.id
				JOIN contacts con 
				ON sol.Supplier_Supplier_ID=con.id
				WHERE Sales_Order_Num=$SalesOrderID";
		$result2 = $conn->query($sql2);
		//$row_cnt = $result2->fetch_assoc();
		$Total_Sales_Amount = $InvoiceData["listprice"];
		if ($result2->num_rows > 0) {
			while($row_cnt = $result2->fetch_assoc()) {
				if($TR_background == "white") {$bgcolor = "#ffffff";$TR_background="grey";} else {$bgcolor = "#f9f9f9";$TR_background="white";}
				$html .='<tr bgcolor="'.$bgcolor.'">
					<td>'.$row_cnt["Line_Type"].'</td>
					<td align="right">'.$row_cnt["Sales_Amount"].'</td>
				</tr>';
				$Total_Sales_Amount = $Total_Sales_Amount+$row_cnt["Sales_Amount"];
			}
		}
		$html .='
		<tr bgcolor="#f2f2f2">
			<td colspan="2" align="right">
				<b>Total: $'.$Total_Sales_Amount.'</b>
			</td>
		</tr>
		<tr>
			<td colspan="2" height="25"></td>
		</tr>';
		$sql = "SELECT * FROM customer_payments WHERE Customer_Account_Customer_ID=$Customer_ID ORDER BY Customer_Payment_Date ASC";
		$result3 = $db->query($sql);
		$TotalPaymentsAmount = 0;
		if ($result3->num_rows > 0) {
		$html .='<tr bgcolor="#eee">
			<td><b>Payment Method</b></td>
			<td align="right"><b>Amount</b></td>
		</tr>';
		while($row = $result3->fetch_assoc()) {
			$html .='<tr>
						<td>'.$row["Customer_Payment_Method"].' <small>('.date("m/d/Y", strtotime($row["Customer_Payment_Date"])).')</small></td>
						<td align="right">$'.$row["Customer_Payment_Amount"].'</td>
					</tr>';
			$TotalPaymentsAmount = $TotalPaymentsAmount+$row["Customer_Payment_Amount"];
			}
		$html .='<tr bgcolor="#f2f2f2">
			<td colspan="2" align="right"><b>Total: $'.$TotalPaymentsAmount.'</b></td>
		</tr>';
		}
		$html .='
		<tr bgcolor="#333">
			<td colspan="2" align="center"><b><span color="white">Total Amount Needed: $'.($Total_Sales_Amount-$TotalPaymentsAmount).'</span></b></td>
		</tr>
		<tr>
			<td colspan="2" height="25">
			</td>
		</tr>
		<tr>
			<td colspan="2"><strong style="font-weight:bold;">Comments:</strong> '.$InvoiceData['Comments'].'</td>
		</tr>
	</tbody>
</table>
<p align="center">If you have any questions about this invoice, please contact us.<br />
<b>Thank you for participating with our pilgrimages!</b></p>
';} else {
$html = 'This customer account does not have any invoice records yet!.';}

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');


// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('Customer-Invoice.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
