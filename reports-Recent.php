<?php
include "session.php";
$PageTitle = "Recent Registrations Report";
include "header.php"; 

$cStatus = $_GET['status'];
$FromDate = $_GET['FromDate'];
$UntilDate = $_GET['UntilDate'];
$DateRange = $_GET['DateRange'];
?>
<!--<script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>	 -->
<style type="text/css">
#customers_list_filter { display:none;}
</style>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
		<div class="card">
            <div class="row">
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
						<label class="float-label" for="cbBIRTH">Search for a specific Customer</label>
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
					</div>
				</div>
				<div class="col-md-9" style="margin-top:15px;">
				<form id="AdvancedFilter" class="col-12 row" action="reports-Recent.php" method="GET">
					<div class="form-group form-default form-static-label col-md-4">
						<label class="float-label" for="cbBIRTH">Quick Date Range</label>
						<div class="pl-3 row">
						<div class="col-md-4 pr-0">
							<input class="form-check-input" id="Yesterday" type="radio" name="DateRange" value="1day" <?php if(!empty($_GET['DateRange']) AND $_GET['DateRange'] == "1day") {echo "checked";} ?>>
							<label class="form-check-label" for="Yesterday">Yesterday</label>
						</div>
						<div class="col-md-4 pr-0">
							<input class="form-check-input" id="SevenDays" type="radio" name="DateRange" value="7day" <?php if(!empty($_GET['DateRange']) AND $_GET['DateRange'] == "7day") {echo "checked";} ?>>
							<label class="form-check-label" for="SevenDays">7 Days</label>
						</div>
						<div class="col-md-4 pr-0">
							<input class="form-check-input" id="ThirtyDays" type="radio" name="DateRange" value="30day" <?php if(!empty($_GET['DateRange']) AND $_GET['DateRange'] == "30day") {echo "checked";} ?>>
							<label class="form-check-label" for="ThirtyDays">30 Days</label>
						</div>
						</div>
					</div>
					<div class="form-group form-default form-static-label col-md-2">
						<label class="float-label" for="cbBIRTH">From</label>
						<input class="form-control" id="cbBIRTH" type="date" name="FromDate" value="<?php if(!empty($_GET['FromDate'])) {echo $_GET['FromDate'];} ?>">
					</div>
					<div class="form-group form-default form-static-label col-md-2">
						<label class="float-label" for="cbPassport">Until</label>
						<input class="form-control" id="cbPassport" type="date" name="UntilDate" value="<?php if(!empty($_GET['UntilDate'])) {echo $_GET['UntilDate'];} ?>">
					</div>
					<div class="form-group form-default form-static-label col-md-2">
						<label class="float-label" for="cbPassport">Status</label>
						<select id="status" name="status" class="form-control form-control-default fill">
							<option value=""></option>
							<option value="both" <?php if(!empty($_GET['status']) AND $_GET['status'] == "both") {echo "selected";} ?>>All</option>
							<option value="Active" <?php if(!empty($_GET['status']) AND $_GET['status'] == "Active") {echo "selected";} ?>>Active</option>
							<option value="Cancelled" <?php if(!empty($_GET['status']) AND $_GET['status'] == "Cancelled") {echo "selected";} ?>>Cancelled</option>
							<option value="Disabled" <?php if(!empty($_GET['status']) AND $_GET['status'] == "Disabled") {echo "selected";} ?>>Disabled</option>
						</select>
					</div>
					<div class="form-group form-default form-static-label col-md-2 pr-0">
						<label class="float-label" for="cbPassport">Let's filter the results</label>
						<button type="submit" class="btn btn-info btn-block waves-effect waves-light" style="padding: 5px 13px;">Filter</button>
					</div>
				</form>
				</div>
			</div>
            <div class="card-block">
				<div class="dt-responsive table-responsive">
                    <table id="customers_list" class="table table-hover table-striped table-bordered nowrap" style="width:100%;" data-page-length="20">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>ContactID</th>
                                <th>Full Name</th>
                                <th>First Name</th>
                                <th>Middle Name</th>
                                <th>Last Name</th>
                                <th>Group ID</th>
                                <th>Group Reference</th>
                                <th>Group Name</th>
                                <th>Added By</th>
                                <th><?php if($cStatus == "Cancelled") { echo "Cancellation Date"; } else { echo "Registration Date"; } ?></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<!-- [ page content ] end -->
<script>
//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#customers_list').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
});


$( document ).ready(function() {
	$('#customers_list').DataTable({
		 "bProcessing": true,
         "serverSide": true,
		 "order": [[ 1, "desc" ]],
		 dom: 'Bfrtip',
		 "columnDefs": [
			  { 
			  //"visible": false,
			  "targets": 2,
				 "render": function(data, type, row, meta){
					if(type === 'display'){
						data = '<a href="contacts-edit.php?id=' + row[1] + '&action=edit&usertype=customer" style="color: #0000EE;">' + row[2] + ' ' + row[3] + ' ' + row[4] + ' ' + row[5] + '</a>';
					}
					return data;
				 }
			  },
			  { 
			  //"visible": false,
			  "targets": 7,
				 "render": function(data, type, row, meta){
					if(type === 'display'){
						data = '<a href="groups-edit.php?id=' + row[6] + '&action=edit" style="color: #0000EE;">' + row[7] +'</a>';
					}
					return data;
				 }
			  },
			  { 
			  //"visible": false,
			  "targets": 8,
				 "render": function(data, type, row, meta){
					if(type === 'display'){
						data = '<a href="groups-edit.php?id=' + row[6] + '&action=edit" style="color: #0000EE;">' + row[8] +'</a>';
					}
					return data;
				 }
			  },
			  { 
			  //"visible": false,
			  "targets": 9,
				 "render": function(data, type, row, meta){
					if(type === 'display'){
						data = '<a href="users-edit.php?id=' + row[9] + '" style="color: #0000EE;">' + row[11] +' ' + row[12] +'</a>';
					}
					return data;
				 }
			  },
             { 
			  //"visible": false,
			  "targets": 10,
				 "render": function(data, type, row, meta){
					if(type === 'display'){
                        <?php if($cStatus == "Cancelled") { ?>
                            data = row[13]; 
                        <?php } else { ?>
                            data = row[10];
                        <?php } ?>
						
					}
					return data;
				 }
			  },
			  { 
			  "visible": false,
			  "targets": [0,1,3,4,5,6]
			  }
			  
		   ],
		 buttons: ['csv', 'excel'],
         "ajax":{
            url :"inc/contact-reports-Recents-List.php?status=<?php echo isset($cStatus) ? $cStatus : '1';?>&from=<?php echo isset($FromDate) ? $FromDate : '0';?>&until=<?php echo isset($UntilDate) ? $UntilDate : '0';?>&DateRange=<?php echo isset($DateRange) ? $DateRange : '0';?>", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#customers_list_processing").css("display","none");
            }
          },
			"language": {
				"emptyTable": "There are no new registrations recorded currently."
			}
        });   
});
</script>
<?php include "footer.php"; ?>