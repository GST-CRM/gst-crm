<?php
include "session.php";
$PageTitle = "Email Templates";
include "header.php";

if ($_GET['action'] == "delete") {
	$template_id = $_GET['template_id'];
	$sql = "DELETE FROM email_templates_new WHERE template_id=$template_id";
	//$sql = "UPDATE email_templates_new SET status = 0 WHERE template_id=$template_id";
	if ($db->multi_query($sql) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected email template record has been deleted successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting record: " . $db->error . "</strong></div>";
	}
}

if (($_GET['action'] == "disable") || ($_GET['action'] == "enable")) {
	$template_id = $_GET['template_id'];
	if ($_GET['action'] == "disable") {
		$setStatus = 0;
	}
	if ($_GET['action'] == "enable") {
		$setStatus = 1;
	}
	$sql = "UPDATE email_templates_new SET status = $setStatus WHERE template_id=$template_id";
	if ($db->multi_query($sql) === TRUE) {
		if ($_GET['action'] == "disable") {
			echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected email template record has been disabled successfully.</strong></div>";
		}
		if ($_GET['action'] == "enable") {
			echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected email template record has been enabled successfully.</strong></div>";
		}
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error Disable/Enable record: " . $db->error . "</strong></div>";
	}
}

if ($_GET['action'] == "delete") {
	$template_id = $_GET['template_id'];
	$sql = "DELETE FROM email_templates_new WHERE template_id=$template_id";
	if ($db->multi_query($sql) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected email template record has been deleted successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting record: " . $db->error . "</strong></div>";
	}
}

?>

<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
		<div class="card">
            <div class="row">
				<div class="col-md-6">
					<div class="card-header table-card-header">
						<h5>List of All Email Templates</h5>
					</div>
				</div>
				<?php echo GUtils::flashMessage() ; ?>
				<div class="col-md-6" style="margin-top:15px;">
					<a href="email-templates-add.php" class="btn waves-effect waves-light btn-success" style="margin-right: 30px;float:right; padding: 3px 13px;"><i class="far fa-check-circle"></i>Add New Email Template</a>
					<?php if ($_GET['status'] == "disabled") { ?>
						<a href="email-templates.php" class="btn btn-mat waves-effect waves-light btn-inverse float-right" style="margin-right: 15px; padding: 3px 13px;">All Templates</a>
						<?php } else { ?>
						<a href="email-templates.php?status=disabled" class="btn btn-mat waves-effect waves-light btn-inverse float-right" style="margin-right: 15px; padding: 3px 13px;">Disabled Templates</a>
						<?php } ?>
				</div>
			</div>
            <div class="card-block">
				<div class="dt-responsive table-responsive">
				<!-- basic-btn -->
                    	<table id="templates_grid" class="table table-hover table-striped table-bordered nowrap" data-page-length="10">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Folder</th>
                                <th>Template Name</th>
                                <th>Subject</th>
                                <th>Created On</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                        		if ($_GET['status'] == "disabled") {
									$templateStatusWhere = "WHERE etn.status=0";
								} else {
									$templateStatusWhere = "";
								}

                        		$sql = "SELECT etn.template_id, ett.name, etn.template_name, etn.template_subject,etn.createdon,etn.status FROM email_templates_new etn JOIN email_templates_type ett ON ett.type_id = etn.template_type_id $templateStatusWhere ORDER BY etn.template_id DESC";
                        		$result = $db->query($sql);
								if ($result->num_rows > 0) { 
									while($row = $result->fetch_assoc()) { 
										$statusDisabled = ($row['status'] == 0) ? "style='background: #ffc3c3'" : '';
										?>
									<tr <?php echo $statusDisabled; ?>>
										<td><?php echo $row['template_id'] ?></td>
										<td><?php echo $row['name']; ?></td>
										<td><a href="email-templates-edit.php?id=<?php echo $row["template_id"]; ?>&action=edit" style="color: #0000EE;"><?php echo wordwrap($row['template_name'],55,"<br>\n") ?></a></td>
										<td><?php echo wordwrap($row['template_subject'],55,"<br>\n"); ?></td>
										<td><?php echo date('m/d/Y', strtotime($row['createdon'])); ?></td>
										<td>
									<div class="btn-group">
										<a class="btn btn-primary btn-sm" href="email-templates-add.php" role="button" id="dropdownMenuLink">Actions</a>
										<button type="button" class="btn btn-sm btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<div class="dropdown-menu">
											<a class="dropdown-item" href="email-templates-edit.php?id=<?php echo $row["template_id"]; ?>&action=edit" >Edit</a>
											<?php 
											if ($row['template_name'] != 'Footer Email Template - Dont Delete') {
												if ($row['status'] == 1) { ?>
												<a class="dropdown-item" href="javascript:void(0);" onclick = 'deleteConfirm( "email-templates.php?action=disable&template_id=<?php echo $row["template_id"]; ?>", "Disable", "to Disable this template")'>Disable</a>
												<?php } else { ?>
												<a class="dropdown-item" href="javascript:void(0);" onclick = 'deleteConfirm( "email-templates.php?action=enable&template_id=<?php echo $row["template_id"]; ?>", "Enable", "to Enable this template")'>Enable</a>
												<?php } ?>
												<a class="dropdown-item" href="javascript:void(0);" onclick = 'deleteConfirm( "email-templates.php?action=delete&template_id=<?php echo $row["template_id"]; ?>", "Delete")'>Delete</a>
											<?php } ?>
										</div>
									</div>
								</td>
									</tr>
								<?php } 
								}
                        	?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<!-- [ page content ] end -->
<script>
$(document).ready(function() {
    $('#templates_grid').DataTable( {
        "ordering": false,
		dom: 'Bfrtip',
        buttons: [
            'csv', 'excel'
        ]
    } );
} );
</script>
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>