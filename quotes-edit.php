<?php include "session.php";


//To make several url options in the same page
$allowed = array('edit');
if ( ! isset($_GET['action'])) {header("location: quotes.php");die('Please go back to the main page.');}
$action = $_GET['action'];
if ( ! in_array($action, $allowed)) {header("location: quotes.php");die('Please go back to the main page.');}

$QuoteID = $_GET["id"];

//To collect the data of the above customer id, and show it in the fields below
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {die("Connection failed: " . $conn->connect_error);} 
$sql = "SELECT quo.*, con.fname,con.mname,con.lname FROM quotes quo JOIN contacts con ON quo.TourleaderID = con.id WHERE Quote_ID=$QuoteID";
$result = $conn->query($sql);
$data = $result->fetch_assoc();
$conn->close();

// Hay el general header information fe kol el pages
$PageTitle = "Edit the quote for - ".$data["Group_Name"];
include "header.php";

?>
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->

<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="card">
            <div class="card-header">
                <h5>Edit the information of this quote</h5>
            </div>
            <div class="card-block">
				<form id="main" name="main" method="post" action="inc/quotes-functions.php" class="row">
					<div class="form-group form-default form-static-label col-sm-6">
						<label class="float-label">Group Name</label>
						<input type="text" name="Current_Group_Name" value="<?php echo $data["Group_Name"]; ?>" class="form-control fill">
						<input type="text" name="Quote_ID" value="<?php echo $QuoteID; ?>" class="form-control fill" hidden>
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Proposal Date</label>
						<input type="date" name="Proposal_Date" value="<?php echo $data["Proposal_Date"]; ?>" class="form-control fill">
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<h4 class="sub-title">Status</h4>
						<div class="can-toggle">
						  <input id="Quote_Status" name="Quote_Status" value="1" type="checkbox" <?php if($data['Quote_Status'] == 1) {echo "checked";} ?>>
						  <label for="Quote_Status">
							<div class="can-toggle__switch" data-checked="Active" data-unchecked="Disabled"></div>
						  </label>
						</div>
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Group Start Date</label>
						<input type="date" name="Start_Date" value="<?php echo $data["Start_Date"]; ?>" class="form-control fill">
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Group End Date</label>
						<input type="date" name="End_Date" value="<?php echo $data["End_Date"]; ?>" class="form-control fill">
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Group Price</label>
						<input type="text" name="Group_Price" value="<?php echo $data["Group_Price"]; ?>" class="form-control fill">
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Single Supplement</label>
						<input type="text" name="Single_Supplement" value="<?php echo $data["Single_Supplement"]; ?>" class="form-control fill">
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Departure City</label>
						<input type="text" name="Departure_City" value="<?php echo $data["Departure_City"]; ?>" class="form-control fill">
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Tour Leader Name</label>
						<select name="TourleaderID" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<?php 
							$TourLeaderID = $data["TourleaderID"];
							$con6 = new mysqli($servername, $username, $password, $dbname);
							$result6 = mysqli_query($con6,"SELECT con.id,con.fname,con.mname,con.lname FROM group_leaders lea JOIN contacts con ON lea.contactid=con.id WHERE lea.status=1");
							while($row6 = mysqli_fetch_array($result6))
							{
								if($row6['id'] == $TourLeaderID) {$LeaderSelected = "selected";} else { $LeaderSelected = ""; }
									
								echo "<option value='".$row6['id']."' $LeaderSelected>".$row6['fname']." ".$row6['lname']."</option>";
								
							} ?>
						</select>
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Church Name</label>
						<input type="text" name="Church_Name" value="<?php echo $data["Church_Name"]; ?>" class="form-control fill">
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Itinerary Title</label>
						<input type="text" name="Itinerary_Title" value="<?php echo $data["Itinerary_Title"]; ?>" class="form-control fill">
					</div>
                    <div class="col-sm-12"><br /><br />
						<button type="submit" name="addquote" class="btn waves-effect waves-light btn-success mr-1" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Update</button>
						<a href="inc/quotes-pdf.php?id=<?php echo $QuoteID; ?>" class="btn waves-effect waves-light btn-info mr-1" download>Download the Proposal</a>
                        <button type="reset" class="btn waves-effect waves-light btn-inverse" onclick="window.location='quotes.php'"><i class="fas fa-ban"></i>Clear</button>
					</div>
				</form>
			</div>
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<?php require('inc/metadata-js.php'); ?>

<script type="text/javascript">
$(document).ready(function() {
    $("form").submit(function() {
        $('#results').html('');
        // Getting the form ID
        var  formID = $(this).attr('id');
        var formDetails = $('#'+formID);
        //To not let the form of uploading attachments included in this
        if (formID != 'contact6') {
            $.ajax({
                type: "POST",
                url: 'inc/quotes-functions.php',
                data: formDetails.serialize(),
                success: function (data) {  
                    // Inserting html into the result div
                    $('#results').html(data);
                    //$("form")[0].reset();
                    formmodified = 0;
                },
                error: function(jqXHR, text, error){
                // Displaying if there are any errors
                    $('#result').html(error);           
                }
            });
            return false;
        }
    });
});
</script>
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>