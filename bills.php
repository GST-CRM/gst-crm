<?php
include "session.php";
$PageTitle = "Supplier Bills";
include "header.php"; 


//To get the URL parameters needed
if(isset($_GET['action'])) {$action = mysqli_real_escape_string($db, $_GET['action']);} else { $action = ""; }
if(isset($_GET['filter'])) {$filter = mysqli_real_escape_string($db, $_GET['filter']);}
if(isset($_GET['bill_id'])) {$Bill_ID = mysqli_real_escape_string($db, $_GET['bill_id']);}
if(isset($_GET['status'])) {$Status = mysqli_real_escape_string($db, $_GET['status']);} else { $Status = ""; }
if(isset($_GET['FromDate'])) {$FromDateFilter = mysqli_real_escape_string($db, $_GET['FromDate']);}
if(isset($_GET['UntilDate'])) {$UntilDateFilter = mysqli_real_escape_string($db, $_GET['UntilDate']);}


//To show the Void Message
if ($action == "void") {
	$BillVoid = "UPDATE suppliers_bill SET Status=0 WHERE Supplier_Bill_Num=$Bill_ID";
	if ($db->multi_query($BillVoid) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected supplier bill record has been voided successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error voiding record: " . $db->error . "</strong></div>";
	}
}

//To show the Un-Void Message
if ($action == "unvoid") {
	$BillVoid = "UPDATE suppliers_bill SET Status=1 WHERE Supplier_Bill_Num=$Bill_ID";
	if ($db->multi_query($BillVoid) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected supplier bill record has been un-voided successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error un-voiding record: " . $db->error . "</strong></div>";
	}
}

//To show the Delete Message
if ($action == "delete") {
	$BillVoid = "DELETE FROM suppliers_bill WHERE Supplier_Bill_Num=$Bill_ID";
	if ($db->multi_query($BillVoid) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected supplier bill record has been deleted successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting record: " . $db->error . "</strong></div>";
	}
}


//Numbers Summary for the Due and Paid
$sqlsummary = "SELECT  SUM(IFNULL(NotYetDue,0)-IFNULL(AmountNotYetDue,0)) AS NotYetDue, SUM(IFNULL(InDue,0)-IFNULL(AmountInDue,0)) AS InDue, SUM(IFNULL(Paid,0)) AS Paid FROM (
        SELECT (IF( Due_Date > CURDATE(), Bill_Amount,0)) AS NotYetDue,  
        (IF( Due_Date <= CURDATE(), Bill_Amount,0)) AS InDue,
        SUM(IF( Due_Date > CURDATE(), Supplier_Payment_Amount ,0)) AS AmountNotYetDue,  
        SUM(IF( Due_Date <= CURDATE(), Supplier_Payment_Amount ,0)) AS AmountInDue,
        SUM(Supplier_Payment_Amount) AS Paid
        from suppliers_bill sb
        LEFt JOIN suppliers_payments sp ON sp.Supplier_Bill_Num= sb.Supplier_Bill_Num AND sp.Status=1 
        WHERE sb.Status=1 AND sb.Bill_Amount != 0 GROUP BY sb.Supplier_Bill_Num ) AS a" ;
$summary = GDb::fetchRow($sqlsummary) ;

$TotalDue = $summary['NotYetDue'] + $summary['InDue'];
$NotYetDue = $summary['NotYetDue'];
$OverDue = $summary['InDue'];
$TotalAmount = $summary['Paid'] ;

if( $TotalDue != 0 ) {
    $first = intval( floor($NotYetDue * 100 / $TotalDue) ) ;
    $second = intval( ceil( $OverDue * 100 / $TotalDue) ) ;
}
else {
    $first = 0 ;
    $second = 0 ;
}
if( ($TotalAmount + $TotalDue) != 0 ) {
    $third = $TotalDue * 100 / ($TotalAmount + $TotalDue) ;
}
else {
    $third = 0 ;
}

//To filter the results regarding if its Due or Paid or not due
$FilterWhere = "";
if( isset($filter) ) {
    $class = $filter ;
    if( $filter == 'total_due' ) {
        $FilterWhere = " HAVING Bill_Amount > total " ;
    } else if( $filter == 'not_yet_due' ) {
        $FilterWhere = " HAVING Bill_Amount > total AND Due_Date > CURDATE() " ;
    } else if( $filter == 'over_due' ) {
        $FilterWhere = " HAVING Bill_Amount > total AND Due_Date <= CURDATE() " ;
    } else if( $filter == 'paid' ) {
        $FilterWhere = " HAVING total > 0 " ;
    }
}

$BillsWhere = "";
if ($Status == "voided") { $BillsWhere .= " AND sb.Status=0 "; } else { $BillsWhere .= " AND sb.Status=1 "; }
if ($FromDateFilter > 0) { $BillsWhere .= " AND sb.Supplier_Bill_Date >= '$FromDateFilter' "; }
if ($UntilDateFilter > 0) { $BillsWhere .= " AND sb.Supplier_Bill_Date <= '$UntilDateFilter' "; }


?>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
        <div class="card">
            <form class="row" action="bills.php" method="GET">
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
					<label class="float-label" for="searchbox">Custom Search</label>
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input type="text" id="searchbox" class="outsideBorderSearch form-control" placeholder="Search...">
					</div>
				</div>
				<div class="col-md-2" style="margin-top:15px;">
					<label class="float-label" for="FromDate">From Date</label>
					<input class="form-control" id="FromDate" type="date" name="FromDate" value="<?php if(!empty($FromDateFilter)) {echo $FromDateFilter;} ?>">
				</div>
				<div class="col-md-2" style="margin-top:15px;">
					<label class="float-label" for="UntilDate">Until Date</label>
					<input class="form-control" id="UntilDate" type="date" name="UntilDate" value="<?php if(!empty($UntilDateFilter)) {echo $UntilDateFilter;} ?>">
				</div>
				<div class="col-md-1" style="margin-top:15px;padding-right:20px;">
					<label class="float-label" for="status">Status</label>
					<select id="status" name="status" class="form-control form-control-default fill">
						<option disabled selected value="all"></option>
						<?php if($Status === "") { $Status = "all"; } ?>
						<option value="active" <?php if(!empty($Status) AND $Status == "active") {echo "selected";} ?>>Active Bills</option>
						<option value="voided" <?php if(!empty($Status) AND $Status == "voided") {echo "selected";} ?>>Voided Bills</option>
					</select>
				</div>
				<div class="col-md-1" style="margin-top:15px;">
					<label class="float-label">Show Results</label>
					<button type="submit" class="btn btn-info btn-block waves-effect waves-light" style="padding: 5px 13px;">Filter</button>
				</div>
				<div class="col-md-3" style="margin-top:15px;padding-right:45px;">
					<hr style="border: 0;height: 1px;background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0));margin-top: 14px;margin-bottom: 13px;" />
					<a href="bills-add.php" class="btn btn-block waves-effect float-right waves-light btn-success" style="padding: 5px 13px;"><i class="fas fa-plus-circle"></i>Add Supplier Bill</a>
				</div>
			</form>
            <div class="row">
				<div class="page-wrapper mt-4" style="width:100%;padding: 0.6rem 2.8rem;">
				<div class="row">
				<div class="col-3">
					<div onclick="window.location.href='bills.php?filter=total_due';" class="card mb-0 o-hidden bg-c-yellow web-num-card" style="cursor: pointer;">
						<div class="card-block text-white">
							<h5 class="mt-1">Total Unpaid</h5>
							<h3 class="mb-1"><?php echo GUtils::formatMoney($TotalDue) ;?></h3>
						</div>
					</div>
				</div>
				<div class="col-3">
					<div onclick="window.location.href='bills.php?filter=not_yet_due';" class="card mb-0 o-hidden bg-c-blue web-num-card" style="cursor: pointer;">
						<div class="card-block text-white">
							<h5 class="mt-1">Not Due Yet</h5>
							<h3 class="mb-1"><?php echo GUtils::formatMoney($NotYetDue) ;?></h3>
						</div>
					</div>
				</div>
				<div class="col-3">
					<div onclick="window.location.href='bills.php?filter=over_due';" class="card mb-0 o-hidden bg-c-red web-num-card" style="cursor: pointer;">
						<div class="card-block text-white">
							<h5 class="mt-1">Overdue</h5>
							<h3 class="mb-1"><?php echo GUtils::formatMoney($OverDue) ;?></h3>
						</div>
					</div>
				</div>
				<div class="col-3">
					<div onclick="window.location.href='bills.php?filter=paid';" class="card mb-0 o-hidden bg-c-green web-num-card" style="cursor: pointer;">
						<div class="card-block text-white">
							<h5 class="mt-1">Paid</h5>
							<h3 class="mb-1"><?php echo GUtils::formatMoney($TotalAmount) ;?></h3>
						</div>
					</div>
				</div>
				</div>
				</div>
			</div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="TableWithNoButtons" class="table table-striped table-bordered nowrap" style="width:100%;" data-page-length="20">
                        <thead>
                            <tr>
                                <th>Bill #</th>
                                <th>Supplier Name</th>
                                <th>Date</th>
                                <th>Due Date</th>
                                <th>Total</th>
                                <th>Paid</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
						<?php
						$sql = "SELECT sb.*,
								if(sb.Due_Date = '0000-00-00','N/A',sb.Due_Date) AS Due_Date_Format,
								c.company,c.fname,c.lname,SUM(IFNULL(sp.Supplier_Payment_Amount,0)) AS total FROM suppliers_bill sb
								LEFT JOIN contacts c 
								ON c.id=sb.Supplier_ID
								LEFT JOIN suppliers_payments sp 
								ON sp.Supplier_Bill_Num=sb.Supplier_Bill_Num
								WHERE 1 $BillsWhere
								GROUP BY sb.Supplier_Bill_Num
								$FilterWhere
								ORDER BY sb.Supplier_Bill_Num DESC";
						$result = $db->query($sql);
						if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
                            <tr>
                                <td><a href="bills-edit.php?id=<?php echo $row["Supplier_Bill_Num"]; ?>&action=edit" style="color: #0000EE;">
								<?php echo "E".$row["Supplier_Bill_Num"]; ?></a></td>
                                <td><a href="contacts-edit.php?id=<?php echo $row["Supplier_ID"]; ?>&action=edit&usertype=supplier" style="color: #0000EE;"><?php echo $row["company"]; ?></td>
                                <td><?php if($row["Supplier_Bill_Date"] != NULL) {echo date('m/d/Y', strtotime($row["Supplier_Bill_Date"]));} else { echo "N/A"; } ?></td>
                                <td><?php if($row["Due_Date_Format"] != 'N/A') {echo date('m/d/Y', strtotime($row["Due_Date_Format"]));} else { echo "N/A"; } ?></td>
								<?php 
								$Supp_Bill_Num = $row["Supplier_Bill_Num"];
								$PaidSQL = "SELECT SUM(Supplier_Payment_Amount) AS Paid FROM suppliers_payments WHERE Supplier_Bill_Num=$Supp_Bill_Num AND Status=1;";
								$PaidResult = $db->query($PaidSQL);
								$PaidAmountData = $PaidResult->fetch_assoc();

								if($PaidAmountData["Paid"] > $row["Bill_Amount"]) {$TextStyle = "class='bg-danger'";}
								elseif($PaidAmountData["Paid"] == $row["Bill_Amount"]) {$TextStyle = "class='bg-success'";}
								else {$TextStyle = "";}
								?>
                                <td><?php echo GUtils::formatMoney($row["Bill_Amount"]); ?></td>
                                <td <?php echo $TextStyle; ?>><?php echo GUtils::formatMoney($PaidAmountData["Paid"]); ?></td>
                                <td>
									<div class="btn-group">
										<?php if($Status == "voided") { ?>
										<a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick = 'deleteConfirm( "bills.php?action=unvoid&bill_id=<?php echo $row["Supplier_Bill_Num"]; ?>", "Unvoid")' role="button">Unvoid</a>
										<?php } else { ?>
										<a class="btn btn-primary btn-sm" href="suppliers-bill-payments-add.php?bill=<?php echo $row["Supplier_Bill_Num"]; ?>" role="button" id="dropdownMenuLink">Send Payment</a>
										<button type="button" class="btn btn-sm btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<div class="dropdown-menu">
											<a class="dropdown-item" href="bills-edit.php?id=<?php echo $row["Supplier_Bill_Num"]; ?>&action=edit" >Edit</a>
											<a class="dropdown-item" target="_blank" href="bills-print.php?print=payment&Bill_ID=<?php echo $row["Supplier_Bill_Num"]; ?>" >Print</a>
											<a class="dropdown-item" href="javascript:void(0);" onclick = 'deleteConfirm( "bills.php?action=delete&bill_id=<?php echo $row["Supplier_Bill_Num"]; ?>", "Delete")'>Delete</a>
											<a class="dropdown-item" href="javascript:void(0);" onclick = 'deleteConfirm( "bills.php?action=void&bill_id=<?php echo $row["Supplier_Bill_Num"]; ?>", "Void")'>Void</a>
										</div>
										<?php } ?>
									</div>
								</td>
							</tr>
						<?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<script type="text/javascript">
//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#TableWithNoButtons').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
});

$(document).ready(function() {
    $('#TableWithNoButtons').DataTable( {
        //"ordering": false, //To disable the ordering of the tables
		"bLengthChange": false, //To hide the Show X entries dropdown
		dom: 'Bfrtip',
		sDom: 'lrtip', //To Hide the search box
        buttons: [] //To hide the export buttons
    } );
} );

function doReload(status){
	document.location = 'bills.php?status=' + status;
}
</script>

<!-- [ page content ] end -->
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>