<?php
include "session.php";
$PageTitle = "Quotes";
include "header.php"; 

?>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
        <div class="card">
            <div class="row">
				<div class="col-md-5">
					<div class="card-header table-card-header">
						<h5>List of All the <?php if($_GET['status'] == "disabled") {echo $WhereClause = "Disabled";} ?> Quotes</h5>
					</div>
				</div>
				<div class="col-md-7" style="margin-top:15px;">
					<div>
						<a href="quotes.php?status=disabled" class="btn btn-mat waves-effect waves-light btn-inverse float-right" style="margin-right: 35px; padding: 3px 13px;">Disabled Quotes</a>
						<a href="quotes.php" class="btn btn-mat waves-effect waves-light btn-inverse float-right" style="margin-right: 5px; padding: 3px 13px;">Active Quotes</a>
						<a href="quotes-add.php" class="btn btn-mat waves-effect waves-light btn-success float-right" style="margin-right: 5px; padding: 3px 13px;">Add New</a>
					</div>
				</div>
			</div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="basic-btn" class="table table-striped table-bordered nowrap">
                        <thead>
                            <tr>
                                <th>Quote ID</th>
                                <th>Group Name</th>
                                <th>Church Name</th>
                                <th>Proposal Date</th>
                                <th>Departure City</th>
                                <th>Suggested Price</th>
                                <th>Tour Leader</th>
                                <th>Download</th>
                            </tr>
                        </thead>
                        <tbody>
						<?php
						//$sql = "select * from agents";
						if($_GET['status'] == "disabled") {$WhereClause = "quo.Quote_Status=0";} else {$WhereClause = "quo.Quote_Status=1";}
						$sql = "SELECT quo.Quote_ID, quo.Group_Name, quo.Proposal_Date, quo.Group_Price, quo.Departure_City, quo.TourleaderID, con.fname, con.mname, con.lname, quo.Church_Name
								FROM quotes quo 
								JOIN contacts con 
								ON quo.TourleaderID = con.id
								WHERE $WhereClause";
						$result = $db->query($sql);
						if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
                            <tr>
                                <td><?php echo "Q".$row["Quote_ID"]; ?></td>
                                <td><a href="quotes-edit.php?id=<?php echo $contactidz=$row["Quote_ID"]; ?>&action=edit" style="color: #0000EE;">
								<?php echo $row["Group_Name"]; ?></a></td>
                                <td><?php echo $row["Church_Name"]; ?></td>
                                <td><?php echo $row["Proposal_Date"]; ?></td>
                                <td><?php echo $row["Group_Price"]; ?></td>
                                <td><?php echo $row["Departure_City"]; ?></td>
                                <td><?php echo $row["fname"]." ".$row["mname"]." ".$row["lname"]; ?></td>
                                <td class="text-center"><a href="inc/quotes-pdf.php?id=<?php echo $row["Quote_ID"]; ?>" download><img src="<?php echo $crm_images; ?>/download.png" alt="Download the Proposal" style="width:25px;height:auto;"/></a></td>
							</tr>
						<?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<!-- [ page content ] end -->
<?php include "footer.php"; ?>