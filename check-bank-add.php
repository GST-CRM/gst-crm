<?php
include "session.php";
$PageTitle = "Add New Account";
include "header.php";
?>

<style>
.cc-selector input{
    margin:0;padding:0;
    -webkit-appearance:none;
       -moz-appearance:none;
            appearance:none;
}

.bank{background-image:url('files/assets/images/account-bank.jpg');}
.cards{background-image:url('files/assets/images/account-cards.jpg');}
.cash{background-image:url('files/assets/images/account-cash.jpg');}
.paypal{background-image:url('files/assets/images/account-paypal.jpg');}
.supplier{background-image:url('files/assets/images/account-supplier.jpg');}

.cc-selector input:active +.drinkcard-cc{opacity: .9;}
.cc-selector input:checked +.drinkcard-cc{
    -webkit-filter: none;
       -moz-filter: none;
            filter: none;
}
.drinkcard-cc{
    cursor:pointer;
    background-size:contain;
    background-repeat:no-repeat;
    display:inline-block;
    width:130px;height:98px;
    margin: auto 10px;
    -webkit-transition: all 100ms ease-in;
       -moz-transition: all 100ms ease-in;
            transition: all 100ms ease-in;
    -webkit-filter: brightness(1.2) grayscale(1) opacity(.7);
       -moz-filter: brightness(1.2) grayscale(1) opacity(.7);
            filter: brightness(1.2) grayscale(1) opacity(.7);
}
.drinkcard-cc:hover{
    -webkit-filter: brightness(1.0) grayscale(.5) opacity(.9);
       -moz-filter: brightness(1.0) grayscale(.5) opacity(.9);
            filter: brightness(1.0) grayscale(.5) opacity(.9);
}
</style>

<form name="CheckSetupForm" action="" method="POST" id="contact1">
<div class="row">
    <div class="col-sm-2"></div>
    <div class="col-sm-8">
        <div class="card">
            <div class="card-header pb-3">
                <h5>Account Information</h5>
            </div>
            <div class="card-block row pt-0">
                
                    <div class="cc-selector text-center col-12 pb-2">
						<input type="hidden" name="Add_Account" value="ADD" />
                        <input id="cash" type="radio" name="accounts" value="cash"><label class="drinkcard-cc cash" for="cash"></label>
                        <input id="bank" type="radio" name="accounts" value="bank"><label class="drinkcard-cc bank"for="bank"></label>
                        <input id="cards" type="radio" name="accounts" value="cards"><label class="drinkcard-cc cards"for="cards"></label>
                        <input id="paypal" type="radio" name="accounts" value="paypal"><label class="drinkcard-cc paypal"for="paypal"></label>
                        <input id="supplier" type="radio" name="accounts" value="supplier"><label class="drinkcard-cc supplier"for="supplier"></label>
                    </div>
                    <hr />
                
                <div class="MainAccountTab form-group form-default form-static-label col-sm-12 text-center">
                    Please select one of the above Account types first.
                </div>
                <div class="CashDetails form-group form-default form-static-label col-sm-4" style="display:none;">
                    <label class="float-label">Cash Account Name</label>
                    <input type="text" name="C_Bank_Name" class="form-control">
                </div>
                <div class="CashDetails form-group form-default form-static-label col-sm-8" style="display:none;">
                    <label class="float-label">Cash Account Notes</label>
                    <input type="text" name="C_Bank_Notes" class="form-control">
                </div>
                <div class="BankDetails form-group form-default form-static-label col-sm-8" style="display:none;">
                    <label class="float-label">Bank Name</label>
                    <input type="text" name="B_BankName" class="form-control" placeholder="The name of the bank">
                </div>
                <div class="BankDetails form-group form-default form-static-label col-sm-4" style="display:none;">
                    <label class="float-label">Check's Start Number</label>
                    <input type="number" name="B_BankCheckNumber" class="form-control" placeholder="ex. 1001">
                </div>
                <div class="BankDetails form-group form-default form-static-label col-sm-12" style="display:none;">
                    <label class="float-label">Bank Address</label>
                    <input type="text" name="B_BankAddress" class="form-control" placeholder="Bank City, State, Zip Code">
                </div>
                <div class="BankDetails form-group form-default form-static-label col-sm-4" style="display:none;">
                    <label class="float-label">Transit Code (Optional)</label>
                    <input type="number" name="B_TransitCode" class="form-control">
                </div>
                <div class="BankDetails form-group form-default form-static-label col-sm-4" style="display:none;">
                    <label class="float-label">Routing Number</label>
                    <input type="number" name="B_RoutingNum" class="form-control">
                </div>
                <div class="BankDetails form-group form-default form-static-label col-sm-4" style="display:none;">
                    <label class="float-label">Account Number</label>
                    <input type="text" name="B_AccountNum" class="form-control">
                </div>
                <div class="CardsDetails form-group form-default form-static-label col-sm-6" style="display:none;">
                    <label class="float-label">Credit Card Account Name</label>
                    <input type="text" name="Cr_Bank_Name" class="form-control">
                </div>
                <div class="CardsDetails form-group form-default form-static-label col-sm-6" style="display:none;">
                    <label class="float-label">Credit Card Number</label>
                    <input type="text" name="Cr_Bank_Account_Number" class="form-control">
                </div>
                <div class="PaypalDetails form-group form-default form-static-label col-sm-4" style="display:none;">
                    <label class="float-label">PayPal Account Name</label>
                    <input type="text" name="P_Bank_Name" class="form-control">
                </div>
                <div class="PaypalDetails form-group form-default form-static-label col-sm-4" style="display:none;">
                    <label class="float-label">PayPal Email</label>
                    <input type="text" name="P_Bank_Address" class="form-control">
                </div>
                <div class="PaypalDetails form-group form-default form-static-label col-sm-4" style="display:none;">
                    <label class="float-label">Notes</label>
                    <input type="text" name="P_Bank_Notes" class="form-control">
                </div>
                <div class="SupplierDetails form-group form-default form-static-label col-sm-4" style="display:none;">
                    <label class="float-label">Supplier Name</label>
                    <select class="form-control" name="S_Bank_Name">
                        <option value="0">Select</option>
                        <?php
							$SupplierResult = mysqli_query($db,"SELECT con.id,con.company FROM suppliers sup JOIN contacts con ON con.id=sup.contactid WHERE sup.status=1 AND con.id NOT IN (SELECT Bank_Name FROM checks_bank WHERE Bank_Type='supplier')");
							while($SupplierData = mysqli_fetch_array($SupplierResult)) {
								echo "<option value='".$SupplierData['id']."'>".$SupplierData['company']."</option>";
							}
						?>
                    </select>
                </div>
                <div class="SupplierDetails form-group form-default form-static-label col-sm-8" style="display:none;">
                    <label class="float-label">Comments/Notes</label>
                    <input type="text" name="S_Bank_Notes" class="form-control">
                </div>
                <div class="col-sm-12">
                    <hr />
                    <button type="submit" name="AddNewBankSubmit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal" style="margin-right:20px;" ><i class="far fa-check-circle"></i>Save</button>
                    <button type="button" onclick="window.location.href = 'check-banks.php'" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"> </i>Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-2"></div>
</div>
</form>
<script>
$(document).ready(function() {
    $("input[name=accounts]").on( "change", function() {
        var AccountType = $(this).val();
        if(AccountType == 'bank') {
            $(".BankDetails").show('slow');
            $(".CardsDetails").hide('slow');
            $(".PaypalDetails").hide('slow');
            $(".MainAccountTab").hide('slow');
            $(".CashDetails").hide('slow');
            $(".SupplierDetails").hide('slow');
        } else if(AccountType == 'cards') {
            $(".CardsDetails").show('slow');
            $(".BankDetails").hide('slow');
            $(".PaypalDetails").hide('slow');
            $(".MainAccountTab").hide('slow');
            $(".CashDetails").hide('slow');
            $(".SupplierDetails").hide('slow');
        } else if(AccountType == 'paypal') {
            $(".PaypalDetails").show('slow');
            $(".BankDetails").hide('slow');
            $(".CardsDetails").hide('slow');
            $(".MainAccountTab").hide('slow');
            $(".CashDetails").hide('slow');
            $(".SupplierDetails").hide('slow');
        } else if(AccountType == 'cash') {
            $(".CashDetails").show('slow');
            $(".BankDetails").hide('slow');
            $(".CardsDetails").hide('slow');
            $(".MainAccountTab").hide('slow');
            $(".PaypalDetails").hide('slow');
            $(".SupplierDetails").hide('slow');
        } else if(AccountType == 'supplier') {
            $(".CashDetails").hide('slow');
            $(".BankDetails").hide('slow');
            $(".CardsDetails").hide('slow');
            $(".MainAccountTab").hide('slow');
            $(".PaypalDetails").hide('slow');
            $(".SupplierDetails").show('slow');
        } else {
            $(".BankDetails").hide('slow');
            $(".CardsDetails").hide('slow');
            $(".PaypalDetails").hide('slow');
            $(".MainAccountTab").hide('slow');
            $(".CashDetails").hide('slow');
            $(".SupplierDetails").hide('slow');
        }
    } );
});
    
$(document).ready(function() {
	$("form").submit(function() {
		// Getting the form ID
		var  formID = $(this).attr('id');
		var formDetails = $('#'+formID);
		//To not let the form of uploading attachments included in this
		if (formID != 'contact6' && formID != 'idGroupEmailForm') {
			$.ajax({
				type: "POST",
				url: 'inc/checks-functions.php',
				data: formDetails.serialize(),
				success: function (data) {	
					// Inserting html into the result div
					$('#results').html(data);
					//$("form")[0].reset();
				},
				error: function(jqXHR, text, error){
				// Displaying if there are any errors
					$('#result').html(error);           
				}
			});
			return false;
		}
	});
});
</script>

<div class="modal fade" id="resultsmodal" tabindex="-1" role="dialog" style="top:50px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
                <div id="results" class="confirmation-message-result-area">
					<div class="upload-loader"></div>
					<p>Prcoessing...</p>
				</div>
            </div>
            <div class="modal-footer2">
                <button type="button" class="btn btn-success waves-effect waves-light" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<?php include "footer.php"; ?>