<?php
include "session.php";
$PageTitle = "General Expenses";
include "header.php"; 

if(isset($_GET['action'])) {
    $action = $_GET['action'];
} else {
    $action = "";
}

if(isset($_GET['status'])) {
    $StatusGET = mysqli_real_escape_string($db, $_GET['status']);
} else {
    $StatusGET = "";
}

//To show the Void Message
if ($action == "void") {
	$ExpenseID = $_GET['exp_id'];
	$ExpVoid1 = "UPDATE expenses SET Status=0 WHERE Expense_Num=$ExpenseID;";
	GDb::execute($ExpVoid1);
	$ExpVoid2 = "UPDATE expenses_payments SET Status=0 WHERE Expense_Num=$ExpenseID;";
	GDb::execute($ExpVoid2);
	if( GDb::execute($ExpVoid1) && GDb::execute($ExpVoid2)) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected expense record has been voided successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error voiding record: " . $db->error . "</strong></div>";
	}
}

//To show the Un-Void Message
if ($action == "unvoid") {
	$ExpenseID = $_GET['exp_id'];
	$ExpUnVoid1 = "UPDATE expenses SET Status=1 WHERE Expense_Num=$ExpenseID;";
	GDb::execute($ExpUnVoid1);
	$ExpUnVoid2 = "UPDATE expenses_payments SET Status=1 WHERE Expense_Num=$ExpenseID;";
	GDb::execute($ExpUnVoid2);
	if( GDb::execute($ExpUnVoid1) && GDb::execute($ExpUnVoid2)) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected expense record has been un-voided successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error un-voiding record: " . $db->error . "</strong></div>";
	}
}

//To show the Delete Message
if ($action == "delete") {
	$ExpenseID = $_GET['exp_id'];
	$ExpDelete1 = "DELETE FROM expenses WHERE Expense_Num=$ExpenseID;";
	GDb::execute($ExpDelete1);
	$ExpDelete2 = "DELETE FROM expenses_payments WHERE Expense_Num=$ExpenseID;";
	GDb::execute($ExpDelete2);
	if( GDb::execute($ExpDelete1) && GDb::execute($ExpDelete2)) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected expense record has been deleted successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting record: " . $db->error . "</strong></div>";
	}
}

?>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
        <div class="card">
            <div class="row">
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
					</div>
				</div>
				<div class="col-md-5" style="margin-top:15px;"></div>
				<div class="col-md-2" style="margin-top:15px;">
					<select id="status" name="status" class="form-control form-control-default fill" onChange="doReload(this.value);">
						<option value="0">Status</option>
						<option value="active" <?php if(!empty($StatusGET) AND $StatusGET == "active") {echo "selected";} ?>>Active Expenses</option>
						<option value="voided" <?php if(!empty($StatusGET) AND $StatusGET == "voided") {echo "selected";} ?>>Voided Expenses</option>
						<option value="paid" <?php if(!empty($StatusGET) AND $StatusGET == "paid") {echo "selected";} ?>>Paid Expenses</option>
					</select>
				</div>
				<div class="col-md-2" style="margin-top:15px;padding-right:45px;">
					<a href="expenses-add.php" class="btn waves-effect float-right waves-light btn-success" style="padding: 3px 13px;"><i class="fas fa-plus-circle"></i>Add New Expense</a>
				</div>
			</div>
            <div class="card-block " >
                <div class="dt-responsive table-responsive " style="position: relative;">                    
                    <table id="TableWithNoButtons" class="table table-striped table-bordered nowrap" style="width:100%;">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Expense Date</th>
                                <th>Payee Name</th>
                                <th>Expense Amount</th>
                                <th style="display:none;">Due Date</th>
                                <th>Paid So Far</th>
                                <th>Balance Due</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
						<?php
						if ($StatusGET == "voided") {
							$WhereCondition = "exp.status=0";
						} elseif($StatusGET == "active") {
							$WhereCondition = "exp.status=1";
						} elseif($StatusGET == "paid") {
							$WhereCondition = "exp.status=1 AND ((exp.Expense_Amount-(SELECT SUM(Exp_Payment_Amount) FROM expenses_payments WHERE Expense_Num=exp.Expense_Num)) = 0) ";
						} else {
							$WhereCondition = "exp.status=1  ";
						}
						$sql = "SELECT exp.*,gro.tourname,payee.Payee_Name,
								(SELECT SUM(Exp_Payment_Amount) FROM expenses_payments WHERE Expense_Num=exp.Expense_Num) AS TotalPaid
								FROM expenses exp 
								LEFT JOIN groups gro 
								ON exp.Group_ID=gro.tourid
                                JOIN expenses_payee payee 
                                ON payee.Payee_ID=exp.Payee_ID
								WHERE $WhereCondition
								ORDER BY exp.Expense_Date DESC";
						$result = $db->query($sql);
						if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
                            <tr>
                                <td><a href="expenses-edit.php?id=<?php echo $row["Expense_Num"]; ?>&action=edit" style="color: #0000EE;">
								<?php echo $row["Expense_Num"]; ?></a></td>
                                <td><?php echo date('m/d/Y', strtotime($row["Expense_Date"])); ?></td>
                                <td><a href="expenses-edit.php?id=<?php echo $row["Expense_Num"]; ?>&action=edit" style="color: #0000EE;"><?php echo $row["Payee_Name"]; ?></a></td>
                                <td><?php echo GUtils::formatMoney($row["Expense_Amount"]); ?></td>
                                <td style="display:none;"><?php echo $row["Due_Date"]; ?></td>
                                <td><?php if($row["TotalPaid"] > 0) {echo GUtils::formatMoney($row["TotalPaid"]);} else { echo "$0.00";} ?></td>
                                <?php $TheBalanceDue = $row["Expense_Amount"]-$row["TotalPaid"];
								if($TheBalanceDue < 0) { $TDstyle = "font-weight:bold;color:#d84444;";} elseif($TheBalanceDue == 0) { $TDstyle = "font-weight:bold;color:#6ba730;";} else { $TDstyle = ""; } ?>
								<td style="<?php echo $TDstyle; ?>"><?php echo GUtils::formatMoney($TheBalanceDue); ?></td>
                                <td>
									<div class="btn-group">
										<?php if($StatusGET == "voided") { ?>
										<a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick = 'deleteConfirm( "expenses.php?action=unvoid&exp_id=<?php echo $row["Expense_Num"]; ?>", "Unvoid")' role="button">Unvoid</a>
										<?php } else { ?>
										<a class="btn btn-primary btn-sm" href="expenses-edit.php?id=<?php echo $row["Expense_Num"]; ?>&action=edit" role="button" id="dropdownMenuLink">Edit</a>
										<button type="button" class="btn btn-sm btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<div class="dropdown-menu">
											<a class="dropdown-item" href="expenses-payment-action.php?action=add&id=<?php echo $row["Expense_Num"]; ?>">Add Payment</a>
											<a class="dropdown-item" href="javascript:void(0);" onclick = 'deleteConfirm( "expenses.php?action=delete&exp_id=<?php echo $row["Expense_Num"]; ?>", "Delete")'>Delete</a>
											<a class="dropdown-item" href="javascript:void(0);" onclick = 'deleteConfirm( "expenses.php?action=void&exp_id=<?php echo $row["Expense_Num"]; ?>", "Void")'>Void</a>
										</div>
										<?php } ?>
									</div>
								</td>
							</tr>
						<?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<!-- [ page content ] end -->
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>

<style type="text/css">
#basic-btn_filter { display:none;}
#basic-btn_wrapper .dt-buttons {
    position: absolute;
    right: 10px;
    top: 10px;
}
</style>
<script>
$(document).ready(function() {

    //Just to move the search box to be on the top of the card section
    $('.outsideBorderSearch').on( 'keyup click', function () {
        $('#TableWithNoButtons').DataTable().search(
            $('.outsideBorderSearch').val()
        ).draw();
    });
});

$(document).ready(function() {
    $('#TableWithNoButtons').DataTable( {
        //"ordering": false, //To disable the ordering of the tables
		"bLengthChange": false, //To hide the Show X entries dropdown
		dom: 'Bfrtip',
		sDom: 'lrtip', //To Hide the search box
        buttons: [], //To hide the export buttons
		"order": [[ 0, "desc" ]]
    } );
} );

function doReload(status){
	document.location = 'expenses.php?status=' + status;
}
</script>