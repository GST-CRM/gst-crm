<?php
include 'session.php';
$PageTitle = 'Dashboard';
$bodyclass = '';

if( AclPermission::activeRoleId() == AclRole::$CUSTOMER ) {
    include __DIR__ . '/inc/dashboard-customer.php' ;
    return ;
}

include 'header.php';

if ( AclPermission::activeRoleId() == AclRole::$AGENT ) {
    include __DIR__ . '/inc/dashboard-agent.php' ;
}
else if( AclPermission::activeRoleId() == AclRole::$ADMIN ) {
    include __DIR__ . '/inc/dashboard-admin.php' ;
}
?>
<?php include 'footer.php';
?>
