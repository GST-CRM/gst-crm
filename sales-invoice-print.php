<?php

require_once('session.php');

require_once 'inc/helpers.php';
ini_set('display_errors', true) ;

$PDF_HEADER = false ;
if( isset($__REQUEST['print']) && $__REQUEST['print'] == 'payment' ) {
    $printTitle = 'Receipt' ;
}
else if( isset($__REQUEST['print']) && $__REQUEST['print'] == 'invoice' ) {
    $printTitle = 'Invoice' ;
}
include 'inc/sales-invoice-pdf.php' ;