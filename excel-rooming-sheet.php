<?php

    $query="SELECT * FROM (SELECT
	co.id,
	co.title,
	co.fname,
	co.mname,
	co.lname,
	IF(ca.roomtype = NULL OR ca.roomtype = 0, 'Not Assigned', ca.roomtype) AS roomtype,
	NULL AS Roommate1ID,
	NULL AS Roommate1,
	NULL AS Roommate2ID,
	NULL AS Roommate2,
	if(ca.Transfer=1,(SELECT description FROM products WHERE id=ca.Transfer_Product_ID),'No') AS Need_Transfer,
	if(ca.Extension=1,(SELECT description FROM products WHERE id=ca.Extension_Product_ID),'No') AS Need_Extension,
	ca.special_land_text,
	ca.Meals_Allergies,
	ca.Meals_Request,
	ca.Allergies_Request,
    ca.status AS contact_status
FROM contacts co
JOIN customer_account ca
	ON ca.contactid=co.id AND ca.tourid=$groupid
WHERE
	/* Khader: I have enabled this line again, Nithin or Kiran, if you have a reason why you want to disable it, please tell me asap */
	/*ca.status=1
	AND */
    ca.tourid=$groupid
	AND NOT EXISTS (SELECT *
                    FROM customer_groups
                    WHERE Type='Rooming'
                    AND co.id IN(Primary_Customer_ID,Additional_Traveler_ID_1,Additional_Traveler_ID_2)
                   )
UNION

SELECT
	co.id,
	co.title,
	co.fname,
	co.mname,
	co.lname,
	ca.roomtype,
	cg.Additional_Traveler_ID_1 AS Roommate1ID,
	(SELECT CONCAT(title,' ',fname,' ',mname,' ',lname) FROM contacts WHERE id=cg.Additional_Traveler_ID_1) AS Roommate1,
	cg.Additional_Traveler_ID_2 AS Roommate2ID,
	(SELECT CONCAT(title,' ',fname,' ',mname,' ',lname) FROM contacts WHERE id=cg.Additional_Traveler_ID_2) AS Roommate2,
    if(ca.Transfer=1,(SELECT description FROM products WHERE id=ca.Transfer_Product_ID),'No') AS Need_Transfer,
	if(ca.Extension=1,(SELECT description FROM products WHERE id=ca.Extension_Product_ID),'No') AS Need_Extension,
	ca.special_land_text,
	ca.Meals_Allergies,
	ca.Meals_Request,
	ca.Allergies_Request,
    ca.status AS contact_status
FROM contacts co 
JOIN customer_account ca 
	ON ca.contactid=co.id AND ca.tourid=$groupid
JOIN customer_groups cg
	ON co.id=cg.Primary_Customer_ID AND cg.Type='Rooming' AND cg.Group_ID=$groupid
WHERE 1 
	/*-- AND ca.status=1*/
) AS a ORDER BY contact_status DESC";
    $result = $db->query($query); 
    //$rows = $result->fetch_assoc(); 
    $header1 = array($groupname." - #".$groupid);
    /*$header2 = array(
      'Room #'=>'integer',
      'Room Type'=>'string',
      'Person #1'=>'string',
      'Person #2'=>'string',
      'Person #3'=>'string',
      'Note'=>'string',
    );*/
    $header2 = array('Room #','Room Type','Person #1','Person #2','Person #3','Need Transfer?','Need Extension?','Special Land Requests','Meals Requests','Allergies Requests');
	
	$styles4 = array('font-style'=>'bold', 'fill'=>'#eee', 'halign'=>'center','border'=>'left,right,top,bottom');
    
    $stylesCancel = array('font-style'=>'bold', 'fill'=>'#eee', 'halign'=>'center','border'=>'left,right,top,bottom');
	
	$writer->setTitle($filetitle);
	$writer->setSubject($filesubject);
	$writer->setAuthor($fileauthor);
	$writer->setCompany($filecompany);
	$writer->setDescription($filedesc);
	
	$writer->writeSheetRow('Rooming', $header1, $styles4);
	$writer->writeSheetRow('Rooming', $header2, $styles4);
    $iRoomingHeadingLines = 1 ;
    
	$i = 1;
    $array = array();
    $canceled = false;
    while ($row=$result->fetch_assoc())
    {
		$person2ID = $row['Roommate1ID'];
		$person3ID = $row['Roommate2ID'];
		$TransferText = NULL;
		$ExtensionText = NULL;
		$NeedTransfer = $row['Need_Transfer'];
		$NeedExtension = $row['Need_Extension'];

		if($person2ID != NULL) {
			//To get the product details if this roommate has transfer product
			$Person2TransferSQL = 	"SELECT ca.Transfer, pro.description FROM products pro
									JOIN customer_account ca ON ca.contactid=$person2ID AND ca.tourid=$groupid
									WHERE id=ca.Transfer_Product_ID;";
			$Person2TransferResult = $db->query($Person2TransferSQL);
			$Person2TransferData = $Person2TransferResult->fetch_assoc();
			if($Person2TransferData['Transfer']==1) {
				$Person2Transfer = $Person2TransferData['description'];
			} else {
				$Person2Transfer = NULL;
			};
			//To get the product details if this roommate has extension product
			$Person2ExtSQL = 	"SELECT ca.Extension, pro.description FROM products pro
								JOIN customer_account ca ON ca.contactid=$person2ID AND ca.tourid=$groupid
								WHERE id=ca.Extension_Product_ID;";
			$Person2ExtResult = $db->query($Person2ExtSQL);
			$Person2ExtData = $Person2ExtResult->fetch_assoc();
			if($Person2ExtData['Extension']==1) {
				$Person2Extension = $Person2ExtData['description'];
			} else {
			$Person2Extension = NULL;
			};
		}
		if($person3ID != NULL) {
			//To get the product details if this roommate has transfer product
			$Person3TransferSQL = 	"SELECT ca.Transfer, pro.description FROM products pro
									JOIN customer_account ca ON ca.contactid=$person3ID AND ca.tourid=$groupid
									WHERE id=ca.Transfer_Product_ID;";
			$Person3TransferResult = $db->query($Person3TransferSQL);
			$Person3TransferData = $Person3TransferResult->fetch_assoc();
			if($Person3TransferData['Transfer']==1) {
				$Person3Transfer = $Person3TransferData['description'];
			} else {
				$Person3Transfer = NULL;
			};
			//To get the product details if this roommate has extension product
			$Person3ExtSQL = 	"SELECT ca.Extension, pro.description FROM products pro
								JOIN customer_account ca ON ca.contactid=$person3ID AND ca.tourid=$groupid
								WHERE id=ca.Extension_Product_ID;";
			$Person3ExtResult = $db->query($Person3ExtSQL);
			$Person3ExtData = $Person3ExtResult->fetch_assoc();
			if($Person3ExtData['Extension']==1) {
				$Person3Extension = $Person3ExtData['description'];
			} else {
			$Person3Extension = NULL;
			};
		}
		
		if($NeedTransfer != "No") {$TransferText .="Person #1: ".$NeedTransfer.", ";}
		if($Person2Transfer != NULL) {$TransferText .="Person #2: ".$Person2Transfer.", ";}
		if($Person3Transfer != NULL) {$TransferText .="Person #3: ".$Person3Transfer.", ";}
		if($NeedTransfer == "No" AND $Person2Transfer == NULL AND $Person3Transfer == NULL) { $TransferText ="No";}
		
		if($NeedExtension != "No") {$ExtensionText .="Person #1: ".$NeedExtension.", ";}
		if($Person2Extension != NULL) {$ExtensionText .="Person #2: ".$Person2Extension.", ";}
		if($Person3Extension != NULL) {$ExtensionText .="Person #3: ".$Person3Extension.", ";}
		if($NeedExtension == "No" AND $Person2Extension == NULL AND $Person3Extension == NULL) { $ExtensionText ="No";}
        
		$MealsAllergies = $row['Meals_Allergies'];
		$MealsRequest = $row['Meals_Request'];
		$AllergiesRequest = $row['Allergies_Request'];

		
		if( ! $canceled && $row['contact_status'] == 0 ) {
            $writer->writeSheetRow('Rooming', ['Canceled Members'], $stylesCancel);
            $canceled = true ;
            $writer->markMergedCell('Rooming', $i+$iRoomingHeadingLines, 0, $i+$iRoomingHeadingLines, 9) ;
        }
        
		$array[1] = $i;
		$array[2] = $row['roomtype'];		
        $array[3] = $row['title']." ".$row['fname']." ".$row['mname']." ".$row['lname'];
        $array[4] = $row['Roommate1'];
        $array[5] = $row['Roommate2'];
        $array[6] = $TransferText;
        $array[7] = $ExtensionText;
		$array[8] = $row["special_land_text"];
		$array[9] = $row["Meals_Request"];
		$array[10] = $row["Allergies_Request"];
        
		$writer->writeSheetRow('Rooming', $array);
		$i++;
    };
	$writer->markMergedCell('Rooming', $start_row=0, $start_col=0, $end_row=0, $end_col=9);
