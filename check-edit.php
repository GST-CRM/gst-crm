<?php
include "session.php";
$PageTitle = "Edit Check";

if(isset($_GET['id']) && is_numeric($_GET['id'])) {
    $CheckID = mysqli_real_escape_string($db, $_GET["id"]);
} else {
    header("location: checks.php");die('Please go back to the main page.');
}

include "header.php";

$GetCheckDataSQL = "SELECT
	ch.Check_Number, 
	ch.Check_Customer_ID, 
    ch.Check_Bank_ID,
    chb.Bank_Name,
    chb.Bank_Address,
    chb.Bank_Transit,
    chb.Bank_Routing,
    chb.Bank_Account_Number,
    ch.Check_Amount, 
    ch.Check_Amount_Words, 
    ch.Check_Date, 
    ch.Check_Note, 
    ch.Check_Memo,
    ch.Check_Status,
    ch.Check_Printed,
    cc.Company_Name,
    cc.Company_Address,
    cc.Company_Address2,
    cc.Company_Phone,
    cc.Company_Email,
    cc.Company_Logo,
    ch.Check_Type,
    CASE
    	WHEN ch.Check_Type = 1 THEN (SELECT CONCAT(co.fname,' ',co.mname,' ',co.lname) FROM contacts co WHERE co.id=ch.Check_Customer_ID)
        WHEN ch.Check_Type = 2 THEN (SELECT ep.Payee_Name FROM expenses_payee ep WHERE ep.Payee_ID=ch.Check_Customer_ID)
        WHEN ch.Check_Type = 3 THEN (SELECT co.company FROM contacts co WHERE co.id=ch.Check_Customer_ID)
        WHEN ch.Check_Type = 4 THEN (SELECT co.company FROM contacts co WHERE co.id=ch.Check_Customer_ID)
    END AS Customer_Name,
    CASE
    	WHEN ch.Check_Type = 1 THEN (SELECT CONCAT(co.address1,', ', co.address2,'<br />',co.city,', ',co.state,', ',co.zipcode) FROM contacts co WHERE co.id=ch.Check_Customer_ID)
        WHEN ch.Check_Type = 2 THEN ''
        WHEN ch.Check_Type = 3 THEN (SELECT CONCAT(co.address1,', ', co.address2,'<br />',co.city,', ',co.state,', ',co.zipcode) FROM contacts co WHERE co.id=ch.Check_Customer_ID)
        WHEN ch.Check_Type = 4 THEN (SELECT CONCAT(co.address1,', ', co.address2,'<br />',co.city,', ',co.state,', ',co.zipcode) FROM contacts co WHERE co.id=ch.Check_Customer_ID)
    END AS Customer_Address
FROM checks ch
JOIN checks_bank chb 
ON chb.Bank_ID=ch.Check_Bank_ID
JOIN checks_company cc
ON cc.Company_ID=1
WHERE ch.Check_ID=$CheckID";
$CheckData = GDb::fetchRow($GetCheckDataSQL);


if($CheckData['Check_Type'] == 1) {
	$PageLabel = "Customer Check Information";
	$ContactLabel = "Customer Name";
} else {
	$PageLabel = "Expense Check Information";
	$ContactLabel = "Payee Name";
}
if($CheckData['Check_Status'] == 1) {
	$CheckStatusFlag = 1;
	$CheckStatusFlagClass = "";
	$CheckStatusFlagText = "";
} else {
	$CheckStatusFlag = 2;
	$CheckStatusFlagClass = "d-none";
	$CheckStatusFlagText = "<h3 align='center' class='text-danger mb-5'>This check has been voided.<br /><a class='btn waves-effect waves-light btn-danger' href='javascript:void(0);' onclick = \"deleteConfirm( 'checks.php?action=unvoid&id=".$CheckID."', 'Un-Void')\"><i class='fas fa-ban'> </i>Unvoid</a></h3>";
} 
?>

<style>
@font-face {
  font-family: MICRfont;
  src: url(files/assets/fonts/micrenc.ttf);
}
.select2-container--default .select2-selection--single .select2-selection__rendered {background:white;color:#333;padding:2px 30px 3px 20px;border-radius:4px;}
.select2-container--default .select2-selection--single .select2-selection__arrow {top:4px;}
.select2-container--default .select2-selection--single .select2-selection__arrow b {border-color:black transparent transparent transparent;}
.select2-selection {height:35px;padding:0px !important;}
</style>

<form name="CheckSetupForm" action="" method="POST" id="contact1">
<div class="row">
    <div class="col-sm-5">
        <div class="card">
            <div class="card-header pb-3">
                <h5><?php echo $PageLabel; ?></h5>
            </div>
            <div class="card-block row pt-0">
                <div class="form-group form-default form-static-label col-sm-8">
                    <label class="float-label">Bank Account</label>
                    <input type="hidden" name="EditCurrentCheck" value="<?php echo $CheckID; ?>">
                    <select id="TheBankSelection" name="BankID" class="form-control">
                        <?php $BankDataSQL = 'SELECT Bank_ID, Bank_Name, Bank_Address, Bank_Transit, Bank_Routing, Bank_Account_Number, Bank_Check_Number FROM checks_bank WHERE Bank_Type="bank" AND Bank_Status=1';
                        $BanksData = GDb::fetchRowSet($BankDataSQL);
                        foreach ($BanksData AS $BankData) {
                            if($BankData['Bank_ID'] == $CheckData['Check_Bank_ID']) { $Selected = 'selected'; } else { $Selected = ''; }
                            echo "<option value='".$BankData['Bank_ID']."' data-name='".$BankData['Bank_Name']."' data-address='".$BankData['Bank_Address']."' data-transit='".$BankData['Bank_Transit']."' data-routing='".$BankData['Bank_Routing']."' data-account='".$BankData['Bank_Account_Number']."' data-checknumber='".$BankData['Bank_Check_Number']."' $Selected>".$BankData['Bank_Name']."</option>";
                        } ?>
                    </select>
                </div>
                <div class="form-group form-default form-static-label col-sm-4">
                    <label class="float-label">Check Number</label>
                    <input type="number" name="CheckNum" id="CheckNum" oninput="CheckNumFunc()" value="<?php echo $CheckData['Check_Number']; ?>" class="form-control">
                </div>
                <div class="form-group form-default form-static-label col-sm-12">
                    <label class="float-label"><?php echo $ContactLabel; ?></label>
                    <input type="hidden" name="CustomerID" value="<?php echo $CheckData['Check_Customer_ID']; ?>" class="form-control" readonly>
                    <input type="text" name="CustomerName" value="<?php echo $CheckData['Customer_Name']; ?>" class="form-control" readonly>
                </div>
                <div class="form-group form-default form-static-label col-sm-6">
                    <label class="float-label">Check Amount</label>
                    <input type="text" name="CheckAmount" id="CheckAmount" value="<?php echo $CheckData['Check_Amount']; ?>" oninput="CheckAmountFunc()" class="form-control">
                    <input type="text" name="CheckAmountWordText" id="CheckAmountWordText" value="<?php echo $CheckData['Check_Amount_Words']; ?>" class="form-control" hidden>
                </div>
                <div class="form-group form-default form-static-label col-sm-6">
                    <label class="float-label">Check Date</label>
                    <input type="date" name="CheckDate" id="CheckDate" value="<?php echo $CheckData['Check_Date']; ?>" oninput="CheckDateFunc()" class="form-control">
                </div>
                <div class="form-group form-default form-static-label col-sm-12">
                    <label class="float-label">Check Memo</label>
                    <input type="text" name="CheckMemo" id="CheckMemo" value="<?php echo $CheckData['Check_Memo']; ?>" oninput="CheckMemoFunc()" class="form-control thresold-i" maxlength="50">
                </div>
                <div class="form-group form-default form-static-label col-sm-12">
                    <label class="float-label">Check Notes <i>(For GST staff only)</i></label>
                    <input type="text" name="CheckNotes" id="CheckNotes" value="<?php echo $CheckData['Check_Note']; ?>" class="form-control">
                </div>
                <div class="col-sm-12">
                    <hr />
                    <button type="submit" name="AddNewBankSubmit" class="btn waves-effect waves-light btn-success" style="margin-right:10px;" data-toggle="modal" data-target="#resultsmodal" style="margin-right:20px;" ><i class="far fa-check-circle"></i>Save</button>
                    <button type="button" onclick="window.location.href = 'check-banks.php'" class="btn waves-effect waves-light btn-inverse" style="margin-right:10px;"><i class="fas fa-eraser"> </i>Cancel</button>
                    <a class="btn waves-effect waves-light btn-danger" style="margin-right:10px;" href="javascript:void(0);" onclick = 'deleteConfirm( "checks.php?action=void&id=<?php echo $CheckID; ?>", "Void")'><i class="fas fa-ban"> </i>Void</a>
                    <a class="btn waves-effect waves-light btn-primary" href="inc/checks-print-pdf.php?checkid=<?php echo $CheckID; ?>" target="_blank"><i class="fas fa-print"> </i>Print</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-7">
        <div class="card">
            <div class="card-header">
                <h5>Preview</h5>
            </div>
			<?php echo $CheckStatusFlagText; ?>
            <div class="card-block row pt-0 no-gutters text-center <?php echo $CheckStatusFlagClass; ?>">
                <div class="col-12 row no-gutters p-3" style="background:#f9f9f9 url(files/assets/images/cream-pixels.png);border: solid 1px #333; border-radius: 15px;">
                    <div class="col-2" style="line-height:13px;">
                        <?php if($CheckData['Company_Logo'] != '') {
                            $LogoPath = "uploads/".$CheckData['Company_Logo'];
                        } else { 
                            $LogoPath = $crm_images."/logo-Placeholder.png";
                        } ?>
                        <img src="<?php echo $LogoPath; ?>" class="img-fluid" style="height:60px;" alt="Bank Check Company Photo" />
                    </div>
                    <div class="col-4" style="line-height:13px;">
                        <b><?php echo $CheckData['Company_Name']; ?></b>
                        <br />
                        <small>
                            <?php echo $CheckData['Company_Address']; ?>
                            <br />
                            <?php echo $CheckData['Company_Address2']; ?>
                            <br />
                            <?php echo $CheckData['Company_Phone']; ?>
                            <br />
                            <?php echo $CheckData['Company_Email']; ?>
                        </small>
                    </div>
                    <div class="col-4">
                        <b id="BankNameText"><?php echo $CheckData['Bank_Name']; ?></b><br /><small id="BankAddressText"><?php echo $CheckData['Bank_Address']; ?></small>
                    </div>
                    <div class="col-2 text-center">
                        <h4 class="mb-0" id="CheckNumText">No. <?php echo $CheckData['Check_Number']; ?></h4>
                        <h6 id="TransitCodeText"><?php if(!empty($CheckData['Bank_Transit'])) { echo $CheckData['Bank_Transit']; } ?></h6>
                    </div>
                    <div class="col-10"></div>
                    <div class="col-2 text-center pb-2"><p style="color:black">
                        <b>Date:</b> <span id="CheckDateText"><?php echo GUtils::clientDate($CheckData['Check_Date']); ?></span></p>
                    </div>
                    <div class="col-10 text-left" style="border-bottom:solid 2px #333;">
                        <b>Pay To The Order Of:</b> <span id="CustomerNameText"><?php echo $CheckData['Customer_Name']; ?></span>
                    </div>
                    <div class="col-2 text-center">
                        <h5 id="CheckAmountText"><?php echo GUtils::formatMoney($CheckData['Check_Amount']); ?></h5>
                    </div>
                    <div class="col-12 pt-2 text-left" id="CustomerAddressText">
                        <?php echo $CheckData['Customer_Address']; ?>
                    </div>
                    <div class="col-12 pt-2 text-left" style="border-bottom:solid 2px #333;" id="CheckAmountWords">
                        <b><?php echo $CheckData['Check_Amount_Words']; ?></b>
                    </div>
                    <div class="col-7 pt-3 text-left">
                        <b>Memo:</b> <u id="CheckMemoText"><?php echo $CheckData['Check_Memo']; ?></u>
                    </div>
                    <div class="col-5 pt-3" style="border-bottom:solid 2px #333;"></div>
                    <div class="col-12 text-center pt-2 pb-0">
                        <h4 class="mb-0" style="font-family:MICRfont;">C00<span id="CheckNum2Text"><?php echo $CheckData['Check_Number']; ?></span>C A<span id="RoutingNumText"><?php echo $CheckData['Bank_Routing']; ?></span>A <span id="AccountNumText"><?php echo $CheckData['Bank_Account_Number']; ?></span>C</h4>
                    </div>
                </div>
                <br/>
                <label class="mt-2" style="margin:auto;font-size:12px;color:#333;">*The info on this check is for sample purposes only.*</label>
            </div>
        </div>
		<label><i><?php if($CheckData['Check_Printed'] > 0) { echo "**This check has printed ".$CheckData['Check_Printed']." times."; } else { echo "**This check has been printed yet."; } ?></i></label>
    </div>
</div>
</form>


<?php /*<div class="modal fade" id="resultsmodal" tabindex="-1" role="dialog" style="top:50px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
                <div id="results" class="confirmation-message-result-area">
					<div class="upload-loader"></div>
					<p>Prcoessing...</p>
				</div>
            </div>
            <div class="modal-footer2">
                <button type="button" class="btn btn-success waves-effect waves-light" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>*/?>

<script>
var selection = document.getElementById("TheBankSelection");
selection.onchange = function(event){
    /*$('#CheckNum').removeAttr("disabled");*/
    $('#CustomerName').removeAttr("disabled");
    $('#CheckAmount').removeAttr("disabled");
    $('#CheckDate').removeAttr("disabled");
    $('#CheckMemo').removeAttr("disabled");
    $('#CheckNotes').removeAttr("disabled");
  var name = event.target.options[event.target.selectedIndex].dataset.name;
  var address = event.target.options[event.target.selectedIndex].dataset.address;
  var transit = event.target.options[event.target.selectedIndex].dataset.transit;
  var routing = event.target.options[event.target.selectedIndex].dataset.routing;
  var account = event.target.options[event.target.selectedIndex].dataset.account;
  var checknumber = event.target.options[event.target.selectedIndex].dataset.checknumber;
  document.getElementById("BankNameText").innerHTML = name;
  document.getElementById("BankAddressText").innerHTML = address;
  document.getElementById("TransitCodeText").innerHTML = transit;
  document.getElementById("RoutingNumText").innerHTML = routing;
  document.getElementById("AccountNumText").innerHTML = account;
  //document.getElementById("CheckNum").value = checknumber;
};
    
function CheckNumFunc() {
    var x = document.getElementById("CheckNum").value;
    document.getElementById("CheckNumText").innerHTML = x;
    document.getElementById("CheckNum2Text").innerHTML = x;
}
function CheckMemoFunc() {
    var x = document.getElementById("CheckMemo").value;
    document.getElementById("CheckMemoText").innerHTML = x;
}
function CheckAmountFunc() {
    var x = document.getElementById("CheckAmount").value;
    document.getElementById("CheckAmountText").innerHTML = formatMoney(x);
}
function CheckDateFunc() {
    var x = document.getElementById("CheckDate").value;
    var today  = new Date(x);
    document.getElementById("CheckDateText").innerHTML = today.toLocaleDateString("en-US");
}

function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = ''; words[1] = 'One'; words[2] = 'Two'; words[3] = 'Three'; words[4] = 'Four'; words[5] = 'Five'; words[6] = 'Six'; words[7] = 'Seven'; words[8] = 'Eight'; words[9] = 'Nine'; words[10] = 'Ten'; words[11] = 'Eleven'; words[12] = 'Twelve'; words[13] = 'Thirteen'; words[14] = 'Fourteen'; words[15] = 'Fifteen'; words[16] = 'Sixteen'; words[17] = 'Seventeen'; words[18] = 'Eighteen'; words[19] = 'Nineteen'; words[20] = 'Twenty'; words[30] = 'Thirty'; words[40] = 'Forty'; words[50] = 'Fifty'; words[60] = 'Sixty'; words[70] = 'Seventy'; words[80] = 'Eighty'; words[90] = 'Ninety';
    
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}
function ConvertToWords(n) {
    var nums = n.toString().split('.')
    var whole = convertNumberToWords(nums[0])
    if (nums.length == 2) {
        //var fraction = convertNumberToWords(nums[1])
        if(nums[1] > 0) { var decimalz = ' and ' + nums[1] + '/100' } else { var decimalz = '' }
        return whole + decimalz + '****';
    } else {
        return whole + '****';
    }
}

               
document.getElementById('CheckAmount').onkeyup = function () {
    document.getElementById('CheckAmountWords').innerHTML = ConvertToWords(document.getElementById('CheckAmount').value);
    document.getElementById('CheckAmountWordText').value = ConvertToWords(document.getElementById('CheckAmount').value);
};    
    
$(document).ready(function() {
	$("form").submit(function() {
		// Getting the form ID
		var  formID = $(this).attr('id');
		var formDetails = $('#'+formID);
		//To not let the form of uploading attachments included in this
		if (formID != 'contact6' && formID != 'idGroupEmailForm') {
			$.ajax({
				type: "POST",
				url: 'inc/checks-functions.php',
				data: formDetails.serialize(),
				success: function (data) {	
					// Inserting html into the result div
					$('#results').html(data);
					//$("form")[0].reset();
				},
				error: function(jqXHR, text, error){
				// Displaying if there are any errors
					$('#result').html(error);           
				}
			});
			return false;
		}
	});
});
    

//To create select2 function for the customers dropdown
$(document).ready(function(){
   $("#CustomerName").select2({
      ajax: {
        url: "inc/select2-customers.php",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
           return {
              searchTerm: params.term,
              ChecksFeature: 'active'
           };
        },
        processResults: function (response) {
           return {
              results: response
           };
        },
        cache: true
      }
   }).on('change', function(e) {
       // Access to full data
       document.getElementById("CustomerNameText").innerHTML = $(this).select2('data')[0].CustomerName;
       document.getElementById("CustomerAddressText").innerHTML = $(this).select2('data')[0].CustomerAddress;
   });
});

    
</script>


<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>