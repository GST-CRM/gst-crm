<?php
include "session.php";
$PageTitle = "Category Contacts";
include "header.php"; 

$CategoryID = $_GET['category'];

$CALLING_PAGE = 'categories-contacts.php?category=' . $CategoryID ;
include __DIR__ . '/inc/categories-action.php' ;


if ( ! isset($_GET['category'])) {
	echo "<meta http-equiv='refresh' content='0;categories.php'>";
	exit;
}

$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {die("Connection failed: " . $conn->connect_error);} 
$sql2 = "SELECT * FROM metadata WHERE id=$CategoryID";
$result2 = $conn->query($sql2);
$data = $result2->fetch_assoc();
$conn->close();

$status = $data['Status'] ;
?>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
		<div class="card">
            <div class="row">
				<div class="col-md-9">
					<div class="card-header table-card-header">
						<h5>List of All the Contacts</h5>
					</div>
				</div>
				<div class="col-md-3" style="margin-top:15px;">
					<a href="contacts-add.php" class="btn waves-effect waves-light btn-success" style="margin-right: 30px;float:right; padding: 3px 13px;"><i class="far fa-check-circle"></i>Add</a>
				</div>
				<div class="col-md-12 text-center">
						<h5>
						<!--<small>Category Name</small><br/>-->
                            <?php echo $data["meta"];?>
                            <?php if( $status == 0 ) { ?>
                            <small class="text-danger">(Category is not active)</small>                             
                            <a href="javascript:void(0);" onclick = 'showConfirm( "categories-contacts.php?action=activate&category=<?php echo $CategoryID; ?>", "Activate", "Please confirm category activation")' class="btn waves-effect waves-light btn-info" style="margin-right: 30px;float:right; padding: 3px 13px;"><i class="far fa-check-circle"></i>Activate</a>
                            <?php } ?>
                    </h5>
                        
				</div>
			</div>
            <div class="card-block">
				<div class="dt-responsive table-responsive">
                    <table id="basic-btn" class="table table-hover table-striped table-bordered nowrap" data-page-length="20">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Full Name</th>
                                <th>Phone</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th style="display:none;">Company</th>
                                <th style="display:none;">Type</th>
                                <th style="display:none;">Birthday</th>
                                <th style="display:none;">Address 1</th>
                                <th style="display:none;">Address 2</th>
                                <th style="display:none;">ZIP Code</th>
                                <th style="display:none;">City</th>
                                <th style="display:none;">State</th>
                                <th style="display:none;">Country</th>
                                <th style="display:none;">Martial Status</th>
                                <th style="display:none;">Gender</th>
                                <th style="display:none;">Language</th>
                            </tr>
                        </thead>
                        <tbody>
						<?php $sql = "select * from contacts WHERE affiliation=$CategoryID"; $result = $db->query($sql);
						if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
                            <tr>
                                <td><?php echo $row["id"]; ?></td>
                                <td><a href="contacts-edit.php?id=<?php echo $row["id"]; ?>&action=edit" style="color: #0000EE;">
								<?php echo $row["title"]." ".$row["fname"]." ".$row["mname"]." ".$row["lname"]; ?></a></td>
                                <td><?php echo $row["phone"]; ?></td>
                                <td><?php echo $row["mobile"]; ?></td>
                                <td><?php echo $row["email"]; ?></td>
                                <td style="display:none;"><?php echo $row["company"]; ?></td>
                                <td style="display:none;">
								<?php
								if (strpos($row["usertype"], '0') !== false) { echo " | contact";}
								if (strpos($row["usertype"], '1') !== false) { echo " | Agent";}
								if (strpos($row["usertype"], '2') !== false) { echo " | Supplier";}
								if (strpos($row["usertype"], '3') !== false) { echo " | Leader";}
								if (strpos($row["usertype"], '4') !== false) { echo " | Customer";}
								?>
								</td>
                                <td style="display:none;"><?php echo $row["birthday"]; ?></td>
                                <td style="display:none;"><?php echo $row["address1"]; ?></td>
                                <td style="display:none;"><?php echo $row["address2"]; ?></td>
                                <td style="display:none;"><?php echo $row["zipcode"]; ?></td>
                                <td style="display:none;"><?php echo $row["city"]; ?></td>
                                <td style="display:none;"><?php echo $row["state"]; ?></td>
                                <td style="display:none;"><?php echo $row["country"]; ?></td>
                                <td style="display:none;"><?php echo $row["martialstatus"]; ?></td>
                                <td style="display:none;"><?php echo $row["gender"]; ?></td>
                                <td style="display:none;"><?php echo $row["language"]; ?></td>
                            </tr>
						<?php }} ?>
						</tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<!-- [ page content ] end -->
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>