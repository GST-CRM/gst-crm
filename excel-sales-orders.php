<?php

$TheStatus = $_GET['status'];
$FromDate = $_GET['FromDate'];
$UntilDate = $_GET['UntilDate'];
$ShowGroups = $_GET['ShowGroups'];

$WhereFlags = "";

if($TheStatus == "active") {
	$WhereFlags .= " AND so.Sales_Order_Stop=0 ";
} elseif($TheStatus == "voided") { 
	$WhereFlags .= " AND so.Sales_Order_Stop=1 ";
} elseif($TheStatus == "all") { 
	$WhereFlags .= " AND so.Sales_Order_Stop=0 ";
}

if($FromDate != "0000-00-00") {
	$WhereFlags .= " AND so.Sales_Order_Date >= '$FromDate' ";
} else { 
	$WhereFlags .= " ";
}

if($UntilDate != "0000-00-00") {
	$WhereFlags .= " AND so.Sales_Order_Date <= '$UntilDate' ";
} else { 
	$WhereFlags .= " ";
}

if($ShowGroups == "show") {
	$WhereFlags .= " ";
} else { 
	$WhereFlags .= " AND so.Group_ID=0 ";
}

$query = "SELECT so.Sales_Order_Num,
		so.Sales_Order_Date,
		so.Customer_Account_Customer_ID,
		IF(ci.Customer_Invoice_Num > 0, 'Yes', 'Not Yet') as HasInvoice,
		co.title,
		co.fname,
		co.mname,
		co.lname,
		so.Total_Sale_Price AS SalesAmount,
		ci.Customer_Invoice_Num,
		so.Sales_Order_Stop,
		so.Group_ID
	FROM sales_order so
	JOIN contacts co 
		ON so.Customer_Account_Customer_ID=co.id
	LEFT JOIN customer_invoice ci
		ON ci.Customer_Order_Num=so.Sales_Order_Num
	WHERE 1 $WhereFlags
	GROUP BY so.Sales_Order_Num ";

    $result = $db->query($query); 

    $HeaderFile = array(
      'Sales Order #'=>'integer',
      'Sales Order Date'=>'MM/DD/YYYY',
      'Customer ID'=>'integer',
      'Customer Name'=>'string',
      'Has Invoice?'=>'string',
      'Sales Order Amount'=>'dollar',
      'Group ID'=>'integer'
    );
	
	$styles4 = array('auto_filter'=>true, 'font-style'=>'bold', 'fill'=>'#eee', 'halign'=>'center','border'=>'left,right,top,bottom');
    
    $stylesCancel = array('font-style'=>'bold', 'fill'=>'#eee', 'halign'=>'center','border'=>'left,right,top,bottom');
	
	$writer->setTitle($filetitle);
	$writer->setSubject($filesubject);
	$writer->setAuthor($fileauthor);
	$writer->setCompany($filecompany);
	$writer->setDescription($filedesc);
	
	$writer->writeSheetHeader('Sales Orders', $HeaderFile, $styles4);

	$TotalSalesOrdersAmount = 0;
	

    $array = array();

	while ($row=$result->fetch_assoc()) {
		$Sales_Order_Num = $row['Sales_Order_Num'];
		$Sales_Order_Date = $row['Sales_Order_Date'];
		$Customer_ID = $row['Customer_Account_Customer_ID'];
		$Customer_Name = $row['title']." ".$row['fname']." ".$row['mname']." ".$row['lname'];
		$HasInvoice = $row['HasInvoice'];
		$SalesAmount = $row['SalesAmount'];
		$TotalSalesOrdersAmount += $SalesAmount;
		$Group_ID = $row['Group_ID'];

		        
		$array[0] = $Sales_Order_Num;
		$array[1] = $Sales_Order_Date;		
        $array[2] = $Customer_ID;
        $array[3] = $Customer_Name;
        $array[4] = $HasInvoice;
        $array[5] = $SalesAmount;
        $array[6] = $Group_ID;
        
		$writer->writeSheetRow('Sales Orders', $array);
    };
    $FooterFile = array("","","","","",$TotalSalesOrdersAmount,"");
	$writer->writeSheetRow('Sales Orders', $FooterFile, $styles4);

//$writer->markMergedCell('Sales Orders', $start_row=0, $start_col=0, $end_row=0, $end_col=6);