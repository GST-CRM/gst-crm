<?php include "session.php";
$PageTitle = "Edit Supplier Bill";
include "header.php";

//To make several url options in the same page
$allowed = array('update', 'edit');
if ( ! isset($_GET['action'])) {header("location: bills.php");die('Please go back to the main page.');}
$action = $_GET['action'];
if ( ! in_array($action, $allowed)) {header("location: bills.php");die('Please go back to the main page.');}

$Supplier_Bill_Num = $_GET["id"];

//To collect the data of the above customer id, and show it in the fields below
$sql = "SELECT sb.*,gro.tourname,gro.tourid, supp.contactid,c.fname,c.lname,c.company, c.email, c.address1, c.address2, c.zipcode, c.city, c.state, c.country,
Bill_Amount as due, SUM(Supplier_Payment_Amount) as paid,sb.Due_Date FROM contacts c
INNER JOIN suppliers supp ON supp.contactid = c.id AND supp.status=1
LEFT JOIN suppliers_bill sb ON sb.Supplier_ID = supp.contactid
LEFT JOIN suppliers_payments sp ON sp.Supplier_ID = supp.contactid AND sp.Supplier_Bill_Num = sb.Supplier_Bill_Num AND sp.Status=1
LEFT JOIN purchase_order po
ON po.Purchase_Order_Num=sb.Purchase_Order_Num
LEFT JOIN groups gro 
ON gro.tourid=po.Group_ID
WHERE /*sb.Status IN (1,2) AND*/ sb.Supplier_Bill_Num='$Supplier_Bill_Num' /*GROUP BY sb.Supplier_Bill_Num*/";
$result = $db->query($sql);
$data = $result->fetch_assoc();

include 'inc/bills-functions.php';

?>
  <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />-->
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5>Edit the Supplier Bill Entry</h5>
            </div>
            <div class="card-block pt-0">
				<form name="contact5" action="" method="POST" class="row" id="contact5" enctype="multipart/form-data">
                    <div class="form-default form-static-label col-sm-4">
                        <label class="float-label gst-label">Supplier Company</label>
                        <input type="text" name="EditBill" value="<?php echo $Supplier_Bill_Num; ?>" hidden>
                        <a href="contacts-edit.php?id=<?php echo $data['contactid']; ?>&action=edit&usertype=supplier">
							<input class="form-control" type="text" value="<?php echo $data['company']; ?>" style="cursor:pointer;" readonly>
						</a>
                    </div>
                    <div class="form-default form-static-label col-sm-4">
                        <label class="float-label gst-label">Group Reference</label>
						<?php if($data['tourid'] == 0) { ?>
							<input class="form-control" type="text" value="This expense isn't assigned to a group" readonly>
						<?php } else { ?>
                        <a href="groups-edit.php?id=<?php echo $data['tourid']; ?>&action=edit">
							<input class="form-control" type="text" value="<?php echo $data['tourname']; ?>" style="cursor:pointer;" readonly>
						</a>
						<?php } ?>
                    </div>
                    <div class="form-default form-static-label col-sm-2 text-center">
					<?php if($data['Status']==0) { echo "<h1><i class='text-danger fas fa-exclamation-triangle'></i></h1>";} ?>
                    </div>
                    <div class="form-default form-static-label col-sm-2 text-center">
                        <h5>Amount</h5>
                        <h3 class="text-danger" id="SupplierBillTotal"><?php echo GUtils::formatMoney($data['due']-$data['paid']); ?></h3>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-2">
                        <label class="float-label gst-label">Supplier Name</label>
                        <a href="contacts-edit.php?id=<?php echo $data['contactid']; ?>&action=edit&usertype=supplier">
							<input class="form-control" type="text" value="<?php echo $data['fname']." ".$data['lname']; ?>" style="cursor:pointer;" readonly>
						</a>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-2">
                        <label class="float-label gst-label">Bill Date</label>
                        <input type="date" name="Bill_Date" value="<?php echo $data['Supplier_Bill_Date']; ?>" class="form-control date-picker">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-2">
                        <label class="float-label gst-label" >Due Date</label>
                        <input type="date" name="Due_Date" value="<?php echo $data['Due_Date']; ?>" class="form-control">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-2">
					<style>.select2-selection {height:35px;padding:0px !important;}</style>
                        <label class="float-label gst-label">Terms</label>
                        <select name="Terms" class="form-control">
                            <option value="">Select</option>
                                <option value="Bank Transfer" <?php if($data['Payment_Method'] == "Bank Transfer") { echo "selected";} ?>>Net 7</option>
                                <option value="Cash" <?php if($data['Payment_Method'] == "Cash") { echo "selected";} ?>>Net 30</option>
                                <option value="Check" <?php if($data['Payment_Method'] == "Check") { echo "selected";} ?>>Net 90</option>
                                <option value="Card" <?php if($data['Payment_Method'] == "Card") { echo "selected";} ?>>Cash on Delivery</option>
                        </select>
                    </div>
                    <div class="form-default form-static-label col-sm-2 text-center">
					<?php if($data['Status']==0) { echo "<h5 class='text-danger'>This record is</h5><h3 class='text-danger'>Voided</h3>";} ?>
                    </div>
                    <div class="form-default form-static-label col-sm-2 text-center">
					<?php if($data['Status']==0) { ?>
					<a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick = 'deleteConfirm( "bills.php?action=unvoid&bill_id=<?php echo $Supplier_Bill_Num; ?>", "Unvoid")' role="button">Unvoid</a>
					<?php } else { ?>
					<a class="btn btn-primary btn-sm" href="javascript:void(0);" onclick = 'deleteConfirm( "bills.php?action=void&bill_id=<?php echo $Supplier_Bill_Num; ?>", "Void")' role="button">Void</a>
					<a class="btn btn-success btn-sm" href="suppliers-bill-payments-add.php?bill=<?php echo $Supplier_Bill_Num; ?>">Send A Payment</a>
					<?php } ?>
                    </div>
                    <div class="col-sm-12"><br /></div>
                    <div class="col-sm-12">
					<h4 class="sub-title">Supplier Bill Details</h4>
						<table class="table table-hover table-striped table-bordered nowrap" id="item_table">
							<thead>
								<tr>
									<th width="30" class="text-center">#</th>
									<th width="20%">Product / Service</th>
									<th>Description</th>
									<th width="100">Quantity</th>
									<th width="150">Amount</th>
									<th width="150">Total</th>
									<th width="50"><button type="button" name="add" class="btn btn-success btn-sm add"><i class="fas fa-plus mr-0"></i></button></th>
								</tr>
							</thead>
							<?php
							$sql = "SELECT * from suppliers_bill_line WHERE Supplier_Bill_Num=$Supplier_Bill_Num";
							$result = $db->query($sql);
							$BillLine = 1;

							//To make the list of products in PHP Variable
							$ProductSelect = '';
							$ProductSelect .= '<option value="0">Select a Product</option>';
							$SupplierID = $data['contactid'];
							$ProductsListResult = mysqli_query($db,"SELECT * FROM products WHERE supplierid=$SupplierID AND status=1");
							while($SupplierProducts = mysqli_fetch_array($ProductsListResult)) {
								$ProductName = str_replace("'"," ",$SupplierProducts['name']);
								$ProductDescription = str_replace("'"," ",$SupplierProducts['description']);
								$ProductSelect .='<option value="'.$SupplierProducts["id"].'" data-description="' .$ProductDescription. '" data-cost="' . $SupplierProducts['cost'] . '" data-quantity="0" data-category="' . $SupplierProducts['categoryid'] . '" data-name="' .$ProductName. '">'.$ProductName.'</option>';
							}


							if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
							<tr class='bill-line-tr'>
								<td class="text-center">
									<?php echo $BillLine; ?>
									<input type="hidden" name="exp_id[]" class="form-control exp_id" value="<?php echo $BillLine; ?>" />
								</td>
								<td>
									<select name="Bill_Product[]" class="form-control Bill_Line_Product" onchange="BillLineSelected(this)">
										<?php
											$ProductID = $row['Product_Product_ID'];
											$ProductsListResult = mysqli_query($db,"SELECT * FROM products WHERE id=$ProductID AND status=1");
											while($SupplierProducts = mysqli_fetch_array($ProductsListResult))
											{
													echo '<option value="'.$SupplierProducts["id"].'" data-description="' . $SupplierProducts['description'] . '" data-cost="' . $SupplierProducts['cost'] . '" data-quantity="' . $row['Qty'] . '">'.$SupplierProducts["name"].'</option>';
													$ProductName = $SupplierProducts['name'];
													$ProductDescription = $SupplierProducts['description'];
													$ProductCategory = $SupplierProducts['categoryid'];
											}
										?>
									</select>
								</td>
								<td>
									<span class="product_desc"><?php echo $ProductDescription; ?></span>
								</td>
								<td><input type="number" onchange="Bill_Line_Qty_Change(this)" name="Bill_Line_Quantity[]" class="form-control bill_line_quantity" value="<?php echo $row["Qty"]; ?>" /></td>
								<td><span class="product_cost"><?php echo GUtils::formatMoney($row["Bill_Line_Total"]); ?></span></td>
								<td><span class="bill_line_total" data-cost="<?php echo ($row["Qty"]*$row["Bill_Line_Total"]); ?>"><?php echo GUtils::formatMoney($row["Qty"]*$row["Bill_Line_Total"]); ?></span></td>
								<td><button type="button" name="remove" class="btn btn-danger btn-sm remove" onclick = "ConfirmRowDelete( 'DeleteBillRow(<?php echo $BillLine; ?>)', 'Delete')"><i class="fas fa-trash-alt mr-0"></i></button></td>
								<td style="display:none;">
									<input type="text" class="product_name" name="product_name[]" value="<?php echo $ProductName; ?>">
									<input type="text" class="product_cost" name="product_cost[]" value="<?php echo $row["Bill_Line_Total"]; ?>">
									<input type="text" class="product_category" name="product_category[]" value="<?php echo $ProductCategory; ?>">
								</td>
							</tr>
							<?php	$BillLine = ++$BillLine;
							}} ?>
							<tfoot>
								<tr>
									<th colspan="5" class="pt-2 pb-2 text-right">Total</th>
									<th colspan="2" class="pt-2 pb-2 FooterTotal"><?php echo GUtils::formatMoney($data["due"]); ?></th>
								</tr>
							</tfoot>
						</table>
					</div>
                    <div class="col-sm-12"><br /><br /></div>
					<?php if($data['paid'] > 0) { ?>
                    <div class="col-sm-12">
					<h4 class="sub-title">Payments List assigned to this Bill</h4>
						<table class="table table-hover table-striped table-bordered nowrap" id="item_table2">
							<thead>
								<tr>
									<th width="30" class="text-center pt-1 pb-1">#</th>
									<th class="pt-1 pb-1" width="150">Payment Date</th>
									<th class="pt-1 pb-1" width="150">Payment Type</th>
									<th class="pt-1 pb-1">Payment Notes</th>
									<th class="pt-1 pb-1" width="215">Amount</th>
								</tr>
							</thead>
							<?php
							$PaymentSQL = "SELECT * from suppliers_payments WHERE Supplier_Bill_Num=$Supplier_Bill_Num AND Status=1";
							$PaymentResult = $db->query($PaymentSQL);
							if ($PaymentResult->num_rows > 0) { while($PaymentData = $PaymentResult->fetch_assoc()) { ?>
							<tr class='bill-line-tr'>
								<td class="text-center">
									<a style="color:blue;font-weight:bold;" href="suppliers-bill-payments-add.php?payment=<?php echo $PaymentData['Supplier_Payment_ID']; ?>"><?php echo $PaymentData['Supplier_Payment_ID']; ?></a>
								</td>
								<td><?php echo $PaymentData['Supplier_Payment_Date']; ?></td>
								<td><?php echo $PaymentData['Supplier_Payment_Method']; ?></td>
								<td><?php echo $PaymentData['Supplier_Payment_Comments']; ?></td>
								<td><a style="color:blue;" href="suppliers-bill-payments-add.php?payment=<?php echo $PaymentData['Supplier_Payment_ID']; ?>"><?php echo GUtils::formatMoney($PaymentData["Supplier_Payment_Amount"]); ?></a></td>
							</tr>
							<?php }} ?>
							<tfoot>
								<tr>
									<th colspan="4" class="pt-2 pb-2 text-right">Paid so Far</th>
									<th class="pt-2 pb-2 FooterTotal"><?php echo GUtils::formatMoney($data["paid"]); ?></th>
								</tr>
								<tr>
									<th colspan="4" class="pt-2 pb-2 text-right">Remaining Balance</th>
									<th class="pt-2 pb-2 FooterTotal"><?php echo GUtils::formatMoney($data["due"]-$data["paid"]); ?></th>
								</tr>
							</tfoot>
						</table>
					</div>
                    <div class="col-sm-12"><br /></div>
					<?php } ?>
					<div class="form-group form-default form-static-label col-md-4">
                        <label class="float-label">Supplier Bill Comments</label>
                        <textarea name="Bill_Comments" class="form-control" rows="6"><?php echo $data["Comments"]; ?></textarea>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label gst-label" for="SupplierBill_File">Attachment <?php if($data["Attachment"] != NULL) { echo "<a href='uploads/".$data['Attachment']."' target='_blank' style='margin-left:100px;' download><i class='fas fa-cloud-download-alt'></i> <small>Download the attachment</small></a>"; } ?></label>
                        <div class="Attachment_Box">
                            <input <?php echo $DisabledFlag; ?> type="file" id="SupplierBill_File" name="fileToUpload" class="Attachment_Input" >
                            <p id="SupplierBill_File_Text" class="Attachment_Text">Drag the file here or click in this area.</p>
                        </div>
                    </div>
                    <div class="col-sm-12"><br /><br />
						<button type="submit" name="addcontact" value="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Edit Bill</button>
                        <button type="reset" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Clear</button>
					</div>
				</form>
			</div>
        </div>
    </div>
</div>
<script type="text/javascript">
//To make the numbers as a currency with two decimels
const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 2
})

//This is to make the attachment box update its content when a file is selected
$(document).ready(function(){
  $('#SupplierBill_File').change(function () {
    $('#SupplierBill_File_Text').text("A file has been selected");
  });
});


function BillLineSelected(obj) {
	var val = $(obj).val();
	var description = $(obj).find("option[value='" + val + "']").data('description');
	var cost = $(obj).find("option[value='" + val + "']").data('cost');
	var quantity = $(obj).find("option[value='" + val + "']").data('quantity');
	var name = $(obj).find("option[value='" + val + "']").data('name');
	var category = $(obj).find("option[value='" + val + "']").data('category');
	$(obj).closest('.bill-line-tr').find('.product_desc').html(description);
	$(obj).closest('.bill-line-tr').find('.product_cost').html(formatMoney(cost));
	$(obj).closest('.bill-line-tr').find('.product_cost').val(cost);
	$(obj).closest('.bill-line-tr').find('.product_name').val(name);
	$(obj).closest('.bill-line-tr').find('.product_category').val(category);
	$(obj).closest('.bill-line-tr').find('.bill_line_quantity').val(quantity);
	var BillLineTotal = quantity * cost;
	$(obj).closest('.bill-line-tr').find('.bill_line_total').html(formatMoney(BillLineTotal));
	$(sel).closest('.bill-line-tr').find('.bill_line_total').data('cost', BillLineTotal);

	Bill_Line_Qty_Change($(obj).closest('.bill-line-tr').find('.bill_line_quantity'));
}

function Bill_Line_Qty_Change(textobj) {
	var qty = $(textobj).val();
	var sel = $(textobj).closest('.bill-line-tr').find('.Bill_Line_Product');
	var val = $(sel).val();

	var cost = $(sel).find("option[value='" + val + "']").data('cost');
	$(sel).closest('.bill-line-tr').find('.product_cost').html(formatMoney(cost));
	var TotalLineCost = cost * qty;

	$(sel).closest('.bill-line-tr').find('.bill_line_total').html(formatMoney(TotalLineCost));
	$(sel).closest('.bill-line-tr').find('.bill_line_total').data('cost', TotalLineCost);

	updateTotal();
}

function updateTotal() {
	var total = 0;

	$('.bill-line-tr .bill_line_total').each(function () {
		total += parseFloat($(this).data('cost'));
	});
	var PaidSoFar = <?php if($data['paid'] > 0) {echo $data['paid'];} else {echo "0";} ?>;
	var FinalAmount = total - PaidSoFar;
	$('#SupplierBillTotal').html(formatMoney(FinalAmount));
	$('.FooterTotal').html(formatMoney(total));

}

function ConfirmRowDelete(url, button, msg, title) {
    if( button == undefined || button.length < 1) {
        button = 'OK' ;
    }
    if( msg != undefined && msg.length > 0 ) {
        $('#deleteConfirm .delete-msg').html(msg) ;
    }
    if( title != undefined && title.length > 0 ) {
        $('#deleteConfirm .delete-title').html(title) ;
    }

    $('#deleteConfirm .delete-target').attr('onclick', url) ;
    $('#deleteConfirm .delete-target').html(button) ;
    $('#deleteConfirm').modal('show');
}

function DeleteBillRow(Numbaarr) {
	document.getElementById("item_table").deleteRow(Numbaarr);
    $('#deleteConfirm').modal('hide');
	updateTotal();
}

//To be able to add new rows or remove old ones
$(document).ready(function(){
	var TDnum = <?php echo $BillLine; ?>;
	$(document).on('click', '.add', function(){
		var html = '';
		html += '<tr class="bill-line-tr">';
		html += '<td class="text-center">'+ TDnum +'<input type="hidden" name="exp_id[]" class="form-control exp_id" value="'+ TDnum +'" /></td>';
		html += '<td><select name="Bill_Product[]" class="form-control Bill_Line_Product" onchange="BillLineSelected(this)">';
		html += '<?php echo $ProductSelect; ?>';
		html += '</select></td>';
		html += '<td><span class="product_desc"></span></td>';
		html += '<td><input type="number" onchange="Bill_Line_Qty_Change(this)" name="Bill_Line_Quantity[]" class="form-control bill_line_quantity" value="" /></td>';
		html += '<td><span class="product_cost"></td>';
		html += '<td><span class="bill_line_total"></span></td>';
		html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm remove" onclick = "ConfirmRowDelete( \'DeleteBillRow('+ TDnum +')\', \'Delete\')"><i class="fas fa-trash-alt mr-0"></i></button></td>';
		html += '<td style="display:none;"><input type="text" class="product_name" name="product_name[]" value="">';
		html += '<input type="text" class="product_cost" name="product_cost[]" value="">';
		html += '<input type="text" class="product_category" name="product_category[]" value="">';
		html += '</td></tr>';
		$('#item_table').append(html);
	TDnum++;
	});
	
	/*$(document).on('click', '.remove', function(){
		$(this).closest('tr').remove();
	}); */
});
</script>
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>