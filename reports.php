<?php include "session.php";
$PageTitle = "Reports";
include "header.php";
?>
    <div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <h5>Reports</h5>
                </div>
				<div class="card-block row">
					<div class="col-md-4">
						<h5 style="border-left: solid 3px #17a2b8;padding-left:10px;">Customers</h5>
						<br />
						<ul class="list-group">
                            <li class="list-group-item list-group-item-action"><a href="reports-AR.php">Account Receivable</a></li>
                            <li class="list-group-item list-group-item-action"><a href="reports-Recent.php">Recent Registrations</a></li>
                            <li class="list-group-item list-group-item-action"><a href="reports-Deposit.php">Deposit Report</a></li>
                            <li class="list-group-item list-group-item-action"><a href="">Another Report 2</a></li>
                            <li class="list-group-item list-group-item-action"><a href="">Another Report 3</a></li>
						</ul>
					</div>
					<div class="col-md-4">
						<h5 style="border-left: solid 3px #17a2b8;padding-left:10px;">Supplier</h5>
						<br />
						<ul class="list-group">
							<li class="list-group-item list-group-item-action"><a href="reports-SP.php">Supplier Payable</a></li>
                            <li class="list-group-item list-group-item-action"><a href="">Another Report 1</a></li>
                            <li class="list-group-item list-group-item-action"><a href="">Another Report 2</a></li>
                            <li class="list-group-item list-group-item-action"><a href="">Another Report 3</a></li>
                            <li class="list-group-item list-group-item-action"><a href="">Another Report 4</a></li>
						</ul>
					</div>
                    <?php /*
					<div class="col-md-4">
						<h5 style="border-left: solid 3px #17a2b8;padding-left:10px;">Other</h5>
						<br />
						<ul class="list-group">
							<li class="list-group-item list-group-item-action">Bank Deposit</li>
							<li class="list-group-item list-group-item-action">Transfer</li>
							<li class="list-group-item list-group-item-action">Payroll</li>
							<li class="list-group-item list-group-item-action">Journal Entry</li>
							<li class="list-group-item list-group-item-action">Estimate\Quotes</li>
						</ul>
					</div>
                     * 
                     */?>
				</div>
				<br />
            </div>
        </div>
		<div class="col-md-1"></div>
	</div>
<?php include "footer.php"; ?>