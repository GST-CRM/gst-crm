<?php

if(isset($_GET['c'])) { //To activate the "c" as Customer Access Screens
    $IsCustomer = 1;
    if(isset($_GET['route']) && $_GET['route'] != 'group') {
        $Route = $_GET['route'];
        $DetailsTabActive = $SalesTabActive = $PaymentsTabActive = '';
        if($Route == 'details') { $DetailsTabActive = "active"; }
        elseif($Route == 'invoices') { $SalesTabActive = "active"; }
        elseif($Route == 'payments') { $PaymentsTabActive = "active"; }
        include 'customer/customer-details.php';
    }
    if(isset($_GET['route']) && $_GET['route'] == 'group') {
        $Route = $_GET['route'];
        $GroupID = $_GET['GroupID'];
        $DetailsTabActive = $SalesTabActive = $PaymentsTabActive = '';
        if($Route == 'details') { $DetailsTabActive = "active"; }
        elseif($Route == 'invoices') { $SalesTabActive = "active"; }
        elseif($Route == 'payments') { $PaymentsTabActive = "active"; }
        include 'customer/customer-groups.php';
    }
} else { die('There is nothing else to do here.'); }

?>