<?php include "session.php";
$PageTitle = "Add General Expense";
include "header.php";
include 'inc/expenses-functions.php';

if(isset($_GET['TourExpense'])) {
    $TourID_Expense = mysqli_real_escape_string($db, $_GET['TourExpense']);
} else {
    $TourID_Expense = "";
}


?>
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />-->
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->

<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {
	background-color: transparent !important;
	padding: 6px !important;
	color: #333 !important;
}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-block">
				<form name="contact5" action="" method="POST" class="row" id="contact5" enctype="multipart/form-data">
                    <div class="form-default form-static-label col-sm-3">
						<div id="CurrentPayeeBox">
							<label class="float-label gst-label">Select A Payee</label>
                            <input type="text" name="NewExpense" value="new" hidden>
							<input type="hidden" id="submitBtn" name="submitBtn" value="" >
							<select name="PayeeReference" class="js-example-tags col-sm-12">
								<option value="0">Select a Payee</option>
								<?php
								$PayeeResult = mysqli_query($db,"SELECT Payee_ID,Payee_Name FROM expenses_payee WHERE Status=1");
								while($PayeeData = mysqli_fetch_array($PayeeResult)) {
									if($PayeeData['Payee_ID'] == $_POST['PayeeReference']) { $SelectedPayee = "selected";} else {$SelectedPayee = ""; }
									echo "<option value='".$PayeeData['Payee_ID']."' $SelectedPayee>".$PayeeData['Payee_Name']."</option>";
								}
								?>
							</select>
						</div>
                    </div>
                    <div class="form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Group Reference</label>
                        <select name="GroupReference" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
                            <option value='0'>This expense isn't assigned to a group</option>
						<?php
							$ToursResult = mysqli_query($db,"SELECT tourid,tourname FROM groups WHERE Status=1");
							while($TourData = mysqli_fetch_array($ToursResult))
							{
								if($TourData['tourid'] == $_POST['GroupReference']) { $SelectedTour = "selected";} else {$SelectedTour = ""; }
								if($TourID_Expense > 0 AND $TourData['tourid'] == $TourID_Expense) { $SelectedTour2 = "selected";} else {$SelectedTour2 = ""; }
								echo "<option value='".$TourData['tourid']."' $SelectedTour $SelectedTour2>".$TourData['tourname']."</option>";
							}
						?>
                        </select>
                    </div>
                    <div class="form-default form-static-label col-sm-3 text-center">
                    </div>
                    <div class="form-default form-static-label col-sm-3 text-center">
                        <h5>Amount</h5>
                        <h3 class="text-danger" id="ExpensesTotalText">$0.00</h3>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Expense Date</label>
                        <input type="date" name="ExpenseDate" id='id_invoice_date' class="form-control date-picker" value="<?php if($_POST['ExpenseDate'] != NULL) {echo $_POST['ExpenseDate'];} else { echo GUtils::mysqlDate( Date('Y-m-d') );} ?>">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label" >Due Date</label>
                        <input type="date" name="due_date" id="id_due_date" class="form-control">
                    </div>
                    <div class="col-sm-12"><br /></div>
                    <div class="col-sm-12 table-responsive">
						<table class="table table-hover table-striped table-bordered nowrap" id="item_table">
							<thead>
								<tr>
									<th width="30" class="text-center">#</th>
									<th width="20%">Expense Category</th>
									<th>Expense Description</th>
									<th width="20%">Expense Amount</th>
									<th width="50"><button type="button" name="add" class="btn btn-success btn-sm add"><i class="fas fa-plus mr-0"></i></button></th>
								</tr>
							</thead>
							<tr class="expensesLines">
								<td class="text-center">
									1
									<input type="hidden" name="exp_id[]" class="form-control exp_id" value="<?php echo $ExpenseLine; ?>" />
								</td>
								<td>
									<select name="exp_category[]" class="form-control exp_category" onchange="ChartOfAccountSelect(this)">
										<?php
											$ExpensesSelect = "";
											$GLaccountResult = mysqli_query($db,"SELECT GL_Group_Num,GL_Group_Name FROM gl_account_groups WHERE GL_Group_Status=1 AND GL_Account_Classes_Class_ID=6");
											while($GLaccountData = mysqli_fetch_array($GLaccountResult))
											{
												$ExpensesSelect .='<option value="'.$GLaccountData["GL_Group_Num"].'">'.$GLaccountData["GL_Group_Name"].'</option>';
											}
										?>
										<option value="">Select Category</option>
										<?php echo $ExpensesSelect; ?>
									</select>
								</td>
								<td>
									<select name="exp_desc[]" class="form-control exp_desc">
										<option value="">Select the main category first</option>
									</select>
								</td>
								<td>
									<div class="dollar"><input type="text" onblur="findTotal()" name="exp_amount[]" onchange="ExpenseLineAmount(this)" class="form-control exp_amount" /></div>
								</td>
								<td>
									<button type="button" name="remove" class="btn btn-danger btn-sm remove"><i class="fas fa-trash-alt mr-0"></i></button>
								</td>
							</tr>
						</table>
					</div>
                    <div class="col-sm-12"><br /></div>
					<div class="form-group form-default form-static-label col-md-4">
                        <label class="float-label">Expense Comments</label>
                        <textarea name="ExpensesNotes" class="form-control" rows="6"></textarea>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label gst-label" for="Expenses_Attachment">Attachment</label>
                        <div class="Attachment_Box">
                            <input type="file" id="Expenses_Attachment" name="fileToUpload" class="Attachment_Input" >
                            <p id="Expenses_Attachment_Text" class="Attachment_Text">Drag the file here or click in this area.</p>
                        </div>
                    </div>
					<div class="col-md-12"><br /><br />
						<div class="btn-group mr-2">
							<button type="submit" value="save-back" name="submit" class="btn btn-success" data-toggle="modal" data-target="#resultsmodal" data-val="save_back"><i class="far fa-check-circle"></i> Save & View the Record</button>
							<button type="button" class="btn btn-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="sr-only">Toggle Dropdown</span>
							</button>
							<div class="dropdown-menu">
								<button type="submit" value="save-group" name="submit" class="dropdown-item" data-toggle="modal" data-target="#resultsmodal" data-val="save_group">Save & back to Group</button>
								<button type="submit" value="save-new" name="submit" class="dropdown-item" data-toggle="modal" data-target="#resultsmodal" data-val="save_new">Save & New</button>
								<button type="submit" value="save" name="submit" class="dropdown-item" data-toggle="modal" data-target="#resultsmodal" data-val="save_close">Save & Close</button>
							</div>
						</div>
						<button type="button" onclick="window.onbeforeunload = null; window.location.href = 'expenses.php'" class="btn waves-effect waves-light btn-inverse mr-2"><i class="fas fa-ban"></i>Cancel</button>
					</div>
				</form>
			</div>
        </div>
    </div>
</div>
<script type="text/javascript">
//To make the numbers as a currency with two decimels
const formatter = new Intl.NumberFormat('en-US', {
	style: 'currency',
	currency: 'USD',
	minimumFractionDigits: 2
})

//This is to make the attachment box update its content when a file is selected
$(document).ready(function(){
  $('#Expenses_Attachment').change(function () {
    $('#Expenses_Attachment_Text').text("A file has been selected");
  });
});

//To generate the total amount for the expense in the right top corner
function findTotal(){
	//To get each expense line amount
	var SingleExpenseAmt = document.getElementsByName('exp_amount[]');
	var TotalExpenses=0;
	for(var i=0;i<SingleExpenseAmt.length;i++){
		if(parseFloat(SingleExpenseAmt[i].value))
			TotalExpenses += parseFloat(SingleExpenseAmt[i].value);
	}
	//Format the numbers to be with 2 decimals
	TotalExpenses = formatter.format(TotalExpenses);
    //And then just show them on the fields listed below
	document.getElementById('ExpensesTotalText').innerHTML = TotalExpenses;
}

//To be able to add new rows or remove old ones
$(document).ready(function(){
    var TDnum = 2;
    $(document).on('click', '.add', function(){
        var html = '';
        html += '<tr class="expensesLines">';
        html += '<td class="text-center">' + TDnum + '</td>';
        html += '<td><select name="exp_category[]" class="form-control exp_category" onchange="ChartOfAccountSelect(this)"><option value="">Select Category</option><?php echo $ExpensesSelect; ?></select></td>';
        html += '<td><select name="exp_desc[]" class="form-control exp_desc"><option value="">Select the main category first</option></select></td>';
        html += '<td><div class="dollar"><input type="text" onblur="findTotal()" name="exp_amount[]" class="form-control exp_amount"></div></td>';
        html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm remove"><i class="fas fa-trash-alt mr-0"></i></button></td></tr>';
        $('#item_table').append(html);
        TDnum++;
    });

    $(document).on('click', '.remove', function(){
    $(this).closest('tr').remove();
    }); 


    $('.btn').click(function() {
          var buttonval    = $(this).attr('data-val');
          $("#submitBtn").val(buttonval);
    })
});

function ChartOfAccountSelect(obj) {
		$(obj).closest('.expensesLines').find('.exp_desc').load("inc/expenses-categories.php?choice=" + $(obj).closest('.expensesLines').find('.exp_category').val());
}
</script>
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>

<script type="text/javascript">
//To make the dropdown for the Payee the ability of adding a new payee directly
$(".js-example-tags").select2({
	tags: true,
	createTag: function (params) {
		var term = $.trim(params.term);

		if (term === '') {
			return null;
		}
		return {
			id: term,
			text: term
		}
	}
});
</script>