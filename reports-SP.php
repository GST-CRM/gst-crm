<?php
include "session.php";
include_once "inc/helpers.php";

$PageTitle = "Supplier Payments";
include "header.php";

$v_Input_Date = GUtils::mysqlDate( Date('Y-m-d') );


$SuppliersListSQL  = "SELECT co.id,co.company,co.fname,co.mname,co.lname FROM suppliers sup JOIN contacts co on sup.contactid=co.id ORDER BY co.fname, co.mname, co.lname";
$SuppliersList = GDb::fetchRowSet($SuppliersListSQL);


if( isset($__REQUEST['submit']) ) {
	if( isset($__REQUEST['SupplierBill_Date']) ) {
		$v_Input_Date = GUtils::mysqlDate( $__REQUEST['SupplierBill_Date'] ) ;
	}
	if( isset($__REQUEST['SupplierID']) ) {
		$v_Input_Supplier = $__REQUEST['SupplierID'];
		$WhereClause = " AND sb.Supplier_ID=$v_Input_Supplier ";
	} else { $WhereClause = ""; }
	
$sql = "SELECT
sb.Supplier_ID,
CONCAT(c.title, ' ',c.fname,' ',c.mname,' ',c.lname) AS Supplier_Name,
s.Description,
sb.Supplier_Bill_Num,
sb.Supplier_Bill_Date,
sb.Bill_Amount,
sb.Due_Date,
IFNULL( payments.Supplier_Payment, 0 ) AS Supplier_Payment_Amount,
(IFNULL(Bill_Amount - payments.Supplier_Payment ,0)) *-1 AS Total_Open,
DATEDIFF(sb.Due_Date, '$v_Input_Date') AS Days_Due,
CASE WHEN sb.Due_Date < '$v_Input_Date' THEN (Bill_Amount - IFNULL(payments.Supplier_Payment,0)) ELSE 0 END AS Past_Due,
CASE WHEN DATEDIFF(sb.Due_Date, '$v_Input_Date') BETWEEN -999 AND 0 THEN (Bill_Amount - IFNULL(payments.Supplier_Payment,0)) ELSE 0 END AS Current_,
CASE WHEN DATEDIFF(sb.Due_Date, '$v_Input_Date') BETWEEN 1 AND 30 THEN (Bill_Amount - IFNULL(payments.Supplier_Payment,0)) ELSE 0 END AS Past_Due1_30,
CASE WHEN DATEDIFF(sb.Due_Date, '$v_Input_Date') BETWEEN 31 AND 60 THEN (Bill_Amount - IFNULL(payments.Supplier_Payment,0)) ELSE 0 END AS Past_Due31_60,
CASE WHEN DATEDIFF(sb.Due_Date, '$v_Input_Date') BETWEEN 61 AND 90 THEN (Bill_Amount - IFNULL(payments.Supplier_Payment,0)) ELSE 0 END AS Past_Due61_90,
CASE WHEN DATEDIFF(sb.Due_Date, '$v_Input_Date') >= 91 THEN (Bill_Amount - IFNULL(payments.Supplier_Payment,0)) ELSE 0 END AS Past_Due_over_90
FROM suppliers_bill sb
Join contacts c on c.id = sb.Supplier_ID
And c.usertype = 2
join suppliers s on
c.id = s.contactid
LEFT Join (SELECT sp.Supplier_Bill_Num,Sum(sp.Supplier_Payment_Amount) As Supplier_Payment  FROM suppliers_payments sp
join suppliers_bill sb1 on sp.Supplier_Bill_Num = sb1.Supplier_Bill_Num
Where sp.status = 1
and sb1.status = 1
and sb1.Due_Date <= '$v_Input_Date'
GROUP BY sp.Supplier_Bill_Num
) payments

on payments.Supplier_Bill_Num = sb.Supplier_Bill_Num
where sb.status = 1 $WhereClause
AND sb.Due_Date <= '$v_Input_Date'
HAVING (Bill_Amount  - Supplier_Payment_Amount) <> 0";

$records = GDb::fetchRowSet($sql);
}
?>
<form action="reports-SP.php" method="GET">
	<div class="card">
		<div class="row col-sm-12 mt-3 mb-2">
			<div class="form-group form-default form-static-label col-sm-2">
				<label class="float-label gst-label">As of Date</label>
				<input type="date" required="required" name="SupplierBill_Date" value='<?php echo GUtils::clientDate(Date('Y-m-d', strtotime($v_Input_Date)), 'Y-m-d'); ?>' class="form-control date-picker">
			</div>
			<div class="form-group form-default form-static-label col-sm-4">
				<label class="float-label gst-label">Supplier Name</label>
				<select id="idCustomerList" onchange="return false;" name="SupplierID" class="js-example-basic-multiple-limit gst-lazy-dropdown col-sm-12" multiple="multiple" size="1" >
				<?php foreach ($SuppliersList as $SupplierData) {
					$selected = '';
					/*if ($SupplierData['id'] == $customerId && $customerId) {
					$selected = 'selected="selected"';
					}*/
					echo "<option $selected value='".$SupplierData['id']."'>"."(".$SupplierData['company'].") ".$SupplierData['fname']." ".$SupplierData['mname']." ".$SupplierData['lname']."</option>";
				} ?>
				</select>
			</div>
			<div class="form-group form-default form-static-label col-sm-2">
				<label class="float-label gst-label"><i class="fas fa-angle-double-right"></i></label><br />
				<button type="submit" onclick="formmodified = 0; window.onbeforeunload = null;" value="save" name="submit" class="btn btn-block waves-effect waves-light btn-info" style="padding-top:6px;padding-bottom:7px;">
					<i class="far fa-check-circle"></i> Run Report
				</button>
			</div>
		</div>
		<?php if( isset($__REQUEST['submit']) ) { ?>
		<div class="col-12"></div>
		<div class="row col-12" style="padding-top: 0;justify-content: center;">
			<div class="col-md-2" style="cursor: pointer;">
				<div class="col-md-12 gst-summary-heading">Total Open</div>
				<div  class="col-md-12 gst-summary-bg bg-silver">
					<div class="bg-danger" style="z-index:1;position: absolute; bottom: 0; left: 0; height: 100%; width: 100%;"></div>
					<div class="bg-danger" style="z-index:2;position: absolute; bottom: 0; left: <?php echo $first;?>%; width: 100%; height: 100%;"></div>
					<span style="z-index:3; position: relative;" id="globalTotalOpen">$0</span>
				</div>
			</div>
			<div class="col-md-2" style="cursor: pointer;">
				<div class="col-md-12 gst-summary-heading">Payment Amount</div>
				<div  class="col-md-12 gst-summary-bg bg-silver">
					<div class="bg-success" style="z-index:1;position: absolute; bottom: 0; left: 0; height: 100%; width: 100%;"></div>
					<div class="bg-success" style="z-index:2;position: absolute; bottom: 0; left: <?php echo $first;?>%; width: 100%; height: 100%;"></div>
					<span style="z-index:3; position: relative;" id="globalPaymentAount">$0</span>
				</div>
			</div>
		</div>
                    <div class="col-2"></div>
                     <div class="row col-md-12 mb-5" style="padding-top: 0;justify-content: center;">
<div class="col-md-2" style="cursor: pointer;">
    <div class="col-md-12 gst-summary-heading">Past Due</div>
    <div  class="col-md-12 gst-summary-bg bg-silver">
        <div class="bg-warning" style="z-index:1;position: absolute; bottom: 0; left: 0; height: 100%; width: 100%;"></div>
        <div class="bg-warning" style="z-index:2;position: absolute; bottom: 0; left: <?php echo $first;?>%; width: 100%; height: 100%;"></div>
        <span style="z-index:3; position: relative;" id="globalPastDue">$0</span>
    </div>
</div>
<div class="col-md-2" style="cursor: pointer;">
    <div class="col-md-12 gst-summary-heading">Current Due</div>
    <div  class="col-md-12 gst-summary-bg bg-silver">
        <div class="bg-danger" style="z-index:1;position: absolute; bottom: 0; left: 0; height: 100%; width: 100%;"></div>
        <div class="bg-danger" style="z-index:2;position: absolute; bottom: 0; left: <?php echo $first;?>%; width: 100%; height: 100%;"></div>
        <span style="z-index:3; position: relative;" id="globalCurrentDue">$0</span>
    </div>
</div>
<div class="col-md-2" style="cursor: pointer;">
    <div class="col-md-12 gst-summary-heading">Past Due 30 Days</div>
    <div  class="col-md-12 gst-summary-bg bg-silver">
        <div class="bg-success" style="z-index:1;position: absolute; bottom: 0; left: 0; height: 100%; width: 100%;"></div>
        <div class="bg-success" style="z-index:2;position: absolute; bottom: 0; left: <?php echo $first;?>%; width: 100%; height: 100%;"></div>
        <span style="z-index:3; position: relative;" id="globalDue30Days">$0</span>
    </div>
</div>
<div class="col-md-2" style="cursor: pointer;">
    <div class="col-md-12 gst-summary-heading">Past Due 60 Days</div>
    <div  class="col-md-12 gst-summary-bg bg-silver">
        <div class="bg-warning" style="z-index:1;position: absolute; bottom: 0; left: 0; height: 100%; width: 100%;"></div>
        <div class="bg-warning" style="z-index:2;position: absolute; bottom: 0; left: <?php echo $first;?>%; width: 100%; height: 100%;"></div>
        <span style="z-index:3; position: relative;" id="globalDue60Days">$0</span>
    </div>
</div>
<div class="col-md-2" style="cursor: pointer;">
    <div class="col-md-12 gst-summary-heading">Past Due 90 Days</div>
    <div  class="col-md-12 gst-summary-bg bg-silver">
        <div class="bg-danger" style="z-index:1;position: absolute; bottom: 0; left: 0; height: 100%; width: 100%;"></div>
        <div class="bg-danger" style="z-index:2;position: absolute; bottom: 0; left: <?php echo $first;?>%; width: 100%; height: 100%;"></div>
        <span style="z-index:3; position: relative;" id="globalDue90Days">$0</span>
    </div>
</div>
<div class="col-md-2" style="cursor: pointer;">
    <div class="col-md-12 gst-summary-heading">Past Due Over 90</div>
    <div  class="col-md-12 gst-summary-bg bg-silver">
        <div class="bg-warning" style="z-index:1;position: absolute; bottom: 0; left: 0; height: 100%; width: 100%;"></div>
        <div class="bg-warning" style="z-index:2;position: absolute; bottom: 0; left: <?php echo $first;?>%; width: 100%; height: 100%;"></div>
        <span style="z-index:3; position: relative;" id="globalDueOver90">$0</span>
    </div>
</div>
                    </div>
                    
<div class="col-md-12">
	<div class="dt-responsive table-responsive">
	<table class="datatable-report table gst-table table-hover table-striped table-bordered nowrap responsive " data-page-length="20" style="width:100%;" id="reports_ar_datatable">
		<thead>
			<tr>
				<th>Supplier ID</th>
				<th>Supplier Name</th>
				<th>Bill ID</th>
				<th>Bill Date</th>
				<th>Bill Amount</th>
				<th>Due Date</th>
				<th>Days Due</th>
				<th>Total Open</th>
				<th>Paid Amount</th>
				<th>Past Due</th>
				<th>Current Due</th>
				<th>Past Due 30 Days</th>
				<th>Past Due 60 Days</th>
				<th>Past Due 90 Days</th>
				<th>Past Due Over 90 Days</th>
			</tr>
		</thead>
		<tbody>
		<?php 
		$foot = [
			'Bill_Amount' => 0.0,
			'Total_Open' => 0.0,
			'Supplier_Payment_Amount' => 0.0,
			'Past_Due' => 0.0,
			'Current_' => 0.0,
			'Past_Due1_30' => 0.0,
			'Past_Due31_60' => 0.0,
			'Past_Due61_90' => 0.0,
			'Past_Due_over_90' => 0.0,
		]; 
		if( isset($records) && is_array($records) ) { 
			foreach( $records as $row ) { 
				$dueColor = '' ;
				if( $row['Days_Due'] < 0 ) {
					$dueColor = 'style="color:red"' ;
				}
				$foot['Bill_Amount'] += $row['Bill_Amount'] ;
				$foot['Total_Open'] += $row['Total_Open'] ;
				$foot['Supplier_Payment_Amount'] += $row['Supplier_Payment_Amount'] ;
				$foot['Past_Due'] += $row['Past_Due'] ;
				$foot['Current_'] += $row['Current_'] ;
				$foot['Past_Due1_30'] += $row['Past_Due1_30'] ;
				$foot['Past_Due31_60'] += $row['Past_Due31_60'] ;
				$foot['Past_Due61_90'] += $row['Past_Due61_90'] ;
				$foot['Past_Due_over_90'] += $row['Past_Due_over_90'] ;
			?>
			<tr>
				<td><?php echo $row['Supplier_ID'];?></td>
				<td><?php echo $row['Supplier_Name'];?></td>
				<td><?php echo $row['Supplier_Bill_Num'];?></td>
				<td><?php echo GUtils::clientDate($row['Supplier_Bill_Date']);?></td>
				<td><?php echo GUtils::formatMoney( $row['Bill_Amount'] );?></td>
				<td><?php echo GUtils::clientDate($row['Due_Date']);?></td>
				<td <?php echo $dueColor;?> ><?php echo $row['Days_Due'];?></td>
				<td><?php echo GUtils::formatMoney( $row['Total_Open'] );?></td>
				<td><?php echo GUtils::formatMoney( $row['Supplier_Payment_Amount'] );?></td>
				<td><?php echo GUtils::formatMoney( $row['Past_Due'] );?></td>
				<td><?php echo GUtils::formatMoney( $row['Current_'] );?></td>
				<td><?php echo GUtils::formatMoney( $row['Past_Due1_30'] );?></td>
				<td><?php echo GUtils::formatMoney( $row['Past_Due31_60'] );?></td>
				<td><?php echo GUtils::formatMoney( $row['Past_Due61_90'] );?></td>
				<td><?php echo GUtils::formatMoney( $row['Past_Due_over_90'] );?></td>
			</tr>
		<?php } }?>
		</tbody>
        <?php 
        $numRecord = count($records) ;
        if( isset($records) && is_array($records) && $numRecord > 0 ) { 
            $globalTotalOpen = "'".GUtils::formatMoney( $foot['Total_Open'])."'";
            $globalPaymentAount = "'".GUtils::formatMoney( $foot['Supplier_Payment_Amount'])."'";
            $globalPastDue = "'".GUtils::formatMoney( $foot['Past_Due'])."'";
            $globalCurrentDue = "'".GUtils::formatMoney( $foot['Current_'])."'";
            $globalDue30Days = "'".GUtils::formatMoney( $foot['Past_Due1_30'])."'";
            $globalDue60Days = "'".GUtils::formatMoney( $foot['Past_Due31_60'])."'";
            $globalDue90Days = "'".GUtils::formatMoney( $foot['Past_Due61_90'])."'";
            $globalDueOver90 = "'".GUtils::formatMoney( $foot['Past_Due_over_90'])."'";
        ?>
        <tfoot>
                <th colspan="3" class="text-right" ></th>
                <th class="text-right" >Total</th>
                <th><?php echo GUtils::formatMoney( $foot['Bill_Amount'] );?></th>
                <th colspan="2"></th>
                <th><?php echo GUtils::formatMoney( $foot['Total_Open']); ?></th>
                <th><?php echo GUtils::formatMoney( $foot['Supplier_Payment_Amount']); ?></th>
                <th><?php echo GUtils::formatMoney( $foot['Past_Due']); ?></th>
                <th><?php echo GUtils::formatMoney( $foot['Current_']); ?></th>
                <th><?php echo GUtils::formatMoney( $foot['Past_Due1_30']); ?></th>
                <th><?php echo GUtils::formatMoney( $foot['Past_Due31_60']); ?></th>
                <th><?php echo GUtils::formatMoney( $foot['Past_Due61_90']); ?></th>
                <th><?php echo GUtils::formatMoney( $foot['Past_Due_over_90']); ?></th>
        </tfoot>
        <?php } ?>
    </table>
</div>
                    </div>
                    <?php } ?>
                </div>
    </form>

<style>
.dt-buttons {
	float: right !important;
}
#reports_ar_datatable_filter {
	float: left !important;
}
button.dt-button, div.dt-button, a.dt-button {
	margin-right: 0 !important;
	margin-left: .333em;
}
</style>

<script type="text/javascript">

/*$(document).ready(function() {
    $('#reports_ar_datatable').DataTable( {
        //"ordering": false, //To disable the ordering of the tables
		"bLengthChange": false, //To hide the Show X entries dropdown
		dom: 'Bfrtip',
		//sDom: 'lrtip', //To Hide the search box
        buttons: [{ extend: 'csvHtml5', footer: true },{ extend: 'excelHtml5', footer: true }] 
    } );
} );*/

$(document).ready( function () {
  var table = $('#reports_ar_datatable').DataTable({
        //"ordering": false, //To disable the ordering of the tables
		"bLengthChange": false, //To hide the Show X entries dropdown
		dom: 'Bfrtip',
		//sDom: 'lrtip', //To Hide the search box
        buttons: [
            {
                extend: 'excelHtml5',
                footer: true,
                customize: function( xlsx ) {
                  var sheet = xlsx.xl.worksheets['sheet1.xml'];
                  var col = $('col', sheet);
                  var ocellXfs = $('cellXfs', xlsx.xl['styles.xml']);
                  ocellXfs.append('<xf numFmtId="170" fontId="0" fillId="0" borderId="0" xfId="0" applyFont="1" applyFill="1" applyBorder="1" applyAlignment="1">'+'</xf>');
                  ocellXfs.attr('count', ocellXfs.attr('count') +1 );

                  var numFmts = $('numFmts', xlsx.xl['styles.xml']);
                  numFmts.append('<numFmt formatCode="$ #,##0.00"  numFmtId="170" />');
                  numFmts.attr('count', numFmts.attr('count') +1 );

                  var oxf = $('xf', xlsx.xl['styles.xml']);
                  var styleIndex = oxf.length;
                  
				  $('row c[r^="F"]', sheet).attr( 's', styleIndex - 2 );
				  $('row c[r^="I"]', sheet).attr( 's', styleIndex - 2 );
				  $('row c[r^="J"]', sheet).attr( 's', styleIndex - 2 );
				  $('row c[r^="K"]', sheet).attr( 's', styleIndex - 2 );
				  $('row c[r^="L"]', sheet).attr( 's', styleIndex - 2 );
				  $('row c[r^="M"]', sheet).attr( 's', styleIndex - 2 );
				  $('row c[r^="N"]', sheet).attr( 's', styleIndex - 2 );
				  $('row c[r^="O"]', sheet).attr( 's', styleIndex - 2 );
				  $('row c[r^="P"]', sheet).attr( 's', styleIndex - 2 );
				  $('row:eq(0) c', sheet).attr( 's', '2' ); //This is for the first row in the sheet
				  },            
            },
        ],

          "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 5 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 5 ).footer() ).html(
                '$'+ total
            );
        }
  });
} );

setTimeout(function() {
    $('#globalTotalOpen').html(<?php echo $globalTotalOpen; ?>);
    $('#globalPaymentAount').html(<?php echo $globalPaymentAount; ?>);
    $('#globalPastDue').html(<?php echo $globalPastDue; ?>);
    $('#globalCurrentDue').html(<?php echo $globalCurrentDue; ?>);
    $('#globalDue30Days').html(<?php echo $globalDue30Days; ?>);
    $('#globalDue60Days').html(<?php echo $globalDue60Days; ?>);
    $('#globalDue90Days').html(<?php echo $globalDue90Days; ?>);
    $('#globalDueOver90').html(<?php echo $globalDueOver90; ?>);
}, 1000);

function loadCustomerList(val) {
	//reset first
	$('#idCustomerList').html('');
	var customerId = $('#idCustomerList').val() ;
		if (val !== undefined) {
			if( val.length > 0 ) {
				ajaxCall('inc/ajax.php', {'action': 'customer-list-html', 'groupId': val, 'customerId' : customerId }, function (data) {
				$('#idCustomerList').html(data);
				}, "html");
			}
		}
	return false;        
};
</script>
<?php include 'inc/notificiations.php'; ?>
    <!-- [ page content ] end -->
<?php include "footer.php"; ?>