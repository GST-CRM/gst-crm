<?php
include "session.php";
$PageTitle = "Payees List";
include "header.php"; 


if(isset($_GET['action'])) {
    $action = mysqli_real_escape_string($db, $_GET['action']);
} else {
    $action = "";
}
if(isset($_GET['status'])) {
    $PayeeStatus = $_GET['status'];
    if($PayeeStatus == "voided") {
        $Payee_Where_URL = "?status=voided";
    } else {
        $Payee_Where_URL = "?status=active";
    }
    
} else {
    $PayeeStatus = "";
    $Payee_Where_URL = "";
}


//To show the Void Message
if ($action == "void") {
	$PayeeID = $_GET['payee_id'];
	$Query = "UPDATE expenses_payee SET Status=0 WHERE Payee_ID=$PayeeID";
	if ($db->multi_query($Query) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected payee has been voided successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error voiding payee: " . $db->error . "</strong></div>";
	}
}

//To show the Un-Void Message
if ($action == "unvoid") {
	$PayeeID = $_GET['payee_id'];
	$Query = "UPDATE expenses_payee SET Status=1 WHERE Payee_ID=$PayeeID";
	if ($db->multi_query($Query) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected payee has been un-voided successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error un-voiding payee: " . $db->error . "</strong></div>";
	}
}

//To show the Delete Message
if($action == "delete") {
	$PayeeID = $_GET['payee_id'];
	$Query = "DELETE FROM expenses_payee WHERE Payee_ID=$PayeeID";
	if ($db->multi_query($Query) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected payee has been deleted successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting payee: " . $db->error . "</strong></div>";
	}
}

?>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
        <div class="card">
             <div class="row">
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
					</div>
				</div>
				<div class="col-md-5" style="margin-top:15px;"></div>
				<div class="col-md-2" style="margin-top:15px;">
					<select id="status" name="status" class="form-control form-control-default fill" onChange="doReload(this.value);">
						<option value="0">Status</option>
						<?php $StatusGET = mysqli_real_escape_string($db, $_GET['status']); ?>
						<option value="active" <?php if(!empty($StatusGET) AND $StatusGET == "active") {echo "selected";} ?>>Active Payees</option>
                        <option value="voided" <?php if(!empty($StatusGET) AND $StatusGET == "voided") {echo "selected";} ?>>Voided Payees</option>
					</select>
				</div>
				<div class="col-md-2" style="margin-top:15px;padding-right:45px;">
					<a href="javascript:void(0);" id="add" class="btn waves-effect float-right waves-light btn-success" style="padding: 3px 13px;"><i class="fas fa-plus-circle"></i>Add New Payee</a>
				</div>
			</div>
            <div class="card-block">
			<style>.PayeeNameField {background:#f2f2f2;} .PayeeNameField:active,.PayeeNameField:focus {background:white;}</style>
                <div class="dt-responsive table-responsive">
					<div class="table-responsive">
						<div id="alert_message"></div>
						<table id="user_data" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Payee ID</th>
									<th>Payee Name</th>
									<th>Add Time</th>
									<th>Last time modified</th>
									<th>Actions</th>
								</tr>
							</thead>
						</table>
					</div>
                </div>
            </div>

<script type="text/javascript" language="javascript" >
$(document).ready(function() {

    //Just to move the search box to be on the top of the card section
    $('.outsideBorderSearch').on( 'keyup click', function () {
        $('#user_data').DataTable().search(
            $('.outsideBorderSearch').val()
        ).draw();
    });
});

function doReload(status){
	document.location = 'expenses-payee.php?status=' + status;
}

$(document).ready(function(){
	fetch_data();
  function fetch_data() {
   var dataTable = $('#user_data').DataTable({
    "processing" : true,
    "serverSide" : true,
    "ordering": false,
       "bLengthChange": false,
       dom: 'Bfrtip',
       sDom: 'lrtip',
       buttons: [],
       "bLengthChange": false,
       "order": [[ 0, "desc" ]],
    "ajax" : {
     url:"inc/payee-fetch.php<?php echo $Payee_Where_URL; ?>",
     type:"POST"
    }
   });
  }
  
  function update_data(id, column_name, value) {
   $.ajax({
    url:"inc/payee-update.php",
    method:"POST",
    data:{id:id, column_name:column_name, value:value},
    success:function(data) {
     $('#alert_message').html(data);
     $('#user_data').DataTable().destroy();
     fetch_data();
    }
   });
   setInterval(function(){
    $('#alert_message').html('');
   }, 10000);
  }

  $(document).on('blur', '.update', function(){
   var id = $(this).data("id");
   var column_name = $(this).data("column");
   var value = $(this).val();
   update_data(id, column_name, value);
  });
  
  $('#add').click(function(){
   var html = '<tr>';
   html += '<td>-</td>';
   //html += '<td contenteditable id="data2">Write the Payee Name</td>';
   html += '<td><div class="input-group mb-0"><input id="data2" type="text" class="form-control" data-column="Payee_Name" placeholder="Write the new name here"><div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="far fa-edit"></i></span></div></div></td>';
   html += '<td>-</td>';
   html += '<td>-</td>';
   html += '<td><button type="button" name="insert" id="insert" class="btn btn-success btn-sm">Insert</button></td>';
   html += '</tr>';
   $('#user_data tbody').prepend(html);
  });
  
  $(document).on('click', '#insert', function(){
   var New_Payee_Name = $('#data2').val();
   if(New_Payee_Name != '')
   {
    $.ajax({
     url:"inc/payee-add.php",
     method:"POST",
     data:{New_Payee_Name:New_Payee_Name},
     success:function(data)
     {
      $('#alert_message').html(''+data+'</div>');
      $('#user_data').DataTable().destroy();
      fetch_data();
     }
    });
    setInterval(function(){
     $('#alert_message').html('');
    }, 10000);
   }
   else
   {
    alert("Please fill the name of the Payee");
   }
  });
 });
</script>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<!-- [ page content ] end -->
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>