<?php
include "config.php";
session_start();
define('LOGIN_TEMPLATE', true);
$PageTitle = "Change Password";

$token = $__GET['t'] ;


if( ! empty($__POST) ) {
    include_once __DIR__ . '/inc/reset_password_update.php';
}


include_once __DIR__ . '/models/users.php' ;
$user = new Users() ;
$userData = $user->get(['token' => $token]) ;

include "header.php";



?>
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->
    <section class="login-block">
        <!-- Container-fluid starts -->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Authentication card start -->
                    <form name="contact3" id="contact3" class="md-float-material form-material" method="post">
                        <div class="text-center">
                            <img src="files/assets/images/auth/logo-dark.png" alt="logo.png">
                        </div>
                        <div class="auth-box card">
                            <div class="card-block">
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <h3 class="text-center">Change Password</h3>
                                    </div>
                                </div>
								<div id="results"><?php echo GUtils::flashMessage() ; ?></div>
                                <?php if( $userData['token_type'] == Users::$TOKEN_CHANGE_PASSWORD ) { ?>
                                <div class="form-group form-primary">
                                    <input type="password" name="old_password" class="form-control" required="" autocomplete="off">
                                    <span class="form-bar"></span>
                                    <label class="float-label">Current Password</label>
                                </div>
                                <?php } ?>
                                <div class="form-group form-primary">
                                    <input type="password" name="new_password" class="form-control" required="" autocomplete="off">
                                    <span class="form-bar"></span>
                                    <label class="float-label">New Password</label>
                                </div>
                                <div class="form-group form-primary">
                                    <input type="password" name="confirm_password" class="form-control" required="" autocomplete="off">
                                    <span class="form-bar"></span>
                                    <label class="float-label">Confirm Password</label>
                                </div>
                                <div class="row m-t-30">
                                    <input type="hidden" name="token" class="form-control" value="<?php echo (isset($__GET['t']) ? $__GET['t'] : ''); ?>" >
                                    <div class="col-md-12 row">
                                        <button type="button" onclick="window.location.href='<?php echo GUtils::domainUrl();?>';"  name="cancel" class="btn col-md-4 btn-default btn-md waves-effect waves-light text-center m-b-20">Cancel</button> 
                                        <button type="submit" name="submit" class="btn m-l-10 col-md-7 btn-primary btn-md waves-effect waves-light text-center m-b-20">Change Password</button>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-8">
                                        <p class="text-inverse text-left m-b-0">Thank you.</p>
                                        <p class="text-inverse text-left"><a href="https://www.tourtheholylands.com/"><b>Back to website</b></a></p>
                                    </div>
                                    <div class="col-md-4">
                                        <img src="files/assets/images/auth/Logo-small-bottom.png" alt="small-logo.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- end of form -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container-fluid -->
    </section>
	
<?php include "footer.php"; ?>
