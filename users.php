<?php
include "session.php";
$PageTitle = "Users";
include "header.php"; 
include_once __DIR__ . '/models/users.php';

//To delete the user
if ($_GET['action'] == "delete") {
	$userid = $_GET["id"];
	$con4 = new mysqli($servername, $username, $password, $dbname);
	if ($con4->connect_error) {die("Connection failed: " . $con4->connect_error);} 
	$sql2 = "DELETE FROM users WHERE UserID=$userid;";
	if ($con4->multi_query($sql2) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The user account has been removed successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting record: " . $con4->error . "</strong></div>";
	}
	$con4->close();
}
else if ($_GET['action'] == "send") {
	$userid = $_GET["id"];
    $userData = (new Users())->get(['UserId' => $userid]) ;
    $token = GUtils::getUniqId() ;
    $data = array(
        'token' => $token,
        'token_type' => 2,
    ) ;
    $email = $userData['EmailAddress'] ;    
    $name = $userData['fname'] . ' ' . $userData['lname'] ;
    
    GUtils::forgotMail($email, $token, $name, false) ;
}

//To Get the value of the user type if not empty 
if(isset($_GET['type'])) {
	$UserType = mysqli_real_escape_string($db, $_GET['type']);
} else {
	$UserType = NULL;
}
?>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
		<div class="card">
            <div class="row">
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
					</div>
				</div>
				<div class="col-md-5" style="margin-top:15px;"></div>
				<div class="col-md-2" style="margin-top:15px;">
					<select id="type" name="type" class="form-control form-control-default fill" onChange="doReload(this.value);">
						<option value="">User Type</option>
						<option value="1" <?php if($UserType == "1") {echo "selected";} ?>>Admins</option>
						<option value="2" <?php if($UserType == "2") {echo "selected";} ?>>Agents</option>
						<option value="8" <?php if($UserType == "8") {echo "selected";} ?>>Suppliers</option>
						<option value="4" <?php if($UserType == "4") {echo "selected";} ?>>Tour Leaders</option>
						<option value="staff" <?php if($UserType == "staff") {echo "selected";} ?> disabled>Staff</option>
						<option value="customers" <?php if($UserType == "customers") {echo "selected";} ?> disabled>Customers</option>
					</select>
				</div>
				<div class="col-md-2" style="margin-top:15px;padding-right:45px;">
					<a href="users-add.php" class="btn waves-effect float-right waves-light btn-success" style="padding: 3px 13px;"><i class="fas fa-plus-circle"></i>Add New User</a>
				</div>
			</div>
            <div class="card-block">
				<div class="dt-responsive table-responsive">
                    <table id="TableWithNoButtons" class="table table-hover table-striped table-bordered nowrap" data-page-length="20">
                        <thead>
                            <tr>
                                <th>ID #</th>
                                <th>Username</th>
                                <th>Full Name</th>
                                <th>User Type</th>
                                <th>Email</th>
                                <th>Creation Date</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
						<?php
						if($UserType != NULL) { $WhereCause = "AND Role_ID=$UserType"; } else { $WhereCause = NULL; }
						$sql = "SELECT * FROM `users` WHERE 1 $WhereCause"; $result = $db->query($sql);
						if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
                            <tr>
                                <td><?php echo $row["UserID"]; ?></td>
                                <td><a href="users-edit.php?id=<?php echo $row["UserID"]; ?>" style="color: #0000EE;">
								<?php echo $row["Username"]; ?></a></td>
                                <td><?php echo $row["fname"]. " ".$row["lname"]; ?></td>
                                <td><?php 
                                echo Users::explainRoles($row['Role_ID']) ;

                                ?></td>
                                <td><?php echo $row["EmailAddress"]; ?></td>
                                <td><?php echo date('m/d/Y - h:i A', strtotime($row["createdate"])); ?></td>
                                <td>
                                    <a href="users.php?id=<?php echo $row["UserID"]; ?>&action=delete" onclick="return confirm('Are you sure?')"><img src="files/assets/images/remove.png" style="margin-left:5px;width:22px;height:auto;" alt="remove" /></a>
                                    <a href="javascript:;" onclick = 'deleteConfirm("users.php?action=send&id=<?php echo $row["UserID"]; ?>", "Send", "Are you sure you want to sent password reset email to selected user ?", "Please Confirm")' style="vertical-align: bottom; margin-left:  5px;" >
                                        <i class="fa fa-key"></i>                                        
                                    </a>
                                </td>
                            </tr>
						<?php }} ?>
						</tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<!-- [ page content ] end -->
<script>
//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#TableWithNoButtons').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
});

$(document).ready(function() {
    $('#TableWithNoButtons').DataTable( {
        //"ordering": false, //To disable the ordering of the tables
		"bLengthChange": false, //To hide the Show X entries dropdown
		dom: 'Bfrtip',
		sDom: 'lrtip', //To Hide the search box
        buttons: [] //To hide the export buttons
    } );
} );

function doReload(type){
	document.location = 'users.php?type=' + type;
}
</script>
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>