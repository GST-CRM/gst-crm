<?php
include "session.php";
$PageTitle = "List of Checks";

//The batch action section
if( isset($__REQUEST['batch-invaction']) ) {
    //batch print
    if( $__REQUEST['batch-invaction'] == 'P' ) {
		$CorrectFolder = 1;
        include __DIR__ . '/inc/checks-print-pdf.php';
    }
    //batch void
    else if( $__REQUEST['batch-invaction'] == 'V' ) {
        $CheckIDs = $__REQUEST['cbInvoice'];
		$CheckCount = 0;
		foreach($CheckIDs AS $CheckID) {
			$CheckVoid = "UPDATE checks SET Check_Status=0 WHERE Check_ID=$CheckID;";
			GDb::execute($CheckVoid);
			$CheckCount++;
		}
		$BatchSuccessMessage = "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>$CheckCount checks have been voided successfully.</strong></div>";
    }
    //batch delete
    else if( $__REQUEST['batch-invaction'] == 'D' ) {
        $CheckIDs = $__REQUEST['cbInvoice'];
		$CheckCount = 0;
		foreach($CheckIDs AS $CheckID) {
			$CheckDelete = "DELETE FROM checks WHERE Check_ID=$CheckID;";
			GDb::execute($CheckDelete);
			$CheckCount++;
		}
		$BatchSuccessMessage = "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>$CheckCount checks have been deleted successfully.</strong></div>";
    }
} else {
	$BatchSuccessMessage = NULL;
}


include "header.php"; 

if(isset($_GET['action'])) {
    $action = mysqli_real_escape_string($db, $_GET['action']);
} else {
    $action = "";
}

if(isset($_GET['status'])) {
    $StatusGET = mysqli_real_escape_string($db, $_GET['status']);
} else {
    $StatusGET = "";
}

//To show the Void Message
if ($action == "void") {
	$CheckID = mysqli_real_escape_string($db, $_GET['id']);
	$CheckNum = mysqli_real_escape_string($db, $_GET['num']);
	$CheckVoid = "UPDATE checks SET Check_Status=0 WHERE Check_ID=$CheckID;";
	if(GDb::execute($CheckVoid)) {
        GDb::execute("INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','check','$CheckID','voided','#$CheckNum');");
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected check record has been voided successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error voiding record: " . $db->error . "</strong></div>";
	}
}

//To show the Un-Void Message
if ($action == "unvoid") {
	$CheckID = mysqli_real_escape_string($db, $_GET['id']);
	$CheckNum = mysqli_real_escape_string($db, $_GET['num']);
	$CheckUnVoid = "UPDATE checks SET Check_Status=1 WHERE Check_ID=$CheckID;";
	if(GDb::execute($CheckUnVoid)) {
        GDb::execute("INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','check','$CheckID','unvoided','#$CheckNum');");
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected check record has been un-voided successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error un-voiding record: " . $db->error . "</strong></div>";
	}
}

//To show the Delete Message
if ($action == "delete") {
	$CheckID = mysqli_real_escape_string($db, $_GET['id']);
	$CheckNum = mysqli_real_escape_string($db, $_GET['num']);
	$CheckDelete = "DELETE FROM checks WHERE Check_ID=$CheckID;";
	if(GDb::execute($CheckDelete)) {
        GDb::execute("INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','check','$CheckID','deleted','#$CheckNum');");
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected check record has been deleted successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting record: " . $db->error . "</strong></div>";
	}
}

//To show the Cashed Message
if ($action == "cashed") {
	$CheckID = mysqli_real_escape_string($db, $_GET['id']);
	$CheckNum = mysqli_real_escape_string($db, $_GET['num']);
	$CheckCashed = "UPDATE checks SET Check_Cashed=1, Check_Cashed_Date=NOW() WHERE Check_ID=$CheckID;";
	if(GDb::execute($CheckCashed)) {
        GDb::execute("INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','check','$CheckID','cashed','#$CheckNum');");
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected check record has been cashed successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error cashing record: " . $db->error . "</strong></div>";
	}
}

//To show the success message if the user used the batch action dropdown
echo $BatchSuccessMessage;
?>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
        <form action="checks.php" name="CheckListForm" method="POST">
        <div class="card">
            <div class="row">
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
					</div>
				</div>
				<div class="col-md-3" style="margin-top:15px;"></div>
                <div class="col-md-2" style="margin-top:15px;">
                    <select name="batch-invaction" onchange="return batchSubmit(this);" class="form-control form-control-default fill gst-invoice-batchaction" disabled="disabled">
                        <option value="">Batch Action</option>
                        <option value="P">Print</option>
                        <option value="V">Void</option>
                        <option value="x">Delete</option>
                    </select>
                </div>
				<div class="col-md-2" style="margin-top:15px;">
					<select id="status" name="status" class="form-control form-control-default fill" onChange="doReload(this.value);">
						<option value="0">Status</option>
						<option value="active" <?php if(!empty($StatusGET) AND $StatusGET == "active") {echo "selected";} ?>>Active Checks</option>
						<option value="cashed" <?php if(!empty($StatusGET) AND $StatusGET == "cashed") {echo "selected";} ?>>Cashed Checks</option>
						<option value="printed" <?php if(!empty($StatusGET) AND $StatusGET == "printed") {echo "selected";} ?>>Printed Checks</option>
						<option value="voided" <?php if(!empty($StatusGET) AND $StatusGET == "voided") {echo "selected";} ?>>Voided Checks</option>
					</select>
				</div>
                <div class="col-md-2" style="margin-top:15px;padding-right:45px;">
					<a href="check-add.php" class="btn waves-effect btn-block float-right waves-light btn-success" style="padding: 5px 13px;"><i class="fas fa-plus-circle"></i>Add New Check</a>
				</div>
			</div>
            <div class="card-block " >
                <div class="dt-responsive table-responsive " style="position: relative;">                    
                    <table id="TableWithNoButtons" class="table table-striped table-bordered nowrap" style="width:100%;">
                        <thead>
                            <tr>
                                <th class=" gst-no-sort gst-has-events text-center text-center pr-0 pl-0">
                                    <input type="checkbox" class="gst-invoice-cb-parent" />
                                </th>
                                <th>Check #</th>
                                <th>Customer Name</th>
                                <th>Bank Name</th>
                                <th>Check Amount</th>
                                <th><?php if($StatusGET == 'cashed') { echo 'Cashed Date'; } else { echo 'Check Date'; } ?></th>
                                <th>Check Type</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
						<?php
						if ($StatusGET == "voided") {
							$WhereCondition = " ch.Check_Status=0 ";
						} elseif($StatusGET == "active") {
							$WhereCondition = " ch.Check_Status=1 ";
						} elseif($StatusGET == "cashed") {
							$WhereCondition = " ch.Check_Status=1 AND ch.Check_Cashed=1 ";
						} elseif($StatusGET == "printed") {
							$WhereCondition = " ch.Check_Status=1 AND ch.Check_Printed !=0 ";
						} else {
							$WhereCondition = " ch.Check_Status=1  ";
						}
						$sql = "SELECT
	ch.Check_ID,
	ch.Check_Type,
	ch.Check_Number,
    ch.Check_Customer_ID,
    ch.Check_Bank_ID,
    chb.Bank_Name,
    ch.Check_Amount, 
    ch.Check_Date,  
    ch.Check_Memo,
    ch.Check_Status,
    ch.Check_Cashed,
    ch.Check_Cashed_Date,
	CASE
    	WHEN ch.Check_Type = 1 THEN (SELECT CONCAT(co.fname,' ',co.mname,' ',co.lname) FROM contacts co WHERE co.id=ch.Check_Customer_ID)
        WHEN ch.Check_Type = 2 THEN (SELECT ep.Payee_Name FROM expenses_payee ep WHERE ep.Payee_ID=ch.Check_Customer_ID)
        WHEN ch.Check_Type = 3 THEN (SELECT co.company FROM contacts co WHERE co.id=ch.Check_Customer_ID)
        WHEN ch.Check_Type = 4 THEN (SELECT co.company FROM contacts co WHERE co.id=ch.Check_Customer_ID)
    END AS Customer_Name
FROM checks ch
JOIN checks_bank chb 
ON chb.Bank_ID=ch.Check_Bank_ID
WHERE $WhereCondition
ORDER BY ch.Check_Date DESC;";
						$result = $db->query($sql);
						if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
                            <tr>
                                <td class="text-center"><input name="cbInvoice[]" value="<?php echo $row["Check_ID"]; ?>" type="checkbox" class="gst-invoice-cb-children gst-invoice-cb"/></td>
                                <td><a href="check-edit.php?id=<?php echo $row["Check_ID"]; ?>" style="color: #0000EE;"><?php echo $row["Check_Number"]; ?></a></td>
                                <td><a href="contacts-edit.php?id=<?php echo $row["Check_Customer_ID"]; ?>&action=edit&usertype=customer" style="color: #0000EE;"><?php echo $row["Customer_Name"]; ?></a></td>
                                <td><a href="check-bank-edit.php?id=<?php echo $row["Check_Bank_ID"]; ?>" style="color: #0000EE;">
								<?php echo $row["Bank_Name"]; ?></a></td>
                                <td><?php echo GUtils::formatMoney($row["Check_Amount"]); ?></td>
                                <td><?php if($StatusGET == 'cashed') { echo GUtils::clientDate($row["Check_Cashed_Date"]); } else { echo GUtils::clientDate($row["Check_Date"]); } ?></td>
                                <td><?php if($row["Check_Type"] == 1) { echo "Customer Check"; } else { echo "Expense Check"; } ?></td>
                                <td>
									<div class="btn-group">
										<?php if($StatusGET == "voided") { ?>
										<a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick = 'deleteConfirm( "checks.php?action=unvoid&id=<?php echo $row["Check_ID"]; ?>&num=<?php echo $row["Check_Number"]; ?>", "Unvoid")' role="button">Unvoid</a>
										<?php } else { ?>
										<a class="btn btn-primary btn-sm pt-1 pb-1" href="check-edit.php?id=<?php echo $row["Check_ID"]; ?>" role="button" id="dropdownMenuLink">Edit</a>
										<button type="button" class="btn btn-sm btn-primary dropdown-toggle dropdown-toggle-split pt-1 pb-1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<div class="dropdown-menu">
											<a class="dropdown-item" href="inc/checks-print-pdf.php?checkid=<?php echo $row["Check_ID"]; ?>" target="_blank">Print</a>
											<?php if($row["Check_Cashed"] == 0) { ?><a class="dropdown-item" href="javascript:void(0);" onclick = 'deleteConfirm( "checks.php?action=cashed&id=<?php echo $row["Check_ID"]; ?>&num=<?php echo $row["Check_Number"]; ?>", "Cashed")'>Cashed</a><?php } ?>
											<a class="dropdown-item" href="javascript:void(0);" onclick = 'deleteConfirm( "checks.php?action=void&id=<?php echo $row["Check_ID"]; ?>&num=<?php echo $row["Check_Number"]; ?>", "Void")'>Void</a>
											<a class="dropdown-item" href="javascript:void(0);" onclick = 'deleteConfirm( "checks.php?action=delete&id=<?php echo $row["Check_ID"]; ?>&num=<?php echo $row["Check_Number"]; ?>", "Delete")'>Delete</a>
										</div>
										<?php } ?>
									</div>
								</td>
							</tr>
						<?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </form>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<!-- [ page content ] end -->


<script>
$(document).ready(function() {

    //Just to move the search box to be on the top of the card section
    $('.outsideBorderSearch').on( 'keyup click', function () {
        $('#TableWithNoButtons').DataTable().search(
            $('.outsideBorderSearch').val()
        ).draw();
    });
});

$(document).ready(function() {
    $('#TableWithNoButtons').DataTable( {
        //"ordering": false, //To disable the ordering of the tables
		"bLengthChange": false, //To hide the Show X entries dropdown
		dom: 'Bfrtip',
		sDom: 'lrtip', //To Hide the search box
        buttons: [], //To hide the export buttons
		"order": [[ 0, "desc" ]],
        "initComplete": function(settings, json) {
            delayedBindingCheckbox();
        }
    } );
} );

function doReload(status){
	document.location = 'checks.php?status=' + status;
}
    
function batchSubmit(obj) {
    var count = $('.gst-invoice-cb:checked').length ;
    if( count > 0 ) {
        obj.form.target = '_blank' ;
        if( obj.value == 'V' ) {
            msg = "Are you sure you want to void " + count + " check(s) ?" ;
        } else if( obj.value == 'P' ) {
            msg = "Are you sure you want to print " + count + " check(s) ?" ;
        } else if( obj.value == 'D' ) {
            msg = "Are you sure you want to delete " + count + " check(s) ?" ;
        } else {
            return ;
        }
        showConfirm( "javascript:CheckListForm.submit();", "Yes", msg, "Please Confirm", "modal-md") ;
        //obj.form.submit();
    }
}
</script>
<?php
global $GLOBAL_SCRIPT ;
$GLOBAL_SCRIPT .= "bindCheckAll('.gst-invoice-cb-parent', '.gst-invoice-cb-children');" ;
?>
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>