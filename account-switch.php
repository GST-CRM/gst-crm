<?php

include "session.php";
$PageTitle = "Welcome";
include_once __DIR__ . '/models/users.php';

$bodyclass = "";
if (!isset($_SESSION['login_user'])) {
    header("location:login.php");
} else {

    Users::switchToRoleByLabel($_REQUEST['account-switch']);

    header("location:dashboard.php");
}
?>
