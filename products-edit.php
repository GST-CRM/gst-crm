<?php include "session.php";

$productid = $_GET["id"];

//To collect the data of the above Product id, and show it in the fields below
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {die("Connection failed: " . $conn->connect_error);} 
$sql = "SELECT * FROM products WHERE id=$productid";
$result = $conn->query($sql);
$product = $result->fetch_assoc();
$conn->close();


// Hay el general header information fe kol el pages
$PageTitle = "Edit The Product ".$product["name"];
include "header.php";

?>
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->

<form name="contact0" action="inc/group-functions.php" method="POST" id="contact0" class="row">
    <div class="row">
		<div class="col-md-9">
            <div class="card">
                <div class="card-header">
                    <h5>Edit Product</h5>
                </div>
				<div class="card-block  row">
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label">Product ID</label>
                        <input type="text" name="productid" class="disabled form-control" value="<?php echo $product['id'];?>" readonly>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-6">
                        <label class="float-label">Product Name</label>
                        <input type="text" name="name" value="<?php echo $product['name'];?>" class="form-control" required="">
                    </div>
					<div class="form-group form-default form-static-label col-sm-3" style="display:<?php if($product['categoryid'] == "1"){ echo "none";}?>;">
                        <div class="checkbox-fade fade-in-success">
						<label class="float-label">Is it a Sub-Product?</label>
                            <label>
                                <input type="checkbox" name="subproduct" id="subproduct" value="1" <?php if($product['subproduct'] == "1"){echo "checked";} ?>>
                                <span class="cr">
                                    <i class="cr-icon fas fa-check txt-success"></i>
                                </span>
                                <span>Yes, it is</span>
                            </label>
                        </div>
					</div>
                    <div class="form-group form-default form-static-label col-sm-12">
                        <label class="float-label">Product Description</label>
                        <input type="text" name="description" class="form-control" value="<?php echo $product['description'];?>">
                    </div>
					<div id="parentcategory" class="col-sm-3" style="display:<?php if($product['subproduct'] == "0"){ echo "none";}?>;">
						<label class="float-label">Parent Product</label>
						<select name="parentid" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<?php $con9 = new mysqli($servername, $username, $password, $dbname);
							$result9 = mysqli_query($con9,"SELECT * FROM products WHERE parentid=0");
							while($row9 = mysqli_fetch_array($result9))
							{
								$selected = '';
								if($product['parentid'] == $row9['id']) {echo $selected = 'selected';}
								echo "<option value='".$row9['id']."' $selected>".$row9['name']."</option>";
							} ?>
						</select>
					</div>
					<div id="producttypediv" class="form-group form-default form-static-label col-sm-3" style="display:<?php if($product['subproduct'] == "0"){ echo "none";}?>;">
						<label class="float-label">Product Type</label>
						<select name="producttype" id="producttype" class="form-control form-control-default">
							<option value="0"> </option>
							<option id="Airfare" value="Airfare" <?php if($product['producttype'] == "Airfare"){echo "selected";} ?>>Airfare</option>
							<option id="Hotels" value="Hotels" <?php if($product['producttype'] == "Hotels"){echo "selected";} ?>>Hotels</option>
							<option id="Restaurants" value="Restaurants" <?php if($product['producttype'] == "Restaurants"){echo "selected";} ?>>Restaurants</option>
							<option id="Guides" value="Guides" <?php if($product['producttype'] == "Guides"){echo "selected";} ?>>Tour Guides</option>
							<option id="Insurance" value="Insurance" <?php if($product['producttype'] == "Insurance"){echo "selected";} ?>>Insurance</option>
						</select>
					</div>
                    <div id="productcost" class="form-group form-default form-static-label col-sm-3" style="display:<?php if($product['subproduct'] == "1"){ echo "none";}?>;">
                        <label class="float-label">Purchase Cost $</label>
                        <div class="dollar"><input type="text" name="cost" class="form-control" value="<?php echo $product['cost'];?>" style="display:<?php if($product['subproduct'] == "1"){ echo "none";}?>;"></div>
                    </div>
                    <div id="productprice" class="form-group form-default form-static-label col-sm-3" style="display:<?php if($product['subproduct'] == "1"){ echo "none";}?>;">
                        <label class="float-label">Selling Price $</label>
                        <div class="dollar"><input type="text" name="price" class="form-control" value="<?php echo $product['price'];?>" style="display:<?php if($product['subproduct'] == "1"){ echo "none";}?>;"></div>
                    </div>
                    <div id="purchasedate" class="form-group form-default form-static-label col-sm-3" style="display:<?php if($product['subproduct'] == "1"){ echo "none";}?>;">
                        <label class="float-label">Purchase Date</label>
                        <input type="date" name="purchasedate" class="form-control" value="<?php echo $product['purchasedate'];?>">
                    </div>
					<div id="productterms" class="form-group form-default form-static-label col-sm-3" style="display:<?php if($product['subproduct'] == "1"){ echo "none";}?>;">
						<label class="float-label">Payment Term</label>
						<select name="paymentterm" class="form-control form-control-default">
							<option value="1" <?php if($product['paymentterm'] == "1"){echo "selected";} ?>>Lump Sum</option>
							<option value="2" <?php if($product['paymentterm'] == "2"){echo "selected";} ?>>Per Person</option>
							<option value="3" <?php if($product['paymentterm'] == "3"){echo "selected";} ?>>Per Day</option>
						</select>
					</div>
					<!-- START // To check if there is a Single Supplement Product for this Land Product or not -->
					<div id="SingleSupplementCheck" class="col-sm-4" style="display:<?php if($product['categoryid'] != 2 OR $product['subproduct'] != 0){ echo "none";}?>;">
						<h4 class="sub-title">Single Supplement</h4>
						<div class="can-toggle">
							<input id="SingleSupplementCheckButton" name="SingleSupplementCheckButton" value="1" type="checkbox" <?php if($product['Product_Reference'] > 0 AND $product['categoryid'] == 2) {echo "checked";} ?>>
							<label for="SingleSupplementCheckButton">
								<div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
							</label>
						</div>
					</div>
					<?php if($product['Product_Reference'] > 0) {
							$SingleProID = $product['Product_Reference'];
							$GetSingleProSQL = "SELECT id,cost,price FROM products WHERE id=$SingleProID";
							$GetSingleProResult = $db->query($GetSingleProSQL);
							$SingleProData = $GetSingleProResult->fetch_assoc();
					} ?>
					<div id="SingleSupplementBox" class="col-sm-8 row" style="display:<?php if(!($product['Product_Reference'] > 0 AND $product['categoryid'] == 2)){ echo "none";}?>;">
						<div id="SingleSuppCost" class="col-sm-6" style="display:<?php if($product['subproduct'] == "1" OR $product['categoryid'] != 2){ echo "none";}?>;">
							<label class="float-label">Single Supplement Cost</label>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">$</span>
								</div>
								<input type="text" name="SingleProID" class="form-control" value="<?php echo $product['Product_Reference'];?>" hidden>
								<input type="text" name="SingleSupplier" class="form-control" value="<?php echo $product['supplierid'];?>" hidden>
								<input type="text" name="SingleName" class="form-control" value="<?php echo $product['name'];?> | Single Supplement" hidden>
								<input type="text" name="SingleSuppCost" class="form-control" value="<?php echo $SingleProData['cost'];?>" style="display:<?php if($product['subproduct'] == "1"){ echo "none";}?>;">
							</div>
						</div>
						<div id="SingleSuppPrice" class="col-sm-6" style="display:<?php if($product['subproduct'] == "1" OR $product['categoryid'] != 2){ echo "none";}?>;">
							<label class="float-label">Single Supplement Price</label>
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">$</span>
								</div>
								<input type="text" name="SingleSuppPrice" class="form-control" value="<?php echo $SingleProData['price'];?>" style="display:<?php if($product['subproduct'] == "1"){ echo "none";}?>;">
							</div>
						</div>
                    </div>
					<!-- END // Single Supplement Section -->
                    <div id="hotelphone" class="form-group form-default form-static-label col-sm-3" style="display:<?php if($product['subproduct'] == "0"){ echo "none";}?>;">
                        <label class="float-label">Hotel's Phone</label>
                        <input type="text" name="hotelphone" class="form-control" value="<?php echo $product['meta1'];?>">
                    </div>
                    <div id="hotelcity" class="form-group form-default form-static-label col-sm-3" style="display:<?php if($product['subproduct'] == "0"){ echo "none";}?>;">
                        <label class="float-label">Hotel's City</label>
                        <input type="text" name="hotelcity" class="form-control" value="<?php echo $product['meta2'];?>">
                    </div>
                    <div id="checkindate" class="form-group form-default form-static-label col-sm-3" style="display:<?php if($product['subproduct'] == "0"){ echo "none";}?>;">
                        <label class="float-label">Check in Date</label>
                        <input type="date" name="checkindate" class="form-control" value="<?php echo $product['meta3'];?>">
                    </div>
                    <div id="checkoutdate" class="form-group form-default form-static-label col-sm-3" style="display:<?php if($product['subproduct'] == "0"){ echo "none";}?>;">
                        <label class="float-label">Check out Date</label>
                        <input type="date" name="checkoutdate" class="form-control" value="<?php echo $product['meta4'];?>">
                    </div>
                    <div id="departurecity" class="form-group form-default form-static-label col-sm-3" style="display:<?php if($product['subproduct'] == "0"){ echo "none";}?>;">
                        <label class="float-label">Departure City</label>
                        <input type="text" name="departurecity" class="form-control" value="<?php echo $product['meta1'];?>">
                    </div>
                    <div id="departurestate" class="form-group form-default form-static-label col-sm-3" style="display:<?php if($product['subproduct'] == "0"){ echo "none";}?>;">
                        <label class="float-label">Departure State</label>
                        <input type="text" name="departurestate" class="form-control" value="<?php echo $product['meta2'];?>">
                    </div>
                    <div id="GuidePhone" class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label">Tour Guide Phone #</label>
                        <input type="text" name="GuidePhone" value="<?php echo $product['meta1'];?>" class="form-control">
                    </div>
                    <div id="GuideCity" class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label">Tour Guide City</label>
                        <input type="text" name="GuideCity" value="<?php echo $product['meta2'];?>" class="form-control">
                    </div>
<?php if($product['categoryid'] == "1"){?>
                    <div class="form-group form-default form-static-label col-sm-12">
                        <label class="float-label">Flight Info</label>
                        <input type="text" name="AirlineActive" class="disabled form-control" value="1" hidden>
                        <textarea class="form-control" rows="5" name="Flight_info" id="Flight_info" placeholder="This is a sample flight info only. Please replace it with the correct one!
LH 443 L 15FEB 5 DTWFRA HK45  420P 625A 16FEB  E  LH/TG3IX3
LH 686 L 16FEB 6 FRATLV HK45 1005A 315P 16FEB  E  LH/TG3IX3
LH 691 L 24FEB 7 TLVFRA HK45  520A 900A 24FEB  E  LH/TG3IX3
LH 442 L 24FEB 7 FRADTW HK45 1055A 215P 24FEB  E  LH/TG3IX3"><?php echo $product["Flight_info"]; ?></textarea>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label">Airline Confirmation</label>
                        <input type="text" name="Confirmation" class="disabled form-control" value="<?php echo $product['Confirmation'];?>">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label">Returned Amount</label>
                        <div class="dollar"><input type="text" name="Returned_Amount" class="disabled form-control" value="<?php echo $product['Returned_Amount'];?>"></div>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label">Deposit Amount</label>
                        <div class="dollar"><input type="text" name="Deposit_Amount" class="disabled form-control" value="<?php echo $product['Deposit_Amount'];?>"></div>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label">Seats</label>
                        <input type="text" name="seats" class="disabled form-control" value="<?php echo $product['seats'];?>">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label">Deposit Return Date</label>
                        <input type="date" name="Deposit_Return_Date" class="disabled form-control" value="<?php echo $product['Deposit_Return_Date'];?>">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label">Deposit Date</label>
                        <input type="date" name="Deposit_Date" class="disabled form-control" value="<?php echo $product['Deposit_Date'];?>">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label">Cancel Date</label>
                        <input type="date" name="Cancel_Date" class="disabled form-control" value="<?php echo $product['Cancel_Date'];?>">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label">Names Due Date</label>
                        <input type="date" name="Names_Due" class="disabled form-control" value="<?php echo $product['Names_Due'];?>">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label">Final Payment Due Date</label>
                        <input type="date" name="Final_Payment_Due" class="disabled form-control" value="<?php echo $product['Final_Payment_Due'];?>">
                    </div>
					<?php if($product['Product_Reference'] > 0 AND $product['categoryid'] == 1) {
						//If there is already a Ticket Discount Product & Show the amount text field
						$TicketAmountDisplay = "";
						$TicketButtonDisplay = "style='display:none;'";
						$ItIsChecked = "checked";
						
						$TicketProID = $product['Product_Reference'];
						$GetTicketProSQL = "SELECT id,cost,price FROM products WHERE id=$TicketProID";
						$GetTicketProResult = $db->query($GetTicketProSQL);
						$TicketProData = $GetTicketProResult->fetch_assoc();
					} else {
						//If there isn't a product yet & Show the button
						$TicketButtonDisplay = "";
						$TicketAmountDisplay = "style='display:none;'";
						$TicketProID = 0;
						$ItIsChecked = "";
					} ?>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label text-danger">Airline Credit Amount</label>
                        <input type="text" name="Airline_Credit_Product" class="form-control" value="<?php echo $TicketProID;?>" hidden>
                        <input type="checkbox" id="Airline_Credit_Check" name="Airline_Credit_Check" class="form-control" value="1" hidden <?php echo $ItIsChecked; ?>>
						<div id="TicketDiscountField" <?php echo $TicketAmountDisplay; ?> class="input-group mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text text-danger">-$</span>
							</div>
							<input type="text" name="Airline_Credit_Amount" class="form-control" value="<?php echo abs($TicketProData['price']);?>">
						</div>
                        <button type="button" id="TicketDiscountButton" <?php echo $TicketButtonDisplay; ?> class="btn btn-primary">Create a Product</button>
                    </div>
<?php } ?>
                    <div class="col-sm-12"><br />
					<button type="submit" name="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Update</button>
                    <button type="reset" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Clear</button>
					</div>
				</div>
            </div>
        </div>
		<div class="col-md-3">
            <div class="card">
                <div class="card-header">
                    <h5>Required Information</h5>
                </div>
                <div class="card-block  row">
                    <div class="form-group col-sm-12">
                        <h4 class="sub-title">Product Status</h4>
						<div class="can-toggle">
							<input id="e" name="status" value="1" type="checkbox" <?php if($product['status']==1){echo "checked";} ?>>
							<label for="e">
								<div class="can-toggle__switch" data-checked="Active" data-unchecked="Inactive"></div>
							</label>
						</div>
                    </div>
                    <div class="col-sm-12">
						<h4 class="sub-title">Product's Supplier</h4>
						<select name="thesupplier" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<?php //here, we are checking the Agent ID with the contact column list to show their names
							$con7 = new mysqli($servername, $username, $password, $dbname);
							$result7 = mysqli_query($con7,"SELECT id, fname, mname, lname FROM contacts WHERE `usertype` LIKE '%2%'");
							while($row7 = mysqli_fetch_array($result7))
							{
								
								$selected = '';
								if($product['supplierid'] == $row7['id']) {echo $selected = 'selected';}
								echo "<option value='".$row7['id']."' $selected>".$row7['fname']." ".$row7['mname']." ".$row7['lname']."</option>";
							} ?>
						</select>
                    </div>
                    <div class="col-sm-12"><br />
						<h4 class="sub-title">Product's Category</h4>
						<select name="thecategory" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<?php //here, we are checking the Agent ID with the contact column list to show their names
							$con8 = new mysqli($servername, $username, $password, $dbname);
							$result8 = mysqli_query($con8,"SELECT * FROM products_category");
							while($row8 = mysqli_fetch_array($result8))
							{
								$selected = '';
								if($product['categoryid'] == $row8['id']) {echo $selected = 'selected';}
								echo "<option value='".$row8['id']."' $selected>".$row8['name']."</option>";
							} ?>
						</select>
                    </div>
				</div>
			</div>
		</div>
	</div>
</form>

<?php include 'inc/notificiations.php'; ?>

<script type="text/javascript">
//This is so it can update the data without refresh from the inc folder
$(document).ready(function() {
	$("form").submit(function() {
		// Getting the form ID
		var  formID = $(this).attr('id');
		var formDetails = $('#'+formID);
		//To not let the form of uploading attachments included in this
		if (formID != 'contact6') {
			$.ajax({
				type: "POST",
				url: 'inc/group-functions.php',
				data: formDetails.serialize(),
				success: function (data) {	
					// Inserting html into the result div
					$('#results').html(data);
					//$("form")[0].reset();
					formmodified = 0;
				},
				error: function(jqXHR, text, error){
				// Displaying if there are any errors
					$('#result').html(error);           
				}
			});
			return false;
		}
	});
});

//This is so it can show the new field where you can create a new product type
function admSelectCheck(nameSelect)
{
    if(nameSelect){
        admOptionValue = document.getElementById("admOption").value;
        if(admOptionValue == nameSelect.value){
            document.getElementById("producttypenew").style.display = "block";
            document.getElementById("producttype").style.display = "none";
        }
        else{
            document.getElementById("producttypenew").style.display = "none";
        }
    }
    else{
        document.getElementById("producttypenew").style.display = "none";
    }
}

//This is so it can show or hide fields depending on the field of is it subcategory or not
$(function () {
        $("#subproduct").click(function () {
            if ($(this).is(":checked")) {
                $("#parentcategory").show("slow");
                $("#producttypediv").show("slow");
                $("#productcost").hide("slow");
                $("#productestcost").hide("slow");
                $("#purchasedate").hide("slow");
                $("#productprice").hide("slow");
                $("#productterms").hide("slow");
                $("#SingleSupplementCheck").hide("slow");
                $("#SingleSupplementBox").hide("slow");
                $("#SingleSuppCost").hide("slow");
                $("#SingleSuppPrice").hide("slow");
            } else {
                $("#parentcategory").hide("slow");
                $("#producttypediv").hide("slow");
                $("#productcost").show("slow");
                $("#productestcost").show("slow");
                $("#purchasedate").show("slow");
                $("#productprice").show("slow");
                $("#productterms").show("slow");
                $("#SingleSupplementCheck").show("slow");
                $("#SingleSupplementBox").show("slow");
                $("#SingleSuppCost").show("slow");
                $("#SingleSuppPrice").show("slow");
            }
        });
    });
	
//This is so it can show or hide the fields of the single supplement
$(function () {
        $("#SingleSupplementCheckButton").click(function () {
            if ($(this).is(":checked")) {
                $("#SingleSupplementBox").show("slow");
                $("#SingleSuppCost").show("slow");
                $("#SingleSuppPrice").show("slow");
            } else {
                $("#SingleSupplementBox").hide("slow");
                $("#SingleSuppCost").hide("slow");
                $("#SingleSuppPrice").hide("slow");
            }
        });
    });
	
//This is so it can show or hide the fields of the Airline Ticket Discount
$(function () {
        $("#TicketDiscountButton").click(function () {
                $("#TicketDiscountField").show("slow");
                $("#TicketDiscountButton").hide();
                document.getElementById("Airline_Credit_Check").checked = true;
        });
    });

//This is so it can show or hide fields when the product type is selected, like hotels, restaurants, ...
$(function() {
  $("#producttype").change(function() {
    if ($("#Hotels").is(":selected")) {
      $("#hotelcity").show("slow");
      $("#checkindate").show("slow");
      $("#checkoutdate").show("slow");
    } else {
      $("#hotelcity").hide("slow");
      $("#checkindate").hide("slow");
      $("#checkoutdate").hide("slow");
    }
    if ($("#Airfare").is(":selected")) {
      $("#departurestate").show("slow");
      $("#departurecity").show("slow");
    } else {
      $("#departurestate").hide("slow");
      $("#departurecity").hide("slow");
    }
    if ($("#Guides").is(":selected")) {
      $("#GuidePhone").show("slow");
      $("#GuideCity").show("slow");
    } else {
      $("#GuideCity").hide("slow");
      $("#GuidePhone").hide("slow");
    }
  }).trigger('change');
});
</script>

<!-- sweet alert js -->
<script type="text/javascript" src="files/bower_components/sweetalert/js/sweetalert.min.js"></script>
<!-- modalEffects js nifty modal window effects -->
<script type="text/javascript" src="files/assets/js/modalEffects.js"></script>

<?php include "footer.php"; ?>