<?php
if( isset($_GET['debug']) ) {
	$time = microtime();
	$time = explode(' ', $time);
	$time = $time[1] + $time[0];
	$start = $time;
}

require_once "inc/helpers.php"; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<title><?php echo $PageTitle; ?> | GST Admin Panel</title>
	<!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 10]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="description" content="GST CRM is a travel agency management system customised Specially for GST to fulfil their needs for better performance." />
	<meta name="keywords" content="CRM, travel, agents, suppliers, tours, groups, emails, management, agency">
	<meta name="author" content="Khader Handal" />
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<!-- Favicon icon -->
	<link rel="icon" href="files/assets/images/favicon.ico" type="image/x-icon">
    <style>
	div.hidden {
	   display: none
	}
    @media screen and (max-width: 568px) {
	  .btn { 
	  	font-size: 11px !important; 
	  	padding: 7px 4px !important;
	  }
	  .form-group .btn {
		  text-align: center !important;
	  }
	}
    </style>
    <link rel="stylesheet" type="text/css" href="files/assets/css/c-r.css">
    <script type="text/javascript" src="files/bower_components/jquery/js/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="files/assets/css/custom.css">
	<!-- Date & Time Picker for Group Edit Page -->
	<link href="files/DateTimeDropdown/bootstrap-datatimepicker-main.css" rel="stylesheet" media="screen">
    <link href="files/DateTimeDropdown/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	<script type="text/javascript" src="files/DateTimeDropdown/bootstrap-datetimepicker.js" charset="UTF-8"></script>
	<!-- Google font-->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700" rel="stylesheet">
</head>
<body>
	<?php if( defined('LOGIN_TEMPLATE') ) { ?>
    <script type="text/javascript" src="files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script> 
	<?php } else { ?>
    <!-- layout design -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
            <!-- [ Header ] start -->
			<?php include "header-profile.php"; ?>

            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">

                    <!-- [ navigation menu ] start -->
					<?php include 'sidebar.php'; ?>
                    <!-- [ navigation menu ] end -->
                    <div class="pcoded-content">
                        
                        <!-- [ breadcrumb ] start -->
						<?php include 'header-breadcrumb.php'; ?>
                        
                        <!-- [ breadcrumb ] end -->
						<div class="pcoded-inner-content">
							<div class="main-body">
                                
								<div class="page-wrapper">
									<div class="page-body">
                                        <?php echo GUtils::flashMessage() ; ?>
	<?php } ?>
