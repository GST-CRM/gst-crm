<?php
include "session.php";
$PageTitle = "Manage the Charts of Accounts";
include "header.php"; 

//To show the Void Message
if ($_GET['action'] == "void") {
	$CoA_ID = $_GET['CoA_id'];
	$Query = "UPDATE gl_chart_of_accounts SET GL_Account_Status=0 WHERE Account_ID=$CoA_ID";
	if ($db->multi_query($Query) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected Chart of Account has been voided successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error voiding Chart of Account: " . $db->error . "</strong></div>";
	}
}

//To show the Un-Void Message
if ($_GET['action'] == "unvoid") {
	$CoA_ID = $_GET['CoA_id'];
	$Query = "UPDATE gl_chart_of_accounts SET GL_Account_Status=1 WHERE Account_ID=$CoA_ID";
	if ($db->multi_query($Query) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected Chart of Account has been un-voided successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error un-voiding Chart of Account: " . $db->error . "</strong></div>";
	}
}

//To show the Delete Message
if ($_GET['action'] == "delete") {
	$CoA_ID = $_GET['CoA_id'];
	$Query = "DELETE FROM gl_chart_of_accounts WHERE Account_ID=$CoA_ID";
	if ($db->multi_query($Query) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected Chart of Account has been deleted successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting Chart of Account: " . $db->error . "</strong></div>";
	}
}

?>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
        <div class="card">
            <div class="row">
				<div class="col-md-6">
					<div class="card-header table-card-header">
						<h5>Charts of Accounts</h5>
					</div>
				</div>
                <div class="col-md-6 ">
                    <div class="card-header table-card-header float-right">
                        <select style="width: 200px;" id="coaFilter" name="month" class=" float-left form-control mr-1 form-control-default fill" >
                            <option value="">Active</option>
                            <option value="voided" <?php if($_GET['status'] == 'voided') {echo "selected";} ?>>Voided</option>
                        </select>
                        <button type="button" style="padding: 5px 13px;" class="btn-info btn mr-0" 
                                onClick="filterCoaStatus(document.getElementById('coaFilter'));" >Filter
                        </button>
                    </div>
                </div>
<!--				<div class="col-md-3" style="margin-top:15px;">
					<div>
						<a href="expenses-CoA.php?status=voided" class="btn btn-mat waves-effect waves-light btn-inverse float-right" style="display: none; margin-right: 30px; padding: 3px 13px;">Voided Chart of Accounts</a>
						<a href="expenses-payee.php" class="btn btn-mat waves-effect waves-light btn-inverse float-right" style="display:none;margin-right: 5px; padding: 3px 13px;">Active Payees</a>
					</div>
				</div>-->
			</div>
            <div class="card-block">
			<style>.PayeeNameField {background:#f2f2f2;} .PayeeNameField:active,.PayeeNameField:focus {background:white;}</style>
                <div class="dt-responsive table-responsive">
					<div class="table-responsive">
						<div align="right">
							<button type="button" name="add" id="add" class="btn btn-success">Add</button><br /><br />
						</div>
						<div id="alert_message"></div>
						<table id="user_data" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>GL Account Num</th>
									<th>GL Account Name</th>
									<th>Chart of Account</th>
									<th>Modified Time</th>
									<th>Actions</th>
								</tr>
							</thead>
						</table>
					</div>
                </div>
            </div>
<script type="text/javascript" language="javascript" >
 $(document).ready(function(){
	fetch_data();
	<?php $PayeeStatus = $_GET['status']; if($PayeeStatus != NULL) {$Payee_Where_URL = "?status=voided";} ?>
  function fetch_data()
  {
   var dataTable = $('#user_data').DataTable({
    "processing" : true,
    "serverSide" : true,
    "ordering": false,
    "ajax" : {
     url:"inc/CoA-fetch.php<?php echo $Payee_Where_URL; ?>",
     type:"POST"
    }
   });
  }
  
  function update_data(id, column_name, value)
  {
   $.ajax({
    url:"inc/CoA-update.php",
    method:"POST",
    data:{id:id, column_name:column_name, value:value},
    success:function(data)
    {
     $('#alert_message').html('<div class="alert alert-success background-success" role="alert">'+data+'</div>');
     $('#user_data').DataTable().destroy();
     fetch_data();
    }
   });
   setInterval(function(){
    $('#alert_message').html('');
   }, 10000);
  }

  $(document).on('blur', '.update', function(){
   var id = $(this).data("id");
   var column_name = $(this).data("column");
   var value = $(this).val();
   update_data(id, column_name, value);
  });

<?php
	$Gl_Group_Select = '<select id="data1" name="GL_Group_Select" class="form-control exp_category" onchange="NewCoACategory(this);"><option value="">Select Category</option><option id="AddNewCategory" value="New">Add New Category</option>';
	$GLaccountResult = mysqli_query($db,"SELECT GL_Group_ID,GL_Group_Num,GL_Group_Name FROM gl_account_groups WHERE GL_Group_Status=1 AND GL_Account_Classes_Class_ID=6");
	while($GLaccountData = mysqli_fetch_array($GLaccountResult)) {
	$Gl_Group_Select .='<option value="'.$GLaccountData["GL_Group_Num"].'">'.$GLaccountData["GL_Group_Name"].'</option>';
	}
	$Gl_Group_Select .= '</select>';
?>

	$('#add').click(function(){
   var html = '<tr>';
   html += '<td>-</td>';
   //html += '<td contenteditable id="data2">Write the Payee Name</td>';
   html += '<td><?php echo $Gl_Group_Select; ?><div id="NewCategory" style="display:none;" class="input-group mb-0"><input id="New_Category" type="text" class="form-control" data-column="New_Category" placeholder="Write the New Category Here"><div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="far fa-edit"></i></span></div></div></td>';
   html += '<td><div class="input-group mb-0"><input id="data2" type="text" class="form-control" data-column="Payee_Name" placeholder="Write the new name here"><div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="far fa-edit"></i></span></div></div></td>';
   html += '<td>-</td>';
   html += '<td><button type="button" name="insert" id="insert" class="btn btn-success btn-sm">Insert</button></td>';
   html += '</tr>';
   $('#user_data tbody').prepend(html);
  });
  
  $(document).on('click', '#insert', function(){
   var GL_Group_Num = $('#data1').val();
   var Chart_Account = $('#data2').val();
   var NewCategoryInput = $('#New_Category').val();
   if(Chart_Account != '')
   {
    $.ajax({
     url:"inc/CoA-add.php",
     method:"POST",
     data:{GL_Group_Num:GL_Group_Num,Chart_Account:Chart_Account,NewCategoryInput:NewCategoryInput},
     success:function(data)
     {
      $('#alert_message').html('<div class="alert alert-success background-success">'+data+'</div>');
      $('#user_data').DataTable().destroy();
      fetch_data();
     }
    });
    setInterval(function(){
     $('#alert_message').html('');
    }, 10000);
   }
   else
   {
    alert("Please fill the name of the Payee");
   }
  });
 });


function NewCoACategory(nameSelect)
{
    if(nameSelect){
        NewCategoryCoA = document.getElementById("AddNewCategory").value;
        if(NewCategoryCoA == nameSelect.value){
            document.getElementById("NewCategory").style.display = "";
            document.getElementById("data1").style.display = "none";
        }
        else{
            document.getElementById("data1").style.display = "block";
			document.getElementById("NewCategory").style.display = "none";
        }
    }
}

function filterCoaStatus(field) {
	location.href='expenses-CoA.php?status='+field.value;
}

</script>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<!-- [ page content ] end -->
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>