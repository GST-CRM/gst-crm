<!-- [ navigation menu ] start -->
<?php
$self = basename($_SERVER['PHP_SELF']);

$agentId = AclPermission::userId() ;

$menuDashboard = array(
    'dashboard.php' => AclPermission::pageAllowed('dashboard.php')
        );
$aclDashboard = AclPermission::anyPageAllowed(array_keys($menuDashboard)) ;

$menuContacts = array(
    'contacts.php' => AclPermission::pageAllowed('contacts.php'),
    'categories.php' => AclPermission::pageAllowed('categories.php'), 
    'categories-contacts.php' => AclPermission::pageAllowed('categories-contacts.php'),
    'leaders.php' => AclPermission::pageAllowed('leaders.php'), 
    'agents.php' => AclPermission::pageAllowed('agents.php'),
    'contacts-add.php' => AclPermission::pageAllowed('contacts-add.php'),
    'contacts-edit.php' => AclPermission::pageAllowed('contacts-edit.php')
        );
$aclContacts = AclPermission::anyPageAllowed(array_keys($menuContacts)) ;

$menuSales = array(
    'customers.php' => AclPermission::pageAllowed('customers.php'),
    'sales.php' => AclPermission::pageAllowed('sales.php'),
    'sales-edit.php' => AclPermission::pageAllowed('sales-edit.php'), 
    'payments.php' => AclPermission::pageAllowed('payments.php'),
    'sales-invoices.php' => AclPermission::pageAllowed('sales-invoices.php'), 
    'sales-invoice-add.php' => AclPermission::pageAllowed('sales-invoice-add.php'),
    'credit_notes.php' => AclPermission::pageAllowed('credit_notes.php'),
    'credit_notes_add.php' => AclPermission::pageAllowed('credit_notes_add.php'),
    'refund-receipts-list.php' => AclPermission::pageAllowed('refund-receipts-list.php'),
    'refund_receipts_add.php' => AclPermission::pageAllowed('refund_receipts_add.php'),
    'contacts-edit-direct.php' => AclPermission::pageAllowed('contacts-edit-direct.php')
        );
$aclSales = AclPermission::anyPageAllowed(array_keys($menuSales)) ;

$menuExpenses = array(
    'suppliers.php' => AclPermission::pageAllowed('suppliers.php'), 
    'expenses.php' => AclPermission::pageAllowed('expenses.php'), 
    'expenses-edit.php' => AclPermission::pageAllowed('expenses-edit.php'),
    'expenses-add.php' => AclPermission::pageAllowed('expenses-add.php'),
    'suppliers-payments.php' => AclPermission::pageAllowed('suppliers-payments.php'),
    'bills-add.php' => AclPermission::pageAllowed('bills-add.php'), 
    'bills-edit.php' => AclPermission::pageAllowed('bills-edit.php'),
    'suppliers-bill-payments-add.php' => AclPermission::pageAllowed('suppliers-bill-payments-add.php'),
    'bills.php' => AclPermission::pageAllowed('bills.php'),
    'expenses-payee.php' => AclPermission::pageAllowed('expenses-payee.php'),
    'expenses-CoA.php' => AclPermission::pageAllowed('expenses-CoA.php'), 
    'products.php' => AclPermission::pageAllowed('products.php'));
$aclExpenses = AclPermission::anyPageAllowed(array_keys($menuExpenses)) ;

$menuProducts = array(
    'products.php' => AclPermission::pageAllowed('products.php'), 
    'products-add.php' => AclPermission::pageAllowed('products-add.php'),
    'products-edit.php' => AclPermission::pageAllowed('products-edit.php')
        );
$aclProducts = AclPermission::anyPageAllowed(array_keys($menuProducts)) ;

$menuGroups = array(
    'groups.php' => AclPermission::pageAllowed('groups.php'), 
    'groups-add.php' => AclPermission::pageAllowed('groups-add.php'), 
    'groups-edit.php' => AclPermission::pageAllowed('groups-edit.php')
        );
$aclGroups = AclPermission::anyPageAllowed(array_keys($menuGroups)) ;

// $menuUsers = array(
//     'users.php', 'users-add.php', 'users-edit.php'
//         );
// $aclUsers = AclPermission::anyPageAllowed($menuUsers) ;

$menuAccounting = array(
    'accounting.php' => AclPermission::pageAllowed('accounting.php'),
    'sales-receipts.php' => AclPermission::pageAllowed('sales-receipts.php'),
    'expenses-CoA.php' => AclPermission::pageAllowed('expenses-CoA.php')
        );
$aclAccounting = AclPermission::anyPageAllowed(array_keys($menuAccounting)) ;

$menuReports = array(
    'reports.php' => AclPermission::pageAllowed('reports.php'),
    'reports-AR.php' => AclPermission::pageAllowed('reports-AR.php'),
    'reports-Recent.php' => AclPermission::pageAllowed('reports-Recent.php'),
    'reports-Deposit.php' => AclPermission::pageAllowed('reports-Deposit.php'),
    'reports-Deposit-Supplier.php' => AclPermission::pageAllowed('reports-Deposit-Supplier.php'),
    'reports-Financials.php' => AclPermission::pageAllowed('reports-Financials.php')
        );
$aclReports = AclPermission::anyPageAllowed(array_keys($menuReports)) ;

$menuSettings = array(
    'email-templates.php' => AclPermission::pageAllowed('email-templates.php'),
    'email-templates-add.php' => AclPermission::pageAllowed('email-templates-add.php'),
    'users.php' => AclPermission::pageAllowed('users.php'),
    'users-add.php' => AclPermission::pageAllowed('users-add.php'),
    'users-edit.php' => AclPermission::pageAllowed('users-edit.php'),
    'quotes.php' => AclPermission::pageAllowed('quotes.php'),
    'quotes-edit.php' => AclPermission::pageAllowed('quotes-edit.php'), 
    'quotes-add.php' => AclPermission::pageAllowed('quotes-add.php'),
    'cron-schedular.php' => AclPermission::pageAllowed('cron-schedular.php'),
    'cron-schedular-add.php' => AclPermission::pageAllowed('cron-schedular-add.php'),
    'cron-schedular-edit.php' => AclPermission::pageAllowed('cron-schedular-edit.php'),
    'checks.php' => AclPermission::pageAllowed('checks.php'),
    'check-banks.php' => AclPermission::pageAllowed('check-banks.php'),
    'check-company.php' => AclPermission::pageAllowed('check-company.php'),
    'check-add.php' => AclPermission::pageAllowed('check-add.php'),
    'check-bank-add.php' => AclPermission::pageAllowed('check-bank-add.php'),
    'check-bank-edit.php' => AclPermission::pageAllowed('check-bank-edit.php')
        );
$aclMenu = AclPermission::anyPageAllowed(array_keys($menuSettings)) ;

$menuCalendar = array(
    'calendar.php' => AclPermission::pageAllowed('calendar.php')
        );
$aclCalendar = AclPermission::anyPageAllowed(array_keys($menuCalendar)) ;

$menuLogs = array(
    'logs.php' => AclPermission::pageAllowed('logs.php'),
    'changelog.php' => AclPermission::pageAllowed('changelog.php')
        );
$aclLogs = AclPermission::anyPageAllowed(array_keys($menuLogs)) ;
?>
<?php
$IsCustomer = AclPermission::activeRoleId() == AclRole::$CUSTOMER  ? true : false ;
if(isset($IsCustomer) && $IsCustomer == 1) { $IsCustomer = 1; } else { $IsCustomer = 2; }
if($IsCustomer != 1 ) { ?>
<nav class="pcoded-navbar">
        <div id="MainMenu" class="pcoded-inner-navbar hidden">
            <ul class="pcoded-item pcoded-left-item">
                <li class="<?php
                if( ! $aclDashboard ) {
                    echo ' d-none ' ;
                }
                else if (in_array($self, array_keys($menuDashboard))) {
                    echo "active";
                }
                ?>">
                    <a href="dashboard.php" class="waves-effect waves-dark <?php echo ((!$menuDashboard['dashboard.php']) ? 'd-none' : '');?>">
                        <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                        <span class="pcoded-mtext">Home</span>
                    </a>
                </li>
                <li class="pcoded-hasmenu <?php
                if( ! $aclContacts ) {
                    echo ' d-none ' ;
                }
                else if (in_array($self, array_keys($menuContacts))) {
                    echo "active";
                }
                ?>">
                    <a href="javascript:void(0);" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-user"></i></span>
                        <span class="pcoded-mtext">Contacts</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li <?php echo ((!$menuContacts['contacts.php']) ? 'class="d-none"' : '');?>  >
                            <a href="contacts.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-user"></i></span>
                                <span class="pcoded-mtext">Contacts</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuContacts['categories.php']) ? 'class="d-none"' : '');?>  >
                            <a href="categories.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                                <span class="pcoded-mtext">Categories</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuContacts['agents.php']) ? 'class="d-none"' : '');?>  >
                            <a href="agents.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                                <span class="pcoded-mtext">Agents</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuContacts['leaders.php']) ? 'class="d-none"' : '');?>  >
                            <a href="leaders.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-sunrise"></i></span>
                                <span class="pcoded-mtext">Group Leaders</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="pcoded-hasmenu <?php
                if( ! $aclSales ) {
                    echo ' d-none ' ;
                }
                else if (in_array($self, array_keys($menuSales))) {
                    echo "active";
                }
                ?>">
                    <a href="javascript:void(0);" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                        <span class="pcoded-mtext">All Sales</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li <?php echo ((!$menuSales['sales.php']) ? 'class="d-none"' : '');?>  >
                            <a href="sales.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Sales</span>
                            </a>
                        </li>                                            
                        <li <?php echo ((!$menuSales['customers.php']) ? 'class="d-none"' : '');?>  >
                            <a href="customers.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shield"></i></span>
                                <span class="pcoded-mtext">Customers</span>
                            </a>
                        </li>                                            
                        <?php if( AclPermission::activeRoleId() == AclRole::$SUPPLIER ) { ?>
                        <li <?php echo ((!$menuSales['contacts-edit-direct.php']) ? 'class="d-none"' : '');?>  >
                            <a href="contacts-edit-direct.php?id=<?php echo $agentId;?>&action=edit&usertype=supplier" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shield"></i></span>
                                <span class="pcoded-mtext">Bills & Payments</span>
                            </a>
                        </li>                                            
                        <?php } ?>
                        <li <?php echo ((!$menuSales['sales-invoices.php']) ? 'class="d-none"' : '');?>  >
                            <a href="sales-invoices.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-file"></i></span>
                                <span class="pcoded-mtext">Invoices</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuSales['payments.php']) ? 'class="d-none"' : '');?>  >
                            <a href="payments.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Payments Received</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuSales['credit_notes.php']) ? 'class="d-none"' : '');?>  >
                            <a href="credit_notes.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Credit Notes</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuSales['refund-receipts-list.php']) ? 'class="d-none"' : '');?>  >
                            <a href="refund-receipts-list.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Refund Receipts</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="pcoded-hasmenu <?php
                if( ! $aclExpenses ) {
                    echo ' d-none ' ;
                }
                else if (in_array($self, array_keys($menuExpenses))) {
                    if (trim($self) != 'expenses-CoA.php') {
                        echo "active";
                    }
                }
                ?>">
                    <a href="javascript:void(0);" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-layers"></i></span>
                        <span class="pcoded-mtext">Expenses</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li class="" style="display:none;">
                            <a href="javascript:void(0)" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">All Expenses</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuExpenses['suppliers.php']) ? 'class="d-none"' : '');?> >
                            <a href="suppliers.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Suppliers</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuExpenses['bills.php']) ? 'class="d-none"' : '');?> >
                            <a href="bills.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Supplier Bills</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuExpenses['suppliers-payments.php']) ? 'class="d-none"' : '');?> >
                            <a href="suppliers-payments.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Supplier Payments</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuExpenses['expenses.php']) ? 'class="d-none"' : '');?> >
                            <a href="expenses.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">General Expenses</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuExpenses['expenses-payee.php']) ? 'class="d-none"' : '');?> >
                            <a href="expenses-payee.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Expense Payees</span>
                            </a>
                        </li>
						<li class="<?php
									if( ! $aclProducts ) {
										echo ' d-none ' ;
									}
									?>">
							<a href="products.php" class="waves-effect waves-dark">
								<span class="pcoded-micon"><i class="feather icon-layers"></i></span>
								<span class="pcoded-mtext">Products</span>
							</a>
						</li>
                    </ul>
                </li>
                <li class="<?php
                            if( ! $aclGroups ) {
                                echo ' d-none ' ;
                            }
                            else if (in_array($self, array_keys($menuGroups))) {
                                echo "active";
                            }
                            ?>">
                    <a href="groups.php" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-server"></i></span>
                        <span class="pcoded-mtext">Groups
<?php

$whereAgent = '' ;

if( AclPermission::roleFlags() & AclRole::$FLAG_ONLY_GROUP ) {
    $agentId = AclPermission::userId() ;
    $whereAgent = " AND CONCAT(',', agent, ',') LIKE '%,$agentId,%' " ;
}
else if( AclPermission::roleFlags() & AclRole::$FLAG_ONLY_MINE && AclPermission::activeRoleId() == AclRole::$LEADER ) {
    $agentId = AclPermission::userId() ;
    $whereAgent = " AND CONCAT(',', groupleader, ',') LIKE '%,$agentId,%' " ;
}
else if( AclPermission::roleFlags() & AclRole::$FLAG_ONLY_MINE && AclPermission::activeRoleId() == AclRole::$SUPPLIER ) {
    $agentId = AclPermission::userId() ;
    $whereAgent = " AND tourid IN(
        SELECT tourid FROM groups g
            JOIN products p ON (g.airline_productid=p.id OR g.land_productid=p.id)
        WHERE p.supplierid='$agentId' ) " ;
}
else if( AclPermission::roleFlags() & AclRole::$FLAG_ONLY_MINE && AclPermission::activeRoleId() == AclRole::$CUSTOMER ) {
    $agentId = AclPermission::userId() ;
    $whereAgent = " AND tourid IN( SELECT tourid FROM customer_account ca WHERE contactid='$loggedUserId' ) " ;
}
$grouppaxsql = "SELECT * FROM groups WHERE warnings=1 AND status=1 AND startdate >= CURDATE() $whereAgent ";

$grouppaxresult = $db->query($grouppaxsql);
?>
                    <?php if ($grouppaxresult->num_rows > 0) { ?>
                                <label class="badge bg-danger">
    <?php echo $grouppaxresult->num_rows; ?>
                                </label>
<?php } ?>
                        </span>
                    </a>
                </li>
                <li class="pcoded-hasmenu <?php
if( ! $aclAccounting ) {
    echo ' d-none ' ;
}
else if (in_array($self, array_keys($menuAccounting))) {
        echo "active";
}
?>">
                    <a href="javascript:void(0);" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i style="font-style: normal;">$</i></span>
                        <span class="pcoded-mtext">Accounting</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li <?php echo ((!$menuAccounting['expenses-CoA.php']) ? 'class="d-none"' : '');?> >
                            <a href="expenses-CoA.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-bookmark"></i></span>
                                <span class="pcoded-mtext">Chart of Accounts</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuAccounting['accounting.php']) ? 'class="d-none"' : '');?> >
                            <a href="accounting.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-bookmark"></i></span>
                                <span class="pcoded-mtext">Create</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li style="display:none;">
                    <a href="javascript:void(0)" class="disabled waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-book"></i></span>
                        <span class="pcoded-mtext">Media & Documents</span>
                    </a>
                </li>
                <li class="pcoded-hasmenu <?php
if( ! $aclReports ) {
echo ' d-none ' ;
}
else if (in_array($self, array_keys($menuReports))) {
    echo "active";
}
?>">
                    <a href="javascript:void(0);" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-pie-chart"></i></span>
                        <span class="pcoded-mtext">Reports</span>
                    </a>
                    <ul class="pcoded-submenu">
                         <li <?php echo ((!$menuReports['reports.php']) ? 'class="d-none"' : '');?> >
                            <a href="reports.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-pie-chart"></i></span>
                                <span class="pcoded-mtext">All Reports</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuReports['reports-AR.php']) ? 'class="d-none"' : '');?> >
                            <a href="reports-AR.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Account Receivable</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuReports['reports-Recent.php']) ? 'class="d-none"' : '');?> >
                            <a href="reports-Recent.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Recent Customers</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuReports['reports.php']) ? 'class="d-none"' : '');?> >
                            <a href="reports-SP.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Supplier Payable</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuReports['reports-Deposit.php']) ? 'class="d-none"' : '');?> >
                            <a href="reports-Deposit.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Customer Deposits</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuReports['reports-Deposit-Supplier.php']) ? 'class="d-none"' : '');?> >
                            <a href="reports-Deposit-Supplier.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Supplier Deposits</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuReports['reports-Financials.php']) ? 'class="d-none"' : '');?> >
                            <a href="reports-Financials.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Financial Report</span>
                            </a>
                        </li>
					</ul>
                </li>
                
                <li class="pcoded-hasmenu <?php
if( ! $aclMenu ) {
echo ' d-none ' ;
}
else if (in_array($self, array_keys($menuSettings))) {
    echo "active";
}
?>">
                    <a href="javascript:void(0);" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="icon feather icon-settings"></i></span>
                        <span class="pcoded-mtext">Settings</span>
                    </a>
                    <ul class="pcoded-submenu">
                        <li <?php echo ((!$menuSettings['users.php']) ? 'class="d-none"' : '');?> >
                            <a href="users.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-bookmark"></i></span>
                                <span class="pcoded-mtext">Users</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuSettings['email-templates.php']) ? 'class="d-none"' : '');?> >
                            <a href="email-templates.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Email Templates</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuSettings['quotes.php']) ? 'class="d-none"' : '');?> >
                            <a href="quotes.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Manage Quotes</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuSettings['cron-schedular.php']) ? 'class="d-none"' : '');?> >
                            <a href="cron-schedular.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Email Schedular</span>
                            </a>
                        </li>
                        <li <?php echo ((!$menuSettings['checks.php']) ? 'class="d-none"' : '');?> >
                            <a href="checks.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">List of Checks</span>
                            </a>
						</li>
						<li <?php echo ((!$menuSettings['check-company.php']) ? 'class="d-none"' : '');?> >
                            <a href="check-company.php?id=1" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Company Details</span>
                            </a>
						</li>
						<li <?php echo ((!$menuSettings['check-banks.php']) ? 'class="d-none"' : '');?> >
                            <a href="check-banks.php" class="waves-effect waves-dark">
                                <span class="pcoded-micon"><i class="feather icon-shopping-cart"></i></span>
                                <span class="pcoded-mtext">Financial Accounts</span>
                            </a>
						</li>
                    </ul>
                </li>

                <li class="<?php
if( ! $aclCalendar ) {
echo ' d-none ' ;
}
else if (in_array($self, array_keys($menuCalendar))) {
    echo "active";
}
?>">
                    <a href="calendar.php" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="fas fa-calendar-alt"></i></span>
                        <span class="pcoded-mtext">Calendar</span>
                    </a>
                </li>
                <li class="<?php
if( ! $aclLogs ) {
echo ' d-none ' ;
}
else if (in_array($self, array_keys($menuLogs))) {
    echo "active";
}
?>">
                    <a href="logs.php" class="waves-effect waves-dark">
                        <span class="pcoded-micon"><i class="feather icon-bookmark"></i></span>
                        <span class="pcoded-mtext">Logs</span>
                    </a>
                </li>
            </ul>
        </div>
</nav>
<?php } else { 
//You can remove this, it was only for the purposes of the customer screens
?>
<nav class="pcoded-navbar">
    <div id="MainMenu" class="pcoded-inner-navbar hidden">
        <ul class="pcoded-item pcoded-left-item">
            <li class="<?php if(isset($HomeTabActive) ) { echo $HomeTabActive; } ?>">
                <a href="dashboard.php?c" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                    <span class="pcoded-mtext">Home</span>
                </a>
            </li>
            <li class="<?php if(isset($DetailsTabActive) || isset($SalesTabActive) || isset($PaymentsTabActive) ) { echo 'active'; } ?>">
                <a href="customer-page-profile.php" class="waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="feather icon-check-square"></i></span>
                    <span class="pcoded-mtext">My Account</span>
                </a>
            </li>
            <li class="">
                <a href="my-account.php" class="disabled waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="feather icon-share"></i></span>
                    <span class="pcoded-mtext">Make a Payment</span>
                </a>
            </li>
            <li class="">
                <a href="my-account.php" class="disabled waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="feather icon-heart-on"></i></span>
                    <span class="pcoded-mtext">Add Testimonial</span>
                </a>
            </li>
            <li class="">
                <a href="my-account.php" class="disabled waves-effect waves-dark">
                    <span class="pcoded-micon"><i class="feather icon-settings"></i></span>
                    <span class="pcoded-mtext">Create a Support Ticket</span>
                </a>
            </li>
            
        </ul>
    </div>
</nav>
                
<?php } ?>
<!-- [ navigation menu ] end -->

<script type="text/javascript" src="files/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script> 
<script src="files/assets/js/pcoded.min.js"></script>

<script>
$('#mobile-collapse,#mobile-collapse1').on('click', function() {
	$('.pcoded-navbar').toggleClass('show-menu')
});

$( document ).ready(function() {
        $( "#pcoded" ).pcodedmenu({
        themelayout: 'horizontal',
        FixedNavbarPosition: true,
        FixedHeaderPosition: false,
    });
});
</script>
