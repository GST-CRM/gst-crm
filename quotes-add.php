<?php include "session.php";
$PageTitle = "Create Quote";
include "header.php";
?>
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->

<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="card">
            <div class="card-header">
                <h5>Create a New Quote</h5>
            </div>
            <div class="card-block">
				<form id="main" name="main" method="post" action="inc/quotes-functions.php" class="row">
					<div class="form-group form-default form-static-label col-sm-8">
						<label class="float-label">Group Name</label>
						<input type="text" name="Group_Name" class="form-control fill">
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Proposal Date</label>
						<input type="date" name="Proposal_Date" value="<?php echo date("Y-m-d"); ?>" class="form-control fill">
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Group Start Date</label>
						<input type="date" name="Start_Date" class="form-control fill">
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Group End Date</label>
						<input type="date" name="End_Date" class="form-control fill">
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Group Price</label>
						<input type="text" name="Group_Price" class="form-control fill">
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Single Supplement</label>
						<input type="text" name="Single_Supplement" class="form-control fill">
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Departure City</label>
						<input type="text" name="Departure_City" class="form-control fill">
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Tour Leader Name</label>
						<select name="TourleaderID" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<?php 
							$con6 = new mysqli($servername, $username, $password, $dbname);
							$result6 = mysqli_query($con6,"SELECT con.id,con.fname,con.mname,con.lname FROM group_leaders lea JOIN contacts con ON lea.contactid=con.id WHERE lea.status=1");
							while($row6 = mysqli_fetch_array($result6))
							{
								echo "<option value='".$row6['id']."'>".$row6['fname']." ".$row6['lname']."</option>";
								
							} ?>
						</select>
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Church Name</label>
						<input type="text" name="Church_Name" class="form-control fill">
					</div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Itinerary Title</label>
						<input type="text" name="Itinerary_Title" class="form-control fill">
					</div>
                    <div class="col-sm-12"><br /><br />
						<button type="submit" name="addquote" class="btn waves-effect waves-light btn-success mr-1" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Create</button>
                        <button type="reset" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Clear</button>
					</div>
				</form>
			</div>
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<?php require('inc/metadata-js.php'); ?>

<script type="text/javascript">
$(document).ready(function() {
    $("form").submit(function() {
        $('#results').html('');
        // Getting the form ID
        var  formID = $(this).attr('id');
        var formDetails = $('#'+formID);
        //To not let the form of uploading attachments included in this
        if (formID != 'contact6') {
            $.ajax({
                type: "POST",
                url: 'inc/quotes-functions.php',
                data: formDetails.serialize(),
                success: function (data) {  
                    // Inserting html into the result div
                    $('#results').html(data);
                    //$("form")[0].reset();
                    formmodified = 0;
                },
                error: function(jqXHR, text, error){
                // Displaying if there are any errors
                    $('#result').html(error);           
                }
            });
            return false;
        }
    });
});
</script>
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>