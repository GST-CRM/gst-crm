<?php
include "config.php";
session_start();
define('LOGIN_TEMPLATE', true);
$PageTitle = "Forgot Password";

if( ! empty($__POST) ) {
    include_once __DIR__ . '/inc/forgot_password_action.php';
}

include "header.php";



?>
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->
    <section class="login-block">
        <!-- Container-fluid starts -->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <!-- Authentication card start -->
                    <form name="contact3" id="contact3" class="md-float-material form-material" method="post">
                        <div class="text-center">
                            <img src="files/assets/images/auth/logo-dark.png" alt="logo.png">
                        </div>
                        <div class="auth-box card">
                            <div class="card-block">
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <h3 class="text-center">Forgot Password</h3>
                                    </div>
                                </div>
                                <div class="row m-b-20">
                                    <div class="col-md-12">
                                        <p>You will receive password reset information on your registered email address.</p>
                                    </div>
                                </div>
								<div id="results"><?php echo GUtils::flashMessage() ; ?></div>
                                
                                <div class="form-group form-primary">
                                    <input type="email" name="email" class="form-control" required="" autocomplete="off">
                                    <span class="form-bar"></span>
                                    <label class="float-label">Type Your Registered Email Address</label>
                                </div>
                                
                                <div class="row m-t-30">
                                    <div class="col-md-12 row">
                                        <button type="button" onclick="window.location.href='<?php echo GUtils::domainUrl();?>';"  name="cancel" class="btn col-md-4 btn-default btn-md waves-effect waves-light text-center m-b-20">Cancel</button> 
                                        <button type="submit" name="submit" class="btn m-l-10 col-md-7 btn-primary btn-md waves-effect waves-light text-center m-b-20">Recover Password</button>
                                    </div>
                                </div>
                                <hr/>
                                <div class="row">
                                    <div class="col-md-8">
                                        <p class="text-inverse text-left m-b-0">Thank you.</p>
                                        <p class="text-inverse text-left"><a href="https://www.tourtheholylands.com/"><b>Back to website</b></a></p>
                                    </div>
                                    <div class="col-md-4">
                                        <img src="files/assets/images/auth/Logo-small-bottom.png" alt="small-logo.png">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- end of form -->
                </div>
                <!-- end of col-sm-12 -->
            </div>
            <!-- end of row -->
        </div>
        <!-- end of container-fluid -->
    </section>
	
<?php include "footer.php"; ?>
