<?php include "session.php";
include_once __DIR__ . '/models/tour_membership.php';
include_once __DIR__ . '/models/groups.php' ;
include __DIR__ . '/models/groups_airline.php' ;

//error_reporting(0xFFFF) ;
//ini_set('display_errors', true);

if(isset($_REQUEST['email_report'])){
    if($_REQUEST['email_report']=="yes" && $_REQUEST['report_slug'] == "Manifest" ){
        include 'inc/manifest-email-report.php';
        
        $newUrl = GUtils::removeParameters(['email_report' => "yes"]) ;
        GUtils::redirect($newUrl) ;
    }
    else if($_REQUEST['email_report']=="yes" && $_REQUEST['report_slug'] == "Rooming" ){
        include 'inc/rooming-email-report.php';
        $newUrl = GUtils::removeParameters(['email_report' => "yes"]) ;
        GUtils::redirect($newUrl) ;
    }
    else if($_REQUEST['email_report']=="yes" && $_REQUEST['report_slug'] == "To-Date"){
        include 'inc/tour-leader-email-report.php';
        $newUrl = GUtils::removeParameters(['email_report' => "yes"]) ;
        GUtils::redirect($newUrl) ;
    }
}

//To make several url options in the same page
$allowed = array('update', 'edit', 'delete','deleteCustomer','makeagent','makesupplier','makeleader','docedit','docdelete');
if (!isset($_GET['action'])) {
	header("location: groups.php");die('Please go back to the main page.');
}
$action = $_GET['action'];
if (!in_array($action, $allowed)) {
	header("location: groups.php");
	die('Please go back to the main page.');
}

$tourid 	= (isset($_GET["id"]) ? $_GET["id"] : 0) ;
$tabstatus 	= (isset($_GET["tab"]) ? $_GET["tab"] : '') ;

//To make the current tab on URL ACTIVE

$mainactive = $paxactive = $landactive = $airactive = $reportsactive = $notesactive = $attachmentactive = $ExpensesActive = $invoiceactive = $confirmations = $sentemails = "";

if ($tabstatus == "participants") {
	$paxactive = "active";
} elseif($tabstatus == "land") {
	$landactive = "active";
} elseif($tabstatus == "airline") {
	$airactive = "active";
} elseif($tabstatus == "reports") {
	$reportsactive = "active";
} elseif($tabstatus == "attachments") {
	$attachmentactive = "active";
} elseif($tabstatus == "expenses") {
	$ExpensesActive = "active";
} elseif($tabstatus == "invoice") {
	$invoiceactive = "active";
} elseif($tabstatus == "main") {
	$mainactive = "active";
} elseif($tabstatus == "notes") {
	$notesactive = "active";
}elseif($tabstatus == "sentemails") {
	$sentemails = "active";
}elseif($tabstatus == "confirmations") {
	$confirmations = "active";
} else {
    $mainactive = "active";
}
	

//To collect the data of the above Tour id, and show it in the fields below
$sql = "select gro.*,gro.status AS Group_Status,pro.*
from groups gro
join products pro
on gro.airline_productid=pro.id
where gro.tourid=$tourid;";
$result 	= $db->query($sql);
$data 		= $result->fetch_assoc();
$count1 	= mysqli_num_rows($result);
//This was made when there is no airline product, will lead to be zero results
if($count1 == 0) {
	$sql 	= "SELECT * FROM groups WHERE tourid=$tourid";
	$result = $db->query($sql);
	$data 	= $result->fetch_assoc();
}

//To collect the prices for the Land Product
$landdproduct 	= $data["land_productid"];
$price1sql 		= "SELECT cost,price FROM products WHERE id=$landdproduct";
$price1result 	= $db->query($price1sql);
$price1 		= $price1result->fetch_assoc();

//To collect the prices for the Air Product
$airrproduct 	= $data["airline_productid"];
$price2sql 		= "SELECT cost,price FROM products WHERE id=$airrproduct";
$price2result 	= $db->query($price2sql);
$price2 		= $price2result->fetch_assoc();

$TourCostTotal 	= $price1['cost'] + $price2['cost'];
$TourPriceTotal = $price1['price'] + $price2['price'];
if (empty($TourCostTotal) && empty($TourPriceTotal))
{
	$TourCostTotal 	= $data['listprice'];
	$TourPriceTotal = $data['tripcost'];
}

// Hay el general header information fe kol el pages
if( ! AclPermission::actionAllowed('GroupSave') ) { 
    $PageTitle = "Group - ".$data["tourname"];
}
else {
    $PageTitle = "Edit Group - ".$data["tourname"];
}
include "header.php";

$paxtotal = (new TourMembership())->paxTotalWithTickets($tourid) ;
if( AclPermission::actionAllowed('GroupSave') ) { 
    if($data["seats"] < $paxtotal AND $data["seats"] > 0) {
        echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Warning: The total number of participants exceeds the reserved seats from the airline company.</strong></div>";
    } elseif($data["seats"] == $paxtotal AND $data["seats"] > 0) {
        echo "<div style='margin-top:25px;' class='alert alert-warning background-warning'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Warning: The total number of participants reached the maximum reserved seats from the airline company.</strong></div>";
    } elseif($data["seats"] == 0) {
        echo "<div style='margin-top:25px;' class='alert alert-primary background-primary'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Notice: The airline information is still not filled out.</strong></div>";
    } 
}

//To Delete a customer from the selected group
if ($_GET['action'] == "deleteCustomer") {
	$customerID = mysqli_real_escape_string($db, $_GET['customerID']);
	$SalesOrderNum = mysqli_real_escape_string($db, $_GET['SalesOrderNum']);
	$TourName = $data["tourname"];
	
	//To get the invoice number and remove it from the system
	$GetInvoiceID = "SELECT Customer_Invoice_Num FROM customer_invoice WHERE Group_ID='$tourid' AND Customer_Order_Num='$SalesOrderNum';" ;
	$CustomerInvoiceNum = GDb::fetchScalar($GetInvoiceID);
	//To get the full name of the customer
	$GetCustomerName = "SELECT CONCAT(co.fname,' ',co.mname,' ',co.lname) AS CustomerName FROM contacts co WHERE co.id='$customerID';" ;
	$CustomerFullName = GDb::fetchScalar($GetCustomerName);
	//To count how many groups this customer has assigned
	$GetCustomerCount = "SELECT COUNT(ca.contactid) FROM customer_account ca WHERE ca.contactid='$customerID';" ;
	$CustomerCount = GDb::fetchScalar($GetCustomerCount);
	
	//To get the UserType from Contacts table and remove the "4" indicator
	$GetUserType = "SELECT usertype FROM contacts WHERE id='$customerID';" ;
	$UserTypeID = GDb::fetchScalar($GetUserType);
	$UserTypeEach = explode(",", $UserTypeID);
	$UserTypeCount = count($UserTypeEach);
	$NewUserType = "";
	for($UserTypeX = 0; $UserTypeX<$UserTypeCount; $UserTypeX++){
		if($UserTypeEach[$UserTypeX] != 4) {
			$NewUserType .= $UserTypeEach[$UserTypeX].",";
		}
	}
	$UpdatedUserType = rtrim($NewUserType,',');
	
	if($UpdatedUserType == NULL OR $UpdatedUserType == "") {
		if($CustomerCount == 1) {
			$UpdatedUserType = 0;
		} else {
			$UpdatedUserType = 4;
		}
	}
	
	$DeleteCustomerSQL .= "DELETE FROM customer_invoice WHERE Customer_Invoice_Num='$CustomerInvoiceNum';";
	$DeleteCustomerSQL .= "DELETE FROM sales_order WHERE Sales_Order_Num='$SalesOrderNum';";
	$DeleteCustomerSQL .= "DELETE FROM customer_account WHERE contactid='$customerID' AND tourid='$tourid';";
	$DeleteCustomerSQL .= "UPDATE contacts SET usertype='$UpdatedUserType' WHERE id='$customerID';";
	$DeleteCustomerSQL .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','customer account','$tourid','deleted','$CustomerFullName <b>from</b> $TourName');";
	if ($db->multi_query($DeleteCustomerSQL) === TRUE) {
		echo "<meta http-equiv='refresh' content='0;groups-edit.php?id=$tourid&action=edit&tab=participants'>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting record: " . $db->error . "</strong></div>";
	}
}

//To Delete the attachment document and show notification
if ($_GET['action'] == "docdelete") {
	$fileid = $_GET['fileid'];
	
	$sql16 = "SELECT * FROM groups_media WHERE id=$fileid";
	$result16 = $db->query($sql16);
	$attachmentdata = $result16->fetch_assoc();

	$mediaurl = $attachmentdata["mediaurl"];
	$uploadpath = "uploads/";
	$fileurl = $uploadpath . $mediaurl;
	
	$sql7 = "DELETE FROM groups_media WHERE id=$fileid";
	if ($db->query($sql7) === TRUE) {
		unlink($fileurl);
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The attachment has been removed successfully!</div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting record: " . $con4->error . "</strong></div>";
	}
}


//This is to add a new product for the Airline Info Section
if(isset($_POST['submit'])){
	$p_status = $_POST["status"];
	$p_thesupplier = $_POST["thesupplier"];
	$p_thecategory = 1;
	$p_productname = $_POST["productname"];
	$p_description = $_POST["description"];
	$p_cost = $_POST["cost"];
	$p_price = $_POST["price"];

    
        //@author Nithin
        //Add entry to airline table if not extist in group_airline list {
        $airlineObj = new GroupsAirline() ;
        $new = $airlineObj->get(" Airline_Name='$p_productname' " ) ;
        if( ! (is_array($new) && count($new) > 0) ) {
            //create new airline entry
            $data = [
                'Airline_Name' => $p_productname 
            ] ;
            $airlineObj->insert($data) ;
        }
        //}

		//this is so i can get the Group_PO_ID for the current group
		$POnumSQL = "SELECT Group_PO_ID FROM group_purchase_order WHERE Group_Group_ID=$tourid";
		$result1 = $db->query($POnumSQL);
		$PurchaseOrderData = $result1->fetch_assoc();
		$PurchaseOrderNumID = $PurchaseOrderData["Group_PO_ID"];
        
        
		//this is so i can check if the purchase order line for this is already exist, so it will update not create new
		$POnumCheck2SQL = "SELECT * FROM group_po_line WHERE Group_PO_ID=$PurchaseOrderNumID AND Line_Type='Airline'";
		$resultCheck2 = $db->query($POnumCheck2SQL);

		
	$sql = "INSERT INTO products (name, description, supplierid, categoryid, subproduct, cost, price, paymentterm, status)
	VALUES ('$p_productname','$p_description','$p_thesupplier','$p_thecategory','0','$p_cost', '$p_price','0','$p_status')";
	if ($db->query($sql) === TRUE) {
		
		$newproductid = mysqli_insert_id($db);
		$productcon = new mysqli($servername, $username, $password, $dbname);
		if ($productcon->connect_error) {die("Connection failed: " . $productcon->connect_error);} 
		if ($resultCheck2->num_rows <= 0) {
		$productsql .= "INSERT INTO group_po_line (Group_PO_ID,Product_Product_ID,Supplier_Supplier_ID,Line_Type)
		VALUES ('$PurchaseOrderNumID','$newproductid','$p_thesupplier','Airline');";
		} else {
		$productsql .= "UPDATE group_po_line SET Product_Product_ID=$newproductid,Supplier_Supplier_ID=$p_thesupplier, Mod_Date=NOW() WHERE Group_PO_ID=$PurchaseOrderNumID AND Line_Type='Airline';";
		}
		if ($productcon->multi_query($productsql) === TRUE) {
			
			$productcon2 = new mysqli($servername, $username, $password, $dbname);
			if ($productcon2->connect_error) {die("Connection failed: " . $productcon2->connect_error);} 
			$productsql2 = "UPDATE groups SET airline_productid='$newproductid',tripcost='$TourCostTotal',listprice='$TourPriceTotal', updatetime=NOW() WHERE tourid=$tourid;";
			if ($productcon2->query($productsql2) === TRUE) {
			
				//Here there should be the success notification

			} else {
				echo $productsql2 . "<br>" . $productcon2->error;
			}
			$productcon2->close();
		
		} else {
			echo $productsql . "<br>" . $productcon->error;
		}
		$productcon->close();
		
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The new product has been created and assigned to the group successfully!</strong></div>";
		echo "<meta http-equiv='refresh' content='2'>";
	} else {
		echo $sql . "<br>" . $db->error."<br> error";
	}
}


//This is to add a new product for the Land Arrangment Info Section
if(isset($_POST['landsubmit'])){
	$p_status = $_POST["status"];
	$p_thesupplier = $_POST["thesupplier"];
	$p_thecategory = 2;
	$p_productname = $_POST["productname"];
	$p_description = $_POST["description"];
	$p_cost = $_POST["cost"];
	$p_price = $_POST["price"];

		//this is so i can get the Group_PO_ID for the current group
		$POnumSQL = "SELECT Group_PO_ID FROM group_purchase_order WHERE Group_Group_ID=$tourid";
		$result1 = $db->query($POnumSQL);
		$PurchaseOrderData = $result1->fetch_assoc();
		$PurchaseOrderNumID = $PurchaseOrderData["Group_PO_ID"];

		//this is so i can check if the purchase order line for this is already exist, so it will update not create new
		$POnumCheck2SQL = "SELECT * FROM group_po_line WHERE Group_PO_ID=$PurchaseOrderNumID AND Line_Type='Land'";
		$resultCheck2 = $db->query($POnumCheck2SQL);

	$sql = "INSERT INTO products (name, description, supplierid, categoryid, subproduct, cost, price, paymentterm, status)
	VALUES ('$p_productname','$p_description','$p_thesupplier','$p_thecategory','0','$p_cost', '$p_price','0','$p_status')";
	if ($db->query($sql) === TRUE) {
		$newlandproductid = mysqli_insert_id($db);
		$productlandcon = new mysqli($servername, $username, $password, $dbname);
		if ($productlandcon->connect_error) {die("Connection failed: " . $productlandcon->connect_error);} 
		$productlandsql = "UPDATE groups SET land_productid='$newlandproductid',tripcost='$TourCostTotal',listprice='$TourPriceTotal', updatetime=NOW() WHERE tourid=$tourid;";		
		if ($resultCheck2->num_rows <= 0) {
		$productlandsql .= "INSERT INTO group_po_line (Group_PO_ID,Product_Product_ID,Line_Type)
		VALUES ('$PurchaseOrderNumID','$newlandproductid','Land');";
		} else {
		$productlandsql .= "UPDATE group_po_line SET Product_Product_ID=$newlandproductid, Mod_Date=NOW() WHERE Group_PO_ID=$PurchaseOrderNumID AND Line_Type='Land';";
		}
		if ($productlandcon->multi_query($productlandsql) === TRUE) {
		//success text here if needed
		} else { 
			echo $productlandsql . "<br>" . $productlandcon->error; 
		}
		$productlandcon->close();
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The new land product has been created successfully! The page will be refreshed in 2 seconds.</strong></div>";
		echo "<meta http-equiv='refresh' content='2'>";
	}
}

$radonlyClass = '' ;
if( AclPermission::activeRoleId() != AclRole::$ADMIN ) {
    $radonlyClass = (AclPermission::activeRoleId() != AclRole::$ADMIN) ? 'readonly-block' : '';
    $aclEditableClass = 'acl-editable' ;

    global $GLOBAL_SCRIPT ;
    $GLOBAL_SCRIPT .= "$('.$radonlyClass input, .$radonlyClass textarea, .$radonlyClass select').attr('readonly', 'readonly');" ;
    $GLOBAL_SCRIPT .= "$('.$radonlyClass input[type=checkbox], .$radonlyClass input[type=radio]').attr('disabled', 'disabled');" ;
    //remove readonly and disabled from editables.
    $GLOBAL_SCRIPT .= "$('.$aclEditableClass input, .$aclEditableClass textarea, .$aclEditableClass select').removeAttr('readonly');" ;
    $GLOBAL_SCRIPT .= "$('.$aclEditableClass input[type=checkbox], .$aclEditableClass input[type=radio]').removeAttr('disabled');" ;
}

$statusCount = (new Groups())->getUserCountByStatus($tourid) ;
$ACTIVE_COUNT = $statusCount['active'];
$INACTIVE_COUNT = $statusCount['inactive'];
?>
<style type="text/css">

.spinner {
	/*position: fixed;
	z-index: 1000;
	top: 0;
	left: 0;*/
	height: 460px ;
	width: 200px;
	margin: 0 auto;
	background: rgba( 255, 255, 255, .8 ) url(files/assets/images/loader.gif) 50% 50% no-repeat;

}
#PaxFullList_filter { display:none;}

</style>
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->
<div class="row">
	<div class="col-md-9">
        <div class="card <?php echo $radonlyClass;?>">
			<div class="card-header">
				<div class="row">
					<div class="col-3">
						<div class="input-group input-group-sm mb-0">
							<span class="input-group-prepend mt-0">
								<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
							</span>
							<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
						</div>
					</div>
					<div class="col-3">
					<?php if( AclPermission::actionAllowed('GroupAddCustomer') ) { ?>
						<a href="#" class="btn btn-mat btn-block waves-effect waves-light btn-success pt-1 pb-1" data-toggle="modal" data-target="#addcustomer">Add Customer</a>
					<?php } ?>
					</div>
					<?php
						$TheCurrentUserName = $_SESSION['login_user'];
						$CheckGroupNotes = "SELECT COUNT(id) FROM groups_notes gn
											JOIN users us
											ON gn.username=us.Username
											WHERE gn.group_id='$tourid' AND us.Last_Login < gn.date_time AND us.Username!='$TheCurrentUserName';" ;
						$GroupNotesCheck = GDb::fetchScalar($CheckGroupNotes);
                        
                        
					?>
					<div class="col-3 text-right tada animated">
                        <?php if( AclPermission::actionAllowed('GroupNotes') ) { ?>
						<a href="groups-edit.php?id=<?php echo $tourid; ?>&action=edit&tab=notes" class="btn btn-mat btn-block waves-effect waves-light btn-inverse pt-1 pb-1">Group Notes <?php if($GroupNotesCheck > 0) { ?><label class="badge badge-danger mb-0"><?php echo $GroupNotesCheck; ?></label><?php } ?></a>
                        <?php } ?>
					</div>
					<div class="col-3 text-right tada animated">
                        <?php
                        if( AclPermission::actionAllowed('GroupEmail') ) { ?>
						<a id="send-group-email" data-inactive="<?php echo $INACTIVE_COUNT;?>" data-active="<?php echo $ACTIVE_COUNT;?>" href="javascript:void(0)" data-toggle="modal" 
                           class="btn btn-mat btn-block waves-effect waves-light btn-info pt-1 pb-1">Group Email</a>
                        <?php } ?>
					</div>
				</div>
			</div>
		<!-- Nav tabs -->
		<ul class="nav nav-tabs /*md-tabs*/" role="tablist">
			<li class="nav-item">
				<a class="nav-link <?php echo $mainactive; ?>" data-toggle="tab" href="#basic" role="tab" onclick="history.pushState('data to be passed', 'Groups Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>groups-edit.php?id=<?php echo $tourid; ?>&action=edit&tab=main');">Basic Info</a>
				<div class="slide"></div>
			</li>
			<li class="nav-item">
				<a class="nav-link <?php echo $airactive; ?>" data-toggle="tab" href="#airline" role="tab" onclick="history.pushState('data to be passed', 'Groups Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>groups-edit.php?id=<?php echo $tourid; ?>&action=edit&tab=airline');">Airline Info</a>
				<div class="slide"></div>
			</li>
			<li class="nav-item">
                <a class="nav-link <?php echo $landactive; ?>" data-toggle="tab" href="#landarrangements" role="tab" onclick="history.pushState('data to be passed', 'Groups Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>groups-edit.php?id=<?php echo $tourid; ?>&action=edit&tab=land');">Land Info</a>
				<div class="slide"></div>
			</li>
            <?php if( AclPermission::actionAllowed('TabGroupInvoice') ) { ?>
			<li class="nav-item">
				<a class="nav-link <?php echo $invoiceactive; ?>" data-toggle="tab" href="#groupinvoice" role="tab" onclick="history.pushState('data to be passed', 'Groups Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>groups-edit.php?id=<?php echo $tourid; ?>&action=edit&tab=invoice');">Group Invoice</a>
				
				<div class="slide"></div>
			</li>
            <?php } ?>
                        <?php if( AclPermission::actionAllowed('TabExpenses') ) { ?>

			<li class="nav-item">
				<a class="nav-link <?php echo $ExpensesActive; ?>" data-toggle="tab" href="#expenses" role="tab" onclick="history.pushState('data to be passed', 'Groups Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>groups-edit.php?id=<?php echo $tourid; ?>&action=edit&tab=expenses');">Expenses</a>
				<div class="slide"></div>
			</li>
                        <?php } ?>
			<li class="nav-item" style="display:none;">
				<a class="nav-link" data-toggle="tab" href="#packages" role="tab">Packages</a>
				<div class="slide"></div>
			</li>
            <?php if( AclPermission::actionAllowed('TabPax') ) { ?>
			<li class="nav-item">
				<a class="nav-link <?php echo $paxactive; ?>" data-toggle="tab" href="#participants" role="tab" onclick="history.pushState('data to be passed', 'Groups Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>groups-edit.php?id=<?php echo $tourid; ?>&action=edit&tab=participants');">Pax</a>
				<div class="slide"></div>
			</li>
            <?php } ?>
			<li class="nav-item">
				<a class="nav-link <?php echo $confirmations; ?>" data-toggle="tab" href="#confirmations" role="tab" onclick="history.pushState('data to be passed', 'Groups Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>groups-edit.php?id=<?php echo $tourid; ?>&action=edit&tab=confirmations');">Confirmations</a>
				
				<div class="slide"></div>
			</li>
			<li class="nav-item">
				<a class="nav-link <?php echo $reportsactive; ?>" data-toggle="tab" href="#reports" role="tab" onclick="history.pushState('data to be passed', 'Groups Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>groups-edit.php?id=<?php echo $tourid; ?>&action=edit&tab=reports');">Reports</a>
				<div class="slide"></div>
			</li>
            <?php if( AclPermission::actionAllowed('TabSentEmails') ) { ?>
			<li class="nav-item">
				<a class="nav-link <?php echo $sentemails; ?>" data-toggle="tab" href="#sentemails" role="tab" onclick="history.pushState('data to be passed', 'Groups Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>groups-edit.php?id=<?php echo $tourid; ?>&action=edit&tab=sentemails');">Sent Emails</a>
				<div class="slide"></div>
			</li>
            <?php } ?>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content card-block">
			<div class="tab-pane <?php echo $mainactive; ?>" id="basic" role="tabpanel">
				<?php include 'inc/group-tabs-basicinfo.php'; ?>
			</div>
			<div class="tab-pane <?php echo $airactive; ?>" id="airline" role="tabpanel">
				<?php include 'inc/group-tabs-airline.php'; ?>
			</div>
			<div class="tab-pane <?php echo $landactive; ?>" id="landarrangements" role="tabpanel">
				<?php include 'inc/group-tabs-land.php'; ?>
			</div>
            <?php if( AclPermission::actionAllowed('TabGroupInvoice') ) { ?>
			<div class="tab-pane <?php echo $invoiceactive; ?>" id="groupinvoice" role="tabpanel">
				<?php include 'inc/group-tabs-groupinvoice.php'; ?>
			</div>
            <?php } ?>
                    <?php if( AclPermission::actionAllowed('TabExpenses') ) { ?>

			<div class="tab-pane <?php echo $ExpensesActive; ?>" id="expenses" role="tabpanel">
				<?php include 'inc/group-tabs-expenses.php'; ?>
			</div>
                    <?php } ?>
			<div class="tab-pane" id="packages" role="tabpanel" style="display:none;">
				<?php //include 'inc/group-tabs-packages.php'; ?>
			</div>
			<div class="tab-pane <?php echo $paxactive; ?>" id="participants" role="tabpanel">
				<?php include 'inc/group-tabs-participants.php'; ?>
			</div>
			<div class="tab-pane <?php echo $confirmations; ?>" id="confirmations" role="tabpanel">
				<?php include 'inc/group-tabs-confirmations.php'; ?>
			</div>
			<div class="tab-pane <?php echo $attachmentactive; ?>" id="attachments" role="tabpanel">
				<?php include 'inc/group-tabs-attachments.php'; ?>
			</div>
			<div class="tab-pane <?php echo $reportsactive; ?>" id="reports" role="tabpanel">
				<?php include 'inc/group-tabs-reports.php'; ?>
			</div>
			<div class="tab-pane <?php echo $notesactive; ?>" id="notes" role="tabpanel">
				<?php include 'inc/group-tabs-notes.php'; ?>
			</div>
			<div class="tab-pane <?php echo $sentemails; ?>" id="sentemails" role="tabpanel">
				<?php include 'inc/groups-email-sent-list.php'; ?>
			</div>
		</div>	
        </div>
    </div>
	<div class="col-md-3" id="sidebar2">
        <div class="card row">
            <div class="card-header">
                <h5>Group's Overview</h5>
            </div>
			<div class="form-group col-sm-12">
			<?php 
			date_default_timezone_set('America/Chicago');
			$today = time();
			$GroupDepartureDate = strtotime($data["enddate"]);
			$current=strtotime('now');
			$diffference =$GroupDepartureDate-$current;
			$days=floor($diffference / (60*60*24));
			if($days > 90) { 
				echo "<h3>$days <span style='font-size:60%;font-weight:300;'>days remaining.</h3>";
			} elseif($days < 90 AND $days > 0) {
				echo "<h3><small><i class='fas fa-exclamation-triangle'></i> Only</small> $days <small>days left!</small></h3>";
			} else {
				echo "<h3 style='color:red;'><small>This group is expired.</small></h3>";
			}
			?>
			</div>
			<form name="contact7" class="col-sm-12" action="inc/group-functions.php" method="POST" id="contact7">
                <div class="form-group col-sm-12 p-0">
						<input name="requiredinfo" value="<?php echo $data["tourid"]; ?>" type="text" hidden>
						<input name="requiredinfoname" value="<?php echo $data["tourname"]; ?>" type="text" hidden>
                    <h4 class="sub-title"># of Pax</h4>
					<h3>
					<?php
                    $toutIdNow = $data["tourid"] ;
					$WithTicketTotal = (new TourMembership())->paxTotalWithTickets($toutIdNow) ;
					
					$WithoutTicketSql = "SELECT * FROM customer_account WHERE tourid=$toutIdNow AND status=1 AND hisownticket=1";
					$WithoutTicketResult = $db->query($WithoutTicketSql);
					$WithoutTicketTotal = $WithoutTicketResult->num_rows; 
					
					echo $WithTicketTotal + $WithoutTicketTotal;
					?>
					<span style="font-size:60%;font-weight:300;"> out of <?php if($data["seats"]==""){ echo "0";} else { echo $data["seats"];} ?></span>
					<span style="font-size:70%;">(<?php echo $WithTicketTotal; ?> <img src="<?php echo $crm_images; ?>/pax-ticket-yes.png" alt="# of pax who needs tickets from us" title="# of pax who needs tickets from us" style="width:15px;height:auto;" /> / <?php echo $WithoutTicketTotal; ?> <img src="<?php echo $crm_images; ?>/pax-ticket-no.png" alt="# of pax who booked their own ticket" title="# of pax who booked their own ticket" style="width:15px;height:auto;" />)</span>
					</h3>
                </div>
                <div class="col-sm-12 p-0">
					<h4 class="sub-title">Agents 
                        <?php if( AclPermission::actionAllowed('BoxEdit') ) { ?>
					<small id="EditAgentBlock" style="cursor:pointer;margin-left:10px;"><i class="far fa-edit"></i> Edit</span></small>
                        <?php } ?>
					<small id="CancelAgentBlock" style="cursor:pointer;margin-left:10px;display:none;"><i class="far fa-edit"></i> Cancel</small></h4>
					<div id="EditAgentBlock1DIV" style="display:block;">
					<p>
					<?php if($data["agent"] != NULL) {
						$AgentsList = $data["agent"]; 
						$Agentsz = explode(",", $AgentsList);
						for($z = 0; $z < count($Agentsz); $z++) {
							$AgentsConn = new mysqli($servername, $username, $password, $dbname);
							$AgentsSQL = "SELECT id,fname,lname FROM contacts WHERE id=$Agentsz[$z]";
							$AgentsResult = $AgentsConn->query($AgentsSQL);
							$AgentsData = $AgentsResult->fetch_assoc();
							$AgentsConn->close();

                            if( AclPermission::userGroup() == AclRole::$ADMIN ) {
                                echo "<a style='margin-right:15px;' href='contacts-edit.php?id=".$AgentsData['id']."&action=edit&usertype=agent'><i class='fas fa-user'></i> ".$AgentsData['fname']." ".$AgentsData['lname']."</a>";
                            }
                            else {
                                echo '<a style="margin-right:15px;cursor:default;" href="javascript:;">' .$AgentsData['fname']." ".$AgentsData['lname'] . '</a>' ;
                            }
						}
					}
					?>
					</p>
					</div>
                </div>
                <div class="col-sm-12 p-0">
					<div id="EditAgentBlock2DIV" style="display:none;">
                    <select id="agentz" name="theagent[]" class="js-example2-basic2-multiple-limit col-sm-12" multiple="multiple">
						<?php $con5 = new mysqli($servername, $username, $password, $dbname);
						$result5 = mysqli_query($con5,"SELECT * FROM agents WHERE status=1");
						while($row5 = mysqli_fetch_array($result5))
						{
							//here, we are checking the Agent ID with the contact column list to show their names
							$con6 = new mysqli($servername, $username, $password, $dbname);
							$result6 = mysqli_query($con6,"SELECT id, fname, mname, lname FROM contacts WHERE `usertype` LIKE '%1%'");
							while($row6 = mysqli_fetch_array($result6))
							{
								$selected = "";
								//This is to take part of the sql cell, and see if this agent id is matching to make selected
								$agentidz = $row5['contactid'];
								if (strpos($data["agent"], $agentidz) !== false) {$selected = "selected";}
								//if($data['agent'] == $row5['id']) {$selected = "selected";}
								if($row5['contactid'] == $row6['id']) {
									echo "<option value='".$row5['contactid']."' ".$selected.">".$row6['fname']." ".$row6['lname']."</option>";
								}
							}
						} ?>
                    </select>
					<br /><br />
					</div>
                </div>
                <div class="col-sm-12 p-0">
					<h4 class="sub-title">Group Leaders
                        <?php if( AclPermission::actionAllowed('BoxEdit') ) { ?>
					<small id="EditLeaderzBlock" style="cursor:pointer;margin-left:10px;"><i class="far fa-edit"></i> Edit</span></small>
                        <?php } ?>
					<small id="CancelLeaderzBlock" style="cursor:pointer;margin-left:10px;display:none;"><i class="far fa-edit"></i> Cancel</small></h4>
					<div id="EditLeaderzBlock1DIV" style="display:block;">
					<p>
					<?php if($data["groupleader"] != NULL) {
						$LeadersList = $data["groupleader"]; 
						$leadersz = explode(",", $LeadersList);
						for($x = 0; $x < count($leadersz); $x++) {
							$LeadersConn = new mysqli($servername, $username, $password, $dbname);
							$LeadersSQL = "SELECT id,fname,lname FROM contacts WHERE id=$leadersz[$x]";
							$LeadersResult = $LeadersConn->query($LeadersSQL);
							$LeadersData = $LeadersResult->fetch_assoc();
							$LeadersConn->close();

                            if( AclPermission::userGroup() == AclRole::$ADMIN ) {
                                echo "<a style='margin-right:15px;' href='contacts-edit.php?id=".$LeadersData['id']."&action=edit&usertype=leader'><i class='fas fa-user'></i> ".$LeadersData['fname']." ".$LeadersData['lname']."</a>";
                            }
                            else {
                                echo '<a style="margin-right:15px;cursor:default;" href="javascript:;">' .$LeadersData['fname']." ".$LeadersData['lname'] . '</a>';
                            }
						}
					}
					?>
					</p>
					</div>
                </div>
                <div class="col-sm-12 p-0">
					<div id="EditLeaderzBlock2DIV" style="display:none;">
					<select data-selection-size="5" id="groupleaderz" name="thegroupleader[]" class="select2-group-leaders col-sm-12" multiple="multiple">
						<?php //here, we are checking the Agent ID with the contact column list to show their names
                        $ids = $data["groupleader"] ;
						$con7 = new mysqli($servername, $username, $password, $dbname);
						$result7 = mysqli_query($con7,"SELECT id, fname, mname, lname FROM contacts WHERE id IN($ids) AND `usertype` LIKE '%3%'");
						while($row7 = mysqli_fetch_array($result7))
						{
							$selected = "";
							//This is to take part of the sql cell, and see if this group leader id is matching to make selected
							$groupleaderid = $row7['id'];
							if (strpos($data["groupleader"], $groupleaderid) !== false) {$selected = "selected";}
							//if($data['groupleader'] == $row7['id']) {$selected = "selected";}
							echo "<option value='".$row7['id']."' ".$selected.">".$row7['fname']." ".$row7['lname']."</option>";
						} ?>
					</select>
					<br />
					<br />
					</div>
                </div>
                <div class="col-sm-12 p-0">
					<h4 class="sub-title">Staff Representative 
                        <?php if( AclPermission::actionAllowed('BoxEdit') ) { ?>
					<small id="EditStaffBlock" style="cursor:pointer;margin-left:10px;"><i class="far fa-edit"></i> Edit</small>
                        <?php } ?>
					<small id="CancelStaffBlock" style="cursor:pointer;margin-left:10px;display:none;"><i class="far fa-edit"></i> Cancel</small></h4>
                    <div id="EditStaffBlock1DIV" style="display:block;">
					<p>
					<?php if($data["Staff_ID"] != NULL) {
						$StaffList = $data["Staff_ID"]; 
						$StaffConn = new mysqli($servername, $username, $password, $dbname);
						$StaffSQL = "SELECT UserID,fname,lname FROM users WHERE UserID=$StaffList";
						$StaffResult = $StaffConn->query($StaffSQL);
						$StaffData = $StaffResult->fetch_assoc();

                        if( AclPermission::userGroup() == AclRole::$ADMIN ) {
                            echo "<a style='margin-right:15px;' href='users-edit.php?id=".$StaffData['UserID']."'><i class='fas fa-user'></i> ".$StaffData['fname']." ".$StaffData['lname']."</a>";
                        }
                        else {
                            echo '<a style="margin-right:15px;cursor:default;" href="javascript:;">' .$StaffData['fname']." ".$StaffData['lname'] . '</a>' ;
                        }
					}
					?>
					</p>
					</div>
                </div>
                <div class="col-sm-12 p-0">
					<div id="EditStaffBlock2DIV" style="display:none;">
                    <select id="Staffz" name="StaffRepID" class="form-control form-control-default col-sm-12">
						<?php 
						$StaffSelectResult = mysqli_query($StaffConn,"SELECT * FROM users WHERE active=1 AND Role_ID=1");
						while($StaffListData = mysqli_fetch_array($StaffSelectResult))
						{
								$selected = "";
								if($StaffListData['fname'] != "" OR $StaffListData['lname'] != "") {
									$FullName = $StaffListData['fname']." ".$StaffListData['lname'];
								} else {
									$FullName = $StaffListData['Username'];
								}
								if($StaffData['UserID'] == $StaffListData['UserID']) { $selected = "selected";} else { $selected = ""; }
								echo "<option value='".$StaffListData['UserID']."' ".$selected.">".$FullName."</option>";
						}
                        if( $StaffConn ) {
                            $StaffConn->close();
                        }
						?>
                    </select>
					<br /><br />
					</div>
                </div>
                <?php if( AclPermission::actionAllowed('SupplierInfo') ) { ?>
                <div class="col-sm-12 p-0">
					<h4 class="sub-title">Suppliers</h4>
						<?php $TourSupp1Conn = new mysqli($servername, $username, $password, $dbname);
						$TourSupp1SQL = "SELECT gro.tourid,gro.tourname,pro.id,pro.name,gro.status,con.company,con.fname,con.lname,con.id AS SupplierContact
							FROM groups gro 
							JOIN products pro 
							ON gro.airline_productid=pro.id
							JOIN contacts con 
							ON pro.supplierid=con.id
							WHERE gro.tourid=$tourid";
						$TourSupp1Result = $TourSupp1Conn->query($TourSupp1SQL);
						$TourSupp1 = $TourSupp1Result->fetch_assoc();
						$TourSupp1Conn->close();
						
						$TourSupp2Conn = new mysqli($servername, $username, $password, $dbname);
						$TourSupp2SQL = "SELECT t.tourid,t.tourname,t.land_productid,p.name,t.status,c.company,c.fname,c.lname,c.id AS SupplierContact
							FROM groups t JOIN products p ON t.land_productid=p.id
							JOIN contacts c 
							ON p.supplierid=c.id
							WHERE t.tourid=$tourid";
						$TourSupp2Result = $TourSupp2Conn->query($TourSupp2SQL);
						$TourSupp2 = $TourSupp2Result->fetch_assoc();
						$TourSupp2Conn->close();?>
					<p style="font-weight: bold;font-size: 13px;margin-bottom: 5px;">
					<?php if($TourSupp1['SupplierContact'] == NULL) {echo "There isn't an Airline product yet.";} else { ?>
						Airline: <a href="contacts-edit.php?id=<?php echo $TourSupp1['SupplierContact']; ?>&action=edit&usertype=supplier"><?php echo $TourSupp1['company']; ?></a> <small style="display:none;">(<?php echo $TourSupp1['name']; ?>)</small>
					<?php } ?>
					</p>
					<p style="font-weight: bold;font-size: 13px;margin-bottom: 0px;">
					<?php if($TourSupp2['SupplierContact'] == NULL) {echo "There isn't a Land product yet.";} else { ?>
						Land: <a href="contacts-edit.php?id=<?php echo $TourSupp2['SupplierContact']; ?>&action=edit&usertype=supplier"><?php echo $TourSupp2['company']; ?></a> <small style="display:none;">(<?php echo $TourSupp2['name']; ?>)</small>
					<?php } ?>
					</p>
                </div>
                <?php } ?>
				<div class="col-sm-12 p-0 row">
                                    <?php if( AclPermission::actionAllowed('GroupCost') ) { ?>
                <div class="form-group col-sm-6"><br />
                    <h4 class="sub-title">Cost $</h4>
					<div class="dollar" style="color:#ffffff !important;"><input type="text" id="total_expensesOLD" name="tripcost" class="form-control form-bg-inverse" value="<?php echo /*$data["tripcost"];*/ $TourCostTotal; ?>" style="cursor: not-allowed;" readonly></div>
                </div>
                                    <?php }?>
                <div class="form-group col-sm-6"><br />
                    <h4 class="sub-title">Price $</h4>
					<input type="text" id="listpriceCurrent" name="listpriceCurrent" class="form-control form-bg-success" value="<?php echo $data["listprice"]; ?>" hidden>
					<div class="dollar" style="color:#ffffff !important;"><input type="text" id="listprice" name="listprice" class="form-control form-bg-success" value="<?php echo /*$data["listprice"]*/ $TourPriceTotal; ?>" style="cursor: not-allowed;" readonly></div>
                </div>
                <div id="submitmaindata" class="form-group col-sm-12" style="display:none;">
				<button type="submit" name="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Update</button>
				</div>
				</div>
			</form>
		</div>
        <div class="card row">
            <div class="card-header" style="padding-bottom:10px;">
                <h5>Group's Notes <small>(Latest 5 notes)</small></h5>
            </div>
            <div class="card-block" style="padding-top:0px;">
			<?php
			$GroupNotesSQL = "select SUBSTRING(note, 1, 150) AS note, username,date_time from groups_notes WHERE group_id='$tourid' ORDER BY id DESC LIMIT 5"; $GroupNotesResult = $db->query($GroupNotesSQL);
			if ($GroupNotesResult->num_rows > 0) {
			while($GroupNotesData = $GroupNotesResult->fetch_assoc()) { ?>
				<i class="fas fa-user"></i><b> <?php echo $GroupNotesData["username"]; ?>:</b> <?php echo $GroupNotesData["note"]; ?>...
				<div style="clear:both;height:6px;width:30px;"></div>
				<small><b><?php echo $GroupNotesData["date_time"]; ?></b></small>
				<hr>
			<?php }} else { ?>
				No group notes yet!
			<br />
			<?php } ?>
			<a class="btn btn-success btn-sm mt-2" href="groups-edit.php?id=<?php echo $tourid; ?>&action=edit&tab=notes" onclick="history.pushState('data to be passed', 'Groups Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>groups-edit.php?id=<?php echo $tourid; ?>&action=edit&tab=notes');">Manage</a>
			</div>
		</div>
        <div class="card row">
            <div class="card-header" style="padding-bottom:10px;">
                <h5>Attachments</h5>
            </div>
            <div class="card-block" style="padding-top:0px;">
			<?php
			$sql = "select * from groups_media WHERE tourid='$tourid'"; $result = $db->query($sql);
			if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) { ?>
				<img src="<?php echo $crm_images; ?>/attachment.png" alt="Attachments" style="width:20px; height:20px;" /> <a href="groups-edit.php?id=<?php echo $row["tourid"]; ?>&tab=attachments&fileid=<?php echo $row["id"]; ?>&action=docedit" style="color: #0000EE;"><?php echo $row["name"]; ?></a>
				<br />
			<?php }} else { ?>
				No attachments yet!
			<br />
			<?php } ?>
			<a class="btn btn-success btn-sm mt-2" href="groups-edit.php?id=<?php echo $tourid; ?>&action=edit&tab=attachments" onclick="history.pushState('data to be passed', 'Groups Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>groups-edit.php?id=<?php echo $tourid; ?>&action=edit&tab=attachments');">Manage</a>
			</div>
		</div>
		<div class="card row">
			<div class="card-header" style="padding-bottom:10px;">
				<a data-toggle="collapse" href="#collapse1"><h5>Group Change log</h5></a>
			</div>
			<div class="card-block" style="padding-top:0px; padding-bottom: 5px;">
				<ul id="collapse1" class="panel-collapse collapse">
				<?php
				$UsersLog = "select * from logs WHERE contactid='$tourid' AND usertype='tour' ORDER BY time DESC";
				$UsersLogResult = $db->query($UsersLog);
				if ($UsersLogResult->num_rows > 0) { ?>
				<?php while($UsersLogRow = $UsersLogResult->fetch_assoc()) { ?>
					<li style="border-bottom:solid 1px #ccc;font-size:12px;padding-bottom:5px;padding-top:5px;">
						<a href="javascript:void(0)" data-id="<?php echo $tourid; ?>" data-type="Group" data-date="<?php echo $UsersLogRow["time"]; ?>" class="changelogline" id="name_badges_tab" data-toggle="modal" data-target="#ChangeLogTrack">
							<strong style="font-weight:bold;"><?php echo $UsersLogRow['username']; ?></strong> updated this group
							<br /><small>on <?php echo date('m/d/Y - h:i A',strtotime($UsersLogRow["time"])); ?></small>
						</a>
					</li>
				<?php }} ?>
				</ul>
			</div>
		</div>
	</div>
</div>

<?php include 'inc/notificiations.php'; ?>

<?php $calculation =
    "var hotel1cost = Number($('#item_price').val());
    var hotel2cost = Number($('#item2_price').val());
    var hotel3cost = Number($('#item3_price').val());
    document.getElementById('total_expenses').value = hotel1cost + hotel2cost + hotel3cost"; ?>
	
<script type="text/javascript">
<?php if(($paxtotal + $WithoutTicketTotal) > 0) { ?>
//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#PaxFullList').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
	$('.nav-tabs a[href="#participants"]').tab('show');
});
<?php } ?>


$(document).ready(function() {
    $('.changelogline').click(function() {
    	$('#ChangeLogTrackDiv').html("<div class='spinner'></div>");
		var type 		= $(this).attr("data-type");
		var id 			= $(this).attr("data-id");
		var ChangeDate 	= $(this).attr("data-date");
		$.ajax({
			type 	: "POST",
			data 	: {"type":type, "id":id, "ChangeDate":ChangeDate},
			url 	: 'inc/group-tabs-changelog.php',
			success: function (data) {
				$('#ChangeLogTrackDiv').html(data);
			}
		});
    })
});

 $(document).ready(function() {
	$("form").submit(function() {
		// Getting the form ID
		var  formID = $(this).attr('id');
		var formDetails = $('#'+formID);
		//To not let the form of uploading attachments included in this
		if (formID != 'contact6' && formID != 'idGroupEmailForm') {
			$.ajax({
				type: "POST",
				url: 'inc/group-functions.php',
				data: formDetails.serialize(),
				success: function (data) {	
					// Inserting html into the result div
					$('#results').html(data);
					//$("form")[0].reset();
					$('#submitmaindata').hide();
				},
				error: function(jqXHR, text, error){
				// Displaying if there are any errors
					$('#result').html(error);           
				}
			});
			return false;
		}
	});
});

//This to show the Add New button when the dropdown menu is selected - AIRLINE INFO
$(function() {
  $("#airlineproduct").change(function() {
    if ($("#addnewoption").is(":selected")) {
      $("#addnewbutton").show();
    } else {
      $("#addnewbutton").hide();
    }
  }).trigger('change');
});

//This to show the Add New button when the dropdown menu is selected - LAND INFO
$(function() {
  $("#landproduct").change(function() {
    if ($("#addnewoptionland").is(":selected")) {
      $("#addnewbuttonland").show();
    } else {
      $("#addnewbuttonland").hide();
    }
  }).trigger('change');
});


//This is for the right sidebox so it can scroll with the screen
/*$(function() {
    var offset = $("#sidebar").offset();
    var topPadding = 70;
    $(window).scroll(function() {
        if ($(window).scrollTop() > offset.top) {
            $("#sidebar").stop().animate({
            marginTop: $(window).scrollTop() - offset.top + topPadding
            });
        } else {
            $("#sidebar").stop().animate({
                marginTop: 0
            });
        };
    });
});*/

//For updating the required info of the Tour
$(document).ready(function() {
  $(document).on('change', '#listprice', function() {
    $('#submitmaindata').show("slow");
  });
  $(document).on('change', '#total_expenses', function() {
    $('#submitmaindata').show("slow");
  });
  $(document).on('change', '#groupleaderz', function() {
    $('#submitmaindata').show("slow");
  });
  $(document).on('change', '#agentz', function() {
    $('#submitmaindata').show("slow");
  });
  $(document).on('change', '#Staffz', function() {
    $('#submitmaindata').show("slow");
  });
  //For notifying about the airline product change!!!
  $(document).on('change', '#airlineproduct', function() {
    $('#airfarewarning').show("slow");
  });
  $(document).on('change', '#landproduct', function() {
    $('#landwarning').show("slow");
  });
});

//get Product Details on product Type
function getSubProductDetails() {
	if ($("#subproductname").val() == 'add_new') {
		$('#subproductname').val('');
		$('#subproductname_textbox').attr('required','');
		$('.check_which_item_selected').val(0);
		$('#subproduct_dropbox').hide();
		$('#subproduct_textbox').show();
	} else {
		$('#subproductname_textbox').val('');
		$('#subproductname_textbox').removeAttr('required');
		$('.check_which_item_selected').val(1);
		$('#subproduct_dropbox').show();
		$('#subproduct_textbox').hide();
		$.ajax({
			type: "POST",
			url: 'inc/group-functions.php',
			data: { 'productDetailInfo': 'Yes', subProductName: $("#subproductname").val(), productType: $("#producttype").val() },
			dataType: "json",
			success: function (data) {
				$('#description').val(data[0].subProductDescription);
				if ($("#producttype").val() == "Hotels") {
					$('#hotelphonevalue').val(data[0].subPhone);
					$('#hotelcityvalue').val(data[0].subCity);
				}
				if ($("#producttype").val() == "Guides") {
					$('#GuidePhonevalue').val(data[0].subPhone);
					$('#GuideCityvalue').val(data[0].subCity);
				}
			},
			error: function(jqXHR, text, error){          
			}
		});
	}
	
}

//This is so it can show or hide fields when the product type is selected, like hotels, restaurants, ...
$(function() {
  $("#producttype").change(function() {
  	$('#subproductname').html('');
  	$('#subproductname_textbox').html('');
  	$('#subproduct_dropbox').show();
	$('#subproduct_textbox').hide();
  	$.ajax({
		type: "POST",
		url: 'inc/group-functions.php',
		data: { 'productLandInfo': 'Yes', productType: $("#producttype").val() },
		dataType: "json",
		success: function (data) {
			var subProductData = "<option value='add_new'>Add New</option>";
			$.each(data, function(i, item) {
			    subProductData += "<option value='"+item.subProductName+"'>"+item.subProductName+"</option>";
			});
			$('#subproductname').html(subProductData);
		},
		error: function(jqXHR, text, error){
		// Displaying if there are any errors
			$('#subproductname').html(error);           
		}
	});

    if ($("#Hotels").is(":selected")) {
      $("#HotelzName").show("slow");
      $("#GuidezName").hide("slow");
      $("#SubProductName").hide("slow");
      $("#hotelcity").show("slow");
      $("#hotelphone").show("slow");
      $("#checkindate").show("slow");
      $("#checkoutdate").show("slow");
      $("#GuideCity").hide("slow");
      $("#GuidePhone").hide("slow");
    } else if ($("#Guides").is(":selected")) {
      $("#HotelzName").hide("slow");
      $("#GuidezName").show("slow");
      $("#SubProductName").hide("slow");
      $("#GuidePhone").show("slow");
      $("#GuideCity").show("slow");
      $("#hotelcity").hide("slow");
      $("#hotelphone").hide("slow");
      $("#checkindate").hide("slow");
      $("#checkoutdate").hide("slow");
    } else {
      $("#SubProductName").show("slow");
      $("#HotelzName").hide("slow");
      $("#GuidezName").hide("slow");
      $("#hotelcity").hide("slow");
      $("#hotelphone").hide("slow");
      $("#checkindate").hide("slow");
      $("#checkoutdate").hide("slow");
      $("#GuideCity").hide("slow");
      $("#GuidePhone").hide("slow");
    }
  }).trigger('change');
});

//Show or hide the options of adding a new customer account for the group 
$(function () {
        $("#CurrentCustomerTourID").click(function () {
            if ($(this).is(":checked")) {
                $("#CurrentCustomerList").show("slow");
                $("#CurrentCustomerSubmit").show("slow");
                $("#NewAddCustomer").hide("slow");
            } else {
                $("#CurrentCustomerList").hide("slow");
                $("#CurrentCustomerSubmit").hide("slow");
                $("#NewAddCustomer").show("slow");
            }
        });
    });
//Show or hide The agent edit blocks 
$( "#EditAgentBlock" ).click(function() {
  $( "#EditAgentBlock2DIV" ).show("slow");
  $( "#EditAgentBlock1DIV" ).hide("slow");
  $( "#EditAgentBlock" ).hide("slow");
  $( "#CancelAgentBlock" ).show("slow");
});
$( "#CancelAgentBlock" ).click(function() {
  $( "#EditAgentBlock2DIV" ).hide("slow");
  $( "#EditAgentBlock1DIV" ).show("slow");
  $( "#EditAgentBlock" ).show("slow");
  $( "#CancelAgentBlock" ).hide("slow");
});
$( "#EditStaffBlock" ).click(function() {
  $( "#EditStaffBlock2DIV" ).show("slow");
  $( "#EditStaffBlock1DIV" ).hide("slow");
  $( "#EditStaffBlock" ).hide("slow");
  $( "#CancelStaffBlock" ).show("slow");
});
$( "#CancelStaffBlock" ).click(function() {
  $( "#EditStaffBlock2DIV" ).hide("slow");
  $( "#EditStaffBlock1DIV" ).show("slow");
  $( "#EditStaffBlock" ).show("slow");
  $( "#CancelStaffBlock" ).hide("slow");
});
$( "#EditLeaderzBlock" ).click(function() {
  $( "#EditLeaderzBlock2DIV" ).show("slow");
  $( "#EditLeaderzBlock1DIV" ).hide("slow");
  $( "#EditLeaderzBlock" ).hide("slow");
  $( "#CancelLeaderzBlock" ).show("slow");
});
$( "#CancelLeaderzBlock" ).click(function() {
  $( "#EditLeaderzBlock2DIV" ).hide("slow");
  $( "#EditLeaderzBlock1DIV" ).show("slow");
  $( "#EditLeaderzBlock" ).show("slow");
  $( "#CancelLeaderzBlock" ).hide("slow");
});


//This is to update an input field when you type in other input field, used for the total cost of the group
/*$('input').keyup(function(){ // run anytime the value changes
	<?php echo $calculation; ?>
});

function myFunction(e) {
    document.getElementById("item_price").value = e.target.value;
    //here to get the text name of the select option, not the value
	var x = document.getElementById("mySelect");
    var i = x.selectedIndex;
    document.getElementById("myText2").value = x.options[i].text;
	<?php echo $calculation; ?>
}
function myFunction2(e) {
    document.getElementById("item2_price").value = e.target.value;
    //here to get the text name of the select option, not the value
	var x = document.getElementById("mySelect2");
    var i = x.selectedIndex;
    document.getElementById("my2Text2").value = x.options[i].text;
	<?php echo $calculation; ?>
}
function myFunction3(e) {
    document.getElementById("item3_price").value = e.target.value;
    //here to get the text name of the select option, not the value
	var x = document.getElementById("mySelect3");
    var i = x.selectedIndex;
    document.getElementById("my3Text2").value = x.options[i].text;
	<?php echo $calculation; ?>
}*/

</script>

<!-- sweet alert js -->
<script type="text/javascript" src="files/bower_components/sweetalert/js/sweetalert.min.js"></script>
<!-- modalEffects js nifty modal window effects -->
<script type="text/javascript" src="files/assets/js/modalEffects.js"></script>
<?php require('inc/metadata-js.php'); ?>

<?php include "footer.php"; ?>



<script type="text/javascript">

//Group email
$(function() {

	$("#send-group-email").click(function() {
		$("#sendGroupEmail").modal("show");
		var groupname = $("[name='requiredinfoname']").val();
		var groupid   = $("[name='requiredinfo']").val();
        
		var activeCount   = $("#send-group-email").data('active') ;
		var inactiveCount   = $("#send-group-email").data('inactive') ;
        
		$(".gst-email-group-name").html(groupname);
		$(".gst-email-group-name-input").val(groupname);
		$(".gst-email-group-id").val(groupid);
        $(".gst-email-group-active-label").html('Active (' + activeCount + ')');
        $(".gst-email-group-inactive-label").html('Canceled (' + inactiveCount + ')');
	});

	$(document).on('change', '#email-template', function() {
		if ($(this).val() > 0) {
			$.ajax({
	                type: "POST",
	                url: 'inc/email-template-functions.php',
	                data: {'template_id': $(this).val(),'previewTemplate': 'Yes'},
	                dataType: "json",
	                success: function (data) {  
	                	$('#email_subject').val(data.template_subject);
	                	$('#email_message').html(data.template_body);
	                	$('#email_message_text').val(data.template_body);
	                	$('#sendGroupEmailBtn').attr('onclick', 'sendGroupEmail()');
	                },
	                error: function(jqXHR, text, error){
	                // Displaying if there are any errors
	                    $('#result').html(error);           
	                }
	            });
		} else {
			$('#email_subject').val('');
	        $('#email_message').html('');
	        $('#email_message_text').val('');
	        $('#sendGroupEmailBtn').removeAttr('onclick');
		}

		// if ($(this).val() != "")
		// {
		// 	$("[name='email_subject']").attr("disabled",true);
		// 	$("[name='email_message']").attr("disabled",true);
		// 	$(".no-template").hide();
		// }
		// else
		// {
		// 	$(".no-template").show();
		// 	$("[name='email_subject']").attr("disabled",false);
		// 	$("[name='email_message']").attr("disabled",false);
		// }
	});

});


//loads participant detail popup
function ajaxload_pax(type)
{
	var tourid = $("[name='GroupID']").val();
	$.ajax({
		type 	: "POST",
		data 	: {"type":type, "tourid":tourid},
		url 	: 'groups-edit-pax-modal.php',
		success: function (data) {
			$('#paxDetailModalBody').html(data);
			$('#paxDetailModal').modal("show");
		}
	});

}

</script>