																		<div class="card-block row row m-0 p-0">
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["title"]; ?></p>
																					<label class="float-label">Title</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["fname"]; ?></p>
																					<label class="float-label">First Name</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["mname"]; ?></p>
																					<label class="float-label">Middle Name</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["lname"]; ?></p>
																					<label class="float-label">Last Name</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["email"]; ?></p>
																					<label class="float-label">Email</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["birthday"]; ?></p>
																					<label class="float-label">Birthday</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["phone"]; ?></p>
																					<label class="float-label">Home Phone #</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["businessphone"]; ?></p>
																					<label class="float-label">Business Phone #</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["fax"]; ?></p>
																					<label class="float-label">Fax #</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["mobile"]; ?></p>
																					<label class="float-label">Mobile #</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["company"]; ?></p>
																					<label class="float-label">Company Name</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["jobtitle"]; ?></p>
																					<label class="float-label">Job Title</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["website"]; ?></p>
																					<label class="float-label">Website</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["address1"]; ?></p>
																					<label class="float-label">Address Line 1</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["address2"]; ?></p>
																					<label class="float-label">Address Line 2</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["zipcode"]; ?></p>
																					<label class="float-label">ZIP Code</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["city"]; ?></p>
																					<label class="float-label">City</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo">
																						<?php $con2 = new mysqli($servername, $username, $password, $dbname);
																						$result2 = mysqli_query($con2,"SELECT name, abbrev FROM states");$stateselected ="";
																						while($row2 = mysqli_fetch_array($result2))
																						{if($data["state"] == $row2['abbrev']) { echo $row2['name'];} else {echo "";}} ?>
																					</p>
																					<label class="float-label">State</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo">
																						<?php $con3 = new mysqli($servername, $username, $password, $dbname);
																						$result3 = mysqli_query($con2,"SELECT name, abbrev FROM countries");$countryselected ="";
																						while($row3 = mysqli_fetch_array($result3))
																						{if($data["country"] == $row3['abbrev']) {echo $row3['name'];} else {echo "";}} ?>
																					</p>
																					<label class="float-label">Country</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["martialstatus"]; ?></p>
																					<label class="float-label">Martial Status</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["gender"]; ?></p>
																					<label class="float-label">Gender</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["language"]; ?></p>
																					<label class="float-label">Language</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["affiliation"]; ?></p>
																					<label class="float-label">Affiliation Type</label>
																				</div>
																				<div class="form-group form-default form-static-label col-sm-4">
																					<p class="showinfo"><?php echo $data["preferredmethod"]; ?></p>
																					<label class="float-label">Preferred Method</label>
																				</div>
																			<div class="form-group form-default form-static-label col-md-12">
																					<p class="showinfo"><?php echo $data["notes"]; ?></p>
																					<label class="float-label">Notes for this Contact</label>
																			</div>
																		</div>
