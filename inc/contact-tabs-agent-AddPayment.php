<form id="contact36" name="contact36" method="post" action="inc/contact-functions.php">
    <div class="row">
        <div class="form-group col-sm-12">
            <h3>Record an Agent's Payment</h3>
        </div>
        <div class="col-sm-4">
            <label class="float-label">Date of Payment</label>
            <input type="text" name="PaymentAgentID" value="<?php echo $agentdata["contactid"]; ?>" class="form-control" hidden>
            <input type="text" name="PaymentAgentName" value="<?php echo $data['fname']." ".$data['mname']." ".$data['lname']; ?>" class="form-control" hidden>
            <input type="date" name="Agent_Payment_Date" class="form-control" required>
        </div>
        <div class="col-sm-4">
            <label class="float-label">Amount of Payment</label>
            <div class="input-group">
                <span class="input-group-prepend">
                    <label class="input-group-text">$</label>
                </span>
                <input name="Agent_Payment_Amount" type="text" class="form-control" required>
            </div>
        </div>
        <div class="col-12"></div>
        <div class="col-sm-4">
            <label class="float-label gst-label">Payment Method</label>
            <select name="Agent_Payment_Method" id="idSelectPaymentMethod" class="form-control" required>
                <option value="">Select</option>
                <option <?php echo (($refundType == 'Cash') ? 'selected' : '');?> value="Cash">Cash</option>
                <option <?php echo (($refundType == 'Bank Transfer') ? 'selected' : '');?> value="Bank Transfer">Bank Transfer</option>
                <option <?php echo (($refundType == 'Check') ? 'selected' : '');?> value="Check">Check</option>
                <option <?php echo (($refundType == 'Credit Card') ? 'selected' : '');?> value="Credit Card">Credit Card</option>
                <option <?php echo (($refundType == 'Online Payment') ? 'selected' : '');?> value="Online Payment">Online Payment</option>
            </select>
        </div>
        <div class="col-sm-4">
            <label class="float-label gst-label">From Account</label>
            <select name="From_Account_None" id="Accounts_None" class="form-control">
                <option value="0" disabled selected>Select Payment Method</option>
            </select>
            <select name="From_Account_Cash" id="Accounts_cash" class="form-control" style="display:none;">
                <option value="">Select</option>
                <?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name FROM checks_bank WHERE Bank_Type='cash' AND Bank_Status=1";
					$BanksData = GDb::fetchRowSet($GetBankAccountSQL);
					foreach ($BanksData AS $BankData) { ?>
                <option value="<?php echo $BankData['Bank_ID']; ?>"><?php echo $BankData['Bank_Name']; ?></option>
                <?php } ?>
            </select>
            <select name="From_Account_Bank" id="Accounts_bank" class="form-control" style="display:none;">
                <option value="" data-checknumber="N/A">Select</option>
                <?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name, Bank_Check_Number FROM checks_bank WHERE Bank_Type='bank' AND Bank_Status=1";
					$BanksData = GDb::fetchRowSet($GetBankAccountSQL);
					foreach ($BanksData AS $BankData) { ?>
                <option value="<?php echo $BankData['Bank_ID']; ?>" data-checknumber='<?php echo $BankData['Bank_Check_Number']; ?>'><?php echo $BankData['Bank_Name']; ?></option>
                <?php } ?>
            </select>
            <select name="From_Account_Card" id="Accounts_cards" class="form-control" style="display:none;">
                <option value="">Select</option>
                <?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name FROM checks_bank WHERE Bank_Type='cards' AND Bank_Status=1";
					$BanksData = GDb::fetchRowSet($GetBankAccountSQL);
					foreach ($BanksData AS $BankData) { ?>
                <option value="<?php echo $BankData['Bank_ID']; ?>"><?php echo $BankData['Bank_Name']; ?></option>
                <?php } ?>
            </select>
            <select name="From_Account_PayPal" id="Accounts_paypal" class="form-control" style="display:none;">
                <option value="">Select</option>
                <?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name FROM checks_bank WHERE Bank_Type='paypal' AND Bank_Status=1";
					$BanksData = GDb::fetchRowSet($GetBankAccountSQL);
					foreach ($BanksData AS $BankData) { ?>
                <option value="<?php echo $BankData['Bank_ID']; ?>"><?php echo $BankData['Bank_Name']; ?></option>
                <?php } ?>
            </select>
        </div>
        <div id="CheckNumberDiv" class="col-sm-4" style="display:none;">
            <label class="float-label gst-label">Check Number <small>(Below is the automatic number)</small></label>
            <input type="text" name="Check_Number" id='Check_Number' placeholder="Select a bank to get the check number" class="form-control">
        </div>
        <div class="col-sm-12 mt-3">
            <label class="float-label">Select the group you are willing to record a payment for</label>
            <select data-current-agent-id="<?php echo $agentid;?>" name="AgentGroupID" class="select2-agent-groups form-control form-control-default" multiple="multiple" required>
            </select>
        </div>
        <div class="col-sm-12">
            <label class="float-label">Comments / Refrences</label>
            <input type="text" name="Agent_Payment_Comments" class="form-control">
        </div>
        <div class="col-sm-12"><br />
            <button type="submit" name="AddAgentPaymentSubmit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Record</button>
            <button type="reset" class="btn waves-effect waves-light btn-inverse" data-dismiss="modal"><i class="fas fa-ban"></i>Cancel</button>
        </div>
    </div>
</form>
<script>
$(document).ready(function(){
    $('#idSelectPaymentMethod').on('change', function() {
      if ( this.value == 'Cash') {
        $("#Accounts_cash").show('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").hide('slow');
        $("#Accounts_bank").hide('slow');
        $("#Accounts_paypal").hide('slow');
        $("#CheckNumberDiv").hide('slow');
      } else if ( this.value == 'Bank Transfer') {
        $("#Accounts_cash").hide('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").hide('slow');
        $("#Accounts_bank").show('slow');
        $("#Accounts_paypal").hide('slow');
        $("#CheckNumberDiv").hide('slow');
      } else if ( this.value == 'Check') {
        $("#Accounts_cash").hide('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").hide('slow');
        $("#Accounts_bank").show('slow');
        $("#Accounts_paypal").hide('slow');
        $("#CheckNumberDiv").show('slow');
      } else if ( this.value == 'Credit Card') {
        $("#Accounts_cash").hide('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").show('slow');
        $("#Accounts_bank").hide('slow');
        $("#Accounts_paypal").hide('slow');
        $("#CheckNumberDiv").hide('slow');
      } else if ( this.value == 'Online Payment') {
        $("#Accounts_cash").hide('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").hide('slow');
        $("#Accounts_bank").hide('slow');
        $("#Accounts_paypal").show('slow');
        $("#CheckNumberDiv").hide('slow');
      }
    });
});


var selection = document.getElementById("Accounts_bank");
selection.onchange = function(event){
  var checknumber = event.target.options[event.target.selectedIndex].dataset.checknumber;
  document.getElementById("Check_Number").value = checknumber;
};
</script>