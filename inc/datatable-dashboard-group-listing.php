<?php
//include connection file 
include __DIR__ . "/../config.php";

include_once __DIR__ . '/../models/tour_membership.php';
include_once __DIR__ . '/../models/groups.php';

$params = $__REQUEST;
$where  = '';
$limit = "LIMIT " . $params['start'] . " ," . $params['length'] . " ;" ;
// check search value exist
if (!empty($params['search']['value'])) {
    $where .= " AND ";
    $where .= " ( tourname LIKE '%" . $params['search']['value'] . "%' ) ";
}


if( $params['order'][0]['column'] == 0 )  {
    $orderBy = " ORDER BY startdate ASC " ;
}
else {
    $orderBy = " ORDER BY tourname ".$params['order'][0]['dir']." " ;
}

$gobj = new Groups() ;
$records = $gobj->getActiveTrips($where, $orderBy, $limit ) ;
$tripCount = $gobj->getActiveTripCount($where, $orderBy) ;

$outputSet = [] ;
foreach( $records as $row ) {
						
							//To show the Green Check or Red X before the title of the group
							if($row["status"]==1) { $groupstatus = "<i class='far fa-check-circle' style='color: #198a10;'></i>"; }
							else { $groupstatus = "<i class='far fa-times-circle' style='color: red;'></i>"; }
							
							$tourzid = $row["tourid"];
							$AirlineProductID = $row["airline_productid"];
							$paxsql = "SELECT COUNT(contactid) AS PaxNumbers FROM customer_account WHERE tourid=$tourzid AND status=1";
							$paxresult = $db->query($paxsql);
							$PaxNumberz = $paxresult->fetch_assoc();
							$paxtotal = $PaxNumberz['PaxNumbers'];

							$seatsql = "SELECT seats FROM products WHERE id=$AirlineProductID";
							$paxresult = $db->query($seatsql);
							$seatdata = $paxresult->fetch_assoc();
							
							if($seatdata["seats"] <= $paxtotal) {
								$paxwarning = 1;
								$warningsql = "UPDATE groups SET warnings=1 WHERE tourid=$tourzid AND status=1;";
								$db->query($warningsql);
							} else {
								$paxwarning =0;
								$warningsqldone = "UPDATE groups SET warnings=0 WHERE tourid=$tourzid AND status=1;";
								$db->query($warningsqldone);
							}
                            
                            $output = [] ;
                            $output[0] = $row["startdate"] ;
                            ob_start() ;
							?> 
                                <?php echo $groupstatus ?> <a href="groups-edit.php?id=<?php echo $row["tourid"]; ?>&action=edit" style="font-weight:bold">
                                <span data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $row['tourname']; ?>"><?php echo substr($row['tourname'], 0, 40); ?>...</span>
                                <br /><small>(From <?php echo date('m/d/Y', strtotime($row["startdate"])); ?>, until <?php echo date('m/d/Y', strtotime($row["enddate"])); ?>)</small></a>
							<?php
                            $output[1] = ob_get_clean() ;
                            ob_start() ;
							?> 

									<a href="groups-edit.php?id=<?php echo $row["tourid"]; ?>&action=edit&tab=participants" style="font-size:16px;font-weight:bold;">
									<?php
									$touridzz = $row["tourid"];
									echo intval( (new TourMembership())->paxTotal($touridzz) ) ;
                                    ?>
									</a><small> out of <?php if($seatdata["seats"] == "") {echo "0";} else {echo $seatdata["seats"];} ?></small>
							<?php
                            $output[2] = ob_get_clean() ;
                            $output[3] = $paxwarning ;
                            
                            $outputSet[] = $output ;
}

$json_data = array(
    "draw"            => intval($params['draw']),
    "recordsTotal"    => intval($tripCount),
    "recordsFiltered" => intval($tripCount),
    "data"            => $outputSet   // total data array
);
echo json_encode($json_data);  // send data as json format
