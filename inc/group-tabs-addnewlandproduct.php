<form  id="contact6" name="contact6" method="post" action="groups-edit.php?id=<?php echo $tourid; ?>&action=edit&tab=land">
    <div class="row">
                    <div class="form-group col-sm-12">
                        <h3>Add a new Land Product</h3>
                    </div>
                    <div class="form-group col-sm-4">
                        <h4 class="sub-title">Product Status</h4>
						<div class="can-toggle">
							<input id="e" name="status" value="1" type="checkbox" checked>
							<label for="e">
								<div class="can-toggle__switch" data-checked="Active" data-unchecked="Inactive"></div>
							</label>
						</div>
                    </div>
                    <div class="col-sm-4">
						<h4 class="sub-title">Product's Supplier</h4>
						<select name="thesupplier" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple" required>
							<?php //here, we are checking the Agent ID with the contact column list to show their names
							$con7 = new mysqli($servername, $username, $password, $dbname);
							$result7 = mysqli_query($con7,"SELECT id, fname, mname, lname FROM contacts WHERE `usertype` LIKE '%2%'");
							while($row7 = mysqli_fetch_array($result7))
							{
								echo "<option value='".$row7['id']."'>".$row7['fname']." ".$row7['mname']." ".$row7['lname']."</option>";
							} ?>
						</select>
                    </div>
                    <div class="col-sm-4">
						<h4 class="sub-title">Product's Category</h4>
						<select name="thecategory" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<option value="2" selected>Land Products</option>
						</select>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">Product Name</label>
                        <input type="text" name="productname" class="form-control" required="">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-8">
                        <label class="float-label">Product Discription</label>
                        <input type="text" name="description" class="form-control">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label">Cost $</label>
                        <input type="text" name="cost" class="form-control">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label">Price $</label>
                        <input type="text" name="price" class="form-control">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label">Single Supplement Cost $</label>
                        <input type="text" name="SingleSupplementCost" class="form-control">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label">Single Supplement Price $</label>
                        <input type="text" name="SingleSupplementPrice" class="form-control">
                    </div>
                    <div class="col-sm-12"><br />
					<button type="submit" name="landsubmit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;"><i class="far fa-check-circle"></i>Add</button>
                    <button type="reset" class="btn waves-effect waves-light btn-inverse" data-dismiss="modal"><i class="fas fa-ban"></i>Cancel</button>
					</div>
	</div>
</form>
