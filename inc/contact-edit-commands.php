<?php 

//To Delete a contact from the whole system
if ($_GET['action'] == "delete") {
	$con4 = new mysqli($servername, $username, $password, $dbname);
	if ($con4->connect_error) {die("Connection failed: " . $con4->connect_error);} 
	$sql2 = "DELETE FROM contacts WHERE id=$leadid;";
	$sql2 .= "DELETE FROM agents WHERE contactid=$leadid;";
	$sql2 .= "DELETE FROM customer_account WHERE contactid=$leadid;";
	$sql2 .= "DELETE FROM suppliers WHERE contactid=$leadid;";
	if ($con4->multi_query($sql2) === TRUE) {
		echo "<meta http-equiv='refresh' content='0;url=contacts.php?action=delete&fullname=$contactname&cid=$leadid'>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting record: " . $con4->error . "</strong></div>";
	}
	$con4->close();
}


// To Upgrade a Contact to an Agent
if ($_GET['action'] == "makeagent") {
	$usertypeupdate = "0";
	if($data["usertype"] == 0){$usertypeupdate = "1";}
	else{$usertypeupdate = $data["usertype"].",1";}

	$AgentCommand = "INSERT INTO agents (contactid,status) VALUES ('$leadid','1') ON DUPLICATE KEY UPDATE status = 1, updatetime = NOW();";
	$ContactToAgentCommand = "UPDATE contacts SET usertype='$usertypeupdate', updatetime=NOW() WHERE id=$leadid;";
	$LogCommand = "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','agent','$leadid','upgraded','$contactname');";
	GDb::execute($AgentCommand);

	if(GDb::execute($AgentCommand)) {
		GDb::execute($ContactToAgentCommand);
		GDb::execute($LogCommand);
		echo "<meta http-equiv='refresh' content='0;url=contacts-edit.php?id=$leadid&action=edit&usertype=agent'>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error creating an agent. ($AgentCommand) and ($ContactToAgentCommand)</strong></div>";
	}
}

// To Disable the Current Agent
if ($_GET['action'] == "disableagent") {
	$AgentCommand = "UPDATE agents SET status='0', updatetime=NOW() WHERE contactid=$leadid;";
	$LogCommand = "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','agent','$leadid','disabled','$contactname');";
	GDb::execute($AgentCommand);

	if(GDb::execute($AgentCommand)) {
		GDb::execute($LogCommand);
		echo "<meta http-equiv='refresh' content='0;url=contacts-edit.php?id=$leadid&action=edit&usertype=agent'>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error disabling the agent. ($AgentCommand)</strong></div>";
	}
}

// To Enable the Current Agent
if ($_GET['action'] == "enableagent") {
	$AgentCommand = "UPDATE agents SET status='1', updatetime=NOW() WHERE contactid=$leadid;";
	$LogCommand = "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','agent','$leadid','enabled','$contactname');";
	GDb::execute($AgentCommand);

	if(GDb::execute($AgentCommand)) {
		GDb::execute($LogCommand);
		echo "<meta http-equiv='refresh' content='0;url=contacts-edit.php?id=$leadid&action=edit&usertype=agent'>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error enabling the agent. ($AgentCommand)</strong></div>";
	}
}


// To Upgrade a Contact to a Supplier
if ($_GET['action'] == "makesupplier") {
	$usertypeupdate = "0";
	if($data["usertype"] == 0){$usertypeupdate = "2";}
	else{$usertypeupdate = $data["usertype"].",2";}

	$SupplierCommand = "INSERT INTO suppliers (contactid,status) VALUES ('$leadid','1') ON DUPLICATE KEY UPDATE status = 1, updatetime = NOW();";
	$ContactToSupplierCommand = "UPDATE contacts SET usertype='$usertypeupdate', updatetime=NOW() WHERE id=$leadid;";
	$LogCommand = "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','supplier','$leadid','upgraded','$contactname');";
	GDb::execute($SupplierCommand);

	if(GDb::execute($SupplierCommand)) {
		GDb::execute($ContactToSupplierCommand);
		GDb::execute($LogCommand);
		echo "<meta http-equiv='refresh' content='0;url=contacts-edit.php?id=$leadid&action=edit&usertype=supplier'>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error creating a supplier. ($SupplierCommand) and ($ContactToSupplierCommand)</strong></div>";
	}
}

// To Disable the Current Supplier
if ($_GET['action'] == "disablesupplier") {
	$SupplierCommand = "UPDATE suppliers SET status='0', updatetime=NOW() WHERE contactid=$leadid;";
	$LogCommand = "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','supplier','$leadid','disabled','$contactname');";
	GDb::execute($SupplierCommand);

	if(GDb::execute($SupplierCommand)) {
		GDb::execute($LogCommand);
		echo "<meta http-equiv='refresh' content='0;url=contacts-edit.php?id=$leadid&action=edit&usertype=supplier'>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error disabling the supplier. ($SupplierCommand)</strong></div>";
	}
}

// To Enable the Current Supplier
if ($_GET['action'] == "enablesupplier") {
	$SupplierCommand = "UPDATE suppliers SET status='1', updatetime=NOW() WHERE contactid=$leadid;";
	$LogCommand = "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','supplier','$leadid','enabled','$contactname');";
	GDb::execute($SupplierCommand);

	if(GDb::execute($SupplierCommand)) {
		GDb::execute($LogCommand);
		echo "<meta http-equiv='refresh' content='0;url=contacts-edit.php?id=$leadid&action=edit&usertype=supplier'>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error enabling the supplier. ($SupplierCommand)</strong></div>";
	}
}


// To Upgrade a Contact to a Leader
if ($_GET['action'] == "makeleader") {
	$usertypeupdate = "0";
	if($data["usertype"] == 0){$usertypeupdate = "3";}
	else{$usertypeupdate = $data["usertype"].",3";}

	$LeaderCommand = "INSERT INTO group_leaders (contactid,status) VALUES ('$leadid','1') ON DUPLICATE KEY UPDATE status = 1, updatetime = NOW();";
	$ContactToLeaderCommand = "UPDATE contacts SET usertype='$usertypeupdate', updatetime=NOW() WHERE id=$leadid;";
	$LogCommand = "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','group leader','$leadid','upgraded','$contactname');";
	GDb::execute($LeaderCommand);

	if(GDb::execute($LeaderCommand)) {
		GDb::execute($ContactToLeaderCommand);
		GDb::execute($LogCommand);
		echo "<meta http-equiv='refresh' content='0;url=contacts-edit.php?id=$leadid&action=edit&usertype=leader'>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error creating the Group Leader. ($LeaderCommand) and ($ContactToLeaderCommand)</strong></div>";
	}
}

// To Disable the Current Leader
if ($_GET['action'] == "disableleader") {
	$LeaderCommand = "UPDATE group_leaders SET status='0', updatetime=NOW() WHERE contactid=$leadid;";
	$LogCommand = "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','group leader','$leadid','disabled','$contactname');";
	GDb::execute($LeaderCommand);

	if(GDb::execute($LeaderCommand)) {
		GDb::execute($LogCommand);
		echo "<meta http-equiv='refresh' content='0;url=contacts-edit.php?id=$leadid&action=edit&usertype=leader'>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error disabling the Group Leader. ($LeaderCommand)</strong></div>";
	}
}

// To Enable the Current Leader
if ($_GET['action'] == "enableleader") {
	$LeaderCommand = "UPDATE group_leaders SET status='1', updatetime=NOW() WHERE contactid=$leadid;";
	$LogCommand = "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','group leader','$leadid','enabled','$contactname');";
	GDb::execute($LeaderCommand);

	if(GDb::execute($LeaderCommand)) {
		GDb::execute($LogCommand);
		echo "<meta http-equiv='refresh' content='0;url=contacts-edit.php?id=$leadid&action=edit&usertype=leader'>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error enabling the Group Leader. ($LeaderCommand)</strong></div>";
	}
}



//This is to add a new Airline Upgrade Product in the customer account
if(isset($_POST['newairlineupgradesubmit'])){
    $AirUpgrade_status = 1;
    $AirUpgrade_thesupplier = mysqli_real_escape_string($db, $_POST["AirUpgrade_thesupplier"]);
    $AirUpgrade_thecategory = 4;
    $AirUpgrade_productname = mysqli_real_escape_string($db, $_POST["AirUpgrade_productname"]);
    $AirUpgrade_description = mysqli_real_escape_string($db, $_POST["AirUpgrade_description"]);
    $AirUpgrade_cost = mysqli_real_escape_string($db, $_POST["AirUpgrade_cost"]);
    $AirUpgrade_price = mysqli_real_escape_string($db, $_POST["AirUpgrade_price"]);
    $AirUpg_GroupID = mysqli_real_escape_string($db, $_POST["AirUpg_GroupID"]);
    
    $sql = "INSERT INTO products (name, description, supplierid, categoryid, subproduct, cost, price, paymentterm, status)
	VALUES ('$AirUpgrade_productname','$AirUpgrade_description','$AirUpgrade_thesupplier','$AirUpgrade_thecategory','0','$AirUpgrade_cost','$AirUpgrade_price','0','$AirUpgrade_status')";
	if ($db->query($sql) === TRUE) {
		$NewAirUpgradeProductID = mysqli_insert_id($db);
		$AirUpgradeProConn = new mysqli($servername, $username, $password, $dbname);
		if ($AirUpgradeProConn->connect_error) {die("Connection failed: " . $AirUpgradeProConn->connect_error);} 
		
		$AirUpgradeProsql = "UPDATE customer_account SET upgradeproduct='$NewAirUpgradeProductID',upgrade=1, updatetime=NOW() WHERE contactid=$leadid AND tourid=$AirUpg_GroupID;";		
		$AirUpgradeProsql .= "DELETE FROM customer_invoice_line WHERE Line_Type='5' AND Customer_Invoice_Num=(SELECT ci.Customer_Invoice_Num FROM customer_invoice ci WHERE ci.Group_ID=$AirUpg_GroupID AND ci.Customer_Account_Customer_ID=$leadid);";		
		$AirUpgradeProsql .= "DELETE FROM sales_order_line WHERE Line_Type='Air Upgrade' AND Sales_Order_Num=(SELECT so.Sales_Order_Num FROM sales_order so WHERE so.Group_ID=$AirUpg_GroupID AND so.Customer_Account_Customer_ID=$leadid);";		
		$AirUpgradeProsql .= "INSERT INTO sales_order_line (Sales_Order_Num, Line_Type, Product_Product_ID, Product_Name, Sales_Amount, Supplier_Supplier_ID) SELECT so.Sales_Order_Num,'Air Upgrade', '$NewAirUpgradeProductID','$AirUpgrade_productname','$AirUpgrade_price','$AirUpgrade_thesupplier' FROM sales_order so WHERE so.Group_ID=$AirUpg_GroupID AND so.Customer_Account_Customer_ID=$leadid;";		
		$AirUpgradeProsql .= "INSERT INTO customer_invoice_line (Customer_Invoice_Num, Customer_Invoice_Line, Line_Type, Product_Product_ID, Product_Name, Qty, Invoice_Line_Total) SELECT ci.Customer_Invoice_Num,'5', '5','$NewAirUpgradeProductID','$AirUpgrade_productname','1','$AirUpgrade_price' FROM customer_invoice ci WHERE ci.Group_ID=$AirUpg_GroupID AND ci.Customer_Account_Customer_ID=$leadid;";		
		$AirUpgradeProsql .= "UPDATE customer_invoice 
									INNER JOIN (
										SELECT SUM(cil.Qty * cil.Invoice_Line_Total) as Total, Customer_Invoice_Num FROM customer_invoice_line cil
										INNER JOIN products p ON p.id = cil.Product_Product_ID 
										GROUP BY Customer_Invoice_Num 
									) AS A ON A.Customer_Invoice_Num = customer_invoice.Customer_Invoice_Num 
								SET customer_invoice.Invoice_Amount=A.Total
								WHERE customer_invoice.Group_ID='$AirUpg_GroupID' AND customer_invoice.Customer_Account_Customer_ID='$leadid';";

		if ($AirUpgradeProConn->multi_query($AirUpgradeProsql) === TRUE) {
		//success text here if needed
		} else { 
			echo $AirUpgradeProsql . "<br>" . $AirUpgradeProConn->error; 
		}
		$AirUpgradeProConn->close();
		
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The new airline upgrade product has been created successfully! The page will be refreshed in 2 seconds.</strong></div>";
		echo "<meta http-equiv='refresh' content='3'>";
	}
}

//This is to add a new Land Upgrade Product in the customer account
if(isset($_POST['newlandsubmit'])){
    $LandUpgrade_status = 1;
    $LandUpgrade_thesupplier = mysqli_real_escape_string($db, $_POST["LandUpgrade_thesupplier"]);
    $LandUpgrade_thecategory = 5;
    $LandUpgrade_productname = mysqli_real_escape_string($db, $_POST["LandUpgrade_productname"]);
    $LandUpgrade_description = mysqli_real_escape_string($db, $_POST["LandUpgrade_description"]);
    $LandUpgrade_cost = mysqli_real_escape_string($db, $_POST["LandUpgrade_cost"]);
    $LandUpgrade_price = mysqli_real_escape_string($db, $_POST["LandUpgrade_price"]);
    $LandUpg_GroupID = mysqli_real_escape_string($db, $_POST["LandUpg_GroupID"]);
    
    $LandUpgradeProConn1 = new mysqli($servername, $username, $password, $dbname);
	if ($LandUpgradeProConn1->connect_error) {die("Connection failed: " . $LandUpgradeProConn1->connect_error);} 
	$LandUpgradeProsql1 = "INSERT INTO products (name, description, supplierid, categoryid, subproduct, cost, price, paymentterm, status)
	VALUES ('$LandUpgrade_productname','$LandUpgrade_description','$LandUpgrade_thesupplier','$LandUpgrade_thecategory','0','$LandUpgrade_cost','$LandUpgrade_price','0','$LandUpgrade_status')";
	if ($LandUpgradeProConn1->query($LandUpgradeProsql1) === TRUE) {
		$NewLandUpgradeProductID = mysqli_insert_id($LandUpgradeProConn1);
		$LandUpgradeProConn = new mysqli($servername, $username, $password, $dbname);
		if ($LandUpgradeProConn->connect_error) {die("Connection failed: " . $LandUpgradeProConn->connect_error);} 

		$LandUpgradeProsql = "UPDATE customer_account SET upgrade_land_product='$NewLandUpgradeProductID',upgrade=1, updatetime=NOW() WHERE contactid=$leadid AND tourid=$LandUpg_GroupID;";		
		$LandUpgradeProsql .= "DELETE FROM customer_invoice_line WHERE Line_Type='6' AND Customer_Invoice_Num=(SELECT ci.Customer_Invoice_Num FROM customer_invoice ci WHERE ci.Group_ID=$LandUpg_GroupID AND ci.Customer_Account_Customer_ID=$leadid);";		
		$LandUpgradeProsql .= "DELETE FROM sales_order_line WHERE Line_Type='Land Upgrade' AND Sales_Order_Num=(SELECT so.Sales_Order_Num FROM sales_order so WHERE so.Group_ID=$LandUpg_GroupID AND so.Customer_Account_Customer_ID=$leadid);";		
		$LandUpgradeProsql .= "INSERT INTO sales_order_line (Sales_Order_Num, Line_Type, Product_Product_ID, Product_Name, Sales_Amount, Supplier_Supplier_ID) SELECT so.Sales_Order_Num,'Land Upgrade', '$NewLandUpgradeProductID','$LandUpgrade_productname','$LandUpgrade_price','$LandUpgrade_thesupplier' FROM sales_order so WHERE so.Group_ID=$LandUpg_GroupID AND so.Customer_Account_Customer_ID=$leadid;";		
		$LandUpgradeProsql .= "INSERT INTO customer_invoice_line (Customer_Invoice_Num, Customer_Invoice_Line, Line_Type, Product_Product_ID, Product_Name, Qty, Invoice_Line_Total) SELECT ci.Customer_Invoice_Num,'6', '6','$NewLandUpgradeProductID','$LandUpgrade_productname','1','$LandUpgrade_price' FROM customer_invoice ci WHERE ci.Group_ID=$LandUpg_GroupID AND ci.Customer_Account_Customer_ID=$leadid;";		
		$LandUpgradeProsql .= "UPDATE customer_invoice 
									INNER JOIN (
										SELECT SUM(cil.Qty * cil.Invoice_Line_Total) as Total, Customer_Invoice_Num FROM customer_invoice_line cil
										INNER JOIN products p ON p.id = cil.Product_Product_ID 
										GROUP BY Customer_Invoice_Num 
									) AS A ON A.Customer_Invoice_Num = customer_invoice.Customer_Invoice_Num 
								SET customer_invoice.Invoice_Amount=A.Total
								WHERE customer_invoice.Group_ID='$LandUpg_GroupID' AND customer_invoice.Customer_Account_Customer_ID='$leadid';";

		if ($LandUpgradeProConn->multi_query($LandUpgradeProsql) === TRUE) {
		//success text here if needed
		} else { 
			echo $LandUpgradeProsql . "<br>" . $LandUpgradeProConn->error; 
		}
		$LandUpgradeProConn->close();
		
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The new Land upgrade product has been created successfully! The page will be refreshed in 2 seconds.</strong></div>";
		echo "<meta http-equiv='refresh' content='3'>";
	} else { 
		echo $LandUpgradeProsql1 . "<br>" . $LandUpgradeProConn1->error; 
	}
	$LandUpgradeProConn1->close();
}

//This is to add a new Extension Product in the customer account
if(isset($_POST['extensionsubmit'])){
    $Extension_status = 1;
    $Extension_supplier = mysqli_real_escape_string($db, $_POST["Extension_supplier"]);
    $Extension_thecategory = 8;
    $Extension_productname = mysqli_real_escape_string($db, $_POST["extension_productname"]);
    $Extension_description = mysqli_real_escape_string($db, $_POST["extension_description"]);
    $Extension_cost = mysqli_real_escape_string($db, $_POST["extension_cost"]);
    $Extension_price = mysqli_real_escape_string($db, $_POST["extension_price"]);
    $Extension_GroupID = mysqli_real_escape_string($db, $_POST["Extension_GroupID"]);
    
    $ExtensionConn = new mysqli($servername, $username, $password, $dbname);
	if ($ExtensionConn->connect_error) {die("Connection failed: " . $ExtensionConn->connect_error);} 
	$ExtensionSQL = "INSERT INTO products (name, description, supplierid, categoryid, subproduct, cost, price, paymentterm, status)
	VALUES ('$Extension_productname','$Extension_description','$Extension_supplier','$Extension_thecategory','0','$Extension_cost','$Extension_price','0','$Extension_status')";
	if ($ExtensionConn->query($ExtensionSQL) === TRUE) {
		$NewExtensionProductID = mysqli_insert_id($ExtensionConn);
		$ExtensionUpdateConn = new mysqli($servername, $username, $password, $dbname);
		if ($ExtensionUpdateConn->connect_error) {die("Connection failed: " . $ExtensionUpdateConn->connect_error);} 
		
		$ExtensionUpdateSQL = "UPDATE customer_account SET Extension_Product_ID='$NewExtensionProductID',Extension=1, updatetime=NOW() WHERE contactid=$leadid AND tourid=$Extension_GroupID;";		
		$ExtensionUpdateSQL .= "DELETE FROM customer_invoice_line WHERE Line_Type='8' AND Customer_Invoice_Num=(SELECT ci.Customer_Invoice_Num FROM customer_invoice ci WHERE ci.Group_ID=$Extension_GroupID AND ci.Customer_Account_Customer_ID=$leadid);";		
		$ExtensionUpdateSQL .= "DELETE FROM sales_order_line WHERE Line_Type='Extension' AND Sales_Order_Num=(SELECT so.Sales_Order_Num FROM sales_order so WHERE so.Group_ID=$Extension_GroupID AND so.Customer_Account_Customer_ID=$leadid);";		
		$ExtensionUpdateSQL .= "INSERT INTO sales_order_line (Sales_Order_Num, Line_Type, Product_Product_ID, Product_Name, Sales_Amount, Supplier_Supplier_ID) SELECT so.Sales_Order_Num,'Extension', '$NewExtensionProductID','$Extension_productname','$Extension_price','$Extension_supplier' FROM sales_order so WHERE so.Group_ID=$Extension_GroupID AND so.Customer_Account_Customer_ID=$leadid;";		
		$ExtensionUpdateSQL .= "INSERT INTO customer_invoice_line (Customer_Invoice_Num, Customer_Invoice_Line, Line_Type, Product_Product_ID, Product_Name, Qty, Invoice_Line_Total) SELECT ci.Customer_Invoice_Num,'8', '8','$NewExtensionProductID','$Extension_productname','1','$Extension_price' FROM customer_invoice ci WHERE ci.Group_ID=$Extension_GroupID AND ci.Customer_Account_Customer_ID=$leadid;";		
		$ExtensionUpdateSQL .= "UPDATE customer_invoice 
									INNER JOIN (
										SELECT SUM(cil.Qty * cil.Invoice_Line_Total) as Total, Customer_Invoice_Num FROM customer_invoice_line cil
										INNER JOIN products p ON p.id = cil.Product_Product_ID 
										GROUP BY Customer_Invoice_Num 
									) AS A ON A.Customer_Invoice_Num = customer_invoice.Customer_Invoice_Num 
								SET customer_invoice.Invoice_Amount=A.Total
								WHERE customer_invoice.Group_ID='$Extension_GroupID' AND customer_invoice.Customer_Account_Customer_ID='$leadid';";		
		
		if ($ExtensionUpdateConn->multi_query($ExtensionUpdateSQL) === TRUE) {
		//success text here if needed
		} else { 
			echo $ExtensionUpdateSQL . "<br>" . $ExtensionUpdateConn->error; 
		}
		$ExtensionUpdateConn->close();
		
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The new Extension product has been created successfully! The page will be refreshed in 2 seconds.</strong></div>";
		echo "<meta http-equiv='refresh' content='3'>";
	} else { 
		echo $ExtensionSQL . "<br>" . $ExtensionConn->error; 
	}
	$ExtensionConn->close();
}

//This is to add a new Insurance Product in the customer account
if(isset($_POST['insurancesubmit'])){
    $Insurance_status = 1;
    $Insurance_supplier = mysqli_real_escape_string($db, $_POST["Insurance_supplier"]);
    $Insurance_thecategory = 3;
    $Insurance_productname = mysqli_real_escape_string($db, $_POST["insurance_productname"]);
    $Insurance_description = mysqli_real_escape_string($db, $_POST["insurance_description"]);
    $Insurance_cost = mysqli_real_escape_string($db, $_POST["insurance_cost"]);
    $Insurance_price = mysqli_real_escape_string($db, $_POST["insurance_price"]);
    $Insurance_GroupID = mysqli_real_escape_string($db, $_POST["Insurance_GroupID"]);
    
    $InsuranceConn = new mysqli($servername, $username, $password, $dbname);
	if ($InsuranceConn->connect_error) {die("Connection failed: " . $InsuranceConn->connect_error);} 
	$InsuranceSQL = "INSERT INTO products (name, description, supplierid, categoryid, subproduct, cost, price, paymentterm, status)
	VALUES ('$Insurance_productname','$Insurance_description','$Insurance_supplier','$Insurance_thecategory','0','$Insurance_cost','$Insurance_price','0','$Insurance_status')";
	if ($InsuranceConn->query($InsuranceSQL) === TRUE) {
		$NewInsuranceProductID = mysqli_insert_id($InsuranceConn);
		$InsuranceUpdateConn = new mysqli($servername, $username, $password, $dbname);
		if ($InsuranceUpdateConn->connect_error) {die("Connection failed: " . $InsuranceUpdateConn->connect_error);} 
		
		$InsuranceUpdateSQL = "UPDATE customer_account SET Insurance_Product_ID='$NewInsuranceProductID',insurance=1, updatetime=NOW() WHERE contactid=$leadid AND tourid=$Insurance_GroupID;";		
		$InsuranceUpdateSQL .= "DELETE FROM customer_invoice_line WHERE Line_Type='10' AND Customer_Invoice_Num=(SELECT ci.Customer_Invoice_Num FROM customer_invoice ci WHERE ci.Group_ID=$Insurance_GroupID AND ci.Customer_Account_Customer_ID=$leadid);";		
		$InsuranceUpdateSQL .= "DELETE FROM sales_order_line WHERE Line_Type='Insurance' AND Sales_Order_Num=(SELECT so.Sales_Order_Num FROM sales_order so WHERE so.Group_ID=$Insurance_GroupID AND so.Customer_Account_Customer_ID=$leadid);";		
		$InsuranceUpdateSQL .= "INSERT INTO sales_order_line (Sales_Order_Num, Line_Type, Product_Product_ID, Product_Name, Sales_Amount, Supplier_Supplier_ID) SELECT so.Sales_Order_Num,'Insurance', '$NewInsuranceProductID','$Insurance_productname','$Insurance_price','$Insurance_supplier' FROM sales_order so WHERE so.Group_ID=$Insurance_GroupID AND so.Customer_Account_Customer_ID=$leadid;";		
		$InsuranceUpdateSQL .= "INSERT INTO customer_invoice_line (Customer_Invoice_Num, Customer_Invoice_Line, Line_Type, Product_Product_ID, Product_Name, Qty, Invoice_Line_Total) SELECT ci.Customer_Invoice_Num,'10', '10','$NewInsuranceProductID','$Insurance_productname','1','$Insurance_price' FROM customer_invoice ci WHERE ci.Group_ID=$Insurance_GroupID AND ci.Customer_Account_Customer_ID=$leadid;";		
		$InsuranceUpdateSQL .= "UPDATE customer_invoice 
									INNER JOIN (
										SELECT SUM(cil.Qty * cil.Invoice_Line_Total) as Total, Customer_Invoice_Num FROM customer_invoice_line cil
										INNER JOIN products p ON p.id = cil.Product_Product_ID 
										GROUP BY Customer_Invoice_Num 
									) AS A ON A.Customer_Invoice_Num = customer_invoice.Customer_Invoice_Num 
								SET customer_invoice.Invoice_Amount=A.Total
								WHERE customer_invoice.Group_ID='$Insurance_GroupID' AND customer_invoice.Customer_Account_Customer_ID='$leadid';";		
		
		if ($InsuranceUpdateConn->multi_query($InsuranceUpdateSQL) === TRUE) {
		//success text here if needed
		} else { 
			echo $InsuranceUpdateSQL . "<br>" . $InsuranceUpdateConn->error; 
		}
		$InsuranceUpdateConn->close();
		
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The new Insurance product has been created successfully! The page will be refreshed in 2 seconds.</strong></div>";
		echo "<meta http-equiv='refresh' content='3'>";
	} else { 
		echo $InsuranceSQL . "<br>" . $InsuranceConn->error; 
	}
	$InsuranceConn->close();
}


//This is to add a new Transfer Product in the customer account
if(isset($_POST['Transfersubmit'])){
    $Transfer_status = 1;
    $Transfer_supplier = mysqli_real_escape_string($db, $_POST["Transfer_supplier"]);
    $Transfer_thecategory = 9;
    $Transfer_productname = mysqli_real_escape_string($db, $_POST["Transfer_productname"]);
    $Transfer_description = mysqli_real_escape_string($db, $_POST["Transfer_description"]);
    $Transfer_cost = mysqli_real_escape_string($db, $_POST["Transfer_cost"]);
    $Transfer_price = mysqli_real_escape_string($db, $_POST["Transfer_price"]);
    $Transfer_GroupID = mysqli_real_escape_string($db, $_POST["Transfer_GroupID"]);
    
    $TransferConn = new mysqli($servername, $username, $password, $dbname);
	if ($TransferConn->connect_error) {die("Connection failed: " . $TransferConn->connect_error);} 
	$TransferSQL = "INSERT INTO products (name, description, supplierid, categoryid, subproduct, cost, price, paymentterm, status)
	VALUES ('$Transfer_productname','$Transfer_description','$Transfer_supplier','$Transfer_thecategory','0','$Transfer_cost','$Transfer_price','0','$Transfer_status')";
	if ($TransferConn->query($TransferSQL) === TRUE) {
		$NewTransferProductID = mysqli_insert_id($TransferConn);
		$TransferUpdateConn = new mysqli($servername, $username, $password, $dbname);
		if ($TransferUpdateConn->connect_error) {die("Connection failed: " . $TransferUpdateConn->connect_error);} 
		
		$TransferUpdateSQL = "UPDATE customer_account SET Transfer_Product_ID='$NewTransferProductID',Transfer=1, hisownticket=1, updatetime=NOW() WHERE contactid=$leadid AND tourid=$Transfer_GroupID;";		
		$TransferUpdateSQL .= "DELETE FROM customer_invoice_line WHERE Line_Type='9' AND Customer_Invoice_Num=(SELECT ci.Customer_Invoice_Num FROM customer_invoice ci WHERE ci.Group_ID=$Transfer_GroupID AND ci.Customer_Account_Customer_ID=$leadid);";		
		$TransferUpdateSQL .= "DELETE FROM sales_order_line WHERE Line_Type='Transfer' AND Sales_Order_Num=(SELECT so.Sales_Order_Num FROM sales_order so WHERE so.Group_ID=$Transfer_GroupID AND so.Customer_Account_Customer_ID=$leadid);";		
		$TransferUpdateSQL .= "INSERT INTO sales_order_line (Sales_Order_Num, Line_Type, Product_Product_ID, Product_Name, Sales_Amount, Supplier_Supplier_ID) SELECT so.Sales_Order_Num,'Transfer', '$NewTransferProductID','$Transfer_productname','$Transfer_price','$Transfer_supplier' FROM sales_order so WHERE so.Group_ID=$Transfer_GroupID AND so.Customer_Account_Customer_ID=$leadid;";		
		$TransferUpdateSQL .= "INSERT INTO customer_invoice_line (Customer_Invoice_Num, Customer_Invoice_Line, Line_Type, Product_Product_ID, Product_Name, Qty, Invoice_Line_Total) SELECT ci.Customer_Invoice_Num,'9', '9','$NewTransferProductID','$Transfer_productname','1','$Transfer_price' FROM customer_invoice ci WHERE ci.Group_ID=$Transfer_GroupID AND ci.Customer_Account_Customer_ID=$leadid;";		
		$TransferUpdateSQL .= "UPDATE customer_invoice 
									INNER JOIN (
										SELECT SUM(cil.Qty * cil.Invoice_Line_Total) as Total, Customer_Invoice_Num FROM customer_invoice_line cil
										INNER JOIN products p ON p.id = cil.Product_Product_ID 
										GROUP BY Customer_Invoice_Num 
									) AS A ON A.Customer_Invoice_Num = customer_invoice.Customer_Invoice_Num 
								SET customer_invoice.Invoice_Amount=A.Total
								WHERE customer_invoice.Group_ID='$Transfer_GroupID' AND customer_invoice.Customer_Account_Customer_ID='$leadid';";		
		
		if ($TransferUpdateConn->multi_query($TransferUpdateSQL) === TRUE) {
		//success text here if needed
		} else { 
			echo $TransferUpdateSQL . "<br>" . $TransferUpdateConn->error; 
		}
		$TransferUpdateConn->close();
		
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The new Transfer product has been created successfully! The page will be refreshed in 2 seconds.</strong></div>";
		echo "<meta http-equiv='refresh' content='3'>";
	} else { 
		echo $TransferSQL . "<br>" . $TransferConn->error; 
	}
	$TransferConn->close();
}
?>
