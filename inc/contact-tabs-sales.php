<?php

include_once __DIR__ . '/../models/customer_invoice.php';
include_once __DIR__ . '/../models/customer_payment.php';

$Customer_ID = $leadid;
$FIRST_INVOICE = null ;
$FIRST_INVOICE_CANCELED = null ;

?>
<div class="row">
    <div class="col-sm-12" style="margin-bottom:20px;">
        <h5 style="margin-top: 10px;">Sale Orders Details</h5>
    </div>
    <div class="col-sm-12">
        <table class="table table-hover table-striped table-smallz" width="100%">
            <thead>
                <tr>
                    <th>Invoices</th>
                    <th class="text-center"><i class="feather icon-layers"></i></th>
                </tr>
            </thead>
            <tbody>
                <?php
                $CustomerAccountSQL = "SELECT
                                        ci.Status AS Invoice_Status,
                                        ci.Customer_Invoice_Num,
                                        so.Sales_Order_Num,
                                        ci.Group_ID AS tourid,
                                        if(ci.Group_ID = 999,'An invoice without a group',gro.tourname) AS tourname,
                                        ca.groupinvoice,
                                        ca.roomtype
                                    FROM customer_invoice ci
                                    JOIN sales_order so 
                                    ON ci.Sales_Order_Reference=so.Sales_Order_Num
                                    LEFT JOIN groups gro
                                    ON gro.tourid=ci.Group_ID
                                    LEFT JOIN customer_account ca 
                                    ON ca.contactid=ci.Customer_Account_Customer_ID AND ci.Group_ID=ca.tourid
                                    WHERE ci.Customer_Account_Customer_ID=$leadid;";
						$CustomerAccountResult = $db->query($CustomerAccountSQL);
						if ($CustomerAccountResult->num_rows > 0) {
						$TourCounter = 1;
						$RowCounter = 0;
						$Total_Sales_Amount = 0;
                        
                        $whereCondition = " AND ci.Status IN(0,1,2,4) AND ca.contactid=$leadid " ;
                        $newObj = new CustomerPayment() ;
                        $groupListing = $newObj->groupListing($whereCondition, "", "ORDER BY ca.tourid ASC") ;
                        $hasOneInvoice = false ;
                        
						//foreach($groupListing as $CustomerAccountRow ) { //= $CustomerAccountResult->fetch_assoc()) {
						while( $CustomerAccountRow = $CustomerAccountResult->fetch_assoc() ) { //= $CustomerAccountResult->fetch_assoc()) {
							$SalesOrderID = $CustomerAccountRow["Sales_Order_Num"];
							$CheckInvoiceID = $CustomerAccountRow["Customer_Invoice_Num"];
							$EachGroupTotal = 0;
                            
                            $rowcolor = '' ;
                            if($RowCounter == 1) {
                                $rowcolor = 'background-color:white;' ;
                            }
                            
                            $InvoiceTotalSQL = "SELECT SUM(Qty * Invoice_Line_Total) AS Invoice_Total FROM customer_invoice_line 
                                WHERE Customer_Invoice_Num=$CheckInvoiceID ";
                            $InvoiceTotalResult = $db->query($InvoiceTotalSQL);
                            $InvoiceTotalCount = $InvoiceTotalResult->num_rows;
                            if($InvoiceTotalCount > 0) {
                                $InvoiceTotalData = $InvoiceTotalResult->fetch_row();
                                $InvoiceTotalAmount = $InvoiceTotalData[0];
                            } else {
                                $InvoiceTotalAmount = 0;
                            }
                            
                            $textcolor = '' ;
                            $thisTotal = $InvoiceTotalAmount ; 
                            if( $CustomerAccountRow['Invoice_Status'] == CustomerInvoice::$CANCELLED ) {
                                $textcolor = 'color: red;';
                                $thisTotal = 0 ;
                            }
                            else {
                                $hasOneInvoice = true ;
                            }
                            
						?>
                <tr style='<?php echo $rowcolor . $textcolor;?>'>
                    <td><a style="<?php echo $textcolor;?>" href="groups-edit.php?id=<?php echo $CustomerAccountRow["tourid"]; ?>&action=edit"><?php echo "<b>#".$TourCounter." -</b> ".$CustomerAccountRow["tourname"]; ?></a><?php if($CustomerAccountRow["groupinvoice"] == 1) { ?> <i class="fas fa-users text-danger" title="This customer is in a Joint Invoice"></i><?php } ?></td>
                    <?php
                                if( $CustomerAccountRow['Invoice_Status'] == CustomerInvoice::$CANCELLED ) {
                                    
                                    if( ! $FIRST_INVOICE_CANCELED ) {
                                        $FIRST_INVOICE_CANCELED = $CustomerAccountRow["Customer_Invoice_Num"] ;
                                    }
                                    
                                    if( $CustomerAccountRow['Status'] == CustomerInvoice::$CANCELLED && floatval($CustomerAccountRow['Refund_Amount']) > 0 && $CustomerAccountRow['Refund_Status'] == 1 ) {
                                    ?>
                    <td><a class="pt-0 pb-0 btn waves-effect waves-light btn-inverse btn-outline-inverse btn-block" title="Issue Refund" href="javascript:void(0);" onclick="window.location.href='refund_receipt_add.php?invid=<?php echo $CustomerAccountRow["Customer_Invoice_Num"]; ?>';">
                            Issue Refund
                        </a>
                    </td>
                    <?php
                                    }
                                    else {
                                        ?><td><a href="<?php echo "contacts-edit.php?invid=$CheckInvoiceID&GroupCount=$TourCounter&id=$leadid&action=edit&usertype=sales#salestab";?>" class="pt-0 pb-0 btn text-danger waves-effect waves-light btn-inverse btn-outline-inverse btn-block">Canceled</a></td>
                                        <?php
                                    }
                                }
                                else {
                                    if($CustomerAccountRow["groupinvoice"] == 1) {
											$GroupIDforEach = $CustomerAccountRow["tourid"];
                                            $GetPrimaryInvoiceIDsql = "SELECT ci.Customer_Invoice_Num FROM customer_groups cg
                                                                    JOIN customer_invoice ci
                                                                    ON ci.Customer_Account_Customer_ID=cg.Primary_Customer_ID
                                                                    AND ci.Group_ID=cg.Group_ID
                                                                    WHERE ('$leadid') IN (cg.Primary_Customer_ID,cg.Additional_Traveler_ID_1,cg.Additional_Traveler_ID_2,cg.Additional_Traveler_ID_3) AND cg.Type='Group Invoice' AND cg.Group_ID=$GroupIDforEach";
                                            $GetPrimaryInvoiceResult = $db->query($GetPrimaryInvoiceIDsql);
                                            $GetPrimaryInvoiceData = $GetPrimaryInvoiceResult->fetch_assoc();
                                            $InvoicePrimaryID = $GetPrimaryInvoiceData['Customer_Invoice_Num'];
											
											$GetInvoiceIDthisROW = $InvoicePrimaryID;
                                            
                                            if( ! $FIRST_INVOICE ) {
                                                $FIRST_INVOICE = $InvoicePrimaryID ;
                                            }

                                    ?>
                    <td><a href="<?php echo "contacts-edit.php?invid=$InvoicePrimaryID&GroupCount=$TourCounter&id=$leadid&action=edit&usertype=sales#salestab";?>" class="pt-0 pb-0 btn waves-effect waves-light btn-inverse btn-outline-inverse btn-block">Invoice</a></td>
                    <?php } else { $GetInvoiceIDthisROW = $CheckInvoiceID; if( ! $FIRST_INVOICE ) { $FIRST_INVOICE = $CheckInvoiceID ; } ?>
                    <td><a href="<?php echo "contacts-edit.php?invid=$CheckInvoiceID&GroupCount=$TourCounter&id=$leadid&action=edit&usertype=sales#salestab";?>" class="pt-0 pb-0 btn waves-effect waves-light btn-inverse btn-outline-inverse btn-block">Invoice</a></td>
                    <?php }
                                }
                                ?>
                </tr>
                <?php 
						$con4 = new mysqli($servername, $username, $password, $dbname);
						if ($con4->connect_error) {die("Connection failed: " . $con4->connect_error);} 
						$sql2 = "UPDATE sales_order SET Total_Sale_Price='$Total_Sales_Amount',Mod_Date=NOW() WHERE Sales_Order_Num=$SalesOrderID;";
						if ($con4->multi_query($sql2) === TRUE) { /*Success*/ }
						$con4->close();

						?>
                <?php 	$TourCounter++;
								if($RowCounter == 0) {$RowCounter =1;} else {$RowCounter=0;}
						}} ?>
                <tr style="border-top: double 4px #ccc;background: #f9f9f9;">
                    <td style="font-weight:bold;" class="text-center" colspan="2">
                        <?php echo "This customer is added has ".($TourCounter-1)." invoice";
								if(($TourCounter-1)>1){echo "s.";} else { echo " only.";}; ?>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<?php if( ! $hasOneInvoice ) { ?>
<script type="text/javascript">
    $('.payment-receive-button').hide();

</script>
<?php } ?>


<?php
if( ! isset($__GET['invid']) ) {
    if( $FIRST_INVOICE ) {
        $__GET['invid'] = $FIRST_INVOICE ;
    }    
    else if( $FIRST_INVOICE_CANCELED ) {
        $__GET['invid'] = $FIRST_INVOICE_CANCELED ;
    }    
}
if( isset($__GET['invid']) ) {
    include 'sales-invoice-fragment.php' ;
}
