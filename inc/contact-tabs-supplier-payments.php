<div class="row">
	<div class="form-group col-sm-12 m-0">
		<h4 class="sub-title">All The Supplier's Payments List</h4>
	</div>
	<div class="form-group col-sm-12 m-0">
<table id="SupplierPaymentsTable" class="table table-hover table-striped table-smallz dataTable" style="width:100%">
        <thead>
            <tr>
                <th class="d-none">Payment Date Raw</th>
                <th>Payment Date</th>
                <th>Group #</th>
                <th>Supp. Bill #</th>
                <th>Amount</th>
                <th>Payment Method</th>
                <th>Comments</th>
            </tr>
        </thead>
        <tbody>
			<?php 
			$TotalPaymentsForSupplier = 0;
			$GetAllSupPaymentsSQL = 	"SELECT gro.tourid, gro.tourname, sp.Supplier_Payment_ID, sp.Supplier_Bill_Num, sp.Supplier_Payment_Date, sp.Supplier_ID, sp.Supplier_Payment_Amount, sp.Supplier_Payment_Method, sp.Supplier_Payment_Comments
											FROM suppliers_payments sp
											JOIN purchase_order po
											ON po.Purchase_Order_Num=sp.Supplier_Payment_Purchase_Order_ID
											JOIN groups gro
											ON gro.tourid=po.Group_ID
											WHERE sp.Status=1 AND sp.Supplier_ID=$leadid
											ORDER BY Supplier_Payment_Date DESC;";
			$GetAllSupPaymentsResult = $db->query($GetAllSupPaymentsSQL);
			if ($GetAllSupPaymentsResult->num_rows > 0) { 
			while($AllPaymentsRow = $GetAllSupPaymentsResult->fetch_assoc()) { ?>
            <tr>
                <td class="d-none"><?php echo $AllPaymentsRow['Supplier_Payment_Date']; ?></td>
                <td><a href="suppliers-bill-payments-add.php?payment=<?php echo $AllPaymentsRow['Supplier_Payment_ID']; ?>"><?php echo date('m/d/Y', strtotime($AllPaymentsRow['Supplier_Payment_Date'])); ?></a></td>
                <td><a href="groups-edit.php?id=<?php echo $AllPaymentsRow['tourid']; ?>&action=edit"><?php echo $AllPaymentsRow['tourid']; ?></a></td>
                <td><a href="bills-edit.php?id=<?php echo $AllPaymentsRow['Supplier_Bill_Num']; ?>&action=edit"><?php echo $AllPaymentsRow['Supplier_Bill_Num']; ?></a></td>
                <td><a href="suppliers-bill-payments-add.php?payment=<?php echo $AllPaymentsRow['Supplier_Payment_ID']; ?>"><?php echo $AllPaymentsRow['Supplier_Payment_Amount']; ?></a></td>
                <td><?php echo $AllPaymentsRow['Supplier_Payment_Method']; ?></td>
                <td><?php echo $AllPaymentsRow['Supplier_Payment_Comments']; ?></td>
            </tr>
			<?php	$TotalPaymentsForSupplier = $TotalPaymentsForSupplier+$AllPaymentsRow['Supplier_Payment_Amount'];
			}} else { ?>
            <tr>
                <td colspan="6" class="text-center">There are no payments sent to this supplier yet.</td>
            </tr>
			<?php } ?>
        </tbody>
		<tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th style="text-align:right;">Total:</th>
                <th style="text-align:left;"><?php echo GUtils::formatMoney($TotalPaymentsForSupplier); ?></th>
            </tr>
		</tfoot>
    </table>
	</div>
</div>
<script>
$( document ).ready(function() {
	$('#SupplierPaymentsTable').DataTable({
		 "order": [[ 1, "desc" ]],
		 pageLength: 20,
		 lengthMenu: [20, 50, 100, 200, 500],
        });   
});
</script>
