<?php

require_once('../session.php');
require_once '../inc/helpers.php';
ini_set('display_errors', false) ;

$QuoteID = isset($_GET['id']) ? $_GET['id'] : 0;

if (!isset($_GET['id'])) {header("location: ../quotes.php");die('Please go back to the main page.');}

//To collect the data of the above customer id, and show it in the fields below
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {die("Connection failed: " . $conn->connect_error);} 
$sql = "SELECT quo.*, con.fname,con.mname,con.lname FROM quotes quo JOIN contacts con ON quo.TourleaderID = con.id WHERE Quote_ID=$QuoteID";
$result = $conn->query($sql);
$data = $result->fetch_assoc();
$conn->close();


// Include the main TCPDF library (search for installation path).
require_once('../files/pdf/tcpdf.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Element Media');
$pdf->SetTitle('GST Proposal');
$pdf->SetSubject('GST Proposal');
$pdf->SetKeywords('GST, CRM, Element, Media, Element.ps');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set margins
$pdf->SetMargins(15, 25, 15);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/../lang/eng.php')) {
	require_once(dirname(__FILE__).'/../lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('Helvetica', '', 11);

// add a page
$pdf->AddPage('P','LETTER');

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

//$pdf->setTextShadow(array('enabled' => true, 'depth_w' => 0.2, 'depth_h' => 0.2, 'color' => array(196, 196, 196), 'opacity' => 1, 'blend_mode' => 'Normal'));

// Set some content to print

$Group_Name = $data["Group_Name"];
$TourLeaderName = $data["fname"]." ".$data["mname"]." ".$data["lname"];
$Church_Name = $data["Church_Name"];
$Itinerary_Title = $data["Itinerary_Title"];
$Suggested_Dates = $data["Start_Date"]." to ".$data["End_Date"];
$Departure_City = $data["Departure_City"];
$Group_Price = $data["Group_Price"];
$Single_Supplement = $data["Single_Supplement"];

//Get the Days and Nights for this proposal
	$date1 = date_create($data["Start_Date"]);
	$date2 = date_create($data["End_Date"]);
	$diff = date_diff($date1,$date2);
	$DurationDays = $diff->format("%a");
$Duration = ($DurationDays+1)." Days / ".$DurationDays." Nights";


$html = <<<EOD
<span style="text-align:center;"><img src="../files/assets/images/GST-header.JPG" alt="" style="width:500px;" /></span>
<br />
<table cellspacing="5" cellpadding="10" width="100%">
	<tr>
		<td bgcolor="#f2f2f2" width="30%"><b>Group name:</b></td>
		<td bgcolor="#f9f9f9" width="70%">$Group_Name</td>
	</tr>
	<tr>
		<td bgcolor="#f2f2f2" width="30%"><b>Tour Leader:</b></td>
		<td bgcolor="#f9f9f9" width="70%">$TourLeaderName</td>
	</tr>
	<tr>
		<td bgcolor="#f2f2f2" width="30%"><b>Church Name:</b></td>
		<td bgcolor="#f9f9f9" width="70%">$Church_Name</td>
	</tr>
	<tr>
		<td bgcolor="#f2f2f2" width="30%"><b>Itinerary:</b></td>
		<td bgcolor="#f9f9f9" width="70%">$Itinerary_Title</td>
	</tr>
	<tr>
		<td bgcolor="#f2f2f2" width="30%"><b>Suggested Dates:</b></td>
		<td bgcolor="#f9f9f9" width="70%">$Suggested_Dates</td>
	</tr>
	<tr>
		<td bgcolor="#f2f2f2" width="30%"><b>Departure City:</b></td>
		<td bgcolor="#f9f9f9" width="70%">$Departure_City</td>
	</tr>
	<tr>
		<td bgcolor="#f2f2f2" width="30%"><b>Duration:</b></td>
		<td bgcolor="#f9f9f9" width="70%">$Duration</td>
	</tr>
</table>
<br />
<br />
<table cellpadding="10" width="100%">
	<tr>
		<td bgcolor="#5f4637"><b style="font-size:20px; color:white;">DAY-BY-DAY ITINERARY</b></td>
	</tr>
	<tr>
		<td bgcolor="#f9f9f9">
			<p><b>Day 1: Monday - Departure</b><br />
Depart US for your overnight flight to the Holy Land. Meals will be served on board the flight.</p>
		</td>
	</tr>
	<tr>
		<td>
			<p><b>Day 2: Tuesday - Arrival, Tel Aviv Airport</b><br />
Meet your guide at the Airport, Drive to your Hotel in Tiberias for dinner and Overnight. (D)</p>
		</td>
	</tr>
	<tr>
		<td bgcolor="#f9f9f9">
			<p><b>Day 3: Wednesday - Caesarea, Carmel, Meggido, Magdala</b><br />
Start our visit to Caesarea Marittima, Mount Carmel Meggido, the place Biblical Armageddon, Magdala by the Sea of Galilee.  Dinner and Overnight in Tiberias. (B,L,D)</p>
		</td>
	</tr>
	<tr>
		<td>
			<p><b>Day 4: Thursday - Caesarea Philippi, Sea of Galilee</b><br />
We will start our day with a boat ride on the Sea of Galilee; visit Caesarea Philippi; See Mount Hermon, the highest mountain in Israel, from a distance; Mount of Beatitudes; Church of Multiplications; Capernaum.  Then we will ride to Yardenit, a possible sight of Jesus’ baptism.  Dinner and overnight in Tiberias. (B,L,D)</p>
		</td>
	</tr>
	<tr>
		<td bgcolor="#f9f9f9">
			<p><b>Day 5: Friday - Cana, Nazareth, Jerusalem, Jordan River</b><br />
Check out of our hotel in Galilee and head south towards Jerusalem. Stop at Cana of Galilee, then Nazareth and Nazareth village. Drive through the Jordan valley, visit the baptismal site on the Jordan River, visit Jericho, and drive up to the Holy city of Jerusalem and panoramic view over Jerusalem from Mount Scopus, and a visit to the Wailing Wall Dinner and overnight in Bethlehem. (B,L,D)</p>
		</td>
	</tr>
	<tr>
		<td>
			<p><b>Day 6: Saturday - Mt of Olives, Bethlehem, Shepherds Field</b><br />
Visit the Mount of Olives and the Garden of Gethsemane. Visit Herodian, Herod’s summer palace and his burial place. Visit Bethlehem and the Church of the Nativity, Shepherds field, shop in Bethlehem. Dinner and overnight in Bethlehem. (B,L,D)</p>
		</td>
	</tr>
	<tr>
		<td bgcolor="#f9f9f9">
			<p><b>Day 7: Sunday - Masada, Qumran and Dead Sea</b><br />
Full Day Tour in the Dead Sea area, we will visit Masada, Qumran  and Swim in the Dead Sea, Elisha Spring and view Mount Temptation Monastery. Drive the Jordan Valley dinner and Overnight in Bethlehem. (B,L,D)</p>
		</td>
	</tr>
	<tr>
		<td>
			<p><b>Day 8: Monday - Temple Mount, Davidson Centre, Mt Zion, Israel Museum</b><br />
Visit Temple Mount where the Temple of Jerusalem stood. Then we will visit Davidson Centre.  Mount Zion and the upper room. On to David’s Tomb. In the afternoon Israel Museum and Yad Vashem. Sunday night Church service at King of Kings, Dinner and overnight in your hotel in Bethlehem. (B,L,D)</p>
		</td>
	</tr>
	<tr>
		<td bgcolor="#f9f9f9">
			<p><b>Day 9: Tuesday - Temple Mount, Garden Tomb</b><br />
Start the day with private prayer at Gethsemane Garden. On to Caiaphas Palace, Ecce Homo, biblical Gabatha. Follow Stations of the Cross to the Church of the Holy Sepulcher, walk through different quarters of Jerusalem and then the Garden Tomb. Dinner and overnight in Bethlehem. (B,L,D)</p>
		</td>
	</tr>
	<tr>
		<td>
			<p><b>Day 10: Wednesday - City of David, Hezekiah’s tunnel</b><br />
This morning we will visit City of David, Hezekiah’s tunnel, pool of siloam. Free afternoon for personal reflection. Dinner and overnight in Bethlehem. (B,L,D)</p>
		</td>
	</tr>
	<tr>
		<td bgcolor="#f9f9f9">
			<p><b>Day 11: Thursday Departure - Arrival</b><br />
We will arrive back in the USA the same day. (B)</p>
		</td>
	</tr>
	<tr>
		<td>
			<p style="font-size:12px;"><i><b>Note:</b> While we do our very best at making sure all items listed in your itinerary are included, there are times when conditions beyond our control affect your program/itinerary, including but not limited to air carrier changes, delays, weather, political climate, time constraints, or changes by your tour leader and tour guide.</i></p>
		</td>
	</tr>
</table>
<br pagebreak="true"/>
<table cellpadding="10" width="100%">
	<tr>
		<td colspan="3" bgcolor="#5f4637"><b style="font-size:20px; color:white;">Package Price:</b></td>
	</tr>
	<tr bgcolor="#f2f2f2">
		<td>
			<b>Passengers</b>
		</td>
		<td>
			<b>Option 1</b>
		</td>
		<td>
			<b>Option 2</b>
		</td>
	</tr>
	<tr>
		<td>1 Free for every 10 paying</td>
		<td>$$Group_Price</td>
		<td>-</td>
	</tr>
	<tr>
		<td>Single Supplement</td>
		<td>$$Single_Supplement</td>
		<td>-</td>
	</tr>
	<tr bgcolor="#f9f9f9">
		<td colspan="3"><i><b>Note:</b> The above-mentioned rates will not apply during, Easter and Christmas and Jewish holidays.</i></td>
	</tr>
</table>
<br />
<br />
<br />
<table cellpadding="10" width="100%">
	<tr>
		<td bgcolor="#5f4637"><b style="font-size:20px; color:white;">Hotels:</b></td>
	</tr>
	<tr>
		<td>
			<i><b>Tiberias:</b> Ron Beach or Caesar or similar 4+* (3 Nights)</i>
			<br /><i><b>Option 1:</b> Jerusalem: Olive tree or Dan Hotel or Grand Court or similar 4* (6 Nights)</i>
			<br /><i><b>Option 2:</b> Bethlehem: Saint Gabriel or similar 4* (6 Nights)</i>
		
		</td>
	</tr>
</table>
<br />
<br />
<br />
<table cellpadding="10" width="100%">
	<tr>
		<td bgcolor="#5f4637"><b style="font-size:20px; color:white;">Rates Includes:</b></td>
	</tr>
	<tr>
		<td>
			<ul>
				<li>Round trip airline tickets</li>
				<li>All taxes, fees and surcharges </li>
				<li>Hotels on Half board Basis: 9 nights (open buffet breakfast, open buffet dinner)</li>
				<li>Air-conditioned bus in Israel </li>
				<li>Farewell dinner </li>
				<li>English speaking guide </li>
				<li>Entrance fees</li>
				<li>Meeting and assistance at Arrival – Ben Gurion Airport </li>
				<li>Porterage Included in all hotels</li>
				<li>Whispers devises during the whole trip </li>
				<li>Communion service </li>
				<li>Porterage at airport</li>
				<li>Lunches</li>
			</ul>
		</td>
	</tr>
</table>
<br />
<br />
<table cellpadding="10" width="100%">
	<tr>
		<td bgcolor="#5f4637"><b style="font-size:20px; color:white;">Rates does not Include:</b></td>
	</tr>
	<tr>
		<td>
			<ul>
				<li>Gratuities for guide / driver </li>
				<li>Personal expenses</li>
				<li>Travel Insurance</li>
				<li>Anything not mentioned above </li>
			</ul>
		</td>
	</tr>
</table>
EOD;
		
// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');


// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

$PDF_OUTPUT = isset($PDF_OUTPUT ) ? $PDF_OUTPUT : 'I' ;
//Close and output PDF document
return $pdf->Output('Quote-Proposal.pdf', $PDF_OUTPUT );

//============================================================+
// END OF FILE
//============================================================+
