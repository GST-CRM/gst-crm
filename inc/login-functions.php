<?php 

include "../config.php";

include_once __DIR__ . '/../models/users.php';
include_once __DIR__ . '/../models/user_login_virtual.php';


//For login in the first interface login in the CRM
if(isset($_POST['login_username'])){
	session_start();
	$username = mysqli_real_escape_string($db,$_POST['login_username']);
	$password = mysqli_real_escape_string($db,$_POST['login_password']);
    $row = (new UserLogin())->checkLogin($username, $password) ;
    
    if(is_array($row)) {
        $_SESSION['login_user'] = $username;
        $_SESSION['Role_ID'] = $row['Role_ID'];
        $_SESSION['Active_Role'] = GUtils::firstMask($row['Role_ID']) ;

        Users::switchToRole($_SESSION['Active_Role']) ;

        echo "<div class='alert alert-success background-success'>Welcome ".$_SESSION['login_user'].", you are redirected now!</div>";
        //echo '<meta http-equiv="refresh" content="2;url=dashboard.php" ></meta>';
        echo '<script>window.setTimeout(function() { window.location.href = "dashboard.php"; }, 2000);</script>';
    } else {
        echo "<div class='alert alert-danger background-danger'>Your login information is invalid. Please try again.</div>";
    }
}

?>