<?php
//include connection file 
include __DIR__ . "/../config.php";

include_once __DIR__ . '/../models/acl_permission.php';

// { Acl Condition
$GroupID       = AclPermission::userGroup();
$contactId  = AclPermission::userId() ;
$ACL_CONDITION = " ";
if (AclPermission::roleFlags() & AclRole::$FLAG_ONLY_GROUP) {
    $ACL_CONDITION = " AND ca.tourid IN($GroupID) ";
}
else if( AclPermission::roleFlags() & AclRole::$FLAG_ONLY_MINE ) {
    $ACL_CONDITION = " AND ca.contactid = $contactId " ;
}
// }

$contactEditAllowed = (new AclPermission())->isActionAllowed('Index', 'contact-edit.php') ;
$paymentsEditAllowed = (new AclPermission())->isActionAllowed('Index', 'payments-add.php') ;


//define index of column
$columns = array( 
    0 => 'Customer_Payment_ID',
    1 => 'Customer_Payment_ID', 
    2 => 'fname', 
    3 => 'tourdesc', 
    4 => 'Customer_Payment_Date', 
    5 => 'Customer_Payment_Amount'
);

$params = $__REQUEST;
$where  = '';
// check search value exist
if (!empty($params['search']['value'])) {
    $where .= " AND ";
    $where .= " ( cp.Customer_Payment_ID LIKE '" . $params['search']['value'] . "%' ";
    $where .= " OR c.fname LIKE '" . $params['search']['value'] . "%' ";
    $where .= " OR c.mname LIKE '" . $params['search']['value'] . "%' ";
    $where .= " OR c.lname LIKE '" . $params['search']['value'] . "%' ";
    $where .= " OR cp.Customer_Payment_Amount LIKE '" . $params['search']['value'] . "%')";
}

if( $params['order'][0]['column'] == 0 )  {
    $orderBy = " ORDER BY Customer_Payment_Date DESC " ;
}
else {
    $orderBy = " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."" ;
}

$sqlCount = "SELECT COUNT(*) as total FROM customer_payments AS cp 
        INNER JOIN customer_invoice ci ON ci.Customer_Invoice_Num = cp.Customer_Invoice_Num AND cp.Status=1 -- AND ci.Status =1
        LEFT JOIN customer_account AS ca ON ca.contactid = cp.Customer_Account_Customer_ID -- AND ca.status=1
        LEFT JOIN contacts AS c ON c.id = ca.contactid 
        WHERE cp.Status=1 
        $ACL_CONDITION    
        $where
        ";

$sql = "SELECT c.fname, c.lname, c.mname, ca.contactid, g.tourdesc, cp.* FROM customer_payments AS cp 
        INNER JOIN customer_invoice ci ON ci.Customer_Invoice_Num = cp.Customer_Invoice_Num AND cp.Status=1 -- AND ci.Status =1
        LEFT JOIN customer_account AS ca ON ca.contactid = cp.Customer_Account_Customer_ID -- AND ca.status=1
        LEFT JOIN contacts AS c ON c.id = ca.contactid 
		LEFT JOIN groups g ON g.tourid=cp.Group_ID
        WHERE cp.Status=1 
        $ACL_CONDITION    
        $where
        GROUP BY cp.Customer_Payment_ID $orderBy LIMIT " . $params['start'] . " ," . $params['length'] . "";


$data         = GDb::fetchRowSet($sql);
$totalRecords = GDb::fetchScalar($sqlCount);


$output = [];
foreach ($data as $row) {
    $oneOutput  = [];
    $name       = $row['fname'] . ' ' . $row['mname'] . ' ' . $row['lname'];
    $nameAppend = ((strlen(trim($name)) > 0) ? $name : '');

    //name & email
    $name_email = '<b>' . $nameAppend . (isset($row['email']) ? ' (' . $row['email'] . ')' : '') . '</b>';

    $oneOutput[0] = "<input name='cbInvoice[]' value='" . $row["Customer_Invoice_Num"] . "' type='checkbox' class='gst-invoice-cb'/>";
    if( $paymentsEditAllowed ) {
        $oneOutput[1] = '<a style="color: #0000EE;" href="payments-add.php?payment=' . $row["Customer_Payment_ID"] . '">' . $row["Customer_Payment_ID"] . '</a>';
    }
    else {
        $oneOutput[1] = $row["Customer_Payment_ID"] ;
    }

    ob_start();
    if ($row["contactid"] && $contactEditAllowed ) {
        ?>
        <a style="color: #0000EE;" href='<?php echo 'contacts-edit.php?id=' . $row["contactid"] . '&action=edit&usertype=sales'; ?>'>
            <?php
        }
        //print text
        echo $nameAppend;

        if ($row["contactid"]) {
            ?>
        </a>
    <?php
    }
    $oneOutput[2] = ob_get_clean();
	
	ob_start(); ?>
	<a style="color: #0000EE;" href='<?php echo 'groups-edit.php?id=' . $row["Group_ID"] . '&action=edit&tab=participants'; ?>'><?php echo $row["tourdesc"]; ?></a>
    <?php
	$oneOutput[3] = ob_get_clean();
	
    $oneOutput[4] = GUtils::clientDate($row["Customer_Payment_Date"]);
    $oneOutput[5] = GUtils::formatMoney($row["Customer_Payment_Amount"]);

    ob_start();
    ?>
    <div style="width: 150px;" class="btn-group gst-btns ">
    <?php 
        if( $paymentsEditAllowed ) {
    if ($row['Status']) { ?>                                               
            <button onclick="window.location.href = 'payments-add.php?payment=<?php echo $row["Customer_Payment_ID"]; ?>';"
                    type="button" class="btn btn-info">Edit/View
            </button>
            <button type="button" class="btn btn-info dropdown-toggle"
                    data-toggle="dropdown">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu" role="menu" style="white-space: nowrap">
                <!--<li><a href="payments-add.php?id=<?php echo $row["Customer_Invoice_Num"]; ?>">Edit/View</a></li>-->
                <li><a target="_blank" href="sales-invoice-print.php?print=payment&invid=<?php echo $row["Customer_Invoice_Num"]; ?>">Print</a></li>
                <li><a href="javascript:void(0);" onclick = 'showConfirm("payments.php?action=send&invid=<?php echo $row["Customer_Invoice_Num"]; ?>", "Send", "Are you sure you want to sent this email to <?php echo $name_email; ?> ?", "Please Confirm", "modal-md")' >Send</a></li>
                <li><a href="javascript:void(0);" onclick = 'deleteConfirm("payments.php?action=delete&id=<?php echo $row["Customer_Payment_ID"]; ?>", "Delete")' >Delete</a></li>
                <li><a href="javascript:void(0);" onclick = 'deleteConfirm("payments.php?action=void&id=<?php echo $row["Customer_Payment_ID"]; ?>", "Void")' >Void</a></li>
            </ul>
        <?php } else {
            ?>
            <button onclick = 'deleteConfirm("payments.php?action=delete&id=<?php echo $row["Customer_Payment_ID"]; ?>")' type="button" class="btn btn-danger">
                Delete
            </button>
            <?php }
        }
        ?>
    </div>
    <?php
    $oneOutput[6] = ob_get_clean();
    $output[]     = $oneOutput;
}

$json_data = array(
    "draw"            => intval($params['draw']),
    "recordsTotal"    => intval($totalRecords),
    "recordsFiltered" => intval($totalRecords),
    "data"            => $output   // total data array
);

echo json_encode($json_data);  // send data as json format
