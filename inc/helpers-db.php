<?php

global $dbObj ;
$dbObj = $db ;
class GDb {
    
    public static function db() {
        global $dbObj ;
        return $dbObj ;
    }
    public static function escape($value) {
        return mysqli_real_escape_string(self::db() , $value);
    }
    public static function fetchRow($sql) {
        $result = self::db()->query($sql);

        if( $result ) {
            $row = $result->fetch_assoc() ;
            return $row ;
        }
        return null ;
    }
    public static function fetchRowSet($sql) {
        $result = self::db()->query($sql);
        if( $result ) {
            $set = [] ;
            while( $row = $result->fetch_assoc() ) {
                $set[] = $row ;
            }
            return $set ;
        }
        return null ;
    }
    public static function fetchScalar($sql) {
        $result = self::db()->query($sql);
        if( $result ) {
            $row = $result->fetch_row() ;
            if( is_array($row) ) {
                return $row[0] ;
            }
        }
        return null ;
    }
    public static function CountRows($sql) {
        $result = self::db()->query($sql);
        if( $result ) {
            $row = $result->num_rows;
            return $row;
        }
        return null ;
    }
    public static function execute($sql) {
        return self::db()->query($sql);
    }
    public static function execute_multi($sql) {
        self::db()->multi_query($sql);
        while (self::db()->next_result()) {}
    }
    
}