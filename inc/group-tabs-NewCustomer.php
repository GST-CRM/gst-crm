	<form id="contact20" name="contact20" method="POST" class="row" action="inc/group-functions.php">
        <div class="form-group col-sm-12">
            <h3>Add A Customer to the Group</h3>
				<input name="TourDueDate" value="<?php echo date('Y-m-d', strtotime($data["startdate"]. ' - 90 days')); ?>" type="text" hidden>
        </div>
        <div class="form-group col-sm-4">
            <h4 class="sub-title">A Current Customer?</h4>
			<div class="can-toggle">
				<input id="CurrentCustomerTourID" name="CurrentCustomerTourID" value="<?php echo $tourid; ?>" type="checkbox" checked>
				<input name="CurrentUserID" value="<?php echo $UserAdminID; ?>" type="text" hidden>
				<label for="CurrentCustomerTourID">
				<div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
				</label>
			</div>
        </div>
        <div id="CurrentCustomerList" class="col-sm-8">
			<h4 class="sub-title">Select A Current Customer Account</h4>
			<select data-current-customer-tourid="<?php echo $tourid; ?>" name="CurrentCustomerSelected" class="select2-cuttentcustomer col-sm-12" multiple="multiple">
			</select>
        </div>
        <div id="CurrentCustomerSubmit" class="col-sm-12">
			<br />
			<button type="submit" name="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Add</button>
            <button type="reset" class="btn waves-effect waves-light btn-inverse" data-dismiss="modal"><i class="fas fa-ban"></i>Cancel</button>
		</div>
	</form>
	<form id="contact21" name="contact21" method="POST" class="row" action="inc/group-functions.php">
		<div id="NewAddCustomer" class="row" style="display:none;">
				<div class="form-group form-default form-static-label col-sm-12">
					<h4 class="sub-title"><b>Basic Contact Information</b></h4>
				</div>
                <div class="form-group form-default form-static-label col-sm-4">
                    <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Title</label>
                    <input type="text" name="NewCustomerAdd" value="<?php echo $tourid; ?>" class="form-control" hidden>
                    <input type="text" name="CurrentUserID" value="<?php echo $UserAdminID; ?>" class="form-control" hidden>
					<select name="title" id="title" class="form-control form-control-default">
                        <option value="0" disabled selected> </option>
                        <option value="Mr.">Mr.</option>
                        <option value="Ms.">Ms.</option>
                        <option value="Mrs.">Mrs.</option>
                        <option value="Dr.">Dr.</option>
                        <option value="Rev.">Rev.</option>
                        <option value="Father">Father</option>
                        <option value="Pastor">Pastor</option>
                    </select>
                </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">First Name</label>
                        <input type="text" id="fname" name="FirstName" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">Middle Name</label>
                        <input type="text" id="mname" name="MiddleName" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">Last Name</label>
                        <input type="text" id="lname" name="LastName" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">Email</label>
                        <input type="text" name="email" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Mobile Phone #</label>
						<input id="mobile" type="tel" name="mobile" value="" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Home Phone #</label>
                        <input id="tel" type="tel" name="homephone" value="" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Business Phone #</label>
                        <input id="businessphone" type="tel" name="businessphone" value="" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Fax #</label>
						<input id="fax" type="tel" name="fax" value="" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">Company Name</label>
                        <input type="text" name="company" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">Job Title</label>
                        <input type="text" name="jobtitle" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">Website</label>
                        <input type="text" name="website" class="form-control">
                        <span class="form-bar"></span>
                    </div>
					<div class="form-group col-sm-12 m-0" style="margin-top:15px !important;">
						<h4 class="sub-title"><b>Home Address Information</b></h4>
					</div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">Address Line 1</label>
                        <input type="text" name="address1" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">Address Line 2</label>
                        <input type="text" name="address2" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">City</label>
                        <input type="text" name="city" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">State</label>
                        <select name="state" class="form-control form-control-default">
							<option value="0" disabled selected> </option>
							<?php $con2 = new mysqli($servername, $username, $password, $dbname);
							$result2 = mysqli_query($con2,"SELECT name, abbrev FROM states");
							while($row2 = mysqli_fetch_array($result2))
							{
								echo "<option value='".$row2['abbrev']."'>".$row2['name']."</option>";
							} ?>
                        </select>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">ZIP Code</label>
                        <input type="text" name="zipcode" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Country</label>
                        <select name="country" class="form-control form-control-default">
                            <option value="0" disabled selected> </option>
							<?php $con3 = new mysqli($servername, $username, $password, $dbname);
							$result3 = mysqli_query($con3,"SELECT name, abbrev FROM countries");
							while($row3 = mysqli_fetch_array($result3))
							{
								$selected = '';
								if($row3['abbrev'] == 'US') {$selected = 'selected';}
								echo "<option value='".$row3['abbrev']."' $selected>".$row3['name']."</option>";
							} ?>
                        </select>
                    </div>
					<div class="form-group col-sm-12 m-0" style="margin-top:15px !important;">
						<h4 class="sub-title"><b>Work Address Information</b></h4>
					</div>
					<div class="form-group col-sm-12 m-0 text-center">
						<div class="checkbox-fade fade-in-success">
							<label>
								<input id="WorkAddressButton" name="WorkAddressButton" type="checkbox" value="1">
								<span class="cr">
									<i class="cr-icon fas fa-check txt-success"></i>
								</span>
								<span>Enable?</span>
							</label>
						</div>
					</div>
					<div id="WorkAddressBlock" class="col-sm-12 row p-0 m-0" style="display:none;">
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">Address Line 1</label>
                        <input type="text" name="Work_address1" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">Address Line 2</label>
                        <input type="text" name="Work_address2" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">City</label>
                        <input type="text" name="Work_city" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">State</label>
                        <select name="Work_state" class="form-control form-control-default">
							<option value="0" disabled selected> </option>
							<?php $con2 = new mysqli($servername, $username, $password, $dbname);
							$result2 = mysqli_query($con2,"SELECT name, abbrev FROM states");
							while($row2 = mysqli_fetch_array($result2))
							{
								echo "<option value='".$row2['abbrev']."'>".$row2['name']."</option>";
							} ?>
                        </select>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label">ZIP Code</label>
                        <input type="text" name="Work_zipcode" class="form-control">
                        <span class="form-bar"></span>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Country</label>
                        <select name="Work_country" class="form-control form-control-default">
                            <option value="0" disabled selected> </option>
							<?php $con3 = new mysqli($servername, $username, $password, $dbname);
							$result3 = mysqli_query($con3,"SELECT name, abbrev FROM countries");
							while($row3 = mysqli_fetch_array($result3))
							{
								$selected = '';
								if($row3['abbrev'] == 'US') {$selected = 'selected';}
								echo "<option value='".$row3['abbrev']."' $selected>".$row3['name']."</option>";
							} ?>
                        </select>
                    </div>
                    </div>
					<div class="form-group col-sm-12 m-0" style="margin-top:15px !important;">
						<h4 class="sub-title"><b>Other Information</b></h4>
					</div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Language</label>
                        <select name="language" class="form-control form-control-default">
                            <option value="English" selected>English</option>
                            <option value="French">French</option>
                            <option value="German">German</option>
                            <option value="Arabic">Arabic</option>
                             <option value="Italian">Italian</option>
                            <option value="Spanish">Spanish</option>
                        </select>
                    </div>
                    <div id="affliationtype" class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Category</label>
                        <select name="affiliation" class="form-control form-control-default" onchange="admSelectCheck(this);">
                            <option value="0" disabled selected> </option>
							<?php $meta1 = new mysqli($servername, $username, $password, $dbname);
							$metadata1 = mysqli_query($meta1,"SELECT * FROM metadata WHERE catid='1' AND Status=1");
							while($keyword1 = mysqli_fetch_array($metadata1))
							{
								echo "<option value='".$keyword1['meta']."'>".$keyword1['meta']."</option>";
							} ?>
							<option id="admOption" value="0">Add New</option>
                        </select>
                    </div>
					<div id="affliationtypenew" class="form-group form-default form-static-label col-sm-4" style="display:none;">
						<label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Add New Category</label>
						<input type="text" name="newaffliationtype" value="" class="form-control">
					</div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Preferred Method</label>
                        <select name="preferredmethod" class="form-control form-control-default">
                            <option value="0" disabled selected> </option>
                            <option value="Email">Email</option>
                            <option value="Phone">Phone</option>
                            <option value="Mobile">Mobile</option>
                        </select>
                    </div>
		<div class="form-group form-default form-static-label col-sm-12">
			<h4 class="sub-title"><b>Customer Account Information</b></h4>
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Passport Number</label>
			<input type="text" name="passport" class="form-control">
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Passport Expiration Date</label>
			<input type="date" name="expirydate" class="form-control">
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Country of Citizenship</label>
			<select name="citizenship" class="form-control form-control-default">
				<?php $con3 = new mysqli($servername, $username, $password, $dbname);
				$result3 = mysqli_query($con3,"SELECT name, abbrev FROM countries");
				while($row3 = mysqli_fetch_array($result3))
				{
						if($row3['abbrev'] == "US") {
							$selectedd = "selected";
						} else {
							$selectedd = "";
						}
					echo "<option value='".$row3['abbrev']."' $selectedd>".$row3['name']."</option>";
				} ?>
			</select>
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Birthday</label>
			<input type="date" name="birthday" class="form-control">
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Name on the Badge</label>
			<input type="text" name="badgename" class="form-control">
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Payment Terms</label>
			<input type="text" name="paymentterm" class="form-control">
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Room Type</label>
			<select name="roomtype" class="form-control form-control-default" onchange="RoomTypeSelect(this);">
				<option value="0" disabled selected> </option>
				<option id="SingleOption" value="Single">Single</option>
				<option id="DoubleOption" value="Double">Double</option>
				<option id="TwinOption" value="Twin">Twin</option>
				<option id="TripleOption" value="Triple">Triple</option>
			</select>
		</div>
		<div id="roomtypedouble" class="form-group form-default form-static-label col-sm-4" style="display:<?php if($customerdata['roomtype'] == 'Double') {echo "block";} elseif ($customerdata['roomtype'] == 'Triple') {echo "block";}  else {echo "none";} ?>;">
			<label class="float-label">(Double) Rooming with</label>
			<select name="travelingwith" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
				<option value="999">**To be Determined**</option>
				<?php 
				$con6 = new mysqli($servername, $username, $password, $dbname);
				$result6 = mysqli_query($con6,"SELECT c.id, c.fname, c.lname FROM contacts c
JOIN customer_account ca 
ON c.id=ca.contactid
WHERE ca.tourid=$tourid");
				while($row6 = mysqli_fetch_array($result6))
				{
					echo "<option value='".$row6['id']."' $travelcontactselect>".$row6['fname']." ".$row6['lname']."</option>";
				} ?>
			</select>
		</div>
		<div id="roomtypetriple" class="form-group form-default form-static-label col-sm-4" style="display:<?php if($customerdata['roomtype'] == 'Triple') {echo "block";} else {echo "none";} ?>;">
			<label class="float-label">(Triple) Rooming with #2</label>
			<select name="travelingwith2" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
				<option value="999">**To be Determined**</option>
				<?php 
				$con6 = new mysqli($servername, $username, $password, $dbname);
				$result6 = mysqli_query($con6,"SELECT c.id, c.fname, c.lname FROM contacts c
JOIN customer_account ca 
ON c.id=ca.contactid
WHERE ca.tourid=$tourid");
				while($row6 = mysqli_fetch_array($result6))
				{
					echo "<option value='".$row6['id']."' $travelcontactselect>".$row6['fname']." ".$row6['lname']."</option>";
				} ?>
			</select>
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Known Traveler #</label>
			<input type="text" name="travelerid" class="form-control">
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Frequent Flyer</label>
			<input type="text" name="flyer" class="form-control">
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Martial Status</label>
			<select name="martialstatus" class="form-control form-control-default">
				<option value="NA">N/A</option>
				<option value="Married">Married</option>
				<option value="Single">Single</option>
			</select>
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Gender</label>
			<select name="gender" class="form-control form-control-default">
				<option value="0" disabled> </option>
				<option value="Male">Male</option>
				<option value="Female">Female</option>
			</select>
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Emergency Contact #1 Name</label>
			<input type="text" name="emergencyname" class="form-control">
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Emergency Contact #1 Relation</label>
			<input type="text" name="emergencyrelation" class="form-control">
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Emergency Contact #1 Phone</label>
			<input type="text" name="emergencyphone" class="form-control">
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Emergency Contact #2 Name</label>
			<input type="text" name="emergency2name" class="form-control">
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Emergency Contact #2 Relation</label>
			<input type="text" name="emergency2relation" class="form-control">
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Emergency Contact #2 Phone</label>
			<input type="text" name="emergency2phone" class="form-control">
		</div>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Missing Information?</label>
			<input type="text" name="missinginfo" class="form-control" value="<?php echo $customerdata['missinginfo'];?>">
		</div>
        <div class="col-sm-12">
			<br />
			<button type="submit" name="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Add</button>
            <button type="reset" class="btn waves-effect waves-light btn-inverse" data-dismiss="modal"><i class="fas fa-ban"></i>Cancel</button>
		</div>
		</div>
	</form>
</div>
