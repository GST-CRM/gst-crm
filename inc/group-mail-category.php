<?php

use PHPMailer\PHPMailer\PHPMailer;

$groupID       = !empty($_REQUEST['groupid']) ? $_REQUEST['groupid'] : "";
$groupName     = !empty($_REQUEST['groupname']) ? $_REQUEST['groupname'] : "";
$emailTemplate = !empty($_REQUEST['email_template']) ? $_REQUEST['email_template'] : "";
$subject       = !empty($_REQUEST['email_subject']) ? $_REQUEST['email_subject'] : "";
$message       = !empty($_REQUEST['email_message']) ? $_REQUEST['email_message'] : "";
$toEmails      = array();
$bcc           = array();

$CategoryID = $__REQUEST['groupid'];

//To collect the data of the above Product id, and show it in the fields below
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}


$sql     = "SELECT email FROM contacts WHERE affiliation=$CategoryID";
$records = GDb::fetchRowSet($sql);
foreach ($records as $rec) {
    $bcc[] = $rec['email'];
}

$userLoginEmail = [];

$toEmails = ['info@goodshepherdtravel.com'];


$toEmailString   = implode(',', $toEmails);
$template        = (new Email_Templates())->getById($emailTemplate);
$templateSubject = $template['template_subject'];
$templateBody    = str_replace('Â ', ' ', $template['template_body']);

//Insert to groups_email table
$insertsql      = "INSERT INTO groups_email(tourid,email_to,email_sent,template_code,email_subject,email_content) VALUES('$groupID','$toEmailString','1','$emailTemplate','$templateSubject','$templateBody')";
$saveGroupEmail = $db->query($insertsql);

if (!empty($toEmails)) {
    $config                = [];
    $config['max_size']    = 50000000;
    $config['upload_path'] = 'groupattach/';
    $uploadFile            = false;
    if (!empty($_FILES['groupAttachment']['name'])) {
        $uploadFile = GUtils::doUpload('groupAttachment', $config);
    }
    $attachmentName = (isset($config['file_name']) ? $config['file_name'] : '');
    $uploadPath     = (isset($config['upload_path']) ? $config['upload_path'] : '');
    $attachmentData = "";

    if ($attachmentName) {
        $attachmentData = __DIR__ . "/../uploads/groupattach/" . $attachmentName;
    }


    if ((!empty($_FILES['groupAttachment']['name']) && $uploadFile && file_exists($attachmentData)) ||
        (empty($_FILES['groupAttachment']['name']) && !$uploadFile)) {
        if ($emailTemplate) {
            //Send Email
            $params = array(
                'groupname' => $groupName,
            );
            GUtils::sendTemplateMailById($toEmails, $emailTemplate, $params, $attachmentName, $attachmentData, $userLoginEmail, [], $bcc);
        } else {
            GUtils::sendMail($toEmails, $subject, $message, $attachmentName, $attachmentData, $userLoginEmail, [], $bcc);
        }
        GUtils::setSuccess('Email sent to all participants in this group.');
    } else {
        GUtils::setWarning('Error in sending email. Try again later');
    }
}


header("location:../categories.php");
// echo json_encode("ok");
// exit;