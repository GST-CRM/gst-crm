<?php

include '../config.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

if (isset($_SERVER['HTTP_ORIGIN'])) {
    header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
}

$file = reset($_FILES);
if (!in_array(strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION)), array("gif", "jpg", "png"))) {
    header("HTTP/1.0 500 Invalid extension.");
    return;
}


$ext      = strtolower(pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION));
$destName = chr(rand(65, 90)) . uniqid(time()) . '.' . $ext;
$destFile = '../uploads/common/' . $destName ;
$siteFile = str_ireplace( '/inc', '', $crm_path) . 'uploads/common/' . $destName ;

if (move_uploaded_file($_FILES['file']['tmp_name'], $destFile)) {
    echo json_encode(array('location' => $siteFile));
}