<?php
$sql = "SELECT * FROM customer_account WHERE tourid=$tourid AND status=1 AND hisownticket=0";
$result = $db->query($sql);
$WithTicketTotal = $result->num_rows; 

$WithoutTicketSql = "SELECT * FROM customer_account WHERE tourid=$tourid AND status=1 AND hisownticket=1";
$WithoutTicketResult = $db->query($WithoutTicketSql);
$WithoutTicketTotal = $WithoutTicketResult->num_rows; 

$SingleRoomsSQL = "SELECT * FROM customer_account WHERE tourid=$tourid AND status=1 AND roomtype='Single'";
$SingleRoomsResult = $db->query($SingleRoomsSQL);
$SingleRoomsTotal = $SingleRoomsResult->num_rows; 

$AirUpgradePaxSQL = "SELECT * FROM customer_account WHERE tourid=$tourid AND status=1 AND upgradeproduct!=0";
$AirUpgradePaxResult = $db->query($AirUpgradePaxSQL);
$AirUpgradePaxTotal = $AirUpgradePaxResult->num_rows; 

$LandUpgradePaxSQL = "SELECT * FROM customer_account WHERE tourid=$tourid AND status=1 AND upgrade_land_product!=0";
$LandUpgradePaxResult = $db->query($LandUpgradePaxSQL);
$LandUpgradePaxTotal = $LandUpgradePaxResult->num_rows; 

$TransferPaxSQL = "SELECT * FROM customer_account WHERE tourid=$tourid AND status=1 AND Transfer!=0";
$TransferPaxResult = $db->query($TransferPaxSQL);
$TransferPaxTotal = $TransferPaxResult->num_rows; 

$ExtensionPaxSQL = "SELECT * FROM customer_account WHERE tourid=$tourid AND status=1 AND Extension!=0";
$ExtensionPaxResult = $db->query($ExtensionPaxSQL);
$ExtensionPaxTotal = $ExtensionPaxResult->num_rows; 

$InsurancePaxSQL = "SELECT * FROM customer_account WHERE tourid=$tourid AND status=1 AND insurance!=0";
$InsurancePaxResult = $db->query($InsurancePaxSQL);
$InsurancePaxTotal = $InsurancePaxResult->num_rows; 

$ComplimentaryPaxSQL = "SELECT ca.contactid FROM customer_account ca
JOIN customer_credit_note cn
ON cn.Customer_Account_Customer_ID=ca.contactid
WHERE ca.tourid=$tourid AND cn.Full_Discount=1";
$ComplimentaryPaxResult = $db->query($ComplimentaryPaxSQL);
$ComplimentaryPaxTotal = $ComplimentaryPaxResult->num_rows; 

$CashReceivedSQL = "SELECT SUM(cp.Customer_Payment_Amount) AS TotalPaymentsFromPax FROM customer_payments cp
JOIN sales_order so 
ON cp.Customer_Payment_Sales_Order_ID=so.Sales_Order_Num
Where so.Group_ID=$tourid";
$CashReceivedResult = $db->query($CashReceivedSQL);
$CashReceivedTotal = $CashReceivedResult->fetch_assoc(); 

/*$ExpectedIncomeExtraSQL = "SELECT SUM(sol.Sales_Amount) AS ExpectedIncomeExtra FROM sales_order_line sol
JOIN sales_order so 
ON so.Sales_Order_Num=sol.Sales_Order_Num
WHERE so.Group_ID=$tourid";
$ExpectedIncomeExtraResult = $db->query($ExpectedIncomeExtraSQL);
$ExpectedIncomeExtraTotal = $ExpectedIncomeExtraResult->fetch_assoc(); */
$ExpectedIncomeSQL = "SELECT SUM(so.Total_Sale_Price) AS TotalBasicPrice FROM sales_order so
JOIN customer_account ca
ON so.Customer_Account_Customer_ID=ca.contactid
WHERE so.Group_ID=$tourid AND ca.status=1";
$ExpectedIncomeResult = $db->query($ExpectedIncomeSQL);
$ExpectedIncomeTotal = $ExpectedIncomeResult->fetch_assoc(); 

$ExpensesLandSQL = "SELECT pro.cost AS TotalExpenses
FROM customer_account ca
JOIN groups gro 
ON ca.tourid=gro.tourid
JOIN products pro 
ON gro.land_productid=pro.id
WHERE ca.tourid=$tourid AND ca.status=1";
$ExpensesLandResult = $db->query($ExpensesLandSQL);
$ExpensesLandTotal = $ExpensesLandResult->fetch_assoc(); 

$ExpensesAirSQL = "SELECT pro.cost AS TotalExpenses
FROM customer_account ca
JOIN groups gro 
ON ca.tourid=gro.tourid
JOIN products pro 
ON gro.airline_productid=pro.id
WHERE ca.tourid=$tourid AND ca.status=1 AND ca.hisownticket=0";
$ExpensesAirResult = $db->query($ExpensesAirSQL);
$ExpensesAirTotal = $ExpensesAirResult->fetch_assoc(); 

//Get the cost of air upgrade for all the pax in this group
$GetCostAirUpgradeSQL = "SELECT SUM(pro.cost) AS TotalCost
FROM customer_account ca
LEFT JOIN products pro 
ON ca.upgradeproduct=pro.id
WHERE ca.tourid=$tourid and ca.status=1";
$GetCostAirUpgradeResult = $db->query($GetCostAirUpgradeSQL);
$GetCostAirUpgradeTotal = $GetCostAirUpgradeResult->fetch_assoc();

//Get the cost of land upgrade for all the pax in this group
$GetCostLandUpgradeSQL = "SELECT SUM(pro.cost) AS TotalCost
FROM customer_account ca
LEFT JOIN products pro 
ON ca.upgrade_land_product=pro.id
WHERE ca.tourid=$tourid and ca.status=1";
$GetCostLandUpgradeResult = $db->query($GetCostLandUpgradeSQL);
$GetCostLandUpgradeTotal = $GetCostLandUpgradeResult->fetch_assoc(); 

//Get the cost of Single Supplement for all the pax in this group
$GetCostSingleSQL = "SELECT SUM(pol.Cost_Amount) AS TotalCost FROM group_po_line pol
JOIN group_purchase_order po 
ON po.Group_PO_ID=pol.Group_PO_ID
WHERE po.Group_Group_ID=$tourid AND pol.Line_Type='Single Supplement'";
$GetCostSingleResult = $db->query($GetCostSingleSQL);
$GetCostSingleTotal = $GetCostSingleResult->fetch_assoc(); 


//To get the expenses of the agents
$ExpenseAgentsSQL = "SELECT 
	SUM((
        (SELECT COUNT(ca.contactid)
      	FROM customer_account ca
      	WHERE ca.status=1
         	AND ca.tourid=$tourid
        	AND ca.contactid not in (
				select cn.Customer_Account_Customer_ID
				from customer_credit_note cn
				JOIN customer_account ca
				ON ca.tourid=$tourid
				WHERE cn.Customer_Account_Customer_ID=ca.contactid
				AND cn.Full_Discount=1
			)
	)*ga.commissionvalue)) AS CommisionTotal
FROM group_agents ga
JOIN contacts co 
ON co.id=ga.agent_id
WHERE ga.group_id=$tourid";
$ExpensesAgentsResult = $db->query($ExpenseAgentsSQL);
$ExpensesAgentsTotal = $ExpensesAgentsResult->fetch_assoc();


//To get the expenses of the General Group Expenses
$GeneralExpensesSQL = "SELECT SUM(exp.Expense_Amount) AS GeneralGroupExpensesTotal FROM expenses exp 
		JOIN groups gro 
		ON exp.Group_ID=gro.tourid
		WHERE exp.status=1 AND exp.Group_ID=$tourid";
$GeneralExpensesResult = $db->query($GeneralExpensesSQL);
$GeneralExpensesTotal = $GeneralExpensesResult->fetch_assoc(); 


//To get the information of the Land supplier for the expenses tab
$LandSupplierDataSQL = "SELECT co.id,co.fname,co.lname,co.company,co.title FROM contacts co 
JOIN products pr 
ON pr.supplierid = co.id
JOIN groups gr 
ON gr.land_productid=pr.id
WHERE gr.tourid=$tourid AND pr.categoryid=2";
$LandSupplierDataResult = $db->query($LandSupplierDataSQL);
$LandSupplierDataTotal = $LandSupplierDataResult->fetch_assoc(); 

//To get the total amount of the Airline Supplier Payments for the group
$TheSupplierLandID = $LandSupplierDataTotal['id'];
if($TheSupplierLandID !="") {
$SuppLandPaymentPaidSQL = "SELECT SUM(sp.Supplier_Payment_Amount) AS TotalPaymentsPaid
FROM suppliers_payments sp
JOIN purchase_order po
ON po.Purchase_Order_Num=sp.Supplier_Payment_Purchase_Order_ID
WHERE sp.Supplier_ID=$TheSupplierLandID AND po.Group_ID=$tourid";
$SuppLandPaymentPaidResult = $db->query($SuppLandPaymentPaidSQL);
$SuppLandPaymentPaidTotal = $SuppLandPaymentPaidResult->fetch_assoc();
$SuppLandPaymentsDone = $SuppLandPaymentPaidTotal['TotalPaymentsPaid']; 
}

//To get the total amount of the Land Supplier Payments for the group
$TheSupplierAirID = $keyword2["id"];
if($TheSupplierAirID !="") {
$SuppAirPaymentPaidSQL = "SELECT SUM(sp.Supplier_Payment_Amount) AS TotalPaymentsPaid
FROM suppliers_payments sp
JOIN purchase_order po
ON po.Purchase_Order_Num=sp.Supplier_Payment_Purchase_Order_ID
WHERE sp.Supplier_ID=$TheSupplierAirID AND po.Group_ID=$tourid";
$SuppAirPaymentPaidResult = $db->query($SuppAirPaymentPaidSQL);
$SuppAirPaymentPaidTotal = $SuppAirPaymentPaidResult->fetch_assoc();
$SuppAirPaymentsDone = $SuppAirPaymentPaidTotal['TotalPaymentsPaid']; 
}

//To get the total amount of the General Payments for the group itself that aren't related to suppliers
$TotalPaymentPaidSQL = "SELECT COUNT(Expense_Num) AS GroupExpensesCount, SUM(Expense_Amount) AS TotalGroupExpenses FROM expenses WHERE Group_ID=$tourid AND status=1";
$TotalPaymentPaidResult = $db->query($TotalPaymentPaidSQL);
$TotalPaymentPaidTotal = $TotalPaymentPaidResult->fetch_assoc(); 

//To get the total number of rows for the general payments for the group itself
$NumberOfPaymentsSQL = "SELECT Group_Payment_Amount FROM groups_payments WHERE Group_Payment_Group_ID=$tourid";
$NumberOfPaymentsResult = $db->query($NumberOfPaymentsSQL);
$NumberOfPaymentsTotal = $NumberOfPaymentsResult->num_rows; 

?>
<style type="text/css">
    @media screen and (max-width: 568px) {
	  .flex-direction-media { 
	  	flex-direction: column !important;
	  }
	}
</style>

<div class="row">
	<div class="col-12">
		<h4 class="sub-title">Detailed Expenses & Revenues</h4>
		<ul class="list-group flex-direction-media" style="flex-direction: row;">
			<li class="list-group-item rounded-0 d-flex justify-content-between align-items-center list-group-item-action list-group-item-success text-dark" style="margin-bottom:0px;">
				Expected Income
				<span class="badge badge-dark badge-pill"><span data-toggle="tooltip" data-placement="top" title="" data-original-title="(Group Selling Price x Pax) + Total Upgrades"><?php echo GUtils::formatMoney($ExpectedIncomeTotal['TotalBasicPrice']); ?></span></span>
			</li>
			<li class="list-group-item rounded-0 d-flex justify-content-between align-items-center list-group-item-action list-group-item-success text-dark" style="margin-bottom:0px;border-right: 0px;border-left: 0px;">
				Cash Received
				<span class="badge badge-dark badge-pill"><?php echo GUtils::formatMoney($CashReceivedTotal['TotalPaymentsFromPax']); ?></span>
			</li>
			<li class="list-group-item rounded-0 d-flex justify-content-between align-items-center list-group-item-action list-group-item-success text-dark">
				Account Receivable
				<span class="badge badge-dark badge-pill"><?php echo GUtils::formatMoney($ExpectedIncomeTotal['TotalBasicPrice']-$CashReceivedTotal['TotalPaymentsFromPax']); ?></span>
			</li>
		</ul>
		<ul class="list-group flex-direction-media" style="flex-direction: row;">
			<li class="list-group-item rounded-0 d-flex justify-content-between align-items-center list-group-item-action list-group-item-danger text-dark" style="margin-bottom:0px;">
				Total Expenses
				<?php
				$TotalExpensesAir = ($ExpensesAirTotal['TotalExpenses']*$WithTicketTotal)+$GetCostAirUpgradeTotal['TotalCost'];
				$TotalParticipants = $WithTicketTotal + $WithoutTicketTotal;
				$TotalExpensesLand = ($ExpensesLandTotal['TotalExpenses']*$TotalParticipants)+$GetCostLandUpgradeTotal['TotalCost']+$GetCostSingleTotal['TotalCost'];
				$ExpensesPaidSoFar = $TotalPaymentPaidTotal['TotalPaymentsPaid'];
				$TotalAgentsExpense = $ExpensesAgentsTotal['CommisionTotal'];
				$TotalGeneralExpense = $GeneralExpensesTotal['GeneralGroupExpensesTotal'];
				echo $TotalExpensesALL = $TotalExpensesLand+$TotalExpensesAir+$ExpensesPaidSoFar+$TotalAgentsExpense+$TotalGeneralExpense;
				?>
				<span class="badge badge-dark badge-pill"><span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Airline & Land Expenses + General Expenses + Agents Expenses"><?php echo GUtils::formatMoney($TotalExpensesALL); ?></span></span>
			</li>
			<li class="list-group-item rounded-0 d-flex justify-content-between align-items-center list-group-item-action list-group-item-danger text-dark" style="margin-bottom:0px;border-right: 0px;border-left: 0px;">
				Cash Paid
				<span class="badge badge-dark badge-pill"><?php echo GUtils::formatMoney($ExpensesPaidSoFar+$SuppAirPaymentsDone+$SuppLandPaymentsDone); ?></span>
			</li>
			<li class="list-group-item rounded-0 d-flex justify-content-between align-items-center list-group-item-action list-group-item-danger text-dark">
				Account Payable
				<span class="badge badge-dark badge-pill"><?php echo GUtils::formatMoney($TotalExpensesALL-$ExpensesPaidSoFar-$SuppAirPaymentsDone-$SuppLandPaymentsDone); ?></span>
			</li>
		</ul>
	</div>
	<div class="col-12"><br /><hr><br />
	</div>

	<div class="col-12">
		<h4 class="sub-title">Group General Expenses <small>That are not related to suppliers but connected to the group.</small></h4>
		<ul class="list-group flex-direction-media" style="flex-direction: row;">
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark rounded-0 m-0">
				Expenses So far
				<span class="badge badge-dark badge-pill"><?php echo GUtils::formatMoney($TotalPaymentPaidTotal['TotalGroupExpenses']); ?></span>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark rounded-0 m-0" style="border-right:0px;border-left:0px;">
				# of Payments
				<span class="badge badge-dark badge-pill"><?php echo $TotalPaymentPaidTotal['GroupExpensesCount']; ?></span>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark rounded-0 m-0">
				<a href="expenses-add.php?TourExpense=<?php echo $tourid; ?>" class="btn btn-outline-success float-right btn-sm btn-block">Add an Expense</a>
			</li>
		</ul>
		<?php
		$sql2 = "SELECT gag.GL_Group_Name,coa.GL_Account_Name,exp.*,gro.tourname FROM expenses exp 
		JOIN expenses_line exline
        ON exp.Expense_Num=exline.Expense_Num
        JOIN gl_account_groups gag
        ON exline.Expense_Line_Category=gag.GL_Group_Num
        LEFT JOIN gl_chart_of_accounts coa
        ON exline.Expense_Line_CoA=coa.Account_ID
        JOIN groups gro 
		ON exp.Group_ID=gro.tourid
		WHERE exp.status=1 AND exp.Group_ID=$tourid";
		$result = $db->query($sql2);
		if ($result->num_rows > 0) { ?>
		<br />
		<ul class="list-group table-responsive">
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-light font-weight-bold bg-dark">
				<div class="col-sm-1">ID</div>
				<div class="col-sm-2">Date</div>
				<div class="col-sm-2">Amount</div>
				<div class="col-sm-3">Comments</div>
				<div class="col-sm-3">Type</div>
				<div class="col-sm-1">Files</div>
			</li>
			<?php while($row = $result->fetch_assoc()) { ?>
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				<div class="col-sm-1">
				<a href="expenses-edit.php?id=<?php echo $row["Expense_Num"]; ?>&action=edit" style="font-weight:bold"><?php echo "E".$row['Expense_Num']; ?></a>
				</div>
				<div class="col-sm-2">
				<?php echo date('m/d/Y', strtotime($row["Expense_Date"])); ?>
				</div>
				<div class="col-sm-2">
				<?php echo GUtils::formatMoney($row['Expense_Amount']); ?>
				</div>
				<div class="col-sm-3">
				<?php echo substr($row['Comments'], 0, 40)."..."; ?>
				</div>
				<div class="col-sm-3">
				<?php if($row['GL_Account_Name'] != NULL) {echo $row['GL_Account_Name'];} else {echo $row['GL_Group_Name'];} ?>
				</div>
				<div class="col-sm-1">
				<a href="uploads/<?php echo $row["Attachment"]; ?>" target="_blank"><img src="files/assets/images/view-file.png" style="width:25px;height:auto;" alt="download" /></a>
				</div>
			</li>
			<?php } ?>
		</ul>
		<br />
		<?php } ?>
	</div>
<?php if($NumberOfPaymentsTotal > 0) { ?>
	<div class="col-sm-12"><br /></div>
	<div class="col-sm-12">
		<h4 class="sub-title">Payments done for the group expenses</h4>
		<table class="table table-hover table-striped table-smallz">
			<tbody>
				<?php 
				$sql = "SELECT * FROM groups_payments WHERE Group_Payment_Group_ID=$tourid";
				$result = $db->query($sql);
				if ($result->num_rows > 0) { while($GeneralPaymentDetails = $result->fetch_assoc()) { ?>
				<tr>
					<td><?php echo $GeneralPaymentDetails['Group_Payment_Date']; ?></td>
					<td><?php echo GUtils::formatMoney($GeneralPaymentDetails['Group_Payment_Amount']); ?></td>
					<td><?php echo $GeneralPaymentDetails['Group_Payment_Comments']; ?></td>
					<td><i class="fas fa-trash-alt"></i></td>
				</tr>
				<?php }} else { ?>
				<tr>
					<td colspan="5">There are no payments or any expenses for this group yet!</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
<?php } ?>
	<div class="col-12"><br /><hr><br />
	</div>

	<div class="col-12">
		<h4 class="sub-title">Agents Expenses <small>The detailed commissions of the agents for this group.</small></h4>
		<?php
		$TotalAgentsExpense = 0;
		$AgentsListSQL = "SELECT 
							ga.agent_id,
							co.fname,
							co.lname,
							ga.commissiontype,
							ga.commissionvalue,
							(SELECT COUNT(ca.contactid)
								FROM customer_account ca
								WHERE ca.status=1
									AND ca.tourid=$tourid
									AND ca.contactid not in (
										select cn.Customer_Account_Customer_ID
										from customer_credit_note cn
										JOIN customer_account ca
										ON ca.tourid=$tourid
										WHERE cn.Customer_Account_Customer_ID=ca.contactid
										AND cn.Full_Discount=1
									)
							) AS PaxTotal
						FROM group_agents ga
						JOIN contacts co 
						ON co.id=ga.agent_id
						WHERE ga.group_id=$tourid";
		$AgentsListResult = $db->query($AgentsListSQL);
		if ($AgentsListResult->num_rows > 0) { ?>
		<ul class="list-group table-responsive">
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-light font-weight-bold bg-dark">
				<div class="col-sm-3">Agent Name</div>
				<div class="col-sm-3">Commision Amount</div>
				<div class="col-sm-3">Commision Type</div>
				<div class="col-sm-3">Total Commision</div>
			</li>
			<?php while($AgentsRow = $AgentsListResult->fetch_assoc()) { ?>
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				<div class="col-sm-3">
				<a href="contacts-edit.php?id=<?php echo $AgentsRow["agent_id"]; ?>&action=edit&usertype=agent" style="font-weight:bold"><?php echo $AgentsRow['fname']." ".$AgentsRow['lname']; ?></a>
				</div>
				<div class="col-sm-3">
				<?php echo GUtils::formatMoney($AgentsRow["commissionvalue"]); ?>
				</div>
				<div class="col-sm-3">
				<?php echo $AgentsRow["commissiontype"]; ?>
				</div>
				<div class="col-sm-3">
				<?php 
					$AgentExAmount = 0;
					if($AgentsRow["commissiontype"] == "Per Person") {
						$AgentExAmount += $AgentsRow["commissionvalue"]*$AgentsRow["PaxTotal"];
						echo GUtils::formatMoney($AgentExAmount);
					} else {
						$AgentExAmount += $AgentsRow["commissionvalue"];
						echo GUtils::formatMoney($AgentExAmount);
					}
					$TotalAgentsExpense += $AgentExAmount;
				?>
				</div>
			</li>
			<?php } ?>
		</ul>
		<br />
		<?php } else { ?>
		<ul class="list-group table-responsive">
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				<div class="col">
					There are no agents assigned for this group yet!
				</div>
			</li>
		</ul>
		<br />
		<?php } ?>
	</div>
	<div class="col-12"><br /><hr><br />
	</div>
	<div class="col-md-6">
		<h4 class="sub-title">Airline Package <small>Detailed Breakdown</small></h4>
		<img src="<?php echo $crm_path.$crm_images; ?>/airplane2-header.jpg" class="img-thumbnail img-fluid rounded m-b-10 w-100" alt="Airplane Header" />
		<ul class="list-group">
		<?php if($keyword2["id"] != "") { ?>
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				Supplier's Company
				<span class="badge badge-dark badge-pill"><?php echo $keyword2["company"]; ?></span>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				Supplier's Name
				<span class="badge badge-dark badge-pill"><?php echo $keyword2["title"]." ".$keyword2["fname"]." ".$keyword2["lname"]; ?></span>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				Participants need tickets
				<span class="badge badge-dark badge-pill"><?php echo $WithTicketTotal; ?> pax</span>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				Participants need Airline upgrades
				<span class="badge badge-dark badge-pill"><?php echo $AirUpgradePaxTotal; ?> pax</span>
			</li>
			<li style="border-top: double 3px #ccc;" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				Total amount owed
				<span class="badge badge-dark badge-pill"><span data-toggle="tooltip" data-placement="top" title="" data-original-title="(Basic Cost x Pax needs tickets) + Total Airline Upgrades"><?php echo GUtils::formatMoney($TotalExpensesAir); ?></span></span>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				Amount paid so far
				<span class="badge badge-dark badge-pill"><?php echo GUtils::formatMoney($SuppAirPaymentsDone); ?></span>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				Remaining to be paid
				<span class="badge badge-dark badge-pill"><?php echo GUtils::formatMoney($TotalExpensesAir-$SuppAirPaymentsDone); ?></span>
			</li>
		</ul>
		<a href="#" class="btn m-t-10 btn-outline-success float-right btn-sm d-none" data-toggle="modal" data-target="#AddGroupAirSuppPayment">Add a Payment</a>
		<a href="suppliers-bill-payments-add.php?supplier=<?php echo $keyword2["id"]; ?>&GroupID=<?php echo $tourid; ?>" class="btn m-t-10 btn-outline-success float-right btn-sm">Add a Payment</a>
		<a href="#" class="btn m-t-10 m-r-10 btn-outline-dark float-right btn-sm d-none">Create a Receipt</a>
		<?php } else { ?>
			<li class="text-center list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				<strong>The airline information isn't filled yet!</strong>
			</li>
		</ul>
		<?php } ?>
	</div>
	<div class="col-md-6">
		<h4 class="sub-title">Land Package <small>Detailed Breakdown</small></h4>
		<img src="<?php echo $crm_path.$crm_images; ?>/land-header.jpg" class="img-thumbnail img-fluid rounded m-b-10 w-100" alt="Land Package Header" />
		<ul class="list-group">
		<?php if($LandSupplierDataTotal["company"] != "") { ?>
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				Supplier's Company
				<span class="badge badge-dark badge-pill"><?php echo $LandSupplierDataTotal['company'];?></span>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				Supplier's Name
				<span class="badge badge-dark badge-pill"><?php echo $LandSupplierDataTotal['title']." ".$LandSupplierDataTotal['fname']." ".$LandSupplierDataTotal['lname'];?></span>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				Participants need single supplement
				<span class="badge badge-dark badge-pill"><?php echo $SingleRoomsTotal; ?> pax</span>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				Participants need land upgrades
				<span class="badge badge-dark badge-pill"><?php echo $LandUpgradePaxTotal; ?> pax</span>
			</li>
			<li style="border-top: double 3px #ccc;" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				Total amount owed
				<span class="badge badge-dark badge-pill"><span data-toggle="tooltip" data-placement="top" title="" data-original-title="(Basic Cost x Pax) + Total Land Upgrades + Total Single Supplements"><?php echo GUtils::formatMoney($TotalExpensesLand); ?></span></span>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				Amount paid so far
				<span class="badge badge-dark badge-pill"><?php echo GUtils::formatMoney($SuppLandPaymentsDone); ?></span>
			</li>
			<li class="list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				Remaining to be paid
				<span class="badge badge-dark badge-pill"><?php echo GUtils::formatMoney($TotalExpensesLand-$SuppLandPaymentsDone); ?></span>
			</li>
		</ul>
		<a href="#" class="btn m-t-10 btn-outline-success float-right btn-sm d-none" data-toggle="modal" data-target="#AddGroupLandSuppPayment">Add a Payment</a>
		<a href="suppliers-bill-payments-add.php?supplier=<?php echo $LandSupplierDataTotal["id"]; ?>&GroupID=<?php echo $tourid; ?>" class="btn m-t-10 btn-outline-success float-right btn-sm">Add a Payment</a>
		<a href="#" class="btn m-t-10 m-r-10 btn-outline-dark float-right btn-sm d-none">Create a Receipt</a>
		<?php } else { ?>
			<li class="text-center list-group-item d-flex justify-content-between align-items-center list-group-item-action list-group-item-light text-dark">
				<strong>The land information isn't filled yet!</strong>
			</li>
		</ul>
		<?php } ?>
	</div>
</div>