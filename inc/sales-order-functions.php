<?php


if(isset($_POST['SalesOrderType'])) {$SalesOrderType = mysqli_real_escape_string($db, $_POST['SalesOrderType']);} else { $SO_Product = ""; }
if(isset($_POST['CustomerID'])) {$CustomerID = mysqli_real_escape_string($db, $_POST['CustomerID']);}
if(isset($_POST['Sales_Order_Date'])) {$Sales_Order_Date = mysqli_real_escape_string($db, $_POST['Sales_Order_Date']);} else { $Sales_Order_Date = ""; }
if(isset($_POST['Sales_Order_Due_Date'])) {$Sales_Order_Due_Date = mysqli_real_escape_string($db, $_POST['Sales_Order_Due_Date']);} else { $Sales_Order_Due_Date = "0000-00-00"; }
if(isset($_POST['Sales_Order_Comments'])) {$Sales_Order_Comments = mysqli_real_escape_string($db, $_POST['Sales_Order_Comments']);} else { $Sales_Order_Comments = ""; }
if(isset($_POST['SalesOrderTotal'])) {$SalesOrderTotal = mysqli_real_escape_string($db, $_POST['SalesOrderTotal']);}
if(isset($_POST['LatestSalesOrderIdentifier'])) {$LatestSalesOrderIdentifier = mysqli_real_escape_string($db, $_POST['LatestSalesOrderIdentifier']);}
if(isset($_POST['SO_Product'])) {$SO_Product = $_POST["SO_Product"];} else { $SO_Product = ""; }

//Add a new general expense and its lines
if(isset($SalesOrderType) AND $SalesOrderType == "NEW"){
	$Warning = "";
	
	if($CustomerID == NULL) { $Warning .= "Please select a customer for this sales order<br />";}
	if($Sales_Order_Date == NULL) { $Warning .= "Please select the create date of the sales order<br />";}
	if($Sales_Order_Due_Date == NULL) { $Warning .= "Please select the due date of the sales order<br />";}
	
	if($Warning == "") {
		if ($_FILES["fileToUpload"]["name"] != "") {
			$target_dir = "uploads/";
			$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			$temp = explode(".", $_FILES["fileToUpload"]["name"]);
			$filename = time() . '.' . end($temp);
			$fileurl = $target_dir . $filename;
			$mediaurl = $filename;
			$UploadMessageText = "";
			// Check if file already exists
			if (file_exists($target_file)) {
				$UploadMessageText .= "Sorry, file already exists.";
				$uploadOk = 0;
			}
			// Check file size
			if ($_FILES["fileToUpload"]["size"] > 50000000) {
				$UploadMessageText .= "Sorry, your file is too large.";
				$uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "pdf"
			&& $imageFileType != "gif" && $imageFileType != "doc" && $imageFileType != "docx" ) {
				$UploadMessageText .= "Sorry, only JPG, JPEG, PNG, GIF & PDF & DOCX & DOC files are allowed.";
				$uploadOk = 0;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
				//$UploadMessageText .= "Sorry, your file was not uploaded.";
				$UploadMessage = base64_encode($UploadMessageText);
				echo "<meta http-equiv='refresh' content='0;url=../contacts-edit.php?id=$userid&action=edit&usertype=attachments&message=$UploadMessage'>";
			// if everything is ok, try to upload file
			}
		move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $fileurl);
		}
		$sql7 = "INSERT INTO sales_order(Group_ID, Customer_Account_Customer_ID, Sales_Order_Identifier, Sales_Order_Type, Sales_Order_Date, Sales_Order_Due_Date, Sales_Order_Comments, Total_Sale_Price, Sales_Order_Attachment)
		VALUES (0,'$CustomerID','$LatestSalesOrderIdentifier','Manual','$Sales_Order_Date','$Sales_Order_Due_Date','$Sales_Order_Comments','$SalesOrderTotal','$mediaurl');";
		$sql7 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','Sales Order','$CustomerID','added','Created a new sales order');";
		if ($db->multi_query($sql7) === TRUE) {
			//Success first query
			$SalesOrderNum = $db->insert_id;
			$query = "";
			$SOlinesConn = new mysqli($servername, $username, $password, $dbname);
			if ($SOlinesConn->connect_error) {die("Connection failed: " . $SOlinesConn->connect_error);}
			
			for($count = 0; $count < count($SO_Product); $count++) {
				$SO_Product_ID = $_POST["SO_Product"][$count];
				$SO_Product_Name = $_POST["product_name"][$count];
				$SO_Quantity = $_POST["SO_line_quantity"][$count];
				$SO_Price = $_POST["product_price"][$count];
				$SO_Category = $_POST["product_category"][$count];
					if($SO_Category == 1) {$LineType = "Airline Package";}
					elseif($SO_Category == 2) {$LineType = "Land Package";}
					elseif($SO_Category == 3) {$LineType = "Insurance";}
					elseif($SO_Category == 4) {$LineType = "Air Upgrade";}
					elseif($SO_Category == 5) {$LineType = "Land Upgrade";}
					elseif($SO_Category == 6) {$LineType = "Ticket Discount";}
					elseif($SO_Category == 7) {$LineType = "Single Supplement";}
				$query .= "INSERT INTO sales_order_line(Sales_Order_Num, Line_Type, Line_Identifier, Product_Product_ID, Product_Name, Sales_Quantity, Sales_Amount, Supplier_Supplier_ID)
				VALUES ('$SalesOrderNum','$LineType', FLOOR(10000 + RAND() * 90000), '$SO_Product_ID','$SO_Product_Name','$SO_Quantity','$SO_Price','$');";
			}

			//$query .= "UPDATE expenses SET Expense_Amount='$ExpenseTotal' WHERE Expense_Num=$SupplierBillNum;";
			if ($SOlinesConn->multi_query($query) === TRUE) {

				GUtils::setSuccess("The Sales Order Record has been added successfully!");
			    if ($_POST['submitBtn'] == 'save_close') {
			        GUtils::redirect('sales.php');
			    } else {
			        GUtils::redirect('sales-add.php');
			    }

				//echo "<h3>Success</h3>The general expense has been recorded! The page will be refreshed in seconds.";
				// echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong><h3>Success!</h3>The Supplier Bill Record has been added successfully! The page will be refreshed in seconds.</strong></div>";
				// echo "<meta http-equiv='refresh' content='2;bills-edit.php?id=$SupplierBillNum&action=edit'>";
			} else {
			echo $query . "<br>" . $SOlinesConn->error;
			}
			$SOlinesConn->close();
		} else {
			echo $sql7 . "<br>" . $db->error;
		}
	} else {
		//echo $Warning;
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong><h3>Error!</h3>".$Warning."</strong></div>";
	}
}

//Edit a current general expense and its lines
if(isset($SalesOrderType) AND $SalesOrderType == "EDIT"){
	$Warning = "";
	$SalesOrderID = mysqli_real_escape_string($db, $_POST['SalesOrderID']);
	$CustomerID = mysqli_real_escape_string($db, $_POST['CustomerID']);
	$Sales_Order_Date = mysqli_real_escape_string($db, $_POST['Sales_Order_Date']);
	$Sales_Order_Due_Date = mysqli_real_escape_string($db, $_POST['Sales_Order_Due_Date']);
	$Sales_Order_Comments = mysqli_real_escape_string($db, $_POST['Sales_Order_Comments']);
	$SalesOrderTotal = mysqli_real_escape_string($db, $_POST['SalesOrderTotal']);
	$SO_Product = $_POST["SO_Product"];
	
	if($Sales_Order_Date == NULL) { $Warning .= "Please specify the sales order Date<br />";}
	if($Sales_Order_Due_Date == NULL) { $Warning .= "Please specify the sales order Due Date<br />";}
	
	if($Warning == "") {
		if ($_FILES["fileToUpload"]["name"] != "") {
			$target_dir = "uploads/";
			$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			$temp = explode(".", $_FILES["fileToUpload"]["name"]);
			$filename = time() . '.' . end($temp);
			$fileurl = $target_dir . $filename;
			$mediaurl = $filename;
			$UploadMessageText = "";
			// Check if file already exists
			if (file_exists($target_file)) {
				$UploadMessageText .= "Sorry, file already exists.";
				$uploadOk = 0;
			}
			// Check file size
			if ($_FILES["fileToUpload"]["size"] > 50000000) {
				$UploadMessageText .= "Sorry, your file is too large.";
				$uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "pdf"
			&& $imageFileType != "gif" && $imageFileType != "doc" && $imageFileType != "docx" ) {
				$UploadMessageText .= "Sorry, only JPG, JPEG, PNG, GIF & PDF & DOCX & DOC files are allowed.";
				$uploadOk = 0;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
				//$UploadMessageText .= "Sorry, your file was not uploaded.";
				$UploadMessage = base64_encode($UploadMessageText);
				echo "<meta http-equiv='refresh' content='0;url=../contacts-edit.php?id=$userid&action=edit&usertype=attachments&message=$UploadMessage'>";
			// if everything is ok, try to upload file
			}
		move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $fileurl);
		}
		if ($_FILES["fileToUpload"]["name"] != "") {
			$sql7 = "UPDATE sales_order SET Sales_Order_Date='$Sales_Order_Date',Sales_Order_Due_Date='$Sales_Order_Due_Date',Sales_Order_Comments='$Sales_Order_Comments',Sales_Order_Attachment='$mediaurl' WHERE Sales_Order_Num=$SalesOrderID;";
		} else { 
			$sql7 = "UPDATE sales_order SET Sales_Order_Date='$Sales_Order_Date',Sales_Order_Due_Date='$Sales_Order_Due_Date',Sales_Order_Comments='$Sales_Order_Comments' WHERE Sales_Order_Num=$SalesOrderID;";
		}
		$sql7 .= "DELETE FROM sales_order_line WHERE Sales_Order_Num=$SalesOrderID;";
		$sql7 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','Sales Order','$SalesOrderID','edited','Edited the sales order #$SalesOrderID');";
		
		$TotalSalesOrder = 0;
		for($count = 0; $count < count($SO_Product); $count++) {
			$SO_Product_ID = $_POST["SO_Product"][$count];
			$SO_Product_Name = $_POST["product_name"][$count];
			$SO_Quantity = $_POST["SO_line_quantity"][$count];
			$SO_Price = $_POST["product_price"][$count];
			$SO_Category = $_POST["product_category"][$count];
				if($SO_Category == 1) {$LineType = "Airline Package";}
				elseif($SO_Category == 2) {$LineType = "Land Package";}
				elseif($SO_Category == 3) {$LineType = "Insurance";}
				elseif($SO_Category == 4) {$LineType = "Air Upgrade";}
				elseif($SO_Category == 5) {$LineType = "Land Upgrade";}
				elseif($SO_Category == 6) {$LineType = "Ticket Discount";}
				elseif($SO_Category == 7) {$LineType = "Single Supplement";}
			$sql7 .= "INSERT INTO sales_order_line(Sales_Order_Num, Line_Type, Line_Identifier, Product_Product_ID, Product_Name, Sales_Quantity, Sales_Amount, Supplier_Supplier_ID)
			VALUES ('$SalesOrderID','$LineType', FLOOR(10000 + RAND() * 90000), '$SO_Product_ID','$SO_Product_Name','$SO_Quantity','$SO_Price','$');";
			$TotalSalesOrder += $SO_Quantity*$SO_Price;
		}
		$sql7 .= "UPDATE sales_order SET Total_Sale_Price='$TotalSalesOrder' WHERE Sales_Order_Num=$SalesOrderID;";
		if ($db->multi_query($sql7) === TRUE) {
			//Success first query
			//echo "<h3>Success</h3>The general expense has been edited! The page will be refreshed in seconds.";
			echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong><h3>Success!</h3>The Sales order has been updated! The page will be refreshed in seconds.</strong></div>";
			echo "<meta http-equiv='refresh' content='2;sales-edit.php?id=$SalesOrderID&action=edit'>";
			die();
		} else {
			echo $sql7 . "<br>" . $db->error;
		}
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong><h3>Error!</h3>".$Warning."</strong></div>";
	}
}



?>
