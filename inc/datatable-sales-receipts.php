<?php
//include connection file 
include_once '../config.php';
include_once '../models/model.php' ;


//define index of column
$columns = array( 
    0 => 'Invoice_ID',
    1 => 'Invoice_Date', 
    2 => 'Customer_ID', 
    3 => 'Full_Name', 
    4 => 'Group_ID', 
    5 => 'Group_Desc',
    6 => 'Invoice_Amount',
    7 => 'Paid_Amount'
);

$params = $__REQUEST;

if(!isset($params['order'])) {
    die("You can't access this page directly.");
}

$where  = '';
// check search value exist
if (!empty($params['search']['value'])) {
    $where .= " AND ";
    $where .= " (ci.Customer_Invoice_Num LIKE '%" . $params['search']['value'] . "%' ";
    $where .= " OR CONCAT(co.fname,' ',co.mname,' ',co.lname) LIKE '%" . $params['search']['value'] . "%' ";
    $where .= " OR gro.tourid LIKE '%" . $params['search']['value'] . "%') ";
}

if( $params['order'][0]['column'] == 0 )  {
    $orderBy = " ORDER BY cp.Customer_Payment_Date DESC " ;
}
else {
    $orderBy = " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."" ;
}

$sqlCount = "SELECT
	ci.Customer_Invoice_Num AS Invoice_ID,
	ci.Customer_Invoice_Date AS Invoice_Date,
	ci.Customer_Account_Customer_ID AS Customer_ID,
	CONCAT(co.fname,' ',co.mname,' ',co.lname) AS Full_Name,
	gro.tourid AS Group_ID,
	gro.tourdesc AS Group_Desc,
	ci.Invoice_Amount,
	SUM(cp.Customer_Payment_Amount) AS Paid_Amount
FROM customer_invoice ci
JOIN contacts co 
	ON co.id=ci.Customer_Account_Customer_ID 
JOIN groups gro 
	ON gro.tourid=ci.Group_ID
JOIN customer_payments cp
	ON cp.Customer_Invoice_Num=ci.Customer_Invoice_Num
WHERE 1 $where
GROUP BY ci.Customer_Invoice_Num
HAVING (ci.Invoice_Amount-Paid_Amount) = 0
ORDER BY cp.Customer_Payment_Date DESC";

$sql = "SELECT
	ci.Customer_Invoice_Num AS Invoice_ID,
	ci.Customer_Invoice_Date AS Invoice_Date,
	ci.Customer_Account_Customer_ID AS Customer_ID,
	CONCAT(co.fname,' ',co.mname,' ',co.lname) AS Full_Name,
	gro.tourid AS Group_ID,
	gro.tourdesc AS Group_Desc,
	ci.Invoice_Amount,
	SUM(cp.Customer_Payment_Amount) AS Paid_Amount
FROM customer_invoice ci
JOIN contacts co 
	ON co.id=ci.Customer_Account_Customer_ID 
JOIN groups gro 
	ON gro.tourid=ci.Group_ID
JOIN customer_payments cp
	ON cp.Customer_Invoice_Num=ci.Customer_Invoice_Num
WHERE 1 $where
GROUP BY ci.Customer_Invoice_Num
HAVING (ci.Invoice_Amount-Paid_Amount) = 0
$orderBy LIMIT " . $params['start'] . " ," . $params['length'];

$data         = GDb::fetchRowSet($sql);
$totalRecords = GDb::CountRows($sqlCount); //We have different query to get the total because this one doesn't have LIMIT flag


$output = [];
foreach ($data as $row) {
    $oneOutput  = [];
    $oneOutput[0] = "<input name='cbInvoice[]' value='".$row["Invoice_ID"]."' type='checkbox' class='gst-invoice-cb'/>";
    $oneOutput[1] = '<a style="color: #0000EE;" href="contacts-edit.php?id='.$row["Customer_ID"].'&action=edit&usertype=sales">'.$row["Invoice_ID"] . '</a>';
    $oneOutput[2] = '<a style="color: #0000EE;" href="contacts-edit.php?id='.$row["Customer_ID"].'&action=edit&usertype=sales">'.$row["Invoice_Date"]. '</a>';
    $oneOutput[3] = '<a style="color: #0000EE;" href="contacts-edit.php?id='.$row["Customer_ID"].'&action=edit&usertype=customer">'.$row["Full_Name"] . '</a>';
    $oneOutput[4] = '<a style="color: #0000EE;" href="groups-edit.php?id='.$row["Group_ID"].'&action=edit&tab=participants">'.$row["Group_Desc"] . '</a>';
    $oneOutput[5] = GUtils::formatMoney($row["Invoice_Amount"]);
    $oneOutput[6] = GUtils::formatMoney($row["Paid_Amount"]);
    
    $output[]     = $oneOutput;
}

$json_data = array(
    "draw"            => intval($params['draw']),
    "recordsTotal"    => intval($totalRecords),
    "recordsFiltered" => intval($totalRecords),
    "data"            => $output   // total data array
);

echo json_encode($json_data);  // send data as json format
