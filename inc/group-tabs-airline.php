				<form name="contact9" action="inc/group-functions.php" class="row" method="POST" id="contact9">
					<div class="form-group form-default form-static-label col-sm-8">
						<label class="float-label">Please select an Airfare Product</label>
                        <select id="airlineproduct" name="airlineproduct" class="form-control select2-airlineproduct form-control-default" multiple="multiple">
							<option value="0" disabled> </option>
							<option id="addnewoption" value="1">Add New</option>
							<?php 
                            $airProductId = $data["airline_productid"] ;
							$meta1 = new mysqli($servername, $username, $password, $dbname);
							$metadata1 = mysqli_query($meta1,"SELECT * FROM products WHERE id='$airProductId' AND status='1' AND subproduct='0' AND categoryid='1'");$metaselected ="";
							while($keyword1 = mysqli_fetch_array($metadata1))
							{
								if($data["airline_productid"] == $keyword1['id']) {$metaselected="selected";} else {$metaselected="";}
								echo "<option value='".$keyword1['id']."' ".$metaselected.">".$keyword1['id']." l ".$keyword1['name']." ($".$keyword1['cost'].")</option>";
							} ?>
						</select>
						<?php if($data["airline_productid"] != 0) { ?>
						<label id="airfarewarning" class="float-label" style="font-weight:bold;color:red;display:none;"> Warning: Changing the Airfare Product will lead to the loss of the current data!</label>
						<?php } ?>
						<input type="text" name="airtourid" class="form-control" value="<?php echo $data["tourid"]; ?>" hidden>
						<input type="text" name="airproductlineid" class="form-control" value="<?php echo $data["id"]; ?>" hidden>
						<input type="text" name="airproductlineSupplier" class="form-control" value="<?php echo $data["supplierid"]; ?>" hidden>
						<input type="text" name="tourtotalcost" class="form-control" value="<?php echo $TourCostTotal; ?>" hidden>
					</div>
                                    <?php if( AclPermission::actionAllowed('GroupSave') ) { ?>
					<div class="form-group form-default form-static-label col-sm-2 p-0">
						<button id="addnewbutton" type="reset" name="addnew" class="btn waves-effect waves-light btn-inverse" style="height:60px;width: 100%;" data-toggle="modal" data-target="#addnewproduct">Add New</button>
					</div>
					<div class="form-group form-default form-static-label col-sm-2">
						<button type="submit" name="submit" class="btn waves-effect waves-light btn-success" style="height:60px;width: 100%;" data-toggle="modal" data-target="#resultsmodal">Update</button>
					</div>
                                    <?php } ?>
				</form>
				<?php if($data["airline_productid"] != 0) { ?>
				<form name="contact2" action="inc/group-functions.php" class="row" method="POST" id="contact2">
                    
                    <?php if( AclPermission::actionAllowed('GroupAirlineDetails') ) { ?>
                    
                        <div class="form-group form-default form-static-label col-sm-4">
                            <label class="float-label">Airline Product (click to edit)</label>
                            <a href="products-edit.php?id=<?php echo $data["id"]; ?>&action=edit">
                            <input style="cursor: pointer;" type="text" name="airlinename" class="form-control" value="<?php echo $data["name"]; ?>" readonly>
                            <input type="text" name="TourNameforAirline" class="form-control" value="<?php echo $data["tourname"]; ?>" hidden>
                            </a>
                        </div>
                        <div class="form-group form-default form-static-label col-sm-4">
                            <?php 
                            $meta2 = new mysqli($servername, $username, $password, $dbname);
                            $supplierid = $data["supplierid"];
                            $metadata2 = mysqli_query($meta2,"SELECT id,company,fname,lname,title FROM contacts WHERE id=$supplierid");
                            $keyword2 = mysqli_fetch_array($metadata2); ?>						

                            <label class="float-label">Airline Supplier (click to edit)</label>
                            <a href="contacts-edit.php?id=<?php echo $keyword2["id"]; ?>&action=edit&usertype=supplier">
                                <input style="cursor: pointer;" type="text" name="AirProductSupplier" class="form-control" value="<?php echo $keyword2["company"]; ?>" readonly>
                            </a>
                        </div>
                                        <?php if( AclPermission::actionAllowed('AirfareCost') ) { ?>
                        <div class="form-group form-default form-static-label col-sm-2">
                            <label class="float-label">Airfare Cost</label>
                            <input type="text" name="airfarecost" class="form-control" value="<?php echo GUtils::formatMoney($data["cost"]); ?>" title="Edit the product directly to modify this number" readonly>
                        </div>
                                        <?php } ?>
                        <div class="form-group form-default form-static-label col-sm-2">
                            <label class="float-label">Airfare Price</label>
                            <input type="text" name="airfareprice" class="form-control" value="<?php echo GUtils::formatMoney($data["price"]); ?>" title="Edit the product directly to modify this number" readonly>
                        </div>
                    <?php } ?>
                    
                    
					<div class="form-group form-default form-static-label col-sm-12">
						<label class="float-label">Airfare Flights Information</label>
						<a href="javascript:void(0)" class="d-none add-row">Add row</a>
						<input name="airlinez" value="<?php echo $data["tourid"]; ?>" type="text" hidden>
						<input name="airlineproductidz" value="<?php echo $data["airline_productid"]; ?>" type="text" hidden>
						
						<?php // START *********** GROUP FLIGHT INFO SECTION *************** // ?>
						<table class="flightstable table table-striped">
							<thead>
								<tr>
									<th style="width:10%; font-size: 14px;">
										<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Airline Code">
											Airline
										</span>
									</th>
									<th style="width:9%; font-size: 14px;">
										<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Flight Number">
											Flight #
										</span>
									</th>
									<th style="width:23%; font-size: 14px;">
										<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Departure Date & Time">
											DEP Time
										</span>
									</th>
									<th style="width:12%; font-size: 14px;">
										<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="The Departure Airport">
											DEP Airport
										</span>
									</th>
									<th style="width:23%; font-size: 14px;">
										<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Arrival Date & Time">
											Arrival Time
										</span>
									</th>
									<th style="width:12%; font-size: 14px;">
										<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="The Arrival Airport">
											Arr. Airport
										</span>
									</th>
									<th style="width:11%; font-size: 14px;">
										<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Triggers for the Supplier">
											Trigger
										</span>
									</th>
								</tr>
							</thead>
							<tbody>
							<?php $FlightCounter = 1; 
							
							$FlightDB = new mysqli($servername, $username, $password, $dbname);
							$FlightSQL = mysqli_query($FlightDB,"SELECT gf.*, ga.Airline_Code, ga.Airline_Name ,gp.Airport_Code AS Departure_Airport_Code, gp.Airport_City AS Departure_Airport_City,gp2.Airport_Code AS Arrival_Airport_Code, gp2.Airport_City AS Arrival_Airport_City FROM groups_flights gf 
																LEFT JOIN groups_airline ga
																ON gf.Flight_Airline_ID=ga.Airline_ID
																LEFT JOIN groups_airport gp
																ON gf.Departure_Airport=gp.Airport_ID
																LEFT JOIN groups_airport gp2
																ON gf.Arrival_Airport=gp2.Airport_ID
																WHERE gf.Group_ID=$tourid AND gf.Specific_Customer_ID=0
																ORDER BY gf.GF_Line_ID ASC");
							$FlightDataCount = mysqli_num_rows ($FlightSQL);
							if($FlightDataCount == 0) {$FlightCounterLimit = 4;} elseif($FlightDataCount == 10) {$FlightCounterLimit = 10;} else { $FlightCounterLimit = $FlightDataCount+1;}
							while($FlightCounter <= $FlightCounterLimit) {
							
							$FlightData = mysqli_fetch_array($FlightSQL);
							?>
								<tr>
									<td class="pt-1 pb-1 select2Airportz">
									<input class="form-control" type="text" name="GF_Line_ID<?php echo $FlightCounter; ?>" value="<?php echo $FlightCounter; ?>" hidden>
										<select class="form-control select2-airfare col-sm-12" name="AirlineID<?php echo $FlightCounter; ?>" multiple="multiple" >
											<?php if($FlightData['Flight_Airline_ID'] != NULL) { echo "<option value='".$FlightData['Flight_Airline_ID']."' title='".$FlightData['Airline_Name']." (".$FlightData['Airline_Code'].")' selected>".$FlightData['Airline_Code']."</option>";} ?>
										</select>
									</td>
									<td class="pt-1 pb-1">
									<input class="form-control" type="text" name="AirlineFlightID<?php echo $FlightCounter; ?>" value="<?php echo $FlightData["Flight_Airline_Number"]; ?>">
									</td>
									<td class="pt-1 pb-1">																	
										<div class="input-group date mb-0 DEPtime" data-date="<?php echo date("Y-m-d")."T".date("h:i:s")."Z"; ?>" data-date-format="M dd @ HH:iip" data-link-field="<?php echo $FlightCounter; ?>dtp_input1">
											<input readonly class="form-control fill" size="16" type="text" value="<?php if($FlightData["Departure_Time"] != NULL) { $DepTime = strtotime( $FlightData["Departure_Time"] ); echo date( 'M d @ h:ia', $DepTime );} ?>">
											<span class="input-group-text input-group-addon" style="border-radius: 0px 5px 5px 0px;border-left:0px;"><span class="glyphicon glyphicon-th"></span></span>
										</div>
										<input type="hidden" id="<?php echo $FlightCounter; ?>dtp_input1" name="DEPtime<?php echo $FlightCounter; ?>" value="<?php echo $FlightData["Departure_Time"]; ?>" />
									</td>
									<td class="pt-1 pb-1 select2Airportz">
										<select class="form-control select2-airport col-sm-12" name="DepartureAirportID<?php echo $FlightCounter; ?>" multiple="multiple" >
											<?php if($FlightData['Departure_Airport'] != NULL) { echo "<option value='".$FlightData['Departure_Airport']."' title='".$FlightData['Departure_Airport_City']." (".$FlightData['Departure_Airport_Code'].")' selected>".$FlightData['Departure_Airport_Code']."</option>";} ?>
										</select>
									</td>
									<td class="pt-1 pb-1">
										<div class="input-group date mb-0 ARRtime" data-date="<?php echo date("Y-m-d")."T".date("h:i:s")."Z"; ?>" data-date-format="M dd @ HH:iip" data-link-field="<?php echo $FlightCounter; ?>dtp_input2">
											<input readonly class="form-control fill" size="16" type="text" value="<?php if($FlightData["Arrival_Time"] != NULL) { $ArrivTime = strtotime( $FlightData["Arrival_Time"] ); echo date( 'M d @ h:ia', $ArrivTime );} ?>">
											<span class="input-group-text input-group-addon" style="border-radius: 0px 5px 5px 0px;border-left:0px;"><span class="glyphicon glyphicon-th"></span></span>
										</div>
										<input type="hidden" id="<?php echo $FlightCounter; ?>dtp_input2" name="ARRtime<?php echo $FlightCounter; ?>" value="<?php echo $FlightData["Arrival_Time"]; ?>" />
									</td>
									<td class="pt-1 pb-1 select2Airportz">
										<select class="form-control select2-airport col-sm-12" name="ArrivalAirportID<?php echo $FlightCounter; ?>" multiple="multiple" >
											<?php if($FlightData['Arrival_Airport'] != NULL) { echo "<option value='".$FlightData['Arrival_Airport']."' title='".$FlightData['Arrival_Airport_City']." (".$FlightData['Arrival_Airport_Code'].")' selected>".$FlightData['Arrival_Airport_Code']."</option>";} ?>
										</select>
									</td>
									<td class="pt-1 pb-1">
										<span data-toggle="tooltip" data-placement="top" title="" data-original-title="The group is arriving the destination">
											<img src="<?php echo $crm_images; ?>/plane-A.png" width="15" alt="Flight Arrival" /> <input type="radio" name="SupplierTrigger<?php echo $FlightCounter; ?>" value="1" <?php if($FlightData["Supplier_Trigger"] == 1) { echo "checked"; } ?>>
										</span>
										<span data-toggle="tooltip" data-placement="top" title="" data-original-title="The group is leaving the destination">
											<img src="<?php echo $crm_images; ?>/plane-D.png" width="15" alt="Flight Departure" /> <input type="radio" name="SupplierTrigger<?php echo $FlightCounter; ?>" value="2" <?php if($FlightData["Supplier_Trigger"] == 2) { echo "checked"; } ?>>
										</span>
										<span data-toggle="tooltip" data-placement="top" title="" data-original-title="This is a normal flight row">
											<img src="<?php echo $crm_images; ?>/none.png" width="15" alt="None" /> <input type="radio" name="SupplierTrigger<?php echo $FlightCounter; ?>" value="0" <?php if($FlightData["Supplier_Trigger"] == 0 OR $FlightData["Supplier_Trigger"] == "") { echo "checked"; } ?>>
										</span>
									</td>
								</tr>
							<?php  $FlightCounter++; } ?>
							</tbody>
						</table>
<script type="text/javascript"> 
/*let lineNo = <?php echo $FlightCounter; ?>; 
$(document).ready(function () { 
	$(".add-row").click(function () { 
		//markup = "<tr><td  class='pt-1 pb-1'>row "+ lineNo +"</td><td  class='pt-1 pb-1'></td><td  class='pt-1 pb-1'></td><td  class='pt-1 pb-1'></td><td  class='pt-1 pb-1'></td><td  class='pt-1 pb-1'></td><td  class='pt-1 pb-1'></td><td  class='pt-1 pb-1'></td></tr>"; 
		markup = "<tr><td class='pt-1 pb-1 select2Airportz'><input class='form-control' type='text' name='GF_Line_ID"+ lineNo +"' value='"+ lineNo +"' hidden><select class='form-control select2-airfare col-sm-12' name='AirlineID"+ lineNo +"' multiple='multiple' ></select></td><td class='pt-1 pb-1'><input class='form-control' type='text' name='AirlineFlightID"+ lineNo +"' value=''></td><td class='pt-1 pb-1 select2Airportz'><select class='form-control select2-airport col-sm-12' name='DepartureAirportID"+ lineNo +"' multiple='multiple' ></select></td><td class='pt-1 pb-1'><div class='input-group date mb-0 DEPtime"+ lineNo +"' data-date='<?php echo date('Y-m-d').'T'.date('h:i:s').'Z'; ?>' data-date-format='M dd @ HH:iip' data-link-field='"+ lineNo +"dtp_input1'><input readonly class='form-control fill' size='16' type='text' value=''><span class='input-group-text input-group-addon' style='border-radius: 0px 5px 5px 0px;border-left:0px;'><span class='glyphicon glyphicon-th'></span></span></div><input type='hidden' id='"+ lineNo +"dtp_input1' name='DEPtime"+ lineNo +"' value='' /></td><td class='pt-1 pb-1 select2Airportz'><select class='form-control select2-airport col-sm-12' name='ArrivalAirportID"+ lineNo +"' multiple='multiple' ></select></td><td class='pt-1 pb-1'><div class='input-group date mb-0 ARRtime"+ lineNo +"' data-date='<?php echo date('Y-m-d').'T'.date('h:i:s').'Z'; ?>' data-date-format='M dd @ HH:iip' data-link-field='"+ lineNo +"dtp_input2'><input readonly class='form-control fill' size='16' type='text' value=''><span class='input-group-text input-group-addon' style='border-radius: 0px 5px 5px 0px;border-left:0px;'><span class='glyphicon glyphicon-th'></span></span></div><input type='hidden' id='"+ lineNo +"dtp_input2' name='ARRtime"+ lineNo +"' value='' /></td><td class='pt-1 pb-1'><span data-toggle='tooltip' data-placement='top' title='' data-original-title='The group is arriving the destination'><img src='<?php echo $crm_images; ?>/plane-A.png' width='15' alt='Flight Arrival' /> <input type='radio' name='SupplierTrigger"+ lineNo +"' value='1'></span><span data-toggle='tooltip' data-placement='top' title='' data-original-title='The group is leaving the destination'><img src='<?php echo $crm_images; ?>/plane-D.png' width='15' alt='Flight Departure' /> <input type='radio' name='SupplierTrigger"+ lineNo +"' value='2'></span></td></tr>"; 
		tableBody = $("table.flightstable tbody"); 
		tableBody.append(markup); 
		lineNo++; 
	}); 
}); */
	
$('.DEPtime').datetimepicker({
	weekStart: 1, todayBtn:  1, autoclose: 1, todayHighlight: true, startView: 4, forceParse: 0, minuteStep: 1, showMeridian: 1
});
$('.DEPtime').datetimepicker('setStartDate', '<?php echo date("Y-m-d"); ?>');
$('.ARRtime').datetimepicker({
	weekStart: 1, todayBtn:  1, autoclose: 1, todayHighlight: true, startView: 4, forceParse: 0, minuteStep: 1, showMeridian: 1
});
$('.ARRtime').datetimepicker('setStartDate', '<?php echo date("Y-m-d"); ?>');
</script> 
						<?php // END *********** GROUP FLIGHT INFO SECTION *************** // ?>
						<textarea class="form-control" rows="5" name="flightinfo" id="flightinfo" placeholder="This is a sample flight info only. Please replace it with the correct one!
LH 443 L 15FEB 5 DTWFRA HK45  420P 625A 16FEB  E  LH/TG3IX3
LH 686 L 16FEB 6 FRATLV HK45 1005A 315P 16FEB  E  LH/TG3IX3
LH 691 L 24FEB 7 TLVFRA HK45  520A 900A 24FEB  E  LH/TG3IX3
LH 442 L 24FEB 7 FRADTW HK45 1055A 215P 24FEB  E  LH/TG3IX3"><?php echo $data["Flight_info"]; ?></textarea>
					</div>
					<div class="form-group form-default form-static-label col-sm-12">
						<label class="float-label"><img src="<?php echo $crm_images; ?>/dropbox.jpg" alt="DropBox" /> Dropbox Shared Link for the Tickets</label>
						<?php if($data["Tickets_Dropbox"] != NULL OR $data["Tickets_Dropbox"]!="") { ?>
						<a href="<?php echo $data["Tickets_Dropbox"]; ?>" target="_blank"> - <u>Click here to open the link.</u></a>
						<?php } ?>
						<input type="text" name="dropbox" class="form-control" value="<?php echo $data["Tickets_Dropbox"]; ?>">
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Group Confirmation Number</label>
						<input type="text" name="confirmation" class="form-control" value="<?php echo $data["Confirmation"]; ?>">
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label"># of Seats Reserved</label>
						<input type="text" name="seats" class="form-control" value="<?php echo $data["seats"]; ?>">
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Deposit Date</label>
						<input type="date" name="depositdate" class="form-control" value="<?php echo $data["Deposit_Date"]; ?>">
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Deposit Amount $</label>
						<div class="dollar"><input type="text" name="depositamount" class="form-control" value="<?php echo $data["Deposit_Amount"]; ?>"></div>
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Deposit Return Date</label>
						<input type="date" name="depositreturndate" class="form-control" value="<?php echo $data["Deposit_Return_Date"]; ?>">
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Deposit Returned Amount $</label>
						<div class="dollar"><input type="text" name="returnedamount" class="form-control" value="<?php echo $data["Returned_Amount"]; ?>"></div>
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Cancelation Date</label>
						<input type="date" name="canceldate" class="form-control" value="<?php echo $data["Cancel_Date"]; ?>">
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Names Due Date</label>
						<input type="date" name="namesdue" class="form-control" value="<?php echo $data["Names_Due"]; ?>">
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Final Payment Due</label>
						<input type="date" name="finalpaymentdue" class="form-control" value="<?php echo $data["Final_Payment_Due"]; ?>">
					</div>
                                    <?php if( AclPermission::actionAllowed('AirlineCreditAmount') ) { ?>
					<div class="form-group form-default form-static-label col-sm-4">
					<?php	//Check if this customer has a Sales Order Line for this Ticket Discount or not
					$TicketProductReferenceID = $data["Product_Reference"];
					$CheckTicketSOLsql = "SELECT id,name,price FROM products WHERE id=$TicketProductReferenceID";
					$CheckTicketSOLresult = $db->query($CheckTicketSOLsql);
					$CheckTicketSOLcount = $CheckTicketSOLresult->num_rows;
					if($CheckTicketSOLcount == 1) {$TicketProductAmount = $CheckTicketSOLresult->fetch_assoc();} ?>
					<label class="float-label">Airline Credit Amount $</label>
						<div class="dollar"><input type="text" name="ticketprice" class="form-control" value="<?php echo $TicketProductAmount["price"]; ?>" title="Edit the product directly to modify this number" readonly></div>
					</div>
                                    <?php } ?>
                                    
                                    <?php if( AclPermission::actionAllowed('IsAirlineReady') ) { ?>
                    <div class="col-sm-4">
						<h4 class="sub-title">Is the Airline Ready?</h4>
						<div class="can-toggle">
							<input id="d" name="airlines" value="1" type="checkbox" <?php if($data["Airline_Ready"] == "1") {echo "checked";} ?>>
							<label for="d">
								<div class="can-toggle__switch" data-checked="Done" data-unchecked="Not Yet"></div>
							</label>
						</div>
                    </div>
                                    <?php } ?>
                    <div class="col-sm-12"><br />
                        <?php if( AclPermission::actionAllowed('GroupSave') ) { ?>
					<button type="submit" name="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Update</button>
                        <?php } ?>
					<button type="reset" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Cancel</button>
					</div>
				</form>
				<?php } ?>