<?php
$TotalAccRec = 0;
$ComplimentaryPaxSQL = "SELECT ca.contactid FROM customer_account ca
JOIN customer_credit_note cn
ON cn.Customer_Account_Customer_ID=ca.contactid
WHERE ca.tourid=$tourid AND cn.Full_Discount=1";
$result = $db->query($ComplimentaryPaxSQL);
?>
<div class="col-sm-12 p-0 table-responsive">
<h5 style="margin:30px">Participants - Complimentary</h5>
	<table id="basic-btn" class="table table-hover table-striped table-smallz" data-page-length="50">
		<thead>
			<th>Name</th>
			<th colspan="4">Notifications</th>
			<th>City</th>
			<th>State</th>
			<th>Balance</th>
		</thead>
		<tbody>
			<?php 
			
			if ($result->num_rows > 0) { while($customer_account = $result->fetch_assoc()) { 
				
				$contactid = $customer_account["contactid"];
				$contactsql = "SELECT id,fname,lname,city,state FROM contacts WHERE id=$contactid";
				$contactresult = $db->query($contactsql);
				$contactcustomer = $contactresult->fetch_assoc();
			?>
			<tr>
				<td><a href="contacts-edit.php?id=<?php echo $contactcustomer["id"]; ?>&action=edit&usertype=customer"><?php echo $contactcustomer["title"]." ".$contactcustomer["fname"]." ".$contactcustomer["lname"]; ?></a></td>
				
				<td style="padding-right:0px !important;padding-left:0px !important;padding-bottom: 7px !important;">
					<?php if($customer_account["hisownticket"] == 1) { ?>
						<img src="<?php echo $crm_images; ?>/Airfare.png" alt="Book his/her own ticket" title="This customer booked his own ticket" class="GroupNotifications-ON" />
					<?php } else { ?>
						<img src="<?php echo $crm_images; ?>/Airfare.png" alt="" class="GroupNotifications-OFF" />
					<?php } ?>
				</td>
				<td style="padding-right:0px !important;padding-left:0px !important;padding-bottom: 7px !important;">
					<?php if($customer_account["upgradeproduct"] > 0) { ?>
						<img src="<?php echo $crm_images; ?>/air-upgrade.png" alt="This customer has an Airline Upgrade" title="This customer has an Airline Upgrade" class="GroupNotifications-ON" />
					<?php } else { ?>
						<img src="<?php echo $crm_images; ?>/air-upgrade.png" alt="" class="GroupNotifications-OFF" />
					<?php } ?>
				</td>
				<td style="padding-right:0px !important;padding-left:0px !important;padding-bottom: 7px !important;">
				<?php if($customer_account["upgrade_land_product"] > 0) { ?>
					<img src="<?php echo $crm_images; ?>/land-upgrade.png" alt="This customer has a Land Upgrade" title="This customer has a Land Upgrade" class="GroupNotifications-ON" />
				<?php } else { ?>
					<img src="<?php echo $crm_images; ?>/land-upgrade.png" alt="" class="GroupNotifications-OFF" />
				<?php } ?>
				</td>
				<td style="padding-right:0px !important;padding-left:0px !important;padding-bottom: 7px !important;">
				<?php if($customer_account["travelingwith"] == 999 OR $customer_account["travelingwith2"] == 999) { ?>
					<img src="<?php echo $crm_images; ?>/anonymous.png" alt="The roommate isn't choosen yet!" title="The roommate isn't choosen yet!" class="GroupNotifications-ON" />
				<?php }elseif($customer_account["roomtype"] == "Single") { ?>
					<img src="<?php echo $crm_images; ?>/Single.png" alt="This customer needs a Single Room" title="This customer needs a Single Room" class="GroupNotifications-ON" />
				<?php } else { ?>
					<img src="<?php echo $crm_images; ?>/Room.png" alt="" class="GroupNotifications-OFF" />
				<?php } ?>
				</td>
				
				<td><?php echo $contactcustomer["city"]; ?></td>
				<td>
					<?php
					$stateabbrev = $contactcustomer["state"];
					$statesql = "SELECT * FROM states WHERE abbrev='$stateabbrev'";
					$stateresult = $db->query($statesql);
					$statename = $stateresult->fetch_assoc();
					echo $stateabbrev." <small>(".$statename["name"].")</small>";
					?>
				</td>
				<td><?php 
				//To collect the finanical balance for the customer
				$sql7 = "SELECT SUM(sol.Sales_Amount) AS Total_Sub_Sales,so.Sales_Order_Num FROM sales_order_line sol
						JOIN sales_order so 
						ON sol.Sales_Order_Num = so.Sales_Order_Num
						WHERE so.Customer_Account_Customer_ID=$contactid";
				$result7 = $db->query($sql7);
				$CustomerBalance = $result7->fetch_assoc();
				$CustomerBalanceData = $CustomerBalance['Total_Sub_Sales'];
				$CustomerBalanceSalesOrder = $CustomerBalance['Sales_Order_Num'];
				
				//To see how much this customer has paid so far
				$sql8 = "SELECT Customer_Payment_ID,Customer_Payment_Amount FROM customer_payments WHERE Customer_Account_Customer_ID=$contactid";
				$result8 = $db->query($sql8);
				$TotalPaymentsAmount = 0;
				if ($result8->num_rows > 0) {
				while($row8 = $result8->fetch_assoc()) {
					$TotalPaymentsAmount = $TotalPaymentsAmount+$row8["Customer_Payment_Amount"];
					}
				} else { 
					$TotalPaymentsAmount = 0; 
				}
				
				//To update the Sales Order total for this customer
				$UpdateSOsql = "UPDATE sales_order SET Total_Sale_Price='$CustomerBalanceData' WHERE Sales_Order_Num=$CustomerBalanceSalesOrder;";
				$UpdateSOresult = $db->query($UpdateSOsql);

				echo "<span style='font-weight:bold;'>".GUtils::formatMoney($TotalPaymentsAmount)."</span> / ".GUtils::formatMoney($CustomerBalanceData); ?></td>
			</tr>
			<?php }
			} else { ?>
			<tr>
				<td colspan="8">There are no participants in this group yet!</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>