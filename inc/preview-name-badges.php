<?php include "../session.php";
$PageTitle = "Name Badge";
?>
<style>
#reportsDiv .special-heading{
    color: #dc3545;
}
#reportsDiv #name-badge-print-content table tr td{
    max-height: 288px;
	height: 200px;
    width: 384px;
    border-width: 6px;
    border-style: solid;
    border-color: #342f21;
}
#reportsDiv table tr td{
    max-height: 288px;
	min-height:200px;
    width: 384px;
    border-width: 4px 4px 4px 4px;
    border-style: double;
    border-color: #484747;
    
}
#reportsDiv h5{
    font-size: 20px;
    font-weight: normal;
    line-height: 40px;
    display:block;
}
</style>

<div id="results"></div>
<div class="row">
    <!-- <div class="col-md-1"></div> -->
    <div class="col-md-12" id="name-badge-print-content">
                <h3 class="text-center">Name Badge</h3>
                <a href="inc/download-name-badges.php?id=<?php echo $_REQUEST['id']; ?>" target="_blank" class="btn waves-effect waves-light btn-success" style="float:right;padding: 3px 13px;"><!--<i class="far fa-print"></i>-->Print</a>
                <a href="inc/download-name-badges.php?id=<?php echo $_REQUEST['id']; ?>" download class="btn waves-effect waves-light btn-inverse mr-2" style="float:right;padding: 3px 13px;"><!--<i class="far fa-download"></i>-->Download</a>
                <?php
                $tourid = $_REQUEST['id'];
                $sqlGroup = "SELECT tourname,destination FROM groups WHERE tourid=$tourid";
                $queryGroupRecord = mysqli_query($db, $sqlGroup) or die("error to fetch group data");
                $groupData = mysqli_fetch_row($queryGroupRecord);
                $groupName = $groupData[0];
                $groupDestination = $groupData[1];
                $sqlBadge = "SELECT co.badgename FROM contacts co JOIN customer_account cus ON cus.contactid=co.id WHERE cus.tourid=$tourid AND cus.status=1";
                //echo $sqlBadge;
                $queryBadgeRecords = mysqli_query($db, $sqlBadge) or die("error to fetch badge data");
                //Columns must be a factor of 12 (1,2,3,4,6,12)
                $numOfCols = 2;
                $rowCount = 0;
                $bootstrapColWidth = 12 / $numOfCols;
                ?>
                <div class="table-responsive">
                    <table style="width: 100%;" border="1" cellpadding="10" class="mt-3 mb-3">
                    <tr nobr="true">
                    <?php
                    while( $badgeRow = mysqli_fetch_row($queryBadgeRecords) ) {
                        if(str_word_count($badgeRow[0])==1){
                            $firstName = $badgeRow[0];
                            $lastName = "<br>";
                        }
                        else{
                            $firstName = strstr($badgeRow[0], ' ', true);
                            $lastName = str_replace($firstName, "", $badgeRow[0]);
                        }
                    ?>  
                            <td style="text-align: center;padding: 10px 30px;">
                                <div class="p-4">
                                <h4>Goodshepherd Travel</h4>
                                <h5><?php echo $firstName; ?> <?php echo $lastName; ?></h5>
                                <span><?php echo $groupName; ?></span>
                                </div>
                            </td>
                    <?php
                        $rowCount++;
                        if($rowCount % $numOfCols == 0) echo '</tr><tr nobr="true">';
                    }
                    ?>
                    </tr>
                    </table>
                </div>


        </div>
</div>