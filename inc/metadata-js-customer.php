<script type="text/javascript">
//Show or hide the Flights Section for customer account if select SAME ITINERARY
$(function () {
	$("#Same_Itinerary<?php echo $TheTourID; ?>").click(function () {
		if ($(this).is(":checked")) {
			$("#FlightsTableTitle<?php echo $TheTourID; ?>").hide("slow");
			$("#FlightsTable<?php echo $TheTourID; ?>").hide("slow");
		} else {
			$("#FlightsTableTitle<?php echo $TheTourID; ?>").show("slow");
			$("#FlightsTable<?php echo $TheTourID; ?>").show("slow");
		}
	});
});
//Show or hide the upgrade product selection field for customer account
$(function () {
	$("#upgrade<?php echo $TheTourID; ?>").click(function () {
		if ($(this).is(":checked")) {
			$("#upgradeproductselect<?php echo $TheTourID; ?>").show("slow");
			$("#upgradeproductselect2<?php echo $TheTourID; ?>").show("slow");
		} else {
			$("#upgradeproductselect<?php echo $TheTourID; ?>").hide("slow");
			$("#upgradeproductselect2<?php echo $TheTourID; ?>").hide("slow");
		}
	});
});
//Show or hide the extension product selection field for customer account
$(function () {
	$("#extension<?php echo $TheTourID; ?>").click(function () {
		if ($(this).is(":checked")) {
			$("#extensionproductselect<?php echo $TheTourID; ?>").show("slow");
		} else {
			$("#extensionproductselect<?php echo $TheTourID; ?>").hide("slow");
		}
	});
});
$('#ExtensionProduct<?php echo $TheTourID; ?>').change(function() {
    var opval = $(this).val();
    if(opval=="Add New"){
        $('#ExtensionWindow').modal("show");
    }
});

//Show or hide the insurance product selection field for customer account
$(function () {
	$("#insurance<?php echo $TheTourID; ?>").click(function () {
		if ($(this).is(":checked")) {
			$("#insuranceproductselect<?php echo $TheTourID; ?>").show("slow");
		} else {
			$("#insuranceproductselect<?php echo $TheTourID; ?>").hide("slow");
		}
	});
});
$('#InsuranceProduct<?php echo $TheTourID; ?>').change(function() {
    var opval = $(this).val();
    if(opval=="Add New"){
        $('#InsuranceWindow').modal("show");
    }
});

//Show or hide the transfer product selection field for customer account
$(function () {
	$("#NeedTransfer<?php echo $TheTourID; ?>").click(function () {
		if ($(this).is(":checked")) {
			$("#transferproductselect<?php echo $TheTourID; ?>").show("slow");
		} else {
			$("#transferproductselect<?php echo $TheTourID; ?>").hide("slow");
		}
	});
});
$('#TransferProduct<?php echo $TheTourID; ?>').change(function() {
    var opval = $(this).val();
    if(opval=="Add New"){
        $('#TransferWindow').modal("show");
    }
});

$(function () {
	$("#specialrequest<?php echo $TheTourID; ?>").click(function () {
		if ($(this).is(":checked")) {
			$("#specialrequestfield<?php echo $TheTourID; ?>").show("slow");
			$("#specialrequestfield2<?php echo $TheTourID; ?>").show("slow");
		} else {
			$("#specialrequestfield<?php echo $TheTourID; ?>").hide("slow");
			$("#specialrequestfield2<?php echo $TheTourID; ?>").hide("slow");
		}
	});
});

$(function () {
	$("#mealsrequest<?php echo $TheTourID; ?>").click(function () {
		if ($(this).is(":checked")) {
			$("#mealsrequestfield<?php echo $TheTourID; ?>").show("slow");
			$("#mealsrequestfield2<?php echo $TheTourID; ?>").show("slow");
		} else {
			$("#mealsrequestfield<?php echo $TheTourID; ?>").hide("slow");
			$("#mealsrequestfield2<?php echo $TheTourID; ?>").hide("slow");
		}
	});
});
$(function () {
	$("#hisownticket<?php echo $TheTourID; ?>").click(function () {
		if ($(this).is(":checked")) {
			$("#hisownticketwarning<?php echo $TheTourID; ?>").show("slow");
			$("#hisownticketbox<?php echo $TheTourID; ?>").show("slow");
		} else {
			$("#hisownticketwarning<?php echo $TheTourID; ?>").hide("slow");
			$("#hisownticketbox<?php echo $TheTourID; ?>").hide("slow");
		}
	});
});
$(function () {
	$("#GroupInvoice<?php echo $TheTourID; ?>").click(function () {
		if ($(this).is(":checked")) {
			$("#GroupInvoiceFields<?php echo $TheTourID; ?>").show("slow");
			$("#GroupInvoicePrimary<?php echo $TheTourID; ?>").show("slow");
		} else {
			$("#GroupInvoiceFields<?php echo $TheTourID; ?>").hide("slow");
			$("#GroupInvoicePrimary<?php echo $TheTourID; ?>").hide("slow");
		}
	});
});
function RoomTypeSelect<?php echo $TheTourID; ?>(nameSelect) {
	if(nameSelect){
		admOptionValue0 = document.getElementById("SingleOption<?php echo $TheTourID; ?>").value;
		admOptionValue1 = document.getElementById("DoubleOption<?php echo $TheTourID; ?>").value;
		admOptionValue2 = document.getElementById("TripleOption<?php echo $TheTourID; ?>").value;
		admOptionValue3 = document.getElementById("TwinOption<?php echo $TheTourID; ?>").value;
		if(admOptionValue1 == nameSelect.value){
			document.getElementById("roomtypedouble<?php echo $TheTourID; ?>").style.display = "block";
			document.getElementById("roomtypetriple<?php echo $TheTourID; ?>").style.display = "none";
		} else if(admOptionValue3 == nameSelect.value){
			document.getElementById("roomtypedouble<?php echo $TheTourID; ?>").style.display = "block";
			document.getElementById("roomtypetriple<?php echo $TheTourID; ?>").style.display = "none";
		} else if (admOptionValue2 == nameSelect.value){
			document.getElementById("roomtypedouble<?php echo $TheTourID; ?>").style.display = "block";
			document.getElementById("roomtypetriple<?php echo $TheTourID; ?>").style.display = "block";
		} else if (admOptionValue0 == nameSelect.value){
			document.getElementById("roomtypedouble<?php echo $TheTourID; ?>").style.display = "none";
			document.getElementById("roomtypetriple<?php echo $TheTourID; ?>").style.display = "none";
		}
	} else {
		document.getElementById("roomtypedouble").style.display = "none";
		document.getElementById("roomtypetriple").style.display = "none";
	}
}
</script>
