<style>
.speech-bubble {
	position: relative;
	background: #f1f0f0;
	border-radius: .4em;
}

.speech-bubble:after {
	content: '';
	position: absolute;
	top: 0;
	left: 5%;
	width: 0;
	height: 0;
	border: 18px solid transparent;
	border-bottom-color: #f1f0f0;
	border-top: 0;
	border-left: 0;
	margin-left: -14px;
	margin-top: -18px;
}
.speech-bubble {
    width: 100%;
    padding: 20px;
    color: #000;
    margin: 20px 0;
}
.speech-bubble span{
    font-size: 16px;
}
.note_item{
    margin: 40px 0;
}
#form_notes textarea{
    border: 0;
    padding: 10px;
    background: whitesmoke;
    width:100%;
    height:80px;
    border-radius: 2px;
    border: 1px solid #ccc;
}
#form_notes input[type="submit"]{
    height: 40px;
    width: 110px;
    margin-top:10px;
}
#form_notes{
    margin-bottom: 35px;
}
b, strong {
    font-weight: 600;
}
</style>

<h5>Group Notes</h5>
<br/>
<?php

$editablelClass ='' ;
if( AclPermission::actionAllowed('EditGroupNote', 'group-functions.php') ) {
    $editablelClass = 'acl-editable' ;
}
?>
<div class="row">
    <div class="col-md-12 <?php echo $editablelClass;?>" >
        <form name="contact41" action="inc/group-functions.php" method="POST" id="form_notes">
			<textarea  name="groupnote" placeholder="Type your note here"></textarea>
			<input type="hidden" name="NoteGroupID" value="<?php echo $tourid; ?>">
			<input type="hidden" name="NoteGroupName" value="<?php echo $data["tourname"]; ?>">
			<input type="hidden" name="username" value="<?php echo $_SESSION['login_user']; ?>">
			<button type="submit" name="submit" class="btn waves-effect waves-light btn-success mt-2" data-toggle="modal" data-target="#resultsmodal">Post note</button>
        </form>

    <!--Note items -->
    <div class="notes_container">
    <?php

    $GetNotesSQL = "SELECT username,note,date_time FROM groups_notes WHERE group_id=$tourid ORDER BY date_time DESC";

    $queryNotes = mysqli_query($db, $GetNotesSQL) or die("error to fetch group notes data");

    while( $notesRow = mysqli_fetch_row($queryNotes) ) {
        $date_time = $notesRow[2];

        $date = date("m/d/Y", strtotime($date_time));
        $time = date("h:i a", strtotime($date_time));


        ?>
		<div class="user_meta">
			Note by <strong><?php echo $notesRow[0]; ?></strong> - <small>on <strong><?php echo $date; ?></strong> at <strong><?php echo $time; ?></strong></small>
		</div>
		<div class="speech-bubble">
			<span><?php echo $notesRow[1]; ?></span>
		</div>
        <?php
    }

    if(mysqli_num_rows($queryNotes)==0){
        echo "<div class='empty_notes'>No notes yet.</div>";
    }
    ?>
    </div>        
    <!-- Note items end -->

    </div>
</div>