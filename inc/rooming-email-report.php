<?php
include_once 'vendor/autoload.php';
include_once __DIR__ . '/../models/groups.php' ;
include_once __DIR__ . '/../models/users.php' ;

use PHPMailer\PHPMailer\PHPMailer ;

//Using same pages for receipt and invoice.

//Set a default invoice/receipt.
$SUBJECT = 'Rooming List' ;

//set default file prefix.
$filePrefix = 'Rooming-Report' ;

//set default messages.
$MESSAGE = 'Please find the Rooming List report attachment.' ;

ob_start() ;

$manifestName = "Rooming List";
                           // Passing `true` enables exceptions
try {
	//Recipients
    $groupId = $_REQUEST['id'];
    $group = (new Groups())->get(['tourid' => $groupId]) ;
    $groupName = $group['tourname'] ;

    $groupLeaders = (new Groups())->getGroupLeadersDetail($groupId) ;
    $groupLeaders = [] ;
    if( isset($_REQUEST['tour-leader']) && $_REQUEST['tour-leader'] ) {
        $loggedInUser = (new Users())->getLoggedInUserDetail() ;
    }
    $groupAgents = [] ;
    if( isset($_REQUEST['agent']) && $_REQUEST['agent'] ) {
        $groupAgents = (new Groups())->getGroupAgentsNameList($groupId) ;
    }
    $groupAirlineEmail = [] ;
    if( isset($_REQUEST['airline']) && $_REQUEST['airline'] ) {
        $groupAirlineEmail = (new Groups())->airlineSupplierEmail($groupId) ;
    }
    
    
    //Find BCCs
    $bcc = [] ;
    foreach( $groupLeaders as $g ) {
        $bcc[] = $g['email'] ;
    }
    //Find BCCs
    foreach( $groupAgents as $a ) {
        $bcc[] = $a['email'] ;
    }
    if( $groupAirlineEmail ) {
        $bcc[] = $groupAirlineEmail ;
    }
    //Finc To
    $to = (isset($loggedInUser['EmailAddress']) ? $loggedInUser['EmailAddress'] : '') ;        //Fix Arrays
    if( ! $to ) {
        $to = 'donotreply@goodshepherdtravel.com' ;
    }

	$PDF_OUTPUT = 'S';
	$PDF_HEADER = true ;
	//$pdfData    = include __DIR__ . '/sales-invoice-pdf.php';

	$report_slug = "Rooming List";
	$pdfData    = include 'rooming-pdf-report.php';

	
	if (GUtils::sendMail($to, $SUBJECT, $MESSAGE, $manifestName . '.xlsx', $pdfData, [], [], $bcc))
	{
		//echo "sent";
		GUtils::setSuccess("Email sent successfully");
	}
	else
	{
		//echo "not sent";
		GUtils::setWarning("Error in sending email. Try again later");
	}
	//GUtils::redirect("groups-edit.php");

} catch (Exception $e) {
	//print_r($e);
	GUtils::setWarning("Error in sending email. Try again later");
	//GUtils::redirect("groups-edit.php");
}