<?php

include_once __DIR__ . '/../models/customer_payment.php';
include_once __DIR__ . '/../models/customer_groups.php';
include_once __DIR__ . '/../models/tour_membership.php';

if( ! AclPermission::actionAllowed('TabPax') ) { 
    return;
}

?>
<style type="text/css">
    @media screen and (max-width: 568px) {
	  .flex-direction-media { 
	  	flex-direction: column !important;
	  }
	  .flex-direction-media li a { 
	  	float: none !important;
	  }
	}
</style>

<div class="row">
	<div class="col-12">
		<h4 class="sub-title">Total Participants: <?php echo $paxtotal + $WithoutTicketTotal; ?> pax <small>Detailed Breakdown</small></h4>
                <?php if( AclPermission::actionAllowed('PaxBox') ) { ?>
		<ul class="list-group flex-direction-media" style="flex-direction: row;">
			<li class="list-group-item list-group-item-action list-group-item-light text-center text-dark rounded-0">
				<a href="javascript:void(0)" onclick="ajaxload_pax('with_ticket')">
				<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Participants needs a ticket">
				<img src="<?php echo $crm_path.$crm_images; ?>/pax-ticket-yes.png" width="30" alt="" /><br />
				<span class="badge badge-dark badge-pill mt-2"><?php echo $paxtotal; ?> pax</span></span>
				</a>
			</li>
			<li class="list-group-item list-group-item-action list-group-item-light text-center text-dark rounded-0">
				<a href="javascript:void(0)" onclick="ajaxload_pax('without_ticket')">
				<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Participants booked their own ticket">
				<img src="<?php echo $crm_path.$crm_images; ?>/pax-ticket-no.png" width="30" alt="" /><br />
				<span class="badge badge-dark badge-pill mt-2"><?php echo $WithoutTicketTotal ?> pax</span></span>
				</a>
			</li>
			<li class="list-group-item list-group-item-action list-group-item-light text-center text-dark rounded-0">
				<a href="javascript:void(0)" onclick="ajaxload_pax('single_room')">
				<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Participants ordered Single Supplement">
				<img src="<?php echo $crm_path.$crm_images; ?>/Single.png" width="30" alt="" /><br />
				<span class="badge badge-dark badge-pill mt-2"><?php echo $SingleRoomsTotal; ?> pax</span></span>
				</a>
			</li>
			<li class="list-group-item list-group-item-action list-group-item-light text-center text-dark rounded-0">
				<a href="javascript:void(0)" onclick="ajaxload_pax('air_upgrade')">
				<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Participants requested Airline upgrades">
				<img src="<?php echo $crm_path.$crm_images; ?>/air-upgrade.png" width="30" alt="" /><br />
				<span class="badge badge-dark badge-pill mt-2"><?php echo $AirUpgradePaxTotal; ?> pax</span></span>
				</a>
			</li>
			<li class="list-group-item list-group-item-action list-group-item-light text-center text-dark rounded-0">
				<a href="javascript:void(0)" onclick="ajaxload_pax('land_upgrade')">
				<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Participants requested land upgrades">
				<img src="<?php echo $crm_path.$crm_images; ?>/land-upgrade.png" width="30" alt="" /><br />
				<span class="badge badge-dark badge-pill mt-2"><?php echo $LandUpgradePaxTotal; ?> pax</span></span>
				</a>
			</li>
			<li class="list-group-item list-group-item-action list-group-item-light text-center text-dark rounded-0">
				<a href="javascript:void(0)" onclick="ajaxload_pax('complimentary')">
				<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Complimentary/Free Participants">
				<img src="<?php echo $crm_path.$crm_images; ?>/Guides.png" width="30" alt="" /><br />
				<span class="badge badge-dark badge-pill mt-2"><?php echo $ComplimentaryPaxTotal; ?> pax</span></span>
				</a>
			</li>
			<li class="list-group-item list-group-item-action list-group-item-light text-center text-dark rounded-0">
				<a href="javascript:void(0)" onclick="ajaxload_pax('need_transfer')">
				<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Participants who requested Transfer">
				<img src="<?php echo $crm_path.$crm_images; ?>/transfer.png" width="30" alt="" /><br />
				<span class="badge badge-dark badge-pill mt-2"><?php echo $TransferPaxTotal; ?> pax</span></span>
				</a>
			</li>
			<li class="list-group-item list-group-item-action list-group-item-light text-center text-dark rounded-0">
				<a href="javascript:void(0)" onclick="ajaxload_pax('need_extension')">
				<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Participants who need extension">
				<img src="<?php echo $crm_path.$crm_images; ?>/extension.png" width="30" alt="" /><br />
				<span class="badge badge-dark badge-pill mt-2"><?php echo $ExtensionPaxTotal; ?> pax</span></span>
				</a>
			</li>
			<li class="list-group-item list-group-item-action list-group-item-light text-center text-dark rounded-0" style="margin-bottom: -1px;">
				<a href="javascript:void(0)" onclick="ajaxload_pax('need_insurance')">
				<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Participants who need insurance">
				<img src="<?php echo $crm_path.$crm_images; ?>/insurance-icon.png" width="30" alt="" /><br />
				<span class="badge badge-dark badge-pill mt-2"><?php echo $InsurancePaxTotal; ?> pax</span></span>
				</a>
			</li>
		</ul>
                <?php } ?>
	</div>
</div>
<div class="col-12"><br /></div>
<div class="row">
	<div class="col-sm-4">
		<h4 class="d-none sub-title pb-0 mb-0" style="border-bottom:0px;">Particpiants Details</h4>
	</div>
	<style>
	.RefreshButtonz {
	}
	.MessageInfoz {
		opacity:0;
		border: solid 1px #ccc; 
		border-radius: 10px 0px 0px 10px; 
		padding: 3px 10px 2px 10px;
		transition: all 2s ease;
		cursor: help;
	}
	.RefreshButtonz:hover .MessageInfoz {
		opacity:1;
	}
	</style>
	<div class="col-sm-8 RefreshButtonz">
		<?php if( AclPermission::actionAllowed('GroupSave') ) { ?>
		<form class="float-right" name="contact25" action="inc/group-functions.php" method="POST" id="contact25">
			<input type="text" name="GroupID" value="<?php echo $tourid; ?>" hidden>
			<input type="text" name="AirlineProID" value="<?php echo $data["airline_productid"]; ?>" hidden>
			<input type="text" name="LandProID" value="<?php echo $data["land_productid"]; ?>" hidden>
			<input type="text" name="AirTicketProductID" value="<?php echo $data["Product_Reference"]; ?>" hidden>
			<input type="text" name="AirProductID" value="<?php echo $data["airline_productid"]; ?>" hidden>
			<input type="text" name="LandProductID" value="<?php echo $data["land_productid"]; ?>" hidden>
			<input type="text" name="SingleProductID" value="<?php echo $LandProData["Product_Reference"]; ?>" hidden>
			<input type="text" name="GroupStartDate" value="<?php echo $data["startdate"]; ?>" hidden>
			<input type="text" name="GroupDueDate" value="<?php $stop_date = new DateTime($data["startdate"]); $stop_date->modify('-90 days'); echo $stop_date->format('Y-m-d'); ?>" hidden>
				<button type="submit" name="RefreshCustomers" value="submit-RefreshCustomers" class="btn btn-mat waves-effect waves-light btn-info" style="padding: 6px 13px 7px 13px;" data-toggle="modal" data-target="#resultsmodal">
				<i class="fas fa-sync" style="margin-right:0px"></i>
			</button>
		</form>
		<div class="MessageInfoz float-right">Click here to refresh all the customers' information and their invoice balances</div>
		<?php } ?>
	</div>
</div>

<?php 

$customerInvoice = new CustomerPayment() ;
$paxs = $customerInvoice->paxListing($tourid . ' ' . "", "", "", "") ;

$hasMembers = false ;
foreach( $paxs as $pax ) {
    if( $pax['Account_Status'] != 0 ) {
        $hasMembers = true ;
    }
}
               
?>
<div class="col-sm-12 p-0 table-responsive">
	<table id="PaxFullList" class="datatablefunction table table-hover table-striped table-smallz" data-page-length="50">
		<thead>
			<th>Register Date</th>
			<th>Name</th>
			<th colspan="8" class="text-center">Notifications</th>
			<th class="d-none">City</th>
			<th class="text-center">State</th>
			<th class="text-center">Package Price</th>
			<th class="text-center">Balance</th>
            <?php if( AclPermission::activeRoleId() == AclRole::$ADMIN ) { ?>
			<th><i class="fas fa-cogs"></i></th>
            <?php } ?>
		</thead>
		<tbody>
			<?php 
			$TotalAccRec = 0;
            if( $hasMembers ) {
			foreach( $paxs as $pax ) {
				
                $contactid = $pax['contactid']  ;
                
                //To get the total of the invoice lines for this invoice!
                $InvoiceID = $pax["Customer_Invoice_Num"];
                $InvoiceTotalAmount = $pax['Invoice_Amount_Revised'] ;

                //To get the balance of this invoice minus the total paid
                $balance = $pax['balance'] ;

                $daysPeriod = $pax['trip_start_days'] ;

				//Check if this customer has a Joint Invoice
				$JointInvoiceCheck = $pax["groupinvoice"];
				if($JointInvoiceCheck == 1) {
					$JointInvoiceStyle = "style='font-weight:bold;'";
				} else {
					$JointInvoiceStyle = "";
				}
                $rowStyle = '' ;
                if( $pax['Account_Status'] != 1 ) {
                    $rowStyle = "text-decoration: line-through; " ;
                    continue;
                }
                //do not deduct credit amount from total while calculating refund amount. use $pax["total"] instead of $totalPaymentAmount
                if($JointInvoiceCheck == 1 && $pax['Additional_Invoice'] ) {
                    $cancellationInfo = CustomerRefund::calculateRefund($pax['Invoice_Amount_Revised'], $pax["charges"], $pax["total"], $daysPeriod, $pax['Cancellation_Charge_Others'], $pax['Customer_Invoice_Num']) ;
                }
                else {
                    $cancellationInfo = CustomerRefund::calculateRefund($pax['Invoice_Amount_Revised'], $pax["charges"], $pax["total"], $daysPeriod, $pax['Cancellation_Charge_Others'], $pax['Customer_Invoice_Num']) ;
                }
			?>
            <tr style="<?php echo $rowStyle;?>" >
            	<?php 
					$altTitle = '';
					$warningCount = 0;
                    $bgWarning = '' ;
					if($pax["Passport_Expiry_Date"] != '0000-00-00' && strtotime($pax["Passport_Expiry_Date"]) > 0 ) {
						$tourEndDate = date('d-m-Y', strtotime($pax["enddate"]));
						$passportExpiryDate = date('d-m-Y', strtotime($pax["Passport_Expiry_Date"]));
						$date1 = new DateTime($tourEndDate);
						$date2 = new DateTime($passportExpiryDate);
						$diff = $date1->diff($date2);
						$TotalDiff = (($diff->y)*365) + (($diff->m)*30) + $diff->d;
								
						if ($TotalDiff <= 180) {
							$altTitle = 'Passport Expired';
							$warningCount++;
                            $bgWarning = 'bg-danger' ;
						}
					}
                    else if(strtoupper($pax["passport"]) == "PASSPORT PENDING") {
                        $altTitle = 'Passport Pending';
                        $warningCount++;
                        $bgWarning = 'bg-warning' ;
                    }
				?>
				<td style="font-size: 13px;"><?php echo date('m/d/Y', strtotime($pax["createtime"])); ?></td>
				<td>
                    <?php if( AclPermission::actionAllowed('Contact') ) { ?>
                    <a <?php echo $JointInvoiceStyle; ?> href="contacts-edit.php?id=<?php echo $pax["id"]; ?>&action=edit&usertype=customer">
                    <?php } ?>
                        <?php echo $pax["title"]." ".$pax["fname"]." ".$pax["mname"]." ".$pax["lname"]; ?>
                    <?php if( AclPermission::actionAllowed('Contact') ) { ?>
                    </a>&nbsp;
                    <?php }?>
                    <span data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php echo $altTitle; ?>">
                        <label class="badge <?php echo $bgWarning;?> ml-1"><?php if ($warningCount > 0) { echo $warningCount; } ?></label>
                    </span> 
                            <?php if($pax["Account_Status"] == 0) { echo '<i class="fas fa-ban text-danger ml-2" title="This customer is DISABLED! To enable, enter his/her edit page."></i>'; } ?>
                </td>

				<td style="padding-right:0px !important;padding-left:0px !important;padding-bottom: 7px !important;">
					<?php if($pax["hisownticket"] == 1) { ?>
						<img src="<?php echo $crm_images; ?>/Airfare.png" alt="Book his/her own ticket" title="This customer booked his own ticket" class="GroupNotifications-ON" />
					<?php } else { ?>
						<img src="<?php echo $crm_images; ?>/Airfare.png" alt="" class="GroupNotifications-OFF" />
					<?php } ?>
				</td>
				<td style="padding-right:0px !important;padding-left:0px !important;padding-bottom: 7px !important;">
					<?php if($pax["upgradeproduct"] > 0) { ?>
						<img src="<?php echo $crm_images; ?>/air-upgrade.png" alt="This customer has an Airline Upgrade" title="This customer has an Airline Upgrade" class="GroupNotifications-ON" />
					<?php } else { ?>
						<img src="<?php echo $crm_images; ?>/air-upgrade.png" alt="" class="GroupNotifications-OFF" />
					<?php } ?>
				</td>
				<td style="padding-right:0px !important;padding-left:0px !important;padding-bottom: 7px !important;">
				<?php if($pax["upgrade_land_product"] > 0) { ?>
					<img src="<?php echo $crm_images; ?>/land-upgrade.png" alt="This customer has a Land Upgrade" title="This customer has a Land Upgrade" class="GroupNotifications-ON" />
				<?php } else { ?>
					<img src="<?php echo $crm_images; ?>/land-upgrade.png" alt="" class="GroupNotifications-OFF" />
				<?php } ?>
				</td>
				<td style="padding-right:0px !important;padding-left:0px !important;padding-bottom: 7px !important;">
				<?php if($pax["travelingwith"] == 999 OR $pax["travelingwith2"] == 999) { ?>
					<img src="<?php echo $crm_images; ?>/anonymous.png" alt="The roommate isn't choosen yet!" title="The roommate isn't choosen yet!" class="GroupNotifications-ON" />
				<?php }elseif($pax["roomtype"] == "Single") { ?>
					<img src="<?php echo $crm_images; ?>/Single.png" alt="This customer needs a Single Room" title="This customer needs a Single Room" class="GroupNotifications-ON" />
				<?php } else { ?>
					<img src="<?php echo $crm_images; ?>/Room.png" alt="" class="GroupNotifications-OFF" />
				<?php } ?>
				</td>
				<td style="padding-right:0px !important;padding-left:0px !important;padding-bottom: 7px !important;">
				<?php if($pax["TourLeaderDiscount"] == 1) { ?>
					<img src="<?php echo $crm_images; ?>/Guides.png" alt="This customer has a full trip discount" title="This customer has a full trip discount" class="GroupNotifications-ON" />
				<?php } else { ?>
					<img src="<?php echo $crm_images; ?>/Guides.png" alt="" class="GroupNotifications-OFF" />
				<?php } ?>
				</td>
				<td style="padding-right:0px !important;padding-left:3px !important;padding-bottom: 7px !important;">
				<?php if($pax["Transfer"] == 1) { ?>
					<img src="<?php echo $crm_images; ?>/transfer.png" alt="This customer has requested a transfer" title="This customer has requested a transfer" class="GroupNotifications-ON" />
				<?php } else { ?>
					<img src="<?php echo $crm_images; ?>/transfer.png" alt="" class="GroupNotifications-OFF" />
				<?php } ?>
				</td>
				<td style="padding-right:0px !important;padding-left:3px !important;padding-bottom: 7px !important;">
				<?php if($pax["Extension"] == 1) { ?>
					<img src="<?php echo $crm_images; ?>/extension.png" alt="This customer has an extension" title="This customer has an extension" class="GroupNotifications-ON" />
				<?php } else { ?>
					<img src="<?php echo $crm_images; ?>/extension.png" alt="" class="GroupNotifications-OFF" />
				<?php } ?>
				</td>
				<td style="padding-right:0px !important;padding-left:3px !important;padding-bottom: 7px !important;">
				<?php if($pax["insurance"] == 1) { ?>
					<img src="<?php echo $crm_images; ?>/insurance-icon.png" alt="This customer has an insurance" title="This customer has an insurance" class="GroupNotifications-ON" />
				<?php } else { ?>
					<img src="<?php echo $crm_images; ?>/insurance-icon.png" alt="" class="GroupNotifications-OFF" />
				<?php } ?>
				</td>
				
				
				<td class="d-none"><?php echo $pax["city"]; ?></td>
				<td class="text-center" style="font-size: 13px;">
					<?php
					$stateabbrev = $pax["state"];
					$statesql = "SELECT * FROM states WHERE abbrev='$stateabbrev'";
					$stateresult = $db->query($statesql);
					$statename = $stateresult->fetch_assoc();
					//echo $stateabbrev." <small>(".$statename["name"].")</small>";
					echo $stateabbrev;
					?>
				</td>
				<?php 
				//To collect the finanical balance for the customer
				$sql7 = "SELECT SUM(sol.Sales_Amount) AS Total_Sub_Sales,so.Sales_Order_Num FROM sales_order_line sol
						JOIN sales_order so 
						ON sol.Sales_Order_Num = so.Sales_Order_Num
						WHERE so.Customer_Account_Customer_ID=$contactid";
				$result7 = $db->query($sql7);
				$CustomerBalance = $result7->fetch_assoc();
				$CustomerBalanceData = $CustomerBalance['Total_Sub_Sales'];
				$CustomerBalanceSalesOrder = $CustomerBalance['Sales_Order_Num'];
                
                $primaryCustomer = (new CustomerGroups())->isPrimaryTraveler($tourid, $contactid) ;
                $additionalMembers = [] ;
                if( $primaryCustomer ) {
                    $additionalMembers = (new CustomerGroups())->getAdditionalCustomerIds($tourid, $contactid, false) ;
                    if( !is_array($additionalMembers)) {
                        $additionalMembers = [] ;
                    }
                }
                $additionalCount = count($additionalMembers) ;
                
                $refundRedClass = '' ;
                if( $balance < 0 ) {
                    $refundRedClass = 'text-danger';
                }

				//To update the Sales Order total for this customer
				$UpdateSOsql = "UPDATE sales_order SET Total_Sale_Price='$CustomerBalanceData' WHERE Sales_Order_Num=$CustomerBalanceSalesOrder;";
				$UpdateSOresult = $db->query($UpdateSOsql); ?>
				<?php if($pax["Invoice_Status"] == 2 AND $pax["Additional_Invoice"] == 1) { ?>
					<td class="text-center" style="font-size: 13px;"><strong>Joint</strong></td>				
				<?php } else { ?>
					<td class="text-center" style="font-size: 13px;"><?php echo GUtils::formatMoney( floatval($InvoiceTotalAmount)); ?></td>				
                <?php } ?>
				<?php if($pax["Invoice_Status"] == 2 AND $pax["Additional_Invoice"] == 1) { ?>
					<td class="text-center" style="font-size: 13px;"><strong>-</strong></td>
				<?php } else { ?>
					<td class="text-center" style="font-size: 13px;"><?php echo "<span class='$refundRedClass' style='font-weight:bold;'>".GUtils::formatMoney($balance)."</span>";?></td>
                <?php } ?>
				
                <?php if( AclPermission::activeRoleId() == AclRole::$ADMIN ) { ?>
                    <td>
					<?php 
                    if($JointInvoiceCheck == 1) {
                    
                    ?>
					<a href="javascript:void(0);" style="opacity:0.5;cursor: default;" title="This customer is in Joint Invoice"><i class="fas fa-trash"></i></a>
					<?php } else {
                        ?>
                    <a title="Delete Customer" href="javascript:void(0);" onclick = 'deleteConfirm( "groups-edit.php?id=<?php echo $tourid; ?>&action=deleteCustomer&customerID=<?php echo $contactid; ?>&SalesOrderNum=<?php echo $CustomerBalanceSalesOrder; ?>", "Delete Customer")' role="button"><i class="fas fa-trash"></i></a>
                    <?php } ?>
                    <?php
                    if( $balance < 0 ) {
                    ?>
                    <a title="Issue Refund" href="javascript:void(0);" onclick="window.location.href='refund_receipt_add.php?invid=<?php echo $pax["Customer_Invoice_Num"]; ?>';"
                            >
                        <i class="fas fa-retweet"></i>
                    </a>

                    <?php
                    }
                    ?>
                            <?php 
                            
        
                            $p = false ;
                            if( $pax['Account_Status'] != 0 ) {
                                
                                //disable for primary customers.
                                $dim = '' ;
                                $title = 'Cancel Customer From Trip' ;
                            /* if( $primaryCustomer ) {
                                if( count($additionalMembers) > 0 ) {
                                    $p = true ;
                                    $dim = 'style="opacity:0.5;cursor: default;"' ;
                                }
                                $title = 'A Primary Traveller of Joint Invoice' ;
                            }
                             */
                                ?>
                        <a title="<?php echo $title;?>" <?php echo $dim; ?> href="javascript:void(0);" 
                           data-name="<?php echo $pax["title"]." ".$pax["fname"]." ".$pax["mname"]." ".$pax["lname"]; ?>" 
                           data-id="<?php echo $pax["Customer_Invoice_Num"]; ?>"
                           data-tourid="<?php echo $tourid; ?>"
                            data-redirect-url="groups-edit.php?id=<?php echo $tourid; ?>&action=edit&tab=participants" 
                            data-cancel-amount="<?php echo GUtils::formatMoney( $cancellationInfo['Cancellation_Charge']);?>" 
                            data-cancel-amount-raw="<?php echo $cancellationInfo['Cancellation_Charge'] ;?>" 
                            data-cancel-refund="<?php echo GUtils::formatMoney( $cancellationInfo['Refund_Amount']) ;?>" 
                            data-cancel-refund-raw="<?php echo $cancellationInfo['Refund_Amount'] ;?>" 
                            data-cancel-outcome="<?php echo $cancellationInfo['Cancellation_Outcome'];?>" 
                            data-cancel-paid="<?php echo ($pax['payment_amount']);?>" 
                            data-cancel-primary="<?php echo($primaryCustomer ? 1 : 0);?>" 
                            data-cancel-additionals="<?php echo $additionalCount ;?>" 
                            data-cancel-options="<?php echo base64_encode( (new CustomerGroups())->getJointMemberOptions($tourid, $contactid) );?>"
                            <?php if( ! $p ) { ?>
                                onclick = 'removeTripCustomerHandler(this)' 
                            <?php } ?>
                            ><i class="fas fa-window-close"></i>
                        </a>
                        
					<?php } ?>
				</td>
                
                <?php } ?>
			</tr>
			<?php }
			} else { ?>
			<tr>
				<td colspan="15" align="center">There are no participants in this group yet!</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>

<?php

$hasCanceled = false ;
foreach( $paxs as $pax ) {
                if( $pax['Account_Status'] == 0 ) {
                    $hasCanceled = true ;
                }
            }
            
if( $hasCanceled ) {
?>
<br/>
<br/>

<div class="row">
	<div class="col-sm-4">
		<h4 class="sub-title text-danger" style="border-bottom:0px;">Canceled Participants</h4>
	</div>
</div>    
<div class="col-sm-12 p-0 table-responsive">
	<table class="datatablefunction table table-hover table-striped table-smallz dataTable no-footer" data-page-length="50">
		<thead>
			<th>Canceled Date</th>
			<th>Name</th>
			<th colspan="4">Notifications</th>
			<th class="d-none">City</th>
			<th class="text-center">State</th>
			<th class="text-center">Package Price</th>
			<th class="text-center">Balance</th>
            <?php if( AclPermission::activeRoleId() == AclRole::$ADMIN ) { ?>
			<th><i class="fas fa-cogs"></i></th>
            <?php } ?>
		</thead>
		<tbody>
			<?php 
            
			$TotalAccRec = 0;            
            if( is_array($paxs) && count($paxs) > 0 ) {
			foreach( $paxs as $pax ) {
			
                if( $pax["Customer_Invoice_Num"] == 192667 ) {
            $x = 10 ;
        }
                $contactid = $pax['contactid']  ;
                
                //To get the total of the invoice lines for this invoice!
                $InvoiceID = $pax["Customer_Invoice_Num"];
                $InvoiceTotalAmount = $pax['Invoice_Amount_Revised'] ;

                //To get the balance of this invoice minus the total paid
                $balance = $pax['balance'] ;

                $daysPeriod = $pax['trip_start_days'] ;

				//Check if this customer has a Joint Invoice
				$JointInvoiceCheck = $pax["groupinvoice"];
				if($JointInvoiceCheck == 1) {
					$JointInvoiceStyle = "style='font-weight:bold;'";
				} else {
					$JointInvoiceStyle = "";
				}
                $rowStyle = '' ;
                if( $pax['Account_Status'] == 0 ) {
                    $rowStyle = "" ;//text-decoration: line-through; " ;
                }
                else {
                    continue;
                }
                //do not deduct credit amount from total while calculating refund amount. use $pax["total"] instead of $totalPaymentAmount
                if($JointInvoiceCheck == 1 && $pax['Additional_Invoice'] ) {
                    $cancellationInfo = CustomerRefund::calculateRefund($pax['Invoice_Amount'], $pax["charges"], $pax["total"], $daysPeriod, $pax['Cancellation_Charge_Others'], $pax['Customer_Invoice_Num']) ;
                }
                else {
                    $cancellationInfo = CustomerRefund::calculateRefund($pax['Invoice_Amount_Revised'], $pax["charges"], $pax["total"], $daysPeriod, $pax['Cancellation_Charge_Others'], $pax['Customer_Invoice_Num']) ;
                }
			?>
            <tr >
				<td style="font-size: 13px;"><?php echo date('m/d/Y', strtotime($pax["Canceled_On"])); ?></td>
				<td style="<?php echo $rowStyle;?>" >
                    <?php if( AclPermission::actionAllowed('Contact') ) { ?>
                    <a <?php echo $JointInvoiceStyle; ?> href="contacts-edit.php?id=<?php echo $pax["id"]; ?>&action=edit&usertype=customer">
                    <?php } ?>
                        <?php echo $pax["title"]." ".$pax["fname"]." ".$pax["mname"]." ".$pax["lname"]; ?>
                        <?php if( AclPermission::actionAllowed('Contact') ) { ?>
                    </a>
                        <?php } ?>
                </td>
				
				<td style="padding-right:0px !important;padding-left:0px !important;padding-bottom: 7px !important;">
					<?php if($pax["hisownticket"] == 1) { ?>
						<img src="<?php echo $crm_images; ?>/Airfare.png" alt="Book his/her own ticket" title="This customer booked his own ticket" class="GroupNotifications-ON" />
					<?php } else { ?>
						<img src="<?php echo $crm_images; ?>/Airfare.png" alt="" class="GroupNotifications-OFF" />
					<?php } ?>
				</td>
				<td style="padding-right:0px !important;padding-left:0px !important;padding-bottom: 7px !important;">
					<?php if($pax["upgradeproduct"] > 0) { ?>
						<img src="<?php echo $crm_images; ?>/air-upgrade.png" alt="This customer has an Airline Upgrade" title="This customer has an Airline Upgrade" class="GroupNotifications-ON" />
					<?php } else { ?>
						<img src="<?php echo $crm_images; ?>/air-upgrade.png" alt="" class="GroupNotifications-OFF" />
					<?php } ?>
				</td>
				<td style="padding-right:0px !important;padding-left:0px !important;padding-bottom: 7px !important;">
				<?php if($pax["upgrade_land_product"] > 0) { ?>
					<img src="<?php echo $crm_images; ?>/land-upgrade.png" alt="This customer has a Land Upgrade" title="This customer has a Land Upgrade" class="GroupNotifications-ON" />
				<?php } else { ?>
					<img src="<?php echo $crm_images; ?>/land-upgrade.png" alt="" class="GroupNotifications-OFF" />
				<?php } ?>
				</td>
				<td style="padding-right:0px !important;padding-left:0px !important;padding-bottom: 7px !important;">
				<?php if($pax["travelingwith"] == 999 OR $pax["travelingwith2"] == 999) { ?>
					<img src="<?php echo $crm_images; ?>/anonymous.png" alt="The roommate isn't choosen yet!" title="The roommate isn't choosen yet!" class="GroupNotifications-ON" />
				<?php }elseif($pax["roomtype"] == "Single") { ?>
					<img src="<?php echo $crm_images; ?>/Single.png" alt="This customer needs a Single Room" title="This customer needs a Single Room" class="GroupNotifications-ON" />
				<?php } else { ?>
					<img src="<?php echo $crm_images; ?>/Room.png" alt="" class="GroupNotifications-OFF" />
				<?php } ?>
				</td>
				
				<td class="d-none"><?php echo $pax["city"]; ?></td>
				<td style="font-size: 13px;">
					<?php
					$stateabbrev = $pax["state"];
					$statesql = "SELECT * FROM states WHERE abbrev='$stateabbrev'";
					$stateresult = $db->query($statesql);
					$statename = $stateresult->fetch_assoc();
					echo $stateabbrev." <small>(".$statename["name"].")</small>";
					?>
				</td>
				<?php 
				//To collect the finanical balance for the customer
				$sql7 = "SELECT SUM(sol.Sales_Amount) AS Total_Sub_Sales,so.Sales_Order_Num FROM sales_order_line sol
						JOIN sales_order so 
						ON sol.Sales_Order_Num = so.Sales_Order_Num
						WHERE so.Customer_Account_Customer_ID=$contactid";
				$result7 = $db->query($sql7);
				$CustomerBalance = $result7->fetch_assoc();
				$CustomerBalanceData = $CustomerBalance['Total_Sub_Sales'];
				$CustomerBalanceSalesOrder = $CustomerBalance['Sales_Order_Num'];
				
				//To update the Sales Order total for this customer
				$UpdateSOsql = "UPDATE sales_order SET Total_Sale_Price='$CustomerBalanceData' WHERE Sales_Order_Num=$CustomerBalanceSalesOrder;";
				$UpdateSOresult = $db->query($UpdateSOsql);
                
                $primaryCustomer = (new CustomerGroups())->isPrimaryTraveler($tourid, $contactid) ;


                $refundRedClass = '' ;
                if( $balance < 0 ) {
                    $refundRedClass = 'text-danger';
                }
                ?>
                <td class="text-center">
                    <?php echo GUtils::formatMoney( floatval($InvoiceTotalAmount)); ?>
                </td>
                <td  class="text-center" style="font-size: 13px;">
                    <?php echo "<span class='$refundRedClass' style='font-weight:bold;'>".GUtils::formatMoney($balance)."</span>";?>
                </td>
                <?php if( AclPermission::activeRoleId() == AclRole::$ADMIN ) { ?>
                <td>
					<?php if($JointInvoiceCheck == 1) { ?>
					<a href="javascript:void(0);" style="opacity:0.5;cursor: default;" title="This customer is in Joint Invoice"><i class="fas fa-trash"></i></a>
					<?php } else {
                        ?>
                    <a title="Delete Customer" href="javascript:void(0);" onclick = 'deleteConfirm( "groups-edit.php?id=<?php echo $tourid; ?>&action=deleteCustomer&customerID=<?php echo $contactid; ?>&SalesOrderNum=<?php echo $CustomerBalanceSalesOrder; ?>", "Delete Customer")' role="button"><i class="fas fa-trash"></i></a>
                    <?php } 
                    
                    if( $pax['Invoice_Status'] == CustomerInvoice::$CANCELLED && floatval($pax['Refund_Amount']) > 0 && ( $pax['Refund_Status'] == 1 || $pax['Refund_Status'] == 3) ) {
                    ?>
                    <a title="Issue Refund" href="javascript:void(0);" onclick="window.location.href='refund_receipt_add.php?invid=<?php echo $pax["Customer_Invoice_Num"]; ?>';"
                            >
                        <i class="fas fa-retweet"></i>
                    </a>

                    <?php
                    }
                    if( $pax['Invoice_Status'] == CustomerInvoice::$CANCELLED && $pax['Refund_Status'] == CustomerRefund::$CREATED ) {
                    ?>
                    <a title="Undo Cancellation" href="javascript:void(0);" 
                           data-name="<?php echo $pax["title"]." ".$pax["fname"]." ".$pax["mname"]." ".$pax["lname"]; ?>" 
                           data-id="<?php echo $pax["Customer_Invoice_Num"]; ?>"
                           data-tourid="<?php echo $tourid; ?>"
                            data-cancel-amount="<?php echo GUtils::formatMoney( $cancellationInfo['Cancellation_Charge']);?>" 
                            data-cancel-refund="<?php echo GUtils::formatMoney( $cancellationInfo['Refund_Amount']) ;?>" 
                            data-cancel-outcome="<?php echo $cancellationInfo['Cancellation_Outcome'];?>" 
                            onclick = 'restoreTripCustomerHandler(this)' ><i class="fas fa-undo"></i>
                        </a>
                    <?php                    
                    }
                    ?>
				</td>
                <?php } ?>
			</tr>
			<?php }
			} else { ?>
			<tr>
				<td colspan="9">There are no participants in this group yet!</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>

<?php
}
    $GLOBAL_SCRIPT .= "$(document).ready(function() {
		$('table.datatablefunction').DataTable( {'paging':   false,'ordering': false,'info':     false} );
		} );";
?>
