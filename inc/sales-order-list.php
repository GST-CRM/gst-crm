<?php
//include connection file 
include "../config.php";
    
include_once __DIR__ . '/../models/acl_permission.php';

// initilize all variable
$params = $columns = $totalRecords = $data = array();
$params = $_REQUEST;
$TheStatus = $_REQUEST['status'];
$FromDate = $_REQUEST['FromDate'];
$UntilDate = $_REQUEST['UntilDate'];
$ShowGroups = $_REQUEST['ShowGroups'];

//define index of column
$columns = array( 
	0 => 'Sales_Order_Num',
	1 => 'Sales_Order_Date',
	2 => 'Customer_Account_Customer_ID', 
	3 => 'HasInvoice',
	4 => 'title',
	5 => 'fname',
	6 => 'lname',
	7 => 'SalesAmount',
	8 => 'Customer_Invoice_Num',
	9 => 'Sales_Order_Stop',
	10 => 'Group_ID'
);

$where = $sqlTot = $sqlRec = "";

// check search value exist
if( !empty($params['search']['value']) ) {   
	$theSearchText = $params['search']['value'];
	$theSearchText2 = str_replace(" ","%",$theSearchText);
	$where .= " AND concat(co.fname, co.lname) LIKE '%".$theSearchText2."%'";
}

$WhereFlags = "";

if($TheStatus == "active") {
	$WhereFlags .= " AND so.Sales_Order_Stop=0 ";
} elseif($TheStatus == "voided") { 
	$WhereFlags .= " AND so.Sales_Order_Stop=1 ";
} elseif($TheStatus == "all") { 
	$WhereFlags .= " AND so.Sales_Order_Stop=0 ";
}

if($FromDate != "0000-00-00") {
	$WhereFlags .= " AND so.Sales_Order_Date >= '$FromDate' ";
} else { 
	$WhereFlags .= " ";
}

if($UntilDate != "0000-00-00") {
	$WhereFlags .= " AND so.Sales_Order_Date <= '$UntilDate' ";
} else { 
	$WhereFlags .= " ";
}

if($ShowGroups == "show") {
	$WhereFlags .= " ";
} else { 
	$WhereFlags .= " AND so.Group_ID=0 ";
}

// getting total number records without any search
$sql = "SELECT so.Sales_Order_Num,
		DATE_FORMAT(so.Sales_Order_Date, '%m/%d/%Y') AS Sales_Order_Date,
		so.Customer_Account_Customer_ID,
		IF(ci.Customer_Invoice_Num > 0, 'Yes', 'Not Yet') as HasInvoice,
		co.title,
		co.fname,
		co.lname,
		so.Total_Sale_Price AS SalesAmount,
		ci.Customer_Invoice_Num,
		so.Sales_Order_Stop,
		so.Group_ID
	FROM sales_order so
	JOIN contacts co 
		ON so.Customer_Account_Customer_ID=co.id
	LEFT JOIN customer_invoice ci
		ON ci.Customer_Order_Num=so.Sales_Order_Num
	WHERE 1 $WhereFlags $where
	GROUP BY so.Sales_Order_Num ";
/*
//This section is commented because it include the flag of GROUPS, must confirm with Sary before deleting this section!!!!
$sql = "SELECT so.Sales_Order_Num,
		DATE_FORMAT(so.Sales_Order_Date, '%m/%d/%Y') AS Sales_Order_Date,
		so.Customer_Account_Customer_ID,
		IF(ci.Customer_Invoice_Num > 0, 'Yes', 'Not Yet') as HasInvoice,
		co.title,
		co.fname,
		co.lname,
		gr.tourid,
		gr.tourdesc,
		so.Total_Sale_Price AS SalesAmount,
		ci.Customer_Invoice_Num
	FROM sales_order so
	JOIN contacts co 
		ON so.Customer_Account_Customer_ID=co.id
	JOIN groups gr 
		ON so.Group_ID=gr.tourid
	LEFT JOIN customer_invoice ci
		ON ci.Customer_Order_Num=so.Sales_Order_Num
	WHERE so.Group_ID=0 AND so.Sales_Order_Stop=0
	GROUP BY so.Sales_Order_Num ";*/
	$sqlTot .= $sql;
	$sqlRec .= $sql;


 	$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";

	$queryTot = mysqli_query($db, $sqlTot) or die("database error:". mysqli_error($db));


	$totalRecords = mysqli_num_rows($queryTot);

	$queryRecords = mysqli_query($db, $sqlRec) or die("error to fetch Sales Order data");

	//iterate on results row and create new index array of data
	while( $row = mysqli_fetch_row($queryRecords) ) { 
		$data[] = $row;
	}	

	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>
