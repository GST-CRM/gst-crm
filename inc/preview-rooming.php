<?php 
include "../session.php";
include_once __DIR__ . '/../models/groups.php' ;
include_once __DIR__ . '/../models/customer_invoice.php' ;

$PageTitle      = "Rooming List";
$groupname      = $_REQUEST["tourname"];
$groupid        = $_REQUEST["groupid"];

$query          = (new Groups())->getRoomingListQuery($groupid) ;

$result         = $db->query($query); 
$heading        = $groupname." - #".$groupid;
?>
<style>
#reportsDiv .special-heading{
    color: #dc3545;
}

#reportsDiv .main_td{
    border-width: 4px;
    border-style: solid;
    border-color: #342f21;
    height: 288px;
}
#reportsDiv h5{
    font-size: 20px;
    font-weight: normal;
    line-height: 60px;
    display:block;
}
</style>

<div id="results"></div>
<div class="row">
    <div id="roomingDiv" class="col-md-12">
                <h3 class="text-center">Rooming List</h3>
                <a id="roomingPrintBtn" href="javascript:void(0)" class="btn waves-effect waves-light btn-success" style="float:right;padding: 3px 13px;" data-gname ="<?php echo mysqli_real_escape_string($db, $groupname); ?>" data-gid="<?php echo $groupid; ?>">Print</a>
                <!-- <a id="roomingPrintBtn" href="excel.php?type=rooming&name=<?php echo $groupname; ?>&groupid=<?php echo $groupid; ?>" target="_blank" class="btn waves-effect waves-light btn-success" style="margin-right: 30px;float:right;padding: 3px 13px;" data-gname ="<?php echo $groupname; ?>" data-gid="<?php echo $groupid; ?>">Print</a> -->
                <a href="javascript:void(0)" download class="btn waves-effect waves-light btn-inverse mr-2" style="float:right;padding: 3px 13px;" onclick="download_report('<?php echo mysqli_real_escape_string($db, $groupname); ?>','<?php echo $groupid; ?>')">Download</a>
                <a href="javascript:void(0);" onclick = "return emailPDFconfirmRooming();" class="btn waves-effect waves-light btn-inverse mr-2" style="float:right;padding: 3px 13px;">Email pdf</a>
                <br />
                <br />
				<div class="dt-responsive table-responsive">
                    <table id="no-btn" class="table table-hover table-striped table-bordered dataTable no-footer nowrap responsive" data-page-length="100">
                        <thead>
                            <tr>
                                <th>Room</th>
                                <th>Room Type</th>
                                <th>Person #1</th>
                                <th>Person #2</th>
                                <th>Person #3</th>
                                <th>Need Transfer?</th>
                                <th>Need Extension?</th>
                                <th>Special Land Requests</th>
                                <th>Meals Requests</th>
                                <th>Allergies Requests</th>
                            </tr>
                        </thead>
                        <tbody>
                                <?php
                                if ($result->num_rows > 0) 
                                { 
                                    $i = 1;
                                    while($row = $result->fetch_assoc()) 
                                    { 
                                        $roomType           = $row['roomtype'];       
                                        $person1            = $row['title']." ".$row['fname']." ".$row['mname']." ".$row['lname'];
                                        $person2ID          = $row['Roommate1ID'];
										if($person2ID != NULL) {
											//To get the product details if this roommate has transfer product
											$Person2TransferSQL = 	"SELECT ca.Transfer, pro.description FROM products pro
																	JOIN customer_account ca ON ca.contactid=$person2ID AND ca.tourid=$groupid
																	WHERE id=ca.Transfer_Product_ID;";
											$Person2TransferResult = $db->query($Person2TransferSQL);
											$Person2TransferData = $Person2TransferResult->fetch_assoc();
											if($Person2TransferData['Transfer']==1) {
												$Person2Transfer = $Person2TransferData['description'];
											} else {
												$Person2Transfer = NULL;
											};
											//To get the product details if this roommate has extension product
											$Person2ExtSQL = 	"SELECT ca.Extension, pro.description FROM products pro
																JOIN customer_account ca ON ca.contactid=$person2ID AND ca.tourid=$groupid
																WHERE id=ca.Extension_Product_ID;";
											$Person2ExtResult = $db->query($Person2ExtSQL);
											$Person2ExtData = $Person2ExtResult->fetch_assoc();
											if($Person2ExtData['Extension']==1) {
												$Person2Extension = $Person2ExtData['description'];
											} else {
												$Person2Extension = NULL;
											};
										}
                                        $person2            = $row['Roommate1'];
                                        $person3ID          = $row['Roommate2ID'];
										if($person3ID != NULL) {
											//To get the product details if this roommate has transfer product
											$Person3TransferSQL = 	"SELECT ca.Transfer, pro.description FROM products pro
																	JOIN customer_account ca ON ca.contactid=$person3ID AND ca.tourid=$groupid
																	WHERE id=ca.Transfer_Product_ID;";
											$Person3TransferResult = $db->query($Person3TransferSQL);
											$Person3TransferData = $Person3TransferResult->fetch_assoc();
											if($Person3TransferData['Transfer']==1) {
												$Person3Transfer = $Person3TransferData['description'];
											} else {
												$Person3Transfer = NULL;
											};
											//To get the product details if this roommate has extension product
											$Person3ExtSQL = 	"SELECT ca.Extension, pro.description FROM products pro
																JOIN customer_account ca ON ca.contactid=$person3ID AND ca.tourid=$groupid
																WHERE id=ca.Extension_Product_ID;";
											$Person3ExtResult = $db->query($Person3ExtSQL);
											$Person3ExtData = $Person3ExtResult->fetch_assoc();
											if($Person3ExtData['Extension']==1) {
												$Person3Extension = $Person3ExtData['description'];
											} else {
												$Person3Extension = NULL;
											};
										}
                                        $person3            = $row['Roommate2'];
                                        $NeedTransfer       = $row['Need_Transfer'];
                                        $NeedExtension      = $row['Need_Extension'];
                                        $landReq            = $row["special_land_text"];
                                        $MealsAllergies     = $row["Meals_Allergies"];
                                        $MealsRequest       = $row["Meals_Request"];
                                        $AllergiesRequest   = $row["Allergies_Request"];
                                        
                                        $style='' ;
                                        $statusTitle ='' ;
                                        if( $row['status'] == 0 ) {
                                            $style='style="color:red"' ;
                                            $statusTitle ='Canceled Customer' ;
                                        }
                                        ?>
                                        <tr <?php echo "title='$statusTitle' $style";?> >
                                            <td><?php echo $i?></td>
                                            <td><?php echo $roomType?></td>
                                            <td><a <?php echo $style; ?> href="contacts-edit.php?id=<?php echo $row['id']; ?>&action=edit&usertype=customer"><?php echo $person1?></a></td>
                                            <td><?php echo $person2?></td>
                                            <td><?php echo $person3?></td>
                                            <td>
											<?php 
											if($NeedTransfer != "No") {echo "Person #1: ".$NeedTransfer."<br />";}
											if($Person2Transfer != NULL) {echo "Person #2: ".$Person2Transfer."<br />";}
											if($Person3Transfer != NULL) {echo "Person #3: ".$Person3Transfer."<br />";}
											if($NeedTransfer == "No" AND $Person2Transfer == NULL AND $Person3Transfer == NULL) { echo "No";}
											?>
											</td>
                                            <td>
											<?php 
											if($NeedExtension != "No") {echo "Person #1: ".$NeedExtension."<br />";}
											if($Person2Extension != NULL) {echo "Person #2: ".$Person2Extension."<br />";}
											if($Person3Extension != NULL) {echo "Person #3: ".$Person3Extension."<br />";}
											if($NeedExtension == "No" AND $Person2Extension == NULL AND $Person3Extension == NULL) { echo "No";}
											?>
											</td>
                                            <td><?php echo $landReq; ?></td>
                                            <td><?php echo $MealsRequest; ?></td>
                                            <td><?php echo $AllergiesRequest; ?></td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                        </tbody>
                    </table>
                </div>
    </div>
</div>


<script type="text/javascript">
    function emailPDFconfirmRooming(){
        var divExtra = "<input type='checkbox' name='tour-leader' value='tour-leader' /> Tour Leader &nbsp;&nbsp;" +
                        "<input type='checkbox' name='airline' value='airline' /> Airline Supplier &nbsp;&nbsp;" +
                        "<input type='checkbox' name='agent'  value='agent'/> Agent" ;
        divExtra = encodeURIComponent(divExtra) ;
        showConfirm( "groups-edit.php?action=edit&tab=reports&report_slug=Rooming&email_report=yes&name=<?php echo $groupName;?>&id=<?php echo $tourid; ?>", "Send", "Are you sure you want to sent this report ?", "Please Confirm", "modal-md", divExtra);
    }
    
    $(document).ready(function()
    {
        $("#roomingPrintBtn").click(function()
        {
            var yourDOCTYPE = "<!DOCTYPE html>"; // your doctype declaration
            var printPreview = window.open('about:blank', 'print_preview', "resizable=yes,scrollbars=yes,status=yes");
            var printDocument = printPreview.document;
            printDocument.open();
            printDocument.write(yourDOCTYPE+'<html><style> td,th {border:1px solid black}</style>')
            printDocument.write(
                       "<div class='jsdiv'>"+
                           $("#roomingDiv").html()+
                       "</div></html>");
            printPreview.print();
            printDocument.close();

        });

        $('#no-btn').DataTable({
            dom: 'Bfrtip',
            buttons:[]
        });
    });
    function download_report(groupname,groupid)
    {
        window.location = "excel.php?type=rooming&name="+groupname+"&groupid="+groupid;
    }
</script>