<?php

$sqlg = "SELECT sb.Add_Date, sup.contactid AS id, Comments, Bill_Amount, /*g.Church_Name, g.tourname, g.tourdesc,*/ sb.Due_Date, /*g.2nd_Payment_Amount,*/
        DATE_SUB(sb.Due_Date, INTERVAL 90 DAY) as second_due, /*DATEDIFF(g.startdate, NOW()) as days_left,*/
        c.fname,c.mname,c.lname,c.address1, c.address2, c.zipcode, c.city, c.state, c.country,c.company /*g.agent*/   
FROM suppliers sup 
INNER JOIN contacts c ON c.id = sup.contactid AND sup.status=1
INNER JOIN suppliers_bill sb ON sb.Supplier_ID=sup.contactid AND sb.Status IN (1,2)
/*LEFT JOIN `groups` g ON g.tourid=ca.tourid AND g.status=1*/
WHERE sb.Supplier_Bill_Num='$invoiceId'";

$invoiceData = GDb::fetchRow($sqlg);
$groupId     = $invoiceData['tourid'];
$groupName   = $invoiceData['tourname'];
$customerId  = $invoiceData['id'];
$agentin     = $invoiceData['id'];

$billingAddress = GUtils::buildGSTAddress(true , $invoiceData, ",", "<br/>");

//Find Group Agent Name
$groupset = [];
$agentname = '' ;
if (strlen($agentin) > 0) {
    $sqlga    = "SELECT c.fname,c.mname,c.lname,c.company FROM contacts c WHERE c.id IN($agentin)";
    $groupset = GDb::fetchRowSet($sqlga);
    
    foreach ($groupset as $one) {
        if( $agentname ) {
            $agentname .= ', ' ;
        }
        $agentname .= $one['fname'] . ' ' . $one['lname'] ;
    }
}

$brCount = substr_count($billingAddress, '<br/>');
//Invoice Data

$editId   = $invoiceId;
//invoice
$sqle     = "SELECT sb.*, sup.contactid, c.email FROM suppliers_bill sb
INNER JOIN suppliers sup ON sup.contactid=sb.Supplier_ID
INNER JOIN contacts c ON c.id=sup.contactid
WHERE Supplier_Bill_Num='$editId'";
$invoices = GDb::fetchRow($sqle);

$editContactId = $invoices['contactid'];

//invoice lines
//$sqll          = "SELECT * FROM customer_invoice_line WHERE Customer_Invoice_Num='$editId'";
$sqll          = "SELECT sbl.Supplier_Bill_Num,sbl.Supplier_Bill_Line,sbl.Line_Type,sbl.Product_Product_ID,sbl.Product_Name,sbl.Qty,sbl.Bill_Line_Total,sbl.Mod_Date,sbl.Add_Date
FROM suppliers_bill_line sbl
JOIN products pro 
ON sbl.Product_Product_ID=pro.id
WHERE sbl.Supplier_Bill_Num='$editId'";
$invoiceLines  = GDb::fetchRowSet($sqll);

$paySql   = "SELECT * FROM suppliers_payments WHERE Supplier_Bill_Num='$invoiceId' AND Status=1";
$payments = GDb::fetchRowSet($paySql);

$charges = 0 ;
$numCharges = 0 ;
foreach( $payments as $payment ) {
    if( floatval($payment['Charges']) != 0 ) {
        $numCharges ++ ;
        $charges += $payment['Charges'];
    }
}
/////////////////////////////////////////////////////////////////////////////////////

$paymentCount   = count($payments);
$depositRowSpan = $paymentCount + 2;
if ($depositRowSpan < 4) {
    $depositRowSpan = 4; //min 4
}

//total payments

$sumPayments        = 0;
$paymentMethodArray = [];
foreach ($payments as $one) {
    $sumPayments          += $one['Supplier_Payment_Amount'];
    $paymentMethodArray[] = $one['Supplier_Payment_Method'];
}
$paymentMethods = implode(',', array_unique($paymentMethodArray));

//reminders
$reminder = '' ;
$hasNoPassport = false ;
if( $invoiceData['days_left'] < 180 && ! $invoiceData['passport']) {
    $reminder .= 'Please submit a copy of your passport\'s photo page.' ;
    $hasNoPassport = true ;
}
if( ($invoiceData['expire_diff'] < 180 && $invoiceData['passport']) && ! $hasNoPassport ) {
    if( $reminder ) {
        $reminder .= '<br/>' ;
    }
    $reminder .= 'Please be sure your passport is valid 6 months from the date of return.' ;
}
if( $reminder ) {
    $reminder = '<br/><br/><span color="#000000" bgcolor="yellow">FRIENDLY REMINDERS:<br/>' . $reminder . '</span>' ;
}

//$img_file = __DIR__ . '/../files/assets/images/letter_head2.jpg';
?>
<!--<div style='background: url(<?php echo $img_file;?>) '>-->
<style>
    .bd-f td, .bd-f th {border: 1px solid #A65754; }
    .bd-f {border: 1px solid #A65754; border-collapse: collapse}
    .bd-r {border-right: 1px solid #A65754;}
    .bd-l {border-left: 1px solid #A65754;}
    .bd-t {border-top: 1px solid #A65754;}
    .bd-b {border-bottom: 1px solid #A65754;}
    .spacing{ float: left; width: 100%; display:block;}
    .bolditalic{ font-weight: bold; font-style: italic}
</style>
<!-- Header Table -->

<table style="width: 100%" >
    <tr>
        <td colspan="2" style="text-align: center; ">
            <h1><?php echo $printTitle;?></h1>
            <br/>
            <br/>
        </td>
    </tr>
    <tr>
        <td><?php echo $billingAddress; //$reminder; ?>
			</td>
			<td>Date: <?php echo GUtils::clientDate($invoiceData['Add_Date']); ?>
			<br/>
			Bill #: <?php echo $invoiceId;?>
			<br/>            
        </td>
    </tr>
</table>
        
<div class="spacing"> </div>
<!-- Summary Table -->

<table  style="width: 100%" cellpadding="5" class="bd-f">
    <tr style="background-color: #ffd2d2;">
        <th align="center">Supplier Name</th>
        <th align="center">Company Name</th>
        <th align="center">Due Date</th>
    </tr>
    <tr>
        <td align="center"><?php echo $agentname;?></td>
        <td align="center"><?php echo $invoiceData['company']; ?></td>
        <td align="center"><?php echo GUtils::clientDate($invoiceData['Due_Date']); ?></td>
    </tr>
</table>

<div class="spacing"> </div>

<!-- Detail Table -->
<table  style="width: 100%; " class="bd-f" cellpadding="5" >
    <tr style="background-color: #ffd2d2; " >
        <th align="center" width="70px">QTY</th>
        <th align="center" style="width: 56%" colspan="3">Description</th>
        <th align="center">Cost</th>
        <th align="center">Total Cost</th>
    </tr>
<?php
foreach ($invoiceLines as $one) {
    ?>
        <tr>
            <td><?php echo $one['Qty']; ?></td>
            <td colspan="3"><?php echo $one['Product_Name']; ?></td>
            <td align="right" ><?php echo GUtils::formatMoney($one['Bill_Line_Total']); ?></td>
            <td align="right" ><?php echo GUtils::formatMoney(($one['Bill_Line_Total']*$one['Qty'])); ?></td>
        </tr>
<?php } ?>
        
    <?php if( $charges != 0 ) { ?>
        <tr>
            <td><?php echo $numCharges; ?></td>
            <td colspan="3">Credit Card Surcharge (4%) </td>
            <td align="right" ><?php echo GUtils::formatMoney($charges); ?></td>
            <td align="right" ><?php echo GUtils::formatMoney($charges); ?></td>
        </tr>
    <?php } ?>
        
    <?php /*
    <tr>
        <td><?php echo $paymentCount; ?></td>
        <td>Payments</td>
        <td align="right" ><?php echo GUtils::formatMoney($sumPayments); ?> </td>
        <td><?php echo $paymentMethods; ?></td>
        <td align="right" ></td>
        <td align="right" ><?php echo GUtils::formatMoney(0 - $sumPayments); ?></td>
    </tr>
     */
    ?>

<!--    <tr>
        <td  colspan="2" rowspan="<?php echo $depositRowSpan; ?>">
            <span style="font-size: 0.9em" >
                Note:
                Please check the information for Errors
                Please save invoice for your records.
                Additional information will be mailed to you.
                Cancellation: Any Cancellation must be made in writing 90 days prior to departure
            </span>
        </td>
        <td></td>
        <td></td>
        <td></td>
    </tr>-->

<?php
$paymentCounter = 1;
if ($paymentCount > 0) {
    $paymentCounter ++
    ?>
        <tr style="background-color: #ffd2d2;">
            <td>Payments</td>
            <td align="center">Payment Date</td>
            <td align="center">Payment Type</td>
            <td colspan="2" align="center">Description</td>
            <td></td>
        </tr>
    <?php
}

$iPayments = 0 ;
foreach ($payments as $one) {
    $iPayments ++ ;
    $paymentCounter ++;
    ?>
        <tr>
            <td><?php echo $iPayments;?></td>
            <td align="center"><?php echo GUtils::clientDate($one['Add_Date']); ?></td>
            <td align="center"><?php echo $one['Supplier_Payment_Method']; ?></td>
            <td align="left" colspan="2"><?php echo $one['Supplier_Payment_Comments']; ?></td>
            <td align="right"><?php echo GUtils::formatMoney(0-$one['Supplier_Payment_Amount']); ?></td>
        </tr>
    <?php
    }
    //add extra empty rows to balance Cancellation Note column.
    for ($j = $paymentCounter; $j < $depositRowSpan; $j ++) {
        ?>
<!--        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>-->
    <?php } ?>
    <tr>
        <td colspan="4" rowspan="2"><?php echo $invoiceData['Comments']; ?></td>
        <td align="right"><b>Subtotal</b></td>
        <td align="right" ><?php echo GUtils::formatMoney($invoiceData['Bill_Amount'] + $charges ); ?></td>
    </tr>
    <tr>
        <td align="right"><b>Balance Due</b></td>
        <td align="right" ><?php echo GUtils::formatMoney( ($invoiceData['Bill_Amount'] + $charges) - $sumPayments); ?></td>
    </tr>
</table>
<div align="center">
	<br />
	<br />
	<b>Thank you for your Business with us!</b>
</div>
