<?php
    $custId = $_REQUEST['id'] ;
    if( is_array($custId) ) {
        $custId = reset($custId) ;
    }
    $sql = "SELECT c.id,fname,mname,lname,address1, address2, zipcode, city, state, country, email,
SUM(Bill_Amount) as due, SUM(Customer_Payment_Amount) as paid FROM contacts c
LEFT JOIN suppliers_bill sb ON sb.Supplier_ID = c.id
LEFT JOIN customer_payments cp ON cp.Customer_Account_Customer_ID = c.id
WHERE c.id='$custId' GROUP BY c.id" ;
    
    $result = $db->query($sql);

    $data = $result->fetch_assoc() ;
    
    $ret = [] ;
    if( is_array($data) ) {
        $ret['id'] = $data['id'] ;
        $ret['name'] = $data['fname'] . ' ' . $data['mname'] . ' ' . $data['lname'] ;
        $ret['email'] = $data['email'] ;
        $ret['balance'] = $data['due'] - $data['paid'] ;
        $tourid = $data['tourid'] ;
        
        $sqlt = "SELECT DATE_SUB(startdate, INTERVAL 90 DAY) as due_date FROM groups WHERE tourid='$tourid'" ;
        $resulttour = $db->query($sqlt);
        if( $resulttour ) {
            $tourdata = $resulttour->fetch_assoc() ;
            $ret['due_date'] = $tourdata['due_date'] ;
        }
        else {
            $ret['due_date'] = '' ;
        }

        $ret['address'] = GUtils::buildGSTAddress( false, $data, ",", "\n" ) ;
    }

    GUtils::jsonResponse($ret) ;