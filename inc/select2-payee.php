<?php 

include "../config.php";
include "../inc/helpers.php";

// Number of records fetch
$numberofrecords = 20;
$response = array();

if(!isset($_POST['searchTerm'])){
	//Here if the text field is empty, don't show anything
	$response[] = array(
		"id" => 0,
		"text" => "Please type the name you want"
	);
} else {
	$search = $_POST['searchTerm'];
	$SearchWords = str_replace(" ","%",$search);
	
        // Fetch records for checks page only with DATA attr.
        $GetPayeesList = "SELECT Payee_ID,Payee_Name FROM expenses_payee WHERE Status=1 AND Payee_Name LIKE '%".$SearchWords."%' ORDER BY Payee_ID DESC
        LIMIT $numberofrecords";
        $PayeesList = GDb::fetchRowSet($GetPayeesList);

        // Read Data
        foreach($PayeesList as $Payee){
            //Get the full name first
            $PayeeName = $Payee['Payee_Name'];
            $PayeeAddress = "";
            //Echo it in the array
            $response[] = array(
                "id" => $Payee['Payee_ID'],
                "text" => $PayeeName,
                "CustomerName" => $PayeeName,
                "CustomerAddress" => $PayeeAddress
            );
        }

}


echo json_encode($response);
exit();

?>