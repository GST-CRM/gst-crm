<?php
include_once __DIR__ . '/../models/groups.php';
include_once __DIR__ . '/../models/customer_invoice.php';
include_once __DIR__ . '/../models/customer_invoice_line.php';
include_once __DIR__ . '/../models/customer_payment.php';
include_once __DIR__ . '/../models/customer_credit_note.php';
include_once __DIR__ . '/../models/customer_refund.php';
//Get invoice details.

$invoiceData = (new CustomerPayment())->customerInvoiceDetails($invoiceId);
$creditNotes = (new CustomerCreditNote())->getList(['Customer_Invoice_Num' => $invoiceId, 'Status' => 1]);

$creditNoteObj = new CustomerCreditNote();

$groupId        = $invoiceData['tourid'];
$groupName      = $invoiceData['tourname'];
$customerId     = $invoiceData['id'];
$agentin        = $invoiceData['agent'];
//make html printable biling address.
$billingAddress = GUtils::buildGSTAddress(true, $invoiceData, "", "<br/>");
$brCount        = substr_count($billingAddress, '<br/>');

//Find Group Agent Name
$groupset  = [];
$agentname = (new Groups())->getAgentsNameString($agentin);
//Invoice Data

$editId = $invoiceId;

//invoice details
$invoices      = (new CustomerInvoice())->withContactDetails($invoiceId);
$editContactId = $invoices['contactid'];

//find invoice lines
$groupName = mysqli_real_escape_string(GDb::db(), $invoiceData['tourname']);
$invoiceLines = (new CustomerInvoiceLine())->getListWithPackageName($invoiceId, $groupName);

//find payments
$payments = (new CustomerPayment())->GetListWithGroupBy(['Customer_Invoice_Num' => $invoiceId, 'Status' => 1]);

//calculate total cc charge.
$charges    = 0;
$numCharges = 0;
foreach ($payments as $payment) {
    if (floatval($payment['Charges']) != 0) {
        $numCharges ++;
        $charges += $payment['Charges'];
    }
}
//find how many rows to span in html.
$paymentCount   = count($payments);
$depositRowSpan = $paymentCount + 2;
if ($depositRowSpan < 4) {
    $depositRowSpan = 4; //min 4
}

//To get how many people are in this invoice
$InvoicePeopleQty = 1; //We will add on this amount in the joint select query down

//total payments and payment methods as comma seperate when there are multiple methods.
$sumPayments        = 0;
$paymentMethodArray = [];
foreach ($payments as $one) {
    $sumPayments          += $one['Customer_Payment_Amount'];
    $paymentMethodArray[] = $one['Customer_Payment_Method'];
}
$paymentMethods = implode(',', array_unique($paymentMethodArray));

//reminders highlight
$reminder      = '';
$hasNoPassport = false;
if ($invoiceData['days_left'] < 180 && !$invoiceData['passport']) {
    $reminder      .= 'Please submit a copy of your passport\'s photo page.';
    $hasNoPassport = true;
}
if (($invoiceData['expire_diff'] < 180 && $invoiceData['passport']) && !$hasNoPassport) {
    if ($reminder) {
        $reminder .= '<br/>';
    }
    $reminder .= 'Please be sure your passport is valid 6 months from the date of return.';
}
if ($reminder) {
    $reminder = '<br/><br/><span color="#000000" bgcolor="yellow">FRIENDLY REMINDERS:<br/>' . $reminder . '</span>';
}

if ($editContactId > 0) {
    $JointCustomersSQL   = "SELECT * FROM customer_groups WHERE Primary_Customer_ID=$editContactId AND Type='Group Invoice' AND Group_ID=$groupId";
    $JointCustomersData  = GDb::fetchRow($JointCustomersSQL);
    $AdditionalTravelers = "";
    //Nithin : Verifying $JointCustomersData is an array before counting it.
    if (is_array($JointCustomersData) && count($JointCustomersData) > 0) {

        if ($JointCustomersData['Additional_Traveler_ID_1'] > 0) {
            $JCustomer_ID        = $JointCustomersData['Additional_Traveler_ID_1'];
            $JCsql               = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
            $JCData              = GDb::fetchRow($JCsql);
            $AdditionalTravelers .= $JCData['fname'] . " " . $JCData['lname'] . ", ";
			$InvoicePeopleQty++;
        }
        if ($JointCustomersData['Additional_Traveler_ID_2'] > 0) {
            $JCustomer_ID        = $JointCustomersData['Additional_Traveler_ID_2'];
            $JCsql               = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
            $JCData              = GDb::fetchRow($JCsql);
            $AdditionalTravelers .= $JCData['fname'] . " " . $JCData['lname'] . ", ";
			$InvoicePeopleQty++;
        }
        if ($JointCustomersData['Additional_Traveler_ID_3'] > 0) {
            $JCustomer_ID        = $JointCustomersData['Additional_Traveler_ID_3'];
            $JCsql               = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
            $JCData              = GDb::fetchRow($JCsql);
            $AdditionalTravelers .= $JCData['fname'] . " " . $JCData['lname'] . ", ";
			$InvoicePeopleQty++;
        }
        if ($JointCustomersData['Additional_Traveler_ID_4'] > 0) {
            $JCustomer_ID        = $JointCustomersData['Additional_Traveler_ID_4'];
            $JCsql               = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
            $JCData              = GDb::fetchRow($JCsql);
            $AdditionalTravelers .= $JCData['fname'] . " " . $JCData['lname'] . ", ";
			$InvoicePeopleQty++;
        }
        if ($JointCustomersData['Additional_Traveler_ID_5'] > 0) {
            $JCustomer_ID        = $JointCustomersData['Additional_Traveler_ID_5'];
            $JCsql               = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
            $JCData              = GDb::fetchRow($JCsql);
            $AdditionalTravelers .= $JCData['fname'] . " " . $JCData['lname'] . ", ";
			$InvoicePeopleQty++;
        }
        if ($JointCustomersData['Additional_Traveler_ID_6'] > 0) {
            $JCustomer_ID        = $JointCustomersData['Additional_Traveler_ID_6'];
            $JCsql               = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
            $JCData              = GDb::fetchRow($JCsql);
            $AdditionalTravelers .= $JCData['fname'] . " " . $JCData['lname'] . ", ";
			$InvoicePeopleQty++;
        }
        if ($JointCustomersData['Additional_Traveler_ID_7'] > 0) {
            $JCustomer_ID        = $JointCustomersData['Additional_Traveler_ID_7'];
            $JCsql               = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
            $JCData              = GDb::fetchRow($JCsql);
            $AdditionalTravelers .= $JCData['fname'] . " " . $JCData['lname'] . ", ";
			$InvoicePeopleQty++;
        }
        if ($JointCustomersData['Additional_Traveler_ID_8'] > 0) {
            $JCustomer_ID        = $JointCustomersData['Additional_Traveler_ID_8'];
            $JCsql               = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
            $JCData              = GDb::fetchRow($JCsql);
            $AdditionalTravelers .= $JCData['fname'] . " " . $JCData['lname'] . ", ";
			$InvoicePeopleQty++;
        }
    }
}
        ?>
        <style>
            .bd-f td, .bd-f th {border: 1px solid #A65754; }
            .bd-f {border: 1px solid #A65754; border-collapse: collapse}
            .bd-r {border-right: 1px solid #A65754;}
            .bd-l {border-left: 1px solid #A65754;}
            .bd-t {border-top: 1px solid #A65754;}
            .bd-b {border-bottom: 1px solid #A65754;}
            .spacing{ float: left; width: 100%; display:block;}
            .bolditalic{ font-weight: bold; font-style: italic}
        </style>
        <!-- Header Table -->

        <table style="width: 100%" >
            <tr>
                <td colspan="2" style="text-align: center; ">
                    <h1><?php echo $printTitle; ?></h1>
                    <br/>
                    <br/>
                </td>
            </tr>
            <tr>
                <td width="60%"><span class="bolditalic">The Ultimate Holy Land Experience!</span>
                    <br/>
                </td>
                <td rowspan="2">Date: <?php echo GUtils::clientDate(Date('Y-m-d')); ?>
                    <br/>
                    Invoice #: <?php echo $invoiceId; ?>
                    <br/>            
                    <br/>            

                    <?php echo $billingAddress; ?>

                </td>
            </tr>
            <tr>
                <td><span>Thank you for your payment</span>
                    <?php echo $reminder; ?>
                </td>
            </tr>
        </table>

        <?php if( $editContactId > 0 && is_array($JointCustomersData) && count($JointCustomersData) > 0) { ?>
        <br /><br />
        <span style="font-weight:bold;">Additional Travelers: </span><label><?php echo substr($AdditionalTravelers, 0, -2); ?>.</label>
        <?php
    }

?>

<div class="spacing"> </div>


<!-- Summary Table -->

<table  style="width: 100%" cellpadding="5" class="bd-f" >
    <tr style="background-color: #ffd2d2; " >
        <th align="center">Tour Coordinator</th>
        <th align="center">Church</th>
        <th align="center">Group Reference Number</th>
        <th align="center">2nd Deposit Due
            <?php if ($invoiceData['2nd_Payment_Amount'] != 0) { ?>
                <br /><?php echo GUtils::formatMoney(($invoiceData['2nd_Payment_Amount']*$InvoicePeopleQty)); ?>
            <?php } ?>
        </th>
        <th align="center">Final Payment Due Date</th>
    </tr>
    <tr>
        <td align="center"><?php echo $agentname; ?></td>
        <td align="center"><?php echo $invoiceData['Church_Name']; ?></td>
        <td align="center"><?php echo $invoiceData['tourdesc']; ?></td>
        <td align="center"><?php echo GUtils::clientDate($invoiceData['second_due']); ?></td>
        <td align="center"><?php echo GUtils::clientDate($invoiceData['Due_Date']); ?></td>
    </tr>
</table>

<div class="spacing"> </div>



<!-- Detail Table -->
<table  style="width: 100%; " class="bd-f" cellpadding="5" >
    <tr style="background-color: #ffd2d2; " >
        <th align="center" width="70px">QTY</th>
        <th align="center" style="width: 56%" colspan="3">Description</th>
        <th align="center">Package Price</th>
        <th align="center">Balance Due</th>
    </tr>
    <?php
    //print invoice lines
    foreach ($invoiceLines as $one) {
        
        if( ! $one['Invoice_Line_Total'] ) {
            $one['Invoice_Line_Total'] = $one['PackageTotal'] ;
        }
        ?>
        <tr>
            <td><?php echo $one['Qty']; ?></td>
            <td colspan="3"><?php echo $one['Product_Name']; ?></td>
            <td align="right" ><?php echo GUtils::formatMoney($one['Invoice_Line_Total']); ?></td>
            <td align="right" ><?php echo GUtils::formatMoney($one['PackageTotal']); ?></td>
        </tr>
    <?php } ?>

    <?php
    //cc charge
    if ($charges != 0) { ?>
        <tr>
            <td><?php echo $numCharges; ?></td>
            <td colspan="4">Credit Card Surcharge (4%) </td>
            <td align="right" ><?php echo GUtils::formatMoney($charges); ?></td>
        </tr>
    <?php } ?>

    <?php 
    //cancelled ?
    if( $invoiceData['Invoice_Status'] == CustomerInvoice::$CANCELLED ) { ?>
        <tr>
            <th align="right" colspan="5"> <?php echo $groupName . ' - Cancelled'; ?></th>
            <th align="right" ><?php echo GUtils::formatMoney(0 - $invoiceData['Invoice_Amount']); ?></th>
        </tr>

        <tr>
            <td  align="right" colspan="5"><?php echo $invoiceData['Cancellation_Outcome'];?></td>
            <td align="right"><?php echo GUtils::formatMoney($invoiceData['Cancellation_Charge']); ?></td>
        </tr>
    <?php } ?>
    <?php
    //payments made
    $paymentCounter = 1;
    if ($paymentCount > 0) {
        $paymentCounter ++
        ?>
        <tr style="background-color: #ffd2d2; ">
            <td>Payments</td>
            <td align="center">Payment Date</td>
            <td align="center">Payment Type</td>
            <td colspan="2" align="center">Description</td>
            <td></td>
        </tr>
        <?php
    }
    $iPayments = 0;
    foreach ($payments as $one) {
        $iPayments ++;
        $paymentCounter ++;
        ?>
        <tr>
            <td><?php echo $iPayments; ?></td>
            <td align="center"><?php echo GUtils::clientDate($one['Customer_Payment_Date']); ?></td>
            <td align="center"><?php echo $one['Customer_Payment_Method']; ?></td>
            <td align="left" colspan="2"><?php echo $one['Customer_Payment_Comments']; ?></td>
            <td align="right"><?php echo GUtils::formatMoney(0 - $one['Customer_Payment_Amount']); ?></td>
        </tr>
        <?php
    }
?>

    <?php 
    
    if( is_array($creditNotes) && count($creditNotes) > 0 ) { ?>
    <tr style="background-color: #ffd2d2; ">
            <td align="center"  colspan="2">Credit Note Date</td>
            <td align="center" colspan="3">Description</td>
            <td align="center"></td>
        </tr>   
    <?php
    }
    $creditAmountTotal = 0 ;
    foreach($creditNotes as $one ) {
    if ($one['Credit_Amount'] != 0 ) {
        $creditAmountTotal += $one['Credit_Amount'] ;
        ?>
        <tr>
            <th align="center" colspan="2"><?php echo GUtils::clientDate($one['Credit_Note_Date']); ?></th>
            <th align="center" colspan="3" ><?php echo $creditNoteObj->printCreditNoteLine($one);?></th>
            <th align="right" ><?php echo GUtils::formatMoney(0 - $one['Credit_Amount']); ?></th>
        </tr>
    <?php }
    }
    
    if ( $invoiceData['Refund_Status'] == 1 || $invoiceData['Refund_Status'] == 4 || $invoiceData['Refund_Status'] == 2 || $invoiceData['Refund_Status'] == 3) /* Refund issued or partial refund */ {
        $refundDetail = (new CustomerRefund())->invoiceRefundDetail($invoiceId) ;
        foreach( $refundDetail as $k => $one ) {
            if( ! $one['Amount']) {
                unset($refundDetail[$k]) ;
            }
        }
        $ri = 0 ;
        if(is_array($refundDetail) && count($refundDetail) > 0 ) { 
        foreach( $refundDetail as $refundOne ) {
            $refundAmount = $refundOne['Amount'] ;
        ?>
        <tr>
            <th><?php echo 'Refund #' . ++$ri ; ?></th>
            <th align="center"><?php echo GUtils::clientDate($refundOne['Issue_Date']); ?></th>
            <th align="center" ><?php echo $refundOne['Payment_Type'] ?></th>
            <th align="left" colspan="2" ><?php echo $refundOne['Description'] ?></th>
            <th align="right" ><?php echo GUtils::formatMoney($refundAmount); ?></th>
        </tr>
    <?php } 
        }
    }
    ?>

    <?php
    $totalInvoiceAmount = $invoiceData['Invoice_Amount'];
    if( $invoiceData['Refund_Status'] && $invoiceData['Refund_Status'] != CustomerRefund::$CUSTOM_REFUND ) {
        $totalInvoiceAmount = $invoiceData['Cancellation_Charge'];
    }
    ?>
    <tr>
        <td colspan="4" rowspan="2"><?php echo $invoiceData['Comments']; ?></td>
        <td align="right"><b>Subtotal</b></td>
        <td align="right" ><?php
            if( $invoiceData['Invoice_Status'] == CustomerInvoice::$CANCELLED ) {
                echo GUtils::formatMoney($totalInvoiceAmount + $charges); 
            }
            else {
                echo GUtils::formatMoney($totalInvoiceAmount + $charges - $creditAmountTotal); 
            }
            
        ?></td>
    </tr>
    <?php if( $invoiceData['Invoice_Status'] == CustomerInvoice::$CANCELLED && floatval($invoiceData['Refund_Amount']) > 0 && ( $invoiceData['Refund_Status'] == 3 || $invoiceData['Refund_Status'] == 1) ) { ?>
    <tr>
        <td align="right"><b>Refund Due</b></td>
        <td align="right" >
            <?php echo GUtils::formatMoney($invoiceData['Pending_Refund_Amount']) ; ?>
        </td>
    </tr>
    <?php } else { ?>
    <tr>
        <td align="right"><b>Balance Due</b></td>
        <td align="right" >
            <?php echo GUtils::formatMoney($invoiceData['balance']) ; ?>
        </td>
    </tr>
    <?php } ?>
</table>
<br/>
<div align="center" style="border: 1px solid #A65754">
    Note:
    Please check the information for Errors
    Please save invoice for your records.
    Additional information will be mailed to you.
    Cancellation: Any Cancellation must be made in writing 90 days prior to departure
</div>
<div align="center">
    <b>Make checks payable to Goodshepherd Travel</b>
    <br/>
    <span>9021 Washington Ln, Lantana TX 76226</span>
</div>


<div align="center">
    <b>Or pay online at <a href="https://www.tourtheholylands.com/make-a-payment/">https://www.tourtheholylands.com/make-a-payment/</a></b>
    <br/>
    <span> (Debit and Credit card payments subject to 4% surcharge)</span>
</div>
