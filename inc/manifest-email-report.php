<?php
include_once 'vendor/autoload.php';
include_once __DIR__ . '/../models/groups.php' ;
include_once __DIR__ . '/../models/users.php' ;
include_once("files/excel/xlsxwriter.class.php");

use PHPMailer\PHPMailer\PHPMailer ;

//Using same pages for receipt and invoice.

//Set a default invoice/receipt.
$SUBJECT = 'Manifest Report' ;

//set default file prefix.
$filePrefix = 'Manifest-Report' ;

//set default messages.
$MESSAGE = 'Please find Manifest Report attachment.' ;

ob_start() ;

$manifestName = "Manifest";
                           // Passing `true` enables exceptions
try {
	//Recipients
    $groupId = $_REQUEST['id'];
    $group = (new Groups())->get(['tourid' => $groupId]) ;
    $groupName = $group['tourname'] ;
    
    $groupLeaders = [] ;
    if( isset($_REQUEST['tour-leader']) && $_REQUEST['tour-leader'] ) {
        $groupLeaders = (new Groups())->getGroupLeadersDetail($groupId) ;
    }
    $groupAgents = [] ;
    if( isset($_REQUEST['agent']) && $_REQUEST['agent'] ) {
        $groupAgents = (new Groups())->getGroupAgentsNameList($groupId) ;
    }
    $groupAirlineEmail = [] ;
    if( isset($_REQUEST['airline']) && $_REQUEST['airline'] ) {
        $groupAirlineEmail = (new Groups())->airlineSupplierEmail($groupId) ;
    }
    
    $loggedInUser = (new Users())->getLoggedInUserDetail() ;
    
    //Group Leader
    $bcc = [] ;
    foreach( $groupLeaders as $g ) {
        $bcc[] = $g['email'] ;
    }
    //Agent
    foreach( $groupAgents as $a ) {
        $bcc[] = $a['email'] ;
    }
    if( $groupAirlineEmail ) {
        $bcc[] = $groupAirlineEmail ;
    }
    //Airline 
    //Finc To
    $to = (isset($loggedInUser['EmailAddress']) ? $loggedInUser['EmailAddress'] : '') ;        //Fix Arrays
    if( ! $to ) {
        $to = 'donotreply@goodshepherdtravel.com' ;
    }

	$PDF_OUTPUT = 'S';
	$PDF_HEADER = true ;

	$writer = new XLSXWriter();

    include __DIR__ . '/manifest-excel-report.php' ;
    
    $pdfData = $writer->writeToString();
	
    if (GUtils::sendMail($to, $SUBJECT, $MESSAGE, $manifestName . '.xlsx', $pdfData, [], [], $bcc))
	{
		//echo "sent";
		GUtils::setSuccess("Email sent successfully");
	}
	else
	{
		//echo "not sent";
		GUtils::setWarning("Error in sending email. Try again later");
	}
	//GUtils::redirect("groups-edit.php");

} catch (Exception $e) {
	print_r($e->getMessage());
	print_r($e);
	GUtils::setWarning("Error in sending email. Try again later");
	//GUtils::redirect("groups-edit.php");
}