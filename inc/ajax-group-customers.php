<?php

if (!isset($__REQUEST['groupId'])) {
    GUtils::jsonResponse([]);
    return;
}
$groupId = $__REQUEST['groupId'];
//$customerId = $__REQUEST['customerId'];

//get list of customers belong to a specific group.
$sql = "SELECT c.id, c.fname, c.mname, c.lname, c.email FROM contacts c
        INNER JOIN customer_account ca ON ca.contactid = c.id 
        INNER JOIN `groups` AS g ON g.tourid = ca.tourid 
        WHERE ca.tourid='$groupId' AND ca.status=1 AND g.status=1 GROUP BY c.id ";

$rows = GDb::fetchRowSet($sql);

$html = '' ;
if(is_array($rows) ) {
    foreach ($rows as $data) {

        $selected = '' ;
        if (is_array($data)) {
            $name = $data['fname'] . ' ' . $data['mname'] . ' ' . $data['lname'];
//            if( $data['id'] == $customerId ) {
//                $selected = 'selected="selected"' ;
//            }
            $html .= '<option ' . $selected . ' value="' . $data['id'] . '">' . $name . '</option>';
        }
    }
}
echo $html;
