<?php

include_once __DIR__ . '/../models/tour_membership.php';
include_once __DIR__ . '/../models/customer_invoice.php';
include_once __DIR__ . '/../models/customer_payment.php';
include_once __DIR__ . '/../models/customer_account.php';
include_once __DIR__ . '/../models/customer_refund.php';
include_once __DIR__ . '/../models/customer_groups.php';
include_once __DIR__ . '/../models/sales_order.php';

try {
        
    //intialize dependencies..
    $tourMembership = new TourMembership() ;
    $customerInvoice = new CustomerInvoice() ;
    $customerPayment = new CustomerPayment() ;
    $customerRefund = new CustomerRefund() ;
    $customerGroups = new CustomerGroups() ;
    
    //find invoice data first
    $invoiceId   = $__REQUEST['invoiceId'];
    $tripId   = $__REQUEST['tourId'];
    $invoiceData = $customerPayment->customerInvoiceDetails($invoiceId);
    //find invoice customer.
    $customerId = $invoiceData['contactid'] ;
    
    if( ! $tripId || ! $customerId ) {
        
        if( $REVERSE ) {
            return GUtils::jsonFail(['message' => "Unable to restore this User's Cancellation !"]);
        }
    else {
            return GUtils::jsonFail(['message' => "Unable to cancel this User's Invoice !"]);
        }
    }
    
    $refundData = $customerRefund->get(['Customer_Invoice_Num' => $invoiceId]) ;

    $skipCancel = false ;
    if( ! $REVERSE ) {
        //if already canceled, return failure immediately.

        if( $refundData && ($refundData['Status'] == CustomerRefund::$CREATED ) ) {
            $skipCancel = true ;
        }
    }
    
    if( $skipCancel ) {
        return GUtils::jsonFail(['message' => "Unable to cancel this User's Invoice, Already Canceled?"]);
    }
    
    $isJoint = false ;
    if( $REVERSE ) {
        //This is not required. Please see comments - https://goodshepherdtravel.atlassian.net/browse/GST2-37
        //Need to figure out group invoice first.
        if( $refundData['Customer_Invoice_Num'] && $refundData['Payee_Invoice_Num'] && $refundData['Customer_Invoice_Num'] != $refundData['Payee_Invoice_Num']) {
            $isJoint = true ;
            (new CustomerAccount())->update(['groupinvoice' => 1 ], ['contactid' => $customerId]) ;    //check whether cancelled it ?            
        }
    }

    if( $customerInvoice->isGroupInvoice($invoiceId) ) {

        if( ! $REVERSE ){

            //Switch Primary.
            $newPrimary = isset($__REQUEST['additional']) ? $__REQUEST['additional'] : '' ;
            $refundType = isset($__REQUEST['refund_method']) ? $__REQUEST['refund_method'] : '' ;
            if( $refundType == '2' && $newPrimary ) {
               $newInvoiceId = (new CustomerGroups())->switchPrimary($invoiceData['tourid'], $invoiceData['contactid'], $newPrimary) ;
               if( ! $newInvoiceId ) {
                   return GUtils::jsonFail(['message' => "Unable to switch primary !"]);
               }
               //From here.. this is the invoice id to cancel.
//               $__REQUEST['invoiceId'] = $newInvoiceId ;
//               $invoiceId = $newInvoiceId ;
//               $invoiceData = $customerPayment->customerInvoiceDetails($invoiceId);
               //$customerId = Do not change we need to cancel the same.
            }
        }
        //Remove products from group 
        $operation = '-' ;
        if( $REVERSE ) {
            //Add to product back to group
            $operation = '+' ;
        }
        
        
        //Is primary ? remove all from group and close this primary customer.
        if( $customerGroups->isPrimaryTraveler($tripId, $customerId) ) {
            $customers = $customerGroups->getAdditionalCustomerIds($tripId, $customerId) ;

            foreach( $customers as $customerOne ) {
                $customerInvoice->moveAdditioalMemberFromGroupInvoice($tripId, $customerOne, $operation) ;
                //remove from customer_group
                $customerGroups->unlinkFromGroup($tripId, $customerOne) ;
            }
            //finally the primary himself
            $customerInvoice->updateInvoiceTotal($invoiceId) ;
            $customerGroups->unlinkFromGroup($tripId, $customerId, true) ;
        }
        else {
            $customerInvoice->moveAdditioalMemberFromGroupInvoice($tripId, $customerId, $operation) ;        
        }
    }

    
    if( ! $REVERSE ) {
        //remove groupinvoice flag, since he already removed from group invoice also.
        (new CustomerAccount())->update(['groupinvoice' => 0 ], ['contactid' => $customerId]) ;    //check whether cancelled it ?
    }

    //recalculate invoie data again 
    $invoiceDataNew = $customerPayment->customerInvoiceDetails($invoiceId);
    
    //calculate refund amount and proceed.
    $refundInfo = $customerRefund->calculateRefund($invoiceDataNew['Invoice_Amount'], $invoiceDataNew['charges'], $invoiceData['paid'], $invoiceData['trip_start_days'], $invoiceData['Cancellation_Charge_Others'], $invoiceData['Customer_Invoice_Num']);

    if( $REVERSE ) {
        //reactivate customer flag
        (new CustomerAccount())->restoreAccount($tripId, $customerId) ;
        (new SalesOrder())->orderResume($tripId, $customerId) ;
    }
    else {
        //cancel customer account status flag
        (new CustomerAccount())->cancelAccount($tripId, $customerId) ;
        (new SalesOrder())->orderStop($tripId, $customerId) ;
    }

    //cancel trip member ship. This different from group_invoice connection, dont get confused.       
    $data = array(
            'Customer_Account_Customer_ID'  => $customerId,
            'Tour_ID'  => $tripId,
            'Add_Date'             => Model::sqlNoQuote('now()'),
            'Mod_Date'             => Model::sqlNoQuote('now()'),
    );

//    if( $REVERSE ) {
//        $data['Status'] = TourMembership::$ACTIVE ;
//    }
//    else {
//        $data['Status'] = TourMembership::$INACTIVE ;
//    }
//
//    $cancellationDate = isset($__REQUEST['cancellation_date']) ? $__REQUEST['cancellation_date'] : '' ;
//    if( $cancellationDate ) {
//        $data['Mod_Date'] = $cancellationDate ;
//    }
//
//    $where = array(
//        'Customer_Account_Customer_ID'  => $customerId,
//        'Tour_ID'  => $tripId,
//    ) ;
//
//    $status = $tourMembership->upsert($data, $where) ;

    if( $REVERSE ) {
        include __DIR__ . '/ajax-invoice-restore-refund.php' ;
    }
    else {
        include __DIR__ . '/ajax-invoice-cancel-refund.php' ;
    }

        
} catch (\Exception $e) {
    $code = $e->getCode();
}