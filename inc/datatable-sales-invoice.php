<?php
//include connection file 
include __DIR__ . "/../config.php";

include_once __DIR__ . '/../models/acl_permission.php';
include_once __DIR__ . '/../models/customer_invoice.php';
include_once __DIR__ . '/../models/customer_refund.php';
include_once __DIR__ . '/../models/customer_payment.php';

// Acl Condition
$params = $__REQUEST;
$GroupID = AclPermission::userGroup() ;
$contactId  = AclPermission::userId() ;
$ACL_CONDITION = "" ;
if( AclPermission::roleFlags() & AclRole::$FLAG_ONLY_GROUP ) {
    $ACL_CONDITION = " AND ca.tourid IN($GroupID) " ;
}
else if( AclPermission::roleFlags() & AclRole::$FLAG_ONLY_MINE ) {
    $ACL_CONDITION = " AND ca.contactid = $contactId " ;
}

//common query used for sales invoice an group invoice. this condition identifies 
//it and choses the condition for right page.
$whereCondition = '' ;
$tourid = $__GET['tourid'] ;
$limit = "LIMIT " . $params['start'] . " ," . $params['length'] . " ;" ;
$redirectUrl = 'sales-invoices.php' ;
if( $__GET['page'] == "groups-edit.php" && $__GET['tourid'] ) {
    $whereCondition = " AND ca.tourid =$tourid $ACL_CONDITION " ;
    $limit = "" ;
    
    $redirectUrl = "groups-edit.php?id=$tourid&action=edit&tab=invoice&rand=" . rand() ;
}
else {
    //sales-invoice page filter by tours ids of agent.
    $whereCondition = $ACL_CONDITION ;
}

if( isset($__GET['showing']) && $__GET['showing'] == 'canceled' ) {
    $whereCondition .= " AND ci.Status IN(4) " ;
}
else if( isset($__GET['showing']) && $__GET['showing'] == 'active' ) {
    $whereCondition .= " AND ci.Status IN(1) " ;
}
else {
    $whereCondition .= " AND ci.Status IN(1, 4) " ;
}

$whereCondition.= " AND ca.invoiced_customer = 1 ";
$invoiceActions = AclPermission::actionAllowed('Action', 'sales-invoice.php') ; 

//To Use for the "Last Updated" Column
$InvoiceNowTime = new DateTime();

//query filter for summary box chosen.
$filter = '' ;
$class = '' ;
$summaryClass = '' ;
if( isset($__GET['filter']) ) {
    $class = $__GET['filter'] ;
    if( $__GET['filter'] == 'total_due' ) {
        $filter = " HAVING Invoice_Amount > total " ;
    } else if( $__GET['filter'] == 'not_yet_due' ) {
        $filter = " HAVING Invoice_Amount > total AND Due_Date > CURDATE() " ;
    } else if( $__GET['filter'] == 'over_due' ) {
        $filter = " HAVING Invoice_Amount > total AND Due_Date <= CURDATE() " ;
    } else if( $__GET['filter'] == 'paid' ) {
        $filter = " HAVING total > 0 " ;
    }
}

//--------------------------------

//define index of column
$columns = array( 
    0 => 'Customer_Invoice_Num',
    1 => 'Customer_Invoice_Num', 
    2 => 'fname', 
    3 => 'Add_Date', 
    4 => 'Due_Date',
    5 => 'Invoice_Amount',
    6 => 'Customer_Invoice_Num',
    7 => 'Invoice_Status, Due_Date',
    8 => 'Last_Update',
    9 => 'Last_Update'
);


$where  = '';
// check search value exist
if (!empty($params['search']['value'])) {
    $where .= " AND ";
    $where .= " ( cp.Customer_Invoice_Num LIKE '" . $params['search']['value'] . "%' ";
    $where .= " OR c.fname LIKE '" . $params['search']['value'] . "%' ";
    $where .= " OR c.mname LIKE '" . $params['search']['value'] . "%' ";
    $where .= " OR Invoice_Amount LIKE '" . $params['search']['value'] . "%' ";
    $where .= " OR c.lname LIKE '" . $params['search']['value'] . "%' )";
}

if( $params['order'][0]['column'] == 0 )  {
    //$orderBy = " ORDER BY Customer_Invoice_Num DESC " ;
    $orderBy = " ORDER BY Last_Update DESC " ;
}
else {
    $orderBy = " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']." " ;
}

$customerInvoice = new CustomerPayment() ;

$sqlCount = $customerInvoice->paymentListingCountQuery($whereCondition . ' ' . $where, $filter ) ;

$data = $customerInvoice->paymentListing($whereCondition . ' ' . $where, $filter, $orderBy, $limit) ;

$totalRecords = GDb::fetchScalar($sqlCount);

$canEditInvoice = AclPermission::pageAllowed('sales-invoice-add.php') ; 
$canEditContact = AclPermission::pageAllowed('contacts-edit.php') ; 

$output = [];
foreach ($data as $row) {
    $oneOutput  = [];
    
    
    
    
    //------------------------------------------------------------------------
   
									
									//To get the total of the invoice lines for this invoice!
									$InvoiceID = $row["Customer_Invoice_Num"];
									$CustomerzID = $row["id"];
									$InvoiceTotalSQL = "SELECT SUM(Qty * Invoice_Line_Total) AS Invoice_Total FROM customer_invoice_line WHERE Customer_Invoice_Num=$InvoiceID";
									$InvoiceTotalResult = $db->query($InvoiceTotalSQL);
									$InvoiceTotalCount = $InvoiceTotalResult->num_rows;
									if($InvoiceTotalCount > 0) {
										$InvoiceTotalData = $InvoiceTotalResult->fetch_row();
										$InvoiceTotalAmount = $InvoiceTotalData[0];
									} else {
										$InvoiceTotalAmount = 0;
									}
									
									//To get the balance of this invoice minus the total paid
                                    //$balance = $row["Invoice_Amount"] - $row["total"];
                                    $charges = $row["charges"] ;
                                    $totalPaymentAmount = $row["total"] - $row["Credit_Amount"] ;
                                    $balance = $row['balance'];
                                    $name = $row['title'].' '.$row['fname'].' '.$row['mname'].' '.$row['lname'];
                                    //$name = $row['fname'] . ' ' . $row['mname'] . ' ' . $row['lname'];
                                    $nameAppend = ((strlen(trim($name)) > 0) ? $name : '');
                                    
                                    $daysPeriod = $row['trip_start_days'] ;
                                    //do not deduct credit amount from total while calculating refund amount. use $row["total"] instead of $totalPaymentAmount
                                    $cancellationInfo = CustomerRefund::calculateRefund($InvoiceTotalAmount, $row['charges'], $row["total"], $daysPeriod, $row['Cancellation_Charge_Others'], $row['Customer_Invoice_Num']) ;
                                    
                                    //name & email
                                    $name_email = '<b>' . $nameAppend  . (isset($row['email']) ? ' ('.$row['email'] .')' : '') . '</b>' ;
                                    
                                    $oneOutput[0] = '<input name="cbInvoice[]" value="' . $InvoiceID . '" type="checkbox" class="gst-invoice-cb-children gst-invoice-cb"/>' ;
                                    
                                        ob_start() ;
                                        if( $canEditInvoice) { ?>
                                        <a style="color: #0000EE;" href="contacts-edit.php?invid=<?php echo $row["Customer_Invoice_Num"]; ?>&id=<?php echo $row["contactid"];?>&action=edit&usertype=sales">
                                        <?php } ?>
                                            <?php echo $InvoiceID; ?>
                                            <?php if( $canEditInvoice) { ?>
                                        </a>
                                        <?php }
                                        
                                     $oneOutput[1] = ob_get_clean() ;
                                        
                                     ob_start() ;
                                     
                                            if( $canEditContact) { ?>
                                            <a style="color: #0000EE;" href="contacts-edit.php?id=<?php echo $CustomerzID; ?>&action=edit&usertype=customer">
                                            <?php } ?>
                                                <?php echo $nameAppend; ?>
                                                <?php if( $canEditContact ) { ?>
                                            </a>
                                                <?php }
                                          
                                       $oneOutput[2] = ob_get_clean() ;
                                       $oneOutput[3] = GUtils::clientDate($row["Add_Date"]);
                                       $oneOutput[4] = GUtils::clientDate($row["Due_Date"]); 
                                       $oneOutput[5] =  GUtils::formatMoney($row['Invoice_Amount_Revised']);
                                       
                                       ob_start() ;
                                        echo GUtils::formatMoney($row['balance']); 
                                        
                                        $oneOutput[6] = ob_get_clean() ;
                                        
                                        ob_start() ;
                                        if( $row['Invoice_Status'] == CustomerInvoice::$CANCELLED ) {
                                            echo '<span style="color:darkred">Cancelled</span>' ;
                                        }
                                        else {
                                            if ($balance > 0) {
//                                                echo GUtils::tellDateDifference(Date('Y-m-d H:i:s'), $row["Due_Date"], 'ymd', 'Due In %s', 'Overdue %s');
                                                if( $row['due_diff'] > 0 ) {
                                                    echo 'Due ' . $row['due_diff'] . ' Day(s)' ;
                                                }
                                                else if( $row['due_diff'] == 0 ) {
                                                    echo 'Due Today' ;
                                                }
                                                else {
                                                    echo '<span style="color:red">Past Due ' . abs($row['due_diff']). ' Day(s)</span>' ;
                                                }
                                            }
                                            else if( $balance == 0 ) {
                                                echo '<span style="color:green">Invoice Paid</span>' ;
                                            }
                                            else {
                                                echo "<a style='color: #0000EE;' href='payments-add.php?invoice=" . $row["Customer_Invoice_Num"] . "'>Issue Refund</a>" ;
                                            }
                                        }
                                        
                                        $oneOutput[7] = ob_get_clean() ;

											/*ob_start();
									
											$UnixDate = $row["Last_Update"]; 
											$UnixDate2 = new DateTime("@$UnixDate"); 
											$FormattedDate = date('m/d/Y h:i A',($UnixDate2->getTimestamp()-28800));
											echo $UnixDate;
											
											$oneOutput[8] = ob_get_clean() ;*/
                                            
											
											ob_start();
									
											$UnixDate = $row["Last_Update"]; 
											$UnixDate2 = new DateTime("@$UnixDate"); 
											$FormattedDate = date('m/d/Y h:i A',($UnixDate2->getTimestamp()));

											$createDate = new DateTime($FormattedDate);
											$difference = $createDate->diff($InvoiceNowTime);
											if($difference->y > 0) { echo $difference->y." years ago"; }
											elseif($difference->m > 0) { echo $difference->m." months ago"; }
											elseif($difference->d > 0) { echo $difference->d." days ago"; }
											elseif($difference->h > 0) { echo $difference->h." hours ago"; }
											elseif($difference->i > 0) { echo $difference->i." minutes ago"; }
											elseif($difference->s > 0) { echo $difference->s." seconds ago"; }
											
											$oneOutput[8] = ob_get_clean() ;
											
                                            if( $invoiceActions) { 
                                                ob_start() ;
                                                ?>
                                        
                                            <div style="width: 150px;" class="btn-group gst-btns ">
                                                <?php 
                                                
                                                //The first button decision {
                                                if( $row['Invoice_Status'] == CustomerInvoice::$ACTIVE ) { ?>
                                                <span onclick="window.location.href='payments-add.php?invoice=<?php echo $row["Customer_Invoice_Num"]; ?>';"
                                                        class=" btn-transparent">Receive Payment
                                                </span>
                                                <?php 
                                                }
                                                else if( $row['Invoice_Status'] == CustomerInvoice::$CANCELLED && floatval($row['Refund_Amount']) > 0 && $row['Refund_Status'] == 1 ) {
                                                ?>
                                                <span onclick="window.location.href='refund_receipt_add.php?invid=<?php echo $row["Customer_Invoice_Num"]; ?>';"
                                                        class=" btn-transparent">Issue Refund
                                                </span>
                                                <?php
                                                }
                                                else if( $row['Invoice_Status'] == CustomerInvoice::$CANCELLED && ( $row['Refund_Status'] != 1 || floatval($row['Refund_Amount']) == 0 ) ) {
                                                ?>
                                                <span onclick="window.location.href='sales-invoice-add.php?invid=<?php echo $row["Customer_Invoice_Num"]; ?>';"
                                                        class=" btn-transparent">Edit / View
                                                </span>
                                                <?php
                                                }
                                                else if( $row['Invoice_Status'] == CustomerInvoice::$VOID ) {
                                                    ?>
                                                <button onclick = 'deleteConfirm( "sales-invoices.php?invaction=delete&invid=<?php echo $row["Customer_Invoice_Num"]; ?>")' type="button" class="btn btn-danger">
                                                    Delete
                                                </button>
                                                    <?php
                                                 }
                                                 //} End of first button
                                                 ?>
                                                
                                                <?php
                                                if( $row['Invoice_Status'] == CustomerInvoice::$CANCELLED || $row['Invoice_Status'] == CustomerInvoice::$ACTIVE ) { ?>
                                                <span class=" btn-transparent dropdown-toggle"
                                                        data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </span>
                                                <ul class="dropdown-menu" role="menu" style="white-space: nowrap">
                                                    <?php 
                                                    //Already show as main button
                                                    if( $canEditInvoice && ! ($row['Invoice_Status'] == CustomerInvoice::$CANCELLED && ( $row['Refund_Status'] != 1 || floatval($row['Refund_Amount']) == 0 )) ) { ?>
                                                    <li><a href="contacts-edit.php?invid=<?php echo $row["Customer_Invoice_Num"]; ?>&id=<?php echo $row["contactid"];?>&action=edit&usertype=sales">Edit/View</a></li>
                                                    <?php } ?>

                                                    <?php
                                                    $primaryCustomer = (new CustomerGroups())->isPrimaryTraveler($row["tourid"], $row["contactid"]) ;
                                                    $dim = '' ;
                                                    $title = '' ;
                                                    if( $primaryCustomer ) {
                                                        $dim = 'style="opacity:0.5;cursor: default;"' ;
                                                        $title = 'A Primary Traveller of Joint Invoice' ;
                                                    }

                                                    if( $row['Invoice_Status'] != CustomerInvoice::$CANCELLED ) { ?>
                                                    <li><a <?php echo $dim;?> href="javascript:void(0);" 
                                                             data-name="<?php echo $row["title"]." ".$row["fname"]." ".$row["mname"]." ".$row["lname"]; ?>" 
                                                             data-id="<?php echo $row["Customer_Invoice_Num"]; ?>"
                                                             data-tourid="<?php echo $row["tourid"]; ?>"
                                                             data-redirect-url="<?php echo $redirectUrl;?>" 
                                                             data-cancel-amount="<?php echo GUtils::formatMoney( $cancellationInfo['Cancellation_Charge']);?>" 
                                                             data-cancel-refund="<?php echo GUtils::formatMoney( $cancellationInfo['Refund_Amount']) ;?>" 
                                                             data-cancel-outcome="<?php echo $cancellationInfo['Cancellation_Outcome'];?>" 
                                                             data-cancel-paid="<?php echo ($row['paid'] - $row['charges']);?>" 
                                                           <?php if( ! $primaryCustomer ) { ?>
                                                           onclick = 'removeTripCustomerHandler(this)'
                                                           <?php } ?>
                                                           >Cancel & Refund</a></li>                                                           
                                                    <?php } ?>
                                                    <li><a target="_blank" href="sales-invoice-print.php?print=invoice&invid=<?php echo $row["Customer_Invoice_Num"]; ?>">Print</a></li>
                                                    <li><a href="javascript:void(0);" onclick = 'showConfirm( "sales-invoices.php?invaction=send&invid=<?php echo $row["Customer_Invoice_Num"]; ?>", "Send", "Are you sure you want to sent this email to <?php echo $name_email;?> ?", "Please Confirm", "modal-md")' >
                                                    <?php if( $row['Invoice_Sent'] > 0 ) { echo 'Sent (' . $row['Invoice_Sent'] . ')'; } else { echo 'Send'; } ?>
                                                            </a></li>
                                                    <li><a data-name="<?php echo $name; ?>" data-id="<?php echo $row["Customer_Invoice_Num"]; ?>" data-email="<?php echo $row['email'];?>" href="javascript:void(0);" onclick = 'sendReminderHandler(this)'  >Send Reminder</a></li>
                                                    <?php 
                                                    //Not applicable for cancelled button
                                                    if( $row['Invoice_Status'] == CustomerInvoice::$ACTIVE ) {
                                                    ?>
                                                    <li><a href="credit_notes_add.php?invid=<?php echo $row["Customer_Invoice_Num"]; ?>" >Credit Note</a></li>
                                                    <?php
                                                    }
                                                    if( ! ($row['Invoice_Status'] == CustomerInvoice::$CANCELLED && $row['Refund_Status'] == 2 ) ) { ?>
                                                    <li><a href="javascript:void(0);" onclick = 'deleteConfirm( "sales-invoices.php?invaction=delete&invid=<?php echo $row["Customer_Invoice_Num"]; ?>", "Delete")' >Delete</a></li>
                                                    <li><a href="javascript:void(0);" onclick = 'deleteConfirm( "sales-invoices.php?invaction=void&invid=<?php echo $row["Customer_Invoice_Num"]; ?>", "Void")' >Void</a></li>
                                                    <?php } ?>
                                                </ul>
                                                <?php }   ?>
                                            </div>
                                        
                                            <?php 
                                            
                                            $oneOutput[9] = ob_get_clean() ;
											
											
                                                    } ?>
                                    
                                <?php 
    //------------------------------------------------------------------------

    $output[]     = $oneOutput;
}

$json_data = array(
    "draw"            => intval($params['draw']),
    "recordsTotal"    => intval($totalRecords),
    "recordsFiltered" => intval($totalRecords),
    "data"            => $output   // total data array
);

echo json_encode($json_data);  // send data as json format
