<?php

include '../session.php';
include_once 'helpers.php';
include_once __DIR__ . '/../models/transactions.php';
include_once __DIR__ . '/../models/customer_refund_line.php';
include_once __DIR__ . '/../models/customer_refund.php';

$transaction = new Transactions();
$refundLine  = new CustomerRefundLine();
$refund      = new CustomerRefund();

$sql     = "SELECT * FROM customer_refund WHERE status=2";
$records = GDb::fetchRowSet($sql);
Model::begin() ;

foreach ($records as $one) {
    //mark transaction
    $tdata = array(
        'Transaction_Type'               => 987,
        'Chart_of_Accounts_GL_Account_N' => '9876',
        'Transaction_Amount'             => $one['Refund_Amount'],
        'Customer_Account _Customer_ID'  => $one['Customer_Account_Customer_ID'],
        'Supplier_Supplier_ID'           => '0',
        'Add_Date'                       => Model::sqlNoQuote('now()'),
        'Mod_Date'                       => Model::sqlNoQuote('now()'),
        );

    $transaction->insert($tdata);
    
    $tid = $transaction->lastInsertId() ;


    $data = [
        'Refund_ID'                       => $one['Refund_ID'],
        'Amount'                          => $one['Refund_Amount'],
        'Issue_Date'                      => $one['Mod_Date'],
        'Description'                     => $one['Refund_Type'],
        'Transaction_Type_Transaction_ID' => $tid,
        'Attachment'                      => $one['Attachment'],
        'Status'                          => 1,
        'Add_Date'                        => Model::sqlNoQuote('NOW()'),
        'Mod_Date'                        => Model::sqlNoQuote('NOW()'),
        ];

    $refundLine->insert($data);
}
//Finally
$sqlRefund = "UPDATE customer_refund SET Issued_Amount=Refund_Amount WHERE Status=2";
GDb::execute($sqlRefund) ;

Model::commit() ;