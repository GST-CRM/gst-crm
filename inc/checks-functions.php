<?php
include "../session.php";

if(isset($_POST['Add_Account'])){
	$AccountType = mysqli_real_escape_string($db, $_POST["accounts"]);
	if($AccountType == 'cash') {
		$Bank_Name = mysqli_real_escape_string($db, $_POST["C_Bank_Name"]);
		$Bank_Notes = mysqli_real_escape_string($db, $_POST["C_Bank_Notes"]);
        $AddNewBankSQL = "INSERT INTO checks_bank(Bank_Type,Bank_Name,Bank_Notes) VALUES ('$AccountType','$Bank_Name','$Bank_Notes');";
	} elseif($AccountType == 'bank') {
		$Bank_Name = mysqli_real_escape_string($db, $_POST["B_BankName"]);
		$BankCheckNumber = mysqli_real_escape_string($db, $_POST["B_BankCheckNumber"]);
		$BankAddress = mysqli_real_escape_string($db, $_POST["B_BankAddress"]);
		$TransitCode = mysqli_real_escape_string($db, $_POST["B_TransitCode"]);
		$RoutingNum = mysqli_real_escape_string($db, $_POST["B_RoutingNum"]);
		$AccountNum = mysqli_real_escape_string($db, $_POST["B_AccountNum"]);
        $AddNewBankSQL = "INSERT INTO checks_bank(Bank_Type,Bank_Name,Bank_Address,Bank_Transit,Bank_Routing,Bank_Account_Number,Bank_Check_Number) VALUES ('$AccountType','$Bank_Name','$BankAddress','$TransitCode','$RoutingNum','$AccountNum','$BankCheckNumber');";
	} elseif($AccountType == 'cards') {
		$Bank_Name = mysqli_real_escape_string($db, $_POST["Cr_Bank_Name"]);
		$Bank_Account_Number = mysqli_real_escape_string($db, $_POST["Cr_Bank_Account_Number"]);
        $AddNewBankSQL = "INSERT INTO checks_bank(Bank_Type,Bank_Name,Bank_Account_Number) VALUES ('$AccountType','$Bank_Name','$Bank_Account_Number');";
	} elseif($AccountType == 'paypal') {
		$Bank_Name = mysqli_real_escape_string($db, $_POST["P_Bank_Name"]);
		$Bank_Address = mysqli_real_escape_string($db, $_POST["P_Bank_Address"]);
		$Bank_Notes = mysqli_real_escape_string($db, $_POST["P_Bank_Notes"]);
        $AddNewBankSQL = "INSERT INTO checks_bank(Bank_Type,Bank_Name,Bank_Address,Bank_Notes) VALUES ('$AccountType','$Bank_Name','$Bank_Address','$Bank_Notes');";
	} elseif($AccountType == 'supplier') {
		$Bank_Name = mysqli_real_escape_string($db, $_POST["S_Bank_Name"]);
		$Bank_Notes = mysqli_real_escape_string($db, $_POST["S_Bank_Notes"]);
        $AddNewBankSQL = "INSERT INTO checks_bank(Bank_Type,Bank_Name,Bank_Notes) VALUES ('$AccountType','$Bank_Name','$Bank_Notes');";
	}
    
    if(empty($Bank_Name) OR empty($AccountType)) {
        echo "<h3>Warning</h3> Please make sure that all the fields are filled.<br />";
        if(empty($AccountType)) { echo "The account type is not chosen.<br />"; }
        if(empty($Bank_Name)) { echo "The account name is missing.<br />"; }
    } else {
		//Get the SQL needed from the IF statement on the top of the commands
        GDb::execute($AddNewBankSQL);
        $NewBankID = GDb::db()->insert_id;

        $AddNewBankLogSQL = "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','$AccountType','$NewBankID','added','$Bank_Name');";
        GDb::execute($AddNewBankLogSQL);

        if ($NewBankID > 0) {
            echo "<h3>Success</h3> The new account ($Bank_Name) has been added successfully.";
            echo "<meta http-equiv='refresh' content='2;check-banks.php'>";
        } else {
            echo $db->error;
        }        
    }
}


if(isset($_POST['EditCurrentBank'])){
	$BankID = mysqli_real_escape_string($db, $_POST["EditCurrentBank"]);
	$BankType = mysqli_real_escape_string($db, $_POST["BankType"]);
	if($BankType == 'cash') {
		$AccountName = mysqli_real_escape_string($db, $_POST["BankName"]);
		$AccountNotes = mysqli_real_escape_string($db, $_POST["BankNotes"]);
		$UpdateAccountSQL = "UPDATE checks_bank SET Bank_Name='$AccountName',Bank_Notes='$AccountNotes' WHERE Bank_ID=$BankID;";
	} elseif($BankType == 'bank') {
		$AccountName = mysqli_real_escape_string($db, $_POST["BankName"]);
		$BankCheckNumber = mysqli_real_escape_string($db, $_POST["BankCheckNumber"]);
		$BankAddress = mysqli_real_escape_string($db, $_POST["BankAddress"]);
		$TransitCode = mysqli_real_escape_string($db, $_POST["TransitCode"]);
		$RoutingNum = mysqli_real_escape_string($db, $_POST["RoutingNum"]);
		$AccountNum = mysqli_real_escape_string($db, $_POST["AccountNum"]);
		$UpdateAccountSQL = "UPDATE checks_bank SET Bank_Name='$AccountName',Bank_Address='$BankAddress',Bank_Transit='$TransitCode',Bank_Routing='$RoutingNum',Bank_Account_Number='$AccountNum',Bank_Check_Number='$BankCheckNumber' WHERE Bank_ID=$BankID;";
	} elseif($BankType == 'cards') {
		$AccountName = mysqli_real_escape_string($db, $_POST["BankName"]);
		$AccountNumber = mysqli_real_escape_string($db, $_POST["AccountNum"]);
		$UpdateAccountSQL = "UPDATE checks_bank SET Bank_Name='$AccountName',Bank_Account_Number='$AccountNumber' WHERE Bank_ID=$BankID;";
	} elseif($BankType == 'paypal') {
		$AccountName = mysqli_real_escape_string($db, $_POST["BankName"]);
		$AccountAddress = mysqli_real_escape_string($db, $_POST["BankAddress"]);
		$AccountNotes = mysqli_real_escape_string($db, $_POST["BankNotes"]);
		$UpdateAccountSQL = "UPDATE checks_bank SET Bank_Name='$AccountName',Bank_Address='$AccountAddress', Bank_Notes='$AccountNotes' WHERE Bank_ID=$BankID;";
	}
	
    
    if(empty($AccountName)) {
        echo "<h3>Warning</h3> Please make sure that all the fields are filled.<br />";
        if(empty($NewBankName)) { echo "The bank name is missing.<br />"; }
    } else {
        //We will get the SQL command from the above IF statement
        $AddNewBankLogSQL = "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','bank','$BankID','updated','$AccountName');";

        if (GDb::execute($UpdateAccountSQL) && GDb::execute($AddNewBankLogSQL)) {
            echo "<h3>Success</h3> The account ($AccountName) has been updated successfully.";
            echo "<meta http-equiv='refresh' content='2;check-bank-edit.php?id=$BankID'>";
        } else {
            echo $db->error;
        }        
    }
}


if(isset($_POST['Deposit_Add'])){
	$Deposit_To = mysqli_real_escape_string($db, $_POST["Deposit_To"]);
	$Deposit_From = mysqli_real_escape_string($db, $_POST["Deposit_From"]);
	$Deposit_Added_By = mysqli_real_escape_string($db, $_POST["Deposit_Added_By"]);
	$Deposit_Date = mysqli_real_escape_string($db, $_POST["Deposit_Date"]);
	$Deposit_Amount = mysqli_real_escape_string($db, $_POST["Deposit_Amount"]);
	$Deposit_Notes = mysqli_real_escape_string($db, $_POST["Deposit_Notes"]);
	$Deposit_Type = mysqli_real_escape_string($db, $_POST["Deposit_Type"]);
    
    if($Deposit_Type == 'original') {
        $Redirect_To = 'check-bank-edit.php?id='.$Deposit_To;
    } else {
        $Redirect_To = 'contacts-edit.php?id='.$Deposit_Type.'&action=edit&usertype=supplierBalance';
    }
    if(empty($Deposit_Amount)) {
        echo "<h3>Warning</h3> Please make sure that all the fields are filled.<br />";
        if(empty($Deposit_Amount)) { echo "The Deposit Amount is missing.<br />"; }
    } else {
        $AddDepositSQL = "INSERT INTO `checks_bank_deposits`(`Deposit_Date`, `Deposit_To`, `Deposit_From`, `Deposit_Amount`, `Deposit_Notes`, `Deposit_Added_By`) VALUES ('$Deposit_Date','$Deposit_To','$Deposit_From','$Deposit_Amount','$Deposit_Notes','$Deposit_Added_By');";
        GDb::execute($AddDepositSQL);
        $NewDepositID = GDb::db()->insert_id;
        
        $AddDepositLogSQL = "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','account deposit','$NewDepositID','added','$Deposit_Notes');";
        GDb::execute($AddDepositLogSQL);

        if ($NewDepositID > 0) {
            echo "<h3>Success</h3> The deposit has been added successfully.";
            echo "<meta http-equiv='refresh' content='2;$Redirect_To'>";
        } else {
            echo $db->error;
        }        
    }
}


if(isset($_POST['AddNewCheck'])){
	$BankID = mysqli_real_escape_string($db, $_POST["BankID"]);
	$CheckType = mysqli_real_escape_string($db, $_POST["Check_Type"]);
	$CheckNum = mysqli_real_escape_string($db, $_POST["CheckNum"]);
	$CustomerID = mysqli_real_escape_string($db, $_POST["CustomerID"]);
	$PayeeID = mysqli_real_escape_string($db, $_POST["PayeeID"]);
	if($CheckType == 1) { $ContactID = $CustomerID; $PayeeID = 999; } else { $ContactID = $PayeeID; $CustomerID = 999; }
	$CheckAmount = mysqli_real_escape_string($db, $_POST["CheckAmount"]);
	$CheckAmountWordText = mysqli_real_escape_string($db, $_POST["CheckAmountWordText"]);
	$CheckDate = mysqli_real_escape_string($db, $_POST["CheckDate"]);
	$CheckMemo = mysqli_real_escape_string($db, $_POST["CheckMemo"]);
	$CheckNotes = mysqli_real_escape_string($db, $_POST["CheckNotes"]);

    
    if(empty($BankID) OR empty($CustomerID) OR empty($CheckAmount) OR empty($CheckDate)) {
        echo "<h3>Warning</h3> Please make sure that all the fields are filled.<br />";
        if(empty($BankID)) { echo "The bank account is missing.<br />"; }
        if(empty($CustomerID)) { echo "The customer selection is missing.<br />"; }
        if(empty($PayeeID)) { echo "The payee selection is missing.<br />"; }
        if(empty($CheckAmount)) { echo "The check amount is missing.<br />"; }
        if(empty($CheckDate)) { echo "The check date is missing.<br />"; }
    } else {
        $AddNewCheckSQL = "INSERT INTO `checks`(`Check_Customer_ID`, `Check_Type`, `Check_Number`, `Check_Bank_ID`, `Check_Amount`, `Check_Amount_Words`, `Check_Date`, `Check_Note`, `Check_Memo`, `Check_Created_By`) VALUES ('$ContactID', '$CheckType', '$CheckNum', '$BankID', '$CheckAmount', '$CheckAmountWordText', '$CheckDate', '$CheckNotes', '$CheckMemo', '$UserAdminID');";
        GDb::execute($AddNewCheckSQL);
        $NewCheckID = GDb::db()->insert_id;

        $UpdateBankCheckNumberSQL = "UPDATE checks_bank SET Bank_Check_Number=Bank_Check_Number+1 WHERE Bank_ID='$BankID';";
        GDb::execute($UpdateBankCheckNumberSQL);
        
        $AddNewBankLogSQL = "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','check','$NewCheckID','added','$CheckMemo');";
        GDb::execute($AddNewBankLogSQL);

        if ($NewCheckID > 0) {
            echo "<h3>Success</h3> The new check #$NewCheckID has been added successfully.";
            echo "<meta http-equiv='refresh' content='2;check-edit.php?id=$NewCheckID'>";
        } else {
            echo $db->error;
        }        
    }
}


if(isset($_POST['EditCurrentCheck'])){
	$BankID = mysqli_real_escape_string($db, $_POST["BankID"]);
	$CheckID = mysqli_real_escape_string($db, $_POST["EditCurrentCheck"]);
	$CheckNum = mysqli_real_escape_string($db, $_POST["CheckNum"]);
	$CustomerID = mysqli_real_escape_string($db, $_POST["CustomerID"]);
	$CheckAmount = mysqli_real_escape_string($db, $_POST["CheckAmount"]);
	$CheckAmountWordText = mysqli_real_escape_string($db, $_POST["CheckAmountWordText"]);
	$CheckDate = mysqli_real_escape_string($db, $_POST["CheckDate"]);
	$CheckMemo = mysqli_real_escape_string($db, $_POST["CheckMemo"]);
	$CheckNotes = mysqli_real_escape_string($db, $_POST["CheckNotes"]);

    
    if(empty($BankID) OR empty($CustomerID) OR empty($CheckAmount) OR empty($CheckDate)) {
        echo "<h3>Warning</h3> Please make sure that all the fields are filled.<br />";
        if(empty($BankID)) { echo "The bank account is missing.<br />"; }
        if(empty($CustomerID)) { echo "The customer selection is missing.<br />"; }
        if(empty($CheckAmount)) { echo "The check amount is missing.<br />"; }
        if(empty($CheckDate)) { echo "The check date is missing.<br />"; }
    } else {
        $EditCheckSQL = "UPDATE checks SET Check_Bank_ID='$BankID',Check_Number='$CheckNum',Check_Amount='$CheckAmount',Check_Amount_Words='$CheckAmountWordText',Check_Date='$CheckDate',Check_Note='$CheckNotes',Check_Memo='$CheckMemo' WHERE Check_ID=$CheckID;";
        $EditCheckLogSQL = "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','check','$CheckID','edited','#$CheckNum');";

        if (GDb::execute($EditCheckSQL) && GDb::execute($EditCheckLogSQL)) {
            echo "<h3>Success</h3> The check #$CheckNum has been edited successfully.";
            echo "<meta http-equiv='refresh' content='2;check-edit.php?id=$CheckID'>";
        } else {
            echo $db->error;
        }        
    }
}

?>