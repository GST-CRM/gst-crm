<?php

include "../session.php";
include_once __DIR__ . '/../models/products.php' ;

if(isset($_POST['email_template_id'])){
	if($_POST['email_template_id']!=""){
		$template_id = mysqli_real_escape_string($db, $_POST['email_template_id']);
		$template_name = mysqli_real_escape_string($db, $_POST["template_name"]);
	    $template_type_id = mysqli_real_escape_string($db, $_POST["template_type_id"]);
	    $template_subject = mysqli_real_escape_string($db, $_POST["template_subject"]);
	    $template_description = mysqli_real_escape_string($db, $_POST["template_description"]);
	    $template_body = mysqli_real_escape_string($db, $_POST["template_body"]);
	    if ($_POST["status"]==1){ $status = "1"; } else { $status = "0"; }
	    
	    $sql = "UPDATE email_templates_new SET template_name='$template_name', template_type_id='$template_type_id', template_subject='$template_subject', template_description='$template_description', template_body='$template_body', status='$status' WHERE template_id = $template_id";

	    if ($db->multi_query($sql) === TRUE) {
			echo "<h3>Success</h3> The Email Template has been updated successfully!";
			echo "<meta http-equiv='refresh' content='2;email-templates-edit.php?id=$template_id&action=edit'>";
		} else {
			echo $sql . "<br>" . $db->error;
		}
	    $db->close();
	}
}

if(isset($_POST['template_id']) && ($_POST['previewTemplate'] == 'Yes')) {
	$template_id = $_POST['template_id'];
	$sql = "SELECT template_subject, template_body FROM email_templates_new WHERE template_id = $template_id AND status = 1";
	$result = $db->query($sql);
	$resultTemplates = $result->fetch_assoc();
	//print_r($resultTemplates);
	$finalresultTemplates = ['template_subject' => $resultTemplates['template_subject'], 'template_body' => htmlspecialchars_decode($resultTemplates['template_body'], ENT_QUOTES)];
	//print_r($finalresultTemplates);
	echo json_encode($finalresultTemplates);
}
?>