<?php

include_once __DIR__ . '/../models/users.php' ;

$email = $__POST['email'] ;

$user = new Users() ;

$userData = null ;
if( $email ) {
    $userData = $user->get( " EmailAddress='$email' ") ;
}

$fail = false ;

if( strlen($email) < 1 ) {
    //invalid email address
    $fail = true ;
    GUtils::setWarning('Invalid email address.') ;
    return ;    
}
if( !is_array($userData) || strlen($userData['Username']) < 1 && strlen($email) > 0 ) {
    //token invalid 
    $fail = true ;
    GUtils::setWarning('Invalid request, or the email address you have entered is not registered with us.') ;
    return ;
}
if( $email != $userData['EmailAddress'] ) {
    //old passwor mis match.
    $fail = true ;
    GUtils::setWarning('The email address you have entered is not found.') ;
    return ;
}

if( ! $fail ) {
    $token = GUtils::getUniqId() ;
    $data = array(
        'token' => $token,
        'token_type' => 2,
    ) ;
    $where = array (
        'EmailAddress' => $email
    ); 
    
    if( $user->update($data, $where) ) {
        //success. Send Email
        $params = array(
            'forgot_url' => GUtils::domainUrl( 'reset_password.php?t=' . $token ),
            'name' => $userData['fname'] . ' ' . $userData['lname']
        ) ;
        GUtils::sendTemplateMailByCode($email, 'FORGOT_PASSWORD', $params) ;

        GUtils::setSuccess('An email with password recovery information has been sent to the email address.') ;
        GUtils::redirect('login.php') ;
        return ;
    }
}
GUtils::setWarning('Unable to send email.') ;
return ;
