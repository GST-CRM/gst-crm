<?php

include_once __DIR__ . '/../vendor/autoload.php';
include_once __DIR__ . '/../models/email_templates.php';

use PHPMailer\PHPMailer\PHPMailer;

global $CRM_PATH;
$CRM_PATH = $crm_path ;

class GUtils {

    public static $directory     = __DIR__ . '/../uploads/';
    public static $directory_url = 'uploads/';
    public static $FLASH_SUCCESS = 'success';
    public static $FLASH_DANGER  = 'success';
    public static $FLASH_INFO    = 'success';

    public static function requestUri() {
        return $_SERVER['REQUEST_URI'] ;
    }
    public function excerpt($str, $len) {
        if(strlen($str) > $len ) {
            return substr($str, 0, $len) . '...';
        }
        return $str ;
    }
    public static function jsonResponse($data, $die = true) {
        header('Content-Type: application/json');
        echo json_encode($data);
        if ($die) {
            die;
        }
    }
    
    public static function removeParameters($removeParams) {
        $host = $_SERVER['HTTP_HOST'] ;
        $str  = $_SERVER['REQUEST_URI'] ;
        $scheme = $_SERVER['REQUEST_SCHEME'] ;
        
        $url = $scheme . '://'. $host . $str ;
        
        foreach( $removeParams as $k => $v ){
            $url = str_ireplace("$k=$v&", '', $url) ;
            $url = str_ireplace("&$k=$v", '', $url) ;
            $url = str_ireplace("?$k=$v", '?', $url) ;
        }
        return $url ;
    }
    

    public static function jsonOk($data = null, $die = true) {
        if (!is_array($data)) {
            $data = [];
        }
        $data['status'] = 'OK';
        return self::jsonResponse($data, $die);
    }

    public static function jsonFail($data = null, $die = true) {
        if (!is_array($data)) {
            $data = [];
        }
        $data['status'] = 'FAIL';
        return self::jsonResponse($data, $die);
    }

    public static function redirect($url, $die = true) {
        if (headers_sent()) {
            echo '<script type="text/javascript">window.location.href="' . $url . '";</script>';
            die;
        } else {
            header('location:' . $url);
            if ($die) {
                die;
            }
        }
    }

    /**
     * Implode GST address.
     * 
     * @param type $array
     * @param type $glue
     */
    public static function buildGSTAddress($fullAddress, $array, $glue = ",", $newline = "<br/>") {

        if (!isset($array['fname'])) {
            return '';
        }
        if ($fullAddress) {
            $address1 = array(
                'name' => $array['fname'] . ' ' . $array['mname'] . ' ' . $array['lname'],
            );
        }
        $address2 = array(
            'address1' => $array['address1'],
            'address2' => $array['address2'],
            'city'     => $array['city'] . ' ' . $array['state'] . ' ' . $array['zipcode']
        );
        if ($fullAddress) {
            $address = $address1 + $address2;
        } else {
            $address = $address2;
        }

        foreach ($address as $k => $v) {

            if (strlen($v) > 32) {
                $va          = str_split($v, 32);
                $v           = implode($newline, $va);
                $address[$k] = $v;
            }
        }
        return GUtils::implodeIfValue($address, $glue . $newline);
    }

    public static function implodeColumn($array, $columns, $glue = ', ', $columnSeparator = " ") {
        $newArray = [];
        foreach ($array as $one) {
            $item = '';
            foreach ($columns as $column) {
                if ($item) {
                    $item .= $columnSeparator;
                }
                $item .= $one[$column];
            }
            $newArray[] = $item;
        }
        return self::implodeIfValue($newArray, $glue);
    }

    public static function implodeIfValue($array, $glue = ', ') {
        $str     = '';
        $setglue = false;

        foreach ($array as $value) {
            if ($str && $setglue) {
                $str     .= $glue;
                $setglue = false;
            }
            if (strlen(trim($value)) > 0) {
                $str     .= $value;
                $setglue = true;
            }
        }

        return rtrim($str, $glue);
    }

    public static function dayDiff($date1, $date2) {
        $d1=date_create($date1);
        $d2=date_create($date2);
        
        $diff = date_diff($d1,$d2);
        $days = $diff->format('%R%a') ;
        
        return doubleval($days) ;
    }
    /**
     * Date difference in words.
     */
    public static function tellDateDifference($dateString1, $dateString2, $format = 'ymdh', $positive = '%s', $negative = '- %s', $conjunction = ' and ') {

        // Declare and define two dates 
        $date1 = strtotime($dateString1);
        $date2 = strtotime($dateString2);

        // Formulate the Difference between two dates 
        $flag = ($date2 - $date1);
        $diff = abs($date2 - $date1);


        // To get the year divide the resultant date into 
        // total seconds in a year (365*60*60*24) 
        $years = floor($diff / (365 * 60 * 60 * 24));


        // To get the month, subtract it with years and 
        // divide the resultant date into 
        // total seconds in a month (30*60*60*24) 
        $months = floor(($diff - $years * 365 * 60 * 60 * 24) / (30 * 60 * 60 * 24));


        // To get the day, subtract it with years and  
        // months and divide the resultant date into 
        // total seconds in a days (60*60*24) 
        $days = floor(($diff - $years * 365 * 60 * 60 * 24 -
            $months * 30 * 60 * 60 * 24) / (60 * 60 * 24));


        // To get the hour, subtract it with years,  
        // months & seconds and divide the resultant 
        // date into total seconds in a hours (60*60) 
        $hours = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24) / (60 * 60));


        // To get the minutes, subtract it with years, 
        // months, seconds and hours and divide the  
        // resultant date into total seconds i.e. 60 
        $minutes = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);


        // To get the minutes, subtract it with years, 
        // months, seconds, hours and minutes  
        $seconds = floor(($diff - $years * 365 * 60 * 60 * 24 - $months * 30 * 60 * 60 * 24 - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60));


        $ret = '';
        if ($years && strpos($format, "y") !== false) {

            $ret .= $years . ' Year(s) ';
        }
        if ($months && strpos($format, "m") !== false) {
            if ($ret) {
                $ret .= $conjunction;
            }
            $ret .= $months . ' Month(s) ';
        }
        if ($days && strpos($format, "d") !== false) {
            if ($ret) {
                $ret .= $conjunction;
            }
            $ret .= $days . ' Day(s) ';
        }
        if ($hours && strpos($format, "h") !== false) {
            if ($ret) {
                $ret .= $conjunction;
            }
            $ret .= $hours . ' Hour(s) ';
        }
        if ($minutes && strpos($format, "i") !== false) {
            if ($ret) {
                $ret .= $conjunction;
            }
            $ret .= $minutes . ' Minute(s) ';
        }
        if ($seconds && strpos($format, "s") !== false) {
            if ($ret) {
                $ret .= $conjunction;
            }
            $ret .= $seconds . ' Second(s) ';
        }

        if ($flag > 0) {
            return str_replace('%s', $ret, $negative);
        } else {
            return str_replace('%s', $ret, $positive);
        }
    }

    public static function dbNow() {
        return DB::raw('NOW()');
    }

    public static function dbDate() {
        return DB::raw('CURDATE()');
    }

    public static function dbTime() {
        return DB::raw('TIME()');
    }

    public static function clientDate($dt = null, $format = 'm/d/Y') {
        return self::clientDateTime($dt, $format);
    }

    public static function clientTime($dt = null, $format = 'h:i a') {
        return self::clientDateTime($dt, $format);
    }

    public static function clientDateTime($dt = null, $format = 'm/d/Y h:i a') {
        if ($dt === null) {
            return '';
        }
        if ($dt) {
            $t = strtotime($dt);

            if ($t && $t != -19800 && $dt != '0000-00-00' && $dt != '0000-00-00 00:00:00') {
                return Date($format, $t);
            }
        }
        return '';
    }

    public static function mysqlDate($dt = null) {
        return self::mysqlDateTime($dt, 'Y-m-d');
    }

    public static function mysqlDateTime($dt = null, $format = 'Y-m-d H:i:s') {
        if ($dt === null) {
            return '';
        }

        if ($dt) {
            return Date($format, strtotime($dt));
        }
        return '';
    }

    public static function formatMoney($money, $prefix = '$', $color = false) {
        if( $money === '' || $money === null ) {
            return $money ;
        }
        if ($money < 0) {
            if($color === true) {
                return '<span style="color:red;">-' . $prefix . number_format(abs($money), 2).'</span>';
            } else {
                return '-' . $prefix . number_format(abs($money), 2);
            }
            
        } else {
            return $prefix . number_format($money, 2);
        }
    }

    /*
     * Base url path
     */

    public static function getBaseUrl() {
        // output: /myproject/index.php
        $currentPath = $_SERVER['PHP_SELF'];

        // output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index ) 
        $pathInfo = pathinfo($currentPath);

        // output: localhost
        $hostName = $_SERVER['HTTP_HOST'];

        // output: http://
        $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 5)) == 'https' ? 'https' : 'http';

        // return: http://localhost/myproject/
        return $protocol . '://' . $hostName . $pathInfo['dirname'] . "/";
    }

    /**
     * Downlaod file path in uploads directory
     */
    public static function doDownload($file, $prefix = '') {
        return self::getBaseUrl() . self::$directory_url . $prefix . $file;
    }

    /**
     *
     * @param type $field
     * @param type $config
     * - allowed_types : file extensions allowed. jpg | gif | png
     * - max_size : max file size
     * - upload_path : upload directory prefix
     * - file_name : target file path
     * - overwrite : overwrite or not, true to overwrite
     * @return boolean true on success.
     */
    public static function doUpload($field, &$config) {
        $uploadPath = self::$directory;

        if (is_array($field)) {
            $FILE = $field;
        } else {
            $FILE = $_FILES[$field];
        }

        if ($FILE['error'] > 0) {
            return false;
        }
        $inFilename = $FILE['name'];
        $extArray   = explode(".", $inFilename);
        $inExt      = end($extArray);
        if ($inExt) {
            $inExt = strtolower($inExt);
        }
        $inSize = $FILE['size'];
        if (@$config['upload_path']) {
            $uploadPath = self::$directory . $config['upload_path'];
        }
        //check size
        if (@$config['max_size']) {
            if ($config['max_size'] < $inSize) {
                return false;
            }
        }
        //check ext
        if (@$config['allowed_types']) {
            $config['allowed_types'] = strtolower($config['allowed_types']);
            $types                   = explode('|', $config['allowed_types']);
            if (count($types) > 0) {
                if (!in_array($inExt, $types)) {
                    return false;
                }
            }
        }
        //has any file suggestion
        if (@$config['file_name']) {
            $destFile = $config['file_name'];
        } else {
            $destFile = chr(rand(65, 90)) . uniqid(time()) . '.' . $inExt;
        }

        //check target file exists ?
        $config['file_name'] = $destFile;
        $destPath            = $uploadPath . $destFile;

        if (!@$config['overwrite']) {
            if (file_exists($destPath)) {
                return false;
            }
        }

        //move file
        return move_uploaded_file($FILE['tmp_name'], $destPath);
    }

    public static function flashMessage() {
        GUtils::sessionStart();

        $msg = '';
        if (isset($_SESSION['flash-message'])) {
            $msg = $_SESSION['flash-message'];
        }
        $type = '';
        if (isset($_SESSION['flash-message'])) {
            $type = $_SESSION['flash-message-type'];
        }

        unset($_SESSION['flash-message']);
        unset($_SESSION['flash-message-type']);

        if (!$msg) {
            return '';
        }
        return "<div style='margin-top:25px;' class='alert alert-$type background-$type'>
            <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
            <i class='far fa-times-circle text-white'></i>
            </button>
                <strong>$msg</strong>
            </div>";
    }

    public static function setWarning($msg) {
        GUtils::sessionStart();

        $_SESSION['flash-message']      = $msg;
        $_SESSION['flash-message-type'] = 'warning';
    }

    public static function setSuccess($msg) {
        GUtils::sessionStart();

        $_SESSION['flash-message']      = $msg;
        $_SESSION['flash-message-type'] = 'success';
    }

    public static function setInformation($msg) {
        GUtils::sessionStart();

        $_SESSION['flash-message']      = $msg;
        $_SESSION['flash-message-type'] = 'info';
    }

    public static function setFlashMessage($msg, $type) {
        GUtils::sessionStart();

        $_SESSION['flash-message']      = $msg;
        $_SESSION['flash-message-type'] = $type;
    }

    public static function sessionStart() {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

    public static function sessionActive() {
        return (session_status() === PHP_SESSION_ACTIVE ? TRUE : FALSE);
    }

    public static function sendTemplateMailByCode($to, $template_code, $params = null, $attachmentName = null, $attachmentData = null, $userLoginEmail = null) {
        $t = (new Email_Templates())->get(['template_code' => $template_code] );

        //get footer email template to add logo in it
        $templateFooter = (new Email_Templates())->get(" status=1 AND template_type_id > 0 AND template_name = 'Footer Email Template - Dont Delete' " ) ;
        $t['template_body'] .= str_replace('Â ', ' ', $templateFooter["template_body"]);

        return self::sendTemplateMail($to, $t['template_subject'], $t['template_body'], $params, $attachmentName, $attachmentData, $userLoginEmail);
    }
    public static function sendTemplateMailById($to, $templateId, $params = null, $attachmentName = null, $attachmentData = null, $userLoginEmail = null, $cc = array(), $bcc = array() ) {
        //$t = (new Email_Templates())->get(['template_code' => $template_code] );
        $t = (new Email_Templates())->getById($templateId);

        //get footer email template to add logo in it
        $templateFooter = (new Email_Templates())->get(" status=1 AND template_type_id > 0 AND template_name = 'Footer Email Template - Dont Delete' " ) ;
        $t['template_body'] .= str_replace('Â ', ' ', $templateFooter["template_body"]);
        // echo $t['template_body'];
        // die();
        //get footer email template to add logo in it - END

        return self::sendTemplateMail($to, $t['template_subject'], $t['template_body'], $params, $attachmentName, $attachmentData, $userLoginEmail, $cc , $bcc );
    }

    public static function sendTemplateMail($to, $subject, $template, $params = null, $attachmentName = null, $attachmentData = null, $userLoginEmail = null, $cc = array(), $bcc = array() ) {
        //replace template variables with actual values.
        if (is_array($params)) {
            foreach ($params as $k => $v) {
                $subject  = str_ireplace('{' . $k . '}', $v, $subject);
                $template = str_ireplace('{' . $k . '}', $v, $template);
            }
        }

        return self::sendMail($to, $subject, $template, $attachmentName, $attachmentData, $userLoginEmail, $cc , $bcc  );
    }

    public static function sendMail($to, $subject, $body, $attachmentName = null, $attachmentData = null, $userLoginEmail = array(), $cc = array(), $bcc = array() ) {
        
        if( !empty($bcc) && !empty($userLoginEmail) ) {
            trigger_error("Can't use user login email & bcc at the same time", E_ERROR) ;
        }
        
        $tobcc = array();
         if (!empty($userLoginEmail)) {
            $tobcc = $to;
            $to = $userLoginEmail;
        }

        
        $mail = new PHPMailer(false); // Passing `true` enables exceptions
        
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = "mail.goodshepherdtravel.com";
        $mail->Port = 465; // or 587
        $mail->IsHTML(true);
        $mail->Username = "donotreply@goodshepherdtravel.com";
        $mail->Password = "Wa{mji@T#u24";
    
        $mail->addCustomHeader('MIME-Version: 1.0');
        //$mail->addCustomHeader('Content-Type: text/html; charset=ISO-8859-1');
        //$mail->addCustomHeader('Content-Type: text/html; charset=utf-8');
        $mail->setFrom('donotreply@goodshepherdtravel.com', 'Good Shepherd Travel');
        $mail->addReplyTo('donotreply@goodshepherdtravel.com', 'Good Shepherd Travel');
        //Attachments
        $attachmentSet = [];
        if( $attachmentName != null ) {
            if( is_array($attachmentName) ) {
                $attachmentSet = $attachmentName ;
            }
            else {
                $attachmentSet[] = ['name' => $attachmentName, 'data' => $attachmentData] ;
            }

            foreach( $attachmentSet as $one ) {
                $attachData = $one['data'] ;
                $attachName = $one['name'] ;
                if ($attachData && $attachName) {
                    $path_parts = pathinfo($attachData);
                    $ext = $path_parts['extension'];
                    if ($ext == "png" || $ext == "jpg" || $ext == "gif" || $ext == "jpeg" || $ext == "pdf")
                    {
                        $mail->addAttachment($attachData, $attachName);
                    }
                    else
                    {
                        $mail->addStringAttachment($attachData, $attachName);
                    }
                }
            }
        }

        if (is_array($to)) {
            foreach ($to as $k => $v) {
                $mail->addAddress($v);
            }
        } else {
            $mail->addAddress($to);
        }

        if (is_array($tobcc) && (!empty($tobcc))) {
            foreach ($tobcc as $v) {
                $mail->addBCC($v);
            }
        }

        if (is_array($bcc) && (!empty($bcc))) {
            foreach ($bcc as $v) {
                $mail->addBCC($v);
            }
        }

        if (is_array($cc) && (!empty($cc))) {
            foreach ($cc as $v) {
                $mail->addCC($v);
            }
        }

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $body;
        $mail->AltBody = strip_tags($body);
        
        return $mail->send();
    }

    /**
     * Return a random string below 32 char length ;
     * 
     * @param type $length
     * @return type
     */
    public static function generatePassword($length) {
        $string1 = md5(uniqid(rand(), true) . rand());
        $string2 = "!@#$%^&*()_+|-+?:abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

        $string = str_shuffle($string1 . $string2);

        return substr($string, 0, $length);
    }

    public static function getUniqId($len = null) {
        $uid = md5(uniqid(rand(), true) . rand());
        if( $len ) {
            return substr($uid, $len) ;
        }
        return $uid ;
    }

    /**
     * Get path of a file from root dir
     * 
     * @param string $file file to retrieve
     * @return string file path
     */
    public static function baseUrl($file = null) {
        $dirname = dirname($_SERVER['SCRIPT_NAME']);
        if ($dirname == '/') {
            $dirname = '';
        }

        return ($dirname . '/' . ltrim($file, '/'));
    }

    /**
     * Get a file path.
     * 
     * @param string $file filename
     * @return string file path.
     */
    public static function fileUrl($file) {
        $path = dirname($_SERVER['SCRIPT_FILENAME']);
        $path .= '/';
        return ($path . $file);
    }

    /**
     * Get a path in site
     * 
     * @param string $path uri to resolve
     * @param bool $relative relative site url ( no http://hostname )
     * @return string url
     */
    public static function siteUrl($path = null, $relative = true) {
        if( ! $relative ) {
            return $this->domainUrl($path) ;
        }
        $host    = $_SERVER['SERVER_NAME'];
        $host = rtrim($host, "/") ;
        $host    .= '/';
        $dirname = dirname($_SERVER['SCRIPT_NAME']);
        $dirname = rtrim($dirname, '/') ;
        if ($dirname) {
            $dirname .= '/' ;
        }
        return ( (!$relative) ? 'http://' . $host : '') . $dirname . $path;
    }

    /**
     * Get a path in site
     * 
     * @param string $path uri to resolve
     * @param bool $relative relative site url ( no http://hostname )
     * @return string url
     */
    public static function domainUrl($path = null) {
        /*
        $host = rtrim($_SERVER['SERVER_NAME'], '//');
        $host .= '/';
//        $pi    = pathinfo($_SERVER['SCRIPT_NAME']);
//        $pathx = ltrim($pi['dirname'], '//');
        return ( (self::isHttps() ? 'https://' : 'http://') . $host . $path );
         * 
         */
        global $CRM_PATH ;
        $url = $CRM_PATH ;
        //if the call hits in /inc folder
        $url = preg_replace('/inc\/$/', '', $url) ;
        $url = rtrim($url, "/") ;
        $url .= "/" ;
        
        if( $path ) {
            $url .= $path ;
        }
        return $url ;
    }
    
    public static function isHttps()
    {
        if ( ! empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off')
        {
            return TRUE;
        }
        elseif (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && strtolower($_SERVER['HTTP_X_FORWARDED_PROTO']) === 'https')
        {
            return TRUE;
        }
        elseif ( ! empty($_SERVER['HTTP_FRONT_END_HTTPS']) && strtolower($_SERVER['HTTP_FRONT_END_HTTPS']) !== 'off')
        {
            return TRUE;
        }

        return FALSE;
    }
    
    public static function getSession($key) {
        self::sessionStart() ;
        if( isset($_SESSION[$key])) {
            return $_SESSION[$key] ;
        }
        return null ;
    }
    
    public static function maskCount($value) {
        $c = 0 ;
        for($i = 1; $i <= $value ; $i = $i << 1 ) {
            if( $value & $i ) {
                $c ++ ;
            }
        }
        return $c ;
    }
    
    public static function firstMask($value) {
        for($i = 1; $i <= $value ; $i = $i << 1 ) {
            if( $value & $i ) {
                return $i ;
            }
        }
        return 0 ;
    }
    
    
    public static function groupByValue($array, $column) {
        $ret = [] ;
        foreach( $array as $v ) {
            $newKey = $v[$column] ;
            if( ! isset($ret[$newKey]) ) {
                $ret[ $newKey ] = [] ;
            }
            $ret[ $newKey ] = $v ; 
        }
        return $ret ;
    }
    /**
     * In a 2D array find a value matching given key. Return the found array.
     * 
     * @param type $array
     * @param type $key
     * @param type $value
     * @return type
     */
    public static function array2DFindByValue( $array, $key, $value ) {
        foreach( $array as $k => $v ) {
            if( $v[$key] == $value ) {
                return $v ;
            }
        }
        return null ;
    }
    public static function contactName($contactData) {
        return $contactData['fname'] . ' ' . $contactData['mname'] . ' ' . $contactData['lname'] ;
    }
    public static function NumToWords( $num ) {
        $decones = array(
            '01' => 'One',
            '02' => 'Two',
            '03' => 'Three',
            '04' => 'Four',
            '05' => 'Five',
            '06' => 'Six',
            '07' => 'Seven',
            '08' => 'Eight',
            '09' => 'Nine',
            10 => 'Ten',
            11 => 'Eleven',
            12 => 'Twelve',
            13 => 'Thirteen',
            14 => 'Fourteen',
            15 => 'Fifteen',
            16 => 'Sixteen',
            17 => 'Seventeen',
            18 => 'Eighteen',
            19 => 'Nineteen'
        );
        $ones = array(
            0 => ' ',
            1 => 'One',
            2 => 'Two',
            3 => 'Three',
            4 => 'Four',
            5 => 'Five',
            6 => 'Six',
            7 => 'Seven',
            8 => 'Eight',
            9 => 'Nine',
            10 => 'Ten',
            11 => 'Eleven',
            12 => 'Twelve',
            13 => 'Thirteen',
            14 => 'Fourteen',
            15 => 'Fifteen',
            16 => 'Sixteen',
            17 => 'Seventeen',
            18 => 'Eighteen',
            19 => 'Nineteen'
        );

        $tens = array(
            0 => '',
            2 => 'Twenty',
            3 => 'Thirty',
            4 => 'Forty',
            5 => 'Fifty',
            6 => 'Sixty',
            7 => 'Seventy',
            8 => 'Eighty',
            9 => 'Ninety'
        );

        $hundreds = array(
            'Hundred',
            'Thousand',
            'Million',
            'Billion',
            'Trillion',
            'Quadrillion'
        );
        //limit t quadrillion
        $num = number_format( $num, 2, '.', ',' );
        $num_arr = explode( '.', $num );
        $wholenum = $num_arr[0];
        $decnum = $num_arr[1];
        $whole_arr = array_reverse( explode( ',', $wholenum ) );
        krsort( $whole_arr );
        $rettxt = '';

        foreach ( $whole_arr as $key => $i ) {
            if ( $i < 20 ) {
                $rettxt .= $ones[$i];
            } elseif ( $i < 100 ) {
                $rettxt .= $tens[substr( $i, 0, 1 )];
                $rettxt .= ' '.$ones[substr( $i, 1, 1 )];
            } else {
                $rettxt .= $ones[substr( $i, 0, 1 )].' '.$hundreds[0];
                $rettxt .= ' '.$tens[substr( $i, 1, 1 )];
                $rettxt .= ' '.$ones[substr( $i, 2, 1 )];
            }
            if ( $key > 0 ) {
                $rettxt .= ' '.$hundreds[$key].' ';
            }
        }

        if ( $decnum > 0 ) {
            $rettxt .= ' and '.$decnum.'/100';
        }
        $rettxt = $rettxt.'****';
        return $rettxt;
    }
    public static function forgotMail($email, $token, $name, $redirectAfter = false ) {
        //success. Send Email
        $params = array(
            'forgot_url' => GUtils::domainUrl( 'reset_password.php?t=' . $token ),
            'name' => $name
        ) ;
        GUtils::sendTemplateMailByCode($email, 'FORGOT_PASSWORD', $params) ;

        GUtils::setSuccess('An email with password recovery information has been sent to the email address.') ;
        if( $redirectAfter ) {
            GUtils::redirect('login.php') ;
        }
        return ;
    }
}
