<?php 
include "../session.php";
$PageTitle      = "Manifest";
$groupname      = $_REQUEST["tourname"];
$groupid        = $_REQUEST["groupid"];

$query          = "SELECT * FROM customer_account cus JOIN contacts co ON co.id=cus.contactid WHERE cus.tourid=$groupid";
$result         = $db->query($query); 
$heading        = $groupname." - #".$groupid;
?>
<style>
#reportsDiv .special-heading{
    color: #dc3545;
}

#reportsDiv .main_td{
    border-width: 4px;
    border-style: solid;
    border-color: #342f21;
    height: 288px;
}
#reportsDiv h5{
    font-size: 20px;
    font-weight: normal;
    line-height: 60px;
    display:block;
}
</style>

<div id="results"></div>
<div class="row">
    <div class="col-md-12">
                <h3 class="text-center">Manifest</h3>
                <a id="manifestPrintBtn" href="javascript:void(0)" class="btn waves-effect waves-light btn-success" style="float:right;padding: 3px 13px;">Print</a>
                <a href="javascript:void(0)" download class="btn waves-effect waves-light btn-inverse mr-2" style="float:right;padding: 3px 13px;" onclick="download_report('<?php echo $groupname; ?>','<?php echo $groupid; ?>')">Download</a>
                <a href="javascript:void(0);" onclick = "return emailPDFconfirmManifest();" class="btn waves-effect waves-light btn-inverse mr-2" style="float:right;padding: 3px 13px;">Email Excel</a>
				<br />
				<br />
                <div id="manifestDiv" class="dt-responsive table-responsive">
                    <table id="no-btn" class="table table-hover table-striped table-bordered dataTable responsive no-footer nowrap" data-page-length="100" style="width:100%;">
                        <thead>
                            <tr>
                                <th>SNo.</th>
                                <th>First Name</th>
                                <th>Middle Name</th>
                                <th>Last Name</th>
                                <th>Date of Birth</th>
                                <th>Gender</th>
                                <th>Citizenship</th>
                                <th>Passport #</th>
                                <th>Passport Expiry Date</th>
                                <th>Purchased separate ticket</th>
                                <th>Known Traveller #</th>
                                <th>Frequent Flyer</th>
                                <th>Need Extension?</th>
                                <th>Need Transfer?</th>
                                <th>Airline Special Requests</th>
                                <th>Meals Requests</th>
                                <th>Allergies Requests</th>
                                <th>Notes</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if ($result->num_rows > 0) 
                            { 
                                $i = 1;
								$CustomerStyle = "";
                                while ($row=$result->fetch_assoc()) { 
								if($row['status'] == 0) { $CustomerStyle = "style='color:red;'"; } else { $CustomerStyle = ""; } 
								
								?>
                                    <tr <?php echo $CustomerStyle; ?>>
                                        <td><?php echo $i?></td>
                                        <td><?php echo $row["fname"]?></td>
                                        <td><?php echo $row["mname"]?></td>
                                        <td><?php echo $row["lname"]?></td>
                                        <td><?php echo date('m/d/Y', strtotime($row["birthday"]));?></td>
                                        <td><?php echo $row["gender"]?></td>
                                        <td><?php echo $row["citizenship"]?></td>
                                        <td><?php echo $row["passport"]?></td>
                                        <td><?php echo date('m/d/Y', strtotime($row["expirydate"]));?></td>
                                        <td><?php if($row["hisownticket"] == 1) { echo "Yes"; } else { echo "No"; } ?></td>
                                        <td><?php echo $row["travelerid"]?></td>
                                        <td><?php echo $row["flyer"]?></td>
                                        <td><?php if($row["Extension"]==1) { echo "Yes"; } else { echo "No"; } ?></td>
                                        <td><?php if($row["Transfer"]==1) { echo "Yes"; } else { echo "No"; } ?></td>
                                        <td><?php echo $row["specialtext"]?></td>
                                        <td><?php echo $row["Meals_Request"]?></td>
                                        <td><?php echo $row["Allergies_Request"]?></td>
                                        <td><?php echo $row["notes"]?></td>
                                    </tr>
                                    <?php
                                    $i++;
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
    </div>
</div>


<script type="text/javascript">
    function emailPDFconfirmManifest(){
        var divExtra = "<input type='checkbox' name='tour-leader' value='tour-leader' /> Tour Leader &nbsp;&nbsp;" +
                        "<input type='checkbox' name='airline' value='airline' /> Airline Supplier &nbsp;&nbsp;" +
                        "<input type='checkbox' name='agent'  value='agent'/> Agent" ;
        divExtra = encodeURIComponent(divExtra) ;
        showConfirm( "groups-edit.php?action=edit&tab=reports&report_slug=Manifest&email_report=yes&name=<?php echo $groupName;?>&id=<?php echo $tourid; ?>", "Send", "Are you sure you want to sent this report ?", "Please Confirm", "modal-md", divExtra);
    }

    $(document).ready(function()
    {
        $("#manifestPrintBtn").click(function()
        {
            var yourDOCTYPE = "<!DOCTYPE html>"; // your doctype declaration
            var printPreview = window.open('about:blank', 'print_preview', "resizable=yes,scrollbars=yes,status=yes");
            var printDocument = printPreview.document;
            printDocument.open();
            printDocument.write(yourDOCTYPE+'<html><style> td,th {border:1px solid black}</style>')
            printDocument.write(
                       "<div class='jsdiv'>"+
                           $("#manifestDiv").html()+
                       "</div></html>");
            printPreview.print();
        });

        $('#no-btn').DataTable({
            dom: 'Bfrtip',
            buttons:[]
        });
        
    });

    function download_report(groupname,groupid)
    {
        window.location = "excel.php?type=manifest&name="+groupname+"&groupid="+groupid;
    }
</script>