<?php

    
if( $CALLING_PAGE == 'sales-invoices.php' ) {
    $ipp = 20 ;
    $BASE_GROUP_URL = $CALLING_PAGE . '?' ;
}
else {
    $ipp = 500 ;
    $BASE_GROUP_URL = $CALLING_PAGE . '&' ;
}
?>
                    <div class="col-md-2">
                        <span style="margin-left: 30px;float:left; ">
                            <select name="batch-invaction" onchange="return batchSubmit(this);" class="form-control form-control-default fill gst-invoice-batchaction"
                                    disabled="disabled">
                                <option value="">Batch Action</option>
                                <option value="S">Send Invoices</option>
                                <option value="P">Print Invoices</option>
                            </select>
                        </span>
                    </div>
                    <div onclick="window.location.href='<?php echo $BASE_GROUP_URL; ?>filter=total_due';" class="col-md-2 <?php echo (($class == 'total_due') ? 'gst-active-tab' : '');?>">
                        <div class="col-md-12 gst-summary-heading">Total Unpaid</div>
                        <div  class="col-md-12 gst-summary-bg bg-silver">
                            <div class="bg-warning" style="z-index:1;position: absolute; bottom: 0; left: 0; height: 100%; width: <?php echo $first;?>%;"></div>
                            <div class="bg-danger" style="z-index:2;position: absolute; bottom: 0; left: <?php echo $first;?>%; width: <?php echo $second;?>%; height: 100%;"></div>
                            <span style="z-index:3; position: relative;"><?php echo GUtils::formatMoney($TotalDue) ;?></span>
                        </div>
                    </div>
                    <div onclick="window.location.href='<?php echo $BASE_GROUP_URL; ?>filter=not_yet_due';"  class="col-md-2 <?php echo (($class == 'not_yet_due') ? 'gst-active-tab' : '');?>">
                        <div class="col-md-12 gst-summary-heading">Not Due Yet</div>
                        <div class="col-md-12 gst-summary-bg bg-warning"><?php echo GUtils::formatMoney($NotYetDue) ;?></div>
                    </div>
                    <div onclick="window.location.href='<?php echo $BASE_GROUP_URL; ?>filter=over_due';" class="col-md-2 <?php echo (($class == 'over_due') ? 'gst-active-tab' : '');?>">
                        <div class="col-md-12 gst-summary-heading">Overdue</div>
                        <div  class="col-md-12 gst-summary-bg bg-danger"><?php echo GUtils::formatMoney($OverDue) ;?></div>
                    </div>
                    <div onclick="window.location.href='<?php echo $BASE_GROUP_URL; ?>filter=paid';" class="col-md-2 <?php echo (($class == 'paid') ? 'gst-active-tab' : '');?>">
                        <div class="col-md-12 gst-summary-heading">Paid</div>
                        <div  class="col-md-12 gst-summary-bg bg-success">
                            <?php echo GUtils::formatMoney($TotalAmount) ;?>
                            <div style="background: white; opacity: 0.3; z-index:1;position: absolute; bottom: 0; right: 0%; height: 100%; width: <?php echo $third;?>%;"></div>
                        </div>
                    </div>

                    <br />
                    <br />
                    <br />
                    <div class="col-md-12">
                        <div class="dt-responsive table-responsive">
                            <div class="col-md-3 p-0 pb-2" style="margin-top:15px;padding-left:45px;">
                                <div class="input-group input-group-sm mb-0">
                                    <span class="input-group-prepend mt-0">
                                        <label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
                                    </span>
                                    <input type="text" class="outsideBorderSearchEx form-control" placeholder="Search...">
                                </div>
                            </div>
                            
                            <table id="idSalesInvoiceTable" class="table gst-table table-hover table-striped table-bordered nowrap responsive"
                                   data-page-length="<?php echo $ipp;?>" style="width:100%;">
                                <thead>
                                <tr>
                                    <th class="gst-no-sort gst-has-events ">
                                        <input type="checkbox" class="gst-invoice-cb-parent" />
                                    </th>
                                    <th>Invoice #</th>
                                    <th>Customer Name</th>
                                    <th>Date</th>
                                    <th>Due Date</th>
                                    <th>Total</th>
                                    <th>Balance</th>
                                    <th>Status</th>
                                    <th>Updated Time</th>
                                    <?php $invoiceActions = AclPermission::actionAllowed('Action', 'sales-invoice.php') ; 
                                    if( $invoiceActions ) {
                                    ?>
                                    <th class="gst-no-sort">Actions</th>
                                    <?php } ?>
                                </tr>
                                </thead>
                                <?php /*
                                <tbody>
                                <?php
                                foreach ($records as $row) {
									
									//To get the total of the invoice lines for this invoice!
									$InvoiceID = $row["Customer_Invoice_Num"];
									$CustomerzID = $row["id"];
									$InvoiceTotalSQL = "SELECT SUM(Invoice_Line_Total) AS Invoice_Total FROM customer_invoice_line WHERE Customer_Invoice_Num=$InvoiceID";
									$InvoiceTotalResult = $db->query($InvoiceTotalSQL);
									$InvoiceTotalCount = $InvoiceTotalResult->num_rows;
									if($InvoiceTotalCount > 0) {
										$InvoiceTotalData = $InvoiceTotalResult->fetch_row();
										$InvoiceTotalAmount = $InvoiceTotalData[0];
									} else {
										$InvoiceTotalAmount = 0;
									}
									
									//To get the balance of this invoice minus the total paid
                                    //$balance = $row["Invoice_Amount"] - $row["total"];
                                    $charges = $row["charges"] ;
                                    $totalPaymentAmount = $row["total"] ;
                                    $balance = ($InvoiceTotalAmount+ $charges) - $totalPaymentAmount ;
                                    $name = $row['fname'] . ' ' . $row['lname'];
                                    //$name = $row['fname'] . ' ' . $row['mname'] . ' ' . $row['lname'];
                                    $nameAppend = ((strlen(trim($name)) > 0) ? $name : '');
                                    
                                    $daysPeriod = $row['trip_start_days'] ;
                                    $cancellationInfo = CustomerRefund::calculateRefund(($InvoiceTotalAmount+ $charges), $totalPaymentAmount, $daysPeriod) ;
                                    
                                    //name & email
                                    $name_email = '<b>' . $nameAppend  . (isset($row['email']) ? ' ('.$row['email'] .')' : '') . '</b>' ;
                                    ?>
                                    <tr>
                                        <td><input name="cbInvoice[]" value="<?php echo $InvoiceID;?>" type="checkbox" class="gst-invoice-cb-children gst-invoice-cb"/></td>
                                        <td>
                                            <?php $invoiceEdit = AclPermission::pageAllowed('sales-invoice-add.php') ; 
                                            if( $invoiceEdit) { ?>
                                            <a style="color: #0000EE;" href="sales-invoice-add.php?invid=<?php echo $row["Customer_Invoice_Num"]; ?>">
                                            <?php } ?>
                                                <?php echo $InvoiceID; ?>
                                                <?php if( $invoiceEdit) { ?>
                                            </a>
                                            <?php } ?>
                                        </td>
                                        <td>
                                            <?php $contactEdit = AclPermission::pageAllowed('contacts-edit.php') ; 
                                            if( $contactEdit) { ?>
                                            <a style="color: #0000EE;" href="contacts-edit.php?id=<?php echo $CustomerzID; ?>&action=edit&usertype=customer">
                                            <?php } ?>
                                                <?php echo $nameAppend; ?>
                                                <?php if( $contactEdit ) { ?>
                                            </a>
                                                <?php } ?>
                                        </td>
                                        <td><?php echo GUtils::clientDate($row["Add_Date"]); ?></td>
                                        <td><?php echo GUtils::clientDate($row["Due_Date"]); ?></td>
                                        <td class="text-right"><?php echo GUtils::formatMoney($InvoiceTotalAmount); //GUtils::formatMoney($row["Invoice_Amount"]); ?></td>
                                        <td class="text-right"><?php 
                                        //delinquent
                                        if( $row['Status'] == CustomerInvoice::$CANCELLED ) {
                                            echo GUtils::formatMoney( 0 - $row['Pending_Refund_Amount'] ); 
                                        }
                                        else {
                                            echo GUtils::formatMoney($balance ); 
                                        }
                                        
                                        ?></td>
                                        <td class="gst-nowrap-min"><?php
                                        if( $row['Status'] == CustomerInvoice::$CANCELLED ) {
                                            echo '<span style="color:darkred">Cancelled</span>' ;
                                        }
                                        else {
                                            if ($balance > 0) {
//                                                echo GUtils::tellDateDifference(Date('Y-m-d H:i:s'), $row["Due_Date"], 'ymd', 'Due In %s', 'Overdue %s');
                                                if( $row['due_diff'] > 0 ) {
                                                    echo 'Due ' . $row['due_diff'] . ' Day(s)' ;
                                                }
                                                else if( $row['due_diff'] == 0 ) {
                                                    echo 'Due Today' ;
                                                }
                                                else {
                                                    echo '<span style="color:red">Past Due ' . abs($row['due_diff']). ' Day(s)</span>' ;
                                                }
                                            }
                                            else if( $balance == 0 ) {
                                                echo '<span style="color:green">Invoice Paid</span>' ;
                                            }
                                            else {
                                                echo "<a style='color: #0000EE;' href='payment-add.php?invoice=" . $row["Customer_Invoice_Num"] . "'>Issue Refund</a>" ;
                                            }
                                        }
                                            ?></td>
                                        <?php 
                                            if( $invoiceActions) { ?>
                                        <td >
                                            <div style="width: 150px;" class="btn-group gst-btns ">
                                                <?php 
                                                
                                                //The first button decision {
                                                if( $row['Status'] == CustomerInvoice::$ACTIVE || $row['Status'] == CustomerInvoice::$JOINT ) { ?>
                                                <span onclick="window.location.href='payments-add.php?invoice=<?php echo $row["Customer_Invoice_Num"]; ?>';"
                                                        class=" btn-transparent">Receive Payment
                                                </span>
                                                <?php 
                                                }
                                                else if( $row['Status'] == CustomerInvoice::$CANCELLED && floatval($row['Refund_Amount']) > 0 && $row['Refund_Status'] == 1 ) {
                                                ?>
                                                <span onclick="window.location.href='refund_receipt_add.php?invid=<?php echo $row["Customer_Invoice_Num"]; ?>';"
                                                        class=" btn-transparent">Issue Refund
                                                </span>
                                                <?php
                                                }
                                                else if( $row['Status'] == CustomerInvoice::$CANCELLED && ( $row['Refund_Status'] != 1 || floatval($row['Refund_Amount']) == 0 ) ) {
                                                ?>
                                                <span onclick="window.location.href='sales-invoice-add.php?invid=<?php echo $row["Customer_Invoice_Num"]; ?>';"
                                                        class=" btn-transparent">Edit / View
                                                </span>
                                                <?php
                                                }
                                                else if( $row['Status'] == CustomerInvoice::$VOID ) {
                                                    ?>
                                                <button onclick = 'deleteConfirm( "sales-invoices.php?invaction=delete&invid=<?php echo $row["Customer_Invoice_Num"]; ?>")' type="button" class="btn btn-danger">
                                                    Delete
                                                </button>
                                                    <?php
                                                 }
                                                 //} End of first button
                                                 ?>
                                                
                                                <?php
                                                if( $row['Status'] == CustomerInvoice::$CANCELLED || $row['Status'] == CustomerInvoice::$ACTIVE || $row['Status'] == CustomerInvoice::$JOINT ) { ?>
                                                <span class=" btn-transparent dropdown-toggle"
                                                        data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </span>
                                                <ul class="dropdown-menu" role="menu" style="white-space: nowrap">
                                                    <?php 
                                                    //Already show as main button
                                                    if( ! ($row['Status'] == CustomerInvoice::$CANCELLED && ( $row['Refund_Status'] != 1 || floatval($row['Refund_Amount']) == 0 )) ) { ?>
                                                    <li><a href="sales-invoice-add.php?invid=<?php echo $row["Customer_Invoice_Num"]; ?>">Edit/View</a></li>
                                                    <?php } ?>

                                                    <?php if( $row['Status'] != CustomerInvoice::$CANCELLED ) { ?>
                                                    <li><a href="javascript:void(0);" data-name="<?php echo $name; ?>" data-id="<?php echo $row["Customer_Invoice_Num"]; ?>"
                                                           data-cancel-amount="<?php echo GUtils::formatMoney( $cancellationInfo['Cancellation_Charge']);?>" 
                                                           data-cancel-refund="<?php echo GUtils::formatMoney( $cancellationInfo['Refund_Amount']) ;?>" 
                                                           data-cancel-outcome="<?php echo $cancellationInfo['Cancellation_Outcome'];?>" 
                                                           onclick = 'sendCancelInvoiceHandler(this)' >Cancel & Refund</a></li>                                                           
                                                    <?php } ?>
                                                    <li><a target="_blank" href="sales-invoice-print.php?print=invoice&invid=<?php echo $row["Customer_Invoice_Num"]; ?>">Print</a></li>
                                                    <li><a href="javascript:void(0);" onclick = 'showConfirm( "sales-invoices.php?invaction=send&invid=<?php echo $row["Customer_Invoice_Num"]; ?>", "Send", "Are you sure you want to sent this email to <?php echo $name_email;?> ?", "Please Confirm", "modal-md")' >Send</a></li>
                                                    <li><a data-name="<?php echo $name; ?>" data-id="<?php echo $row["Customer_Invoice_Num"]; ?>" data-email="<?php echo $row['email'];?>" href="javascript:void(0);" onclick = 'sendReminderHandler(this)'  >Send Reminder</a></li>
                                                    <?php 
                                                    //Not applicable for cancelled button
                                                    if( ! ($row['Status'] == CustomerInvoice::$CANCELLED && $row['Refund_Status'] == 2 ) ) { ?>
                                                    <li><a href="javascript:void(0);" onclick = 'deleteConfirm( "sales-invoices.php?invaction=delete&invid=<?php echo $row["Customer_Invoice_Num"]; ?>", "Delete")' >Delete</a></li>
                                                    <li><a href="javascript:void(0);" onclick = 'deleteConfirm( "sales-invoices.php?invaction=void&invid=<?php echo $row["Customer_Invoice_Num"]; ?>", "Void")' >Void</a></li>
                                                    <?php } ?>
                                                </ul>
                                                <?php }   ?>
                                            </div>
                                        </td>
                                            <?php } ?>
                                    </tr>
                                <?php } ?>
                                </tbody>
                                 * 
                                 */
                                ?>
                            </table>
                        </div>
                    </div>
                    
<style type="text/css">
#idSalesInvoiceTable_filter { display:none;}
#idSalesInvoiceTable_wrapper .dt-buttons {
    position: absolute;
    right: 10px;
    top: 10px;
}
</style>

<script type="text/javascript">

    document.addEventListener('DOMContentLoaded', function () {

//Just to move the search box to be on the top of the card section
$('.outsideBorderSearchEx').on( 'keyup click', function () {
	$('#idSalesInvoiceTable').DataTable().search(
		$('.outsideBorderSearchEx').val()
	).draw();
});

    $('#idSalesInvoiceTable').DataTable({
		 "bProcessing": true,
         "serverSide": true,
		 //"ordering": false,
		 dom: 'Bfrtip',
		 buttons: ['csv', 'excel'],
         ajax : {
            url :"inc/datatable-sales-invoice.php?tourid=<?php echo isset($tourid) ? $tourid : '';?>&page=<?php echo basename($_SERVER['PHP_SELF']);?>&filter=<?php echo (isset($__GET['filter']) ? $__GET['filter'] : '');?>&showing=<?php echo (isset($__GET['showing']) ? $__GET['showing'] : '');?>", // json datasource
            complete : function(){
                delayedBindingCheckbox() ;

              },
          }
        }); 
    } ) ;
</script>