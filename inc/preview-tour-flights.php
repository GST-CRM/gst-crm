<?php

include "../session.php";
//include_once "inc/helpers.php";
include_once __DIR__ . '/../models/customer_payment.php';

require_once('../files/pdf/tcpdf.php');
include_once __DIR__ . '/../library/MYPDF2.php' ;

$printTitle = 'Arrival & Departure & Transfer Report' ;

$PageTitle = "Arrival & Departure & Transfer Report";

$tourid = $_REQUEST['id'];


$sql = "SELECT tourname FROM groups WHERE tourid=$tourid";
$queryGroup = mysqli_query($db, $sql) or die("error to fetch group name");
$groupRow = mysqli_fetch_row($queryGroup);
$groupName = $groupRow[0];

//Groupname And Leader name
//SELECT groups.tourname,contacts.fname,contacts.lname FROM groups JOIN contacts ON groups.groupleader=contacts.id AND groups.tourid=20050
$sql = "SELECT groups.tourid,groups.tourname,groups.land_productid,CONCAT(contacts.fname,' ', contacts.lname) AS leader_name,contacts.email,groups.airline_productid,SUM(products.price) AS trip_package
FROM groups 
LEFT JOIN products ON products.id = groups.land_productid OR products.id = groups.airline_productid 
LEFT JOIN contacts ON contacts.id = groups.groupleader
WHERE groups.tourid = $tourid";

$queryGroup = mysqli_query($db, $sql) or die("error to fetch group data");
$groupData = mysqli_fetch_assoc($queryGroup);


?>
<style>
td{
    font-size: 14px;
}
.dt-buttons {
	display:block !important;
}
</style>
        <!-- [ page content ] start -->
        <div class="row">
            <div class="col-sm-12">
                <!-- HTML5 Export Buttons table start -->
                        <h3 class="text-center"><?php echo $PageTitle; ?></h3>
                        <a href="excel.php?type=tour_leader&name=<?php echo $groupName;?>&groupid=<?php echo $tourid; ?>" class="d-none btn waves-effect waves-light btn-success" style="float:right;padding: 3px 13px;">Download</a>
                        <a href="javascript:void(0);" onclick = "return emailPDFconfirm();" class="d-none btn waves-effect waves-light btn-inverse mr-2" style="float:right;padding: 3px 13px;">Email pdf</a>
                        <h4><?php echo $groupName; ?></h4>
						<br />
                        <div class="dt-responsive table-responsive">
                            <table id="no-btn" class="datatable-report table gst-table table-hover table-striped table-bordered nowrap dt-responsive " data-page-length="100" style="width:100%;">
                                <thead>
                                <tr>
                                    <th rowspan="2" class="text-center align-middle">Full Name</th>
                                    <th colspan="5" class="text-center">Arrival</th>
                                    <th colspan="5" class="text-center">Departure</th>
                                    <th rowspan="2" class="text-center align-middle">Extension</th>
                                    <th rowspan="2" class="text-center align-middle">Special Requests</th>
                                    <th rowspan="2" class="text-center align-middle">Meals Request</th>
                                    <th rowspan="2" class="text-center align-middle">Allergies Request</th>
                                </tr>
                                <tr>
                                    <th>Date</th>
                                    <th>Flight</th>
                                    <th>Airport</th>
                                    <th>Time</th>
                                    <th>Transfer</th>
                                    <th>Date</th>
                                    <th>Flight</th>
                                    <th>Airport</th>
                                    <th>Time</th>
                                    <th>Transfer</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sql = "SELECT
												co.id,
												co.title,
												co.lname,
												co.fname,
												co.mname,
												(SELECT ga.Airline_Code FROM groups_flights gf JOIN groups_airline ga ON ga.Airline_ID=gf.Flight_Airline_ID WHERE gf.Group_ID=$tourid AND Specific_Customer_ID=0 AND gf.Supplier_Trigger=1) AS Arrival_Airline,
												(SELECT Flight_Airline_Number FROM groups_flights WHERE Group_ID=$tourid AND Specific_Customer_ID=0 AND Supplier_Trigger=1) AS Arrival_Number,
												(SELECT gap.Airport_Code FROM groups_flights gf JOIN groups_airport gap ON gap.Airport_ID=gf.Arrival_Airport WHERE gf.Group_ID=$tourid AND Specific_Customer_ID=0 AND gf.Supplier_Trigger=1) AS Arrival_Airport,
												(SELECT Arrival_Time FROM groups_flights WHERE Group_ID=$tourid AND Specific_Customer_ID=0 AND Supplier_Trigger=1) AS Arrival_Time,
												if(ca.Transfer=1,'Yes','No') AS Transfer,
												(SELECT ga.Airline_Code FROM groups_flights gf JOIN groups_airline ga ON ga.Airline_ID=gf.Flight_Airline_ID WHERE gf.Group_ID=$tourid AND Specific_Customer_ID=0 AND gf.Supplier_Trigger=2) AS Departure_Airline,
												(SELECT Flight_Airline_Number FROM groups_flights WHERE Group_ID=$tourid AND Specific_Customer_ID=0 AND Supplier_Trigger=2) AS Departure_Number,
												(SELECT gap.Airport_Code FROM groups_flights gf JOIN groups_airport gap ON gap.Airport_ID=gf.Departure_Airport WHERE gf.Group_ID=$tourid AND Specific_Customer_ID=0 AND gf.Supplier_Trigger=2) AS Departure_Airport,
												(SELECT Departure_Time FROM groups_flights WHERE Group_ID=$tourid AND Specific_Customer_ID=0 AND Supplier_Trigger=2) AS Departure_Time,
												ca.hisownticket AS Separate_Ticket,
												ca.Same_Itinerary,
												if(ca.Extension=1,(SELECT pro.name FROM products pro WHERE pro.id=ca.Extension_Product_ID),'No') AS Need_Transfer,
												if(ca.specialrequest=1,ca.special_land_text,'N/A') AS Special_Requests,
												if(ca.Meals_Allergies=1,ca.Meals_Request,'N/A') AS Meals_Request,
												if(ca.Meals_Allergies=1,ca.Allergies_Request,'N/A') AS Allergies_Request
											FROM customer_account ca
											JOIN contacts co 
											ON ca.contactid=co.id AND ca.status=1
											WHERE ca.tourid=$tourid "; /* "AND customer_account.status=1" removed to list canceled also */

                                    $queryContacts = mysqli_query($db, $sql) or die("error to fetch the flights data");
                                    $no = 1;

                                    while( $contactsData = mysqli_fetch_assoc($queryContacts) ) {?>
                                    <tr>
                                        <?php //echo "<td>".$no."</td>"; ?>
                                        <td><?php echo $contactsData['title']." ".$contactsData['fname']." ".$contactsData['mname']. " ".$contactsData['lname']; ?></td>
										
										<?php 
										$SpecificCustomerID = $contactsData["id"];
										$Separate_Ticket = $contactsData["Separate_Ticket"];
										$Same_Itinerary = $contactsData["Same_Itinerary"];
										
										if($Separate_Ticket==1 AND $Same_Itinerary==0) {
										$GetSpecificCustomerSQL = "SELECT
												(SELECT ga.Airline_Code FROM groups_flights gf JOIN groups_airline ga ON ga.Airline_ID=gf.Flight_Airline_ID WHERE gf.Group_ID=$tourid AND Specific_Customer_ID=$SpecificCustomerID AND gf.Supplier_Trigger=1) AS Arrival_Airline,
												(SELECT Flight_Airline_Number FROM groups_flights WHERE Group_ID=$tourid AND Specific_Customer_ID=$SpecificCustomerID AND Supplier_Trigger=1) AS Arrival_Number,
												(SELECT gap.Airport_Code FROM groups_flights gf JOIN groups_airport gap ON gap.Airport_ID=gf.Arrival_Airport WHERE gf.Group_ID=$tourid AND Specific_Customer_ID=$SpecificCustomerID AND gf.Supplier_Trigger=1) AS Arrival_Airport,
												(SELECT Arrival_Time FROM groups_flights WHERE Group_ID=$tourid AND Specific_Customer_ID=$SpecificCustomerID AND Supplier_Trigger=1) AS Arrival_Time,
												(SELECT ga.Airline_Code FROM groups_flights gf JOIN groups_airline ga ON ga.Airline_ID=gf.Flight_Airline_ID WHERE gf.Group_ID=$tourid AND Specific_Customer_ID=$SpecificCustomerID AND gf.Supplier_Trigger=2) AS Departure_Airline,
												(SELECT Flight_Airline_Number FROM groups_flights WHERE Group_ID=$tourid AND Specific_Customer_ID=$SpecificCustomerID AND Supplier_Trigger=2) AS Departure_Number,
												(SELECT gap.Airport_Code FROM groups_flights gf JOIN groups_airport gap ON gap.Airport_ID=gf.Departure_Airport WHERE gf.Group_ID=$tourid AND Specific_Customer_ID=$SpecificCustomerID AND gf.Supplier_Trigger=2) AS Departure_Airport,
												(SELECT Departure_Time FROM groups_flights WHERE Group_ID=$tourid AND Specific_Customer_ID=$SpecificCustomerID AND Supplier_Trigger=2) AS Departure_Time
											FROM customer_account ca
											WHERE ca.tourid=$tourid AND ca.contactid=$SpecificCustomerID ";
										$SpecificCustomerFlight = mysqli_query($db, $GetSpecificCustomerSQL) or die("error to fetch the flights data");
										$SpecificCustomerFlightData = mysqli_fetch_assoc($SpecificCustomerFlight);
										//echo $GetSpecificCustomerSQL."<br />";
										//Collect Arrival Data Here
										$Arrival_Timestamp = $SpecificCustomerFlightData["Arrival_Time"];
										$Arrival_Airline = $SpecificCustomerFlightData["Arrival_Airline"];
										$Arrival_Airport = $SpecificCustomerFlightData["Arrival_Airport"];
										$Arrival_Number = $SpecificCustomerFlightData["Arrival_Number"];
										
										//Collect Departure Data Here
										$Departure_Timestamp = $SpecificCustomerFlightData["Departure_Time"];
										$Departure_Airline = $SpecificCustomerFlightData["Departure_Airline"];
										$Departure_Airport = $SpecificCustomerFlightData["Departure_Airport"];
										$Departure_Number = $SpecificCustomerFlightData["Departure_Number"];
										
										} else {
										
										//Collect Arrival Data Here
										$Arrival_Timestamp = $contactsData["Arrival_Time"];
										$Arrival_Airline = $contactsData["Arrival_Airline"];
										$Arrival_Airport = $contactsData["Arrival_Airport"];
										$Arrival_Number = $contactsData["Arrival_Number"];
										
										//Collect Departure Data Here
										$Departure_Timestamp = $contactsData["Departure_Time"];
										$Departure_Airline = $contactsData["Departure_Airline"];
										$Departure_Airport = $contactsData["Departure_Airport"];
										$Departure_Number = $contactsData["Departure_Number"];
										
										}
										?>

										<?php //To show the date of the arrival date
										if($Arrival_Timestamp==NULL) { echo"<td>-</td>";} else { echo "<td>".date('m/d/Y', strtotime($Arrival_Timestamp))."</td>"; } ?>
										<?php //To show the arrival flight and number data
										if($Arrival_Airline==NULL OR $Arrival_Number==NULL) { echo "<td>-</td>";} else { echo "<td>".$Arrival_Airline." ".$Arrival_Number."</td>"; } ?>
										<?php //To show the arrival Airport
										if($Arrival_Timestamp==NULL) { echo "<td>-</td>"; } else { echo "<td>".$Arrival_Airport."</td>"; } ?>
										<?php //To show the arrival time
										if($Arrival_Timestamp==NULL) { echo "<td>-</td>"; } else { echo "<td>".date('h:ia', strtotime($Arrival_Timestamp))."</td>"; } ?>
										
                                        <td><?php echo $contactsData['Transfer']; ?></td>
										
										<?php //To show the departure date
										if($Departure_Timestamp==NULL) { echo"<td>-</td>";} else { echo "<td>".date('m/d/Y', strtotime($Departure_Timestamp))."</td>"; } ?>
										<?php //To show the departure flight and number data
										if($Departure_Airline==NULL OR $Departure_Number==NULL) { echo "<td>-</td>";} else { echo "<td>".$Departure_Airline." ".$Departure_Number."</td>"; } ?>
										<?php //To show the departure airport
										if($Departure_Timestamp==NULL) { echo "<td>-</td>"; } else { echo "<td>".$Departure_Airport."</td>"; } ?>
										<?php //To show the departure time
										if($Departure_Timestamp==NULL) { echo "<td>-</td>"; } else { echo "<td>".date('h:ia', strtotime($Departure_Timestamp))."</td>"; } ?>
										
                                        <td><?php echo $contactsData['Transfer']; ?></td>
                                        <td><?php echo $contactsData['Need_Transfer']; ?></td>
                                        <td><?php echo $contactsData['Special_Requests']; ?></td>
                                        <td><?php echo $contactsData['Meals_Request']; ?></td>
                                        <td><?php echo $contactsData['Allergies_Request']; ?></td>
										
                                    </tr>
                                    <?php
                                    $no++;
									} ?>

                                </tbody>
                                
                            </table>
                        </div>
                    <?php //} ?>
                <!-- HTML5 Export Buttons end -->
            </div>
        </div>

    <!-- [ page content ] end -->

<script>
function emailPDFconfirm(){
    showConfirm( "groups-edit.php?action=edit&tab=reports&report_slug=To-Date&email_report=yes&name=<?php echo $groupName;?>&id=<?php echo $tourid; ?>", "Send", "Are you sure you want to send this report to tour leader?", "Please Confirm", "modal-md");
    //showConfirm( "tour-leader-email-report.php?report_slug=To-Date&email_report=yes&name=<?php echo $groupName;?>&id=<?php echo $tourid; ?>", "Send", "Are you sure you want to sent this report to tour leader?", "Please Confirm", "modal-md");
}
//showConfirm( "javascript:customerPaymentsListForm.submit();", "Yes", msg, "Please Confirm", "modal-md") ;
        $('#no-btn').DataTable({
            dom: 'Bfrtip',
			"order": [[ 2, "asc" ],[ 4, "asc" ]],
            buttons:['excel',
					{extend: 'pdf', title: '<?php echo $groupName; ?> - Arrival & Departure & Transfer Report', orientation: 'landscape' },
					{extend: 'print', title: '<h3><?php echo $groupName; ?></h3><h5>Arrival & Departure & Transfer Report</h5>',
						customize: function(win){
							var last = null;
							var current = null;
							var bod = [];
							var css = '@page { size: landscape; }',
								head = win.document.head || win.document.getElementsByTagName('head')[0],
								style = win.document.createElement('style');
								
							style.type = 'text/css';
							style.media = 'print';
							
							if (style.styleSheet) { style.styleSheet.cssText = css; } else { style.appendChild(win.document.createTextNode(css)); }
							head.appendChild(style);
						}
					},
					/*{text: 'Email', action: function ( e, dt, node, config ) { showConfirm( "groups-edit.php?action=edit&tab=reports&report_slug=Flight-Report&email_report=yes&name=<?php echo $groupName;?>&id=<?php echo $tourid; ?>", "Send", "<h4>NOT YET WORKING!</h4><br />Are you sure you want to send this report to Land Supplier?", "Please Confirm", "modal-md"); } }*/
					]
        });
</script>
<?php include 'notificiations.php'; ?>
