<?php
	//include connection file 
	include "../config.php";
	 
	// initilize all variable
	$params = $columns = $totalRecords = $data = array();

	$params = $_REQUEST;

	//define index of column
	$columns = array( 
		0 => 'id',
		1 => 'title', 
		2 => 'fname', 
		3 => 'mname',
		4 => 'lname',
		5 => 'phone',
		6 => 'mobile',
		7 => 'email',
		8 => 'company',
		9 => 'address1',
		10 => 'address2',
		11 => 'zipcode',
		12 => 'city',
		13 => 'state',
		14 => 'country'
	);

	$where = $sqlTot = $sqlRec = "";

	// check search value exist
	if( !empty($params['search']['value']) ) {   
                          $where .= ' WHERE '; 
						  $theSearchText = $params['search']['value'];
						  $theSearchText2 = str_replace(" ","%",$theSearchText);
						  $where .= "concat(fname, mname, lname,mobile) LIKE '%".$theSearchText2."%'";

                          /*$query = explode(" ", $theSearchText);  
                          foreach($query as $text)  
                          {  
                               $where .= "fname LIKE '%".mysqli_real_escape_string($db, $text)."%' OR ";  
                               $where .= "mname LIKE '%".mysqli_real_escape_string($db, $text)."%' OR ";  
                               $where .= "lname LIKE '%".mysqli_real_escape_string($db, $text)."%' OR ";  
                          }  
                          $where = substr($where, 0, -4);  */
		/*$where .=" WHERE ";
		$where .=" ( fname LIKE '%".$params['search']['value']."%' ";    
		$where .=" OR mname LIKE '%".$params['search']['value']."%' ";
		$where .=" OR lname LIKE '%".$params['search']['value']."%' )";*/
	}

	// getting total number records without any search
	$sql = "SELECT id,title,fname,mname,lname,phone,mobile,email,company,address1,address2,zipcode,city,state,country FROM contacts ";
	//$sql = "SELECT id,title,fname FROM contacts ";
	$sqlTot .= $sql;
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}


 	$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."  LIMIT ".$params['start']." ,".$params['length']." ";

	$queryTot = mysqli_query($db, $sqlTot) or die("database error:". mysqli_error($db));


	$totalRecords = mysqli_num_rows($queryTot);

	$queryRecords = mysqli_query($db, $sqlRec) or die("error to fetch employees data");

	//iterate on results row and create new index array of data
	while( $row = mysqli_fetch_row($queryRecords) ) { 
		$data[] = $row;
	}	

	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>
