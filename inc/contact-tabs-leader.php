<?php

include_once __DIR__ . '/../models/tour_membership.php';
?>
<form name="contact-leader" action="inc/contact-functions.php" method="POST" id="contact-leader">
	<div class="row">
		<div class="form-group col-sm-12 m-0">
			<h4 class="sub-title">Group Leader Information</h4>
		</div>
  		<div class="form-default form-static-label col-sm-3">
			<label class="float-label">Status</label>
			<div class="can-toggle">
			  <input type="checkbox" id="dd" name="leaderstatus" value="1" <?php if($leaderdata["status"]==1) echo "checked"; ?>>
			  <label for="dd">
				<div class="can-toggle__switch" data-checked="Active" data-unchecked="Disabled"></div>
			  </label>
			</div>
		</div>
		<div class="form-group row col-sm-12" style="display:none;">
			<label class="col-sm-4 col-form-label">Leader #</label>
			<div class="col-sm-8"><input type="text" name="leadercode" class="form-control" value="<?php echo $leaderdata["id"]; ?>" readonly>
			<input type="text" name="contactcode" class="form-control" value="<?php echo $leaderdata["contactid"]; ?>" hidden>
			<input type="text" name="fullname" class="form-control" value="<?php echo $data['fname']." ".$data['mname']." ".$data['lname']; ?>" hidden></div>
		</div>
		<div class="form-group col-sm-6">
			<label class="float-label">Description</label>
			<input type="text" name="leaderdesc" value="<?php echo $leaderdata["description"]; ?>" class="form-control">
		</div>

		<div class="form-group col-sm-3">
			<label class="float-label">Enable Login</label>
			<div class="can-toggle">
				<input type="checkbox" class="leader-active-disable-switch-checkbox" id="leader-enable" name="leaderlogin" value="1" <?php if($leaderdata["active"]==1) echo"checked"; ?> >
				<label for="leader-enable">
					<div class="can-toggle__switch" data-checked="Active" data-unchecked="Disabled"></div>
				</label>
			</div>
		</div>
        
		<div class="form-group row col-sm-12">
			<div class="col-sm-12">
				<button type="submit" id="idLeaderUpdateButton" name="updateleader" value="submit" class="btn waves-effect waves-light btn-success"style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Update</button>
			</div>
		</div>
	</div>
</form>

<div class="form-group row col-sm-12">
	<div class="col-sm-12">
		<br />
		<h6>Groups assigned to this Group Leader</h6>
		<br />
		<table class="table table-hover table-striped table-bordered">
			<thead>
				<tr role="row">
					<th>Group Name</th>
					<th>Destination</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th># of Pax</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$sql = "select * from groups WHERE `groupleader` LIKE '%$leadid%'"; $result = $db->query($sql);
			if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
				<tr>
					<td><a href="groups-edit.php?id=<?php echo $row["tourid"]; ?>&action=edit" style="color: #0000EE;"><?php echo $row["tourname"]; ?></a></td>
					<td><?php echo $row["destination"]; ?></td>
					<td><?php echo $row["startdate"]; ?></td>
					<td><?php echo $row["enddate"]; ?></td>
					<td>
						<a href="groups-edit.php?id=<?php echo $row["tourid"]; ?>&action=edit&tab=participants" style="color: #0000EE;font-size:14px;">
						<?php
						$touridz = $row["tourid"];
						echo intval( (new TourMembership())->paxTotal($touridz) ) ;?>
						</a><span> out of
						<?php
						$sqltourpaxMax = "SELECT seats FROM products WHERE id=$AirlineProductID";
						$resulttourpaxMax = $db->query($sqltourpaxMax);
						$SeatsMaxCount = $resulttourpaxMax->num_rows;
						if($SeatsMaxCount > 0) {
							$SeatsMaxData = $resulttourpaxMax->fetch_assoc();
							echo $SeatsMaxData['seats'];
						} else {
							echo "0";
						}
						 ?>
						</span>
					</td>
				</tr>
			<?php }} else { ?>
				<tr>
					<td colspan="5" style="text-align:center;">There are no groups assigned for this group leader yet.</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>