<?php

$GetCheckDataSQL = "SELECT
	ch.Check_Number, 
	ch.Check_Type, 
	ch.Check_Customer_ID, 
    ch.Check_Bank_ID,
    chb.Bank_Name,
    chb.Bank_Address,
    chb.Bank_Transit,
    chb.Bank_Routing,
    chb.Bank_Account_Number,
    ch.Check_Amount, 
    ch.Check_Amount_Words, 
    ch.Check_Date, 
    ch.Check_Note, 
    ch.Check_Memo,
    ch.Check_Status,
    cc.Company_Name,
    cc.Company_Address,
    cc.Company_Address2,
    cc.Company_Phone,
    cc.Company_Email,
    cc.Company_Logo,
	CASE
    	WHEN ch.Check_Type = 1 THEN (SELECT CONCAT(co.fname,' ',co.mname,' ',co.lname) FROM contacts co WHERE co.id=ch.Check_Customer_ID)
        WHEN ch.Check_Type = 2 THEN (SELECT ep.Payee_Name FROM expenses_payee ep WHERE ep.Payee_ID=ch.Check_Customer_ID)
    END AS Customer_Name,
    CASE
    	WHEN ch.Check_Type = 1 THEN (SELECT CONCAT(co.address1,', ', co.address2,'<br />',co.city,', ',co.state,', ',co.zipcode) FROM contacts co WHERE co.id=ch.Check_Customer_ID)
        WHEN ch.Check_Type = 2 THEN ''
    END AS Customer_Address
FROM checks ch
JOIN checks_bank chb 
ON chb.Bank_ID=ch.Check_Bank_ID
JOIN checks_company cc
ON cc.Company_ID=1
WHERE ch.Check_ID=$CheckID";
$CheckData = GDb::fetchRow($GetCheckDataSQL);

$UpdatePrintedCounter = "UPDATE checks SET Check_Printed=Check_Printed+1 WHERE Check_ID=$CheckID";
GDb::execute($UpdatePrintedCounter);
?>
<style>
@font-face {
  font-family: MICRfont;
  src: url(../files/assets/fonts/micrenc.ttf);
}
</style>
<table cellspacing="10" style="width:100%;height:336px;display:block;">
    <tr>
        <td width="15%">
		<?php if(isset($CorrectFolder) && $CorrectFolder == 1) { $LogoRealPath = "uploads/".$CheckData['Company_Logo']; } else { $LogoRealPath = "../uploads/".$CheckData['Company_Logo']; } 
        if($CheckData['Company_Logo'] != '') {$LogoPath = $LogoRealPath; } else {$LogoPath = $crm_images."/logo-Placeholder.png";} ?>
            <img src="<?php echo $LogoPath; ?>" class="img-fluid" style="height:60px;" alt="Bank Check Company Photo" />
        </td>
        <td width="35%" align="center">
            <span style="font-size:14px;"><b><?php echo $CheckData['Company_Name']; ?></b></span><br />
            <span style="font-size:10px;"><?php echo $CheckData['Company_Address']; ?><br />
            <?php echo $CheckData['Company_Address2']; ?><br />
            <?php echo $CheckData['Company_Phone']; ?><br />
            <?php echo $CheckData['Company_Email']; ?></span> 
        </td>
        <td width="35%" align="center">
            <span style="font-size:14px;"><b><?php echo $CheckData['Bank_Name']; ?></b></span><br />
            <span style="font-size:10px;"><?php echo $CheckData['Bank_Address']; ?></span>
        </td>
        <td width="15%" align="center">
            <span style="font-size:16px;"><b>No.</b> <?php echo $CheckData['Check_Number']; ?></span><br/>
            <span><?php if(!empty($CheckData['Bank_Transit'])) { echo $CheckData['Bank_Transit']; } ?></span><br /><br />
            <b>Date</b> <span><?php echo GUtils::clientDate($CheckData['Check_Date']); ?></span><br />
        </td>
    </tr>
    <tr>
        <td colspan="3" style="border-bottom:solid 10px #333;border-bottom-width: 1.2px;"><b>Pay To The Order Of </b> <span><?php echo "   ".$CheckData['Customer_Name']; ?></span>
        </td>
        <td><span style="font-size:14px;"><b></b><?php echo "$ **".GUtils::formatMoney($CheckData['Check_Amount'],''); ?></span></td>
    </tr>
    <tr>
        <td colspan="3" style="border-bottom:solid 2px #333;border-bottom-width: 1.2px;"><span><?php echo $CheckData['Check_Amount_Words']; ?></span></td>
        <td colspan="3"><span><b>Dollars</b></span></td>
    </tr>
    <tr>
        <td colspan="3" height="60"><?php echo $CheckData['Customer_Name']."<br />".$CheckData['Customer_Address']; ?></td>
        <td></td>
    </tr>
    <tr>
        <td colspan="4"><table><tr><td width="50%" style="border-bottom:solid 2px #333;border-bottom-width: 1.2px;"><b>Memo:</b> <?php echo substr($CheckData['Check_Memo'],0,50); ?></td><td width="10%"></td><td width="40%" style="border-bottom:solid 2px #333;border-bottom-width: 1.2px;"></td></tr></table></td>
    </tr>
    <tr>
        <td colspan="4" align="center">
        <h4 style="margin:0px;font-family:MICREncoding;font-size:26px;line-height:14px;">C0000<span id="CheckNum2Text"><?php echo $CheckData['Check_Number']; ?></span>C A<span id="RoutingNumText"><?php echo $CheckData['Bank_Routing']; ?></span>A <span id="AccountNumText"><?php echo $CheckData['Bank_Account_Number']; ?></span>C</h4>
        </td>
    </tr>
</table>
<br />
<br />
<br />
<br />
<table cellspacing="10" style="width:100%;height:336px;display:block;">
    <tr>
        <td><b>No. <?php echo $CheckData['Check_Number']; ?></b>
            <br />
            <b>Pay To: <?php echo $CheckData['Customer_Name']; ?></b>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </td>
        <td><b>Amount: <?php echo GUtils::formatMoney($CheckData['Check_Amount']); ?></b></td>
        <td><b>Date: <?php echo GUtils::clientDate($CheckData['Check_Date']); ?></b></td>
    </tr>
    <tr>
        <td colspan="3"><b><?php echo $CheckData['Company_Name']; ?></b><br /><br /><b>Memo: <?php echo $CheckData['Check_Memo']; ?></b></td>
    </tr>
</table>
<br />
<br />
<br />
<br />
<table cellspacing="10" style="width:100%;height:336px;display:block;">
    <tr>
        <td><b>No. <?php echo $CheckData['Check_Number']; ?></b>
            <br />
            <b>Pay To: <?php echo $CheckData['Customer_Name']; ?></b>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
        </td>
        <td><b>Amount: <?php echo GUtils::formatMoney($CheckData['Check_Amount']); ?></b></td>
        <td><b>Date: <?php echo GUtils::clientDate($CheckData['Check_Date']); ?></b></td>
    </tr>
    <tr>
        <td colspan="3"><b><?php echo $CheckData['Company_Name']; ?></b><br /><br /><b>Memo: <?php echo $CheckData['Check_Memo']; ?></b></td>
    </tr>
</table>