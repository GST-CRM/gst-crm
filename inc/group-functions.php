<?php
include "../session.php";
include_once __DIR__ . '/../models/products.php' ;
include_once __DIR__ . '/../models/acl_permission.php' ;

if( ! AclPermission::actionAllowed('EditGroupNote', 'group-functions.php') && ! isset($_POST['groupnote']) ) {
      header("location:login.php");
	  exit;
}
//Update the Basic Information of the Group / Tour
if(isset($_POST['tourid'])){
	$tourid = mysqli_real_escape_string($db, $_POST["tourid"]);
	$TourName = mysqli_real_escape_string($db, $_POST["tourname"]);
	if ($_POST["status"]==1){$status = "1";} else {$status = "0";}
	$TourDesc = mysqli_real_escape_string($db, $_POST["tourdesc"]);
	$ChurchName = mysqli_real_escape_string($db, $_POST["ChurchName"]);
	$DepartureCity = mysqli_real_escape_string($db, $_POST["DepartureCity"]);
	$destination = mysqli_real_escape_string($db, $_POST["destination"]);
	$Group_URL = mysqli_real_escape_string($db, $_POST["Group_URL"]);
	$StartDate = mysqli_real_escape_string($db, $_POST["startdate"]);
	$EndDate = mysqli_real_escape_string($db, $_POST["enddate"]);
	$DepositAmount = mysqli_real_escape_string($db, $_POST["DepositAmount"]);
	$SecondPayment = mysqli_real_escape_string($db, $_POST["SecondPayment"]);
	$itinerary = mysqli_real_escape_string($db, $_POST["itinerary"]);
    
    /* hard coded value recommended so not receiving data from input 
	$DueDateBefore = mysqli_real_escape_string($db, $_POST["DueBefore"]);
	$SecondDueDateBefore = mysqli_real_escape_string($db, $_POST["SecondDueBefore"]);
	$FinalDueDateBefore = mysqli_real_escape_string($db, $_POST["FinalDueBefore"]);
     */

	$DueDateBefore = 90; //mysqli_real_escape_string($db, $_POST["DueBefore"]);
	$SecondDueDateBefore = 180; //mysqli_real_escape_string($db, $_POST["SecondDueBefore"]);
	$FinalDueDateBefore = 90;// mysqli_real_escape_string($db, $_POST["FinalDueBefore"]);

	$con11 = new mysqli($servername, $username, $password, $dbname);
	if ($con11->connect_error) {die("Connection failed: " . $con11->connect_error);} 
	$sql11 = "UPDATE groups SET tourname='$TourName', tourdesc='$TourDesc',Church_Name='$ChurchName',
        Departure_City='$DepartureCity', status='$status',destination='$destination',Group_URL='$Group_URL', 
            startdate='$StartDate', enddate='$EndDate', Group_Deposit_Amount='$DepositAmount', 2nd_Payment_Amount='$SecondPayment', 
                due_before='$DueDateBefore', second_due_before='$SecondDueDateBefore', final_due_before='$FinalDueDateBefore', 
                itinerary='$itinerary', 
                updatetime=NOW() WHERE tourid=$tourid;";
    //Find New Due Before
    $tsDueDate = strtotime($StartDate) - ($DueDateBefore * 86400) ;
    $newDueDate = Date('Y-m-d', $tsDueDate) ;
    //Update due date in customer invoice table, Status will update even the closed ones, since w/ earlier discusssion product price had to update similarly.
    $sql11 .= "UPDATE customer_invoice SET Due_Date='$newDueDate' WHERE Group_ID='$tourid';" ;
	$sql11 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','tour','$tourid','edited','$TourName');";
    //Nithin
    //Update agent info the group {
    if( isset($__REQUEST['selAgents']) && count($__REQUEST['selAgents']) > 0 ) {

        foreach( $__REQUEST['selAgents'] as $k => $v ) {
            if( ! $v ) {
                continue;
            }

            $commissionType = $__REQUEST['commissiontype'][$k] ;
            $commissionValue = $__REQUEST['commissionvalue'][$k] ;
            $agentId = $v ;
            $sql11 .= "UPDATE group_agents SET agent_id='$agentId', commissiontype='$commissionType', commissionvalue='$commissionValue' WHERE id='$k'; " ;
        }
    }
    //}
if ($con11->multi_query($sql11) === TRUE) {
	echo "<h3>Success</h3> The basic information for the tour ($TourName) has been updated successfully!";
	echo "<meta http-equiv='refresh' content='2;groups-edit.php?id=$tourid&action=edit'>";
} else {
	echo $sql11 . "<br>" . $con11->error;
}
$con11->close();
}

//Update the Required Information of the Group / Tour
if(isset($_POST['requiredinfo'])){
	$tourid = mysqli_real_escape_string($db, $_POST["requiredinfo"]);
	$TourName = mysqli_real_escape_string($db, $_POST["requiredinfoname"]);
	$ListPriceNew = mysqli_real_escape_string($db, $_POST["listprice"]);
	$TripCost = mysqli_real_escape_string($db, $_POST["tripcost"]);
	$Staff_ID = mysqli_real_escape_string($db, $_POST["StaffRepID"]);
	//$theagent = $_POST['theagent'];
	$theagent = "";
    $agentsIncoming = [] ;
		foreach ($_POST['theagent'] as $select2) {
			$theagent .= $select2 . ",";
            $agentsIncoming[] = $select2 ;
		}
	$theagent = substr($theagent,0,-1);  //to remove the last comma
    
    $sqld = "DELETE FROM group_agents WHERE agent_id NOT IN($theagent) AND group_id='$tourid'; " ;
    GDb::execute($sqld);
    $sqls = "SELECT agent_id FROM group_agents WHERE group_id='$tourid'; " ;
    $agentsLeftSet = GDb::fetchRowSet($sqls) ;
    $agentsLeft = [] ;
    foreach( $agentsLeftSet as $a ) {
        $agentsLeft[] = $a['agent_id'];
    }
    $agentsNew = array_diff($agentsIncoming , $agentsLeft);
    $sql11 = '' ;
    foreach( $agentsNew as $newAgentId ) {
        $sql11 .= "INSERT INTO group_agents (group_id, agent_id,commissiontype,commissionvalue) VALUES('$tourid', '$newAgentId', '', '') ;" ;
    }
    
	//$thegroupleader = $_POST['thegroupleader'];
	$thegroupleader = "";
		foreach ($_POST['thegroupleader'] as $select3) {
			$thegroupleader .= $select3 . ",";
		}
	$thegroupleader = substr($thegroupleader,0,-1);  //to remove the last comma

	$con11 = new mysqli($servername, $username, $password, $dbname);
	if ($con11->connect_error) {die("Connection failed: " . $con11->connect_error);} 
	$sql11 .= "UPDATE groups SET listprice='$ListPriceNew', tripcost='$TripCost',agent='$theagent', groupleader='$thegroupleader', Staff_ID='$Staff_ID', updatetime=NOW() WHERE tourid=$tourid;";
	$sql11 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','tour','$tourid','edited','$TourName');";
if ($con11->multi_query($sql11) === TRUE) {
	echo "<h3>Success</h3> The required information for the tour ($TourName) has been updated successfully! The page will be refreshed in seconds.";
	echo "<meta http-equiv='refresh' content='2;groups-edit.php?id=$tourid&action=edit'>";
} else {
	echo $sql11 . "<br>" . $con11->error;
}
$con11->close();
}

//Update the Airline Product Selection in Groups Edit Page
if(isset($_POST['airtourid'])){
	$tourid = mysqli_real_escape_string($db, $_POST["airtourid"]);
	$productid = mysqli_real_escape_string($db, $_POST["airlineproduct"]);
	$airproductlineSupplier = mysqli_real_escape_string($db, $_POST["airproductlineSupplier"]);
	$totalcost = mysqli_real_escape_string($db, $_POST["tourtotalcost"]);
	$AirProductLineCurrentID = mysqli_real_escape_string($db, $_POST["airproductlineid"]);
		
		//To collect the information of the Airline Product
			$ProductInfoSQL = "SELECT id,name,supplierid,cost,price FROM products WHERE id=$productid";
			$ProductInfoResult = $db->query($ProductInfoSQL);
			$ProductInfo = $ProductInfoResult->fetch_assoc();
				$ProductInfoID = $ProductInfo['id'];
				$ProductInfoName = $ProductInfo['name'];
				$ProductInfoSupplier = $ProductInfo['supplierid'];
				$ProductInfoCost = $ProductInfo['cost'];
				$ProductInfoPrice = $ProductInfo['price'];

		//The main query line
		$sql11 = "UPDATE groups SET airline_productid='$productid',tripcost='$totalcost', updatetime=NOW() WHERE tourid=$tourid;";

		/*============= GROUP PURCHASE ORDER & GROUP PURCHASE ORDER LINE =============*/
		//Check if this group has a Group Purchase Order and get Group_PO_ID
		$GPOnumSQL = "SELECT Group_PO_ID FROM group_purchase_order WHERE Group_Group_ID=$tourid";
		$result1 = $db->query($GPOnumSQL);
		$GroupPurchaseOrderData = $result1->fetch_assoc();
		$GroupPurchaseOrderNumID = $GroupPurchaseOrderData["Group_PO_ID"];
	
		//Check if this group has a Group Purchase Order Line for the Airline
		$GPOnumCheckSQL = "SELECT Group_PO_ID FROM group_po_line
						WHERE Group_PO_ID=$GroupPurchaseOrderNumID
						AND Line_Type='Airline'";
		$resultCheck1 = $db->query($GPOnumCheckSQL);
		$checkIFairPOnumExist = $resultCheck1->num_rows;
		if ( $checkIFairPOnumExist == 0) {
			$sql11 .= "INSERT INTO group_po_line (Group_PO_ID,Product_Product_ID,Line_Type,Cost_Amount,Supplier_Supplier_ID)
			VALUES ('$GroupPurchaseOrderNumID','$productid','Airline','$ProductInfoCost','$ProductInfoSupplier');";
		} else {
			$sql11 .= "UPDATE group_po_line SET Product_Product_ID=$productid,Cost_Amount='$ProductInfoCost',Supplier_Supplier_ID='$ProductInfoSupplier', Mod_Date=NOW() WHERE Group_PO_ID=$GroupPurchaseOrderNumID AND Line_Type='Airline';";
		}

		$sql11 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','tour','$tourid','updated','$TourName');";
		
		if ($db->multi_query($sql11) === TRUE) {
			//echo "<h3>Success</h3>";
		} else {
			echo $sql11 . "<br>" . $db->error;
		}
		


		/*============= PURCHASE ORDER CHECK =============*/
		//Check if this group has a Purchase Order for AIRLINE
		$POcheckConn = new mysqli($servername, $username, $password, $dbname);
		if ($POcheckConn->connect_error) {die("Connection failed: " . $POcheckConn->connect_error);} 

		$POnumSQL = "SELECT Purchase_Order_Num FROM purchase_order WHERE Group_ID=$tourid AND Purchase_Order_Category='Airline'";
		$POnumResult = $POcheckConn->query($POnumSQL);
		$PurchaseOrderCount = $POnumResult->num_rows;
		if($PurchaseOrderCount > 0) {
			$PurchaseOrderData = $POnumResult->fetch_assoc();
			$PurchaseOrderNum_ID = $PurchaseOrderData["Purchase_Order_Num"];
			$CheckPOsql = "UPDATE purchase_order SET Group_ID='$tourid',Supplier_ID='$ProductInfoSupplier', Mod_Date=NOW() WHERE Purchase_Order_Num=$PurchaseOrderNum_ID AND Purchase_Order_Category='Airline';";
		} else {
			$CheckPOsql = "INSERT INTO purchase_order (Group_ID,Supplier_ID,Purchase_Order_Category,Purchase_Order_Date) VALUES ('$tourid','$ProductInfoSupplier','Airline',NOW());";
		}
		if ($POcheckConn->multi_query($CheckPOsql) === TRUE) {
			$PurchaseOrderNumNewInsert = $POcheckConn->insert_id;
			if($PurchaseOrderNumNewInsert > 0) { $PurchaseOrderNum_ID = $PurchaseOrderNumNewInsert;}
			//echo $PurchaseOrderNum_ID;
			//echo "<h3>Success</h3>";
		} else {
			echo $CheckPOsql . "<br>" . $POcheckConn->error;
		}
		$POcheckConn->close();

		/*============= PURCHASE ORDER LINE CHECK =============*/
		$POconn = new mysqli($servername, $username, $password, $dbname);
		if ($POconn->connect_error) {die("Connection failed: " . $POconn->connect_error);} 
		//Check if this group has a Purchase Order Line for the Airline Main Package Product
		$POnumCheckSQL2 = "SELECT Purchase_Order_Line FROM purchase_order_line
						WHERE Purchase_Order_Num=$PurchaseOrderNum_ID
						AND Line_Type='Airline Package'";
		$resultCheck2 = $POconn->query($POnumCheckSQL2);
		$checkAirPOnumExist = $resultCheck2->num_rows;
		if ( $checkAirPOnumExist == 0) {
			$sql12 = "INSERT INTO purchase_order_line (Purchase_Order_Num,Line_Type,Product_Product_ID,Product_Name,Cost_Amount,Supplier_Supplier_ID)
			VALUES ('$PurchaseOrderNum_ID','Airline Package','$productid','$ProductInfoName','$ProductInfoCost','$ProductInfoSupplier');";
		} else {
			$sql12 = "UPDATE purchase_order_line SET Product_Product_ID=$productid,Product_Name='$ProductInfoName',Cost_Amount='$ProductInfoCost',Supplier_Supplier_ID='$ProductInfoSupplier', Mod_Date=NOW() WHERE Purchase_Order_Num=$PurchaseOrderNum_ID AND Line_Type='Airline Package';";
		}
		
		if ($POconn->multi_query($sql12) === TRUE) {
			//echo "<h3>Success</h3>";
		} else {
			echo $sql12 . "<br>" . $POconn->error;
		}
		$POconn->close();


		/*============= SUPPLIER BILL CHECK & INSER or UPDATE THE SUPPLIER LINE =============*/
		//Check if the Airline Product / Supplier has an invoice
		$SBnumDB = new mysqli($servername, $username, $password, $dbname);
		if ($SBnumDB->connect_error) {die("Connection failed: " . $SBnumDB->connect_error);} 
			$SBnumSql = "SELECT Supplier_Bill_Num FROM suppliers_bill WHERE Purchase_Order_Num=$PurchaseOrderNum_ID";
			$SBnumResult = $SBnumDB->query($SBnumSql);
			$SBnumCount = $SBnumResult->num_rows;
			if($SBnumCount > 0) {
				$SupplierBillData = $SBnumResult->fetch_assoc();
				$SupplierBillNumID = $SupplierBillData["Supplier_Bill_Num"];
				$SBnumQuery = "UPDATE suppliers_bill SET Supplier_ID=$ProductInfoSupplier,Mod_Date=NOW() WHERE Supplier_Bill_Num=$SupplierBillNumID;";
				$SBnumDB->query($SBnumQuery);
			} else {
				$SBnumQuery = "INSERT INTO suppliers_bill (Supplier_Bill_Date,Purchase_Order_Num,Supplier_ID) VALUES (NOW(),'$PurchaseOrderNum_ID','$ProductInfoSupplier');";
				$SBnumDB->query($SBnumQuery);
				$SupplierBillNumID = $SBnumDB->insert_id;
			}
			//Create the Suppleir Bil Line for the Airline
			$SBLineQuery = 	"INSERT INTO  suppliers_bill_line (Supplier_Bill_Num,Supplier_Bill_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Bill_Line_Total)
							VALUES ($SupplierBillNumID,1,'Airline',$productid,'$ProductInfoName',(SELECT COUNT(contactid) FROM customer_account WHERE tourid=$tourid AND status=1 AND hisownticket=0),$ProductInfoCost)
							ON DUPLICATE KEY
							UPDATE 
								Line_Type = 'Airline',
								Product_Product_ID = $productid,
								Product_Name = '$ProductInfoName',
								Qty = (SELECT COUNT(contactid) FROM customer_account WHERE tourid=$tourid AND status=1 AND hisownticket=0),
								Bill_Line_Total = $ProductInfoCost,
								Mod_Date = NOW();";
			$SBnumDB->query($SBLineQuery);
		
		$SBnumDB->close();
		
		
		//To Add the airline product as a line in the Sales Order Line for Each Customer in the group
		$SalesOrderLineDB = new mysqli($servername, $username, $password, $dbname);
		if ($SalesOrderLineDB->connect_error) {die("Connection failed: " . $SalesOrderLineDB->connect_error);} 
		$SelectCustomersIDSQL = "SELECT so.Sales_Order_Num,ca.contactid
		FROM customer_account ca
		JOIN sales_order so 
		ON so.Customer_Account_Customer_ID=ca.contactid
		WHERE ca.tourid=$tourid AND ca.status=1";
		$SelectCustomersIDresult = $SalesOrderLineDB->query($SelectCustomersIDSQL);
		$CustomersExistsCount = $SelectCustomersIDresult->num_rows;
		if ($CustomersExistsCount > 0) {
						
			$AllQueriesSQL = "";
			while($SelectCustomersIDdata = $SelectCustomersIDresult->fetch_assoc()){ 
				$CurrentCustomerID = $SelectCustomersIDdata['contactid'];
				$CurrentCustomerSOLnum = $SelectCustomersIDdata['Sales_Order_Num'];

				//To create a Customer_Invoice for this customer if it doesn't exist ONLY
				$InvoiceConnCheckDB = new mysqli($servername, $username, $password, $dbname);
				if ($InvoiceConnCheckDB->connect_error) {die("Connection failed: " . $InvoiceConnCheckDB->connect_error);} 
				$InvoiceConnCheckSQL = "SELECT * FROM customer_invoice WHERE Customer_Order_Num=$CurrentCustomerSOLnum AND Customer_Account_Customer_ID=$CurrentCustomerID";
				$InvoiceConnCheckResult = $InvoiceConnCheckDB->query($InvoiceConnCheckSQL);
				if ($InvoiceConnCheckResult->num_rows == 0) {
					$CusInv_New = "INSERT INTO customer_invoice (Customer_Invoice_Date,Customer_Order_Num,Customer_Account_Customer_ID,Group_ID)
								VALUES (NOW(),'$CurrentCustomerSOLnum','$CurrentCustomerID','$tourid');";
					$InvoiceConnCheckDB->query($CusInv_New);
					$CusInv_id = $InvoiceConnCheckDB->insert_id;
				} else {
					$CustomerInvoiceData = $InvoiceConnCheckResult->fetch_assoc();
					$CusInv_id = $CustomerInvoiceData['Customer_Invoice_Num'];
					$CusInv_Status = $CustomerInvoiceData['Group_Invoice'];
				}
				$InvoiceConnCheckDB->close();

				
				//To check if there is a Sales order line for this customer and this sales order num
				$CheckSOLexistSQL = "SELECT * FROM sales_order_line WHERE Sales_Order_Num=$CurrentCustomerSOLnum AND Line_Type='Airline Package'";
				$CheckSOLexistResult = $SalesOrderLineDB->query($CheckSOLexistSQL);
				if ($CheckSOLexistResult->num_rows == 0) {
					$AllQueriesSQL .= "INSERT INTO sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
					VALUES ('$CurrentCustomerSOLnum','Airline Package','$productid','$ProductInfoName','$ProductInfoPrice','$ProductInfoSupplier');";
				} else {
					$AllQueriesSQL .= "UPDATE sales_order_line SET Product_Product_ID='$productid',Product_Name='$ProductInfoName',Sales_Amount='$ProductInfoPrice',Supplier_Supplier_ID='$ProductInfoSupplier', Mod_Date=NOW() WHERE Sales_Order_Num=$CurrentCustomerSOLnum AND Line_Type='Airline Package';";
				}
				//If the Customer Invoice is Joint Invoice, don't do this step!
				if($CusInv_Status != 1) {
					//To check if there is an Invoice line for this customer
					$CheckCILexistSQL = "SELECT * FROM customer_invoice_line WHERE Customer_Invoice_Num=$CusInv_id AND Line_Type='1'";
					$CheckCILexistResult = $SalesOrderLineDB->query($CheckCILexistSQL);
					if ($CheckCILexistResult->num_rows == 0) {
						$AllQueriesSQL .= "INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
						VALUES ('$CusInv_id','1','1','$productid','$ProductInfoName','1','$ProductInfoPrice');";
					} else {
						$AllQueriesSQL .= "UPDATE customer_invoice_line SET Product_Product_ID='$productid',Product_Name='$ProductInfoName',Customer_Invoice_Line='1',Invoice_Line_Total='$ProductInfoPrice', Mod_Date=NOW() WHERE Customer_Invoice_Num=$CusInv_id AND Line_Type='1';";
					}
				}
			}
			if ($SalesOrderLineDB->multi_query($AllQueriesSQL) === TRUE) {
				//echo $AllQueriesSQL."<h3>Success</h3> The Airline Product has been added as sales order line for all the participants";
			} else {
				echo $AllQueriesSQL . "<br>" . $SalesOrderLineDB->error;
			}
		}
		$SalesOrderLineDB->close();

		
	echo "<h3>Success</h3> You have successfully added the Airline product to this group!<br />The page will be refresh shortly.";
	echo "<meta http-equiv='refresh' content='2;url=groups-edit.php?id=$tourid&action=edit&tab=airline'>";
}


//Update the Land Arrangements product selection
if(isset($_POST['landtourid'])){
	$tourid = mysqli_real_escape_string($db, $_POST["landtourid"]);
	$productid = mysqli_real_escape_string($db, $_POST["landproduct"]);
	$totalcost = mysqli_real_escape_string($db, $_POST["tourtotalcost"]);
	$LandProductLineCurrentID = mysqli_real_escape_string($db, $_POST["landproductlineid"]);
	$DBerrorText = NULL;
	
		//To collect the information of the Land Product
		//$ProductInfoSQL = "SELECT id,name,supplierid,cost,price FROM products WHERE id=$productid";
		$ProductInfoSQL =	"SELECT
								pro.id AS id1,
								pro.name AS name1,
								pro.supplierid AS supplierid1,
								pro.cost AS cost1,
								pro.price AS price1,
								pro.Product_Reference AS Product_Reference1,
								pro2.id AS id2,
								pro2.name AS name2,
								pro2.supplierid AS supplierid2,
								pro2.cost AS cost2,
								pro2.price AS price2
							FROM products pro
							LEFT JOIN products pro2
							ON pro.Product_Reference=pro2.id
							WHERE pro.id=$productid";
		$ProductInfoResult = $db->query($ProductInfoSQL);
		$ProductInfo = $ProductInfoResult->fetch_assoc();
			$ProductInfoID = $ProductInfo['id1'];
			$ProductInfoName = $ProductInfo['name1'];
			$ProductInfoSupplier = $ProductInfo['supplierid1'];
			$ProductInfoCost = $ProductInfo['cost1'];
			$ProductInfoPrice = $ProductInfo['price1'];
			$ProductInfoReference = $ProductInfo['Product_Reference1'];
			if($ProductInfoReference > 0) {
				$SubProductInfoName = $ProductInfo['name2'];
				$SubProductInfoSupplier = $ProductInfo['supplierid2'];
				$SubProductInfoCost = $ProductInfo['cost2'];
				$SubProductInfoPrice = $ProductInfo['price2'];
			}
			
		//The main query line
		$sql11 = "UPDATE groups SET land_productid='$productid',tripcost='$totalcost', updatetime=NOW() WHERE tourid=$tourid;";
			
		/*============= GROUP PURCHASE ORDER & GROUP PURCHASE ORDER LINE =============*/
		//Check if this group has a Group Purchase Order and get Group_PO_ID
		$GPOnumSQL = "SELECT Group_PO_ID FROM group_purchase_order WHERE Group_Group_ID=$tourid";
		$result1 = $db->query($GPOnumSQL);
		$GroupPurchaseOrderData = $result1->fetch_assoc();
		$GroupPurchaseOrderNumID = $GroupPurchaseOrderData["Group_PO_ID"];
	
		//Check if this group has a Group Purchase Order Line for the Land
		$GPOnumCheckSQL = "SELECT Group_PO_ID FROM group_po_line
						WHERE Group_PO_ID=$GroupPurchaseOrderNumID
						AND Line_Type='Land'";
		$resultCheck1 = $db->query($GPOnumCheckSQL);
		$checkIFairPOnumExist = $resultCheck1->num_rows;
		if ( $checkIFairPOnumExist == 0) {
			$sql11 .= "INSERT INTO group_po_line (Group_PO_ID,Product_Product_ID,Line_Type,Cost_Amount,Supplier_Supplier_ID)
			VALUES ('$GroupPurchaseOrderNumID','$productid','Land','$ProductInfoCost','$ProductInfoSupplier');";
		} else {
			$sql11 .= "UPDATE group_po_line SET Product_Product_ID=$productid,Cost_Amount='$ProductInfoCost',Supplier_Supplier_ID='$ProductInfoSupplier', Mod_Date=NOW() WHERE Group_PO_ID=$GroupPurchaseOrderNumID AND Line_Type='Land';";
		}

		$sql11 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','tour','$tourid','updated','$TourName');";
		
		if ($db->multi_query($sql11) === TRUE) {
			//echo "<h3>Success</h3>";
		} else {
			echo $sql11 . "<br>" . $db->error;
		}
		


		/*============= PURCHASE ORDER CHECK =============*/
		//Check if this group has a Purchase Order for AIRLINE
		$POcheckConn = new mysqli($servername, $username, $password, $dbname);
		if ($POcheckConn->connect_error) {die("Connection failed: " . $POcheckConn->connect_error);} 

		$POnumSQL = "SELECT Purchase_Order_Num FROM purchase_order WHERE Group_ID=$tourid AND Purchase_Order_Category='Land'";
		$POnumResult = $POcheckConn->query($POnumSQL);
		$PurchaseOrderCount = $POnumResult->num_rows;
		if($PurchaseOrderCount > 0) {
			$PurchaseOrderData = $POnumResult->fetch_assoc();
			$PurchaseOrderNum_ID = $PurchaseOrderData["Purchase_Order_Num"];
			$CheckPOsql = "UPDATE purchase_order SET Group_ID='$tourid',Supplier_ID='$ProductInfoSupplier', Mod_Date=NOW() WHERE Purchase_Order_Num=$PurchaseOrderNum_ID AND Purchase_Order_Category='Land';";
		} else {
			$CheckPOsql = "INSERT INTO purchase_order (Group_ID,Supplier_ID,Purchase_Order_Category,Purchase_Order_Date) VALUES ('$tourid','$ProductInfoSupplier','Land',NOW());";
		}
		if ($POcheckConn->multi_query($CheckPOsql) === TRUE) {
			$PurchaseOrderNumNewInsert = $POcheckConn->insert_id;
			if($PurchaseOrderNumNewInsert > 0) { $PurchaseOrderNum_ID = $PurchaseOrderNumNewInsert;}
			//echo $PurchaseOrderNum_ID;
			//echo "<h3>Success</h3>";
		} else {
			echo $CheckPOsql . "<br>" . $POcheckConn->error;
		}
		$POcheckConn->close();

		/*============= PURCHASE ORDER LINE CHECK =============*/
		$POconn = new mysqli($servername, $username, $password, $dbname);
		if ($POconn->connect_error) {die("Connection failed: " . $POconn->connect_error);} 
		//Check if this group has a Purchase Order Line for the Land Main Package Product
		$POnumCheckSQL2 = "SELECT Purchase_Order_Line FROM purchase_order_line
						WHERE Purchase_Order_Num=$PurchaseOrderNum_ID
						AND Line_Type='Land Package'";
		$resultCheck2 = $POconn->query($POnumCheckSQL2);
		$checkAirPOnumExist = $resultCheck2->num_rows;
		if ( $checkAirPOnumExist == 0) {
			$sql12 = "INSERT INTO purchase_order_line (Purchase_Order_Num,Line_Type,Product_Product_ID,Product_Name,Cost_Amount,Supplier_Supplier_ID)
			VALUES ('$PurchaseOrderNum_ID','Land Package','$productid','$ProductInfoName','$ProductInfoCost','$ProductInfoSupplier');";
		} else {
			$sql12 = "UPDATE purchase_order_line SET Product_Product_ID=$productid,Product_Name='$ProductInfoName',Cost_Amount='$ProductInfoCost',Supplier_Supplier_ID='$ProductInfoSupplier', Mod_Date=NOW() WHERE Purchase_Order_Num=$PurchaseOrderNum_ID AND Line_Type='Land Package';";
		}
		
		if ($POconn->multi_query($sql12) === TRUE) {
			//echo "<h3>Success</h3>";
		} else {
			echo $sql12 . "<br>" . $POconn->error;
		}
		$POconn->close();

		/*============= PURCHASE ORDER LINE CHECK -- SINGLE SUPPLEMENT =============*/
		if($ProductInfoReference > 0) {
			$POconn = new mysqli($servername, $username, $password, $dbname);
			if ($POconn->connect_error) {die("Connection failed: " . $POconn->connect_error);} 
			//Check if this group has a Purchase Order Line for the Land Main Package Product
			$POnumCheckSQL2 = "SELECT Purchase_Order_Line FROM purchase_order_line
							WHERE Purchase_Order_Num=$PurchaseOrderNum_ID
							AND Line_Type='Single Supplement'";
			$resultCheck2 = $POconn->query($POnumCheckSQL2);
			$checkAirPOnumExist = $resultCheck2->num_rows;
			if ( $checkAirPOnumExist == 0) {
				$sql13 = "INSERT INTO purchase_order_line (Purchase_Order_Num,Line_Type,Product_Product_ID,Product_Name,Cost_Amount,Supplier_Supplier_ID)
				VALUES ('$PurchaseOrderNum_ID','Single Supplement','$ProductInfoReference','$SubProductInfoName','$SubProductInfoCost','$SubProductInfoSupplier');";
			} else {
				$sql13 = "UPDATE purchase_order_line SET Product_Product_ID=$ProductInfoReference,Product_Name='$SubProductInfoName',Cost_Amount='$SubProductInfoCost',Supplier_Supplier_ID='$SubProductInfoSupplier', Mod_Date=NOW() WHERE Purchase_Order_Num=$PurchaseOrderNum_ID AND Line_Type='Single Supplement';";
			}
			
			if ($POconn->multi_query($sql13) === TRUE) {
				//echo "<h3>Success</h3>";
			} else {
				echo $sql13 . "<br>" . $POconn->error;
			}
			$POconn->close();
		}


		/*============= SUPPLIER BILL CHECK & INSER or UPDATE THE SUPPLIER LINE =============*/
		//Check if the Airline Product / Supplier has an invoice
		$SBnumDB = new mysqli($servername, $username, $password, $dbname);
		if ($SBnumDB->connect_error) {die("Connection failed: " . $SBnumDB->connect_error);} 
			$SBnumSql = "SELECT Supplier_Bill_Num FROM suppliers_bill WHERE Purchase_Order_Num=$PurchaseOrderNum_ID";
			$SBnumResult = $SBnumDB->query($SBnumSql);
			$SBnumCount = $SBnumResult->num_rows;
			if($SBnumCount > 0) {
				$SupplierBillData = $SBnumResult->fetch_assoc();
				$SupplierBillNumID = $SupplierBillData["Supplier_Bill_Num"];
				$SBnumQuery = "UPDATE suppliers_bill SET Supplier_ID=$ProductInfoSupplier,Mod_Date=NOW() WHERE Supplier_Bill_Num=$SupplierBillNumID;";
				$SBnumDB->query($SBnumQuery);
			} else {
				$SBnumQuery = "INSERT INTO suppliers_bill (Supplier_Bill_Date,Purchase_Order_Num,Supplier_ID) VALUES (NOW(),'$PurchaseOrderNum_ID','$ProductInfoSupplier');";
				$SBnumDB->query($SBnumQuery);
				$SupplierBillNumID = $SBnumDB->insert_id;
			}
			//Create the Suppleir Bil Line for the Airline
			$SBLineQuery = 	"INSERT INTO  suppliers_bill_line (Supplier_Bill_Num,Supplier_Bill_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Bill_Line_Total)
							VALUES ($SupplierBillNumID,1,'Land',$productid,'$ProductInfoName',(SELECT COUNT(contactid) FROM customer_account WHERE tourid=$tourid AND status=1 AND hisownticket=0),$ProductInfoCost)
							ON DUPLICATE KEY
							UPDATE 
								Line_Type = 'Land',
								Product_Product_ID = $productid,
								Product_Name = '$ProductInfoName',
								Qty = (SELECT COUNT(contactid) FROM customer_account WHERE tourid=$tourid AND status=1 AND hisownticket=0),
								Bill_Line_Total = $ProductInfoCost,
								Mod_Date = NOW();";
			$SBLineQuery .= "INSERT INTO  suppliers_bill_line (Supplier_Bill_Num,Supplier_Bill_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Bill_Line_Total)
							VALUES ($SupplierBillNumID,2,'Single',$ProductInfoReference,'$SubProductInfoName',(SELECT COUNT(contactid) FROM customer_account WHERE tourid=$tourid AND status=1 AND roomtype='Single'),$SubProductInfoCost)
							ON DUPLICATE KEY
							UPDATE 
								Line_Type = 'Single',
								Product_Product_ID = $ProductInfoReference,
								Product_Name = '$SubProductInfoName',
								Qty = (SELECT COUNT(contactid) FROM customer_account WHERE tourid=$tourid AND status=1 AND hisownticket=0),
								Bill_Line_Total = $SubProductInfoCost,
								Mod_Date = NOW();";
			$SBnumDB->multi_query($SBLineQuery);
		
		$SBnumDB->close();
		
		//To Add the Land arrangements product as a line in the Sales Order Line for Each Customer in the group
		$SalesOrderLineDB = new mysqli($servername, $username, $password, $dbname);
		if ($SalesOrderLineDB->connect_error) {die("Connection failed: " . $SalesOrderLineDB->connect_error);} 
		$SelectCustomersIDSQL = "SELECT so.Sales_Order_Num,ca.contactid
		FROM customer_account ca
		JOIN sales_order so 
		ON so.Customer_Account_Customer_ID=ca.contactid
		WHERE ca.tourid=$tourid AND ca.status=1";
		$SelectCustomersIDresult = $SalesOrderLineDB->query($SelectCustomersIDSQL);
		$CustomersExistsCount = $SelectCustomersIDresult->num_rows;
		if ($CustomersExistsCount > 0) {
						
			$AllQueriesSQL = "";
			while($SelectCustomersIDdata = $SelectCustomersIDresult->fetch_assoc()){ 
				$CurrentCustomerID = $SelectCustomersIDdata['contactid'];
				$CurrentCustomerSOLnum = $SelectCustomersIDdata['Sales_Order_Num'];

				//To create a Customer_Invoice for this customer if it doesn't exist ONLY
				$InvoiceConnCheckDB = new mysqli($servername, $username, $password, $dbname);
				if ($InvoiceConnCheckDB->connect_error) {die("Connection failed: " . $InvoiceConnCheckDB->connect_error);} 
				$InvoiceConnCheckSQL = "SELECT * FROM customer_invoice WHERE Customer_Order_Num=$CurrentCustomerSOLnum AND Customer_Account_Customer_ID=$CurrentCustomerID";
				$InvoiceConnCheckResult = $InvoiceConnCheckDB->query($InvoiceConnCheckSQL);
				if ($InvoiceConnCheckResult->num_rows == 0) {
					$CusInv_New = "INSERT INTO customer_invoice (Customer_Invoice_Date,Customer_Order_Num,Group_ID,Customer_Account_Customer_ID,Sales_Order_Reference)
								VALUES (NOW(),'$CurrentCustomerSOLnum','$tourid','$CurrentCustomerID','$CurrentCustomerSOLnum');";
					$InvoiceConnCheckDB->query($CusInv_New);
					$CusInv_id = $InvoiceConnCheckDB->insert_id;
				} else {
					$CustomerInvoiceData = $InvoiceConnCheckResult->fetch_assoc();
					$CusInv_id = $CustomerInvoiceData['Customer_Invoice_Num'];
					$CusInv_Status = $CustomerInvoiceData['Group_Invoice'];
				}
				$InvoiceConnCheckDB->close();
				
				//To check if there is a Sales order line for this customer and this sales order num
				$CheckSOLexistSQL = "SELECT * FROM sales_order_line WHERE Sales_Order_Num=$CurrentCustomerSOLnum AND Line_Type='Land Package'";
				$CheckSOLexistResult = $SalesOrderLineDB->query($CheckSOLexistSQL);
				if ($CheckSOLexistResult->num_rows == 0) {
					$AllQueriesSQL .= "INSERT INTO sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
					VALUES ('$CurrentCustomerSOLnum','Land Package','$productid','$ProductInfoName','$ProductInfoPrice','$ProductInfoSupplier');";
				} else {
					$AllQueriesSQL .= "UPDATE sales_order_line SET Product_Product_ID='$productid',Product_Name='$ProductInfoName',Sales_Amount='$ProductInfoPrice',Supplier_Supplier_ID='$ProductInfoSupplier', Mod_Date=NOW() WHERE Sales_Order_Num=$CurrentCustomerSOLnum AND Line_Type='Land Package';";
				}
				//If the Customer Invoice is Joint Invoice, don't do this step!
				if($CusInv_Status != 1) {
					//To check if there is an Invoice line for this customer
					$CheckCILexistSQL = "SELECT * FROM customer_invoice_line WHERE Customer_Invoice_Num=$CusInv_id AND Line_Type='2'";
					$CheckCILexistResult = $SalesOrderLineDB->query($CheckCILexistSQL);
					if ($CheckCILexistResult->num_rows == 0) {
						$AllQueriesSQL .= "INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
						VALUES ('$CusInv_id','2','2','$productid','$ProductInfoName','1','$ProductInfoPrice');";
					} else {
						$AllQueriesSQL .= "UPDATE customer_invoice_line SET Product_Product_ID='$productid',Product_Name='$ProductInfoName',Customer_Invoice_Line='2',Invoice_Line_Total='$ProductInfoPrice', Mod_Date=NOW() WHERE Customer_Invoice_Num=$CusInv_id AND Line_Type='2';";
					}
				}
			}
			if ($SalesOrderLineDB->multi_query($AllQueriesSQL) === TRUE) {
				//echo $AllQueriesSQL."<h3>Success</h3> The Land Product has been added as sales order line for all the participants";
				//echo "<h3>Success</h3> You have successfully added the Land product to this group!<br />The page will be refresh shortly.";
				//echo "<meta http-equiv='refresh' content='2;url=groups-edit.php?id=$tourid&action=edit&tab=land'>";
			} else {
				$DBerrorText .= $SalesOrderLineDB->error;
			}
		}
		$SalesOrderLineDB->close();
		
		if($DBerrorText != NULL) {
			echo $DBerrorText;
		} else {
			echo "<h3>Success</h3> You have successfully added the Land product to this group!<br />The page will be refresh shortly.";
			echo "<meta http-equiv='refresh' content='2;url=groups-edit.php?id=$tourid&action=edit&tab=land'>";
		}
	
}




//Update the Airline Info of the Group / Tour
if(isset($_POST['airlinez'])){
    $tourid = mysqli_real_escape_string($db, $_POST["airlinez"]);
    $TourName = mysqli_real_escape_string($db, $_POST["TourNameforAirline"]);
	$airlineproductidz = mysqli_real_escape_string($db, $_POST["airlineproductidz"]);
	$flightinfo = mysqli_real_escape_string($db, $_POST["flightinfo"]);
	$seats = mysqli_real_escape_string($db, $_POST["seats"]);
	$depositdate = mysqli_real_escape_string($db, $_POST["depositdate"]);
	$depositamount = mysqli_real_escape_string($db, $_POST["depositamount"]);
	$canceldate = mysqli_real_escape_string($db, $_POST["canceldate"]);
	$namesdue = mysqli_real_escape_string($db, $_POST["namesdue"]);
	$finalpaymentdue = mysqli_real_escape_string($db, $_POST["finalpaymentdue"]);
	$confirmation = mysqli_real_escape_string($db, $_POST["confirmation"]);
	$Tickets_Dropbox = mysqli_real_escape_string($db, $_POST["dropbox"]);
	$ticketprice = mysqli_real_escape_string($db, $_POST["ticketprice"]);
	//$airlinename = $_POST["airlinename"];
	//$airfarecost = $_POST["airfarecost"];
	//airlinename='$airlinename',airfarecost='$airfarecost'
	$depositreturndate = mysqli_real_escape_string($db, $_POST["depositreturndate"]);
	$returnedamount = mysqli_real_escape_string($db, $_POST["returnedamount"]);
	if ($_POST["airlines"]==1){$airlines = "1";} else {$airlines = "0";}
	
	/////////////// GROUP FLIGHT SECTION START //////////////////////
	//Group Flight Info Data #1
	
	$AirlineID1 = mysqli_real_escape_string($db, $_POST["AirlineID1"]);
	if($AirlineID1 != NULL) {
		$GF_Line_ID1 = mysqli_real_escape_string($db, $_POST["GF_Line_ID1"]);
		$AirlineFlightID1 = mysqli_real_escape_string($db, $_POST["AirlineFlightID1"]);
		$DepartureAirportID1 = mysqli_real_escape_string($db, $_POST["DepartureAirportID1"]);
		$DEPtime1 = mysqli_real_escape_string($db, $_POST["DEPtime1"]);
		$ArrivalAirportID1 = mysqli_real_escape_string($db, $_POST["ArrivalAirportID1"]);
		$ARRtime1 = mysqli_real_escape_string($db, $_POST["ARRtime1"]);
		$SupplierTrigger1 = mysqli_real_escape_string($db, $_POST["SupplierTrigger1"]); //0 is not selected, 1 is arriving, 2 is leaving
		if($SupplierTrigger1 == "") { $SupplierTriggerValue = 0; } else { $SupplierTriggerValue = $SupplierTrigger1; }

		//Insert or Update the line for the Group's flight info
		$GroupFlightSQL1 =	"INSERT INTO  groups_flights (Group_ID, GF_Line_ID, Flight_Airline_ID, Flight_Airline_Number, Departure_Airport, Departure_Time, Arrival_Airport, Arrival_Time, Supplier_Trigger)
							VALUES ($tourid,1,'$AirlineID1','$AirlineFlightID1','$DepartureAirportID1','$DEPtime1','$ArrivalAirportID1','$ARRtime1','$SupplierTriggerValue')
							ON DUPLICATE KEY
							UPDATE 
								Flight_Airline_ID = '$AirlineID1',
								Flight_Airline_Number = '$AirlineFlightID1',
								Departure_Airport = '$DepartureAirportID1',
								Departure_Time = '$DEPtime1',
								Arrival_Airport = '$ArrivalAirportID1',
								Arrival_Time = '$ARRtime1',
								Supplier_Trigger = '$SupplierTriggerValue';";
		GDb::execute($GroupFlightSQL1);
	}
	
	//Group Flight Info Data #2
	$AirlineID2 = mysqli_real_escape_string($db, $_POST["AirlineID2"]);
	if($AirlineID2 != NULL) {
		$GF_Line_ID2 = mysqli_real_escape_string($db, $_POST["GF_Line_ID2"]);
		$AirlineFlightID2 = mysqli_real_escape_string($db, $_POST["AirlineFlightID2"]);
		$DepartureAirportID2 = mysqli_real_escape_string($db, $_POST["DepartureAirportID2"]);
		$DEPtime2 = mysqli_real_escape_string($db, $_POST["DEPtime2"]);
		$ArrivalAirportID2 = mysqli_real_escape_string($db, $_POST["ArrivalAirportID2"]);
		$ARRtime2 = mysqli_real_escape_string($db, $_POST["ARRtime2"]);
		$SupplierTrigger2 = mysqli_real_escape_string($db, $_POST["SupplierTrigger2"]); //0 is not selected, 1 is arriving, 2 is leaving
		if($SupplierTrigger2 == "") { $SupplierTriggerValue = 0; } else { $SupplierTriggerValue = $SupplierTrigger2; }

		//Insert or Update the line for the Group's flight info
		$GroupFlightSQL1 =	"INSERT INTO  groups_flights (Group_ID, GF_Line_ID, Flight_Airline_ID, Flight_Airline_Number, Departure_Airport, Departure_Time, Arrival_Airport, Arrival_Time, Supplier_Trigger)
							VALUES ($tourid,2,'$AirlineID2','$AirlineFlightID2','$DepartureAirportID2','$DEPtime2','$ArrivalAirportID2','$ARRtime2','$SupplierTriggerValue')
							ON DUPLICATE KEY
							UPDATE 
								Flight_Airline_ID = '$AirlineID2',
								Flight_Airline_Number = '$AirlineFlightID2',
								Departure_Airport = '$DepartureAirportID2',
								Departure_Time = '$DEPtime2',
								Arrival_Airport = '$ArrivalAirportID2',
								Arrival_Time = '$ARRtime2',
								Supplier_Trigger = '$SupplierTriggerValue';";
		GDb::execute($GroupFlightSQL1);
	}
	
	//Group Flight Info Data #3
	$AirlineID3 = mysqli_real_escape_string($db, $_POST["AirlineID3"]);
	if($AirlineID3 != NULL) {
		$GF_Line_ID3 = mysqli_real_escape_string($db, $_POST["GF_Line_ID3"]);
		$AirlineFlightID3 = mysqli_real_escape_string($db, $_POST["AirlineFlightID3"]);
		$DepartureAirportID3 = mysqli_real_escape_string($db, $_POST["DepartureAirportID3"]);
		$DEPtime3 = mysqli_real_escape_string($db, $_POST["DEPtime3"]);
		$ArrivalAirportID3 = mysqli_real_escape_string($db, $_POST["ArrivalAirportID3"]);
		$ARRtime3 = mysqli_real_escape_string($db, $_POST["ARRtime3"]);
		$SupplierTrigger3 = mysqli_real_escape_string($db, $_POST["SupplierTrigger3"]); //0 is not selected, 1 is arriving, 2 is leaving
		if($SupplierTrigger3 == "") { $SupplierTriggerValue = 0; } else { $SupplierTriggerValue = $SupplierTrigger3; }

		//Insert or Update the line for the Group's flight info
		$GroupFlightSQL3 =	"INSERT INTO  groups_flights (Group_ID, GF_Line_ID, Flight_Airline_ID, Flight_Airline_Number, Departure_Airport, Departure_Time, Arrival_Airport, Arrival_Time, Supplier_Trigger)
							VALUES ($tourid,3,'$AirlineID3','$AirlineFlightID3','$DepartureAirportID3','$DEPtime3','$ArrivalAirportID3','$ARRtime3','$SupplierTriggerValue')
							ON DUPLICATE KEY
							UPDATE 
								Flight_Airline_ID = '$AirlineID3',
								Flight_Airline_Number = '$AirlineFlightID3',
								Departure_Airport = '$DepartureAirportID3',
								Departure_Time = '$DEPtime3',
								Arrival_Airport = '$ArrivalAirportID3',
								Arrival_Time = '$ARRtime3',
								Supplier_Trigger = '$SupplierTriggerValue';";
		GDb::execute($GroupFlightSQL3);
	}
	
	
	//Group Flight Info Data #4
	$AirlineID4 = mysqli_real_escape_string($db, $_POST["AirlineID4"]);
	if($AirlineID4 != NULL) {
		$GF_Line_ID4 = mysqli_real_escape_string($db, $_POST["GF_Line_ID4"]);
		$AirlineFlightID4 = mysqli_real_escape_string($db, $_POST["AirlineFlightID4"]);
		$DepartureAirportID4 = mysqli_real_escape_string($db, $_POST["DepartureAirportID4"]);
		$DEPtime4 = mysqli_real_escape_string($db, $_POST["DEPtime4"]);
		$ArrivalAirportID4 = mysqli_real_escape_string($db, $_POST["ArrivalAirportID4"]);
		$ARRtime4 = mysqli_real_escape_string($db, $_POST["ARRtime4"]);
		$SupplierTrigger4 = mysqli_real_escape_string($db, $_POST["SupplierTrigger4"]); //0 is not selected, 1 is arriving, 2 is leaving
		if($SupplierTrigger4 == "") { $SupplierTriggerValue = 0; } else { $SupplierTriggerValue = $SupplierTrigger4; }

		//Insert or Update the line for the Group's flight info
		$GroupFlightSQL4 =	"INSERT INTO  groups_flights (Group_ID, GF_Line_ID, Flight_Airline_ID, Flight_Airline_Number, Departure_Airport, Departure_Time, Arrival_Airport, Arrival_Time, Supplier_Trigger)
							VALUES ($tourid,4,'$AirlineID4','$AirlineFlightID4','$DepartureAirportID4','$DEPtime4','$ArrivalAirportID4','$ARRtime4','$SupplierTriggerValue')
							ON DUPLICATE KEY
							UPDATE 
								Flight_Airline_ID = '$AirlineID4',
								Flight_Airline_Number = '$AirlineFlightID4',
								Departure_Airport = '$DepartureAirportID4',
								Departure_Time = '$DEPtime4',
								Arrival_Airport = '$ArrivalAirportID4',
								Arrival_Time = '$ARRtime4',
								Supplier_Trigger = '$SupplierTriggerValue';";
		GDb::execute($GroupFlightSQL4);
	}
	
	
	//Group Flight Info Data #5
	$AirlineID5 = mysqli_real_escape_string($db, $_POST["AirlineID5"]);
	if($AirlineID5 != NULL) {
		$GF_Line_ID5 = mysqli_real_escape_string($db, $_POST["GF_Line_ID5"]);
		$AirlineFlightID5 = mysqli_real_escape_string($db, $_POST["AirlineFlightID5"]);
		$DepartureAirportID5 = mysqli_real_escape_string($db, $_POST["DepartureAirportID5"]);
		$DEPtime5 = mysqli_real_escape_string($db, $_POST["DEPtime5"]);
		$ArrivalAirportID5 = mysqli_real_escape_string($db, $_POST["ArrivalAirportID5"]);
		$ARRtime5 = mysqli_real_escape_string($db, $_POST["ARRtime5"]);
		$SupplierTrigger5 = mysqli_real_escape_string($db, $_POST["SupplierTrigger5"]); //0 is not selected, 1 is arriving, 2 is leaving
		if($SupplierTrigger5 == "") { $SupplierTriggerValue = 0; } else { $SupplierTriggerValue = $SupplierTrigger5; }

		//Insert or Update the line for the Group's flight info
		$GroupFlightSQL5 =	"INSERT INTO  groups_flights (Group_ID, GF_Line_ID, Flight_Airline_ID, Flight_Airline_Number, Departure_Airport, Departure_Time, Arrival_Airport, Arrival_Time, Supplier_Trigger)
							VALUES ($tourid,5,'$AirlineID5','$AirlineFlightID5','$DepartureAirportID5','$DEPtime5','$ArrivalAirportID5','$ARRtime5','$SupplierTriggerValue')
							ON DUPLICATE KEY
							UPDATE 
								Flight_Airline_ID = '$AirlineID5',
								Flight_Airline_Number = '$AirlineFlightID5',
								Departure_Airport = '$DepartureAirportID5',
								Departure_Time = '$DEPtime5',
								Arrival_Airport = '$ArrivalAirportID5',
								Arrival_Time = '$ARRtime5',
								Supplier_Trigger = '$SupplierTriggerValue';";
		GDb::execute($GroupFlightSQL5);
	}
	
	
	//Group Flight Info Data #6
	$AirlineID6 = mysqli_real_escape_string($db, $_POST["AirlineID6"]);
	if($AirlineID6 != NULL) {
		$GF_Line_ID6 = mysqli_real_escape_string($db, $_POST["GF_Line_ID6"]);
		$AirlineFlightID6 = mysqli_real_escape_string($db, $_POST["AirlineFlightID6"]);
		$DepartureAirportID6 = mysqli_real_escape_string($db, $_POST["DepartureAirportID6"]);
		$DEPtime6 = mysqli_real_escape_string($db, $_POST["DEPtime6"]);
		$ArrivalAirportID6 = mysqli_real_escape_string($db, $_POST["ArrivalAirportID6"]);
		$ARRtime6 = mysqli_real_escape_string($db, $_POST["ARRtime6"]);
		$SupplierTrigger6 = mysqli_real_escape_string($db, $_POST["SupplierTrigger6"]); //0 is not selected, 1 is arriving, 2 is leaving
		if($SupplierTrigger6 == "") { $SupplierTriggerValue = 0; } else { $SupplierTriggerValue = $SupplierTrigger6; }

		//Insert or Update the line for the Group's flight info
		$GroupFlightSQL6 =	"INSERT INTO  groups_flights (Group_ID, GF_Line_ID, Flight_Airline_ID, Flight_Airline_Number, Departure_Airport, Departure_Time, Arrival_Airport, Arrival_Time, Supplier_Trigger)
							VALUES ($tourid,6,'$AirlineID6','$AirlineFlightID6','$DepartureAirportID6','$DEPtime6','$ArrivalAirportID6','$ARRtime6','$SupplierTriggerValue')
							ON DUPLICATE KEY
							UPDATE 
								Flight_Airline_ID = '$AirlineID6',
								Flight_Airline_Number = '$AirlineFlightID6',
								Departure_Airport = '$DepartureAirportID6',
								Departure_Time = '$DEPtime6',
								Arrival_Airport = '$ArrivalAirportID6',
								Arrival_Time = '$ARRtime6',
								Supplier_Trigger = '$SupplierTriggerValue';";
		GDb::execute($GroupFlightSQL6);
	}
	
	
	//Group Flight Info Data #7
	$AirlineID7 = mysqli_real_escape_string($db, $_POST["AirlineID7"]);
	if($AirlineID7 != NULL) {
		$GF_Line_ID7 = mysqli_real_escape_string($db, $_POST["GF_Line_ID7"]);
		$AirlineFlightID7 = mysqli_real_escape_string($db, $_POST["AirlineFlightID7"]);
		$DepartureAirportID7 = mysqli_real_escape_string($db, $_POST["DepartureAirportID7"]);
		$DEPtime7 = mysqli_real_escape_string($db, $_POST["DEPtime7"]);
		$ArrivalAirportID7 = mysqli_real_escape_string($db, $_POST["ArrivalAirportID7"]);
		$ARRtime7 = mysqli_real_escape_string($db, $_POST["ARRtime7"]);
		$SupplierTrigger7 = mysqli_real_escape_string($db, $_POST["SupplierTrigger7"]); //0 is not selected, 1 is arriving, 2 is leaving
		if($SupplierTrigger7 == "") { $SupplierTriggerValue = 0; } else { $SupplierTriggerValue = $SupplierTrigger7; }

		//Insert or Update the line for the Group's flight info
		$GroupFlightSQL7 =	"INSERT INTO  groups_flights (Group_ID, GF_Line_ID, Flight_Airline_ID, Flight_Airline_Number, Departure_Airport, Departure_Time, Arrival_Airport, Arrival_Time, Supplier_Trigger)
							VALUES ($tourid,7,'$AirlineID7','$AirlineFlightID7','$DepartureAirportID7','$DEPtime7','$ArrivalAirportID7','$ARRtime7','$SupplierTriggerValue')
							ON DUPLICATE KEY
							UPDATE 
								Flight_Airline_ID = '$AirlineID7',
								Flight_Airline_Number = '$AirlineFlightID7',
								Departure_Airport = '$DepartureAirportID7',
								Departure_Time = '$DEPtime7',
								Arrival_Airport = '$ArrivalAirportID7',
								Arrival_Time = '$ARRtime7',
								Supplier_Trigger = '$SupplierTriggerValue';";
		GDb::execute($GroupFlightSQL7);
	}
	
	
	//Group Flight Info Data #8
	$AirlineID8 = mysqli_real_escape_string($db, $_POST["AirlineID8"]);
	if($AirlineID8 != NULL) {
		$GF_Line_ID8 = mysqli_real_escape_string($db, $_POST["GF_Line_ID8"]);
		$AirlineFlightID8 = mysqli_real_escape_string($db, $_POST["AirlineFlightID8"]);
		$DepartureAirportID8 = mysqli_real_escape_string($db, $_POST["DepartureAirportID8"]);
		$DEPtime8 = mysqli_real_escape_string($db, $_POST["DEPtime8"]);
		$ArrivalAirportID8 = mysqli_real_escape_string($db, $_POST["ArrivalAirportID8"]);
		$ARRtime8 = mysqli_real_escape_string($db, $_POST["ARRtime8"]);
		$SupplierTrigger8 = mysqli_real_escape_string($db, $_POST["SupplierTrigger8"]); //0 is not selected, 1 is arriving, 2 is leaving
		if($SupplierTrigger8 == "") { $SupplierTriggerValue = 0; } else { $SupplierTriggerValue = $SupplierTrigger8; }

		//Insert or Update the line for the Group's flight info
		$GroupFlightSQL8 =	"INSERT INTO  groups_flights (Group_ID, GF_Line_ID, Flight_Airline_ID, Flight_Airline_Number, Departure_Airport, Departure_Time, Arrival_Airport, Arrival_Time, Supplier_Trigger)
							VALUES ($tourid,8,'$AirlineID8','$AirlineFlightID8','$DepartureAirportID8','$DEPtime8','$ArrivalAirportID8','$ARRtime8','$SupplierTriggerValue')
							ON DUPLICATE KEY
							UPDATE 
								Flight_Airline_ID = '$AirlineID8',
								Flight_Airline_Number = '$AirlineFlightID8',
								Departure_Airport = '$DepartureAirportID8',
								Departure_Time = '$DEPtime8',
								Arrival_Airport = '$ArrivalAirportID8',
								Arrival_Time = '$ARRtime8',
								Supplier_Trigger = '$SupplierTriggerValue';";
		GDb::execute($GroupFlightSQL8);
	}
	
	
	//Group Flight Info Data #9
	$AirlineID9 = mysqli_real_escape_string($db, $_POST["AirlineID9"]);
	if($AirlineID9 != NULL) {
		$GF_Line_ID9 = mysqli_real_escape_string($db, $_POST["GF_Line_ID9"]);
		$AirlineFlightID9 = mysqli_real_escape_string($db, $_POST["AirlineFlightID9"]);
		$DepartureAirportID9 = mysqli_real_escape_string($db, $_POST["DepartureAirportID9"]);
		$DEPtime9 = mysqli_real_escape_string($db, $_POST["DEPtime9"]);
		$ArrivalAirportID9 = mysqli_real_escape_string($db, $_POST["ArrivalAirportID9"]);
		$ARRtime9 = mysqli_real_escape_string($db, $_POST["ARRtime9"]);
		$SupplierTrigger9 = mysqli_real_escape_string($db, $_POST["SupplierTrigger9"]); //0 is not selected, 1 is arriving, 2 is leaving
		if($SupplierTrigger9 == "") { $SupplierTriggerValue = 0; } else { $SupplierTriggerValue = $SupplierTrigger9; }

		//Insert or Update the line for the Group's flight info
		$GroupFlightSQL9 =	"INSERT INTO  groups_flights (Group_ID, GF_Line_ID, Flight_Airline_ID, Flight_Airline_Number, Departure_Airport, Departure_Time, Arrival_Airport, Arrival_Time, Supplier_Trigger)
							VALUES ($tourid,9,'$AirlineID9','$AirlineFlightID9','$DepartureAirportID9','$DEPtime9','$ArrivalAirportID9','$ARRtime9','$SupplierTriggerValue')
							ON DUPLICATE KEY
							UPDATE 
								Flight_Airline_ID = '$AirlineID9',
								Flight_Airline_Number = '$AirlineFlightID9',
								Departure_Airport = '$DepartureAirportID9',
								Departure_Time = '$DEPtime9',
								Arrival_Airport = '$ArrivalAirportID9',
								Arrival_Time = '$ARRtime9',
								Supplier_Trigger = '$SupplierTriggerValue';";
		GDb::execute($GroupFlightSQL9);
	}
	
	
	//Group Flight Info Data #10
	$AirlineID10 = mysqli_real_escape_string($db, $_POST["AirlineID10"]);
	if($AirlineID10 != NULL) {
		$GF_Line_ID10 = mysqli_real_escape_string($db, $_POST["GF_Line_ID10"]);
		$AirlineFlightID10 = mysqli_real_escape_string($db, $_POST["AirlineFlightID10"]);
		$DepartureAirportID10 = mysqli_real_escape_string($db, $_POST["DepartureAirportID10"]);
		$DEPtime10 = mysqli_real_escape_string($db, $_POST["DEPtime10"]);
		$ArrivalAirportID10 = mysqli_real_escape_string($db, $_POST["ArrivalAirportID10"]);
		$ARRtime10 = mysqli_real_escape_string($db, $_POST["ARRtime10"]);
		$SupplierTrigger10 = mysqli_real_escape_string($db, $_POST["SupplierTrigger10"]); //0 is not selected, 1 is arriving, 2 is leaving
		if($SupplierTrigger10 == "") { $SupplierTriggerValue = 0; } else { $SupplierTriggerValue = $SupplierTrigger10; }

		//Insert or Update the line for the Group's flight info
		$GroupFlightSQL10 =	"INSERT INTO  groups_flights (Group_ID, GF_Line_ID, Flight_Airline_ID, Flight_Airline_Number, Departure_Airport, Departure_Time, Arrival_Airport, Arrival_Time, Supplier_Trigger)
							VALUES ($tourid,10,'$AirlineID10','$AirlineFlightID10','$DepartureAirportID10','$DEPtime10','$ArrivalAirportID10','$ARRtime10','$SupplierTriggerValue')
							ON DUPLICATE KEY
							UPDATE 
								Flight_Airline_ID = '$AirlineID10',
								Flight_Airline_Number = '$AirlineFlightID10',
								Departure_Airport = '$DepartureAirportID10',
								Departure_Time = '$DEPtime10',
								Arrival_Airport = '$ArrivalAirportID10',
								Arrival_Time = '$ARRtime10',
								Supplier_Trigger = '$SupplierTriggerValue';";
		GDb::execute($GroupFlightSQL10);
	}
	
	
	/////////////// GROUP FLIGHT SECTION IS DONE //////////////////////

	$con11 = new mysqli($servername, $username, $password, $dbname);
	if ($con11->connect_error) {die("Connection failed: " . $con11->connect_error);} 
	$sql11 = "UPDATE products SET Flight_info='$flightinfo',Confirmation='$confirmation',Tickets_Dropbox='$Tickets_Dropbox',Deposit_Return_Date='$depositreturndate',Returned_Amount='$returnedamount',seats='$seats', Deposit_Date='$depositdate', Deposit_Amount='$depositamount', Cancel_Date='$canceldate', Names_Due='$namesdue', Final_Payment_Due='$finalpaymentdue',Airline_Ready='$airlines', updatetime=NOW() WHERE id=$airlineproductidz;";
	$sql11 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','tour','$tourid','edited','$TourName');";
if ($con11->multi_query($sql11) === TRUE) {
	echo "<h3>Success</h3> The Airline information has been updated successfully!";
	echo "<meta http-equiv='refresh' content='2;url=groups-edit.php?id=$tourid&action=edit&tab=airline'>";
} else {
	echo $sql11 . "<br>" . $con11->error;
}
$con11->close();
}

//Update the check list of the Group / Tour
if(isset($_POST['checklistz'])){
	$tourid = $_POST["checklistz"];
	$TourName = $_POST["checklistzname"];
	if ($_POST["guide"]==1){$guide = "1";} else {$guide = "0";}
	if ($_POST["buses"]==1){$buses = "1";} else {$buses = "0";}
	if ($_POST["hotels"]==1){$hotels = "1";} else {$hotels = "0";}
	if ($_POST["insurance"]==1){$insurance = "1";} else {$insurance = "0";}
	if ($_POST["rooming"]==1){$rooming = "1";} else {$rooming = "0";}

	$con11 = new mysqli($servername, $username, $password, $dbname);
	if ($con11->connect_error) {die("Connection failed: " . $con11->connect_error);} 
	$sql11 = "UPDATE groups SET guide='$guide', buses='$buses', hotels='$hotels', insurance='$insurance', rooming='$rooming', updatetime=NOW() WHERE tourid=$tourid;";
	$sql11 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','tour','$tourid','edited','$TourName');";
if ($con11->multi_query($sql11) === TRUE) {
	echo "<h3>Success</h3> The Check List for the tour ($TourName) has been updated successfully!";
} else {
	echo $sql11 . "<br>" . $con11->error;
}
$con11->close();
}

//Update the expenses of the Group / Tour
if(isset($_POST['expensez'])){
	$tourid = mysqli_real_escape_string($db, $_POST["expensez"]);
	$TourName = mysqli_real_escape_string($db, $_POST["expensezname"]);
	$airfareproduct = str_replace("l", "", $_POST["airfareproduct"]);
	$landproduct = str_replace("l", "", $_POST["landproduct"]);
	$insuranceproduct = str_replace("l", "", $_POST["insuranceproduct"]);
	
	$air = mysqli_real_escape_string($db, $_POST["airfareproductcost"]);
	$land = mysqli_real_escape_string($db, $_POST["landproductcost"]);
	$insurance = $_POST["insuranceproductcost"];
	$totalcost = $air + $land + $insurance;

	$con11 = new mysqli($servername, $username, $password, $dbname);
	if ($con11->connect_error) {die("Connection failed: " . $con11->connect_error);} 
	$sql11 = "UPDATE groups SET product_air='$airfareproduct',product_land='$landproduct',product_insurance='$insuranceproduct',tripcost='$totalcost', updatetime=NOW() WHERE tourid=$tourid;";
	$sql11 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','tour','$tourid','edited','$TourName');";
if ($con11->multi_query($sql11) === TRUE) {
	echo "<h3>Success</h3> The expenses of the tour ($TourName) has been updated successfully!";
} else {
	echo $sql11 . "<br>" . $con11->error;
}
$con11->close();
}

//Update the data of the products
if(isset($_POST['productid'])){

$productid = mysqli_real_escape_string($db, $_POST['productid']);
$name = mysqli_real_escape_string($db, $_POST["name"]);
$description = mysqli_real_escape_string($db, $_POST["description"]);
$cost = mysqli_real_escape_string($db, $_POST["cost"]);
$price = mysqli_real_escape_string($db, $_POST["price"]);
$paymentterm = mysqli_real_escape_string($db, $_POST["paymentterm"]);
$subproduct = mysqli_real_escape_string($db, $_POST["subproduct"]);
if($subproduct == 0) {
	$parentid = $producttype = $meta1 = $meta2 = $meta3 = $meta4 = 0;
} else {
	$parentid = mysqli_real_escape_string($db, $_POST["parentid"]);
	$producttype = mysqli_real_escape_string($db, $_POST["producttype"]);
	if($producttype == "Airfare"){
		$meta1 = mysqli_real_escape_string($db, $_POST["departurecity"]);
		$meta2 = mysqli_real_escape_string($db, $_POST["departurestate"]);
		$meta3 = NULL;
		$meta4 = NULL;
	} elseif ($producttype == "Hotels"){
		$meta1 = mysqli_real_escape_string($db, $_POST["hotelphone"]);
		$meta2 = mysqli_real_escape_string($db, $_POST["hotelcity"]);
		$meta3 = mysqli_real_escape_string($db, $_POST["checkindate"]);
		$meta4 = mysqli_real_escape_string($db, $_POST["checkoutdate"]);
	} elseif ($producttype == "Guides"){
		$meta1 = mysqli_real_escape_string($db, $_POST["GuidePhone"]);
		$meta2 = mysqli_real_escape_string($db, $_POST["GuideCity"]);
		$meta3 = NULL;
		$meta4 = NULL;
	} else {
		$meta1 = NULL;
		$meta2 = NULL;
		$meta3 = NULL;
		$meta4 = NULL;
	}
}
$AirlineActive = mysqli_real_escape_string($db, $_POST["AirlineActive"]);
if($AirlineActive == 1) {
	$Flight_info = mysqli_real_escape_string($db, $_POST["Flight_info"]);
	$Confirmation = mysqli_real_escape_string($db, $_POST["Confirmation"]);
	$Returned_Amount = mysqli_real_escape_string($db, $_POST["Returned_Amount"]);
	$Deposit_Amount = mysqli_real_escape_string($db, $_POST["Deposit_Amount"]);
	$seats = mysqli_real_escape_string($db, $_POST["seats"]);
	$Deposit_Return_Date = mysqli_real_escape_string($db, $_POST["Deposit_Return_Date"]);
	$Deposit_Date = mysqli_real_escape_string($db, $_POST["Deposit_Date"]);
	$Cancel_Date = mysqli_real_escape_string($db, $_POST["Cancel_Date"]);
	$Names_Due = mysqli_real_escape_string($db, $_POST["Names_Due"]);
	$Final_Payment_Due = mysqli_real_escape_string($db, $_POST["Final_Payment_Due"]);
} else {
	$Confirmation = $Returned_Amount = $Deposit_Amount = $seats = $Deposit_Return_Date = $Deposit_Date = $Cancel_Date = $Names_Due = $Final_Payment_Due = NULL;
}

$status = mysqli_real_escape_string($db, $_POST["status"]);
$thesupplier = mysqli_real_escape_string($db, $_POST['thesupplier']);
$thecategory = mysqli_real_escape_string($db, $_POST['thecategory']);

//To create or update the Ticket Discount Product
$Airline_Credit_Product = mysqli_real_escape_string($db, $_POST["Airline_Credit_Product"]);
$Airline_Credit_Check = mysqli_real_escape_string($db, $_POST["Airline_Credit_Check"]);
$Airline_Credit_Amount = mysqli_real_escape_string($db, $_POST["Airline_Credit_Amount"]);
//Create DB connection to update or insert the Ticket Discount Product
$TicketDBconn = new mysqli($servername, $username, $password, $dbname);
if ($TicketDBconn->connect_error) {die("Connection failed: " . $TicketDBconn->connect_error);} 

if($Airline_Credit_Check == 1) {
	if($Airline_Credit_Product > 0) {
		$TicketDiscountSQL = "UPDATE products SET name='$name | Airline Credit',supplierid='$thesupplier',categoryid='6',cost='0', price='-$Airline_Credit_Amount',updatetime=NOW() WHERE id=$Airline_Credit_Product;";
	} else {
		$TicketDiscountSQL = "INSERT INTO products (name,supplierid,categoryid,cost,price,status) VALUES ('$name | Ticket Discount','$thesupplier','6','0','-$Airline_Credit_Amount','1');";		
	}
	if ($TicketDBconn->multi_query($TicketDiscountSQL) === TRUE) {
        //Nithin : Reflect price change in customer invoices. {
        if( $Airline_Credit_Product > 0 ) {
            (new Products())->raisePriceChangedEvent($Airline_Credit_Product) ;
        }
        //}
		//echo "<h3>Success</h3> The product has been updated successfully!";
		$TicketProNewID = $TicketDBconn->insert_id;
		$AirlineCreditProductID = $TicketProNewID;
	} else {
		echo $TicketDiscountSQL . "<br>" . $TicketDBconn->error."<br> error";
		$AirlineCreditProductID = $Airline_Credit_Product;
	}
	$TicketDBconn->close();
}


$SingleSupplementCheckButton = mysqli_real_escape_string($db, $_POST["SingleSupplementCheckButton"]);
if($SingleSupplementCheckButton == 1) {
	//First, collect the fields data
	$SingleProID = mysqli_real_escape_string($db, $_POST["SingleProID"]);
	$SingleSupplier = mysqli_real_escape_string($db, $_POST["SingleSupplier"]);
	$SingleName = mysqli_real_escape_string($db, $_POST["SingleName"]);
	$SingleSuppCost = mysqli_real_escape_string($db, $_POST["SingleSuppCost"]);
	$SingleSuppPrice = mysqli_real_escape_string($db, $_POST["SingleSuppPrice"]);
	
	//Create DB connection to update or insert the Single Supplement Product
	$SingleDBconn = new mysqli($servername, $username, $password, $dbname);
	if ($SingleDBconn->connect_error) {die("Connection failed: " . $SingleDBconn->connect_error);} 

	//Check if the single product is there or not
		if($SingleProID > 0) {
			$SingleSQL = "UPDATE products SET name='$SingleName',supplierid='$SingleSupplier',categoryid='7',cost='$SingleSuppCost', price='$SingleSuppPrice',updatetime=NOW() WHERE id=$SingleProID;";
		} else {
			$SingleSQL = "INSERT INTO products (name,supplierid,categoryid,cost,price,status) VALUES ('$SingleName','$SingleSupplier','7','$SingleSuppCost','$SingleSuppPrice','1');";		
		}
	if ($SingleDBconn->multi_query($SingleSQL) === TRUE) {
        //Nithin : Reflect price change in customer invoices. {
        if( $SingleProID > 0 ) {
            (new Products())->raisePriceChangedEvent($SingleProID) ;
        }
        //}
        //echo "<h3>Success</h3> The product has been updated successfully!";
		$SingleProNewID = $SingleDBconn->insert_id;
	} else {
	echo $SingleSQL . "<br>" . $SingleDBconn->error."<br> error";
	}
	$SingleDBconn->close();
}	

//To add the Single Supplement or Ticket Discount Porducts ID to the Product_Reference in Products Table Line
if($SingleProNewID > 0) {
	$QueryAddReference = "Product_Reference='$SingleProNewID',";
	$SingleProID = $SingleProNewID;
} elseif($AirlineCreditProductID > 0 AND $Airline_Credit_Check == 1) {
	$QueryAddReference = "Product_Reference='$AirlineCreditProductID',";
} else {
	$QueryAddReference = "";
}

	$procon = new mysqli($servername, $username, $password, $dbname);
	if ($procon->connect_error) {die("Connection failed: " . $procon->connect_error);} 
	
$prosql = "UPDATE products SET name='$name', description='$description', supplierid='$thesupplier', categoryid='$thecategory',$QueryAddReference subproduct='$subproduct',parentid='$parentid', producttype='$producttype', cost='$cost', price='$price',paymentterm='$paymentterm',meta1='$meta1',meta2='$meta2',meta3='$meta3',meta4='$meta4', status='$status',Flight_info='$Flight_info',Confirmation='$Confirmation',Deposit_Return_Date='$Deposit_Return_Date',Returned_Amount='$Returned_Amount',seats='$seats', Deposit_Date='$Deposit_Date', Deposit_Amount='$Deposit_Amount', Cancel_Date='$Cancel_Date', Names_Due='$Names_Due', Final_Payment_Due='$Final_Payment_Due', updatetime=NOW() WHERE id=$productid;";

$prosql .= "UPDATE group_po_line SET Supplier_Supplier_ID='$thesupplier',Cost_Amount='$cost',Mod_Date=NOW() WHERE Product_Product_ID=$productid;";
if($SingleProID > 0 ) {$prosql .= "UPDATE group_po_line SET Supplier_Supplier_ID='$thesupplier',Cost_Amount='$SingleSuppCost',Mod_Date=NOW() WHERE Product_Product_ID=$SingleProID;";}
//$prosql .= "UPDATE group_po_line SET Supplier_Supplier_ID='$thesupplier',Cost_Amount='$cost',Mod_Date=NOW() WHERE Product_Product_ID=$productid AND Line_Type='Air Upgrade';";
//$prosql .= "UPDATE group_po_line SET Supplier_Supplier_ID='$thesupplier',Cost_Amount='$cost',Mod_Date=NOW() WHERE Product_Product_ID=$productid AND Line_Type='Land Upgrade';";

$prosql .= "UPDATE sales_order_line SET Supplier_Supplier_ID='$thesupplier',Sales_Amount='$price',Mod_Date=NOW() WHERE Product_Product_ID=$productid;";
if($SingleProID > 0 ) {$prosql .= "UPDATE sales_order_line SET Supplier_Supplier_ID='$thesupplier',Sales_Amount='$SingleSuppPrice',Mod_Date=NOW() WHERE Product_Product_ID=$SingleProID;";}
if($AirlineCreditProductID > 0 ) {$prosql .= "UPDATE sales_order_line SET Supplier_Supplier_ID='$thesupplier',Sales_Amount='-$Airline_Credit_Amount',Mod_Date=NOW() WHERE Product_Product_ID=$AirlineCreditProductID;";}
//$prosql .= "UPDATE sales_order_line SET Supplier_Supplier_ID='$thesupplier',Sales_Amount='$price',Mod_Date=NOW() WHERE Product_Product_ID=$productid AND Line_Type='Air Upgrade';";
//$prosql .= "UPDATE sales_order_line SET Supplier_Supplier_ID='$thesupplier',Sales_Amount='$price',Mod_Date=NOW() WHERE Product_Product_ID=$productid AND Line_Type='Land Upgrade';";

$prosql .= "UPDATE customer_invoice_line SET Invoice_Line_Total='$price',Mod_Date=NOW() WHERE Product_Product_ID=$productid;";
if($SingleProID > 0 ) {$prosql .= "UPDATE customer_invoice_line SET Invoice_Line_Total='$SingleSuppPrice',Mod_Date=NOW() WHERE Product_Product_ID=$SingleProID;";}
if($AirlineCreditProductID > 0 ) {$prosql .= "UPDATE customer_invoice_line SET Invoice_Line_Total='-$Airline_Credit_Amount',Mod_Date=NOW() WHERE Product_Product_ID=$AirlineCreditProductID;";}
//$prosql .= "UPDATE customer_invoice_line SET Invoice_Line_Total='$price',Mod_Date=NOW() WHERE Product_Product_ID=$productid AND Line_Type=5;";
//$prosql .= "UPDATE customer_invoice_line SET Invoice_Line_Total='$price',Mod_Date=NOW() WHERE Product_Product_ID=$productid AND Line_Type=6;";

$prosql .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','product','$productid','edited','$name');";
if ($procon->multi_query($prosql) === TRUE) {
    //Nithin : Reflect price change in customer invoices. {
    (new Products())->raisePriceChangedEvent($productid) ;
    //}
	echo "<h3>Success</h3> The product has been updated successfully!<br />The page will be refreshed in couple of seconds.";
	echo "<meta http-equiv='refresh' content='2;products-edit.php?id=$productid&action=edit'>";
} else {
echo $prosql . "<br>" . $procon->error."<br> error";
}

$procon->close();
}


//Create new products from the products-add.php
if(isset($_POST['productname'])){

$name = mysqli_real_escape_string($db, $_POST["productname"]);
$description = mysqli_real_escape_string($db, $_POST["description"]);
$cost = mysqli_real_escape_string($db, $_POST["cost"]);
$price = mysqli_real_escape_string($db, $_POST["price"]);
$paymentterm = mysqli_real_escape_string($db, $_POST["paymentterm"]);
$subproduct = mysqli_real_escape_string($db, $_POST["subproduct"]);
if($subproduct == 0) {
	$parentid = $producttype = $meta1 = $meta2 = $meta3 = $meta4 = 0;
} else {
	$parentid = mysqli_real_escape_string($db, $_POST["parentid"]);
	$producttype = mysqli_real_escape_string($db, $_POST["producttype"]);
	if($producttype == "Airfare"){
		$meta1 = mysqli_real_escape_string($db, $_POST["departurecity"]);
		$meta2 = mysqli_real_escape_string($db, $_POST["departurestate"]);
		$meta3 = NULL;
		$meta4 = NULL;
	} elseif ($producttype == "Hotels"){
		$meta1 = mysqli_real_escape_string($db, $_POST["hotelcity"]);
		$meta2 = mysqli_real_escape_string($db, $_POST["checkindate"]);
		$meta3 = mysqli_real_escape_string($db, $_POST["checkoutdate"]);
		$meta4 = NULL;
	} elseif ($producttype == "Guides"){
		$meta1 = mysqli_real_escape_string($db, $_POST["GuidePhone"]);
		$meta2 = mysqli_real_escape_string($db, $_POST["GuideCity"]);
		$meta3 = NULL;
		$meta4 = NULL;
	} else {
		$meta1 = NULL;
		$meta2 = NULL;
		$meta3 = NULL;
		$meta4 = NULL;
	}
}
$status = mysqli_real_escape_string($db, $_POST["status"]);
$thesupplier = mysqli_real_escape_string($db, $_POST['thesupplier']);
$thecategory = mysqli_real_escape_string($db, $_POST['thecategory']);

$sql = "INSERT INTO products (name,description,supplierid,categoryid,subproduct,parentid,producttype,cost,price,paymentterm,meta1,meta2,meta3,meta4,status)
VALUES ('$name','$description','$thesupplier','$thecategory','$subproduct','$parentid','$producttype','$cost','$price','$paymentterm','$meta1','$meta2','$meta3','$meta4','$status')";

if ($db->query($sql) === TRUE) {
	GUtils::setSuccess("The product has been created successfully!");

    if ($_POST['submitBtn'] == 'save_close') {
        GUtils::redirect('products.php');
    } else {
        GUtils::redirect('products-add.php');
    }
	//echo "<h3>Success</h3> The product has been created successfully!";
} 
else {
	//echo $sql . "<br>" . $db->error."<br> error";
}

$db->close();
}


//Update the data of the dcoument attachment
if(isset($_POST['documentid'])){
	$fileid = mysqli_real_escape_string($db, $_POST["documentid"]);
	$mediatype = mysqli_real_escape_string($db, $_POST["attachmenttype"]);
	$medianame = mysqli_real_escape_string($db, $_POST["documentname"]);
	$mediadesc = mysqli_real_escape_string($db, $_POST["description"]);

	$con11 = new mysqli($servername, $username, $password, $dbname);
	if ($con11->connect_error) {die("Connection failed: " . $con11->connect_error);} 
	$sql11 = "UPDATE groups_media SET mediatype='$mediatype', name='$medianame', mediadescription='$mediadesc', updatetime=NOW() WHERE id=$fileid;";
	$sql11 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','attachment','$fileid','edited','$medianame');";
if ($con11->multi_query($sql11) === TRUE) {
	echo "<h3>Success</h3> The attachment information has been updated successfully!";
} else {
	echo $sql11 . "<br>" . $con11->error;
}
$con11->close();
}


//Add a current customer directly from the group page to that group
if(isset($_POST['CurrentCustomerTourID'])){
	$Warning = NULL;
	$contactid = mysqli_real_escape_string($db, $_POST["CurrentCustomerSelected"]);
	$tourid = mysqli_real_escape_string($db, $_POST["CurrentCustomerTourID"]);
	$CurrentUserID = mysqli_real_escape_string($db, $_POST["CurrentUserID"]);
	date_default_timezone_set('America/Chicago');
	$NowDate = date("Y-m-d");
	$Due_Date = mysqli_real_escape_string($db, $_POST["TourDueDate"]);
		
		if($contactid > 0) {
			$conn = new mysqli($servername, $username, $password, $dbname);
			if ($conn->connect_error) {die("Connection failed: " . $conn->connect_error);} 
			$sql = "SELECT fname,lname,usertype FROM contacts WHERE id=$contactid";
			$result = $conn->query($sql);
			$data = $result->fetch_assoc();
			$conn->close();
		} else {
			$Warning = "There is no contact selected yet!";
		}

		$conn2 = new mysqli($servername, $username, $password, $dbname);
		if ($conn2->connect_error) {die("Connection failed: " . $conn2->connect_error);} 
		$sql2 = "SELECT (SELECT price FROM products WHERE id=airline_productid) AS Airline,(SELECT price FROM products WHERE id=land_productid) AS Land FROM groups WHERE tourid=$tourid";
		$result2 = $conn2->query($sql2);
		$groupdata = $result2->fetch_assoc();
		$conn2->close();
		$Group_price = $groupdata["Airline"] + $groupdata["Land"];
	
	$fullname = $data["fname"]." ".$data["lname"];
	if($data["usertype"] == 0){$usertypeupdate = "4";}
	else{$usertypeupdate = $data["usertype"].",4";}

	
	$con12 = new mysqli($servername, $username, $password, $dbname);
	if ($con12->connect_error) {die("Connection failed: " . $con12->connect_error);} 

	$sql12 = "INSERT INTO sales_order (Group_ID,Customer_Account_Customer_ID,Sales_Order_Date,Sales_Order_Stop,Total_Sale_Price) VALUES ('$tourid','$contactid','$NowDate',0,'$Group_price');";
	$sql12 .= "INSERT INTO customer_account (contactid,status,tourid,Added_By) VALUES ('$contactid','1','$tourid','$CurrentUserID');";
	$sql12 .= "UPDATE contacts SET usertype='$usertypeupdate', updatetime=NOW() WHERE id=$contactid;";
	$sql12 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','customer account','$contactid','added','$fullname to the group # T$tourid');";
	if($Warning == NULL) {
		if ($con12->multi_query($sql12) === TRUE) {
			//To collect the sales order number for this customer
			$SalesOrderNumID = mysqli_insert_id($con12);
			
			//To create a Customer_Invoice for this customer 
			$CusInv_New = "INSERT INTO customer_invoice (Customer_Invoice_Date,Customer_Order_Num,Sales_Order_Reference,Customer_Account_Customer_ID,Group_ID,Invoice_Amount,Due_Date)
						VALUES ('$NowDate','$SalesOrderNumID','$SalesOrderNumID','$contactid','$tourid','$Group_price','$Due_Date');";
			$db->query($CusInv_New);
			$CusInv_id = $db->insert_id;

			//To collect the information of the Airline Product for this group
			$GetAirProductSQL = "SELECT pro.id,pro.name,pro.supplierid,pro.price
			FROM products pro 
			JOIN groups gro
			ON gro.airline_productid=pro.id
			WHERE gro.tourid=$tourid";
			$GetAirProductResult = $db->query($GetAirProductSQL);
			$GetAirProductCount = $GetAirProductResult->num_rows;
			if($GetAirProductCount > 0) {
				$GetAirProductData = $GetAirProductResult->fetch_assoc();
					$AirProductID = $GetAirProductData['id'];
					$AirProductName = $GetAirProductData['name'];
					$AirProductSupplier = $GetAirProductData['supplierid'];
					$AirProductPrice = $GetAirProductData['price'];
			}

			//To collect the information of the Land Product for this group
			$GetLandProductSQL = "SELECT pro.id,pro.name,pro.supplierid,pro.price
			FROM products pro 
			JOIN groups gro
			ON gro.land_productid=pro.id
			WHERE gro.tourid=$tourid";
			$GetLandProductResult = $db->query($GetLandProductSQL);
			$GetLandProductCount = $GetLandProductResult->num_rows;
			if($GetLandProductCount > 0) {
				$GetLandProductData = $GetLandProductResult->fetch_assoc();
					$LandProductID = $GetLandProductData['id'];
					$LandProductName = $GetLandProductData['name'];
					$LandProductSupplier = $GetLandProductData['supplierid'];
					$LandProductPrice = $GetLandProductData['price'];
			}
			
			//To fill the sales order line insert query for AIR and LAND
			$SaleOrderLineQuerySQL = "";
			
			//For Airline product and line
			if($GetAirProductCount > 0) {
				$SaleOrderLineQuerySQL .= "INSERT INTO sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
				VALUES ('$SalesOrderNumID','Airline Package','$AirProductID','$AirProductName','$AirProductPrice','$AirProductSupplier');";
				$SaleOrderLineQuerySQL .= "INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
				VALUES ('$CusInv_id','1','1','$AirProductID','$AirProductName','1','$AirProductPrice');";
			}
			//For Land product and line
			if($GetLandProductCount > 0) {
				$SaleOrderLineQuerySQL .= "INSERT INTO sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
				VALUES ('$SalesOrderNumID','Land Package','$LandProductID','$LandProductName','$LandProductPrice','$LandProductSupplier');";
				$SaleOrderLineQuerySQL .= "INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
				VALUES ('$CusInv_id','2','2','$LandProductID','$LandProductName','1','$LandProductPrice');";
			}
			
			if ($db->multi_query($SaleOrderLineQuerySQL) === TRUE) {
				echo "<div class='upload-loader-small'></div> Adding the contact $fullname to this group ...";
				echo "<meta http-equiv='refresh' content='2;url=groups-edit.php?id=$tourid&action=edit&tab=participants'>";
			} else {
				echo $SaleOrderLineQuerySQL . "<br>" . $db->error;
			}
		//	echo "<div class='upload-loader-small'></div> Adding the contact $fullname to this group ...";
		//	echo "<meta http-equiv='refresh' content='2;url=groups-edit.php?id=$tourid&action=edit&tab=participants'>";
		} else {
			echo $sql12 . "<br>" . $con12->error;
		}
	} else {
		echo $Warning;
	}
$con12->close();
}
		
		
//Create a new contact and customer account for the current group itself in the same page of the group edit
if(isset($_POST['NewCustomerAdd'])){

	//These are the fields of the contact information
	$CurrentUserID = mysqli_real_escape_string($db, $_POST["CurrentUserID"]);
	$tourid = mysqli_real_escape_string($db, $_POST["NewCustomerAdd"]);
	$title = mysqli_real_escape_string($db, $_POST["title"]);
	$fname = mysqli_real_escape_string($db, $_POST["FirstName"]);
	$mname = mysqli_real_escape_string($db, $_POST["MiddleName"]);
	$lname = mysqli_real_escape_string($db, $_POST["LastName"]);
	$email = mysqli_real_escape_string($db, $_POST["email"]);
	$phone = mysqli_real_escape_string($db, $_POST["homephone"]);
	$businessphone = mysqli_real_escape_string($db, $_POST["businessphone"]);
	$fax = mysqli_real_escape_string($db, $_POST["fax"]);
	$mobile = mysqli_real_escape_string($db, $_POST["mobile"]);
	$company = mysqli_real_escape_string($db, $_POST["company"]);
	$jobtitle = mysqli_real_escape_string($db, $_POST["jobtitle"]);
	$website = mysqli_real_escape_string($db, $_POST["website"]);
	$address1 = mysqli_real_escape_string($db, $_POST["address1"]);
	$address2 = mysqli_real_escape_string($db, $_POST["address2"]);
	$zipcode = mysqli_real_escape_string($db, $_POST["zipcode"]);
	$city = mysqli_real_escape_string($db, $_POST["city"]);
	$state = mysqli_real_escape_string($db, $_POST["state"]);
	$country = mysqli_real_escape_string($db, $_POST["country"]);
	if ($_POST["WorkAddressButton"]==1){$Work_enabled = "1";} else {$Work_enabled = "0";}
	if ($Work_enabled == 1) {
		$Work_address1 = mysqli_real_escape_string($db, $_POST["Work_address1"]);
		$Work_address2 = mysqli_real_escape_string($db, $_POST["Work_address2"]);
		$Work_zipcode = mysqli_real_escape_string($db, $_POST["Work_zipcode"]);
		$Work_city = mysqli_real_escape_string($db, $_POST["Work_city"]);
		$Work_state = mysqli_real_escape_string($db, $_POST["Work_state"]);
		$Work_country = mysqli_real_escape_string($db, $_POST["Work_country"]);
	} else {
		$Work_address1 = $Work_address2 = $Work_zipcode = $Work_city = $Work_state = $Work_country = NULL;
	}
	$language = mysqli_real_escape_string($db, $_POST["language"]);
	if($_POST['newaffliationtype'] == null){
		$affiliation = $_POST["affiliation"];
	} else {
		$affiliation = mysqli_real_escape_string($db, $_POST["newaffliationtype"]);
	}
	$preferredmethod = $_POST["preferredmethod"];
	//These are the fields of the customer account information
	$passport = mysqli_real_escape_string($db, $_POST["passport"]);
	$expirydate = mysqli_real_escape_string($db, $_POST["expirydate"]);
	$citizenship = mysqli_real_escape_string($db, $_POST["citizenship"]);
	$badgename = mysqli_real_escape_string($db, $_POST["badgename"]);
	$birthday = mysqli_real_escape_string($db, $_POST["birthday"]);
	$martialstatus = mysqli_real_escape_string($db, $_POST["martialstatus"]);
	$gender = mysqli_real_escape_string($db, $_POST["gender"]);
	$payment = mysqli_real_escape_string($db, $_POST["paymentterm"]);
	$roomtype = mysqli_real_escape_string($db, $_POST["roomtype"]);
	if($roomtype == 'Single') {
		$travelingwith = 0;
	} else {
		$travelingwith = mysqli_real_escape_string($db, $_POST["travelingwith"]);
	}
	if($roomtype != 'Triple') {
		$travelingwith2 = 0;
	} else {
		$travelingwith2 = mysqli_real_escape_string($db, $_POST["travelingwith2"]);
	}
	$travelerid = mysqli_real_escape_string($db, $_POST["travelerid"]);
	$flyer = mysqli_real_escape_string($db, $_POST["flyer"]);
	$emergencyname = mysqli_real_escape_string($db, $_POST["emergencyname"]);
	$emergencyrelation = mysqli_real_escape_string($db, $_POST["emergencyrelation"]);
	$emergencyphone = mysqli_real_escape_string($db, $_POST["emergencyphone"]);
	$emergency2name = mysqli_real_escape_string($db, $_POST["emergency2name"]);
	$emergency2relation = mysqli_real_escape_string($db, $_POST["emergency2relation"]);
	$emergency2phone = mysqli_real_escape_string($db, $_POST["emergency2phone"]);
	$missinginfo = mysqli_real_escape_string($db, $_POST["missinginfo"]);
	//This is to get the now date for the sales order
	date_default_timezone_set('America/Chicago');
	$NowDate = date("Y-m-d");
	
	//I want to get the price of the group ticket to add it in the sales order
	$conn2 = new mysqli($servername, $username, $password, $dbname);
	if ($conn2->connect_error) {die("Connection failed: " . $conn2->connect_error);} 
	$sql2 = "SELECT listprice FROM groups WHERE tourid=$tourid";
	$result2 = $conn2->query($sql2);
	$groupdata = $result2->fetch_assoc();
	$conn2->close();
	$Group_price = $groupdata["listprice"];

	if (!empty($fname)) {
	$sqlcheck = "SELECT * FROM contacts WHERE(fname = '$fname' AND mname = '$mname' AND lname = '$lname')";
	$result = mysqli_query($db,$sqlcheck);
	$row = mysqli_fetch_array($result,MYSQLI_ASSOC);
	$count = mysqli_num_rows($result);
	if($count == 1) {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error: This contact name is already recorded on the system!</strong></div>";
	}else {
		$sql = "INSERT INTO contacts (usertype,title, fname, mname, lname, email, phone,businessphone, fax, mobile, company, jobtitle, website, address1, address2, zipcode, city, state, country, Work_enabled, Work_address1, Work_address2, Work_zipcode, Work_city, Work_state, Work_country, language, affiliation, preferredmethod,passport,expirydate,citizenship,badgename,birthday,martialstatus,gender,emergencyname,emergencyrelation,emergencyphone,emergency2name,emergency2relation,emergency2phone,payment,travelerid,flyer,missinginfo)
		VALUES ('4','$title','$fname','$mname','$lname','$email','$phone','$businessphone','$fax','$mobile','$company','$jobtitle','$website','$address1','$address2','$zipcode','$city','$state','$country','$Work_enabled','$Work_address1','$Work_address2','$Work_zipcode','$Work_city','$Work_state','$Work_country','$language','$affiliation','$preferredmethod','$passport','$expirydate','$citizenship','$badgename','$birthday','$martialstatus','$gender','$emergencyname','$emergencyrelation','$emergencyphone','$emergency2name','$emergency2relation','$emergency2phone','$payment','$travelerid','$flyer','$missinginfo');";
		$sql .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','contact','0','created','$fname $mname $lname');";
		if($_POST['newaffliationtype'] != null){
			$meta = $_POST['newaffliationtype'];
		$sql .= "INSERT INTO metadata (catid, catname, meta) VALUES ('1','Contact Category','$meta');";
		}
		if ($db->multi_query($sql) === TRUE) {

			$contactnewID = mysqli_insert_id($db);
			$addcontactconn = new mysqli($servername, $username, $password, $dbname);
			if ($addcontactconn->connect_error) {die("Connection failed: " . $addcontactconn->connect_error);} 
			$AddContactSQL = "INSERT INTO sales_order (Group_ID,Customer_Account_Customer_ID,Sales_Order_Date,Sales_Order_Stop,Total_Sale_Price)
			VALUES ('$tourid','$contactnewID','$NowDate',0,'$Group_price');";
			$AddContactSQL .= "INSERT INTO customer_account (contactid,tourid,status,roomtype,travelingwith,travelingwith2,Added_By)
			VALUES ('$contactnewID','$tourid','1','$roomtype','$travelingwith','$travelingwith2','$CurrentUserID');";
			//$AddContactSQL .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','tour','$tourid','updated','$TourName');";
			
			if ($addcontactconn->multi_query($AddContactSQL) === TRUE) {
//xxxxxxx
				//To collect the sales order number for this customer
				$SalesOrderNumID = mysqli_insert_id($addcontactconn);

				//To create a Customer_Invoice for this customer 
				$InvoiceConnDB = new mysqli($servername, $username, $password, $dbname);
				if ($InvoiceConnDB->connect_error) {die("Connection failed: " . $InvoiceConnDB->connect_error);} 
				$CusInv_New = "INSERT INTO customer_invoice (Customer_Invoice_Date,Customer_Order_Num,Sales_Order_Reference,Customer_Account_Customer_ID,Group_ID,Invoice_Amount)
							VALUES ('$NowDate','$SalesOrderNumID','$SalesOrderNumID','$contactnewID','$tourid','$Group_price');";
				$InvoiceConnDB->query($CusInv_New);
				$CusInv_id = $InvoiceConnDB->insert_id;
				$InvoiceConnDB->close();

				//To collect the information of the Airline Product for this group
				$NewContactWithSalesLinesDB = new mysqli($servername, $username, $password, $dbname);
				if ($NewContactWithSalesLinesDB->connect_error) {die("Connection failed: " . $NewContactWithSalesLinesDB->connect_error);} 
				$GetAirProductSQL = "SELECT pro.id,pro.name,pro.supplierid,pro.price
				FROM products pro 
				JOIN groups gro
				ON gro.airline_productid=pro.id
				WHERE gro.tourid=$tourid";
				$GetAirProductResult = $NewContactWithSalesLinesDB->query($GetAirProductSQL);
				$GetAirProductCount = $GetAirProductResult->num_rows;
				if($GetAirProductCount > 0) {
					$GetAirProductData = $GetAirProductResult->fetch_assoc();
						$AirProductID = $GetAirProductData['id'];
						$AirProductName = $GetAirProductData['name'];
						$AirProductSupplier = $GetAirProductData['supplierid'];
						$AirProductPrice = $GetAirProductData['price'];
				}

				//To collect the information of the Land Product for this group
				$GetLandProductSQL = "SELECT pro.id,pro.name,pro.supplierid,pro.price
				FROM products pro 
				JOIN groups gro
				ON gro.land_productid=pro.id
				WHERE gro.tourid=$tourid";
				$GetLandProductResult = $NewContactWithSalesLinesDB->query($GetLandProductSQL);
				$GetLandProductCount = $GetLandProductResult->num_rows;
				if($GetLandProductCount > 0) {
					$GetLandProductData = $GetLandProductResult->fetch_assoc();
						$LandProductID = $GetLandProductData['id'];
						$LandProductName = $GetLandProductData['name'];
						$LandProductSupplier = $GetLandProductData['supplierid'];
						$LandProductPrice = $GetLandProductData['price'];
				}
				
				//To fill the sales order line insert query for AIR and LAND
				$SaleOrderLineQuerySQL = "";
				
				//For Airline product and line
				if($GetAirProductCount > 0) {
					$SaleOrderLineQuerySQL .= "INSERT INTO sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
					VALUES ('$SalesOrderNumID','Airline Package','$AirProductID','$AirProductName','$AirProductPrice','$AirProductSupplier');";
					$SaleOrderLineQuerySQL .= "INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
					VALUES ('$CusInv_id','1','1','$AirProductID','$AirProductName','1','$AirProductPrice');";
				}
				//For Land product and line
				if($GetLandProductCount > 0) {
					$SaleOrderLineQuerySQL .= "INSERT INTO sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
					VALUES ('$SalesOrderNumID','Land Package','$LandProductID','$LandProductName','$LandProductPrice','$LandProductSupplier');";
					$SaleOrderLineQuerySQL .= "INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
					VALUES ('$CusInv_id','2','2','$LandProductID','$LandProductName','1','$LandProductPrice');";
				}
				
				if ($NewContactWithSalesLinesDB->multi_query($SaleOrderLineQuerySQL) === TRUE) {
					//echo $SaleOrderLineQuerySQL;
					//echo "<div class='upload-loader-small'></div> Adding the contact $fullname to this group ...";
					//echo "<meta http-equiv='refresh' content='2;url=groups-edit.php?id=$tourid&action=edit&tab=participants'>";
				} else {
					echo $SaleOrderLineQuerySQL . "<br>" . $NewContactWithSalesLinesDB->error;
				}
				$NewContactWithSalesLinesDB->close();
//xxxxxxx
				//echo "<h3>Success</h3> You have successfully added the Airline product to this group!";
			} else {
				echo $AddContactSQL . "<br>" . $addcontactconn->error;
			}
			$addcontactconn->close();

			echo "<div class='upload-loader-small'></div> Creating and adding the contact $fname $lname to this group ...";
			echo "<meta http-equiv='refresh' content='2;url=groups-edit.php?id=$tourid&action=edit&tab=participants'>";
		} else {
			echo $sql . "<br>" . $db->error."<br> error";
		}
	}
	$db->close();
	}
}


//Add a new payment for the Group
if(isset($_POST['GeneralPaymentAmount'])){
	$TheGroupID = mysqli_real_escape_string($db, $_POST["GeneralPaymentGroupID"]);
	$GeneralPaymentDate = mysqli_real_escape_string($db, $_POST["GeneralPaymentDate"]);
	$GeneralPaymentAmount = mysqli_real_escape_string($db, $_POST["GeneralPaymentAmount"]);
	$GeneralPaymentComments = mysqli_real_escape_string($db, $_POST["GeneralPaymentComments"]);

	$con14 = new mysqli($servername, $username, $password, $dbname);
	if ($con14->connect_error) {die("Connection failed: " . $con14->connect_error);} 
	$sql14 = "INSERT INTO groups_payments (Group_Payment_Date,Group_Payment_Group_ID, Group_Payment_Amount, Group_Payment_Comments)
				VALUES ('$GeneralPaymentDate','$TheGroupID','$GeneralPaymentAmount','$GeneralPaymentComments');";
	$sql14 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','payment for','$GeneralPaymentComments','added','$GeneralPaymentComments (Group #$TheGroupID)');";
if ($con14->multi_query($sql14) === TRUE) {
	echo "<h3>Success</h3> The payment has been added successfully!<br />The page will be refreshed in seconds.";
	echo "<meta http-equiv='refresh' content='2;groups-edit.php?id=$TheGroupID&action=edit&tab=expenses'>";
} else {
	echo $sql14 . "<br>" . $con14->error;
}
$con14->close();
}


//Add a new Airline Supplier payment for the Group
/*if(isset($_POST['AirSupplierPaymentGroupID'])){
	$TheGroupID = mysqli_real_escape_string($db, $_POST["AirSupplierPaymentGroupID"]);
	$AirSuppID = mysqli_real_escape_string($db, $_POST["AirSupplierPaymentSuppID"]);
	$AirSuppPaymentDate = mysqli_real_escape_string($db, $_POST["AirSupplierPaymentDate"]);
	$AirSuppPaymentAmount = mysqli_real_escape_string($db, $_POST["AirSupplierPaymentAmount"]);
	$AirSuppPaymentMethod = mysqli_real_escape_string($db, $_POST["AirSupplierPaymentAmount"]);
	$AirSuppPaymentComments = mysqli_real_escape_string($db, $_POST["AirSupplierPaymentComments"]);

	$xcon15 = new mysqli($servername, $username, $password, $dbname);
	if ($xcon15->connect_error) {die("Connection failed: " . $xcon15->connect_error);} 
	$xsql15 = "INSERT INTO supplier_payments (Supplier_Payment_Date,Supplier_Payment_Method,Supplier_Payment_Amount,Supplier_Supplier_Comments,Supplier_Supplier_ID,Group_Group_ID)
				VALUES ('$AirSuppPaymentDate','$AirSuppPaymentMethod','$AirSuppPaymentAmount','$AirSuppPaymentComments','$AirSuppID','$TheGroupID');";
	$xsql15 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','Airline Supplier payment for','$AirSuppPaymentComments','added','Group #$TheGroupID ($AirSuppPaymentComments)');";
if ($xcon15->multi_query($xsql15) === TRUE) {
	echo "<h3>Success</h3> The payment has been added successfully!<br />The page will be refreshed in seconds.";
	echo "<meta http-equiv='refresh' content='2;groups-edit.php?id=$TheGroupID&action=edit&tab=expenses'>";
} else {
	echo $xsql15 . "<br>" . $xcon15->error;
}
$xcon15->close();
}*/


//Add a new Land package Supplier payment for the Group
/*if(isset($_POST['LandSupplierPaymentGroupID'])){
	$TheGroupID = mysqli_real_escape_string($db, $_POST["LandSupplierPaymentGroupID"]);
	$LandSuppID = mysqli_real_escape_string($db, $_POST["LandSupplierPaymentSuppID"]);
	$LandSuppPaymentDate = mysqli_real_escape_string($db, $_POST["LandSupplierPaymentDate"]);
	$LandSuppPaymentAmount = mysqli_real_escape_string($db, $_POST["LandSupplierPaymentAmount"]);
	$LandSuppPaymentMethod = mysqli_real_escape_string($db, $_POST["LandSupplierPaymentAmount"]);
	$LandSuppPaymentComments = mysqli_real_escape_string($db, $_POST["LandSupplierPaymentComments"]);

	$xcon16 = new mysqli($servername, $username, $password, $dbname);
	if ($xcon16->connect_error) {die("Connection failed: " . $xcon16->connect_error);} 
	$xsql16 = "INSERT INTO supplier_payments (Supplier_Payment_Date,Supplier_Payment_Method,Supplier_Payment_Amount,Supplier_Supplier_Comments,Supplier_Supplier_ID,Group_Group_ID)
				VALUES ('$LandSuppPaymentDate','$LandSuppPaymentMethod','$LandSuppPaymentAmount','$LandSuppPaymentComments','$LandSuppID','$TheGroupID');";
	$xsql16 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','Land Supplier payment for','$LandSuppPaymentComments','added','Group #$TheGroupID ($LandSuppPaymentComments)');";
if ($xcon16->multi_query($xsql16) === TRUE) {
	echo "<h3>Success</h3> The payment has been added successfully!<br />The page will be refreshed in seconds.";
	echo "<meta http-equiv='refresh' content='2;groups-edit.php?id=$TheGroupID&action=edit&tab=expenses'>";
} else {
	echo $xsql16 . "<br>" . $xcon16->error;
}
$xcon16->close();
}*/


//Refresh and Update the Customers in a group
if(isset($_POST['GroupID'])){
	//Get the main fields from the form in the Pax tab in Groups Edit page
	$GroupID = mysqli_real_escape_string($db, $_POST["GroupID"]);
	$AirlineProID = mysqli_real_escape_string($db, $_POST["AirlineProID"]);
	$LandProID = mysqli_real_escape_string($db, $_POST["LandProID"]);
	$AirProductID = mysqli_real_escape_string($db, $_POST["AirProductID"]);
	$AirTicketProductID = mysqli_real_escape_string($db, $_POST["AirTicketProductID"]);
	$LandProductID = mysqli_real_escape_string($db, $_POST["LandProductID"]);
	$SingleCurrentProductID = mysqli_real_escape_string($db, $_POST["SingleProductID"]);
	$GroupDueDate = mysqli_real_escape_string($db, $_POST["GroupDueDate"]);
	
	//Counters to give a fast stats at the end
	$NumberOfSalesOrdersCreated = 0;
	$NumberOfSalesOrdersUpdated = 0;
	$NumberOfInvoicesCreated = 0;
	$NumberOfInvoicesUpdated = 0;
	$NumberOfAirlineCreated = 0;
	$NumberOfAirlineUpdated = 0;
	$NumberOfLandCreated = 0;
	$NumberOfLandUpdated = 0;
	$NumberOfJointInvoiceUpdated = 0;
	$NumberOfTicketLinesUpdated = 0;
	$NumberOfTicketLinesCreated = 0;
	$NumberOfAirProLinesUpdated = 0;
	$NumberOfAirProLinesCreated = 0;
	$NumberOfLandProLinesUpdated = 0;
	$NumberOfLandProLinesCreated = 0;
	$NumberOfSingleLinesUpdated = 0;
	$NumberOfSingleLinesCreated = 0;
	$NumberOfAirUpLinesUpdated = 0;
	$NumberOfAirUpLinesCreated = 0;
	$NumberOfLandUpLinesUpdated = 0;
	$NumberOfLandUpLinesCreated = 0;
	$NumberOfTransferLinesUpdated = 0;
	$NumberOfTransferLinesCreated = 0;
	$NumberOfExtensionLinesUpdated = 0;
	$NumberOfExtensionLinesCreated = 0;
	$NumberOfInsuranceLinesUpdated = 0;
	$NumberOfInsuranceLinesCreated = 0;
	
	//Before we start making checks and queries, we should see if this group has a Purchase Order
	$CheckPOsql = "SELECT Group_PO_ID FROM group_purchase_order WHERE Group_Group_ID=$GroupID";
	$CheckPOresult = $db->query($CheckPOsql);
	$CheckPOcount = $CheckPOresult->num_rows;
	if($CheckPOcount == 1) { //Check if there is a Purchase Order, and if the group has one, update it
		$POdata = $CheckPOresult->fetch_assoc();
		$PurchaseOrderNum = $POdata['Group_PO_ID'];
		
		/*$UpdateSOsql = "UPDATE sales_order SET Mod_Date=NOW() WHERE Sales_Order_Num=$SalesOrderNum;";
		if ($db->multi_query($UpdateSOsql) === TRUE) {
			$NumberOfSalesOrdersUpdated++;
		} else {
			echo "Failed to update a Sales Order.<pre>".$UpdateSOsql."</pre>";
		}*/
	} else { //If the group doesn't have a Purchase Order, Create one
		$AddPOsql = "INSERT INTO group_purchase_order (Group_Group_ID,PO_Date,Group_PO_Stop,Total_Order_Amount) VALUES ('$GroupID',NOW(),'0','0')";
		if ($db->multi_query($AddPOsql) === TRUE) {
			$PurchaseOrderNum = $db->insert_id;
		} else {
			echo "Failed to create a purchase order.<pre>".$AddPOsql."</pre>";
		}
	}

	//Let's get the list of customers in this group
	$CustomersListSQL = "SELECT contactid,hisownticket,upgrade,upgradeproduct,upgrade_land_product,Insurance_Product_ID,Transfer_Product_ID,Extension_Product_ID,roomtype,groupinvoice FROM customer_account WHERE tourid=$GroupID AND status=1";
	$CustomersListResult = $db->query($CustomersListSQL);
	

	while($CustomersListData = $CustomersListResult->fetch_assoc()){
		//We are now checking customers, one by one, to get all the data needed
		$CustomerID = $CustomersListData['contactid'];
		$groupinvoice = $CustomersListData['groupinvoice'];
		
		//First, check if this customer has a **Sales_order entry** and create Sales_Order if not exist
		$CheckSOsql = "SELECT Sales_Order_Num FROM sales_order WHERE Group_ID=$GroupID AND Customer_Account_Customer_ID=$CustomerID";
		$CheckSOresult = $db->query($CheckSOsql);
		$CheckSOcount = $CheckSOresult->num_rows;
		if($CheckSOcount == 1) { //Check if there is a Sales Order, and if he has one, update it
			$SOdata = $CheckSOresult->fetch_assoc();
			$SalesOrderNum = $SOdata['Sales_Order_Num'];
			
			$UpdateSOsql = "UPDATE sales_order SET Mod_Date=NOW() WHERE Sales_Order_Num=$SalesOrderNum;";
			//$UpdateSOsql .= "DELETE FROM sales_order_line WHERE Sales_Order_Num=$SalesOrderNum;";
			if ($db->multi_query($UpdateSOsql) === TRUE) {
				$NumberOfSalesOrdersUpdated++;
				
				//Here i removed all the current lines in the sales order for cleaner records
				$sql = "DELETE FROM sales_order_line WHERE Sales_Order_Num=$SalesOrderNum;" ;
				GDb::execute($sql);
			} else {
				echo "Failed to update a Sales Order.<pre>".$UpdateSOsql."</pre>";
			}
		} else { //If he doesn't have a Sales Order, Create one
			$AddSOsql = "INSERT INTO sales_order (Group_ID, Customer_Account_Customer_ID, Sales_Order_Date) VALUES ('$GroupID','$CustomerID',NOW());";
			if ($db->multi_query($AddSOsql) === TRUE) {
				$SalesOrderNum = $db->insert_id;
				$NumberOfSalesOrdersCreated++;
			} else {
				echo "Failed to create a sales order.<pre>".$AddSOsql."</pre>";
			}
		}
		
		//Second, check if this customer has a **Customer_invoice entry** and create Customer_invoice if not exist
		//$CheckCIsql = "SELECT Customer_Invoice_Num FROM customer_invoice WHERE Customer_Order_Num=$SalesOrderNum";
		$CheckCIsql = "SELECT Customer_Invoice_Num FROM customer_invoice WHERE Group_ID=$GroupID AND Customer_Account_Customer_ID=$CustomerID;";
		$CheckCIresult = $db->query($CheckCIsql);
		$CheckCIcount = $CheckCIresult->num_rows;
		if($CheckCIcount == 1) { //Check if there is a customer invoice, and if he has one, update it
			$CIdata = $CheckCIresult->fetch_assoc();
			$CustomerInvoiceNum = $CIdata['Customer_Invoice_Num'];
			
			$UpdateCIsql = "UPDATE customer_invoice SET Customer_Order_Num='$SalesOrderNum',Sales_Order_Reference='$SalesOrderNum',Customer_Account_Customer_ID='$CustomerID',Due_Date='$GroupDueDate', Group_ID='$GroupID', Mod_Date=NOW() WHERE Customer_Invoice_Num=$CustomerInvoiceNum;";
			//$UpdateCIsql .= "DELETE FROM customer_invoice_line WHERE Customer_Invoice_Num=$CustomerInvoiceNum;";
			if ($db->multi_query($UpdateCIsql) === TRUE) {
				$NumberOfInvoicesUpdated++;
				
				//Here i removed all the current lines in the invoice for cleaner records
				$sql = "DELETE FROM customer_invoice_line WHERE Customer_Invoice_Num=$CustomerInvoiceNum;" ;
				GDb::execute($sql);
			} else {
				echo "Failed to update a customer invoice.<pre>".$UpdateCIsql."</pre>";
			}
		} else { //If he doesn't have a customer invoice, Create one
			$AddCIsql = "INSERT INTO customer_invoice (Customer_Invoice_Date,Customer_Order_Num,Due_Date,Customer_Account_Customer_ID,Group_ID,Add_Date) VALUES (NOW(),'$SalesOrderNum','$GroupDueDate','$CustomerID','$GroupID',NOW());";
			if ($db->multi_query($AddCIsql) === TRUE) {
				$CustomerInvoiceNum = $db->insert_id;
				$NumberOfInvoicesCreated++;
			} else {
				echo "Failed to create a customer invoice.<pre>".$AddCIsql."</pre>";
			}
		}
		
		//Third, Add the Airline Product to the Sales Order Line and Customer Invoice Line if it doesn't exist
		if($AirProductID > 0) { //Only let this query work if there is an Airline product
			$GetAirlineProSQL = "SELECT id,name,supplierid,price FROM products WHERE id=$AirProductID";
			$GetAirlineProResult = $db->query($GetAirlineProSQL);
			$GetAirlineProCount = $GetAirlineProResult->num_rows;
			if($GetAirlineProCount == 1) { //Only if there is a product with this ID in the products table
				$AirlineProData = $GetAirlineProResult->fetch_assoc();
				$AirlineProID = $AirlineProData['id'];
				$AirlineProName = $AirlineProData['name'];
				$AirlineProSuppID = $AirlineProData['supplierid'];
				$AirlineProPrice = $AirlineProData['price'];
				
				//Check if this customer has a **Sales Order Line** for this Airline Product or not
				$UpdateAirProSOLsql = 	"INSERT INTO  sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
											VALUES ('$SalesOrderNum','Airline Package','$AirlineProID','$AirlineProName','$AirlineProPrice','$AirlineProSuppID')
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$AirlineProID',
												Product_Name = '$AirlineProName',
												Sales_Amount = '$AirlineProPrice',
												Supplier_Supplier_ID = '$AirlineProSuppID',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateAirProSOLsql) === TRUE) {
					$NumberOfAirProLinesUpdated++;
				}
				
				//Check if this customer has a **Customer Invoice Line** for this Airline Product or not
				$UpdateAirProCILsql = 	"INSERT INTO  customer_invoice_line (Customer_Invoice_Line,Customer_Invoice_Num,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total,Line_Type,Add_Date)
											VALUES ('1','$CustomerInvoiceNum','$AirlineProID','$AirlineProName','1','$AirlineProPrice','1',NOW())
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$AirlineProID',
												Product_Name = '$AirlineProName',
												Qty = '1',
												Invoice_Line_Total = '$AirlineProPrice',
												Line_Type = '1',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateAirProCILsql) === TRUE) {
					$NumberOfAirProLinesCreated++;
				}
			}
		}
		
		//Fourth, Add the Land Product to the Sales Order Line and Customer Invoice Line if it doesn't exist
		if($LandProductID > 0) { //Only let this query work if there is a Land product
			$GetLandProSQL = "SELECT id,name,supplierid,price FROM products WHERE id=$LandProductID";
			$GetLandProResult = $db->query($GetLandProSQL);
			$GetLandProCount = $GetLandProResult->num_rows;
			if($GetLandProCount == 1) { //Only if there is a product with this ID in the products table
				$LandProData = $GetLandProResult->fetch_assoc();
				$LandProID = $LandProData['id'];
				$LandProName = $LandProData['name'];
				$LandProSuppID = $LandProData['supplierid'];
				$LandProPrice = $LandProData['price'];
				
				//Check if this customer has a Sales Order Line for this Land Product or not
				$UpdateLandProSOLsql = 	"INSERT INTO  sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
											VALUES ('$SalesOrderNum','Land Package','$LandProID','$LandProName','$LandProPrice','$LandProSuppID')
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$LandProID',
												Product_Name = '$LandProName',
												Sales_Amount = '$LandProPrice',
												Supplier_Supplier_ID = '$LandProSuppID',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateLandProSOLsql) === TRUE) {
					$NumberOfLandProLinesUpdated++;
				}
				
				//Check if this customer has a Customer Invoice Line for this Land Product or not
				$UpdateLandProCILsql = 	"INSERT INTO  customer_invoice_line (Customer_Invoice_Line,Customer_Invoice_Num,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total,Line_Type,Add_Date)
											VALUES ('2','$CustomerInvoiceNum','$LandProID','$LandProName','1','$LandProPrice','2',NOW())
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$LandProID',
												Product_Name = '$LandProName',
												Qty = '1',
												Invoice_Line_Total = '$LandProPrice',
												Line_Type = '2',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateLandProCILsql) === TRUE) {
					$NumberOfLandProLinesCreated++;
				}
			}
		}
		
		//Fifth, check if this customer has a Ticket discount (Buying his own ticket) and add it to invoice and sales order
		$BuyHisOwnTicket = $CustomersListData['hisownticket'];
		if($BuyHisOwnTicket == 1) { //Only if the customer wants a Ticket Discount
			$GetTicketSQL = "SELECT id,name,supplierid,price FROM products WHERE id=$AirTicketProductID";
			$GetTicketResult = $db->query($GetTicketSQL);
			$GetTicketCount = $GetTicketResult->num_rows;
			if($GetTicketCount == 1) { //Only if there is a product for this group
				$TicketData = $GetTicketResult->fetch_assoc();
				$TicketProductID = $TicketData['id'];
				$TicketProductName = $TicketData['name'];
				$TicketProductSuppID = $TicketData['supplierid'];
				$TicketProductCredit = $TicketData['price'];
				
				//Check if this customer has a Sales Order Line for this Ticket Discount or not
				$UpdateTicketSOLsql = 	"INSERT INTO  sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
											VALUES ('$SalesOrderNum','Ticket Discount','$TicketProductID','$TicketProductName','$TicketProductCredit','$TicketProductSuppID')
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$TicketProductID',
												Product_Name = '$TicketProductName',
												Sales_Amount = '$TicketProductCredit',
												Supplier_Supplier_ID = '$TicketProductSuppID',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateTicketSOLsql) === TRUE) {
					$NumberOfTicketLinesUpdated++;
				}
				
				//Check if this customer has a Customer Invoice Line for this Ticket Discount or not
				$UpdateTicketCILsql = 	"INSERT INTO  customer_invoice_line (Customer_Invoice_Line,Customer_Invoice_Num,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total,Line_Type,Add_Date)
											VALUES ('3','$CustomerInvoiceNum','$TicketProductID','$TicketProductName','1','$TicketProductCredit','3',NOW())
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$TicketProductID',
												Product_Name = '$TicketProductName',
												Qty = '1',
												Invoice_Line_Total = '$TicketProductCredit',
												Line_Type = '3',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateTicketCILsql) === TRUE) {
					$NumberOfTicketLinesCreated++;
				}
			}
		}
		
		//Sixth, check if this customer has a Single Supplement and add it to invoice and sales order
		$SingleSupplementStatus = $CustomersListData['roomtype'];
		if($SingleSupplementStatus == 'Single') { //Only if the customer wants a Single Room
			$GetSingleSQL = "SELECT id,name,supplierid,price FROM products WHERE id=$SingleCurrentProductID";
			$GetSingleResult = $db->query($GetSingleSQL);
			$GetSingleCount = $GetSingleResult->num_rows;
			if($GetSingleCount == 1) { //Only if there is a product for this group
				$SingleData = $GetSingleResult->fetch_assoc();
				$SingleProductID = $SingleData['id'];
				$SingleProductName = $SingleData['name'];
				$SingleProductSuppID = $SingleData['supplierid'];
				$SingleProductPrice = $SingleData['price'];
				
				//Check if this customer has a Sales Order Line for this Single Supplement or not
				$UpdateSingleSOLsql = 	"INSERT INTO  sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
											VALUES ('$SalesOrderNum','Single Supplement','$SingleProductID','$SingleProductName','$SingleProductPrice','$SingleProductSuppID')
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$SingleProductID',
												Product_Name = '$SingleProductName',
												Sales_Amount = '$SingleProductPrice',
												Supplier_Supplier_ID = '$SingleProductSuppID',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateSingleSOLsql) === TRUE) {
					$NumberOfSingleLinesUpdated++;
				}
				
				//Check if this customer has a Customer Invoice Line for this Single Supplement or not
				$UpdateSingleCILsql = 	"INSERT INTO  customer_invoice_line (Customer_Invoice_Line,Customer_Invoice_Num,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total,Line_Type,Add_Date)
											VALUES ('4','$CustomerInvoiceNum','$SingleProductID','$SingleProductName','1','$SingleProductPrice','4',NOW())
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$SingleProductID',
												Product_Name = '$SingleProductName',
												Qty = '1',
												Invoice_Line_Total = '$SingleProductPrice',
												Line_Type = '4',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateSingleCILsql) === TRUE) {
					$NumberOfSingleLinesCreated++;
				}
			}
		}
		
		//Seventh, check if this customer has an Airline Upgrade Product and add it to invoice and sales order
		$AirUpgradeID = $CustomersListData['upgradeproduct'];
		if($AirUpgradeID > 0) { //Only if the customer has an Air Upgrade Value
			$GetAirUpSQL = "SELECT id,name,supplierid,price FROM products WHERE id=$AirUpgradeID";
			$GetAirUpResult = $db->query($GetAirUpSQL);
			$GetAirUpCount = $GetAirUpResult->num_rows;
			if($GetAirUpCount == 1) { //Only if there is a product for this group
				$AirUpgradeData = $GetAirUpResult->fetch_assoc();
				$AirUpgradeID = $AirUpgradeData['id'];
				$AirUpgradeName = $AirUpgradeData['name'];
				$AirUpgradeSuppID = $AirUpgradeData['supplierid'];
				$AirUpgradePrice = $AirUpgradeData['price'];
				
				//Check if this customer has a Sales Order Line for this Airline Upgrade or not
				$UpdateAirUpSOLsql = 	"INSERT INTO  sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
											VALUES ('$SalesOrderNum','Air Upgrade','$AirUpgradeID','$AirUpgradeName','$AirUpgradePrice','$AirUpgradeSuppID')
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$AirUpgradeID',
												Product_Name = '$AirUpgradeName',
												Sales_Amount = '$AirUpgradePrice',
												Supplier_Supplier_ID = '$AirUpgradeSuppID',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateAirUpSOLsql) === TRUE) {
					$NumberOfAirUpLinesUpdated++;
				}
				
				//Check if this customer has a Customer Invoice Line for this Airline Upgrade or not
				$UpdateAirUpCILsql = 	"INSERT INTO  customer_invoice_line (Customer_Invoice_Line,Customer_Invoice_Num,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total,Line_Type,Add_Date)
											VALUES ('5','$CustomerInvoiceNum','$AirUpgradeID','$AirUpgradeName','1','$AirUpgradePrice','5',NOW())
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$AirUpgradeID',
												Product_Name = '$AirUpgradeName',
												Qty = '1',
												Invoice_Line_Total = '$AirUpgradePrice',
												Line_Type = '5',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateAirUpCILsql) === TRUE) {
					$NumberOfAirUpLinesCreated++;
				}
			}
		}
		
		//Eighth, check if this customer has a Land Upgrade Product and add it to invoice and sales order
		$LandUpgradeID = $CustomersListData['upgrade_land_product'];
		if($LandUpgradeID > 0) { //Only if the customer has an Land Upgrade Value
			$GetLandUpSQL = "SELECT id,name,supplierid,price FROM products WHERE id=$LandUpgradeID";
			$GetLandUpResult = $db->query($GetLandUpSQL);
			$GetLandUpCount = $GetLandUpResult->num_rows;
			if($GetLandUpCount == 1) { //Only if there is a product for this group
				$LandUpgradeData = $GetLandUpResult->fetch_assoc();
				$LandUpgradeID = $LandUpgradeData['id'];
				$LandUpgradeName = $LandUpgradeData['name'];
				$LandUpgradeSuppID = $LandUpgradeData['supplierid'];
				$LandUpgradePrice = $LandUpgradeData['price'];
				
				//Check if this customer has a Sales Order Line for this Land Upgrade or not
				$UpdateLandUpSOLsql = 	"INSERT INTO  sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
											VALUES ('$SalesOrderNum','Land Upgrade','$LandUpgradeID','$LandUpgradeName','$LandUpgradePrice','$LandUpgradeSuppID')
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$LandUpgradeID',
												Product_Name = '$LandUpgradeName',
												Sales_Amount = '$LandUpgradePrice',
												Supplier_Supplier_ID = '$LandUpgradeSuppID',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateLandUpSOLsql) === TRUE) {
					$NumberOfLandUpLinesUpdated++;
				}
				
				//Check if this customer has a Customer Invoice Line for this Land Upgrade or not
				$UpdateLandUpCILsql = 	"INSERT INTO  customer_invoice_line (Customer_Invoice_Line,Customer_Invoice_Num,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total,Line_Type,Add_Date)
											VALUES ('6','$CustomerInvoiceNum','$LandUpgradeID','$LandUpgradeName','1','$LandUpgradePrice','6',NOW())
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$LandUpgradeID',
												Product_Name = '$LandUpgradeName',
												Qty = '1',
												Invoice_Line_Total = '$LandUpgradePrice',
												Line_Type = '6',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateLandUpCILsql) === TRUE) {
					$NumberOfLandUpLinesCreated++;
				}
			}
		}
		
		//Ninth, check if this customer has a Transfer Product and add it to invoice and sales order
		$TransferProID = $CustomersListData['Transfer_Product_ID'];
		if($BuyHisOwnTicket == 1 AND $TransferProID > 0) { //Only if the customer has a Transfer Product Value
			$GetTransferProSQL = "SELECT id,name,supplierid,price FROM products WHERE id=$TransferProID";
			$GetTransferProResult = $db->query($GetTransferProSQL);
			$GetTransferProCount = $GetTransferProResult->num_rows;
			if($GetTransferProCount == 1) { //Only if there is a product for this group
				$TransferProData = $GetTransferProResult->fetch_assoc();
				$TransferProID = $TransferProData['id'];
				$TransferProName = $TransferProData['name'];
				$TransferProSuppID = $TransferProData['supplierid'];
				$TransferProPrice = $TransferProData['price'];
				
				//Check if this customer has a Sales Order Line for this Transfer Product or not
				$UpdateTransferSOLsql = 	"INSERT INTO  sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
											VALUES ('$SalesOrderNum','Transfer','$TransferProID','$TransferProName','$TransferProPrice','$TransferProSuppID')
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$TransferProID',
												Product_Name = '$TransferProName',
												Sales_Amount = '$TransferProPrice',
												Supplier_Supplier_ID = '$TransferProSuppID',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateTransferSOLsql) === TRUE) {
					$NumberOfTransferLinesUpdated++;
				}
				
				//Check if this customer has a Customer Invoice Line for this Transfer Product or not
				$UpdateTransferCILsql = 	"INSERT INTO  customer_invoice_line (Customer_Invoice_Line,Customer_Invoice_Num,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total,Line_Type,Add_Date)
											VALUES ('9','$CustomerInvoiceNum','$TransferProID','$TransferProName','1','$TransferProPrice','9',NOW())
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$TransferProID',
												Product_Name = '$TransferProName',
												Qty = '1',
												Invoice_Line_Total = '$TransferProPrice',
												Line_Type = '9',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateTransferCILsql) === TRUE) {
					$NumberOfTransferLinesCreated++;
				}
			}
		}
		
		//Tenth, check if this customer has an Extension Product and add it to invoice and sales order
		$ExtensionID = $CustomersListData['Extension_Product_ID'];
		if($ExtensionID > 0) { //Only if the customer has an Extension Product Value
			$GetExtProSQL = "SELECT id,name,supplierid,price FROM products WHERE id=$ExtensionID";
			$GetExtProResult = $db->query($GetExtProSQL);
			$GetExtProCount = $GetExtProResult->num_rows;
			if($GetExtProCount == 1) { //Only if there is a product for this group
				$ExtProData = $GetExtProResult->fetch_assoc();
				$ExtProID = $ExtProData['id'];
				$ExtProName = $ExtProData['name'];
				$ExtProSuppID = $ExtProData['supplierid'];
				$ExtProPrice = $ExtProData['price'];
				
				//Check if this customer has a Sales Order Line for this Extension Product or not
				$UpdateExtSOLsql = 	"INSERT INTO  sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
											VALUES ('$SalesOrderNum','Extension','$ExtProID','$ExtProName','$ExtProPrice','$ExtProSuppID')
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$ExtProID',
												Product_Name = '$ExtProName',
												Sales_Amount = '$ExtProPrice',
												Supplier_Supplier_ID = '$ExtProSuppID',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateExtSOLsql) === TRUE) {
					$NumberOfExtensionLinesUpdated++;
				}
				
				//Check if this customer has a Customer Invoice Line for this Extension Product or not
				$UpdateExtCILsql = 	"INSERT INTO  customer_invoice_line (Customer_Invoice_Line,Customer_Invoice_Num,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total,Line_Type,Add_Date)
											VALUES ('8','$CustomerInvoiceNum','$ExtProID','$ExtProName','1','$ExtProPrice','8',NOW())
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$ExtProID',
												Product_Name = '$ExtProName',
												Qty = '1',
												Invoice_Line_Total = '$ExtProPrice',
												Line_Type = '8',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateExtCILsql) === TRUE) {
					$NumberOfExtensionLinesCreated++;
				}
			}
		}
		
		//Eleventh, update the invoice and sales order records about the main AIRLINE product
		if($AirlineProID > 0) {
			$GetMainAirSQL = "SELECT id,name,supplierid,price FROM products WHERE id=$AirlineProID";
			$GetMainAirResult = $db->query($GetMainAirSQL);
			$GetMainAirCount = $GetMainAirResult->num_rows;
			if($GetMainAirCount == 1) { //Only if there is a product for this group
				$MainAirData = $GetMainAirResult->fetch_assoc();
				$MainAirID = $MainAirData['id'];
				$MainAirName = $MainAirData['name'];
				$MainAirSuppID = $MainAirData['supplierid'];
				$MainAirPrice = $MainAirData['price'];
				
				//Advanced query for the Sales_Order table
				$UpdateMainAirSOLsql = 	"INSERT INTO  sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
											VALUES ('$SalesOrderNum','Airline Package','$MainAirID','$MainAirName','$MainAirPrice','$MainAirSuppID')
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$MainAirID',
												Product_Name = '$MainAirName',
												Sales_Amount = '$MainAirPrice',
												Supplier_Supplier_ID = '$MainAirSuppID',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateMainAirSOLsql) === TRUE) {
					$NumberOfAirlineUpdated++;
				}
				
				//Advanced query for the Customer_Invoice table
				$UpdateMainAirCILsql = 	"INSERT INTO  customer_invoice_line (Customer_Invoice_Line,Customer_Invoice_Num,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total,Line_Type,Add_Date)
											VALUES ('1','$CustomerInvoiceNum','$MainAirID','$MainAirName','1','$MainAirPrice','1',NOW())
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$MainAirID',
												Product_Name = '$MainAirName',
												Qty = '1',
												Invoice_Line_Total = '$MainAirPrice',
												Line_Type = '1',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateMainAirCILsql) === TRUE) {
					$NumberOfAirlineCreated++;
				}
			}
		}
		
		//Twelveth, update the invoice and sales order records about the main LAND product
		if($LandProID > 0) {
			$GetMainLandSQL = "SELECT id,name,supplierid,price FROM products WHERE id=$LandProID";
			$GetMainLandResult = $db->query($GetMainLandSQL);
			$GetMainLandCount = $GetMainLandResult->num_rows;
			if($GetMainLandCount == 1) { //Only if there is a product for this group
				$MainLandData = $GetMainLandResult->fetch_assoc();
				$MainLandID = $MainLandData['id'];
				$MainLandName = $MainLandData['name'];
				$MainLandSuppID = $MainLandData['supplierid'];
				$MainLandPrice = $MainLandData['price'];
				
				//Advanced query for the Sales_Order table
				$UpdateMainLandSOLsql = "INSERT INTO  sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
											VALUES ('$SalesOrderNum','Land Package','$MainLandID','$MainLandName','$MainLandPrice','$MainLandSuppID')
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$MainLandID',
												Product_Name = '$MainLandName',
												Sales_Amount = '$MainLandPrice',
												Supplier_Supplier_ID = '$MainLandSuppID',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateMainLandSOLsql) === TRUE) {
					$NumberOfLandUpdated++;
				}
				
				//Advanced query for the Customer_Invoice table
				$UpdateMainLandCILsql = 	"INSERT INTO  customer_invoice_line (Customer_Invoice_Line,Customer_Invoice_Num,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total,Line_Type,Add_Date)
											VALUES ('2','$CustomerInvoiceNum','$MainLandID','$MainLandName','1','$MainLandPrice','2',NOW())
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$MainLandID',
												Product_Name = '$MainLandName',
												Qty = '1',
												Invoice_Line_Total = '$MainLandPrice',
												Line_Type = '2',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateMainLandCILsql) === TRUE) {
					$NumberOfLandCreated++;
				}
			}
		}
		
		//Thirteenth, update the invoice and sales order records about the Insurance product yyyyyyyyy
		$InsuranceID = $CustomersListData['Insurance_Product_ID'];
		if($InsuranceID > 0) { //Only if the customer has an Extension Product Value
			$GetInsurProSQL = "SELECT id,name,supplierid,price FROM products WHERE id=$InsuranceID";
			$GetInsurProResult = $db->query($GetInsurProSQL);
			$GetInsurProCount = $GetInsurProResult->num_rows;
			if($GetInsurProCount == 1) { //Only if there is a product for this group
				$InsurProData = $GetInsurProResult->fetch_assoc();
				$InsurProID = $InsurProData['id'];
				$InsurProName = $InsurProData['name'];
				$InsurProSuppID = $InsurProData['supplierid'];
				$InsurProPrice = $InsurProData['price'];
				
				//Check if this customer has a Sales Order Line for this Insurance Product or not
				$UpdateInsurSOLsql = 	"INSERT INTO  sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
											VALUES ('$SalesOrderNum','Insurance','$InsurProID','$InsurProName','$InsurProPrice','$InsurProSuppID')
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$InsurProID',
												Product_Name = '$InsurProName',
												Sales_Amount = '$InsurProPrice',
												Supplier_Supplier_ID = '$InsurProSuppID',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateInsurSOLsql) === TRUE) {
					$NumberOfInsuranceLinesUpdated++;
				}
				
				//Check if this customer has a Customer Invoice Line for this Insurance Product or not
				$UpdateInsurCILsql = 	"INSERT INTO  customer_invoice_line (Customer_Invoice_Line,Customer_Invoice_Num,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total,Line_Type,Add_Date)
											VALUES ('8','$CustomerInvoiceNum','$InsurProID','$InsurProName','1','$InsurProPrice','10',NOW())
										ON DUPLICATE KEY
											UPDATE 
												Product_Product_ID = '$InsurProID',
												Product_Name = '$InsurProName',
												Qty = '1',
												Invoice_Line_Total = '$InsurProPrice',
												Line_Type = '10',
												Mod_Date = NOW();";
				if ($db->multi_query($UpdateInsurCILsql) === TRUE) {
					$NumberOfInsuranceLinesCreated++;
				}
			}
		}
		
		//WHATEVER COUNTER :D << 14, The most complicated part, updating the data of the JOINT CUSTOMER correctly
		if($groupinvoice == 1) {
			$CGconn = new mysqli($servername, $username, $password, $dbname);
			if ($CGconn->connect_error) {die("Connection failed: " . $CGconn->connect_error);} 
			$GetJointCustomersSQL = "SELECT ci.Customer_Invoice_Num,cg.Primary_Customer_ID,cg.Additional_Traveler_ID_1,cg.Additional_Traveler_ID_2,cg.Additional_Traveler_ID_3,cg.Additional_Traveler_ID_4,cg.Additional_Traveler_ID_5,cg.Additional_Traveler_ID_6,cg.Additional_Traveler_ID_7,cg.Additional_Traveler_ID_8 FROM customer_groups cg JOIN customer_invoice ci ON ci.Customer_Account_Customer_ID=cg.Primary_Customer_ID WHERE $CustomerID IN(cg.Primary_Customer_ID,cg.Additional_Traveler_ID_1,cg.Additional_Traveler_ID_2,cg.Additional_Traveler_ID_3,cg.Additional_Traveler_ID_4,cg.Additional_Traveler_ID_5,cg.Additional_Traveler_ID_6,cg.Additional_Traveler_ID_7,cg.Additional_Traveler_ID_8) AND cg.Type='Group Invoice' AND cg.Group_ID=$GroupID";
			$GetJointCustomersResult = $CGconn->query($GetJointCustomersSQL);
			$GetJointCustomersCount = $GetJointCustomersResult->num_rows;
			if($GetJointCustomersCount == 1) { 
				$JointCustomersData = $GetJointCustomersResult->fetch_assoc();
				$JointPrimaryCustomer = $JointCustomersData['Primary_Customer_ID'];
				$JointPrimaryCustomerInvoice = $JointCustomersData['Customer_Invoice_Num'];
				$Traveler1 = $JointCustomersData['Additional_Traveler_ID_1'];
				$Traveler2 = $JointCustomersData['Additional_Traveler_ID_2'];
				$Traveler3 = $JointCustomersData['Additional_Traveler_ID_3'];
				$Traveler4 = $JointCustomersData['Additional_Traveler_ID_4'];
				$Traveler5 = $JointCustomersData['Additional_Traveler_ID_5'];
				$Traveler6 = $JointCustomersData['Additional_Traveler_ID_6'];
				$Traveler7 = $JointCustomersData['Additional_Traveler_ID_7'];
				$Traveler8 = $JointCustomersData['Additional_Traveler_ID_8'];
				
				$UpdateJointCustomersql  = "UPDATE customer_account SET groupinvoice='1',updatetime=NOW() WHERE (contactid=$JointPrimaryCustomer OR contactid=$Traveler1 OR contactid=$Traveler2 OR contactid=$Traveler3 OR contactid=$Traveler4 OR contactid=$Traveler5 OR contactid=$Traveler6 OR contactid=$Traveler7 OR contactid=$Traveler8) AND tourid=$GroupID;";
				$UpdateJointCustomersql .= "UPDATE customer_invoice SET Group_Invoice='1',Mod_Date=NOW() WHERE Customer_Invoice_Num=$JointPrimaryCustomerInvoice;";
				$UpdateJointCustomersql .= "UPDATE customer_invoice SET Additional_Invoice='1', Mod_Date=NOW() WHERE Customer_Account_Customer_ID IN ($Traveler1,$Traveler2,$Traveler3,$Traveler4,$Traveler5,$Traveler6,$Traveler7,$Traveler8) AND Group_ID=$GroupID;";
				$UpdateJointCustomersql .= "DELETE FROM customer_invoice_line WHERE Customer_Invoice_Num=$JointPrimaryCustomerInvoice;";
				$UpdateJointCustomersql .= "SET @cnt = 0;
										SET @InvoiceNum = $JointPrimaryCustomerInvoice;
										INSERT INTO customer_invoice_line(Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
										SELECT (@InvoiceNum) AS Customer_Invoice_Num,(@cnt := @cnt + 1) AS Customer_Invoice_Line,('Joint Invoice') AS Line_Type,SOL.Product_Product_ID,pro.name AS Product_Name,COUNT(SOL.Product_Product_ID) AS Qty,
										(pro.price) AS Invoice_Line_Total FROM sales_order_line SOL
										JOIN sales_order SO 
										ON SO.Sales_Order_Num=SOL.Sales_Order_Num
										JOIN products pro 
										ON pro.id=SOL.Product_Product_ID
										WHERE SO.Customer_Account_Customer_ID IN ($JointPrimaryCustomer,$Traveler1,$Traveler2,$Traveler3,$Traveler4,$Traveler5,$Traveler6,$Traveler7,$Traveler8) AND SO.Group_ID=$GroupID
										GROUP BY SOL.Product_Product_ID
										ORDER BY Customer_Invoice_Line;";
				$UpdateJointCustomersql .= "UPDATE customer_invoice AS Inv,
										(
											SELECT SUM(pro.price) AS Invoice_Line_Total FROM sales_order_line SOL
											JOIN sales_order SO 
											ON SO.Sales_Order_Num=SOL.Sales_Order_Num
											JOIN products pro 
											ON pro.id=SOL.Product_Product_ID
											WHERE SO.Customer_Account_Customer_ID IN ($JointPrimaryCustomer,$Traveler1,$Traveler2,$Traveler3,$Traveler4,$Traveler5,$Traveler6,$Traveler7,$Traveler8) AND SO.Group_ID=$GroupID
										) AS Src
										SET Inv.Invoice_Amount=Src.Invoice_Line_Total WHERE Inv.Customer_Invoice_Num=$JointPrimaryCustomerInvoice;";
				
				if ($CGconn->multi_query($UpdateJointCustomersql) === TRUE) {
					$NumberOfJointInvoiceUpdated++;
				}
			//$CGconn->multi_query($CustomerGroupQuery);
			$CGconn->close();
			}
		}
		
		//Lastly, collect the total lines for the sales order and the customer invoice and update the main tables "Sales_Order" and "Customer_Invoice"
		$UpdateSOandCIconn = new mysqli($servername, $username, $password, $dbname);
		if ($UpdateSOandCIconn->connect_error) {die("Connection failed: " . $UpdateSOandCIconn->connect_error);} 

		$GetTotalSOLsql = "SELECT SUM(Sales_Amount) AS Total_Sales FROM sales_order_line WHERE Sales_Order_Num=$SalesOrderNum";
		$GetTotalSOLresult = $UpdateSOandCIconn->query($GetTotalSOLsql);
		$GetTotalSOLamount = $GetTotalSOLresult->fetch_assoc();
		$TotalSalesOrder = $GetTotalSOLamount['Total_Sales'];

		$GetTotalCILsql = "SELECT SUM(Invoice_Line_Total) AS Total_Invoice_Lines FROM customer_invoice_line WHERE Customer_Invoice_Num=$CustomerInvoiceNum";
		$GetTotalCILresult = $UpdateSOandCIconn->query($GetTotalCILsql);
		$GetTotalCILamount = $GetTotalCILresult->fetch_assoc();
		$TotalCustomerInvoice = $GetTotalCILamount['Total_Invoice_Lines'];

		$UpdateSOandCI = "UPDATE sales_order SET Total_Sale_Price='$TotalSalesOrder', Mod_Date=NOW() WHERE Sales_Order_Num=$SalesOrderNum;";
		$UpdateSOandCI .= "UPDATE customer_invoice SET Invoice_Amount='$TotalCustomerInvoice', Mod_Date=NOW() WHERE Customer_Invoice_Num=$CustomerInvoiceNum;";

		if ($UpdateSOandCIconn->multi_query($UpdateSOandCI) === TRUE) {
			//Success
		} else {
			echo $sql11 . "<br>" . $UpdateSOandCIconn->error;
		}
		$UpdateSOandCIconn->close();
					
	} //The while for each customer is done here
	echo "<h3>Refreshing Customers Data!</h3><ul style='text-align: left;list-style-type: circle;margin-left: 35px;'>";
	echo "<li id='showMe1'><span style='font-weight:bold;'>Sales Orders:</span> Updated: ".$NumberOfSalesOrdersUpdated."</li>";
	echo "<li id='showMe2'><span style='font-weight:bold;'>Customer Invoices:</span> Updated: ".$NumberOfInvoicesUpdated."</li>";
	echo "<li id='showMe3'><span style='font-weight:bold;'>Airline Package Lines:</span> Updated: ".$NumberOfAirProLinesUpdated."</li>";
	echo "<li id='showMe4'><span style='font-weight:bold;'>Land Package Lines:</span> Updated: ".$NumberOfLandProLinesUpdated."</li>";
	echo "<li id='showMe5'><span style='font-weight:bold;'>Airline Credit Lines:</span> Updated: ".$NumberOfTicketLinesUpdated."</li>";
	echo "<li id='showMe6'><span style='font-weight:bold;'>Single Supplement Lines:</span> Updated: ".$NumberOfSingleLinesUpdated."</li>";
	echo "<li id='showMe7'><span style='font-weight:bold;'>Air Upgrade Lines:</span> Updated: ".$NumberOfAirUpLinesUpdated."</li>";
	echo "<li id='showMe8'><span style='font-weight:bold;'>Land Upgrade Lines:</span> Updated: ".$NumberOfLandUpLinesUpdated."</li>";
	echo "<li id='showMe9'><span style='font-weight:bold;'>Transfer Lines:</span> Updated: ".$NumberOfTransferLinesUpdated."</li>";
	echo "<li id='showMe10'><span style='font-weight:bold;'>Extension Lines:</span> Updated: ".$NumberOfExtensionLinesUpdated."</li>";
	echo "<li id='showMe11'><span style='font-weight:bold;'>Insurance Lines:</span> Updated: ".$NumberOfInsuranceLinesUpdated."</li>";
	echo "<li id='showMe12'><span style='font-weight:bold;'>Joint Customers / Invoices:</span> Updated: ".$NumberOfJointInvoiceUpdated."</li>";
	echo "</ul>";
	echo "<br /><span id='showMe13'><span style='font-weight:bold;color:#2f782f'>Done Successfully!</span> The page will be refreshed in 5 seconds ...</span>";
	echo "<meta http-equiv='refresh' content='17;groups-edit.php?id=$GroupID&action=edit&tab=participants'>";
}

//Create new Sub product for the Land Main Product
if(isset($_POST["GroupCurrentID"])){

	$GroupCurrentID = $_POST["GroupCurrentID"];
	if ($_POST['check_which_item_selected'] == 0) {
		$name = $_POST['subproductname_textbox'];
	} else {
		$name = $_POST['subproductname'];
	}
	$description = $_POST["description"];
	$cost = 0;
	$subproduct = 1;
	$parentid = $_POST["ParentProductID"];
	$producttype = $_POST["producttype"];
	if ($producttype == "Hotels"){
		$meta1 = $_POST["hotelphone"];
		$meta2 = $_POST["hotelcity"];
		$meta3 = $_POST["checkindate"];
		$meta4 = $_POST["checkoutdate"];
	} elseif ($producttype == "Guides"){
		$meta1 = $_POST["GuidePhone"];
		$meta2 = $_POST["GuideCity"];
		$meta3 = NULL;
		$meta4 = NULL;
	} else {
		$meta1 = NULL;
		$meta2 = NULL;
		$meta3 = NULL;
		$meta4 = NULL;
	}
	$status = 1;
	$thesupplier = $_POST['thesupplier'];
	$thecategory = 2;

	// check products_logistics exists or not - if not exists add to products_logistics table
	$sql = "SELECT * FROM products_logistics WHERE Sub_Name = '$name' AND Sub_Type = '$producttype' LIMIT 1";
	$result = $db->query($sql);
	$checkLogisticsExists = $result->num_rows;
	if ( $checkLogisticsExists == 0) {
		$sqlInsert = "INSERT INTO products_logistics (Sub_Type, Sub_Name, Sub_Description, Sub_Phone, Sub_City) VALUES ('$producttype', '$name', '$description', '$meta1', '$meta2')";
		$db->query($sqlInsert);
	} else {
		$sqlUpdate = "UPDATE products_logistics SET Sub_Description = '$description', Sub_Phone = '$meta1', Sub_City = '$meta2' WHERE Sub_Type = '$producttype' AND Sub_Name = '$name'";
		$db->query($sqlUpdate);
	}

	$sql = "INSERT INTO products (name,description,supplierid,categoryid,subproduct,parentid,producttype,cost,paymentterm,meta1,meta2,meta3,meta4,status)
	VALUES ('$name','$description','$thesupplier','$thecategory','$subproduct','$parentid','$producttype','$cost','$paymentterm','$meta1','$meta2','$meta3','$meta4','$status')";

	if ($db->query($sql) === TRUE) {
		echo "<div class='upload-loader-small'></div> Adding the sub product ($name) to this group ...";
		echo "<meta http-equiv='refresh' content='2;url=groups-edit.php?id=$GroupCurrentID&action=edit&tab=land'>";
	} else {
		echo $sql . "<br>" . $db->error."<br> error";
	}

	$db->close();
}

//Add a new Group Notes for the group itsef
if(isset($_POST['groupnote'])){
    if($_POST['groupnote']!=""){
		$note = mysqli_real_escape_string($db, $_POST['groupnote']);
		$username = mysqli_real_escape_string($db, $_POST['username']);
		$tourid = mysqli_real_escape_string($db, $_POST['NoteGroupID']);
		$tourname = mysqli_real_escape_string($db, $_POST['NoteGroupName']);
		$AddNotesSQL = "INSERT INTO groups_notes (username, note, date_time, group_id) VALUES ('$username','$note',NOW(),'$tourid');";
		$AddNotesSQL.= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$username','group note','$tourid','added','$tourname');";
		if ($db->multi_query($AddNotesSQL) === TRUE) {
			echo "<h3>Success</h3> The group note has been added successfully!<br />The page will be refreshed in seconds.";
			echo "<meta http-equiv='refresh' content='2;groups-edit.php?id=$tourid&action=edit&tab=notes'>";
		} else {
			echo $AddNotesSQL . "<br>" . $db->error;
		}
	} else {
		echo "<h3 class='text-danger'>Error</h3> Please write the note before submitting.";
	}
}

// Get List of SubProduct in Groups Land Info screen.
if (isset($_POST['productLandInfo']) && ($_POST['productLandInfo'] == 'Yes')) {

	$finalresultSubProducts = array();
	$productType = $_POST['productType'];
	$sql = "SELECT Sub_Name FROM products_logistics WHERE Sub_Type='".$productType."'";
	$result = $db->query($sql);
	while($resultSubProducts = $result->fetch_assoc()){ 
		$finalresultSubProducts[] = ['subProductName' => $resultSubProducts['Sub_Name']];
	}
	echo json_encode($finalresultSubProducts);
}

// Get SubProduct Details in Groups Land Info screen.
if (isset($_POST['productDetailInfo']) && ($_POST['productDetailInfo'] == 'Yes')) {

	$finalresultSubProductsDetails = array();
	$productType = $_POST['productType'];
	$subProductName = $_POST['subProductName'][0];
	if ($productName != 'add_new') {
		$sql = "SELECT * FROM products_logistics WHERE Sub_Name='".$subProductName."' AND Sub_Type='".$productType."' LIMIT 1";
		$result = $db->query($sql);
		$resultSubProductsDetails = $result->fetch_assoc();
		$finalresultSubProductsDetails[] = ['subProductDescription' => $resultSubProductsDetails['Sub_Description'], 'subPhone' => $resultSubProductsDetails['Sub_Phone'], 'subCity' => $resultSubProductsDetails['Sub_City']];
	}
	echo json_encode($finalresultSubProductsDetails);
}
?>