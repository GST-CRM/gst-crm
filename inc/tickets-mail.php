<?php
include_once 'vendor/autoload.php';

include_once __DIR__ . '/../models/customer_invoice.php';

use PHPMailer\PHPMailer\PHPMailer ;

//set default file prefix.
$isInvoiceFlag = false ;
if ($FileTitlePrefix == 'Ticket') {
    $filePrefix = 'GST-Ticket' ;
    $template_code = 'GST_INVOICE' ;
    $isInvoiceFlag = true ;
}

$TicketsIdSet = [] ;

//even if there is one invoice convert it to an array. 
if( isset($__REQUEST['TicketID']) ) {
    $TicketsIdSet = $__REQUEST['TicketID'] ;
}
ob_start() ;
if( ! isset($TicketsIdSet) || empty($TicketsIdSet) ) {
    $TicketsIdSet = 0 ;
}
//itereate with invoice and mail invoce/receipt.
$p = 0 ;
foreach( $TicketsIdSet as $TicketID ) {

    try {

        $PDF_OUTPUT = 'S';
        $PDF_HEADER = true ;
        $attachmentData    = include __DIR__ . '/ticket-pdf.php';

        //Attachments
        $attachmentName = $filePrefix. '-' . $TicketID . '.pdf' ;
        $params = [] ;
        $to = [] ;
        
        if( isset($MESSAGE) ) {
            GUtils::sendMail($to, $SUBJECT, $MESSAGE, $attachmentName, $attachmentData) ;
        }
        else {
            GUtils::sendTemplateMailByCode($to, $template_code, $params, $attachmentName, $attachmentData) ;
        }
        
    } catch (Exception $e) {
        
    }
}