<?php

include_once __DIR__ . '/../models/customer_account.php';
include_once __DIR__ . '/../models/customer_invoice.php';
include_once __DIR__ . '/../models/customer_payment.php';
include_once __DIR__ . '/../models/customer_invoice_line.php';
include_once __DIR__ . '/../models/customer_refund.php';
include_once __DIR__ . '/../models/transactions.php';

$code = 0;

try {
    //intialize dependencies..
    $customerInvoice = new CustomerInvoice();
    $customerPayment = new CustomerPayment();
    $customerInvoiceLine = new CustomerInvoiceLine();
    $customerRefund  = new CustomerRefund();
    $transactions = new Transactions();

    //find invoice data first
    $invoiceId   = $__REQUEST['invoiceId'];
    $cancellationMessage   = isset($__REQUEST['cancellation_message']) ? $__REQUEST['cancellation_message'] : '' ;
    $cancellationDate = isset($__REQUEST['cancellation_date']) ? $__REQUEST['cancellation_date'] : '' ;
    $noCancelFee = isset($__REQUEST['nocancelfee']) ? $__REQUEST['nocancelfee'] : '0' ;
    $customCancelFee = isset($__REQUEST['customcancelfee']) ? $__REQUEST['customcancelfee'] : '0' ;
    $customCancelFeeValue = isset($__REQUEST['customcancelfee_value']) ? floatval($__REQUEST['customcancelfee_value']) : '0' ;
    $invoiceData = $customerPayment->customerInvoiceDetails($invoiceId);

    //check whether cancelled it ?
    $isCancelled = $customerRefund->get(['Customer_Invoice_Num' => $invoiceId]) ;
    
    //if already canceled, return failure immediately.
    if( $invoiceData && $invoiceData['Status'] == CustomerInvoice::$CANCELLED ) {
        return GUtils::jsonFail(['message' => "This Invoice is already cancelled."]);
    }


    //other calculate te refund amount and proceed.
    if( $noCancelFee ) {
        $refundInfo['Cancellation_Charge'] = 0.0 ;
        $refundInfo['Refund_Amount'] = $invoiceData['payment_amount'] ;
        $refundInfo['Cancellation_Outcome'] = 'No Fee Cancellation';
    }
    else if( $customCancelFee ) {
        $paid = floatval($invoiceData['payment_amount']) ;
        $charges = floatval($invoiceData['charges']) ;
        $customFee = floatval($customCancelFeeValue) ;
        $refundAmount = $paid - $customFee ;

        $refundInfo['Cancellation_Charge'] = $customCancelFeeValue ;
        $refundInfo['Refund_Amount'] = $refundAmount ;
        $refundInfo['Cancellation_Outcome'] = $cancellationMessage;
    }    
    else {
        $refundInfo = $customerRefund->calculateRefund($invoiceData['Invoice_Amount'], $invoiceData['charges'], $invoiceData['paid'], $invoiceData['trip_start_days'], $invoiceData['Cancellation_Charge_Others'], $invoiceData['Customer_Invoice_Num']);
    }    

    Model::begin() ;
    
    if( $invoiceData['Additional_Invoice'] == 1 ) {
        $thePayee = (new CustomerGroups())->getPrimaryTraveler($invoiceData['tourid'], $invoiceData['contactid']) ;
        $payeeInvoiceId = $customerInvoice->getScalar( 'Customer_Invoice_Num', ['Customer_Account_Customer_ID' => $thePayee, 'Group_ID' =>$invoiceData['tourid'] ]) ;
    }
    else {
        $thePayee = $invoiceData['contactid'] ;
        $payeeInvoiceId = $invoiceId ;
    }
    //Store in payees Invoice ID
    
    
 
    //preare refund data.
    $refund = array(
        'Customer_Account_Customer_ID'  => $thePayee,
        'Cancellation_Charge'  => $refundInfo['Cancellation_Charge'],
        'Refund_Amount'        => $refundInfo['Refund_Amount'],
        'Cancellation_Outcome' => $refundInfo['Cancellation_Outcome'],
        'Cancelled_On'          => $cancellationDate,
        'Customer_Invoice_Num' => $invoiceId,
        'Payee_Invoice_Num' => $payeeInvoiceId,
        'Cancelation_Reason'   => $cancellationMessage ,
        'Status'               => CustomerRefund::$CREATED ,
        'Add_Date'             => Model::sqlNoQuote('now()'),
        'Mod_Date'             => Model::sqlNoQuote('now()'),
        );
    
    //prepare transaction data
    $tdata = array(
        'Transaction_Type' => 5,
        'Chart_of_Accounts_GL_Account_N' => '1200',
        'Transaction_Amount' => $refundInfo['Cancellation_Charge'],
        'Customer_Account _Customer_ID' => $thePayee,
        'Supplier_Supplier_ID' => '0',
        'Add_Date' => Model::sqlNoQuote('now()'),
        'Mod_Date' => Model::sqlNoQuote('now()'),
    ) ;    
    $transactions->insert($tdata) ;
    
    $datal = array (
        'Customer_Invoice_Num' => $invoiceId, 
        'Customer_Invoice_Line' => 1,
        'Product_Name' => 'Cancellation Charge',
        'Qty' => '1',
        'Invoice_Line_Total' => $refundInfo['Cancellation_Charge'],
        'Add_Date' => Model::sqlNoQuote('now()'),
        'Mod_Date' => Model::sqlNoQuote('now()')
    ) ;
    $customerInvoiceLine->insert($datal) ;

    //update customer status as cancelled.
    $exist  =$customerRefund->get(['Customer_Invoice_Num' => $invoiceId]) ;
    if(is_array($exist) ) {
        $status = $customerRefund->update($refund, ['Refund_ID' => $exist['Refund_ID']]) ;
    }
    else {
        $status = $customerRefund->insert($refund) ;
    }
    if( $status ) {
        $customerInvoice->update( ['status' => CustomerInvoice::$CANCELLED ], ['Customer_Invoice_Num' => $invoiceId] ) ;
    }
    
} catch (\Exception $e) {
    $code = $e->getCode();
}

if ($status) {
    Model::commit() ;
    GUtils::jsonOk(['message' => 'Successfully saved.']);
} else {
    Model::rollback() ;
    GUtils::jsonFail(['message' => "Failed to save, Please try again (Error: $code)."]);
}

