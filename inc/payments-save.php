<?php

//POST DATA
//---------
//invoice_date: 2019-03-17
//cbPayment[]: 1900008
//payment[]: 7800
//hidPaymentSave: 1
//submit: create

include_once __DIR__ . '/../models/customer_invoice.php' ;
include_once __DIR__ . '/../models/customer_groups.php' ;
include_once __DIR__ . '/../models/transactions.php' ;

$cbPayment = '' ;
$payment = '' ;

$invoiceDate = GUtils::mysqlDate( $__REQUEST['invoice_date'] ) ;
$paymentMethod = $__REQUEST['payment_method'] ;
$paymentDepositTo = '';
$paymentCheck = '';
if( $paymentMethod == 'Cash' ) {
    $paymentDepositTo = $__REQUEST['Deposit_To_Cash'];
} elseif( $paymentMethod == 'Bank Transfer' ) {
    $paymentDepositTo = $__REQUEST['Deposit_To_Bank'];
} elseif( $paymentMethod == 'Check' ) {
    $paymentDepositTo = $__REQUEST['Deposit_To_Bank'];
    $paymentCheck = $__REQUEST['Check_Number'];
} elseif( $paymentMethod == 'Credit Card' ) {
    $paymentDepositTo = $__REQUEST['Deposit_To_Card'];
} elseif( $paymentMethod == 'Online Payment' ) {
    $paymentDepositTo = $__REQUEST['Deposit_To_PayPal'];
}


$chargable = 0 ;

if( isset($__REQUEST['hidPaymentSave']) ) {
    
    //upload file {
    $config = [] ;
    $config['max_size'] = 50000000 ;
    $config['upload_path'] = 'invattach/';

    GUtils::doUpload('filAttachment', $config) ;
    $fileName = (isset($config['file_name']) ? $config['file_name'] : '') ;
    //}
    //
    //update everything here..
    if( isset($__GET['payment']) && $__GET['payment']  ) {
        $paymentId = $__GET['payment'] ;
        
        Model::begin() ;

        foreach( $__REQUEST['payment'] as $invoiceId => $amount ) {
            if( $amount == 0 ) {
                continue; 
            }
            $chargable = 0 ;
            $charge = 0 ;
            if( $paymentMethod == 'Credit Card' ) {
                $chargable = $amount ;
            }
            $comment = $__REQUEST['description'][$invoiceId] ;
            //Find Customer Id of invoice.
            $sqlc = "SELECT Customer_Invoice_Num, Customer_Account_Customer_ID, Transaction_Type_Transaction_ID FROM customer_payments WHERE Customer_Payment_ID='$paymentId' LIMIT 1" ;
            $invData = GDb::fetchRow($sqlc) ;

            $customerId = $invData['Customer_Account_Customer_ID'] ;
            $invoiceId = $invData['Customer_Invoice_Num'] ;
            $transactionId = $invData['Transaction_Type_Transaction_ID'] ;

            //inser to transaction
            $sqlt = "UPDATE `transactions` SET `Transaction_Amount`='$amount', `Mod_Date`= now() WHERE Transaction_Type_Transaction_ID='$transactionId' LIMIT 1 ;" ;
            $db->query($sqlt) ;

            if( $chargable != 0 ) {
                $charge = ($chargable * 4) / 100 ;
            }
                
            $sqlp = "UPDATE customer_payments SET
						Customer_Payment_Date = '$invoiceDate',
						Customer_Payment_Amount =  '$amount', 
						Customer_Payment_Comments = '$comment',
						Charges = '$charge' ,
						Attachment = '$fileName',
						Customer_Payment_Method = '$paymentMethod',
						Customer_Payment_Deposit_To = '$paymentDepositTo',
						Customer_Payment_Check = '$paymentCheck',
						Transaction_Type_Transaction_ID = '$transactionId',
						Mod_Date = now()
					WHERE Customer_Payment_ID='$paymentId' LIMIT 1" ;

            $db->query($sqlp) ;

            break ;
        }
        Model::commit() ;
        GUtils::setSuccess('Payment details saved successfully.') ;
    }
    else {

        Model::begin() ;
        foreach( $__REQUEST['payment'] as $invoiceId => $amount ) {
            if( $amount == 0 ) {
                continue; 
            }            
            $chargable = 0 ;
            if( $paymentMethod == 'Credit Card' ) {
                $chargable = $amount ;
            }
            $comment = $__REQUEST['description'][$invoiceId] ;

            //Find Customer Id of invoice.
            $sqlc = "SELECT Customer_Account_Customer_ID, Customer_Order_Num, Group_ID FROM customer_invoice WHERE Customer_Invoice_Num='$invoiceId' LIMIT 1" ;
            $cusInfo = GDb::fetchRow($sqlc);
            $customerId = $cusInfo['Customer_Account_Customer_ID'];
            $salesOrderNumber = $cusInfo['Customer_Order_Num'];
            $PaymentGroupID = $cusInfo['Group_ID'];
           
            //Additional customer info for joint invoices            
            $ci = (new CustomerInvoice()) ;
            $ciData = $ci->get(['Customer_Invoice_Num' => $invoiceId]) ;
            $customerCount = 1 ;
            if( $ciData['Group_Invoice'] == 1 ) {
                $customerSet = [ $customerId ] ;
            }
            else {
                $customerSet = [ $customerId ] ;
            }
            
            $indivitualAmount = floatval( $amount / $customerCount) ;
            $charge = 0.0 ;
            if( $chargable != 0 ) {
                $charge = floatval( ( (($chargable * 4) / 100) / $customerCount) ) ;
            }

            $uniqId = GUtils::getUniqId(8) ;
            foreach( $customerSet as $customerId ) {
                //inser to transaction
                $dataTransaction = [
                    'Transaction_Type' => '8',
                    'Chart_of_Accounts_GL_Account_N' => '1001',
                    'Transaction_Amount' => $indivitualAmount, 
                    'Customer_Account _Customer_ID' => $customerId,
                    'Supplier_Supplier_ID' => '0',
                    'Canceled_Flag' => '0', 
                    'Posted_Flag' => '0', 
                    'Voided_Flag' => '0', 
                    'Add_Date' => Model::sqlNoQuote('now()'),
                    'Mod_Date' => Model::sqlNoQuote('now()')
                ] ;
                $transTable = new Transactions() ;
                $transTable->insert($dataTransaction) ;

                $transactionId = $transTable->lastInsertId() ;

                //Make payments
                $dataCi = [
                    'Customer_Invoice_Num' => $invoiceId, 
                    'Customer_Payment_Date' => $invoiceDate,
                    'Customer_Account_Customer_ID' => $customerId,
                    'Customer_Payment_Sales_Order_ID' => $salesOrderNumber,
                    'Group_ID' => $PaymentGroupID,
                    'Customer_Payment_Amount' => $indivitualAmount,
                    'Customer_Payment_Method' => $paymentMethod,
                    'Customer_Payment_Deposit_To' => $paymentDepositTo,
                    'Customer_Payment_Check' => $paymentCheck,
                    'Customer_Payment_Comments' => $comment,
                    'Attachment' => $fileName,
                    'Transaction_Type_Transaction_ID' => $transactionId,
                    'Payment_Unique_ID' => $uniqId,
                    'Add_Date' => Model::sqlNoQuote('now()'),
                    'Mod_Date' => Model::sqlNoQuote('now()'),
                    'Status' => 1
                    ]; 

                $cp = new CustomerPayment() ;
                $cp->insert($dataCi) ;
                $paymentId = $cp->lastInsertId() ;

                if( $chargable != 0 ) {

                    //Update cc-charge // Set charge to zero if credit cart is not choosen.
                    $dataCiu = [
                        'Charges' => $charge,
                    ] ;
                    $cp->update($dataCiu, " Customer_Payment_ID='$paymentId' ") ;
                }
            }
        }
        GUtils::setSuccess('Payment details saved successfully.') ;
        Model::commit() ;
    }
}
