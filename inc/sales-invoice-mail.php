<?php
include_once 'vendor/autoload.php';

include_once __DIR__ . '/../models/customer_invoice.php';

use PHPMailer\PHPMailer\PHPMailer ;

//set default file prefix.
$isInvoiceFlag = false ;
if ($FileTitlePrefix == 'invoice') {
    $filePrefix = 'GST-Invoice' ;
    $template_code = 'GST_INVOICE' ;
    $isInvoiceFlag = true ;
}
else if ($FileTitlePrefix == 'receipt') {
    $filePrefix = 'GST-Receipt' ;
    $template_code = 'GST_RECEIPT' ;
}

$invoiceNumSet = [] ;

//even if there is one invoice convert it to an array. 
if( isset($__REQUEST['cbInvoice']) ) {
    $invoiceNumSet = $__REQUEST['cbInvoice'] ;
}
ob_start() ;
if( ! isset($invoiceNumSet) || empty($invoiceNumSet) ) {
    $invoiceNumSet = [ $__REQUEST['invid'] ] ;
}
//itereate with invoice and mail invoce/receipt.
$p = 0 ;
foreach( $invoiceNumSet as $invoiceId ) {

    try {

        $PDF_OUTPUT = 'S';
        $PDF_HEADER = true ;
        $attachmentData    = include __DIR__ . '/sales-invoice-pdf.php';

        //Attachments
        $attachmentName = $filePrefix. '-' . $invoiceId . '.pdf' ;
        $params = [] ;
        
        $ciRead = new CustomerInvoice() ;
        $ciDetails = $ciRead->withContactDetails($invoiceId) ;
        
        if( ! is_array($ciDetails) || ! isset($ciDetails['email']) ) {
            GUtils::setWarning('Email not send to some customers.') ;
            continue;
        }
        $to = [$ciDetails['email']] ;
        
        if( $isInvoiceFlag ) {
            $ciUpdate = new CustomerInvoice() ;
            $ciUpdate->update( ['Invoice_Sent' => Model::sqlNoQuote('Invoice_Sent+1') ], ['Customer_Invoice_Num' => $invoiceId]) ;
        }
        
        if( isset($MESSAGE) ) {
            GUtils::sendMail($to, $SUBJECT, $MESSAGE, $attachmentName, $attachmentData) ;
        }
        else {
            GUtils::sendTemplateMailByCode($to, $template_code, $params, $attachmentName, $attachmentData) ;
        }
        
    } catch (Exception $e) {
        
    }
}