
<script type="text/javascript">
    
    function sendCancelInvoiceHandler(obj) {
        
        $("#showCancelInvoice").modal("show");
        
        $("#showCancelInvoice .text-success").html('') ;
        $("#showCancelInvoice .text-danger").html('') ;
        $('.gst-cancel-invoice-id').val( $(obj).data('id') ) ;
        $('.gst-cancel-invoice-class').html( $(obj).data('name') ) ;
        $('.gst-cancel-invoice-charge').html( $(obj).data('cancel-amount') ) ;
        $('.gst-cancel-invoice-refund').html( $(obj).data('cancel-refund') + '  <br/><i>' + $(obj).data('cancel-outcome') + '</i>'  ) ;
        $('.gst-cancel-invoice-reason').val( '' ) ;
    }
    
    function sendReminderHandler(obj) {
        
        $("#showSendReminder").modal("show");
        
        $('.gst-reminder-id').val( $(obj).data('id') ) ;
        $('.gst-reminder-class').html( $(obj).data('name') ) ;
        $('.gst-reminder-email').val( $(obj).data('email') ) ;
        $('.gst-reminder-subject').val( '' ) ;
        $('.gst-reminder-message').val( '' ) ;
    }
    
    function batchSubmit(obj) {
        var count = $('.gst-invoice-cb:checked').length ;
        if( count > 0 ) {
            
            obj.form.target = '_blank' ;
            if( obj.value == 'S' ) {
                msg = "Are you sure you want to sent " + count + " invoice(s) ?" ;
            }
            else if( obj.value == 'P' ) {
                msg = "Are you sure you want to print " + count + " invoice(s) ?" ;
            }
            else {
                return ;
            }
            showConfirm( "javascript:saleInvoiceListForm.submit();", "Yes", msg, "Please Confirm", "modal-md") ;
//            obj.form.submit();
        }
    }
    
    function batchBillSubmit(obj) {
        var count = $('.gst-invoice-cb:checked').length ;
        if( count > 0 ) {
            
            obj.form.target = '_blank' ;
            if( obj.value == 'S' ) {
                msg = "Are you sure you want to sent " + count + " bill(s) ?" ;
            }
            else if( obj.value == 'P' ) {
                msg = "Are you sure you want to print " + count + " bill(s) ?" ;
            }
            else {
                return ;
            }
            showConfirm( "javascript:saleInvoiceListForm.submit();", "Yes", msg, "Please Confirm", "modal-md") ;
//            obj.form.submit();
        }
    }
</script>
<?php
global $GLOBAL_SCRIPT ;
$GLOBAL_SCRIPT .= "bindCheckAll('.gst-invoice-cb-parent', '.gst-invoice-cb-children');" ;
?>