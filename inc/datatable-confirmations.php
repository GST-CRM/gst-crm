<?php
	//include connection file 
	include "../config.php";
    include_once __DIR__ . '/../models/acl_permission.php';

    $contactEditAllowed = (new AclPermission())->isActionAllowed('Index', 'contact-edit.php') ;
    
	// initilize all variable
	$params = $columns = $totalRecords = $data = array();

	$params = $_REQUEST;

	//define index of column
	$columns = array( 
		0 => 'contactid', 
		1 => 'fname',
		2 => 'mname', 
		3 => 'lname', 
		4 => 'tourid', 
		5 => 'hisownticket',
		6 => 'Same_Itinerary',
		7 => 'Ticket_Number',
		8 => 'Confirmation_Number',
		9 => 'Confirmation'
	);

	$where = $sqlTot = $sqlRec = "";

	// check search value exist
	if( !empty($params['search']['value']) ) {   
		$theSearchText = $params['search']['value'];
		$theSearchText2 = str_replace(" ","%",$theSearchText);
		$where .= " AND concat(co.fname,co.mname,co.lname) LIKE '%".$theSearchText2."%'";
	}
	//Advanced Filters for the Customer Accounts List Page
	$GroupID = $__GET['tourid'];

	// getting total number records without any search
	$sql = "SELECT 
		co.fname,
		co.mname,
        co.lname, 
        ca.contactid, 
        ca.tourid,
        ca.hisownticket,
        ca.Same_Itinerary,
        ca.Ticket_Number,
        ca.Confirmation_Number,
		pro.Confirmation
FROM customer_account ca
JOIN contacts co
ON co.id=ca.contactid
JOIN groups gro 
ON gro.tourid=ca.tourid
LEFT JOIN products pro 
ON pro.id=gro.airline_productid
WHERE ca.tourid=$GroupID AND ca.status=1 ";
	//$sql = "SELECT id,title,fname FROM contacts ";
	$sqlTot .= $sql;
	$sqlRec .= $sql;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where;
		$sqlRec .= $where;
	}


 	//$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']." LIMIT ".$params['start']." ,".$params['length']." ";
 	//$sqlRec .=  " ORDER BY cus.updatetime DESC LIMIT ".$params['start']." ,".$params['length']." ";

	$queryTot = mysqli_query($db, $sqlTot) or die("database error:". mysqli_error($db));


	$totalRecords = mysqli_num_rows($queryTot);

	$queryRecords = mysqli_query($db, $sqlRec) or die($sqlRec);

	//iterate on results row and create new index array of data
	//while( $row = mysqli_fetch_row($queryRecords) ) { 
	while( $row = mysqli_fetch_array($queryRecords) ) { 
		//$data[] = $row;
		
		//Column #1
		$OneInput[0] = '<input name="TicketID[]" value="' . $row["contactid"] . '" type="checkbox" class="gst-ticket-cb-children gst-ticket-cb"/>';
		//Column #2		
			ob_start();
            if( $contactEditAllowed ) {
            ?>
			<a style="font-size:14px; color:blue;" href="contacts-edit.php?id=<?php echo $row['contactid']; ?>&action=edit&usertype=customer"><?php echo $row['fname']." ".$row['mname']." ".$row['lname']; ?></a>
			<?php
            }
            else {
                echo $row['fname']." ".$row['mname']." ".$row['lname'];
            }
		$OneInput[1] = ob_get_clean();
		//Column #3
		if($row['Ticket_Number']=="" OR $row['Ticket_Number']==NULL) { $OneInput[2] = "N/A"; } else { $OneInput[2] = $row['Ticket_Number']; };
		//Column #4
		if($row['hisownticket'] == 1 ) { if($row['Confirmation_Number']=="" OR $row['Confirmation_Number']==NULL) { $OneInput[3] = "N/A"; } else { $OneInput[3] = $row['Confirmation_Number']; } } else { $OneInput[3] = $row['Confirmation']; };
		$data[] = $OneInput;
		
		
		//echo $OneInput[1];
		//echo "<br />";
	}	

	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
	
	//print_r($data);
?>
