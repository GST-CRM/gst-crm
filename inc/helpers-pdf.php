<?php

require_once( __DIR__ . '/../files/pdf/tcpdf.php');

class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
//        $img_file = K_PATH_IMAGES.'image_demo.jpg';
//        $this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
        
        $img_file = __DIR__ . '/../files/assets/images/letter_head2.jpg';
        $this->Image($img_file, 0, 0, 215, 0, '', '', '', true, 300, 'C', false, false, 0, false, false, false);

        // restore auto-page-break status
        $this->SetAutoPageBreak($auto_page_break, 25);
        // set the starting point for the page content
        $this->setPageMark();
    }
}