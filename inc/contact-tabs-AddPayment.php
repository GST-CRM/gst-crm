<form  id="contact16" name="contact16" method="post" action="inc/contact-functions.php">
    <div class="row">
		<div class="form-group col-sm-12">
			<h3>Receive Payment</h3>
		</div>
		<div class="col-sm-4">
			<label class="float-label">Date of Payment</label>
			<input type="text" name="SalesOrderNumID" value="<?php echo $SalesOrderID; ?>" class="form-control" hidden>
			<input type="text" name="SalesOrderCustomerID" value="<?php echo $SalesData["Customer_Account_Customer_ID"]; ?>" class="form-control" hidden>
			<input type="text" name="SalesOrderCustomerName" value="<?php echo $SalesData['title']." ".$SalesData['fname']." ".$SalesData['lname'];?>" class="form-control" hidden>
			<input type="date" name="PaymentDate" class="form-control" required>
		</div>
		<div class="col-sm-4">
			<label class="float-label">Amount of Payment</label>
			<div class="input-group">
				<span class="input-group-prepend">
					<label class="input-group-text">$</label>
				</span>
				<input name="PaymentAmount" type="text" class="form-control" required>
			</div>
		</div>
		<div class="col-sm-4">
			<label class="float-label">Method of Payment</label>
			<select name="PaymentMethod" class="form-control form-control-default" required>
				<option value="Bank Transfer">Bank Transfer</option>
				<option value="Cash">Cash</option>
				<option value="Check">Check</option>
				<option value="Credit Card">Credit Card</option>
				<option value="Online Payment">Online Payment</option>
				<option value="Refund">Refund</option>
				<option value="Discount">Discount</option>
				<option value="Complimentary">Free Complimentary</option>
			</select>
		</div>
		<div class="col-sm-12">
			<label class="float-label">Comments / Refrences</label>
			<input type="text" name="PaymentComments" class="form-control">
		</div>
		<div class="col-sm-12"><br />
			<button type="submit" name="AddPaymentSubmit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Add</button>
			<button type="reset" class="btn waves-effect waves-light btn-inverse" data-dismiss="modal"><i class="fas fa-ban"></i>Cancel</button>
		</div>
	</div>
</form>
