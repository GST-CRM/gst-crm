<script type="text/javascript">
function admSelectCheck(nameSelect)
{
    if(nameSelect){
        admOptionValue = document.getElementById("admOption").value;
        if(admOptionValue == nameSelect.value){
            document.getElementById("affliationtypenew").style.display = "block";
            document.getElementById("affliationtype").style.display = "none";
        }
        else{
            document.getElementById("affliationtypenew").style.display = "none";
        }
    }
    else{
        document.getElementById("affliationtypenew").style.display = "none";
    }
}
    
    
$("#TitleContactSelect").change(function(){
    $(this).find("option:selected").each(function(){
        var optionValue = $(this).attr("value");
        if(optionValue == "Dr." || optionValue == "Rev." || optionValue == "Father" || optionValue == "Pastor" || optionValue == "Deacon"){
            document.getElementById("DenominationSelect").style.display = "block";
        } else {
            document.getElementById("DenominationSelect").style.display = "none";
        }
    });
}).change();

    
function DenominationSelect(nameSelect)
{
    if(nameSelect){
        admOptionValue = document.getElementById("admOption").value;
        if(admOptionValue == nameSelect.value){
            document.getElementById("DenominationNew").style.display = "block";
            document.getElementById("DenominationSelect").style.display = "none";
        }
        else{
            document.getElementById("DenominationNew").style.display = "none";
        }
    }
    else{
        document.getElementById("DenominationNew").style.display = "none";
    }
}

function SupplierTypeSelect(nameSelect)
{
    if(nameSelect){
        admOptionValue = document.getElementById("admOption").value;
        if(admOptionValue == nameSelect.value){
            document.getElementById("suppliertypenew").style.display = "block";
            document.getElementById("suppliertype").style.display = "none";
        }
        else{
            document.getElementById("suppliertypenew").style.display = "none";
        }
    }
    else{
        document.getElementById("suppliertypenew").style.display = "none";
    }
}

function SupPaymentTypeSelect(nameSelect)
{
    if(nameSelect){
        admOptionValue = document.getElementById("admOption").value;
        if(admOptionValue == nameSelect.value){
            document.getElementById("paymenttermsnew").style.display = "block";
            document.getElementById("paymentterms").style.display = "none";
        }
        else{
            document.getElementById("paymenttermsnew").style.display = "none";
        }
    }
    else{
        document.getElementById("paymenttermsnew").style.display = "none";
    }
}

function RoomTypeSelect(nameSelect) {
    if(nameSelect){
        admOptionValue0 = document.getElementById("SingleOption").value;
        admOptionValue1 = document.getElementById("DoubleOption").value;
        admOptionValue2 = document.getElementById("TripleOption").value;
        admOptionValue3 = document.getElementById("TwinOption").value;
        if(admOptionValue1 == nameSelect.value){
            document.getElementById("roomtypedouble").style.display = "block";
			document.getElementById("roomtypetriple").style.display = "none";
        } else if(admOptionValue3 == nameSelect.value){
            document.getElementById("roomtypedouble").style.display = "block";
			document.getElementById("roomtypetriple").style.display = "none";
        } else if (admOptionValue2 == nameSelect.value){
            document.getElementById("roomtypedouble").style.display = "block";
            document.getElementById("roomtypetriple").style.display = "block";
        } else if (admOptionValue0 == nameSelect.value){
			document.getElementById("roomtypedouble").style.display = "none";
			document.getElementById("roomtypetriple").style.display = "none";
        }
    } else{
        document.getElementById("roomtypedouble").style.display = "none";
        document.getElementById("roomtypetriple").style.display = "none";
    }
}

$('#AirUpgrade').change(function() {
    var opval = $(this).val();
    if(opval=="Add New"){
        $('#AirUpgradeWindow').modal("show");
    }
});

$('#LandUpgrade').change(function() {
    var opval = $(this).val();
    if(opval=="Add New"){
        $('#LandUpgradeWindow').modal("show");
    }
});

$(function () {
        $("#WorkAddressButton").click(function () {
            if ($(this).is(":checked")) {
                $("#WorkAddressBlock").show("slow");
            } else {
                $("#WorkAddressBlock").hide("slow");
            }
        });
    });
	
	
$(function () {
        $("#PassportPending").click(function () {
            if ($(this).is(":checked")) {
                $("#passportID").val("PASSPORT PENDING");
                $("#expirydateID").val("0000-00-00");
            } else {
                $("#passportID").val("");
            }
        });
    });

</script>
