<h4 class="sub-title">Agent Info</h4>

    <?php 
    include_once __DIR__ . '/../models/groups.php';
    
    $agentsInfo = (new Groups())->getGroupAgentsNameList($tourid) ;
    foreach ($agentsInfo as $agentInfo) { ?>
        <div class="row " >
            <div class="form-group form-default form-static-label col-sm-4 gst-readonly-select2">
                <label class="float-label">Agent</label>
                <select name="selAgents[<?php echo $agentInfo['id'];?>]" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple" required>
                    <?php
                    $allAgentRows = GDb::fetchRowSet("SELECT fname, mname, lname, contactid FROM agents a "
                        . "INNER JOIN contacts c ON c.id = a.contactid WHERE a.status=1 ") ;
                    foreach( $allAgentRows as $agentOne ) {
                        $selected= '' ;
                        if( $agentOne['contactid'] == $agentInfo['agent_id'] ) {
                            $selected = 'selected="selected"' ;
                        }
                        echo "<option $selected value='" . $agentOne['contactid'] . "'>" . $agentOne['fname'] . " " . $agentOne['lname'] . "</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="form-group col-sm-4">
                <label class="float-label">Commission Type</label>
                <select name="commissiontype[<?php echo $agentInfo['id'];?>]" class="form-control gst-agent-commision-fields">
                    <option value="Per Person" <?php if ($agentInfo["commissiontype"] == "Per Person") { echo "selected"; } ?> >Per Person</option>
                    <option value="Per Trip" <?php if ($agentInfo["commissiontype"] == "Per Trip") { echo "selected"; } ?> >Per Trip</option>
                </select>
            </div>
            <div class="form-group col-sm-4">
                <label class="float-label">Commission Value</label>
                <div class="dollar"><input type="text" class="form-control  gst-agent-commision-fields" name="commissionvalue[<?php echo $agentInfo['id'];?>]" value="<?php echo $agentInfo["commissionvalue"]; ?>" /></div>
            </div>
        </div>

<?php } ?>
