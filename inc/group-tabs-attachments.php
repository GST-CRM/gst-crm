<?php if($_GET['action'] == "docedit") { 

	$fileid = $_GET['fileid'];
	$con17 = new mysqli($servername, $username, $password, $dbname);
	if ($con17->connect_error) {die("Connection failed: " . $con17->connect_error);} 
	$sql17 = "SELECT * FROM groups_media WHERE id=$fileid";
	$result17 = $con17->query($sql17);
	$docdata = $result17->fetch_assoc();
	$con17->close();

?>
<form name="contact7" id="contact7" action="inc/contact-functions.php" method="POST" class="row">
	<div class="col-sm-12">
		<h6>Edit the Attachment</h6>
		<br />
	</div>
	<div class="col-sm-2">
		<label class="float-label">Document Type</label>
		<select name="attachmenttype" class="form-control form-control-default">
			<option value="payment" <?php if($docdata['mediatype'] == "payment") { echo "selected";} ?>>Payment Voucher</option>
			<option value="airline" <?php if($docdata['mediatype'] == "airline") { echo "selected";} ?>>Airline Document</option>
			<option value="land" <?php if($docdata['mediatype'] == "land") { echo "selected";} ?>>Land Operation</option>
			<option value="Brochure" <?php if($docdata['mediatype'] == "Brochure") { echo "selected";} ?>>Brochure</option>
			<option value="Proposal" <?php if($docdata['mediatype'] == "Proposal") { echo "selected";} ?>>Proposal</option>
			<option value="other" <?php if($docdata['mediatype'] == "other") { echo "selected";} ?>>Other</option>
		</select>
	</div>
	<div class="col-sm-4">
		<label class="float-label">Document Name</label>
		<input type="text" name="documentname" value="<?php echo $docdata['name']; ?>" class="form-control" required>
		<input type="text" name="documentid" value="<?php echo $docdata['id']; ?>" class="form-control" hidden>
	</div>
	<div class="col-sm-6">
		<label class="float-label">Document Description</label>
		<input type="text" name="description" value="<?php echo $docdata['mediadescription']; ?>" class="form-control">
	</div>
	<div class="col-sm-12" style="text-align:center;">
	<br />
	<?php $filetype = array_pop(explode('.', $docdata['mediaurl']));
	if($filetype == "pdf" OR $filetype == "doc" OR $filetype == "docx") {?>
		<iframe src="https://docs.google.com/gview?url=<?php echo $crm_path . "uploads/" . $docdata['mediaurl']; ?>&embedded=true" style="width:100%; height:400px;" frameborder="1"></iframe>
	<?php } else { ?>
		<img src="uploads/<?php echo $docdata['mediaurl']; ?>" alt="<?php echo $docdata['name']; ?>" style="width:auto;height:400px;margin:auto;" />
	<?php } ?>
	</div>
	<div class="col-sm-12"><br /><br />
            <?php if( AclPermission::actionAllowed('GroupSave') ) { ?>
		<button type="submit" name="updatedocument" value="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Update</button>
            <?php } ?>
		<a href="groups-edit.php?id=<?php echo $tourid; ?>&action=edit&tab=attachments" class="btn waves-effect waves-light btn-inverse" style="margin-right:20px;"><i class="fas fa-ban"></i>Go back to the attachments list</a>
		<?php if( AclPermission::actionAllowed('GroupSave') ) { ?>
                <a href="groups-edit.php?id=<?php echo $tourid; ?>&tab=attachments&fileid=<?php echo $docdata["id"]; ?>&action=docdelete" onclick="return confirm('Are you sure?')" class="btn waves-effect waves-light btn-danger" style="margin-right:20px;"><i class="fas fa-trash"></i>Delete</a>
                <?php } ?>
	</div>
</form>
<?php } else { ?>
<h5>Attachments</h5>
<br />
<table class="table table-hover table-striped table-bordered">
    <thead>
        <tr role="row">
            <th style="width:10%;">Type</th>
            <th style="width:25%;">Name</th>
            <th style="width:40%;">Description</th>
            <th style="width:10%;">Upload Time</th>
            <th style="width:15%;">Actions</th>
        </tr>
    </thead>
    <tbody>
	<?php
	$sql = "select * from groups_media WHERE tourid='$tourid'"; $result = $db->query($sql);
	if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
        <tr>
			<td><?php echo $row["mediatype"]; ?></td>
			<td><a href="groups-edit.php?id=<?php echo $tourid; ?>&tab=attachments&fileid=<?php echo $row["id"]; ?>&action=docedit" style="color: #0000EE;"><?php echo $row["name"]; ?></a></td>
			<td><span data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $row['mediadescription']; ?>"><?php echo substr($row['mediadescription'], 0, 20)."..."; ?></span></td>
			<td><?php echo $row["createtime"]; ?></td>
			<td style="text-align:center;">
				<a href="uploads/<?php echo $row["mediaurl"]; ?>" download><img src="files/assets/images/download.png" style="width:25px;height:auto;" alt="download" /></a>
                <?php if( AclPermission::actionAllowed('GroupSave') ) { ?>
				<a href="groups-edit.php?id=<?php echo $tourid; ?>&tab=attachments&fileid=<?php echo $row["id"]; ?>&action=docdelete" onclick="return confirm('Are you sure?')"><img src="files/assets/images/remove.png" style="margin-left:5px;width:22px;height:auto;" alt="remove" /></a>
                <?php } ?>
			</td>
        </tr>
	<?php }} else { ?>
        <tr>
			<td colspan="5" style="text-align:center;">There are no documents or attachments for this tour yet.</td>
        </tr>
	<?php } ?>
    </tbody>
</table>
<?php if( AclPermission::actionAllowed('GroupSave') ) { ?>
<br /><br />
<form name="contact6" id="contact6" action="inc/group-upload-functions.php" method="POST" class="dropzone row" enctype="multipart/form-data">
	<div class="col-sm-12">
		<h6>Upload A New Document</h6>
		<br />
	</div>
	<div class="col-sm-2" style="display:none;">
		<label class="float-label">Tour ID#</label>
		<input type="text" name="touridupload" value="<?php echo $tourid; ?>" class="form-control" required hidden>
	</div>
	<div class="col-sm-3">
		<label class="float-label">Document Type</label>
		<select name="attachmenttype" class="form-control form-control-default">
			<option value="opt1" disabled selected></option>
			<option value="payment">Payment Voucher</option>
			<option value="airline">Airline Document</option>
			<option value="land">Land Operation</option>
			<option value="Brochure">Brochure</option>
			<option value="Proposal">Proposal</option>
			<option value="other">Other</option>
		</select>
	</div>
	<div class="col-sm-4">
		<label class="float-label">Document Name</label>
		<input type="text" name="documentname" class="form-control" required>
	</div>
	<div class="col-sm-5">
		<label class="float-label">Doccument Description</label>
		<input type="text" name="description" class="form-control">
	</div>
	<div class="col-sm-12">
		<br /><label class="float-label" for="Group_Attachment">Select the document to upload</label>
        <div class="Attachment_Box bg-white">
            <input type="file" id="Group_Attachment" name="fileToUpload" class="Attachment_Input" >
            <p id="Group_Attachment_Text" class="Attachment_Text">Drag the file here or click in this area.</p>
        </div>
		<br /><input type="submit" value="Upload" class="btn btn-success" name="attachmentsubmit" data-toggle="modal" data-target="#uploadingmodal" onclick="uploaddisablealert()">
	</div>
</form>
<script>
//This is to make the attachment box update its content when a file is selected
$(document).ready(function(){
  $('#Group_Attachment').change(function () {
    $('#Group_Attachment_Text').text("A file has been selected");
  });
});
</script>
<?php } ?>
<?php } ?>