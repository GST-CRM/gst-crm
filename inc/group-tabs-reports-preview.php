<?php 
$previewHtml 	= "";
if (!empty($_REQUEST['type']))
{
	$id 		= !empty($_REQUEST['id'])?$_REQUEST['id']:"";
	$tourid 	= !empty($_REQUEST['groupid'])?$_REQUEST['groupid']:"";
	$tourname 	= !empty($_REQUEST['tourname'])?$_REQUEST['tourname']:"";
	switch ($_REQUEST['type'])
	{
		case "name_badges" :
			$modalHtml = include "preview-name-badges.php";
			break;

		case "shipping_labels" :
			$modalHtml = include "preview-shipping-label.php";
			break;
			
		case "emergency_contacts" :
			$modalHtml = include "preview-emergency-contacts.php";
			break;
			
		case "manifest" :
			$modalHtml = include "preview-manifest.php";
			break;
			
		case "rooming" :
			$modalHtml = include "preview-rooming.php";
			break;
			
		case "tour_leader" :
			$modalHtml = include "preview-tour-leader.php";
			break;
			
		case "tour_flights" :
			$modalHtml = include "preview-tour-flights.php";
			break;

	}
}
$previewHtml;
exit;
?>