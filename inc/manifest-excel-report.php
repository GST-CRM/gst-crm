<?php

$filetitle   = "Manifest Report";
$groupname   = $groupName ;
$groupid     = $groupId;
$filesubject = "Manifest Report for the Group #$groupid";
$fileauthor  = "Good Shepherd Travel";
$filecompany = "Element Media";
$filedesc    = "Manifest Report for the Group #$groupid";

include __DIR__ . '/../excel-manifest.php';
