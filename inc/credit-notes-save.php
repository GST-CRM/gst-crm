<?php

include_once __DIR__ . '/../models/customer_credit_note.php' ;
include_once __DIR__ . '/../models/customer_invoice.php' ;
include_once __DIR__ . '/../models/transactions.php' ;

//Common Values
$refundDate = $__REQUEST['credit_date'] ;
$refundFrom = $__REQUEST['credit_from'] ;
$FullDiscount = (isset($__REQUEST['TourLeaderDiscount']) ? $__REQUEST['TourLeaderDiscount'] : 0) ;


$cnObj = new CustomerCreditNote() ;
$tobj = new Transactions() ;
$ciObj = new CustomerInvoice() ;

foreach( $__REQUEST['cbNote'] AS $noteId ) {
    
    $amount = $__REQUEST['credit_amount'][$noteId] ;
    $reason = $__REQUEST['credit_reason'][$noteId] ;
    $invoiceId = $__REQUEST['cbInvoice'][$noteId] ;
    
    if( ! $amount ) {
        return ;
    }
    
    $ciData = $ciObj->get(['Customer_Invoice_Num' => $invoiceId]) ;
    $customerId = $ciData['Customer_Account_Customer_ID'] ;    
    
    //mark transaction
    $tdata = array(
        'Transaction_Type' => 999,
        'Chart_of_Accounts_GL_Account_N' => '9999',
        'Transaction_Amount' => $amount,
        'Credit_Note_Date' => $refundDate,
        'Customer_Account _Customer_ID' => $customerId,
        'Supplier_Supplier_ID' => '0',
        'Add_Date' => Model::sqlNoQuote('now()'),
        'Mod_Date' => Model::sqlNoQuote('now()'),
    ) ;    
    //credit data
    $credit = array(
        'Status'                => 1,
        'Credit_Amount'         => $amount,
        'Credit_Reason'         => $reason,
        'Full_Discount'     	    => $FullDiscount,
        'Credit_Note_Date' => $refundDate,
        'Customer_Account_Customer_ID' => $customerId,
        'Customer_Invoice_Num' => $invoiceId,
        'Mod_Date'              => Model::sqlNoQuote('now()'),
        );
    $one = $cnObj->get(['Note_ID' => $noteId]) ;
    //TODO; Prevent Editing for now $one
//    update note
    if(is_array($one) ) {
        $tId = $one['Transaction_Type_Transaction_ID'] ;
        $tobj->update($tdata, ['Transaction_ID' => $tId]) ;
        
        $credit['Transaction_Type_Transaction_ID'] = $tId ;
        $cnObj->update($credit, ['Note_ID' => $noteId]) ;
    }
    else {
        $tobj->insert($tdata) ;
        $credit['Transaction_Type_Transaction_ID'] = $tobj->lastInsertId() ;        
        $cnObj->insert($credit) ;
    }
}

GUtils::setSuccess('Credit note saved.') ;
