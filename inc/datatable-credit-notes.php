<?php
//include connection file 
include __DIR__ . "/../config.php";

include_once __DIR__ . '/../models/acl_permission.php';

// { Acl Condition
$GroupID       = AclPermission::userGroup();
$contactId  = AclPermission::userId() ;
$ACL_CONDITION = " ";

if( AclPermission::roleFlags() & AclRole::$FLAG_ONLY_MINE ) {
    $ACL_CONDITION = " AND ca.contactid = $contactId " ;
}
// }

//define index of column
$columns = array( 
    0 => 'Customer_Invoice_Num',
    1 => 'fname', 
    2 => 'Credit_Note_Date', 
    3 => 'Credit_Amount', 
    4 => 'Credit_Reason', 
);

$params = $__REQUEST;
$where  = '';
// check search value exist
if (!empty($params['search']['value'])) {
    $where .= " AND ";
    $where .= " ( cn.Customer_Invoice_Num LIKE '" . $params['search']['value'] . "%' ";
    $where .= " OR c.fname LIKE '" . $params['search']['value'] . "%' ";
    $where .= " OR c.mname LIKE '" . $params['search']['value'] . "%' ";
    $where .= " OR c.lname LIKE '" . $params['search']['value'] . "%' ";
    $where .= " OR cn.Credit_Amount LIKE '" . $params['search']['value'] . "%')";
}

if( $params['order'][0]['column'] == 0 )  {
    $orderBy = " ORDER BY Note_ID DESC " ;
}
else {
    $orderBy = " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']."" ;
}

$sqlCount = "SELECT COUNT(*) as total FROM customer_credit_note AS cn
        INNER JOIN customer_invoice ci ON ci.Customer_Invoice_Num = cn.Customer_Invoice_Num 
        INNER JOIN customer_account AS ca ON ca.contactid = cn.Customer_Account_Customer_ID AND ca.tourid = ci.Group_ID
        INNER JOIN contacts AS c ON c.id = ca.contactid 
        WHERE 1
        $ACL_CONDITION    
        $where
        ";

$sql = "SELECT c.fname, c.lname, c.mname, ca.contactid, ci.Customer_Invoice_Num, 
        cn.Note_ID, cn.Credit_Amount, cn.Credit_Reason, cn.Credit_Note_Date, cn.Full_Discount, cn.Status FROM customer_credit_note AS cn
        INNER JOIN customer_invoice ci ON ci.Customer_Invoice_Num = cn.Customer_Invoice_Num 
        INNER JOIN customer_account AS ca ON ca.contactid = cn.Customer_Account_Customer_ID AND ca.tourid = ci.Group_ID
        INNER JOIN contacts AS c ON c.id = ca.contactid 
        WHERE 1
        $ACL_CONDITION    
        $where
        $orderBy LIMIT " . $params['start'] . " ," . $params['length'] . "";


$data         = GDb::fetchRowSet($sql);
$totalRecords = GDb::fetchScalar($sqlCount);

$output = [];
foreach ($data as $row) {
    $oneOutput  = [];
    $name       = $row['fname'] . ' ' . $row['mname'] . ' ' . $row['lname'];
    $nameAppend = ((strlen(trim($name)) > 0) ? $name : '');

    //name & email
    $name_email = '<b>' . $nameAppend . (isset($row['email']) ? ' (' . $row['email'] . ')' : '') . '</b>';

    $oneOutput[0] = '<a style="color: #0000EE;" href="sales-invoce-add.php?invoice=' . $row["Customer_Invoice_Num"] . '">' . $row["Customer_Invoice_Num"] . '</a>';

    ob_start();
    if ($row["contactid"]) {
        ?>
        <a style="color: #0000EE;" href='<?php echo 'contacts-edit.php?id=' . $row["contactid"] . '&action=edit&usertype=sales'; ?>'>
            <?php
        }
        //print text
        echo $nameAppend;

        if ($row["contactid"]) {
            ?>
        </a>
    <?php
    }
    $oneOutput[1] = ob_get_clean();
    $oneOutput[2] = GUtils::clientDate($row["Credit_Note_Date"]);
    $oneOutput[3] = $row["Credit_Reason"];
    $oneOutput[4] = GUtils::formatMoney($row["Credit_Amount"]);

    ob_start();
    ?>
    <div style="width: 150px;" class="btn-group gst-btns ">
    <?php if ($row['Status']) { ?>                                               
            <button onclick="window.location.href = 'credit_notes_add.php?noteid=<?php echo $row["Note_ID"]; ?>';"
                    type="button" class="btn btn-info">Edit/View
            </button>
            <button type="button" class="btn btn-info dropdown-toggle"
                    data-toggle="dropdown">
                <span class="caret"></span>
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <ul class="dropdown-menu" role="menu" style="white-space: nowrap">
                <li><a href="javascript:void(0);" onclick = 'deleteConfirm("credit_notes.php?action=delete&noteid=<?php echo $row["Note_ID"]; ?>", "Delete")' >Delete</a></li>
                <li><a href="javascript:void(0);" onclick = 'deleteConfirm("credit_notes.php?action=void&noteid=<?php echo $row["Note_ID"]; ?>", "Void")' >Void</a></li>
            </ul>
        <?php } else {
            ?>
            <button onclick = 'deleteConfirm("credit_notes.php?action=delete&noteid=<?php echo $row["Note_ID"]; ?>")' type="button" class="btn btn-danger">
                Delete
            </button>
            <?php }
        ?>
    </div>
    <?php
    $oneOutput[5] = ob_get_clean();
    $output[]     = $oneOutput;
}

$json_data = array(
    "draw"            => intval($params['draw']),
    "recordsTotal"    => intval($totalRecords),
    "recordsFiltered" => intval($totalRecords),
    "data"            => $output   // total data array
);

echo json_encode($json_data);  // send data as json format
