<?php

// Include the main TCPDF library (search for installation path).
require_once('files/pdf/tcpdf.php');
include_once __DIR__ . '/../models/groups.php' ;
include_once __DIR__ . '/../models/customer_payment.php' ;
include_once __DIR__ . '/../library/MYPDF2.php' ;

/*
$report_title = "";
$report_slug = "";
$report_slug = $_GET['report_slug'];*/
$report_title = str_replace("-"," ",$report_slug);


// create new PDF document
$pdf = new MYPDF2(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Element Media');
$pdf->SetTitle($report_title);
$pdf->SetSubject($report_title);
$pdf->SetKeywords('');

// remove default header/footer
$PDF_HEADER = isset($PDF_HEADER) ? $PDF_HEADER : false ;
$pdf->setPrintHeader($PDF_HEADER);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/../lang/eng.php')) {
    require_once(dirname(__FILE__).'/../lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
$pdf->SetMargins(10, 12, 10);

// set font
$pdf->SetFont('Helvetica', '', 10);
    // add a page
$pdf->AddPage('L','A3');

// set margins
$pdf->setPageMark() ;



ob_start();

?>
<style>
th{
	height:40px;
}
td{
	border:1px solid #000;
	min-height:25px;
	line-height:20px;
}
</style>
<?php

$tourid = $_REQUEST['id'];
$groupid = $tourid;

$sql = "SELECT tourname FROM groups WHERE tourid=$tourid";
$queryGroup = mysqli_query($db, $sql) or die("error to fetch group name");
$groupRow = mysqli_fetch_row($queryGroup);
$groupName = $groupRow[0];

$query          = (new Groups())->getRoomingListQuery($tourid) ;
$result         = $db->query($query); 

$table_headings = array();
$before_table = '<h4>'.$groupName.'</h4>' ;

?>

<?php echo $before_table; ?>
	<table width="100%" align="center">
        <tr>
            <th style="background-color:#ccc;line-height:34px;">Room</th>
            <th style="background-color:#ccc;line-height:34px;">Room Type</th>
            <th style="background-color:#ccc;line-height:34px;">Person #1</th>
            <th style="background-color:#ccc;line-height:34px;">Person #2</th>
            <th style="background-color:#ccc;line-height:34px;">Person #3</th>
            <th style="background-color:#ccc;line-height:34px;">Need Transfer?</th>
            <th style="background-color:#ccc;line-height:34px;">Need Extension?</th>
            <th style="background-color:#ccc;line-height:34px;">Special Land Requests</th>
            <th style="background-color:#ccc;line-height:34px;">Meals Requests</th>
            <th style="background-color:#ccc;line-height:34px;">Allergies Requests</th>
        </tr>
        <?php
        if ($result->num_rows > 0) 
        { 
            $i = 1;
            while($row = $result->fetch_assoc()) 
            { 
                $roomType           = $row['roomtype'];       
                $person1            = $row['title']." ".$row['fname']." ".$row['mname']." ".$row['lname'];
                $person2ID          = $row['Roommate1ID'];
                if($person2ID != NULL) {
                    //To get the product details if this roommate has transfer product
                    $Person2TransferSQL = 	"SELECT ca.Transfer, pro.description FROM products pro
                                            JOIN customer_account ca ON ca.contactid=$person2ID AND ca.tourid=$groupid
                                            WHERE id=ca.Transfer_Product_ID;";
                    $Person2TransferResult = $db->query($Person2TransferSQL);
                    $Person2TransferData = $Person2TransferResult->fetch_assoc();
                    if($Person2TransferData['Transfer']==1) {
                        $Person2Transfer = $Person2TransferData['description'];
                    } else {
                        $Person2Transfer = NULL;
                    };
                    //To get the product details if this roommate has extension product
                    $Person2ExtSQL = 	"SELECT ca.Extension, pro.description FROM products pro
                                        JOIN customer_account ca ON ca.contactid=$person2ID AND ca.tourid=$groupid
                                        WHERE id=ca.Extension_Product_ID;";
                    $Person2ExtResult = $db->query($Person2ExtSQL);
                    $Person2ExtData = $Person2ExtResult->fetch_assoc();
                    if($Person2ExtData['Extension']==1) {
                        $Person2Extension = $Person2ExtData['description'];
                    } else {
                        $Person2Extension = NULL;
                    };
                }
                $person2            = $row['Roommate1'];
                $person3ID          = $row['Roommate2ID'];
                if($person3ID != NULL) {
                    //To get the product details if this roommate has transfer product
                    $Person3TransferSQL = 	"SELECT ca.Transfer, pro.description FROM products pro
                                            JOIN customer_account ca ON ca.contactid=$person3ID AND ca.tourid=$groupid
                                            WHERE id=ca.Transfer_Product_ID;";
                    $Person3TransferResult = $db->query($Person3TransferSQL);
                    $Person3TransferData = $Person3TransferResult->fetch_assoc();
                    if($Person3TransferData['Transfer']==1) {
                        $Person3Transfer = $Person3TransferData['description'];
                    } else {
                        $Person3Transfer = NULL;
                    };
                    //To get the product details if this roommate has extension product
                    $Person3ExtSQL = 	"SELECT ca.Extension, pro.description FROM products pro
                                        JOIN customer_account ca ON ca.contactid=$person3ID AND ca.tourid=$groupid
                                        WHERE id=ca.Extension_Product_ID;";
                    $Person3ExtResult = $db->query($Person3ExtSQL);
                    $Person3ExtData = $Person3ExtResult->fetch_assoc();
                    if($Person3ExtData['Extension']==1) {
                        $Person3Extension = $Person3ExtData['description'];
                    } else {
                        $Person3Extension = NULL;
                    };
                }
                $person3            = $row['Roommate2'];
                $NeedTransfer       = $row['Need_Transfer'];
                $NeedExtension      = $row['Need_Extension'];
                $landReq            = $row["special_land_text"];
                $Meals_Request      = $row["Meals_Request"];
                $Allergies_Request  = $row["Allergies_Request"];

                $style='' ;
                $statusTitle ='' ;
                if( $row['status'] == 0 ) {
                    $style='style="color:red"' ;
                    $statusTitle ='Canceled Customer' ;
                }
                ?>
                    <tr <?php echo "title='$statusTitle' $style";?> >
                        <td><?php echo $i?></td>
                        <td><?php echo $roomType?></td>
                        <td><?php echo $person1?></td>
                        <td><?php echo $person2?></td>
                        <td><?php echo $person3?></td>
                        <td>
                        <?php 
                        if($NeedTransfer != "No") {echo "Person #1: ".$NeedTransfer."<br />";}
                        if($Person2Transfer != NULL) {echo "Person #2: ".$Person2Transfer."<br />";}
                        if($Person3Transfer != NULL) {echo "Person #3: ".$Person3Transfer."<br />";}
                        if($NeedTransfer == "No" AND $Person2Transfer == NULL AND $Person3Transfer == NULL) { echo "No";}
                        ?>
                        </td>
                        <td>
                        <?php 
                        if($NeedExtension != "No") {echo "Person #1: ".$NeedExtension."<br />";}
                        if($Person2Extension != NULL) {echo "Person #2: ".$Person2Extension."<br />";}
                        if($Person3Extension != NULL) {echo "Person #3: ".$Person3Extension."<br />";}
                        if($NeedExtension == "No" AND $Person2Extension == NULL AND $Person3Extension == NULL) { echo "No";}
                        ?>
                        </td>
                        <td><?php echo $landReq; ?></td>
                        <td><?php echo $Meals_Request; ?></td>
                        <td><?php echo $Allergies_Request; ?></td>
                    </tr>
                    <?php
                    $i++;
                }
            }
            ?>

	</table>
	<div style="height:100px"></div>
	<div style="text-align:right;">Updated:<?php echo date("m/d/Y"); ?></div>
	
<?php



$pdfData = ob_get_clean() ;


// output the HTML content
$pdf->writeHTML($pdfData, true, false, true, false, '');


// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

$PDF_OUTPUT = isset($PDF_OUTPUT ) ? $PDF_OUTPUT : 'I' ;

if (ob_get_contents()) ob_end_clean();
//Close and output PDF document
return $pdf->Output($report_slug.'.pdf', $PDF_OUTPUT );

//============================================================+
// END OF FILE
//============================================================+
