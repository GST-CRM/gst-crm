<?php

//delete invoice
if( isset($__REQUEST['invaction']) && $__REQUEST['invaction'] == 'delete' ) {
    $editId = $__GET['invid'] ;
    //invoice line
    $sqll =  "DELETE FROM customer_invoice_line WHERE Customer_Invoice_Num = '$editId'" ;
    GDb::execute($sqll) ;
    //Fidn transaction id
    $sqlts = "SELECT Transaction_Type_Transaction_ID FROM customer_invoice WHERE Customer_Invoice_Num ='$editId'" ;
    $transactionId = GDb::fetchScalar($sqlts) ;
    //delete transaction id
    $sqltd =  "DELETE FROM transactions WHERE Transaction_ID='$transactionId' LIMIT 1" ;
    GDb::execute($sqltd) ;
    //invoice
    $sqlci =  "DELETE FROM customer_invoice WHERE Customer_Invoice_Num = '$editId' LIMIT 1" ;
    GDb::execute($sqlci) ;
    //Todo delete other table too
    GUtils::redirect($CALLING_PAGE) ;
}
//void an invoice
else if( isset($__REQUEST['invaction']) && $__REQUEST['invaction'] == 'void' ) {
    $editId = $__GET['invid'] ;
    $sql =  "UPDATE customer_invoice SET Status=0 WHERE Customer_Invoice_Num = '$editId' LIMIT 1" ;
    GDb::execute($sql) ;
    //Fidn transaction id
    $sqlts = "SELECT Transaction_Type_Transaction_ID FROM customer_invoice WHERE Customer_Invoice_Num ='$editId'" ;
    $transactionId = GDb::fetchScalar($sqlts) ;
    //delete transaction id
    $sqltd =  "UPDATE transactions SET Voided_Flag=1 WHERE Transaction_ID='$transactionId' LIMIT 1" ;
    GDb::execute($sqltd) ;

    GUtils::redirect($CALLING_PAGE) ;
}
//send invoice
else if( isset($__REQUEST['invaction']) && $__REQUEST['invaction'] == 'send' ) {
    $FileTitlePrefix = 'invoice' ;
    include __DIR__ . '/../inc/sales-invoice-mail.php' ;
    GUtils::redirect('sales-invoices.php') ;
}
//email reminder
else if( isset($__REQUEST['invaction']) && $__REQUEST['invaction'] == 'reminder' ) {
    $FileTitlePrefix = 'invoice' ;
    include __DIR__ . '/../inc/sales-invoice-reminder.php' ;
    GUtils::redirect($CALLING_PAGE) ;
}
//the ajax email reminder 
else if( isset($__REQUEST['invaction']) && $__REQUEST['invaction'] == 'ajax-reminder' ) {
    $FileTitlePrefix = 'invoice' ;
    include __DIR__ . '/../inc/sales-invoice-reminder.php' ;
    $data = ['status' => 'OK'] ;
    GUtils::jsonResponse($data) ;
}
//the batch actions.
else if( isset($__REQUEST['batch-invaction']) ) {
    //batch print
    if( $__REQUEST['batch-invaction'] == 'P' ) {
        include __DIR__ . '/../inc/sales-invoice-pdf.php';
    }
    //batch send
    else if( $__REQUEST['batch-invaction'] == 'S' ) {
        $FileTitlePrefix = 'invoice' ;
        include __DIR__ . '/../inc/sales-invoice-mail.php';
        echo '<script type="text/javascript">window.close();</script>' ;
        die;
    }
}