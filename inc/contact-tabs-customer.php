<form name="contact5" action="inc/contact-functions.php" method="POST" class="row" id="contact5">
	<div class="form-group col-sm-12 m-0">
		<h4 class="sub-title">Customer Information</h4>
	</div>
	<div class="form-group form-default form-static-label col-sm-4">
		<label class="float-label">Passport Number</label>
		<input type="text" id="passportID" name="passport" class="form-control" value="<?php echo $data['passport'];?>">
		<input type="text" name="CustomerAccountBasicDataID" class="form-control" value="<?php echo $leadid; ?>" hidden>
		<input type="text" name="CustomerAccountBasicDataName" class="form-control" value="<?php echo $data["fname"]." ".$data["mname"]." ".$data["lname"]; ?>" hidden>
	</div>
	<div class="form-group form-default form-static-label col-sm-1">
		<label class="float-label">Pending?</label>
		<div class="checkbox-fade fade-in-success pt-2">
			<label>
				<input id="PassportPending" name="PassportPending" type="checkbox" value="1" <?php if($data['passport']=="PASSPORT PENDING") { echo "checked";} ?>>
				<span class="cr">
					<i class="cr-icon fas fa-check txt-success"></i>
				</span>
			</label>
		</div>
	</div>
	<div class="form-group form-default form-static-label col-sm-3">
		<label class="float-label">Passport Expiration Date</label>
		<input type="date" id="expirydateID" name="expirydate" class="form-control" value="<?php echo $data['expirydate'];?>"
               min="<?php echo date('Y-m-d', strtotime('+1 day')); ?>"
               max="<?php echo date('Y-m-d', strtotime('+100 year')); ?>"
               onblur="return this.reportValidity();"
               >
	</div>
	<div class="form-group form-default form-static-label col-sm-4">
		<label class="float-label">Country of Citizenship</label>
		<select name="citizenship" class="form-control form-control-default">
			<?php $con3 = new mysqli($servername, $username, $password, $dbname);
			$result3 = mysqli_query($con3,"SELECT name, abbrev FROM countries");
			while($row3 = mysqli_fetch_array($result3))
			{
				$selectedd = "";
				if($data['citizenship'] == "") {
					if($row3['abbrev'] == "US") {
						$selectedd = "selected";
					} else {
						$selectedd = "";
					}
				} elseif($data['citizenship'] == $row3['abbrev']) {
					$selectedd = "selected";
				} else {
					$selectedd = "";
				}
				echo "<option value='".$row3['abbrev']."' $selectedd>".$row3['name']."</option>";
			} ?>
		</select>
	</div>
	<div class="form-group form-default form-static-label col-sm-4">
		<label class="float-label">Birthday</label>
		<input type="date" name="birthday" value="<?php echo $data["birthday"]; ?>" 
               class="form-control" 
               max="<?php echo date('Y-m-d', strtotime('-1 day')); ?>"  
               min="1925-01-01" 
               onblur="return this.reportValidity();"
               >
	</div>
	<div class="form-group form-default form-static-label col-sm-4">
		<label class="float-label">Name on the Badge</label>
		<input type="text" name="badgename" class="form-control" value="<?php echo $data['badgename'];?>">
	</div>
	<div class="col-sm-4">
		<label class="float-label">Martial Status</label>
		<select name="martialstatus" class="form-control form-control-default">
			<option value="NA" <?php if($data["martialstatus"] == "NA") {echo "selected";} ?>>N/A</option>
			<option value="Married" <?php if($data["martialstatus"] == "Married") {echo "selected";} ?>>Married</option>
			<option value="Single"<?php if($data["martialstatus"] == "Single") {echo "selected";} ?>>Single</option>
		</select>
	</div>
	<div class="col-sm-4">
		<label class="float-label">Gender</label>
		<select name="gender" class="form-control form-control-default">
			<option value="0" disabled <?php if($data["gender"] != "Male" OR $data["gender"] != "Female") {echo "selected";} ?>> </option>
			<option value="Male" <?php if($data["gender"] == "Male") {echo "selected";} ?>>Male</option>
			<option value="Female" <?php if($data["gender"] == "Female") {echo "selected";} ?>>Female</option>
		</select>
	</div>
	<div class="form-group form-default form-static-label col-sm-4">
		<label class="float-label">Payment Terms</label>
		<input type="text" name="paymentterm" class="form-control" value="<?php echo $data['payment'];?>">
	</div>
	<div class="form-group form-default form-static-label col-sm-4">
		<label class="float-label">Known Traveler #</label>
		<input type="text" name="travelerid" class="form-control" value="<?php echo $data['travelerid'];?>">
	</div>
	<div class="form-group form-default form-static-label col-sm-4">
		<label class="float-label">Frequent Flyer</label>
		<input type="text" name="flyer" class="form-control" value="<?php echo $data['flyer'];?>">
	</div>
	<div class="form-group form-default form-static-label col-sm-4">
		<label class="float-label">Missing Information?</label>
		<input type="text" name="missinginfo" class="form-control" value="<?php echo $data['missinginfo'];?>">
	</div>
	<div class="form-group form-default form-static-label col-sm-4">
		<label class="float-label">Enable Login</label>
		<div class="can-toggle">
            <input type="checkbox" class="customer-active-disable-switch-checkbox" id="customerLogin" name="customerlogin" value="1" <?php if($customerData["customeractive"]==1) echo"checked"; ?> >
            <label for="customerLogin">
                <div class="can-toggle__switch" data-checked="Active" data-unchecked="Disabled"></div>
            </label>
        </div>
	</div>
	<div class="form-group col-sm-12">
		<input type="submit" name="updatecustomer" 
               onclick="return showContactSubmitPopup(this);"
               value="Update" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" >
	</div>
</form>
	<div class="row">
		<div class="col-sm-12" style="background:#f2f2f2;margin-bottom: 20px;border-radius: 20px 0px;padding:6px 0px;text-align:center;font-weight:bold;">This customer is added to the following groups</div>
		<div class="form-group col text-center p-1 bg-inverse">Group's Color Index:</div>
		<div class="form-group col text-center p-1 text-white" style="background:url(files/assets/images/alt4.jpg) repeat-x top;">Active Group</div>
		<div class="form-group col text-center p-1 text-white" style="background:url(files/assets/images/alt3.jpg) repeat-x top;">Expired Group</div>
		<div class="form-group col text-center p-1 text-white" style="background:url(files/assets/images/alt1.jpg) repeat-x top;">Joint Invoice</div>
		<div class="form-group col text-center p-1 text-white" style="background:url(files/assets/images/alt2.jpg) repeat-x top;">Cancelled Customer</div>
	</div>
<div class="row p-0 mb-4">
	<div class="accordion" style="width:100%;" id="accordionExample">
		<?php
		$CustomerAccountSQL = 	"SELECT ca.*, gro.tourname, gro.startdate, gro.enddate FROM customer_account ca
								JOIN groups gro
								ON ca.tourid=gro.tourid
								WHERE ca.contactid=$leadid
								/*AND ca.status=1  */  
                                ORDER BY ca.status, gro.startdate DESC
                                ;";
		$CustomerAccountResult = $db->query($CustomerAccountSQL);
		if ($CustomerAccountResult->num_rows > 0) {
		$LoopCounter = 1;
		while($CustomerAccountRow = $CustomerAccountResult->fetch_assoc()) {
		
		//To get the status of the groups list and color their background
		//Colors code: 1 is blue, 2 is grey, 3 is red, 4 is green
		//Color meaning: Acitve=4 , Exipred=3 , Joint=1 , Cancelled=2 
		if($CustomerAccountRow['status'] == 0) {
			$AltID = 2;
		} else {
			if($CustomerAccountRow['enddate'] >= date("Y-m-d")) {
				if($CustomerAccountRow['groupinvoice'] == 0) {
					$AltID = 4;
				} else {
					$AltID = 1;
				}
			} else {
				$AltID = 3;
			}
		}
		?>
		<div class="card mb-1" style="background: #fafafa url(files/assets/images/alt<?php echo $AltID; ?>.jpg) no-repeat -290px 0px;">
			<div id="headingOne">
				<h2 class="mb-0">
					<button class="btn btn-link text-dark" type="button" data-toggle="collapse" data-target="#collapse<?php echo $CustomerAccountRow['tourid']; ?>" aria-expanded="true" aria-controls="collapseOne">
					<b><?php echo $CustomerAccountRow['tourname']; ?></b><small> | (<?php echo date('m/d/Y', strtotime($CustomerAccountRow['startdate'])); ?> - <?php echo date('m/d/Y', strtotime($CustomerAccountRow['enddate'])); ?>)</small>
					</button>
				</h2>
			</div>
			<div id="collapse<?php echo $CustomerAccountRow['tourid']; ?>" class="collapse collapsed <?php //if($LoopCounter == 1) {echo "show";} else { echo "collapsed";} ?>" aria-labelledby="headingOne" data-parent="#accordionExample">
				<div class="card-body">
				<form name="contact<?php echo $TheTourID; ?>" action="inc/contact-functions.php" method="POST" class="row" id="contact<?php echo $TheTourID; ?>">
					<div class="col-sm-12 mb-4">
					<div class="alert-box notice">
						<span>Notice:</span> The customer is added to the group <a style="text-decoration:underline;" href="groups-edit.php?id=<?php echo $CustomerAccountRow['tourid']; ?>&action=edit&tab=participants"><?php echo $CustomerAccountRow['tourname']; ?></a> on <?php echo date('m/d/Y', strtotime($CustomerAccountRow['createtime'])); ?>.
					</div>
					</div>
                    <div class="col-sm-4" style="display: none;">
						<h4 class="sub-title">Status</h4>
							<div class="can-toggle">
							  <input id="costomerstatus" name="status" value="1" type="checkbox" <?php if($CustomerAccountRow['status'] == 1) {echo "checked";} ?>>
							  <input name="CustomerLoopID" value="<?php echo $leadid; ?>" type="text" hidden>
							  <input name="CustomerLoopName" value="<?php echo $data["fname"]." ".$data["lname"]; ?>" type="text" hidden>
							  <input name="CustomerTourID" value="<?php echo $CustomerAccountRow['tourid']; ?>" type="text" hidden>
							  <label for="costomerstatus">
								<div class="can-toggle__switch" data-checked="Active" data-unchecked="Disabled"></div>
							  </label>
							</div>
					</div>
                    <div class="col-sm-4" style="display: none;">
						<h4 class="sub-title">Invoiced Customer</h4>
						<div class="can-toggle">
						  <input id="invoiced_customer<?php echo $TheTourID; ?>" name="invoiced_customer" value="1" type="checkbox" <?php if($CustomerAccountRow['invoiced_customer'] == 1) {echo "checked";} ?>>
						  <label for="invoiced_customer<?php echo $TheTourID; ?>">
							<div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
						  </label>
						</div>
					</div>
					<div class="form-group col-sm-4">
						<label class="float-label">Room Type</label>
							<?php 
							$TheTourID = $CustomerAccountRow['tourid'];
							$WhereClauseNoRepeat = "";
							
							//First, lets check if the current customer is the primary customer or not
							$RoomingSQL = 	"SELECT Primary_Customer_ID,Additional_Traveler_ID_1,Additional_Traveler_ID_2
											FROM customer_groups
											WHERE Type='Rooming' AND Group_ID=$TheTourID AND Primary_Customer_ID=$leadid";
							$RoomingResult = $db->query($RoomingSQL);
							$RoomingCount = $RoomingResult->num_rows;
							if($RoomingCount == 1) { //If the outcome is positive and one row result then get the values
								$RoomingData = $RoomingResult->fetch_assoc();
								$RoomingPriamryID = $RoomingData['Primary_Customer_ID'];
								$RoomingDoubleID = $RoomingData['Additional_Traveler_ID_1'];
								$RoomingTripleID = $RoomingData['Additional_Traveler_ID_2'];
								$RoomingType = $CustomerAccountRow['roomtype'];
							} else { //If the outcome gave more or less than one result, we should check the following
								$RoomingSQL = 	"SELECT Primary_Customer_ID,Additional_Traveler_ID_1,Additional_Traveler_ID_2
												FROM customer_groups
												WHERE Type='Rooming' AND Group_ID=$TheTourID AND $leadid IN(Additional_Traveler_ID_1,Additional_Traveler_ID_2)";
								$RoomingResult = $db->query($RoomingSQL);
								$RoomingCount2 = $RoomingResult->num_rows;
								if($RoomingCount2 == 1) {
									$RoomingData2 = $RoomingResult->fetch_assoc();
									$RoomingPriamryID = $RoomingData2['Primary_Customer_ID'];
									$RoomingDoubleID = $RoomingData2['Additional_Traveler_ID_1'];
									$RoomingTripleID = $RoomingData2['Additional_Traveler_ID_2'];
									$RoomingType = $CustomerAccountRow['roomtype'];
								} elseif($RoomingCount2 > 1) {
									$xx = 1;
									$RoomingLineWeWant = NULL;
									$RoomingLinesDelete = NULL;
									
									//If there are duplicate entries and more than one rooming record for customer, delete it
									while($RoomingDataExtra = $RoomingResult->fetch_assoc()) {
										if($xx == 1) {
											$RoomingPriamryID = $RoomingDataExtra['Primary_Customer_ID'];
											$RoomingLineWeWant = $RoomingPriamryID;
											$RoomingDoubleID = $RoomingDataExtra['Additional_Traveler_ID_1'];
											$RoomingTripleID = $RoomingDataExtra['Additional_Traveler_ID_2'];
											$RoomingType = $RoomingDataExtra['roomtype'];
										} else {
											$RoomingLinesDelete = $RoomingPriamryID;
											$RoomingDeleteLines =  "DELETE FROM customer_groups WHERE Primary_Customer_ID=$RoomingLinesDelete AND Type='Rooming' AND Group_ID=$TheTourID" ;
											GDb::execute($RoomingDeleteLines);
										}
										$xx++;
									}
								} else {
									$SingleRoomingSQL 		= "SELECT ca.roomtype
															   FROM customer_account ca
															   WHERE ca.contactid=$leadid AND ca.tourid=$TheTourID";
									$SingleRoomingResult	= $db->query($SingleRoomingSQL);
									$SingleRoomingCount 	= $SingleRoomingResult->num_rows;
									if($SingleRoomingCount == 1) {
										$SingleRoomingData 	= $SingleRoomingResult->fetch_assoc();
										$RoomingType 		= $SingleRoomingData['roomtype'];
									}
									$RoomingDoubleID = 0;
									$RoomingTripleID = 0;
								}
							}
							?>
						<select name="roomtype" class="form-control form-control-default" onchange="RoomTypeSelect<?php echo $TheTourID; ?>(this);">
							<option value="0" <?php if($RoomingType == 0 OR $RoomingType == NULL) {echo "selected";} ?>> </option>
							<option id="SingleOption<?php echo $TheTourID; ?>" value="Single" <?php if($RoomingType == 'Single') {echo "selected";} ?>>Single</option>
							<option id="DoubleOption<?php echo $TheTourID; ?>" value="Double" <?php if($RoomingType == 'Double') {echo "selected";} ?>>Double</option>
							<option id="TwinOption<?php echo $TheTourID; ?>" value="Twin" <?php if($RoomingType == 'Twin') {echo "selected";} ?>>Twin</option>
							<option id="TripleOption<?php echo $TheTourID; ?>" value="Triple" <?php if($RoomingType == 'Triple') {echo "selected";} ?>>Triple</option>
						</select>
					</div>
					<div id="roomtypePrimary" class="form-group form-default form-static-label col-sm-4" style="display:<?php if($RoomingType == 'Double' OR $RoomingType == 'Twin') {echo "block";} elseif ($RoomingType == 'Triple') {echo "block";}  else {echo "none";} ?>;">
						<label class="float-label">(Primary) Rooming Contact <?php if($RoomingPriamryID > 0 AND $RoomingPriamryID != 999) { if($RoomingPriamryID != $leadid) { echo "<a class='text-info' href='contacts-edit.php?id=$RoomingPriamryID&action=edit&usertype=customer'>- View Customer</a>";} } ?></label>
						<select name="PrimaryRoomingContact" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<?php 
							$PrimaryRoomingSQL = "SELECT co.id, co.fname, co.lname FROM contacts co 
									JOIN customer_account ca 
									ON co.id = ca.contactid
									WHERE ca.status=1 AND ca.tourid=$TheTourID AND ca.contactid=$RoomingPriamryID";
							$PrimaryRoomingResult = $db->query($PrimaryRoomingSQL);
							$PrimaryRoomingCount = $PrimaryRoomingResult->num_rows;
							if($PrimaryRoomingCount > 0) { 
								$PrimaryRoomingData = $PrimaryRoomingResult->fetch_assoc();
								echo "<option value='".$RoomingPriamryID."' selected>".$PrimaryRoomingData['fname']." ".$PrimaryRoomingData['lname']."</option>";
							} else {
								$WhereClauseNoRepeat = "AND NOT EXISTS(SELECT * FROM customer_groups WHERE Type='Rooming' AND Group_ID=$TheTourID AND co.id IN(Primary_Customer_ID,Additional_Traveler_ID_1,Additional_Traveler_ID_2))";
								echo "<option value='".$leadid."' selected>".$data["fname"]." ".$data["lname"]."</option>";
							}
							?>
						</select>
					</div>
					<div id="roomtypedouble<?php echo $TheTourID; ?>" class="form-group form-default form-static-label col-sm-4" style="display:<?php if($RoomingType == 'Double' OR $RoomingType == 'Twin') {echo "block";} elseif ($RoomingType == 'Triple') {echo "block";}  else {echo "none";} ?>;">
						<label class="float-label">(Double) Rooming with <?php if($RoomingDoubleID > 0 AND $RoomingDoubleID != 999) { echo "<a class='text-info' href='contacts-edit.php?id=$RoomingDoubleID&action=edit&usertype=customer'>- View Customer</a>"; } ?></label>
						<select name="travelingwith" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<option value="999" <?php if ($RoomingDoubleID == 999) {echo "selected";} ?>> **To be Determined**</option>
							<?php 
							$con6 = new mysqli($servername, $username, $password, $dbname);
							$result6 = mysqli_query($con6,	"SELECT co.id, co.fname, co.lname FROM contacts co 
															JOIN customer_account ca 
															ON co.id = ca.contactid
															WHERE ca.status=1 AND ca.tourid=$TheTourID");
															//$WhereClauseNoRepeat");
							while($row6 = mysqli_fetch_array($result6)) {
								if($RoomingDoubleID == $row6['id']) {$travelcontactselect = "selected";}
								echo "<option value='".$row6['id']."' $travelcontactselect>".$row6['fname']." ".$row6['lname']."</option>";
								$travelcontactselect = "";
							} ?>
						</select>
					</div>
					<div id="roomtypetriple<?php echo $TheTourID; ?>" class="form-group form-default form-static-label col-sm-4" style="display:<?php if($RoomingType == 'Triple') {echo "block";} else {echo "none";} ?>;">
						<label class="float-label">(Triple) Rooming with #2 <?php if($RoomingTripleID > 0 AND $RoomingTripleID != 999) { echo "<a class='text-info' href='contacts-edit.php?id=$RoomingTripleID&action=edit&usertype=customer'>- View Customer</a>"; } ?></label>
						<select name="travelingwith2" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<option value="999" <?php if ($RoomingTripleID == 999) {echo "selected";} ?>>**To be Determined**</option>
							<?php
							$con6 = new mysqli($servername, $username, $password, $dbname);
							$result6 = mysqli_query($con6,"SELECT co.id, co.fname, co.lname FROM contacts co 
				JOIN customer_account ca 
				ON co.id = ca.contactid
				WHERE ca.status=1 AND ca.tourid=$TheTourID");
				//$WhereClauseNoRepeat");
							while($row6 = mysqli_fetch_array($result6))
							{
								if ($RoomingTripleID == $row6['id']) {$travelcontactselect = "selected";}
								echo "<option value='".$row6['id']."' $travelcontactselect>".$row6['fname']." ".$row6['lname']."</option>";
								$travelcontactselect = "";
							} ?>
						</select>
					</div>
					<div class="col-sm-12" style="background:#f2f2f2;margin-bottom: 20px;border-radius: 20px 0px;"><br/></div>
					<div class="col-sm-4">
						<h4 class="sub-title">Joint Invoice?</h4>
						<div class="can-toggle">
							<input id="GroupInvoice<?php echo $TheTourID; ?>" name="GroupInvoice" value="1" type="checkbox" <?php if($CustomerAccountRow['groupinvoice'] == 1) {echo "checked";} ?>>
							<label for="GroupInvoice<?php echo $TheTourID; ?>">
								<div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
							</label>
						</div>
					</div>
					<?php
						$DisplayCSS = "display:none"; //To hide the joint invoice box if it wasn't checked as Active
						$DisplayDisabled = ""; //To enable filling up the fields if the joint invoice fields are empty
						
						$CheckCustomerGroupExist = mysqli_query($db,"SELECT Primary_Customer_ID AS PrimaryID, Additional_Traveler_ID_1 AS Addition1, Additional_Traveler_ID_2 AS Addition2, Additional_Traveler_ID_3 AS Addition3, Additional_Traveler_ID_4 AS Addition4, Additional_Traveler_ID_5 AS Addition5, Additional_Traveler_ID_6 AS Addition6, Additional_Traveler_ID_7 AS Addition7, Additional_Traveler_ID_8 AS Addition8, Additional_Traveler_ID_9 AS Addition9, Additional_Traveler_ID_10 AS Addition10 FROM customer_groups WHERE (Primary_Customer_ID=$leadid OR Additional_Traveler_ID_1=$leadid OR Additional_Traveler_ID_2=$leadid OR Additional_Traveler_ID_3=$leadid OR Additional_Traveler_ID_4=$leadid OR Additional_Traveler_ID_5=$leadid OR Additional_Traveler_ID_6=$leadid OR Additional_Traveler_ID_7=$leadid OR Additional_Traveler_ID_8=$leadid OR Additional_Traveler_ID_9=$leadid OR Additional_Traveler_ID_10=$leadid) AND Group_ID=$TheTourID AND Type='Group Invoice'");
						$CheckCustomerGroupCount = mysqli_num_rows($CheckCustomerGroupExist);
						if($CheckCustomerGroupCount > 0) {
							$CustGroupIDz = mysqli_fetch_array($CheckCustomerGroupExist);
							$PrimaryID = $CustGroupIDz['PrimaryID'];
							$Addition1 = $CustGroupIDz['Addition1'];
							$Addition2 = $CustGroupIDz['Addition2'];
							$Addition3 = $CustGroupIDz['Addition3'];
							$Addition4 = $CustGroupIDz['Addition4'];
							$Addition5 = $CustGroupIDz['Addition5'];
							$Addition6 = $CustGroupIDz['Addition6'];
							$Addition7 = $CustGroupIDz['Addition7'];
							$Addition8 = $CustGroupIDz['Addition8'];
							$Addition9 = $CustGroupIDz['Addition9'];
							$Addition10 = $CustGroupIDz['Addition10'];
							
							$DisplayCSS = ""; //To show the joint invoice box
							$DisplayDisabled = "disabled"; //To disable the fields control for non primary customers
							if($PrimaryID == $leadid) {$DisplayDisabled = "";} //To enable the fields control for the primary contact
						}
						//To get the ID of the current customer and make sure there is no primary customer for Joint Invoice for this contact
						if($PrimaryID > 0) {$CustomerIDquery = $PrimaryID;} else {$CustomerIDquery = $leadid;}
					?>
					<div id="GroupInvoicePrimary<?php echo $TheTourID; ?>" class="col-sm-8" style="<?php echo $DisplayCSS; ?>">
						<label class="float-label">Primary Traveler - Click on the primary customer to edit all the listed customers!</label>
							<?php 
								
								$Add_Traveler_Result = mysqli_query($db,"SELECT co.id,co.fname,co.mname,co.lname FROM contacts co WHERE co.id=$CustomerIDquery");
								$TravelersRow = mysqli_fetch_array($Add_Traveler_Result);
								$GetPrimaryCustomerInv_ID = mysqli_query($db,"SELECT Customer_Invoice_Num FROM customer_invoice WHERE Customer_Account_Customer_ID=$CustomerIDquery AND Group_ID=$TheTourID;");
								$PrimaryInvoiceID = mysqli_fetch_array($GetPrimaryCustomerInv_ID);
								$PrimaryInvoiceID_data = $PrimaryInvoiceID['Customer_Invoice_Num'];
							?>
							<input type="text" name="PrimaryCustomerInvoiceID" class="form-control" value="<?php echo $PrimaryInvoiceID_data;?>" hidden>
							<input type="text" name="PrimaryCustomer" class="form-control" value="<?php echo $TravelersRow['id'];?>" hidden>
							<a href="contacts-edit.php?id=<?php echo $TravelersRow['id'];?>&action=edit&usertype=customer">
								<input type="text" name="PrimaryCustomerName" class="form-control" style="cursor:pointer;" value="<?php echo $TravelersRow['fname']." ".$TravelersRow['mname']." ".$TravelersRow['lname']; ?>" readonly>
							</a>
					</div>
					<div id="GroupInvoiceFields<?php echo $TheTourID; ?>" class="col-sm-12 pr-0 row" style="<?php echo $DisplayCSS; ?>">
						<div class="col-sm-3 mb-2">
							<label class="float-label">Additional Traveler #1</label>
							<input type="text" name="TravelerCurrent1" class="form-control" value="<?php echo $Addition1; ?>" hidden>
							<?php if($DisplayDisabled == "disabled") {?><input type="text" name="Traveler1" class="form-control" value="<?php echo $Addition1; ?>" hidden><?php } ?>
							<select name="Traveler1" class="form-control form-control-default <?php if($PrimaryID == $leadid) {echo '"';} else {echo 'text-dark" style="opacity:1;"';} ?> <?php echo $DisplayDisabled; ?>>
								<option value="0" selected> </option>
								<?php 
								$Add_Traveler_Result = mysqli_query($db,"SELECT co.id,co.fname,co.mname,co.lname FROM contacts co JOIN customer_account ca  ON ca.contactid=co.id WHERE ca.tourid=$TheTourID AND co.id !=$CustomerIDquery");
								while($TravelersRow = mysqli_fetch_array($Add_Traveler_Result))
								{
									$CurrCustID = $TravelersRow['id'];
									$CheckingCustomerJointInvoiceResult = mysqli_query($con6,"SELECT Primary_Customer_ID FROM customer_groups WHERE Group_ID=$TheTourID AND Type='Group Invoice' AND CONCAT_WS('', Primary_Customer_ID, Additional_Traveler_ID_1, Additional_Traveler_ID_2, Additional_Traveler_ID_3, Additional_Traveler_ID_4) LIKE '%$CurrCustID%'");
									$CheckingCustomerJointInvoiceCount = mysqli_num_rows($CheckCustomerGroupExist);
									if($CheckingCustomerJointInvoiceCount > 0) {
										$CheckingCustomerJointInvoiceData = mysqli_fetch_array($CheckingCustomerJointInvoiceResult);
										if($CheckingCustomerJointInvoiceData['Primary_Customer_ID'] > 0) {$DisabledSelect = " disabled";} else { $DisabledSelect = ""; }
										$selected = ''.$DisabledSelect;
									}
									if($Addition1==$TravelersRow['id']){$selected = 'selected';};
									echo "<option value='".$TravelersRow['id']."' $selected>".$TravelersRow['fname']." ".$TravelersRow['mname']." ".$TravelersRow['lname']."</option>";
								} ?>
							</select>
						</div>
						<div class="col-sm-3 mb-2">
							<label class="float-label">Additional Traveler #2</label>
							<input type="text" name="TravelerCurrent2" class="form-control" value="<?php echo $Addition2; ?>" hidden>
							<?php if($DisplayDisabled == "disabled") {?><input type="text" name="Traveler2" class="form-control" value="<?php echo $Addition2; ?>" hidden><?php } ?>
							<select name="Traveler2" class="form-control form-control-default <?php if($PrimaryID == $leadid) {echo '"';} else {echo 'text-dark" style="opacity:1;"';} ?> <?php echo $DisplayDisabled; ?>>
								<option value="0" selected> </option>
								<?php 
								$Add_Traveler_Result = mysqli_query($db,"SELECT co.id,co.fname,co.mname,co.lname FROM contacts co JOIN customer_account ca  ON ca.contactid=co.id WHERE ca.tourid=$TheTourID AND co.id !=$CustomerIDquery");
								while($TravelersRow = mysqli_fetch_array($Add_Traveler_Result))
								{
									$CurrCustID = $TravelersRow['id'];
									$CheckingCustomerJointInvoiceResult = mysqli_query($con6,"SELECT Primary_Customer_ID FROM customer_groups WHERE Group_ID=$TheTourID AND Type='Group Invoice' AND CONCAT_WS('', Primary_Customer_ID, Additional_Traveler_ID_1, Additional_Traveler_ID_2, Additional_Traveler_ID_3, Additional_Traveler_ID_4) LIKE '%$CurrCustID%'");
									$CheckingCustomerJointInvoiceCount = mysqli_num_rows($CheckCustomerGroupExist);
									if($CheckingCustomerJointInvoiceCount > 0) {
										$CheckingCustomerJointInvoiceData = mysqli_fetch_array($CheckingCustomerJointInvoiceResult);
										if($CheckingCustomerJointInvoiceData['Primary_Customer_ID'] > 0) {$DisabledSelect = " disabled";} else { $DisabledSelect = ""; }
										$selected = ''.$DisabledSelect;
									}
									if($Addition2==$TravelersRow['id']){$selected = 'selected';};
									echo "<option value='".$TravelersRow['id']."' $selected>".$TravelersRow['fname']." ".$TravelersRow['mname']." ".$TravelersRow['lname']."</option>";
								} ?>
							</select>
						</div>
						<div class="col-sm-3 mb-2">
							<label class="float-label">Additional Traveler #3</label>
							<input type="text" name="TravelerCurrent3" class="form-control" value="<?php echo $Addition3; ?>" hidden>
							<?php if($DisplayDisabled == "disabled") {?><input type="text" name="Traveler3" class="form-control" value="<?php echo $Addition3; ?>" hidden><?php } ?>
							<select name="Traveler3<?php if($DisplayDisabled == "disabled") {echo "xXx";} ?>" class="form-control form-control-default <?php if($PrimaryID == $leadid) {echo '"';} else {echo 'text-dark" style="opacity:1;"';} ?> <?php echo $DisplayDisabled; ?>>
								<option value="0" selected> </option>
								<?php 
								$Add_Traveler_Result = mysqli_query($db,"SELECT co.id,co.fname,co.mname,co.lname FROM contacts co JOIN customer_account ca  ON ca.contactid=co.id WHERE ca.tourid=$TheTourID AND co.id !=$CustomerIDquery");
								while($TravelersRow = mysqli_fetch_array($Add_Traveler_Result))
								{
									$CurrCustID = $TravelersRow['id'];
									$CheckingCustomerJointInvoiceResult = mysqli_query($con6,"SELECT Primary_Customer_ID FROM customer_groups WHERE Group_ID=$TheTourID AND Type='Group Invoice' AND CONCAT_WS('', Primary_Customer_ID, Additional_Traveler_ID_1, Additional_Traveler_ID_2, Additional_Traveler_ID_3, Additional_Traveler_ID_4) LIKE '%$CurrCustID%'");
									$CheckingCustomerJointInvoiceCount = mysqli_num_rows($CheckCustomerGroupExist);
									if($CheckingCustomerJointInvoiceCount > 0) {
										$CheckingCustomerJointInvoiceData = mysqli_fetch_array($CheckingCustomerJointInvoiceResult);
										if($CheckingCustomerJointInvoiceData['Primary_Customer_ID'] > 0) {$DisabledSelect = " disabled";} else { $DisabledSelect = ""; }
										$selected = ''.$DisabledSelect;
									}
									if($Addition3==$TravelersRow['id']){$selected = 'selected';};
									echo "<option value='".$TravelersRow['id']."' $selected>".$TravelersRow['fname']." ".$TravelersRow['mname']." ".$TravelersRow['lname']."</option>";
								} ?>
							</select>
						</div>
						<div class="col-sm-3 pr-0 mb-2">
							<label class="float-label">Additional Traveler #4</label>
							<input type="text" name="TravelerCurrent4" class="form-control" value="<?php echo $Addition4; ?>" hidden>
							<?php if($DisplayDisabled == "disabled") {?><input type="text" name="Traveler3" class="form-control" value="<?php echo $Addition4; ?>" hidden><?php } ?>
							<select name="Traveler4<?php if($DisplayDisabled == "disabled") {echo "xXx";} ?>" class="form-control form-control-default <?php if($PrimaryID == $leadid) {echo '"';} else {echo 'text-dark" style="opacity:1;"';} ?> <?php echo $DisplayDisabled; ?>>
								<option value="0" selected> </option>
								<?php 
								$Add_Traveler_Result = mysqli_query($db,"SELECT co.id,co.fname,co.mname,co.lname FROM contacts co JOIN customer_account ca  ON ca.contactid=co.id WHERE ca.tourid=$TheTourID AND co.id !=$CustomerIDquery");
								while($TravelersRow = mysqli_fetch_array($Add_Traveler_Result))
								{
									$CurrCustID = $TravelersRow['id'];
									$CheckingCustomerJointInvoiceResult = mysqli_query($con6,"SELECT Primary_Customer_ID FROM customer_groups WHERE Group_ID=$TheTourID AND Type='Group Invoice' AND CONCAT_WS('', Primary_Customer_ID, Additional_Traveler_ID_1, Additional_Traveler_ID_2, Additional_Traveler_ID_3, Additional_Traveler_ID_4) LIKE '%$CurrCustID%'");
									$CheckingCustomerJointInvoiceCount = mysqli_num_rows($CheckCustomerGroupExist);
									if($CheckingCustomerJointInvoiceCount > 0) {
										$CheckingCustomerJointInvoiceData = mysqli_fetch_array($CheckingCustomerJointInvoiceResult);
										if($CheckingCustomerJointInvoiceData['Primary_Customer_ID'] > 0) {$DisabledSelect = " disabled";} else { $DisabledSelect = ""; }
										$selected = ''.$DisabledSelect;
									}
									if($Addition4==$TravelersRow['id']){$selected = 'selected';};
									echo "<option value='".$TravelersRow['id']."' $selected>".$TravelersRow['fname']." ".$TravelersRow['mname']." ".$TravelersRow['lname']."</option>";
								} ?>
							</select>
						</div>
						<div class="col-sm-3 mb-2">
							<label class="float-label">Additional Traveler #5</label>
							<input type="text" name="TravelerCurrent5" class="form-control" value="<?php echo $Addition5; ?>" hidden>
							<?php if($DisplayDisabled == "disabled") {?><input type="text" name="Traveler3" class="form-control" value="<?php echo $Addition5; ?>" hidden><?php } ?>
							<select name="Traveler5<?php if($DisplayDisabled == "disabled") {echo "xXx";} ?>" class="form-control form-control-default <?php if($PrimaryID == $leadid) {echo '"';} else {echo 'text-dark" style="opacity:1;"';} ?> <?php echo $DisplayDisabled; ?>>
								<option value="0" selected> </option>
								<?php 
								$Add_Traveler_Result = mysqli_query($db,"SELECT co.id,co.fname,co.mname,co.lname FROM contacts co JOIN customer_account ca  ON ca.contactid=co.id WHERE ca.tourid=$TheTourID AND co.id !=$CustomerIDquery");
								while($TravelersRow = mysqli_fetch_array($Add_Traveler_Result))
								{
									$CurrCustID = $TravelersRow['id'];
									$CheckingCustomerJointInvoiceResult = mysqli_query($con6,"SELECT Primary_Customer_ID FROM customer_groups WHERE Group_ID=$TheTourID AND Type='Group Invoice' AND CONCAT_WS('', Primary_Customer_ID, Additional_Traveler_ID_1, Additional_Traveler_ID_2, Additional_Traveler_ID_3, Additional_Traveler_ID_4) LIKE '%$CurrCustID%'");
									$CheckingCustomerJointInvoiceCount = mysqli_num_rows($CheckCustomerGroupExist);
									if($CheckingCustomerJointInvoiceCount > 0) {
										$CheckingCustomerJointInvoiceData = mysqli_fetch_array($CheckingCustomerJointInvoiceResult);
										if($CheckingCustomerJointInvoiceData['Primary_Customer_ID'] > 0) {$DisabledSelect = " disabled";} else { $DisabledSelect = ""; }
										$selected = ''.$DisabledSelect;
									}
									if($Addition5==$TravelersRow['id']){$selected = 'selected';};
									echo "<option value='".$TravelersRow['id']."' $selected>".$TravelersRow['fname']." ".$TravelersRow['mname']." ".$TravelersRow['lname']."</option>";
								} ?>
							</select>
						</div>
						<div class="col-sm-3 mb-2">
							<label class="float-label">Additional Traveler #6</label>
							<input type="text" name="TravelerCurrent6" class="form-control" value="<?php echo $Addition6; ?>" hidden>
							<?php if($DisplayDisabled == "disabled") {?><input type="text" name="Traveler3" class="form-control" value="<?php echo $Addition6; ?>" hidden><?php } ?>
							<select name="Traveler6<?php if($DisplayDisabled == "disabled") {echo "xXx";} ?>" class="form-control form-control-default <?php if($PrimaryID == $leadid) {echo '"';} else {echo 'text-dark" style="opacity:1;"';} ?> <?php echo $DisplayDisabled; ?>>
								<option value="0" selected> </option>
								<?php 
								$Add_Traveler_Result = mysqli_query($db,"SELECT co.id,co.fname,co.mname,co.lname FROM contacts co JOIN customer_account ca  ON ca.contactid=co.id WHERE ca.tourid=$TheTourID AND co.id !=$CustomerIDquery");
								while($TravelersRow = mysqli_fetch_array($Add_Traveler_Result))
								{
									$CurrCustID = $TravelersRow['id'];
									$CheckingCustomerJointInvoiceResult = mysqli_query($con6,"SELECT Primary_Customer_ID FROM customer_groups WHERE Group_ID=$TheTourID AND Type='Group Invoice' AND CONCAT_WS('', Primary_Customer_ID, Additional_Traveler_ID_1, Additional_Traveler_ID_2, Additional_Traveler_ID_3, Additional_Traveler_ID_4) LIKE '%$CurrCustID%'");
									$CheckingCustomerJointInvoiceCount = mysqli_num_rows($CheckCustomerGroupExist);
									if($CheckingCustomerJointInvoiceCount > 0) {
										$CheckingCustomerJointInvoiceData = mysqli_fetch_array($CheckingCustomerJointInvoiceResult);
										if($CheckingCustomerJointInvoiceData['Primary_Customer_ID'] > 0) {$DisabledSelect = " disabled";} else { $DisabledSelect = ""; }
										$selected = ''.$DisabledSelect;
									}
									if($Addition6==$TravelersRow['id']){$selected = 'selected';};
									echo "<option value='".$TravelersRow['id']."' $selected>".$TravelersRow['fname']." ".$TravelersRow['mname']." ".$TravelersRow['lname']."</option>";
								} ?>
							</select>
						</div>
						<div class="col-sm-3 mb-2">
							<label class="float-label">Additional Traveler #7</label>
							<input type="text" name="TravelerCurrent7" class="form-control" value="<?php echo $Addition7; ?>" hidden>
							<?php if($DisplayDisabled == "disabled") {?><input type="text" name="Traveler3" class="form-control" value="<?php echo $Addition7; ?>" hidden><?php } ?>
							<select name="Traveler7<?php if($DisplayDisabled == "disabled") {echo "xXx";} ?>" class="form-control form-control-default <?php if($PrimaryID == $leadid) {echo '"';} else {echo 'text-dark" style="opacity:1;"';} ?> <?php echo $DisplayDisabled; ?>>
								<option value="0" selected> </option>
								<?php 
								$Add_Traveler_Result = mysqli_query($db,"SELECT co.id,co.fname,co.mname,co.lname FROM contacts co JOIN customer_account ca  ON ca.contactid=co.id WHERE ca.tourid=$TheTourID AND co.id !=$CustomerIDquery");
								while($TravelersRow = mysqli_fetch_array($Add_Traveler_Result))
								{
									$CurrCustID = $TravelersRow['id'];
									$CheckingCustomerJointInvoiceResult = mysqli_query($con6,"SELECT Primary_Customer_ID FROM customer_groups WHERE Group_ID=$TheTourID AND Type='Group Invoice' AND CONCAT_WS('', Primary_Customer_ID, Additional_Traveler_ID_1, Additional_Traveler_ID_2, Additional_Traveler_ID_3, Additional_Traveler_ID_4) LIKE '%$CurrCustID%'");
									$CheckingCustomerJointInvoiceCount = mysqli_num_rows($CheckCustomerGroupExist);
									if($CheckingCustomerJointInvoiceCount > 0) {
										$CheckingCustomerJointInvoiceData = mysqli_fetch_array($CheckingCustomerJointInvoiceResult);
										if($CheckingCustomerJointInvoiceData['Primary_Customer_ID'] > 0) {$DisabledSelect = " disabled";} else { $DisabledSelect = ""; }
										$selected = ''.$DisabledSelect;
									}
									if($Addition7==$TravelersRow['id']){$selected = 'selected';};
									echo "<option value='".$TravelersRow['id']."' $selected>".$TravelersRow['fname']." ".$TravelersRow['mname']." ".$TravelersRow['lname']."</option>";
								} ?>
							</select>
						</div>
						<div class="col-sm-3 pr-0 mb-2">
							<label class="float-label">Additional Traveler #8</label>
							<input type="text" name="TravelerCurrent8" class="form-control" value="<?php echo $Addition8; ?>" hidden>
							<?php if($DisplayDisabled == "disabled") {?><input type="text" name="Traveler3" class="form-control" value="<?php echo $Addition8; ?>" hidden><?php } ?>
							<select name="Traveler8<?php if($DisplayDisabled == "disabled") {echo "xXx";} ?>" class="form-control form-control-default <?php if($PrimaryID == $leadid) {echo '"';} else {echo 'text-dark" style="opacity:1;"';} ?> <?php echo $DisplayDisabled; ?>>
								<option value="0" selected> </option>
								<?php 
								$Add_Traveler_Result = mysqli_query($db,"SELECT co.id,co.fname,co.mname,co.lname FROM contacts co JOIN customer_account ca  ON ca.contactid=co.id WHERE ca.tourid=$TheTourID AND co.id !=$CustomerIDquery");
								while($TravelersRow = mysqli_fetch_array($Add_Traveler_Result))
								{
									$CurrCustID = $TravelersRow['id'];
									$CheckingCustomerJointInvoiceResult = mysqli_query($con6,"SELECT Primary_Customer_ID FROM customer_groups WHERE Group_ID=$TheTourID AND Type='Group Invoice' AND CONCAT_WS('', Primary_Customer_ID, Additional_Traveler_ID_1, Additional_Traveler_ID_2, Additional_Traveler_ID_3, Additional_Traveler_ID_4) LIKE '%$CurrCustID%'");
									$CheckingCustomerJointInvoiceCount = mysqli_num_rows($CheckCustomerGroupExist);
									if($CheckingCustomerJointInvoiceCount > 0) {
										$CheckingCustomerJointInvoiceData = mysqli_fetch_array($CheckingCustomerJointInvoiceResult);
										if($CheckingCustomerJointInvoiceData['Primary_Customer_ID'] > 0) {$DisabledSelect = " disabled";} else { $DisabledSelect = ""; }
										$selected = ''.$DisabledSelect;
									}
									if($Addition8==$TravelersRow['id']){$selected = 'selected';};
									echo "<option value='".$TravelersRow['id']."' $selected>".$TravelersRow['fname']." ".$TravelersRow['mname']." ".$TravelersRow['lname']."</option>";
								} ?>
							</select>
						</div>
					</div>
					<div class="col-sm-12"><br/></div>
					<div class="col-sm-4">
						<h4 class="sub-title">Purchased Separate Ticket?</h4>
						<div class="can-toggle">
							<?php	//To get the Ticket Airline Credit Amount
							$TicketCreditDiscountResult = mysqli_query($db,"SELECT pro.Product_Reference FROM products pro
							JOIN groups gro ON gro.airline_productid=pro.id WHERE gro.tourid=$TheTourID");
							$TicketCreditDiscount = mysqli_fetch_array($TicketCreditDiscountResult);
							$TicketProductID = $TicketCreditDiscount['Product_Reference'];
							$TicketCreditDiscountResult2 = mysqli_query($db,"SELECT id,price,name,supplierid FROM products WHERE id=$TicketProductID");
							$TicketCreditDiscount2 = mysqli_num_rows($TicketCreditDiscountResult2);
							if($TicketCreditDiscount2 > 0) {
								$TicketCreditData = mysqli_fetch_array($TicketCreditDiscountResult2);
								$thisticketProID = $TicketCreditData['id'];
								$thisticketprice = $TicketCreditData['price']; 
								$ThisTicketProductName = $TicketCreditData['name']; 
								$ThisTicketProductSupplier = $TicketCreditData['supplierid']; 
							} else {
								$thisticketProID = $TicketProductID;
								$thisticketprice = 0; 
								$ThisTicketProductName = NULL; 
								$ThisTicketProductSupplier = 0; 
							}
							?>
							<input id="hisownticket<?php echo $TheTourID; ?>" name="hisownticket" value="1" type="checkbox" <?php if($CustomerAccountRow['hisownticket'] == 1) {echo "checked";} ?>>
							<label for="hisownticket<?php echo $TheTourID; ?>">
								<div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
							</label>
						</div>
					</div>
					<div id="hisownticketwarning<?php echo $TheTourID; ?>" class="col-sm-8" style="display:<?php if($CustomerAccountRow['hisownticket'] == 1) {echo "block";} else {echo "none";} ?>;">
						<input name="hisownticket_ID" type="text" value="<?php echo $thisticketProID; ?>" hidden>
						<input name="hisownticket_price" type="text" value="<?php echo $thisticketprice; ?>" hidden>
						<input name="hisownticket_Name" type="text" value="<?php echo $ThisTicketProductName; ?>" hidden>
						<input name="hisownticket_Supplier" type="text" value="<?php echo $ThisTicketProductSupplier; ?>" hidden>
						<?php if(abs($thisticketprice) > 0) { ?>
						<p style="text-align:justify;">NOTICE: <span style="font-size:18px;font-weight:bold;color:darkred;">$<?php echo abs($thisticketprice); ?>.00</span> will be deducted from the customer’s total due to airfare purchase from a third party. <span style="font-weight:bold;">To edit this amount, <a style="font-size:13px;" href="groups-edit.php?id=<?php echo $CustomerAccountRow['tourid']; ?>&action=edit&tab=airline">click here.</a></span></p>
						<?php } else { ?>
						<p style="text-align:justify;"><span class="text-danger" style="font-weight:bold;">WARNING:</span> There is no Ticket Credit Discount for this product. To edit this amount, <span style="font-weight:bold;"><a style="font-size:13px;" href="groups-edit.php?id=<?php echo $CustomerAccountRow['tourid']; ?>&action=edit&tab=airline">click here.</a></span></p>
						<?php } ?>
					</div>
					<div id="hisownticketbox<?php echo $TheTourID; ?>" class="col-sm-12" style="display:<?php if($CustomerAccountRow['hisownticket'] == 1) {echo "block";} else {echo "none";} ?>;">
						<div class="row mb-4">
							<div class="col-sm-3">
								<h4 class="sub-title">E-Ticket #</h4>
								<input type="text" name="Ticket_Number" class="form-control" value="<?php echo $data['Ticket_Number'];?>">
							</div>
							<div class="col-sm-3">
								<h4 class="sub-title">Ticket Confirmation #</h4>
								<input type="text" name="Confirmation_Number" class="form-control" value="<?php echo $data['Confirmation_Number'];?>">
							</div>
							<div class="col-sm-3">
								<h4 class="sub-title">Apply To Invoice?</h4>
								<div style="float:left;">No</div>
								<div class="material-switch pull-right mt-1 ml-2 mr-3" style="float:left;">
									<input id="Apply_Credit<?php echo $TheTourID; ?>" name="Apply_Credit" value="1" type="checkbox" <?php if($CustomerAccountRow['Apply_Credit'] == 1) {echo "checked";} ?>>
									<label for="Apply_Credit<?php echo $TheTourID; ?>" class="label-info"></label>
								</div>
								<div style="float:left;">Yes</div>
							</div>
							<div class="col-sm-3">
								<h4 class="sub-title">Same as Group's Itinerary?</h4>
								<div style="float:left;">No</div>
								<div class="material-switch pull-right mt-1 ml-2 mr-3" style="float:left;">
									<input id="Same_Itinerary<?php echo $TheTourID; ?>" name="Same_Itinerary" value="1" type="checkbox" <?php if($CustomerAccountRow['Same_Itinerary'] == 1) {echo "checked";} ?>>
									<label for="Same_Itinerary<?php echo $TheTourID; ?>" class="label-info"></label>
								</div>
								<div style="float:left;">Yes</div>
							</div>
						</div>
						<?php // START *********** GROUP FLIGHT INFO SECTION *************** // ?>
						<label id="FlightsTableTitle<?php echo $TheTourID; ?>" class="float-label">Please list the custom airfare flight information for this customer</label>
						<input name="airlinez" value="<?php echo $data["tourid"]; ?>" type="text" hidden>
						<input name="airlineproductidz" value="<?php echo $data["airline_productid"]; ?>" type="text" hidden>
						<table id="FlightsTable<?php echo $TheTourID; ?>" class="<?php if($CustomerAccountRow["Same_Itinerary"] == 1) {echo "d-none";} ?> flightstable table table-striped">
							<thead>
								<tr>
									<th style="width:10%; font-size: 14px;">
										<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Airline Code">
											Airline
										</span>
									</th>
									<th style="width:9%; font-size: 14px;">
										<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Flight Number">
											Flight #
										</span>
									</th>
									<th style="width:23%; font-size: 14px;">
										<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Departure Date & Time">
											DEP Time
										</span>
									</th>
									<th style="width:12%; font-size: 14px;">
										<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="The Departure Airport">
											D. Airport
										</span>
									</th>
									<th style="width:23%; font-size: 14px;">
										<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Arrival Date & Time">
											Arrival Time
										</span>
									</th>
									<th style="width:12%; font-size: 14px;">
										<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="The Arrival Airport">
											A. Airport
										</span>
									</th>
									<th style="width:11%; font-size: 14px;">
										<span data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Triggers for the Supplier">
											Trigger
										</span>
									</th>
								</tr>
							</thead>
							<tbody>
							<?php $FlightCounter = 1; 
							
							$FlightDB = new mysqli($servername, $username, $password, $dbname);
							$FlightSQL = mysqli_query($FlightDB,"SELECT gf.*, ga.Airline_Code, ga.Airline_Name ,gp.Airport_Code AS Departure_Airport_Code, gp.Airport_City AS Departure_Airport_City,gp2.Airport_Code AS Arrival_Airport_Code, gp2.Airport_City AS Arrival_Airport_City FROM groups_flights gf 
																LEFT JOIN groups_airline ga
																ON gf.Flight_Airline_ID=ga.Airline_ID
																LEFT JOIN groups_airport gp
																ON gf.Departure_Airport=gp.Airport_ID
																LEFT JOIN groups_airport gp2
																ON gf.Arrival_Airport=gp2.Airport_ID
																WHERE gf.Group_ID=$TheTourID AND gf.Specific_Customer_ID=$leadid
																ORDER BY gf.GF_Line_ID ASC");
							$FlightDataCount = mysqli_num_rows ($FlightSQL);
							if($FlightDataCount == 0) {$FlightCounterLimit = 4;} elseif($FlightDataCount == 4) {$FlightCounterLimit = 4;} else { $FlightCounterLimit = $FlightDataCount+1;}
							while($FlightCounter <= $FlightCounterLimit) {
							
							$FlightData = mysqli_fetch_array($FlightSQL);
							?>
								<tr>
									<td class="pt-1 pb-1 select2Airportz">
									<input class="form-control" type="text" name="GF_Line_ID<?php echo $FlightCounter; ?>" value="<?php echo $FlightCounter; ?>" hidden>
										<select class="form-control select2-airfare col-sm-12" name="AirlineID<?php echo $FlightCounter; ?>" multiple="multiple" >
											<?php if($FlightData['Flight_Airline_ID'] != NULL) { echo "<option value='".$FlightData['Flight_Airline_ID']."' title='".$FlightData['Airline_Name']." (".$FlightData['Airline_Code'].")' selected>".$FlightData['Airline_Code']."</option>";} ?>
										</select>
									</td>
									<td class="pt-1 pb-1">
									<input class="form-control" type="text" name="AirlineFlightID<?php echo $FlightCounter; ?>" value="<?php echo $FlightData["Flight_Airline_Number"]; ?>">
									</td>
									<td class="pt-1 pb-1">																	
										<div class="input-group date mb-0 DEPtime" data-date="<?php echo date("Y-m-d")."T".date("h:i:s")."Z"; ?>" data-date-format="M dd @ HH:iip" data-link-field="<?php echo $FlightCounter; ?>dtp_input1">
											<input readonly class="form-control fill" size="16" type="text" value="<?php if($FlightData["Departure_Time"] != NULL) { $DepTime = strtotime( $FlightData["Departure_Time"] ); echo date( 'M d @ h:ia', $DepTime );} ?>">
											<span class="input-group-text input-group-addon" style="border-radius: 0px 5px 5px 0px;border-left:0px;"><span class="glyphicon glyphicon-th"></span></span>
										</div>
										<input type="hidden" id="<?php echo $FlightCounter; ?>dtp_input1" name="DEPtime<?php echo $FlightCounter; ?>" value="<?php echo $FlightData["Departure_Time"]; ?>" />
									</td>
									<td class="pt-1 pb-1 select2Airportz">
										<select class="form-control select2-airport col-sm-12" name="DepartureAirportID<?php echo $FlightCounter; ?>" multiple="multiple" >
											<?php if($FlightData['Departure_Airport'] != NULL) { echo "<option value='".$FlightData['Departure_Airport']."' title='".$FlightData['Departure_Airport_City']." (".$FlightData['Departure_Airport_Code'].")' selected>".$FlightData['Departure_Airport_Code']."</option>";} ?>
										</select>
									</td>
									<td class="pt-1 pb-1">
										<div class="input-group date mb-0 ARRtime" data-date="<?php echo date("Y-m-d")."T".date("h:i:s")."Z"; ?>" data-date-format="M dd @ HH:iip" data-link-field="<?php echo $FlightCounter; ?>dtp_input2">
											<input readonly class="form-control fill" size="16" type="text" value="<?php if($FlightData["Arrival_Time"] != NULL) { $ArrivTime = strtotime( $FlightData["Arrival_Time"] ); echo date( 'M d @ h:ia', $ArrivTime );} ?>">
											<span class="input-group-text input-group-addon" style="border-radius: 0px 5px 5px 0px;border-left:0px;"><span class="glyphicon glyphicon-th"></span></span>
										</div>
										<input type="hidden" id="<?php echo $FlightCounter; ?>dtp_input2" name="ARRtime<?php echo $FlightCounter; ?>" value="<?php echo $FlightData["Arrival_Time"]; ?>" />
									</td>
									<td class="pt-1 pb-1 select2Airportz">
										<select class="form-control select2-airport col-sm-12" name="ArrivalAirportID<?php echo $FlightCounter; ?>" multiple="multiple" >
											<?php if($FlightData['Arrival_Airport'] != NULL) { echo "<option value='".$FlightData['Arrival_Airport']."' title='".$FlightData['Arrival_Airport_City']." (".$FlightData['Arrival_Airport_Code'].")' selected>".$FlightData['Arrival_Airport_Code']."</option>";} ?>
										</select>
									</td>
									<td class="pt-1 pb-1">
										<span data-toggle="tooltip" data-placement="top" title="" data-original-title="The group is arriving the destination">
											<img src="<?php echo $crm_images; ?>/plane-A.png" width="15" alt="Flight Arrival" /> <input type="radio" name="SupplierTrigger<?php echo $FlightCounter; ?>" value="1" <?php if($FlightData["Supplier_Trigger"] == 1) { echo "checked"; } ?>>
										</span>
										<span data-toggle="tooltip" data-placement="top" title="" data-original-title="The group is leaving the destination">
											<img src="<?php echo $crm_images; ?>/plane-D.png" width="15" alt="Flight Departure" /> <input type="radio" name="SupplierTrigger<?php echo $FlightCounter; ?>" value="2" <?php if($FlightData["Supplier_Trigger"] == 2) { echo "checked"; } ?>>
										</span>
										<span data-toggle="tooltip" data-placement="top" title="" data-original-title="This is a normal flight row">
											<img src="<?php echo $crm_images; ?>/none.png" width="15" alt="None" /> <input type="radio" name="SupplierTrigger<?php echo $FlightCounter; ?>" value="0" <?php if($FlightData["Supplier_Trigger"] == 0 OR $FlightData["Supplier_Trigger"] == "") { echo "checked"; } ?>>
										</span>
									</td>
								</tr>
							<?php  $FlightCounter++; } ?>
							</tbody>
						</table>
<script type="text/javascript"> 
/*let lineNo = <?php echo $FlightCounter; ?>; 
$(document).ready(function () { 
	$(".add-row").click(function () { 
		//markup = "<tr><td  class='pt-1 pb-1'>row "+ lineNo +"</td><td  class='pt-1 pb-1'></td><td  class='pt-1 pb-1'></td><td  class='pt-1 pb-1'></td><td  class='pt-1 pb-1'></td><td  class='pt-1 pb-1'></td><td  class='pt-1 pb-1'></td><td  class='pt-1 pb-1'></td></tr>"; 
		markup = "<tr><td class='pt-1 pb-1 select2Airportz'><input class='form-control' type='text' name='GF_Line_ID"+ lineNo +"' value='"+ lineNo +"' hidden><select class='form-control select2-airfare col-sm-12' name='AirlineID"+ lineNo +"' multiple='multiple' ></select></td><td class='pt-1 pb-1'><input class='form-control' type='text' name='AirlineFlightID"+ lineNo +"' value=''></td><td class='pt-1 pb-1 select2Airportz'><select class='form-control select2-airport col-sm-12' name='DepartureAirportID"+ lineNo +"' multiple='multiple' ></select></td><td class='pt-1 pb-1'><div class='input-group date mb-0 DEPtime"+ lineNo +"' data-date='<?php echo date('Y-m-d').'T'.date('h:i:s').'Z'; ?>' data-date-format='M dd @ HH:iip' data-link-field='"+ lineNo +"dtp_input1'><input readonly class='form-control fill' size='16' type='text' value=''><span class='input-group-text input-group-addon' style='border-radius: 0px 5px 5px 0px;border-left:0px;'><span class='glyphicon glyphicon-th'></span></span></div><input type='hidden' id='"+ lineNo +"dtp_input1' name='DEPtime"+ lineNo +"' value='' /></td><td class='pt-1 pb-1 select2Airportz'><select class='form-control select2-airport col-sm-12' name='ArrivalAirportID"+ lineNo +"' multiple='multiple' ></select></td><td class='pt-1 pb-1'><div class='input-group date mb-0 ARRtime"+ lineNo +"' data-date='<?php echo date('Y-m-d').'T'.date('h:i:s').'Z'; ?>' data-date-format='M dd @ HH:iip' data-link-field='"+ lineNo +"dtp_input2'><input readonly class='form-control fill' size='16' type='text' value=''><span class='input-group-text input-group-addon' style='border-radius: 0px 5px 5px 0px;border-left:0px;'><span class='glyphicon glyphicon-th'></span></span></div><input type='hidden' id='"+ lineNo +"dtp_input2' name='ARRtime"+ lineNo +"' value='' /></td><td class='pt-1 pb-1'><span data-toggle='tooltip' data-placement='top' title='' data-original-title='The group is arriving the destination'><img src='<?php echo $crm_images; ?>/plane-A.png' width='15' alt='Flight Arrival' /> <input type='radio' name='SupplierTrigger"+ lineNo +"' value='1'></span><span data-toggle='tooltip' data-placement='top' title='' data-original-title='The group is leaving the destination'><img src='<?php echo $crm_images; ?>/plane-D.png' width='15' alt='Flight Departure' /> <input type='radio' name='SupplierTrigger"+ lineNo +"' value='2'></span></td></tr>"; 
		tableBody = $("table.flightstable tbody"); 
		tableBody.append(markup); 
		lineNo++; 
	}); 
}); */
	
$('.DEPtime').datetimepicker({
	weekStart: 1, todayBtn:  1, autoclose: 1, todayHighlight: true, startView: 4, forceParse: 0, minuteStep: 1, showMeridian: 1
});
$('.DEPtime').datetimepicker('setStartDate', '<?php echo date("Y-m-d"); ?>');
$('.ARRtime').datetimepicker({
	weekStart: 1, todayBtn:  1, autoclose: 1, todayHighlight: true, startView: 4, forceParse: 0, minuteStep: 1, showMeridian: 1
});
$('.ARRtime').datetimepicker('setStartDate', '<?php echo date("Y-m-d"); ?>');
</script> 
						<?php // END *********** GROUP FLIGHT INFO SECTION *************** // ?>

						<div class="row">
							<div class="col-sm-3">
								<h4 class="sub-title">Need Transfer?</h4>
								<div class="material-switch pull-right ml-4">
									<input id="NeedTransfer<?php echo $TheTourID; ?>" name="transfer" value="1" type="checkbox" <?php if($CustomerAccountRow['Transfer'] == 1) {echo "checked";} ?>>
									<label for="NeedTransfer<?php echo $TheTourID; ?>" class="label-info"></label>
								</div>
							</div>
							<div id="transferproductselect<?php echo $TheTourID; ?>" style="display:<?php if($CustomerAccountRow['Transfer'] == 1) {echo "block";} else {echo "none";} ?>;" class="form-group form-default form-static-label col-sm-9">
								<label class="float-label">Choose A Transfer Product</label>
								<select id="TransferProduct<?php echo $TheTourID; ?>" name="TheTransferProduct" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
									<option value="0"></option>
									<option value="Add New">Add New</option>
									<?php 
									$TransferResult = mysqli_query($db,"SELECT id, name, price FROM products WHERE status=1 AND categoryid=9");
									while($TransferRow = mysqli_fetch_array($TransferResult))
									{
										$selected = '';
										if($CustomerAccountRow['Transfer_Product_ID']==$TransferRow['id']){$selected = 'selected';};
										echo "<option value='".$TransferRow['id']."' $selected>".$TransferRow['name']." | $".$TransferRow['price']."</option>";
									} ?>
								</select>
							</div>
						</div>
					</div>
					<div class="col-sm-12"><br/></div>
					<div class="col-sm-4">
						<h4 class="sub-title">Need upgrade?</h4>
						<div class="can-toggle">
							<input id="upgrade<?php echo $TheTourID; ?>" name="upgrade" value="1" type="checkbox" <?php if($CustomerAccountRow['upgrade'] == 1) {echo "checked";} ?>>
							<label for="upgrade<?php echo $TheTourID; ?>">
								<div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
							</label>
						</div>
					</div>
					<?php 
					$SelectAirSupplierResult = mysqli_query($db,"SELECT gro.airline_productid,pro.name,pro.supplierid FROM groups gro
					JOIN products pro 
					ON gro.airline_productid=pro.id
					WHERE gro.tourid=$TheTourID");
					$SelectAirSupplierID = mysqli_fetch_array($SelectAirSupplierResult);
					$AirSupplierID = $SelectAirSupplierID['supplierid'];
					if($AirSupplierID > 0) { ?>
					<div id="upgradeproductselect<?php echo $TheTourID; ?>" style="display:<?php if($CustomerAccountRow['upgrade'] == 1) {echo "block";} else {echo "none";} ?>;" class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Choose An Upgrade Airline Product</label>
						<select id="AirUpgrade" name="theupgrade" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<option value="0"></option>
							<option value="Add New">Add New</option>
							<?php 
							$upgraderesult = mysqli_query($db,"SELECT id, name, price FROM products WHERE status=1 AND categoryid=4 AND supplierid=$AirSupplierID");
							while($upgraderow = mysqli_fetch_array($upgraderesult))
							{
								$selected = '';
								if($CustomerAccountRow['upgradeproduct']==$upgraderow['id']){$selected = 'selected';};
								echo "<option value='".$upgraderow['id']."' $selected>".$upgraderow['name']." | $".$upgraderow['price']."</option>";
							} ?>
						</select>
					</div>
					<?php } else { ?>
					<div class="col-sm-4">
						<div id="upgradeproductselect" style="display:<?php if($CustomerAccountRow['upgrade'] == 1) {echo "block";} else {echo "none";} ?>;">
							<label class="float-label">Choose An Upgrade Airline Product</label>
							<br />
							<span style="font-weight:700;color:#970b0b;font-size:13px;">Please select an Airline product for the group first!</span>
						</div>
					</div>
					<?php } 
					$SelectLandSupplierResult = mysqli_query($db,"SELECT gro.land_productid,pro.name,pro.supplierid FROM groups gro
					JOIN products pro
					ON gro.land_productid=pro.id
					WHERE gro.tourid=$TheTourID");
					$SelectLandSupplierID = mysqli_fetch_array($SelectLandSupplierResult);
					$LandSupplierID = $SelectLandSupplierID['supplierid'];
					if($LandSupplierID > 0) { ?>
					<div id="upgradeproductselect2<?php echo $TheTourID; ?>" style="display:<?php if($CustomerAccountRow['upgrade'] == 1) {echo "block";} else {echo "none";} ?>;" class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Choose An Upgrade Land Product</label>
						<select id="LandUpgrade" name="thelandupgrade" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<option value="0"></option>
							<option value="Add New">Add New</option>
							<?php 
							$upgraderesult = mysqli_query($db,"SELECT id, name, price FROM products WHERE status=1 AND categoryid=5 AND supplierid=$LandSupplierID");
							while($upgraderow = mysqli_fetch_array($upgraderesult))
							{
								$selected = '';
								if($CustomerAccountRow['upgrade_land_product']==$upgraderow['id']){$selected = 'selected';};
								echo "<option value='".$upgraderow['id']."' $selected>".$upgraderow['name']." | $".$upgraderow['price']."</option>";
							} ?>
						</select>
					</div>
					<?php } else { ?>
					<div class="col-sm-4">
						<div id="upgradeproductselect2<?php echo $TheTourID; ?>"  style="display:<?php if($CustomerAccountRow['upgrade'] == 1) {echo "block";} else {echo "none";} ?>;">
							<label class="float-label">Choose An Upgrade Land Product</label>
							<br />
							<span style="font-weight:700;color:#970b0b;font-size:13px;">Please select a Land product for the group first!</span>
						</div>
					</div>
					<?php } ?> 
					<div class="col-sm-12"><br/></div>
					<div class="col-sm-4">
						<h4 class="sub-title">Need Extension?</h4>
						<div class="can-toggle">
							<input id="extension<?php echo $TheTourID; ?>" name="extension" value="1" type="checkbox" <?php if($CustomerAccountRow['Extension'] == 1) {echo "checked";} ?>>
							<label for="extension<?php echo $TheTourID; ?>">
								<div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
							</label>
						</div>
					</div>
					<div id="extensionproductselect<?php echo $TheTourID; ?>" style="display:<?php if($CustomerAccountRow['Extension'] == 1) {echo "block";} else {echo "none";} ?>;" class="form-group form-default form-static-label col-sm-8">
						<label class="float-label">Choose An Extension Product</label>
						<select id="ExtensionProduct<?php echo $TheTourID; ?>" name="TheExtensionProduct" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<option value="0"></option>
							<option value="Add New">Add New</option>
							<?php 
							$ExtensionResult = mysqli_query($db,"SELECT id, name, price FROM products WHERE status=1 AND categoryid=8");
							while($ExtensionRow = mysqli_fetch_array($ExtensionResult))
							{
								$selected = '';
								if($CustomerAccountRow['Extension_Product_ID']==$ExtensionRow['id']){$selected = 'selected';};
								echo "<option value='".$ExtensionRow['id']."' $selected>".$ExtensionRow['name']." | $".$ExtensionRow['price']."</option>";
							} ?>
						</select>
					</div>
					<div class="col-sm-12"><br/></div>
					<div class="col-sm-4">
						<h4 class="sub-title">Need Insurance?</h4>
						<div class="can-toggle">
							<input id="insurance<?php echo $TheTourID; ?>" name="insurance" value="1" type="checkbox" <?php if($CustomerAccountRow['insurance'] == 1) {echo "checked";} ?>>
							<label for="insurance<?php echo $TheTourID; ?>">
								<div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
							</label>
						</div>
					</div>
					<div id="insuranceproductselect<?php echo $TheTourID; ?>" style="display:<?php if($CustomerAccountRow['insurance'] == 1) {echo "block";} else {echo "none";} ?>;" class="form-group form-default form-static-label col-sm-8">
						<label class="float-label">Choose An Insurance Product</label>
						<select id="InsuranceProduct<?php echo $TheTourID; ?>" name="TheInsuranceProduct" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<option value="0"></option>
							<option value="Add New">Add New</option>
							<?php 
							$InsuranceResult = mysqli_query($db,"SELECT id, name, price FROM products WHERE status=1 AND categoryid=3");
							while($InsuranceRow = mysqli_fetch_array($InsuranceResult))
							{
								$selected = '';
								if($CustomerAccountRow['Insurance_Product_ID']==$InsuranceRow['id']){$selected = 'selected';};
								echo "<option value='".$InsuranceRow['id']."' $selected>".$InsuranceRow['name']." | $".$InsuranceRow['price']."</option>";
							} ?>
						</select>
					</div>
					<div class="col-sm-12"><br/></div>
					<div class="col-sm-4">
						<h4 class="sub-title">Special Requests?</h4>
						<div class="can-toggle">
							<input id="specialrequest<?php echo $TheTourID; ?>" name="specialrequest" value="1" type="checkbox" <?php if($CustomerAccountRow['specialrequest'] == 1) {echo "checked";} ?>>
							<label for="specialrequest<?php echo $TheTourID; ?>">
								<div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
							</label>
						</div>
					</div>
					<div class="col-sm-4">
						<div id="specialrequestfield<?php echo $TheTourID; ?>" style="display:<?php if($CustomerAccountRow['specialrequest'] == 1) {echo "block";} else {echo "none";} ?>;">
						<label class="float-label">Airline Special Requests</label>
						<input type="text" name="specialtext" class="form-control" value="<?php echo $CustomerAccountRow['specialtext'];?>">
						</div>
					</div>
					<div class="col-sm-4">
						<div id="specialrequestfield2<?php echo $TheTourID; ?>" style="display:<?php if($CustomerAccountRow['specialrequest'] == 1) {echo "block";} else {echo "none";} ?>;">
						<label class="float-label">Land Operations Special Requests</label>
						<input type="text" name="speciallandtext" class="form-control" value="<?php echo $CustomerAccountRow['special_land_text'];?>">
						</div>
					</div>
					<div class="col-sm-12"><br/></div>
					<div class="col-sm-4">
						<h4 class="sub-title">Any Meals or Allergies requests?</h4>
						<div class="can-toggle">
							<input id="mealsrequest<?php echo $TheTourID; ?>" name="Meals_Allergies" value="1" type="checkbox" <?php if($CustomerAccountRow['Meals_Allergies'] == 1) {echo "checked";} ?>>
							<label for="mealsrequest<?php echo $TheTourID; ?>">
								<div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
							</label>
						</div>
					</div>
					<div class="col-sm-4">
						<div id="mealsrequestfield<?php echo $TheTourID; ?>" style="display:<?php if($CustomerAccountRow['Meals_Allergies'] == 1) {echo "block";} else {echo "none";} ?>;">
						<label class="float-label">Meals Requests</label>
						<input type="text" name="Meals_Request" class="form-control" value="<?php echo $CustomerAccountRow['Meals_Request'];?>">
						</div>
					</div>
					<div class="col-sm-4">
						<div id="mealsrequestfield2<?php echo $TheTourID; ?>" style="display:<?php if($CustomerAccountRow['Meals_Allergies'] == 1) {echo "block";} else {echo "none";} ?>;">
						<label class="float-label">Allergies Requests</label>
						<input type="text" name="Allergies_Request" class="form-control" value="<?php echo $CustomerAccountRow['Allergies_Request'];?>">
						</div>
					</div>
					<div class="form-group col-sm-12"><br />
						<input type="submit" name="UpdateTheCustomer"
                               value="Update" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal">
					</div>
				</form>
				</div>
			</div>
		</div>
		<?php 
		include "metadata-js-customer.php";
		$LoopCounter++;
		}} ?>
	</div>
</div>

<script type="text/javascript">
    function showContactSubmitPopup(obj) {
        if( obj.form.reportValidity() ) { 
            $('#resultsmodal').modal('show'); 
            return true ;
        }
        return false ;
    }
</script>