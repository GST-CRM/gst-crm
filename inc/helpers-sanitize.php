<?php

$__GET =$_GET ;
$__POST =$_POST ;
$__REQUEST =$_REQUEST ;

//Get variable
try {
foreach ($_GET as $key => $value) {
    if(is_scalar($key)) {
        $key = mysqli_real_escape_string($db, $key);
    }
    if(is_scalar($value)) {
        $value = mysqli_real_escape_string($db, $value);
    }
    
    if(is_scalar($value)) {
        $__GET[$key] = $value ;
    }
}
}
catch(Exception $e) {
    $__GET = $_GET ;
}
//POst variable
try {
foreach ($_POST as $key => $value) {
    if(is_scalar($key)) {
        $key = mysqli_real_escape_string($db, $key);
    }
    
    if(is_scalar($value)) {
        $value = mysqli_real_escape_string($db, $value);
    }
    
    
    $__POST[$key] = $value ;
}
}
catch(Exception $e) {
    $__POST = $_POST ;
}
//Request variable
try {
foreach ($_REQUEST as $key => $value) {
    if(is_scalar($key)) {
        $key = mysqli_real_escape_string($db, $key);
    }
    
    if(is_scalar($value)) {
        $value = mysqli_real_escape_string($db, $value);
    }
    
    $__REQUEST[$key] = $value ;
}}
catch(Exception $e) {
    $__REQUEST = $_REQUEST ;
}