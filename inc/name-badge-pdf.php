<?php
// Include the main TCPDF library (search for installation path).
require_once('../files/pdf/tcpdf.php');

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Element Media');
$pdf->SetTitle('Name Badge');
$pdf->SetSubject('Name Badge');
//$pdf->SetKeywords('GST, CRM, Element, Media, Element.ps');

// remove default header/footer
$PDF_HEADER = isset($PDF_HEADER) ? $PDF_HEADER : false ;
$pdf->setPrintHeader($PDF_HEADER);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/../lang/eng.php')) {
    require_once(dirname(__FILE__).'/../lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
$pdf->SetMargins(4, 12, 5);

// set font
$pdf->SetFont('Helvetica', '', 10);


    // add a page
$pdf->AddPage('P','LETTER');

// set margins
$pdf->setPageMark() ;

$invoiceNumSet = [] ;

if( isset($_REQUEST['cbInvoice']) && isset($_REQUEST['batch-action']) && $_REQUEST['batch-action'] == 'P') {
    $invoiceNumSet = $_REQUEST['cbInvoice'] ;
}
else if( isset($_REQUEST['batch-action']) && $_REQUEST['batch-action'] == 'S') {
    $invoiceNumSet = [ $invoiceId ] ;
}
ob_start();
if( ! isset($invoiceNumSet) || empty($invoiceNumSet) ) {
    $invoiceNumSet = [ $_REQUEST['id'] ] ;
}
$p = 0 ;
$invoiceNumSet = array_unique($invoiceNumSet) ;
foreach( $invoiceNumSet as $invoiceId ) {

    if( $p > 0 ) {
        echo '<br pagebreak="true" style="height:0;" />' ;
    }
    include 'name-badge-pdf-content.php';
    $p ++ ;
}
$pdfData = ob_get_clean() ;

// output the HTML content
$pdf->writeHTML($pdfData, true, false, true, false, '');


// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

$PDF_OUTPUT = isset($PDF_OUTPUT ) ? $PDF_OUTPUT : 'I' ;
//Close and output PDF document
ob_end_clean();
return $pdf->Output('Name-Badge.pdf', $PDF_OUTPUT );

//============================================================+
// END OF FILE
//============================================================+