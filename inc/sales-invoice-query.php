<?php

include_once __DIR__ . '/../models/customer_payment.php' ;

//query filter for summary box chosen.
$filter = '' ;
$class = '' ;
$summaryClass = '' ;
if( isset($__GET['filter']) ) {
    $class = $__GET['filter'] ;
    if( $__GET['filter'] == 'total_due' ) {
        $filter = " HAVING Invoice_Amount > total " ;
    } else if( $__GET['filter'] == 'not_yet_due' ) {
        $filter = " HAVING Invoice_Amount > total AND Due_Date > CURDATE() " ;
    } else if( $__GET['filter'] == 'over_due' ) {
        $filter = " HAVING Invoice_Amount > total AND Due_Date <= CURDATE() " ;
    } else if( $__GET['filter'] == 'paid' ) {
        $filter = " HAVING total > 0 " ;
    }
}


// { Acl Condition
$GroupID = AclPermission::userGroup() ;
$contactid = AclPermission::userId() ;
$ACL_CONDITION = "" ;
if( AclPermission::roleFlags() & AclRole::$FLAG_ONLY_GROUP ) {
    $ACL_CONDITION = "" ;
}
else if( AclPermission::roleFlags() & AclRole::$FLAG_ONLY_MINE ) {
    $ACL_CONDITION = " AND ca.contactid=$contactid " ;
}
// }
// 
//common query used for sales invoice an group invoice. this condition identifies 
//it and choses the condition for right page.
$whereCondition = '' ;
if( basename($_SERVER['PHP_SELF']) == "groups-edit.php" && $tourid ) {
    $whereCondition = " AND ca.tourid =$tourid $ACL_CONDITION " ;
}
else {
    //sales-invoice page filter by tours ids of agent.
    $whereCondition = $ACL_CONDITION ;
}
//get sale invoice list.
$records = [] ;
if( $_SERVER['PHP_SELF'] == "groups-edit.php" ) {
    $records = (new CustomerPayment())->paymentListing($whereCondition, $filter) ;
}

$summary = (new CustomerPayment())->paymentSummary($whereCondition) ;
       
$TotalDue = $summary['summary']['due'] ;
$NotYetDue = $summary['summary']['not_yet_due'] ;
$OverDue = $summary['summary']['over_due'] ;
$TotalAmount = $summary['summary']['paid'] ;

if( $TotalDue != 0 ) {
    $first = intval( floor($NotYetDue * 100 / $TotalDue) ) ;
    $second = intval( ceil( $OverDue * 100 / $TotalDue) ) ;
    
}
else {
    $first = 0 ;
    $second = 0 ;
}
if( ($TotalAmount + $TotalDue) != 0 ) {
    $third = $TotalDue * 100 / ($TotalAmount + $TotalDue) ;
}
else {
    $third = 0 ;
}

if( $first > 100 || $second > 100 || $first <= 0 || $second <= 0 ) {
    $first = 0;
    $second = 0;
}


if( $third > 100 ) {
    $third = 100;
}
else if( $third <= 0 ) {
    $third = 0 ;
}
//}