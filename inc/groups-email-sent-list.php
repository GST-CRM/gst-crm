
<style type="text/css">
    @media screen and (max-width: 568px) {
      .flex-direction-media { 
        flex-direction: column !important;
      }
      .flex-direction-media li a { 
        float: none !important;
      }
    }
</style>
<div class="col-12"><br /></div>

<?php 


if( ! AclPermission::actionAllowed('TabSentEmails') ) { 
    return;
}


$groupEmailSentSQL = "SELECT ett.name, etn.template_name, ge.email_subject, ge.email_to, ge.email_sent_on FROM groups_email ge JOIN email_templates_new etn ON etn.template_id = ge.template_code JOIN email_templates_type ett ON ett.type_id = etn.template_type_id WHERE ge.tourid = {$tourid} ORDER BY ge.groups_email_id DESC";
$groupEmailSentResult = $db->query($groupEmailSentSQL);
$groupEmailSentCount = $groupEmailSentResult->num_rows;
?>
<div class="col-sm-12 p-0 table-responsive">
    <table class="datatablefunction_1 table table-hover table-striped table-smallz" data-page-length="50">
        <thead>
            <th style="width:10%;">#</th>
            <th style="width:10%;">Template Name</th>
            <th style="width:10%;">Email Subject</th>
            <th style="width:50%;">Sent To</th>
            <th style="width:20%;">Sent On</th>
        </thead>
        <tbody>
            <?php 
            if ($groupEmailSentCount > 0) {
                $i = 1;
                while($row = $groupEmailSentResult->fetch_assoc()) {
            ?>
            <tr >
                <td style="font-size: 13px;width:10%;"><?php echo $i; ?></td>
                <td style="font-size: 13px;width:10%;"><?php echo $row['template_name']; ?></td>
                <td style="font-size: 13px;width:10%;"><?php echo $row['email_subject']; ?></td>
                <td style="font-size: 13px;width:50%;" title="<?php echo $row['email_to'];?>"><?php echo wordwrap($row['email_to'], 80, "\n", true);?></td>
                <td style="font-size: 13px;width:20%;"><?php echo date('m/d/Y H:i:s', strtotime($row['email_sent_on'])); ?></td>
            </tr>
            <?php $i++; } 
            } else { ?>
            <tr>
                <td colspan="5" style="text-align: center;">There are no Emails Sent in this group yet!</td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<?php
    $GLOBAL_SCRIPT .= "$(document).ready(function() {
        $('table.datatablefunction_1').DataTable( {'paging':   false,'ordering': false,'info':     false} );
        } );";
?>
