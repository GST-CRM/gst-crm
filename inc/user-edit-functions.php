<?php 
include "../session.php";

$target_dir = "../uploads/profile/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$temp = explode(".", $_FILES["fileToUpload"]["name"]);
$filename = time() . '.' . end($temp);
$fileurl = $target_dir . $filename;
$mediadescription = mysqli_real_escape_string($db, $_POST["description"]);
$mediaurl = $filename;
$UploadMessageText = "";

$GSTuserid = mysqli_real_escape_string($db, $_POST["userid"]);
$GSTusername = mysqli_real_escape_string($db, $_POST["currentusername"]);
$OLDpassword = mysqli_real_escape_string($db, $_POST["ConfirmNewPassword"]);
$GSTpassword = hash('sha256', mysqli_real_escape_string($db, $_POST["NewPassword"]));
$GSTfname = mysqli_real_escape_string($db, $_POST["fname"]);
$GSTlname = mysqli_real_escape_string($db, $_POST["lname"]);
$GSTemailaddress = mysqli_real_escape_string($db, $_POST["email"]);

//Check User's Password
$DefaultPassword = hash('sha256',"Password");
$PasswordUpdateQuery = "";

if($GSTpassword != $DefaultPassword) {
	$PasswordUpdateQuery = "password='$GSTpassword', Algorithm='SHA256', ";
}

	
if ($_FILES["fileToUpload"]["name"] != "") {
	// Check if file already exists
	if (file_exists($target_file)) {
		$UploadMessageText .= "Sorry, file already exists.";
		$uploadOk = 0;
	}
	// Check file size
	if ($_FILES["fileToUpload"]["size"] > 50000000) {
		$UploadMessageText .= "Sorry, your file is too large.";
		$uploadOk = 0;
	}
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "pdf"
	&& $imageFileType != "gif" && $imageFileType != "doc" && $imageFileType != "docx" ) {
		$UploadMessageText .= "Sorry, only JPG, JPEG, PNG, GIF & PDF & DOCX & DOC files are allowed.";
		$uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
		//$UploadMessageText .= "Sorry, your file was not uploaded.";
		$UploadMessage = base64_encode($UploadMessageText);
		echo "<meta http-equiv='refresh' content='0;url=../users-edit.php?id=$GSTuserid&message=$UploadMessage'>";
	// if everything is ok, try to upload file
	} else {
		if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $fileurl)) {
			$con15 = new mysqli($servername, $username, $password, $dbname);
			if ($con15->connect_error) {die("Connection failed: " . $con15->connect_error);} 
			$sql15 = "UPDATE users SET password='$GSTpassword', Algorithm='SHA256', fname='$GSTfname', lname='$GSTlname', Profile_Path='$mediaurl', EmailAddress='$GSTemailaddress' WHERE UserID=$GSTuserid;";
			if ($con15->query($sql15) === TRUE) {
			echo "<meta http-equiv='refresh' content='0;url=../users-edit.php?id=$GSTuserid'>";
			} else {
			echo $sql15 . "<br>" . $con15->error."<br> error";
			}
		} else {
			echo $sql15 . $con15->error."Sorry, there was an error updating the user data.";
		}
	}
} else {
	// To update the user without uploading any profile
	$con15 = new mysqli($servername, $username, $password, $dbname);
	if ($con15->connect_error) {die("Connection failed: " . $con15->connect_error);} 
	$sql15 = "UPDATE users SET $PasswordUpdateQuery fname='$GSTfname', lname='$GSTlname', EmailAddress='$GSTemailaddress' WHERE UserID=$GSTuserid;";
	if ($con15->query($sql15) === TRUE) {
	echo "<meta http-equiv='refresh' content='0;url=../users-edit.php?id=$GSTuserid'>";
	} else {
		echo $sql15 . "<br>" . $con15->error."<br> error";
	}
}

?>