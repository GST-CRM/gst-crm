<?php 
include "../session.php";
$TourID = $__REQUEST['GroupTicketID'];


//the batch actions.
if( isset($__REQUEST['batch-invaction']) ) {
    //batch print
    if( $__REQUEST['batch-invaction'] == 'P' ) {
        include __DIR__ . '/tickets-pdf.php';
    }
    //batch send
    else if( $__REQUEST['batch-invaction'] == 'S' ) {
        $FileTitlePrefix = 'Ticket' ;
        include __DIR__ . '/tickets-mail.php';
        echo '<script type="text/javascript">window.close();</script>' ;
        die;
    }
}
?>
