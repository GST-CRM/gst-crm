<?php 

include "../config.php";
include "../inc/helpers.php";

// Number of records fetch
$numberofrecords = 20;
$response = array();

$SelectCustomerTypes = 0;
if(isset($_POST['FinancialReport'])) { $SelectCustomerTypes = 1; }

if(!isset($_POST['searchTerm'])){
	//Here if the text field is empty, don't show anything
	$response[] = array(
		"id" => 0,
		"text" => "Please type the name you want"
	);
} else {
	$search = $_POST['searchTerm'];// Search text

	// Fetch records
	$GetGroupsList = "SELECT tourid,tourname,tourdesc FROM groups
	WHERE status=1 AND (tourdesc LIKE '%".$search."%' OR tourname LIKE '%".$search."%')
	LIMIT $numberofrecords";
	$GroupsList = GDb::fetchRowSet($GetGroupsList);


	// Read Data
    if($SelectCustomerTypes == 0) {
        foreach($GroupsList as $Groups){
            $response[] = array(
                "id" => $Groups['tourid'].":".$Groups['tourdesc'],
                "text" => $Groups['tourdesc']." | ".$Groups['tourname']
            );
        }
    } elseif($SelectCustomerTypes == 1) {
        foreach($GroupsList as $Groups){
            $response[] = array(
                "id" => $Groups['tourid'],
                "text" => $Groups['tourdesc']." | ".$Groups['tourname']
            );
        }
    }
	
}


echo json_encode($response);
exit();

?>