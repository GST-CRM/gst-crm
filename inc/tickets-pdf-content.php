<style>
table {
	width:100%;
	border-collapse: collapse;
}
td,th {
	border: solid 1px black;
	text-align:center;
}
p {
	text-align:justify;
}
.notez {
	color:red;
	font-weight:bold;
}
</style>
<?php 
$CustomersListSQL = mysqli_query($db,"SELECT 
		co.fname,
		co.mname,
        co.lname, 
        ca.contactid, 
        ca.tourid,
        ca.hisownticket,
        ca.Same_Itinerary,
        ca.Ticket_Number,
        ca.Confirmation_Number,
        gro.tourname,
        gro.tourdesc,
        gro.startdate,
        gro.enddate,
		pro.Confirmation
FROM customer_account ca
JOIN contacts co
ON co.id=ca.contactid
JOIN groups gro 
ON gro.tourid=ca.tourid
LEFT JOIN products pro 
ON pro.id=gro.airline_productid
WHERE ca.tourid=$TourID AND ca.contactid=$CustomerTicketID AND ca.status=1");
while($CustomersList = mysqli_fetch_array($CustomersListSQL)) { ?>
<div id="container">
<h1 align="center">Flight Itinerary</h1>
<h3 align="center" style="margin-top:5px;"><?php echo $CustomersList['tourname']; ?></h3>
<table cellpadding="5">
	<thead>
		<tr>
			<th colspan="2" bgcolor="#c44747" color="#ffffff"><strong>Passenger Name:</strong></th>
			<th colspan="2" bgcolor="#c44747" color="#ffffff"><strong>Confirmation #:</strong></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td colspan="2"><?php echo $CustomersList['fname']." ".$CustomersList['mname']." ".$CustomersList['lname']; ?></td>
			<td colspan="2"><?php 
			if($CustomersList['hisownticket'] == 1 ) {
				if($CustomersList['Confirmation_Number']=="" OR $CustomersList['Confirmation_Number']==NULL) {
					echo "N/A";
				} else {
					echo $CustomersList['Confirmation_Number'];
				}
			} else {
				echo $CustomersList['Confirmation'];
			}
			?></td>
		</tr>
		<tr>
			<td colspan="4"><br /></td>
		</tr>
		<tr>
			<td colspan="2" bgcolor="#c44747" color="#ffffff"><strong>Departure</strong></td>
			<td colspan="2" bgcolor="#c44747" color="#ffffff"><strong>Arrival</strong></td>
		</tr>
		<?php 
			//If Statements to get the correct details of the Flights
			if($CustomersList['hisownticket'] == 0) {$CustomerID = 0;}
			if($CustomersList['hisownticket'] == 1 AND $CustomersList['Same_Itinerary']==0) {$CustomerID = $CustomersList['contactid'];}
			if($CustomersList['hisownticket'] == 1 AND $CustomersList['Same_Itinerary']==1) {$CustomerID = 0;}
			
			$FlightCounter = 1;
			$FlightsListSQL = mysqli_query($db,"SELECT
				ga.Airline_Name,
				ga.Airline_Code,
				gf.Flight_Airline_Number,
				gf.Departure_Time,
				gp.Airport_City AS Departure_Airport_City,
				gp.Airport_State AS Departure_State,
				gp.Airport_Code AS Departure_Airport,
				gf.Arrival_Time,
				gp2.Airport_City AS Arrival_Airport_City,
				gp2.Airport_State AS Arrival_State,
				gp2.Airport_Code AS Arrival_Airport
			FROM groups_flights gf 
			LEFT JOIN groups_airline ga
			ON gf.Flight_Airline_ID=ga.Airline_ID
			LEFT JOIN groups_airport gp
			ON gf.Departure_Airport=gp.Airport_ID
			LEFT JOIN groups_airport gp2
			ON gf.Arrival_Airport=gp2.Airport_ID
			WHERE gf.Group_ID=$TourID AND gf.Specific_Customer_ID=$CustomerID
			ORDER BY gf.GF_Line_ID ASC");
			while($FlightsDetails = mysqli_fetch_array($FlightsListSQL)) { ?>
		<tr>
			<td colspan="4" bgcolor="#fcecec"><h4 class="notez" style="margin-top:5px;margin-bottom:5px;"><?php echo "Flight ".$FlightCounter." (".$FlightsDetails['Airline_Name']." (".$FlightsDetails['Airline_Code'].") ".$FlightsDetails['Flight_Airline_Number'].")"; ?></h4></td>
		</tr>
		<tr>
			<td colspan="2"><strong>Airport:</strong> <?php echo $FlightsDetails['Departure_Airport_City'].", ".$FlightsDetails['Departure_State']." (".$FlightsDetails['Departure_Airport'].")"; ?></td>
			<td colspan="2"><strong>Airport:</strong> <?php echo $FlightsDetails['Arrival_Airport_City'].", ".$FlightsDetails['Arrival_State']." (".$FlightsDetails['Arrival_Airport'].")"; ?></td>
		</tr>
		<tr>
			<td colspan="2"><strong>Scheduled Time:</strong> <?php echo date('F d, Y | h:i A', strtotime($FlightsDetails['Departure_Time'])); ?></td>
			<td colspan="2"><strong>Scheduled Time:</strong> <?php echo date('F d, Y | h:i A', strtotime($FlightsDetails['Arrival_Time'])); ?></td>
		</tr>
		<?php $FlightCounter++; } ?>
	</tbody>
</table>
<p>Your airline ticket has been issued electronically. The confirmation number and ticket number are listed above.
When you arrive to the airport, please be prepared to present a photo I.D., along with your confirmation number,
to an airline representative in order to receive your boarding pass. If your flight is delayed or changed for any
reason, and you need assistance, you may call our office at 844-659-5263 when departing from the U.S. or (810)
397-8669 if you are outside the country.</p>
<p><span class="notez">Note:</span> All group seat assignments are under airport control. All requests should be made with the gate agent on
the day of departure. Visible seat assignments online may or may not be accurate. Pre-check in is not required
and is not available to most groups.</p>
<p style="text-align:center;"><strong>ALL FLIGHTS ARE SUBJECT TO CHANGE.</strong></p>
</div>
<?php } ?>