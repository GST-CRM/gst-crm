<form name="contact10" action="inc/group-functions.php" class="row" method="POST" id="contact10">
	<div class="form-group form-default form-static-label col-sm-8">
		<label class="float-label">Please select a Land Product</label>
        <select id="landproduct" name="landproduct" class="form-control select2-landproduct form-control-default" multiple="multiple">
			<option value="0" disabled> </option>
			<option id="addnewoptionland" value="1">Add New</option>
			<?php 	
            $landProductId = $data["land_productid"] ;
			$meta1 = new mysqli($servername, $username, $password, $dbname);
			$metadata1 = mysqli_query($meta1,"SELECT * FROM products WHERE id='$landProductId' AND status='1' AND subproduct='0' AND categoryid='2'");
			$metaselected ="";
			while($keyword1 = mysqli_fetch_array($metadata1))
			{
			if($data["land_productid"] == $keyword1['id']) {$metaselected="selected";} else {$metaselected="";}
			echo "<option value='".$keyword1['id']."' ".$metaselected.">".$keyword1['id']." l ".$keyword1['name']." ($".$keyword1['cost'].")</option>";
			
			} ?>
		</select>
		<?php if($data["land_productid"] != 0) { ?>
		<label id="landwarning" class="float-label" style="font-weight:bold;color:red;display:none;"> Warning: Changing the Land Product will lead to the loss of the current data!</label>
		<?php } ?>
		<input type="text" name="landtourid" class="form-control" value="<?php echo $data["tourid"]; ?>" hidden>
		<input type="text" name="landproductlineid" class="form-control" value="<?php echo $data["id"]; ?>" hidden>
		<input type="text" name="tourtotalcost" class="form-control" value="<?php echo $TourCostTotal; ?>" hidden>
	</div>
    <?php if( AclPermission::actionAllowed('LandInfoUpdate') ) { ?>
	<div class="form-group form-default form-static-label col-sm-2 p-0">
		<button id="addnewbuttonland" type="reset" name="addnew" class="btn waves-effect waves-light btn-inverse" style="height:60px;width: 100%;" data-toggle="modal" data-target="#addnewlandproduct">Add New</button>
	</div>
	<div class="form-group form-default form-static-label col-sm-2">
		<button type="submit" name="submit" class="btn waves-effect waves-light btn-success" style="height:60px;width: 100%;" data-toggle="modal" data-target="#resultsmodal">Update</button>
	</div>
    <?php } ?>
</form>
<?php if($data["land_productid"] != 0) { 
//To collect the data for the Land Product
$CurrentLandProductz = $data["land_productid"];
$LandProDataSQL = "SELECT * FROM products WHERE id=$CurrentLandProductz";
$LandProDataResult = $db->query($LandProDataSQL);
$LandProData = $LandProDataResult->fetch_assoc();

//To collect the data for the Land Product
if($LandProData['Product_Reference'] > 0) {
	$SingleSupplementProductID = $LandProData['Product_Reference'];
	$SingleProDataSQL = "SELECT cost,price FROM products WHERE id=$SingleSupplementProductID";
	$SingleProDataResult = $db->query($SingleProDataSQL);
	$SingleProData = $SingleProDataResult->fetch_assoc();
}
$aclEditLand = false ;
if( AclPermission::userGroup() == AclRole::$ADMIN ) {
    $aclEditLand = true ;
}
?>
	<div class="row">
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Land Product Name <?php echo ($aclEditLand ? '(click to edit)' : '');?></label>
            <?php if( $aclEditLand ) { ?>
			<a href="products-edit.php?id=<?php echo $LandProData["id"]; ?>&action=edit">
            <?php } ?>
                <input <?php echo (($aclEditLand) ? 'style="cursor: pointer;"' : '');?> type="text" name="landname" class="form-control" value="<?php echo $LandProData["name"]; ?>" readonly>
			<?php if( $aclEditLand ) { ?>
            </a>
            <?php } ?>
		</div>
        <?php if( AclPermission::actionAllowed('SupplierInfo') ) { ?>
		<div class="form-group form-default form-static-label col-sm-4">
			<?php 
			$meta2 = new mysqli($servername, $username, $password, $dbname);
			$supplierid = $LandProData["supplierid"];
			$metadata3 = mysqli_query($meta2,"SELECT id,company,fname,lname,title FROM contacts WHERE id=$supplierid");
			$keyword3 = mysqli_fetch_array($metadata3); ?>						
			
			<label class="float-label">Land Supplier</label>
			<a href="contacts-edit.php?id=<?php echo $keyword3["id"]; ?>&action=edit&usertype=supplier">
				<input style="cursor: pointer;" type="text" name="AirProductSupplier" class="form-control" value="<?php echo $keyword3["company"]; ?>" readonly>
			</a>
		</div>
        <?php } ?>
		<div class="form-group form-default form-static-label col-sm-4">
			<label class="float-label">Product Description</label>
			<input type="text" name="airfaredesc" class="form-control" value="<?php echo $LandProData["description"]; ?>" readonly>
		</div>
            <?php if( AclPermission::actionAllowed('LandCost') ) { ?>
		<div class="form-group form-default form-static-label col-sm-3">
			<label class="float-label">Land Cost</label>
			<input type="text" name="airfarecost" class="form-control" value="<?php echo GUtils::formatMoney($LandProData["cost"]); ?>" title="Edit the product directly to modify this number" readonly>
		</div>
            <?php }?>
		<div class="form-group form-default form-static-label col-sm-3">
			<label class="float-label">Land Price</label>
			<input type="text" name="airfareprice" class="form-control" value="<?php echo GUtils::formatMoney($LandProData["price"]); ?>" title="Edit the product directly to modify this number" readonly>
		</div>
		<div class="form-group form-default form-static-label col-sm-3">
			<label class="float-label">Single Supplement Cost</label>
			<input type="text" name="SingleSuppCost" class="form-control" value="<?php echo GUtils::formatMoney($SingleProData["cost"]); ?>" title="Edit the product directly to modify this number" readonly>
		</div>
		<div class="form-group form-default form-static-label col-sm-3">
			<label class="float-label">Single Supplement Price</label>
			<input type="text" name="SingleSuppPrice" class="form-control" value="<?php echo GUtils::formatMoney($SingleProData["price"]); ?>" title="Edit the product directly to modify this number" readonly>
		</div>
	</div>
<?php if( AclPermission::actionAllowed('SubProductsAssigned') ) { ?>
	<div>
	<br />
		<h4 class="sub-title">Subproducts assigned to the Land Package Product</h4>
		<table class="table table-hover table-striped table-smallz">
			<tbody>
				<?php 
				$parentidx2 = $data["land_productid"];
				$sql = "SELECT * FROM products WHERE parentid=$parentidx2 AND subproduct=1 AND status=1";
				$result = $db->query($sql);
				if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
				<tr>
					<td style="width:25px;"><img src="<?php echo $crm_images; ?>/<?php echo $row["producttype"]; ?>.png" alt="<?php echo $row["producttype"]; ?>" style="width:22px; height:auto;" /></td>
					<td><a href="products-edit.php?id=<?php echo $row["id"]; ?>&action=edit"><?php echo $row["name"]; ?></a></td>
					<td><?php echo $row["meta1"]; ?></td>
					<td><?php echo $row["meta2"]; ?></td>
					<td><?php echo $row["meta3"]; ?></td>
					<td><?php echo $row["meta4"]; ?></td>
				</tr>
				<?php } ?>
				<tr>
					<td colspan="6" style="text-align:center;"><button type="reset" name="addnew" class="btn waves-effect waves-light btn-inverse" style="padding-top: 5px;padding-bottom: 5px;" data-toggle="modal" data-target="#addnewsubproduct">Add A New Sub-Product</button></td>
				</tr>
				<?php } else { ?>
				<tr>
					<td colspan="5">There are no subproducts for this package!</td>
				</tr>
				<tr>
					<td colspan="5" style="text-align:center;"><button type="reset" name="addnew" class="btn waves-effect waves-light btn-inverse" style="padding-top: 5px;padding-bottom: 5px;" data-toggle="modal" data-target="#addnewsubproduct">Add A New Sub-Product</button></td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
	</div>
<?php } ?>
<?php } ?>
