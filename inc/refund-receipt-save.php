<?php

include_once __DIR__ . '/../models/customer_refund.php';
include_once __DIR__ . '/../models/customer_refund_line.php';
include_once __DIR__ . '/../models/customer_payment.php';
include_once __DIR__ . '/../models/transactions.php';

//Common Values
$refundDate = $__REQUEST['refund_date'];
$refundFrom = $__REQUEST['refund_from'];
$paymentType = $__REQUEST['payment_method'];
$paymentFrom = '';
$paymentCheck = '';
$comments = $__REQUEST['comments'];
$CancelReason = $__REQUEST['Cancel_Reasonz'];
$RefundPaymentText = $__REQUEST['RefundPaymentText'];
$amount = $__REQUEST['payment'];

    if( $paymentType == 'Cash' ) {
        $paymentFrom = mysqli_real_escape_string($db, $_POST['refund_from_Cash']);
    } elseif( $paymentType == 'Bank Transfer' ) {
        $paymentFrom = mysqli_real_escape_string($db, $_POST['refund_from_Bank']);
    } elseif( $paymentType == 'Check' ) {
        $paymentFrom = mysqli_real_escape_string($db, $_POST['refund_from_Bank']);
        $paymentCheck = mysqli_real_escape_string($db, $_POST['Check_Number']);
    } elseif( $paymentType == 'Credit Card' ) {
        $paymentFrom = mysqli_real_escape_string($db, $_POST['refund_from_Card']);
    } elseif( $paymentType == 'Online Payment' ) {
        $paymentFrom = mysqli_real_escape_string($db, $_POST['refund_from_PayPal']);
    }


$robj = new CustomerRefund() ;
$refundLineObj = new CustomerRefundLine() ;
$tobj = new Transactions() ;

//upload file {
$config = [] ;
$config['max_size'] = 50000000 ;
$config['upload_path'] = 'invattach/';

GUtils::doUpload('filAttachment', $config) ;
$fileName = (isset($config['file_name']) ? $config['file_name'] : '') ;
//}
//

    $invoiceId = $__REQUEST['cbInvoice'] ;
    $one = $robj->get(['Customer_Invoice_Num' => $invoiceId]) ;
    $invoiceData = (new CustomerInvoice())->get(['Customer_Invoice_Num' => $invoiceId]) ;
    
    // No refund records exists? create new one.
    if( $invoiceData['Status'] != 4) {
        //refund issued.
        $refundNew = array(
            'Customer_Invoice_Num' => $invoiceId, 
            'Customer_Account_Customer_ID' => $invoiceData['Customer_Account_Customer_ID'],
            'Cancellation_Charge' => 0,
            'Cancellation_Outcome' => 0,
            'Status'               => CustomerRefund::$CUSTOM_REFUND ,
            'Active_Refund'        => 0,
            'Issued_Amount'        => 0,
            'Payee_Invoice_Num'    => 0,
            'Refund_Amount'        => 0,
            'Mod_Date'             => Model::sqlNoQuote('now()'),
        );
                
        if( !is_array($one) ) {
            $robj->insert($refundNew) ;
            $refundNew['Refund_ID'] = $robj->lastInsertId() ;
            $one = $refundNew ;
        }
    }
    
    $issuedAmount = $one['Issued_Amount'] ;
    $refundId = $one['Refund_ID'] ;
    $customerId = $one['Customer_Account_Customer_ID'] ;
    $balance = $one['Refund_Amount'] - ($issuedAmount + $amount) ;
    $rfStatus = $one['Status'] ;
    $activeRefund = $one['Active_Refund'] ;

    if( $paymentType == 'Check' ) {
        $AddNewCheckSQL = "INSERT INTO checks(Check_Type, Check_Number, Check_Customer_ID, Check_Bank_ID, Check_Amount, Check_Amount_Words, Check_Date, Check_Memo, Check_Note, Check_Created_By) VALUES (1,'$paymentCheck','$customerId','$paymentFrom','$amount','$RefundPaymentText','$refundDate','$CancelReason','$comments','$UserAdminID')";
        GDb::execute($AddNewCheckSQL);
        $IncreaseCheckNumSQL = "UPDATE checks_bank SET Bank_Check_Number=Bank_Check_Number+1 WHERE Bank_ID=$paymentFrom";
        GDb::execute($IncreaseCheckNumSQL);
    }
    
    //mark transaction
    $tdata = array(
        'Transaction_Type' => 6,
        'Chart_of_Accounts_GL_Account_N' => '1200',
        'Transaction_Amount' => 0 -$amount,
        'Customer_Account _Customer_ID' => $customerId,
        'Supplier_Supplier_ID' => '0',
        'Add_Date' => Model::sqlNoQuote('now()'),
        'Mod_Date' => Model::sqlNoQuote('now()'),
    ) ;
    $transactionId = $tobj->insert($tdata) ;

    
    if( $rfStatus == CustomerRefund::$CUSTOM_REFUND ) {
        //refund issued.
        $refund = array(
            'Active_Refund'        => $activeRefund + $amount,
            ) ;
    }
    else {
        //refund issued.
        $refund = array(
            'Status'               => ( ($balance <= 0) ? CustomerRefund::$ISSUED  : CustomerRefund::$PARTIALLY_ISSUED ) ,
            'Issued_Amount'        => $issuedAmount + $amount,
            'Mod_Date'             => Model::sqlNoQuote('now()'),
            'Refund_Date'          => $refundDate,
        );
    }
    $where = array(
        'Customer_Invoice_Num' => $invoiceId
    ) ;
    $robj->update($refund, $where) ;
    
    //Update line
    $refundLine = array(
        'Refund_ID' => $refundId,
        'Amount' => $amount,
        'Issue_Date' => $refundDate,
        'Payment_Type' => $paymentType,
        'Payment_From_Account' => $paymentFrom,
        'Payment_Check' => $paymentCheck,
        'Transaction_Type_Transaction_ID' => $transactionId,
        'Description' => $comments,
        'Status'=> 1,
        'Add_Date' => Model::sqlNoQuote('now()'),
    ); 
    if( $fileName ) {
        $refundLine['Attachment'] = $fileName ;
    }
    
    $refundLineObj->insert($refundLine) ;

    GUtils::setSuccess('Refund details successfully saved.') ;

