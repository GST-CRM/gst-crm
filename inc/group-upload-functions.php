<?php 
include "../session.php";

$uploadcon = new mysqli($servername, $username, $password, $dbname);

$leadid = $_GET["id"];
$target_dir = "../uploads/";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
$temp = explode(".", $_FILES["fileToUpload"]["name"]);
$filename = time() . '.' . end($temp);
$fileurl = $target_dir . $filename;
$mediatype = $_POST["attachmenttype"];
$name = $_POST["documentname"];
$mediadescription = $_POST["description"];
$mediaurl = $filename;
$tourid = $_POST["touridupload"];

if ($_FILES["fileToUpload"]["name"] != "") {
	// Check if file already exists
	if (file_exists($target_file)) {
		echo "Sorry, file already exists.";
		$uploadOk = 0;
	}
	// Check file size
	if ($_FILES["fileToUpload"]["size"] > 50000000) {
		echo "Sorry, your file is too large.";
		$uploadOk = 0;
	}
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "pdf"
	&& $imageFileType != "gif" && $imageFileType != "doc" && $imageFileType != "docx" ) {
		echo "Sorry, only JPG, JPEG, PNG, GIF & PDF & DOCX & DOC files are allowed.";
		$uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
		echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
		if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $fileurl)) {
			$con15 = new mysqli($servername, $username, $password, $dbname);
			if ($con15->connect_error) {die("Connection failed: " . $con15->connect_error);} 
			$sql15 = "INSERT INTO groups_media (mediatype, name, mediadescription, mediaurl, tourid)
			VALUES ('$mediatype','$name','$mediadescription','$mediaurl','$tourid')";
			if ($con15->query($sql15) === TRUE) {
			$idz = mysqli_insert_id($con15);
			//echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>New file (".$idz.") has been recorded correctly!</strong></div>";
			echo "<meta http-equiv='refresh' content='0;url=../groups-edit.php?id=$tourid&action=edit&tab=attachments'>";
			} else {
			echo $sql15 . "<br>" . $con15->error."<br> error";
			}
		} else {
			echo $sql15 . $con15->error."Sorry, there was an error uploading your file.";
		}
	}
}


?>