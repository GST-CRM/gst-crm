<?php //This pop up modal is the old version of saving or updating a contact ?>
<div class="modal fade" id="updated" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" style="background:white;border-radius:10px;" role="document">
		<div class="sweet-alert showSweetAlert sweet-alert-customized" data-custom-class="" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: block;">
			<div class="sa-icon sa-success animate" style="display: block;">
				<span class="sa-line sa-tip animateSuccessTip"></span>
				<span class="sa-line sa-long animateSuccessLong"></span>
				<div class="sa-placeholder"></div>
				<div class="sa-fix"></div>
			</div>
            <h2>Done!</h2>
            <p>Your edits have been recorded Successfully.</p>
            <div class="sa-button-container">
                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<?php //This popup to show the details of the changes for a specific customer ?>
<div class="modal fade" id="ChangeLogTrack" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" style="background:white;border-radius:10px;" role="document">
		<div class="sweet-alert showSweetAlert sweet-alert-customized" data-custom-class="" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: block;">
            <h2>Change logs Details:</h2>
            <div id="ChangeLogTrackDiv" style="width:80%;margin:auto;"></div>
            <div class="sa-button-container">
                <button type="button" class="btn btn-default waves-effect " data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<?php //This pop up modal is for when trying to delete a contact ?>
<div class="modal fade" id="delete" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" style="background:white;border-radius:10px;" role="document">
		<div class="sweet-alert showSweetAlert sweet-alert-customized" data-custom-class="" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: block;">
			<div class="sa-icon sa-warning pulseWarning" style="display: block;">
				<span class="sa-body pulseWarningIns"></span>
				<span class="sa-dot pulseWarningIns"></span>
			</div>
            <h2>Are you sure?</h2>
            <p>This action is irreversable!</p>
            <div class="sa-button-container"><br />
                <a type="button" class="btn btn-default waves-effect  waves-light" data-dismiss="modal">Close</a>
                <a type="submit" href="contacts-edit.php?id=<?php echo $leadid; ?>&action=delete" class="btn btn-danger waves-effect waves-light" onclick="uploaddisablealert()">Delete</a>
            </div>
        </div>
    </div>
</div>

<?php //This pop up modal is for when saving any form in the edit contact, this is the updated pop up ?>
<div class="modal fade" id="resultsmodal" tabindex="-1" role="dialog" style="top:50px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
                <div id="results" class="confirmation-message-result-area">
					<div class="upload-loader"></div>
					<p>Prcoessing...</p>
				</div>
            </div>
            <div class="modal-footer2">
                <button type="button" class="btn btn-success waves-effect waves-light" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<?php //This pop up modal is for when you want to add a payment for a CUSTOMER ?>
<div class="modal fade" id="addpayment" tabindex="-1" role="dialog" style="z-index:1049;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/contact-tabs-AddPayment.php'; ?>
            </div>
        </div>
    </div>
</div>

<?php //This pop up modal is for when you want to add a payment for an AGENT ?>
<div class="modal fade" id="AgentPayments" tabindex="-1" role="dialog" style="z-index:1049;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/contact-tabs-agent-AddPayment.php'; ?>
            </div>
        </div>
    </div>
</div>

<?php //This pop up modal is for when you want to add an Airline supplier payment in the group ?>
<div class="modal fade" id="AddGroupAirSuppPayment" tabindex="-1" role="dialog" style="z-index:1049;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/group-tabs-AddSuppAirPayment.php'; ?>
            </div>
        </div>
    </div>
</div>

<?php //This pop up modal is for when you want to add a Land supplier payment in the group ?>
<div class="modal fade" id="AddGroupLandSuppPayment" tabindex="-1" role="dialog" style="z-index:1049;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/group-tabs-AddSuppLandPayment.php'; ?>
            </div>
        </div>
    </div>
</div>

<?php /*This pop up modal is for the rooming list 
<div class="modal fade" id="RoomingList" tabindex="-1" role="dialog" style="z-index:1049;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<iframe src="inc/group-tabs-rooming.php?GroupID=<?php echo $tourid; ?>" style="width:100%;height:auto;border:0px;min-height:350px;"></iframe>
				<?php //include 'inc/group-tabs-rooming.php'; ?>
            </div>
        </div>
    </div>
</div> */ ?>


<?php //This pop up modal is for when you want to add a new product ?>
<div class="modal fade" id="addnewproduct" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/group-tabs-addnewproduct.php'; ?>
            </div>
        </div>
    </div>
</div>

<?php //This pop up modal is for when you want to add a new LAND product ?>
<div class="modal fade" id="addnewlandproduct" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/group-tabs-addnewlandproduct.php'; ?>
            </div>
        </div>
    </div>
</div>

<?php //This pop up modal is for when you want to add a new LAND SUB product
if($Current_Page == 'group-edit.php') { ?>
<div class="modal fade" id="addnewsubproduct" tabindex="-1" role="dialog" style="z-index:1049;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/group-tabs-addnewsubproduct.php'; ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>

<?php //This popup is for creating a new customer account with a group assigned ?>
<div class="modal fade" id="makecustomersmodal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <form name="contact6" action="contacts-edit.php?id=<?php echo $leadid; ?>&action=makeaccount" method="POST" id="contact6">
			<div class="modal-body" style="text-align:center;">
				<h4>Creating a Customer Account</h4>
				Please assign a group for this contact:
				<div style="width:60%;margin:auto;">
				<select name="thetourid" class="select2-the-tours col-sm-12" multiple="multiple" required>
					<?php 
                    $tourId = $customerdata['tourid'] ;
                    $con5 = new mysqli($servername, $username, $password, $dbname);
					$result5 = mysqli_query($con5,"SELECT tourid, tourname FROM groups WHERE tourid='$tourId' AND status=1 AND startdate >= CURDATE()");
					while($row5 = mysqli_fetch_array($result5))
					{
						$selected = '';
						if($customerdata['tourid']==$row5['tourid']){$selected = 'selected';};
						echo "<option value='".$row5['tourid']."' $selected>".$row5['tourname']."</option>";
					} ?>
				</select>
				</div>
            </div>
            <div class="modal-footer2">
                <button type="button" class="btn btn-inverse waves-effect waves-light" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-success waves-effect waves-light" style="margin-left:15px;" onclick="uploaddisablealert()">Create Account</button>
            </div>
        </form>
		</div>
    </div>
</div>

<?php //This pop up modal is for when uploading the files in the attachment section ?>
<div class="modal fade" id="uploadingmodal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<div class="upload-loader"></div>
				<p>Uploading the document and submitting ...</p>
            </div>
            <div class="modal-footer2">
                <button type="button" class="btn btn-default waves-effect waves-light" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<?php //This pop up modal is for when you want to add a new Airline Upgrade in the customer account ?>
<div class="modal fade" id="AirUpgradeWindow" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/contact-tabs-NewAirUpgrade.php'; ?>
            </div>
        </div>
    </div>
</div>

<?php //This pop up modal is for when you want to add a new Land Upgrade in the customer account ?>
<div class="modal fade" id="LandUpgradeWindow" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/contact-tabs-NewLandUpgrade.php'; ?>
            </div>
        </div>
    </div>
</div>

<?php //This pop up modal is for when you want to add a new extension product in the customer account ?>
<div class="modal fade" id="ExtensionWindow" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/contact-tabs-NewExtension.php'; ?>
            </div>
        </div>
    </div>
</div>

<?php //This pop up modal is for when you want to add a new insurance product in the customer account ?>
<div class="modal fade" id="InsuranceWindow" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/contact-tabs-NewInsurance.php'; ?>
            </div>
        </div>
    </div>
</div>

<?php //This pop up modal is for when you want to add a new Transfer product in the customer account ?>
<div class="modal fade" id="TransferWindow" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/contact-tabs-NewTransfer.php'; ?>
            </div>
        </div>
    </div>
</div>

<?php //This pop up modal is for when you want to add a new customer to the group 
if(basename($_SERVER['PHP_SELF']) == "groups-edit.php") { ?>
<div class="modal fade" id="addcustomer" tabindex="-1" role="dialog" style="z-index:1049;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/group-tabs-NewCustomer.php'; ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>


<?php 
//This popup model is for when you delete an attachment, it handles dynamic attachment id. ?>
<div class="modal fade" id="delete-attachment" tabindex="-1" role="dialog" data-value="">
    <div class="modal-dialog modal-sm" style="background:white;border-radius:10px;" role="document">
		<div class="sweet-alert showSweetAlert sweet-alert-customized" data-custom-class="" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: block;">
			<div class="sa-icon sa-warning pulseWarning" style="display: block;">
				<span class="sa-body pulseWarningIns"></span>
				<span class="sa-dot pulseWarningIns"></span>
			</div>
            <h2>Are you sure?</h2>
            <p>This action is irreversable!</p>
            <div class="sa-button-container"><br />
                <a type="button" class="btn btn-default waves-effect  waves-light" data-dismiss="modal">Close</a>
                <a type="submit" href="javascript:;" class="btn btn-danger waves-effect waves-light modal-action-target" onclick="uploaddisablealert()">Delete</a>
            </div>
        </div>
    </div>
</div>

<?php //This pop up modal is for when trying to delete a record ?>
<div class="modal fade" id="deleteConfirm" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" style="background:white;border-radius:10px;" role="document">
        <div class="sweet-alert showSweetAlert sweet-alert-customized" data-custom-class="" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: block;">
            <div class="sa-icon sa-warning pulseWarning" style="display: block;">
                <span class="sa-body pulseWarningIns"></span>
                <span class="sa-dot pulseWarningIns"></span>
            </div>
            <h2 class="delete-title">Are you sure?</h2>
            <p class="delete-msg">This action is irreversible!</p>
            <div class="sa-button-container"><br />
                <a type="button" class="btn btn-default waves-effect  waves-light" data-dismiss="modal">Close</a>
                <a type="submit" href="javascript:void(0)" class="delete-target btn btn-danger waves-effect waves-light" >Delete</a>
            </div>
        </div>
    </div>
</div>

    <?php //This is a common pop up modal where we need a prompt for actions ?>
<div class="modal fade" id="confirmBox" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-sm" style="background:white;border-radius:10px;" role="document">
        <form id="confirmBoxWrap" class="sweet-alert showSweetAlert sweet-alert-customized show-input show-input-gst" data-custom-class="" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: block;">
            <div class="sa-icon sa-info" style="display: block;">
                <span class="sa-body pulseWarningIns"></span>
                <span class="sa-dot pulseWarningIns"></span>
            </div>
            <h2 class="confirm-title">Are you sure?</h2>
            <p class="confirm-msg">This action is irreversible!</p>
            <div class="confirm-div"></div>
            <div class="sa-button-container"><br />
                <a type="button" class="btn btn-default waves-effect  waves-light" data-dismiss="modal">Close</a>
                <a type="submit" href="javascript:void(0)" onclick="this.href = appendIfNotJUrl(this.href, $('#confirmBoxWrap').serialize()) ; $('#confirmBox').modal('hide');  "  class="confirm-target btn btn-info waves-effect waves-light" >OK</a>
            </div>
        </form>
    </div>
</div>


    <?php //Send Reminder Popup Box ?>
<div class="modal fade" id="showSendReminder" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" style="background:white;padding:30px;" role="document">
        <div class="" data-custom-class="" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: block;">
            <h5 class="confirm-title">Send reminder email for <label class="gst-reminder-class"></label></h5>
            <form  method="POST" name="reminderForm" id="idReminderForm" >
                <div class="row">
                    <div class="col-sm-12 mt-2 mb-1">To</div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <input type="hidden" class="gst-reminder-id form-control" name="invid" />
                        <input type="text" class="gst-reminder-email form-control" name="reminder_to" required="required" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mt-2 mb-1">Subject</div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><input type="text" class="gst-reminder-subject form-control" name="reminder_subject" required="required" /></div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mt-2 mb-1">Message</div>
                </div>
                <div class="row">
                    <div class="col-sm-12"><textarea rows="4" class="gst-reminder-message form-control" name="reminder_message" required="required" ></textarea></div>
                </div>
            </form>
            <div class="sa-button-container"><br />
                <a type="button" class="btn btn-default waves-effect  waves-light" data-dismiss="modal">Close</a>
                <a type="submit" href="javascript:void(0)" onclick="submitReminderForm()"  class="reminder-submit btn btn-info waves-effect waves-light" >Send Reminder</a>
                <img class="reminder-loader" style="display: none;" src="files/assets/images/preloader.gif" />
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="paxDetailModal" tabindex="-1" role="dialog" style="z-index:1049;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div id="paxDetailModalBody" class="modal-body" style="text-align:center;">

            </div>
        </div>
    </div>
</div>

<?php //This pop up modal is for filter by clicking number of participants
if(basename($_SERVER['PHP_SELF']) == "groups-edit.php") 
{ 
/*?>
<div class="modal fade" id="filterWithTickets" tabindex="-1" role="dialog" style="z-index:1049;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/group-tabs-filter-with-ticket.php'; ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="filterWithoutTickets" tabindex="-1" role="dialog" style="z-index:1049;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/group-tabs-filter-without-ticket.php'; ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="filterSingleRooms" tabindex="-1" role="dialog" style="z-index:1049;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
            <?php $pax_filter = "single_room"; ?>
				<?php include 'inc/group-tabs-filter-single-rooms.php'; ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="filterAirUpgrades" tabindex="-1" role="dialog" style="z-index:1049;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/group-tabs-filter-air-upgrades.php'; ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="filterLandUpgrades" tabindex="-1" role="dialog" style="z-index:1049;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/group-tabs-filter-land-upgrades.php'; ?>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="filterComplimentary" tabindex="-1" role="dialog" style="z-index:1049;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
				<?php include 'inc/group-tabs-filter-complimentary.php'; ?>
            </div>
        </div>
    </div>
</div>
<?php */
} ?>


<?php //Send Cancel / Refund Popup ?>
<div class="modal fade" id="showCancelInvoice" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" style="background:white;padding:30px;" role="document">
        <div class="" data-custom-class="" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: block;">
            <h5 class="confirm-title">Cancel Invoice for <label class="gst-cancel-invoice-class"></label></h5>
            <span class="text-success text-bold"></span>
            <span class="text-danger text-bold"></span>
            <form  method="POST" name="cancelInvoiceForm" id="idCancelInvoiceForm" >
                <div class="row">
                    <div class="col-sm-12">
                        <input type="hidden" name="invoiceId" class="gst-cancel-invoice-id form-control" name="id" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 mt-2 mb-1">Cancellation Charge</div>
                    <div class="col-sm-8 mt-2 mb-1 gst-cancel-invoice-charge" >$0.0</div>
                </div>
                <div class="row">
                    <div class="col-sm-4 mt-2 mb-1">Refund Amount</div>
                    <div class="col-sm-8 mt-2 mb-1 gst-cancel-invoice-refund" >$0.0</div>
                </div>
                <div class="row">
                    <div class="col-sm-4 mt-2 mb-1">Cancellation Reason</div>
                    <div class="col-sm-8"><textarea rows="4" class="gst-cancel-invoice-reason form-control" name="cancellation_message" required="required" placeholder="Cancellation Reason" ></textarea></div>
                </div>
            </form>
            <div class="sa-button-container row"><br />
                <div class="col-sm-4 mt-2 mb-1"></div>
                <div class="col-sm-8 mt-2 mb-1">
                    <a type="button" class="btn btn-default waves-effect  waves-light" data-dismiss="modal">Close</a>
                    <a type="submit" href="javascript:void(0)" onclick="submitCancelInvoiceForm()"  class="cancel-invoice-submit btn btn-info waves-effect waves-light" >Submit</a>
                    <img class="cancel-invoice-loader" style="display: none;" src="files/assets/images/preloader.gif" />
                </div>
            </div>
        </div>
    </div>
</div>


<?php //Send Group Email Popup Box ?>
<div class="modal fade" id="sendGroupEmail" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" style="background:white;padding:30px;" role="document">
        <div class="" data-custom-class="" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: block;">
            <h5 class="confirm-title">Send email for Group :: <label class="gst-email-group-name"></label></h5>
            <form action="inc/group-tabs-mail.php" method="POST" name="groupEmailForm" id="idGroupEmailForm" enctype="multipart/form-data" >
                <input type="hidden" class="gst-email-group-id" name="groupid" value=""/>
                <input type="hidden" class="gst-email-group-name-input" name="groupname" value=""/>
                <input type="hidden" class="gst-email-type" name="mailtype" value=""/>
                <div class="row">
                    <div class="col-sm-12 mt-2 mb-1">To</div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <input type="text" class="gst-email-group-name-input form-control" readonly/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mt-2 mb-1">Email Template</div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <select class="form-control" id="email-template" name="email_template">
                            <option value="0">Select Template</option>
                            <?php
                                $userId = (new Users())->getLoggedInUserId();
                                $userId = ($userId) ? $userId : 0 ;
                                $templateTypeResult = mysqli_query($db,"SELECT etn.created_userid, etn.template_id,etn.template_name FROM email_templates_new etn WHERE etn.status=1 AND etn.template_type_id > 0 AND etn.template_name != 'Footer Email Template - Dont Delete' ORDER BY created_userid='$userId' DESC");
                                
                                $isSelected = false ;
                                while($ResultData = mysqli_fetch_array($templateTypeResult))
                                {   
                                    $selected = '' ;
                                    if( ! $isSelected && $ResultData['created_userid'] == $userId ) {
                                        $selected = 'selected="selected"';
                                        $isSelected = true ;
                                    }
                                    echo "<option $selected value='".$ResultData['template_id']."'>".wordwrap($ResultData['template_name'],65,"<br>\n") ."</option>" ;
                                }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 mt-2 mb-1">Attachment</div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="Attachment_Box bg-white">
                            <input type="file" id="GroupEmail_Attachment" name="groupAttachment" class="Attachment_Input" >
                            <p id="GroupEmail_Attachment_Text" class="Attachment_Text">Drag the file here or click in this area.</p>
                        </div>
                    </div>
                </div>
                <div class="row no-template">
                    <div class="col-sm-12 mt-2 mb-1">Subject</div>
                </div>
                <div class="row no-template">
                    <div class="col-sm-12"><input type="text" class="form-control" name="email_subject" id="email_subject" readonly required="required" /></div>
                </div>
                <div class="row no-template">
                    <div class="col-sm-12 mt-2 mb-1">Message</div>
                </div>
                <div class="row no-template">
                    <div class="col-sm-12"><div contenteditable="false" readonly id="email_message" style="width:100%; border:1px solid #ccc; padding:15px;"></div><textarea hidden rows="4" class="form-control" name="email_message_text" id="email_message_text" ></textarea></div>
                </div>
                <div class="row no-template">
                    <div class="col-sm-12 mt-2 mb-1">Recipent Type</div>
                </div>
                <div class="row no-template">
                    <div class="col-sm-12 row mt-2 mb-1">
                        <div class="col-sm-4">
                            <input id="activeCheck" name="activeMembers" value="1" type="checkbox" checked="checkedg" />
                            <label for="activeCheck" class="gst-email-group-active-label" >Active</label>
                        </div>
                        <div class="col-sm-6">
                            <input id="inactiveCheck" name="inactiveMembers" value="1" type="checkbox" /> 
                            <label for="inactiveCheck" class="gst-email-group-inactive-label" >Inactive</label>
                        </div>
                    </div>
                </div>
                <div class="sa-button-container"><br />
                    <!--  onclick="sendGroupEmail()" -->
                    <a type="button" class="btn btn-default waves-effect  waves-light" data-dismiss="modal">Close</a>
                    <a type="submit" id="sendGroupEmailBtn" href="javascript:void(0)" class="btn btn-info waves-effect waves-light groupemail-submit" >Send Email</a>
                    <!-- <input type="submit"  class="btn btn-info waves-effect waves-light" value="Send Email"> -->
                    <img class="groupemail-loader" style="display: none;" src="files/assets/images/preloader.gif" />
                </div>
            </form>
        </div>
    </div>
</div>
<script>
//This is to make the attachment box update its content when a file is selected
$(document).ready(function(){
  $('#GroupEmail_Attachment').change(function () {
    $('#GroupEmail_Attachment_Text').text("A file has been selected");
  });
});
</script>


<?php //Send Remove customer Popup ?>
<div class="modal fade" id="showRemoveCustomer" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" style="background:white;padding:30px;" role="document">
        <form  method="POST" name="RemoveCustomerForm" id="idRemoveCustomerForm" >
        <div class="" data-custom-class="" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: block;">
            <h5 class="confirm-title">Remove Customer <label class="gst-remove-customer-class"></label></h5>
            <span class="text-success text-bold"></span>
            <span class="text-danger text-bold"></span>
            
                <div class="row">
                    <div class="col-sm-12">
                        <input type="hidden" name="invoiceId" class="gst-remove-customer-id form-control" />
                        <input type="hidden" name="tourId" class="gst-remove-customer-tourid form-control" />
                        <input type="hidden" name="url" class="gst-remove-customer-url form-control" />
                        <input type="hidden" name="paid" class="gst-remove-customer-paid form-control" />
                        <input type="hidden" name="outcome" class="gst-remove-customer-outcome form-control" />
                        <input type="hidden" name="refund" class="gst-remove-customer-refund form-control" />
                        <input type="hidden" name="amount" class="gst-remove-customer-amount form-control" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4 mt-2 mb-1">Cancellation Charge</div>
                    <input type="hidden" class="gst-remove-customer-amount-raw" value="" />
                    <input type="hidden" class="gst-remove-customer-refund-raw" value="" />
                    <div class="col-sm-8 mt-2 mb-1 gst-remove-customer-charge" >$0.0</div>
                    <input name="customcancelfee_value" type="number" class="col-sm-4 mt-2 mb-1 pl-1 ml-3 gst-remove-customer-charge-input" min="0" style="display: none;" value="0" onchange="onCancellationRefundAmountChanged(this)"  />
                </div>
                <div class="row">
                    <div class="col-sm-4 mt-2 mb-1">Cancellation Date</div>
                    <div class="col-sm-8 mt-2 mb-1 gst-remove-customer-date" >
                        <input type="date" name="cancellation_date" value="<?php echo GUtils::clientDate(Date('Y-m-d H:i:s'), 'Y-m-d'); ?>" />
                    </div>
                </div>
                <div class="row hidden gst-remove-refund-method ">
                    <div class="col-sm-4 mt-2 mb-1">Refund Method</div>
                    <div class="col-sm-8 mt-2 mb-1 gst-remove-customer-method" >
                        <input type="radio" onclick="toggleRefundType(this);" value="1" name="refund_method" class="refundMethodNormal" checked="checked" /> Normal
                        &nbsp;&nbsp;
                        <input type="radio" onclick="toggleRefundType(this);" value="2" name="refund_method" /> Change Primary
                    </div>
                </div>
                <div class="row hidden mg-2 gst-remove-refund-method gst-remove-additional-wrap">
                    <div class="col-sm-4 mt-2 mb-1">New Primary</div>
                    <div class="col-sm-8 mt-2 mb-1 gst-remove-customer-additional" >
                        <select style="width: 160px;" name="additional" class="select2 gst-remove-additional-selection" >
                            <option>Select</option>
                        </select>
                    </div>
                </div>

                <div class="row gst-remove-normal-wrap ">
                    <div class="col-sm-4 mt-2 mb-1">Refund Amount</div>
                    <div class="col-sm-8 mt-2 mb-1 gst-remove-customer-refund" >$0.0</div>
                </div>
                <div class="row">
                    <div class="col-sm-4 mt-2 mb-1">Cancellation Reason</div>
                    <div class="col-sm-8"><textarea rows="4" class="gst-remove-customer-reason form-control" name="cancellation_message" required="required" placeholder="Cancellation Reason" ></textarea></div>
                </div>
            
            <div class="sa-button-container row mt-3"><br />
                <div class="col-sm-8 mt-2 mb-1">
                
                    <div class="col-sm-6 float-left" >
                        <label class="float-label">No Fee Cancellation</label>
                            <div class="can-toggle">
                                <input onclick="toggleNoFeeCancellation(this);" type="checkbox" id="idNoFeeCancellation" name="nocancelfee" value="1" <?php if($supplierdata["status"]==1) echo "checked"; ?>>
                                <label for="idNoFeeCancellation">
                                  <div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
                                </label>
                            </div>
                    </div>
                    <div class="col-sm-6 float-right" >

                        <label class="float-label">Custom Cancellation Fee </label>
                            <div class="can-toggle">
                                <input onclick="toggleCustomFeeCancellation(this);" type="checkbox" id="idCustomFeeCancellation" name="customcancelfee" value="1" <?php if($supplierdata["status"]==1) echo "checked"; ?>>
                                <label for="idCustomFeeCancellation">
                                  <div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
                                </label>
                            </div>
                   </div>

                </div>
                <div class="col-sm-4 mt-2 mb-1 float-right text-right">
                    <div class="col-sm-12"><label class="float-label">&nbsp;</label></div>
                    <a type="button" class="btn btn-default waves-effect  waves-light" data-dismiss="modal">Close</a>
                    <a type="submit" href="javascript:void(0)" onclick="submitRemoveCustomerForm()"  class="remove-customer-submit btn btn-info waves-effect waves-light" >Submit</a>
                    <img class="remove-customer-loader" style="display: none;" src="files/assets/images/preloader.gif" />
                </div>
            </div>
        </div>
            </form>
    </div>
</div>



<?php //Send Restore customer Popup ?>
<div class="modal fade" id="showRestoreCustomer" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md" style="background:white;padding:30px;" role="document">
        <form  method="POST" name="RestoreCustomerForm" id="idRestoreCustomerForm" >
        <div class="" data-custom-class="" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: block;">
            <h5 class="confirm-title">Restore Customer <label class="gst-restore-customer-class"></label></h5>
            <span class="text-success text-bold"></span>
            <span class="text-danger text-bold"></span>
            
                <div class="row">
                    <div class="col-sm-12">
                        <div class="mt-3">Are you sure you want to restore this customer back to trip?</div>
                        <input type="hidden" name="invoiceId" class="gst-restore-customer-id form-control" />
                        <input type="hidden" name="tourId" class="gst-restore-customer-tourid form-control" />
                        <input type="hidden" name="url" class="gst-restore-customer-url form-control" />
                        <input type="hidden" name="paid" class="gst-restore-customer-paid form-control" />
                        <input type="hidden" name="outcome" class="gst-restore-customer-outcome form-control" />
                        <input type="hidden" name="refund" class="gst-restore-customer-refund form-control" />
                        <input type="hidden" name="amount" class="gst-restore-customer-amount form-control" />
                    </div>
                </div>
            
            <div class="sa-button-container row mt-3"><br />
                <div class="col-sm-12 mt-2 mb-1 float-right text-right">
                    <a type="button" class="btn btn-default waves-effect  waves-light" data-dismiss="modal">Cancel</a>
                    <a type="submit" href="javascript:void(0)" onclick="submitRestoreCustomerForm()"  class="restore-customer-submit btn btn-info waves-effect waves-light" >Yes</a>
                    <img class="restore-customer-loader" style="display: none;" src="files/assets/images/preloader.gif" />
                </div>
            </div>
        </div>
            </form>
    </div>
</div>



<script type="text/javascript">

function prepareDeleteAttachment(e) {
  var invoker = $(e);
  var lead = $(invoker).data('lead') ;
  var file = $(invoker).data('file') ;

  $('#delete-attachment .modal-action-target').attr('href', "contacts-edit.php?id=" + lead + "&usertype=attachments&fileid=" + file + "&action=docdelete") ;  
}


</script>
