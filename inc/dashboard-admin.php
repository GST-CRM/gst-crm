<?php
include_once __DIR__ . '/../models/tour_membership.php';

//Total Number of Customers
$paxsql = "SELECT contactid FROM customer_account WHERE status=1";
$paxresult = $db->query($paxsql);
$paxtotal = $paxresult->num_rows;


//Total of Estimated Income
$sql = "SELECT SUM(ci.Invoice_Amount) AS total FROM customer_invoice ci WHERE ci.Status=1";
$result = $db->query($sql);
$data = $result->fetch_assoc();

//Total Number of Groups
$GroupSQL = "SELECT tourid FROM groups WHERE status=1 AND startdate >= CURDATE()";
$GroupResult = $db->query($GroupSQL);
$GroupsTotal = $GroupResult->num_rows;

//Total Number of Contacts
$ContactSQL = "SELECT id FROM contacts";
$ContactResult = $db->query($ContactSQL);
$ContactsTotal = $ContactResult->num_rows;
?>
<!-- [ page content ] start -->
<!--<script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>	 -->
<div class="row">
	<div class="col-md-3">
		<div class="card stat-rev-card">
			<div class="card-block">
				<div class="rev-icon bg-c-red"><i class="fas fa-shopping-cart text-white"></i><span class="ring-sm"></span><span class="ring-lg"></span></div>
				<h2 class="text-c-red" style="font-size: 30px;"><?php echo number_format($paxtotal); ?></h2>
				<p class="m-b-0">Customers</p>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="card stat-rev-card">
			<div class="card-block">
				<div class="rev-icon bg-c-green"><i class="fas fa-money-bill-alt text-white"></i><span class="ring-sm"></span><span class="ring-lg"></span></div>
				<h2 class="text-c-green" style="font-size: 30px;"><?php echo "$".number_format($data["total"]); ?></h2>
				<p class="m-b-0">Estimated Income</p>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="card stat-rev-card">
			<div class="card-block">
				<div class="rev-icon bg-c-blue"><i class="fas fa-cloud-download-alt text-white"></i><span class="ring-sm"></span><span class="ring-lg"></span></div>
				<h2 class="text-c-blue" style="font-size: 30px;"><?php echo number_format($GroupsTotal); ?></h2>
				<p class="m-b-0">Groups</p>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="card stat-rev-card">
			<div class="card-block">
				<div class="rev-icon bg-c-yellow"><i class="fas fa-user text-white"></i><span class="ring-sm"></span><span class="ring-lg"></span></div>
				<h2 class="text-c-yellow" style="font-size: 30px;"><?php echo number_format($ContactsTotal); ?></h2>
				<p class="m-b-0">Contacts</p>
			</div>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="card exp-build-card" style="display:none;">
			<div class="RefreshGroupsBox">
				<h5 class="text-white"><i class="fas fa-exclamation-triangle"></i> Warning: Click here to update all the system!
					<form name="contact25" action="inc/group-functions.php" method="POST" id="contact25" style="display: inline;">
						<button type="submit" name="RefreshCustomers" value="submit-RefreshCustomers" class="btn btn-mat waves-effect waves-light btn-inverse float-right" style="padding: 0px 10px;" data-toggle="modal" data-target="#resultsmodal">
							<i class="fas fa-sync" style="margin-right:0px"></i>
						</button>
					</form>
				</h5>
			</div>
		</div>
		<div class="card exp-build-card d-none" style="overflow:hidden;">
			<div class="p-0 card-block">
				<img src="<?php echo $crm_images; ?>/Greetings-4.jpg" alt="" style="width:100%; height:auto;" />
			</div>
		</div>
		<div class="card exp-build-card">
			<div class="card-header">
				<h5>Welcome to GST CRM Control Panel</h5>
			</div>
			<div class="pt-0 card-block text-justify">
				Use this panel to manage all aspects of Good Shepherd Travel Customer Relations. If you encounter any problems navigating the software, please contact Tony Abu Aita with a brief explanation of the problem and accompanying screenshots. Thank you for helping us improve our work.
			</div>
		</div>
		<div class="card exp-build-card">
			<div class="card-header">
				<a href="changelog.php"><h5>Click to view the changelog details History</h5></a>
			</div>
		</div>
	</div>
	<div class="col-md-6">
        <div class="card table-card">
            <div class="card-header" style="padding-bottom:0px !important;">
                <h5>Active Groups List</h5>
            </div>
            <div class="card-block p-b-0 p-t-0">
                <div class="table-responsive">
                    <table id="basic-btn2" class="table table-striped table-hover m-b-0" data-page-length="5">
                        <thead>
                            <tr>
                                <th >Departure Date</th>
                                <th>Group Name</th>
                                <th># of Pax</th>
                            </tr>
                        </thead>
                        <tbody>
						
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- [ page content ] end -->
<script type="text/javascript">
$(document).ready(function() {
    $('#basic-btn2').DataTable( {
        //"ordering": false,
		 "bProcessing": true,
         "serverSide": true,
		"order": [[ 0, "asc" ]],
		dom: 'Bfrtip',
        "columnDefs": [
                      { "targets": 0 , "visible": false },
                      { "targets": 3 , "visible": false }
		   ],
        ajax : {
            url :"inc/datatable-dashboard-group-listing.php", // json datasource
            complete : function(){
              }

          },
        buttons: [
            'csv', 'excel'
        ],
        "createdRow": function ( row, data, index ) {
            if( data[3] == "1") {
                $(row).addClass('table-danger') ;
            }
        }
    } );
} );
</script>