<?php
include_once 'vendor/autoload.php';
include_once __DIR__ . '/../models/groups.php' ;
include_once __DIR__ . '/../models/users.php' ;

use PHPMailer\PHPMailer\PHPMailer ;

//Using same pages for receipt and invoice.

//Set a default invoice/receipt.
$SUBJECT = 'Tour Leader Report' ;

//set default file prefix.
$filePrefix = 'Tour-Leader-Report' ;

//set default messages.
$MESSAGE = 'Please find Tour Leader Report attachment.' ;

$report_slug = array("To-Date","Name-Badges","Paid-To-Date","All-Info", "Rooming-List");

ob_start() ;

$toDateName = "To-Date";
$toBadgeName = "Name-Badges";
$toPaidTo = "Paid-To-Date";
$toAllInfo = "All-Info";
$toRoomingList = "Rooming-List";

/*

$sql = "SELECT CONCAT(contacts.fname,' ', contacts.lname) AS leader_name,contacts.email
FROM groups 
LEFT JOIN contacts ON contacts.id = groups.groupleader
WHERE groups.tourid = $tourid";

$queryGroup = mysqli_query($db, $sql) or die("error to fetch group data");
$groupData = mysqli_fetch_assoc($queryGroup);

$leaderEmail = $groupData['email'];

*/

try {
	    
    //Recipients
    $groupId = $_REQUEST['id'];
    $group = (new Groups())->get(['tourid' => $groupId]) ;
    $groupName = $group['tourname'] ;
    
    $groupLeaders = [] ;
    if( isset($_REQUEST['tour-leader']) && $_REQUEST['tour-leader'] ) {
        $groupLeaders = (new Groups())->getGroupLeadersDetail($groupId) ;
    }
    $groupAgents = [] ;
    if( isset($_REQUEST['agent']) && $_REQUEST['agent'] ) {
        $groupAgents = (new Groups())->getGroupAgentsNameList($groupId) ;
    }
    $groupStaffEmail = null ;
    if( isset($_REQUEST['staff']) && $_REQUEST['staff'] ) {
        $groupStaffEmailId = (new Groups())->staffEmail($groupId) ;
    }
    
    $customEmails = null ;
    if( isset($__REQUEST['additional']) && $__REQUEST['additional'] ) {
        $customEmails = explode(',', $__REQUEST['additional']) ;
    }
    
    $loggedInUser = (new Users())->getLoggedInUserDetail() ;
    $loggedEmailId = (isset($loggedInUser['EmailAddress']) ? $loggedInUser['EmailAddress'] : '') ;        //Fix Arrays
    
    //Group Leader
    $bcc = [] ;
    foreach( $groupLeaders as $g ) {
        $bcc[] = $g['email'] ;
    }
    //Agent
    foreach( $groupAgents as $a ) {
        $bcc[] = $a['email'] ;
    }
    if( $groupStaffEmailId ) {
        $bcc[] = $groupStaffEmailId ;
    }
    if( $loggedEmailId ) {
        $bcc[] = $loggedEmailId ;
    }
    foreach( $customEmails as $a ) {
        if( strlen(trim($a)) ) {
            $bcc[] = trim($a) ;
        }
    }
    
    if( ! $to ) {
        $to  = 'donotreply@goodshepherdtravel.com' ;
    }

    
    $attachment = [] ;
    
	$PDF_OUTPUT = 'S';
	$PDF_HEADER = true ;
	//$pdfData    = include __DIR__ . '/sales-invoice-pdf.php';

	$report_slug = "To-Date";
	$pdfData    = include 'tour-leader-pdf-report.php';
    $attachment[] = ['name' => $toDateName . '.pdf', 'data' => $pdfData] ;

	$report_slug = "Name-Badges";
	$pdfData   = include 'tour-leader-pdf-report.php';
    $attachment[] = ['name' => $toBadgeName . '.pdf', 'data' => $pdfData] ;

	$report_slug = "Paid-To-Date";
	$pdfData    = include 'tour-leader-pdf-report.php';
    $attachment[] = ['name' => $toPaidTo . '.pdf', 'data' => $pdfData] ;

	$report_slug = "All-Info";
	$pdfData    = include 'tour-leader-pdf-report.php';
    $attachment[] = ['name' => $toAllInfo . '.pdf', 'data' => $pdfData] ;

	$report_slug = "Rooming-List";
	$pdfData    = include 'tour-leader-pdf-report.php';
    $attachment[] = ['name' => $toRoomingList . '.pdf', 'data' => $pdfData] ;
	
	if (GUtils::sendMail($to, $SUBJECT, $MESSAGE, $attachment, null, [], [], $bcc))
	{
		//echo "sent";
		GUtils::setSuccess("Email sent successfully");
	}
	else
	{
		//echo "not sent";
		GUtils::setWarning("Error in sending email. Try again later");
	}
	//GUtils::redirect("groups-edit.php");

} catch (Exception $e) {
	//print_r($e);
	GUtils::setWarning("Error in sending email. Try again later");
	//GUtils::redirect("groups-edit.php");
}

