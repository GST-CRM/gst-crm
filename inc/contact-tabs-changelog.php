<?php 
include "../session.php";

$previewHtml 	= "";
if (!empty($_REQUEST['type']))
{
	$id 		= !empty($_REQUEST['id'])?$_REQUEST['id']:"";
	$type 	= !empty($_REQUEST['type'])?$_REQUEST['type']:"";
	$ChangeDate 	= !empty($_REQUEST['ChangeDate'])?$_REQUEST['ChangeDate']:"";
	switch ($_REQUEST['type'])
	{
		case "Customer" :
			$CustomerLogSQL = "SELECT Log_Type, Reference_ID, Column_Name, Column_Old_Value, Column_New_Value FROM changelog WHERE Change_Time='$ChangeDate' AND Reference_ID='$id'";
			$CustomerLogResult = $db->query($CustomerLogSQL);
			if ($CustomerLogResult->num_rows > 0) { 
				echo "<ul style='text-align: left;list-style-type: circle;font-size:12px;'>";
				while($CustomerLogRow = $CustomerLogResult->fetch_assoc()) {
					echo "<li>The <u>".$CustomerLogRow['Column_Name']."</u> Has been changed from <b>(".$CustomerLogRow['Column_Old_Value'].")</b> to <b>(".$CustomerLogRow['Column_New_Value'].")</b></li>";
				}
				echo "</ul>";
			} else {
				echo "There are no changes that could have been tracked. That could be the reason that the tracking feature was added after this update has been occured.";
			}
			//$modalHtml = include "contact-tabs-changelog-data.php?id=$id&type=Customer&ChangeDate=$ChangeDate";
			break;
	}
}
$previewHtml;
exit;
?>