<?php

$cbPayment = '' ;
$payment = '' ;

$invoiceDate = GUtils::mysqlDate( $_REQUEST['invoice_date'] ) ;
$paymentMethod = $_REQUEST['payment_method'] ;
//$PaymentFromAccount = $_REQUEST['deposit_to'] ;
$chargable = 0 ;

if( isset($_REQUEST['hidPaymentSave']) ) {
    
    //update everything here..
    if( isset($_GET['payment']) && $_GET['payment']  ) {
        $paymentId = $_GET['payment'] ;

        foreach( $_REQUEST['payment'] as $invoiceId => $amount ) {
            if( $amount == 0 ) {
                continue; 
            }
            $comment = $_REQUEST['description'][$invoiceId] ;
            //Find Customer Id of invoice.
            $sqlc = "SELECT Supplier_Bill_Num, Supplier_ID, Transaction_Type_Transaction_ID FROM suppliers_payments WHERE Supplier_Payment_ID='$paymentId' LIMIT 1" ;
            $invData = GDb::fetchRow($sqlc) ;

            $customerId = $invData['Supplier_ID'] ;
            $invoiceId = $invData['Supplier_Bill_Num'] ;
            $transactionId = $invData['Transaction_Type_Transaction_ID'] ;
            
            $GetSupplierBankIDsql  = "SELECT Bank_ID FROM checks_bank WHERE Bank_Type='supplier' AND Bank_Name=$customerId";
            $SuppBankID = GDb::fetchRow($GetSupplierBankIDsql);
            if(count($SuppBankID['Bank_ID']) > 0) {
                $SupplierBankID = $SuppBankID['Bank_ID'];
            } else { 
                $SupplierBankID = 0;
            }

            //inser to transaction
            $sqlt = "UPDATE `transactions` SET `Transaction_Amount`='$amount', `Mod_Date`= now() WHERE Transaction_Type_Transaction_ID='$transactionId' LIMIT 1 ;" ;
            $db->query($sqlt) ;
            
            $paymentAccountFrom = '';
            $paymentCheck = '';
            if( $paymentMethod == 'Cash' ) {
                $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_Cash']);
            } elseif( $paymentMethod == 'Bank Transfer' ) {
                $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_Bank']);
            } elseif( $paymentMethod == 'Check' ) {
                $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_Bank']);
                $paymentCheck = mysqli_real_escape_string($db, $_POST['Check_Number']);
                $AddNewCheckSQL = "INSERT INTO checks(Check_Type, Check_Number, Check_Customer_ID, Check_Bank_ID, Check_Amount, Check_Date, Check_Note, Check_Created_By) VALUES (4,'$paymentCheck','$customerId','$paymentAccountFrom','$amount','$invoiceDate','$comment','$UserAdminID')";
                GDb::execute($AddNewCheckSQL);
                $IncreaseCheckNumSQL = "UPDATE checks_bank SET Bank_Check_Number=Bank_Check_Number+1 WHERE Bank_ID=$paymentAccountFrom";
                echo $AddNewCheckSQL;
                echo "<br /><hr />";
                echo $IncreaseCheckNumSQL;
                echo "<br /><hr />";
                GDb::execute($IncreaseCheckNumSQL);
            } elseif( $paymentMethod == 'Credit Card' ) {
                $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_Card']);
            } elseif( $paymentMethod == 'Online Payment' ) {
                $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_PayPal']);
            }

                
            $sqlp = "UPDATE suppliers_payments SET Supplier_Payment_Date = '$invoiceDate', Supplier_Payment_Amount =  '$amount', Supplier_Payment_Comments = '$comment', Supplier_Payment_Method = '$paymentMethod', Supplier_Payment_From_Account='$paymentAccountFrom', Supplier_Payment_Financial_Account='$SupplierBankID', Supplier_Payment_Check='$paymentCheck', Transaction_Type_Transaction_ID = '$transactionId' WHERE Supplier_Payment_ID='$paymentId' LIMIT 1" ;

            $db->query($sqlp) ;
            break ;
        }
    }
    else {

        foreach( $_REQUEST['payment'] as $invoiceId => $amount ) {
            if( $amount == 0 ) {
                continue; 
            }
            $chargable = 0 ;
            if( $paymentMethod == 'Credit Card' ) {
                $chargable = $amount ;
            }
            $comment = $_REQUEST['description'][$invoiceId] ;

            //Find Customer Id of invoice.
            $sqlc= "SELECT Supplier_ID, Purchase_Order_Num FROM suppliers_bill WHERE Supplier_Bill_Num='$invoiceId' LIMIT 1" ;
            $cusInfo = GDb::fetchRow($sqlc) ;
            $customerId = $cusInfo['Supplier_ID'] ;
            $salesOrderNumber = $cusInfo['Purchase_Order_Num'] ;
            
            $GetSupplierBankIDsql  = "SELECT Bank_ID FROM checks_bank WHERE Bank_Type='supplier' AND Bank_Name=$customerId";
            $SuppBankID = GDb::fetchRow($GetSupplierBankIDsql);
            if(count($SuppBankID['Bank_ID']) > 0) {
                $SupplierBankID = $SuppBankID['Bank_ID'];
            } else { 
                $SupplierBankID = 0;
            }
            
            $paymentAccountFrom = '';
            $paymentCheck = '';
            if( $paymentMethod == 'Cash' ) {
                $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_Cash']);
            } elseif( $paymentMethod == 'Bank Transfer' ) {
                $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_Bank']);
            } elseif( $paymentMethod == 'Check' ) {
                $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_Bank']);
                $paymentCheck = mysqli_real_escape_string($db, $_POST['Check_Number']);
                $AddNewCheckSQL = "INSERT INTO checks(Check_Type, Check_Number, Check_Customer_ID, Check_Bank_ID, Check_Amount, Check_Date, Check_Note, Check_Created_By) VALUES (4,'$paymentCheck','$customerId','$paymentAccountFrom','$amount','$invoiceDate','$comment','$UserAdminID')";
                GDb::execute($AddNewCheckSQL);
                $IncreaseCheckNumSQL = "UPDATE checks_bank SET Bank_Check_Number=Bank_Check_Number+1 WHERE Bank_ID=$paymentAccountFrom";
                GDb::execute($IncreaseCheckNumSQL);
            } elseif( $paymentMethod == 'Credit Card' ) {
                $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_Card']);
            } elseif( $paymentMethod == 'Online Payment' ) {
                $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_PayPal']);
            }
           
            //inser to transaction
            $sqlt = "INSERT INTO `transactions` (`Transaction_Type`, `Chart_of_Accounts_GL_Account_N`, `Transaction_Amount`, `Customer_Account _Customer_ID`, 
                        `Supplier_Supplier_ID`, `Canceled_Flag`, `Posted_Flag`, `Voided_Flag`, `Add_Date`, `Mod_Date`)
                    VALUES ('6', '5001', '$amount', '0', '0', '0', '0', '0', now(), now() );" ;
            $db->query($sqlt) ;

            $transactionId = $db->insert_id ;

            //Make payments
            $sqlp = "INSERT INTO `suppliers_payments` (`Supplier_Bill_Num`, `Supplier_Payment_Date`, `Supplier_ID`, `Supplier_Payment_Purchase_Order_ID`, 
                                                    `Supplier_Payment_Amount`, `Supplier_Payment_Method`, `Supplier_Payment_From_Account`, `Supplier_Payment_Financial_Account`, `Supplier_Payment_Check`, `Supplier_Payment_Comments`, `Transaction_Type_Transaction_ID`, `Add_Date`, `Mod_Date`, `Status`)
                        VALUES ('$invoiceId', '$invoiceDate', '$customerId', '$salesOrderNumber', '$amount', '$paymentMethod', '$paymentAccountFrom', '$SupplierBankID', '$paymentCheck', '$comment', '$transactionId', now(), now(), '1'); " ;

            $db->query($sqlp) ;
            $paymentId = $db->insert_id ;
            

            if( $chargable != 0 ) {
                //Update cc-charge // Set charge to zero if credit cart is not choosen.
                $charge = ($chargable * 4) / 100 ;
                $sqlcc = "UPDATE customer_payments SET Charges='$charge' WHERE Customer_Payment_ID='$paymentId'" ;
                $db->query($sqlcc) ;
            }
        }
    }

}