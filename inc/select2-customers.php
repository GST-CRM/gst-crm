<?php 

include "../config.php";
include "../inc/helpers.php";

// Number of records fetch
$numberofrecords = 20;
$response = array();
$SelectCustomerTypes = 0; //ZERO means get contacts, ONE means get customers, TWO means get suppliers
//To check if this select2 ajax call is for checks or just other pages
if(isset($_POST['ChecksFeature'])) { $SelectCustomerTypes = 1; }
if(isset($_POST['DepositsFeature'])) { $SelectCustomerTypes = 2; }

if(!isset($_POST['searchTerm'])){
	//Here if the text field is empty, don't show anything
	$response[] = array(
		"id" => 0,
		"text" => "Please type the name you want"
	);
} else {
	$search = $_POST['searchTerm'];
	$SearchWords = str_replace(" ","%",$search);
	
    if($SelectCustomerTypes == 1) {
        // Fetch records for checks page only with DATA attr.
        $GetCustomersList = "SELECT
            c.id,
            c.usertype,
            c.title,
            c.fname,
			c.mname,
			c.lname,
            c.company,
			c.address1,
            c.address2,
            c.zipcode,
            c.city,
            c.state,
            c.country
		FROM contacts c
		/*JOIN customer_account ca ON ca.contactid=c.id AND ca.status=1*/
		WHERE 1 AND concat(c.fname, c.mname, c.lname) LIKE '%".$SearchWords."%' OR c.company LIKE '%".$SearchWords."%'
		GROUP BY c.id
		ORDER BY c.fname DESC
        LIMIT $numberofrecords";
        $CustomersList = GDb::fetchRowSet($GetCustomersList);

        // Read Data
        foreach($CustomersList as $Customers){
            //To check if the contact is a supplier or not
            $CustomerType = $Customers['usertype'];
            if (strpos($CustomerType, '2') !== false) {
                //Get the full company
                $CustomerName = $Customers['company'];
            } else {
                //Get the full name
                $CustomerName = $Customers['title']." ".$Customers['fname']." ".$Customers['mname']." ".$Customers['lname'];
            }
            
            //Get the full address
            $CustomerAddress = $Customers['address1'].", ".$Customers['address2']."<br />".$Customers['city'].", ".$Customers['state'].", ".$Customers['zipcode'];
            //Echo it in the array
            $response[] = array(
                "id" => $Customers['id'],
                "text" => $CustomerName,
                "CustomerName" => $CustomerName,
                "CustomerAddress" => $CustomerAddress
            );
        }
    } elseif($SelectCustomerTypes == 2) {
        // Fetch records normally if it wasn't related to the checks
        $GetCustomersList = "SELECT co.id,co.fname,co.lname,co.company
        FROM suppliers su
        JOIN contacts co
        ON co.id=su.contactid
        WHERE concat(co.fname, co.mname, co.lname) LIKE '%".$SearchWords."%' OR co.company LIKE '%".$SearchWords."%'
        LIMIT $numberofrecords";
        $CustomersList = GDb::fetchRowSet($GetCustomersList);

        // Read Data
        foreach($CustomersList as $Customers){
            //Get the full name first
            $CustomerName = $Customers['company']." - (".$Customers['fname']." ".$Customers['lname'].")";
            //Echo it in the array
            $response[] = array(
                "id" => $Customers['id'],
                "text" => $CustomerName
            );
        }
    } else {
        // Fetch records normally if it wasn't related to the checks
        $GetCustomersList = "SELECT co.id,co.fname,co.mname,co.lname
        FROM contacts co
        JOIN customer_account ca ON ca.contactid=co.id
        WHERE ca.status=1 AND concat(co.fname, co.mname, co.lname) LIKE '%".$SearchWords."%'
        GROUP BY co.id
        LIMIT $numberofrecords";
        $CustomersList = GDb::fetchRowSet($GetCustomersList);

        // Read Data
        foreach($CustomersList as $Customers){
            //Get the full name first
            $CustomerName = $Customers['fname']." ".$Customers['mname']." ".$Customers['lname'];
            //Echo it in the array
            $response[] = array(
                "id" => $Customers['id'].":".$CustomerName,
                "text" => $CustomerName
            );
        }
    }
}


echo json_encode($response);
exit();

?>