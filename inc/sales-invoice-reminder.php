<?php
$SUBJECT = 'GST Invoice Reminder' ;
$MESSAGE = 'Please find GST invoice attached.' ;

if( isset($__REQUEST['reminder_subject'])) {
    $SUBJECT = $__REQUEST['reminder_subject'] ;
}
if( isset($__REQUEST['reminder_message'])) {
    $MESSAGE = $__REQUEST['reminder_message'] ;
}

include 'sales-invoice-mail.php' ;