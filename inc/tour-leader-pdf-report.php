<?php

// Include the main TCPDF library (search for installation path).
require_once('files/pdf/tcpdf.php');
include_once __DIR__ . '/../models/groups.php' ;
include_once __DIR__ . '/../models/customer_payment.php' ;
include_once __DIR__ . '/../library/MYPDF2.php' ;

/*
$report_title = "";
$report_slug = "";
$report_slug = $_GET['report_slug'];*/
$report_title = str_replace("-"," ",$report_slug);


// create new PDF document
$pdf = new MYPDF2(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Element Media');
$pdf->SetTitle($report_title);
$pdf->SetSubject($report_title);
$pdf->SetKeywords('');

// remove default header/footer
$PDF_HEADER = isset($PDF_HEADER) ? $PDF_HEADER : false ;
$pdf->setPrintHeader($PDF_HEADER);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/../lang/eng.php')) {
    require_once(dirname(__FILE__).'/../lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
$pdf->SetMargins(10, 12, 10);

// set font
$pdf->SetFont('Helvetica', '', 10);
    // add a page
$pdf->AddPage('L','LETTER');

// set margins
$pdf->setPageMark() ;



ob_start();

?>
<style>
th{
	height:40px;
}
td{
	border:1px solid #000;
	min-height:25px;
	line-height:20px;
}
</style>
<?php

$tourid = $_REQUEST['id'];



$sql = "SELECT tourname FROM groups WHERE tourid=$tourid";
$queryGroup = mysqli_query($db, $sql) or die("error to fetch group name");
$groupRow = mysqli_fetch_row($queryGroup);
$groupName = $groupRow[0];

//Groupname And Leader name
//SELECT groups.tourname,contacts.fname,contacts.lname FROM groups JOIN contacts ON groups.groupleader=contacts.id AND groups.tourid=20050
$sql = "SELECT groups.tourid,groups.tourname,groups.land_productid,CONCAT(contacts.fname,' ', contacts.lname) AS leader_name,contacts.email,groups.airline_productid,SUM(products.price) AS trip_package
FROM groups 
LEFT JOIN products ON products.id = groups.land_productid OR products.id = groups.airline_productid 
LEFT JOIN contacts ON contacts.id = groups.groupleader
WHERE groups.tourid = $tourid";

$queryGroup = mysqli_query($db, $sql) or die("error to fetch group data");
$groupData = mysqli_fetch_assoc($queryGroup);



$table_headings = array();
$table_contents = array();
if($report_slug=="To-Date"){
	$table_headings = array('#','Full Name','Join Date','Mobile Phone','Email', 'Address');
	$before_table = '<h4>'.$groupName.'</h4>
<h5>Tour Coordinator: '.$groupData['leader_name'].'</h5>';
}

elseif($report_slug=="Paid-To-Date"){
	$table_headings = array('#','Full Name','Join Date','Mobile Phone','Email','Paid to Date','Balance','Extras');
	$before_table = '<h4>'.$groupName.'</h4>
<h5>Trip Package: '.$groupData['trip_package'].'</h5>';
}

elseif($report_slug=="Name-Badges"){
	$table_headings = array('#','Full Name','Join Date','Ticket','Final','Packet');
	$before_table = '<h4>'.$groupName.'</h4>';
}

elseif($report_slug=="All-Info"){
	$table_headings = array('#','Full Name','Join Date','Passport #','PP Exp','Mobile Phone','Email');
	$before_table = '<h4>'.$groupName.'</h4>
<h5>Tour Coordinator: '.$groupData['leader_name'].'</h5>';
}

elseif($report_slug=="Rooming-List"){
	$table_headings = array('#','Room Type','Person 1','Person 2','Person 3','Special Land Requests');
	$before_table = '<h4>'.$groupName.'</h4>
<h5>Tour Coordinator: '.$groupData['leader_name'].'</h5>';
}


?>

<?php echo $before_table; ?>
	<table width="100%" align="center">
		<tr>
			<?php
			foreach($table_headings as $table_heading){
				if($table_heading=="#"){
					$th_style='width="30"';
				}
				elseif($table_heading=="Mobile Phone"){
					$th_style='width="100"';
				}
				elseif($table_heading=="Email"){
					$th_style='width="220"';
				}
				elseif($table_heading=="Paid to Date"){
					$th_style='width="80"';
				}
				elseif($table_heading=="Balance"){
					$th_style='width="80"';
				}
				elseif($table_heading=="Extras"){
					$th_style='width="150"';
				}
				elseif($table_heading=="Passport #" || $table_heading=="PP Exp"){
					$th_style='width="90"';
				}
				else{
					$th_style='width="130"';
				}
				?><td style="background-color:#ccc;line-height:34px;<?php //echo $th_style; ?>" <?php echo $th_style; ?>><?php echo $table_heading; ?></td><?php
			}
			?>
		</tr>
		<?php

		$sql = "SELECT contacts.id,
                    contacts.fname,
                    contacts.lname,
                    contacts.badgename,
                    contacts.passport,
                    contacts.expirydate,
                    customer_account.createtime,
                    customer_account.specialtext,
                    customer_account.special_land_text,
                    contacts.mobile,
                    contacts.email,
                    contacts.address1,
                    contacts.address2,
                    contacts.city,
                    contacts.state,
                    contacts.zipcode
             FROM contacts
             LEFT JOIN customer_account ON contacts.id=customer_account.contactid
             WHERE customer_account.tourid = $tourid
             ORDER BY customer_account.status DESC  ";

		$queryContacts = mysqli_query($db, $sql) or die("error to fetch contacts data");

		//While loop
		$no = 1;

        if( $report_slug == 'Rooming-List' ) {
            $contactDataSet = (new Groups())->getRoomingList($tourid) ;
            
                foreach( $contactDataSet as $contactsData ) {
                    $table_contents = array($no,$contactsData['roomtype'], $contactsData['fname']. " ".$contactsData['lname'], $contactsData['Roommate1'], $contactsData['Roommate2'],$contactsData['special_land_text']);
                    ?>
                    <tr nobr="true">
                        <?php foreach($table_contents as $table_content){ ?>
                        <td><?php echo $table_content; ?></td>
                        <?php } ?>
                    </tr>

                    <?php
                    $no++;
                }
        }
        else {
            while( $contactsData = mysqli_fetch_assoc($queryContacts) ) {
                //foreach( $queryContacts as $contactsData ) { 

                $contactId = $contactsData['id'];
                $extras = $contactsData['specialtext']."<br>".$contactsData['special_land_text'];

                $invoiceData2 = (new CustomerPayment())->customerPaymentDetails($contactId, $tourid) ;


                if($report_slug=="To-Date"){
                    $table_contents = array($no,$contactsData['fname']. " ".$contactsData['lname'],$contactsData['createtime'],$contactsData['mobile'],$contactsData['email'], GUtils::buildGSTAddress(true, $contactsData, ",", " ") );
                }

                elseif($report_slug=="Paid-To-Date"){
                    $table_contents = array($no,$contactsData['fname']. " ".$contactsData['lname'],$contactsData['createtime'],$contactsData['mobile'],$contactsData['email'],$invoiceData2['total'],$invoiceData2['balance'],$extras);
                }

                elseif($report_slug=="Name-Badges"){
                    $table_contents = array($no,$contactsData['fname']. " ".$contactsData['lname'],$contactsData['createtime'],"","","");
                }

                elseif($report_slug=="All-Info"){
                    $table_contents = array($no,$contactsData['fname']. " ".$contactsData['lname'],$contactsData['createtime'],$contactsData['passport'],$contactsData['expirydate'],$contactsData['mobile'],$contactsData['email']);
                }


                ?>
                <tr nobr="true">
                    <?php foreach($table_contents as $table_content){ ?>
                    <td><?php echo $table_content; ?></td>
                    <?php } ?>
                </tr>

                <?php
                $no++;

            }
        }

		
		?>
		
		
	</table>
	<div style="height:100px"></div>
	<div style="text-align:right;">Updated:<?php echo date("m/d/Y"); ?></div>
	
<?php



$pdfData = ob_get_clean() ;


// output the HTML content
$pdf->writeHTML($pdfData, true, false, true, false, '');


// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

$PDF_OUTPUT = isset($PDF_OUTPUT ) ? $PDF_OUTPUT : 'I' ;

if (ob_get_contents()) ob_end_clean();
//Close and output PDF document
return $pdf->Output($report_slug.'.pdf', $PDF_OUTPUT );

//============================================================+
// END OF FILE
//============================================================+
