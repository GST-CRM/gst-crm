<?php
//Add a new general expense and its lines
if(isset($_POST['NewExpense'])){
	$Warning = "";
	$GroupReference = mysqli_real_escape_string($db, $_POST['GroupReference']);
	if($GroupReference == "") { $GroupReference = 0; }
	$PayeeReference = mysqli_real_escape_string($db, $_POST['PayeeReference']);
	$ExpenseDate = mysqli_real_escape_string($db, $_POST['ExpenseDate']);
	$due_date = mysqli_real_escape_string($db, $_POST['due_date']);
	$ExpensesNotes = mysqli_real_escape_string($db, $_POST['ExpensesNotes']);
	$submitBtn = mysqli_real_escape_string($db, $_POST['submitBtn']);
	
	if($PayeeReference == NULL) { $Warning .= "Please select a payee for this expense<br />";}
	if($ExpenseDate == NULL) { $Warning .= "Please select the expense date<br />";}
	$NewPayeeCheck = (is_numeric($PayeeReference) == 1) ? 0 : 1;
	
	//If there is a new payee name and all field filled out, insert that payee to database and get the ID
	if($NewPayeeCheck == 1 AND $Warning == "") {
		$AddPayeeConn = new mysqli($servername, $username, $password, $dbname);
		if ($AddPayeeConn->connect_error) {die("Connection failed: " . $AddPayeeConn->connect_error);}
		$PayeeQuery = "INSERT INTO expenses_payee (Payee_Name) VALUES ('$PayeeReference');";
		if ($AddPayeeConn->query($PayeeQuery) === TRUE) {
			//echo "<h3>Success</h3>";
			$PayeeReference = $AddPayeeConn->insert_id;
		} else {
		$Warning .= $PayeeQuery . "<br>" . $AddPayeeConn->error;
		}
		$AddPayeeConn->close();
	}


	if($Warning == "") {
		if ($_FILES["fileToUpload"]["name"] != "") {
			$target_dir = "uploads/";
			$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			$temp = explode(".", $_FILES["fileToUpload"]["name"]);
			$filename = time() . '.' . end($temp);
			$fileurl = $target_dir . $filename;
			$mediaurl = $filename;
			$UploadMessageText = "";
			// Check if file already exists
			if (file_exists($target_file)) {
				$UploadMessageText .= "Sorry, file already exists.";
				$uploadOk = 0;
			}
			// Check file size
			if ($_FILES["fileToUpload"]["size"] > 50000000) {
				$UploadMessageText .= "Sorry, your file is too large.";
				$uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "pdf"
			&& $imageFileType != "gif" && $imageFileType != "doc" && $imageFileType != "docx" ) {
				$UploadMessageText .= "Sorry, only JPG, JPEG, PNG, GIF & PDF & DOCX & DOC files are allowed.";
				$uploadOk = 0;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
				//$UploadMessageText .= "Sorry, your file was not uploaded.";
				$UploadMessage = base64_encode($UploadMessageText);
				echo "<meta http-equiv='refresh' content='0;url=../contacts-edit.php?id=$userid&action=edit&usertype=attachments&message=$UploadMessage'>";
			// if everything is ok, try to upload file
			}
		move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $fileurl);
		}
		$sql7 = "INSERT INTO expenses (Expense_Date,Payee_ID,Group_ID,Due_Date,Comments,Attachment) VALUES (NOW(),'$PayeeReference','$GroupReference','$due_date','$ExpensesNotes','$mediaurl');";
		$sql7 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','expenses','$theid','added','$fullname');";
		if ($db->multi_query($sql7) === TRUE) {
			//Success first query
			$ExpenseNum = $db->insert_id;
			$query = "";
			$ExpenseLine = 1;
			$ExpenseTotal = 0;
			$ExpLinesConn = new mysqli($servername, $username, $password, $dbname);
			if ($ExpLinesConn->connect_error) {die("Connection failed: " . $ExpLinesConn->connect_error);}
			for($count = 0; $count < count($_POST["exp_category"]); $count++) {
				$ExpCategory = $_POST["exp_category"][$count];
				$ExpAmount = $_POST["exp_amount"][$count];
				$ExpDesc = $_POST["exp_desc"][$count];
				$ExpenseTotal = $ExpenseTotal + $ExpAmount;
				$query .= "INSERT INTO expenses_line 
						(Expense_Num, Expense_Line, Expense_Line_Category, Expense_Line_Amount, Expense_Line_CoA) 
						VALUES ('$ExpenseNum','$ExpenseLine','$ExpCategory','$ExpAmount','$ExpDesc');";
				$ExpenseLine++;
			}
			$query .= "UPDATE expenses SET Expense_Amount='$ExpenseTotal' WHERE Expense_Num=$ExpenseNum;";
			if ($ExpLinesConn->multi_query($query) === TRUE) {
				
				GUtils::setSuccess("The general expense has been recorded successfully!");
			    if ($submitBtn == 'save_close') {
			        GUtils::redirect('expenses.php');
			    } elseif ($submitBtn == 'save_back') {
			        GUtils::redirect('expenses-edit.php?id='.$ExpenseNum.'&action=edit');
			    } elseif ($submitBtn == 'save_group') {
			        GUtils::redirect('groups-edit.php?id='.$GroupReference.'&action=edit&tab=expenses');
			    } else {
			        GUtils::redirect('expenses-add.php');
			    }

				//echo "<h3>Success</h3>The general expense has been recorded! The page will be refreshed in seconds.";
				// echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong><h3>Success!</h3>The general expense has been recorded! The page will be refreshed in seconds.</strong></div>";
				// echo "<meta http-equiv='refresh' content='3;expenses-edit.php?id=$ExpenseNum&action=edit'>";
			} else {
			echo $query . "<br>" . $ExpLinesConn->error;
			}
			$ExpLinesConn->close();
		} else {
			echo $sql7 . "<br>" . $db->error;
		}
	} else {
		//echo $Warning;
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong><h3>Error!</h3>".$Warning."</strong></div>";
	}
}

//Edit a current general expense and its lines
if(isset($_POST['EditExpense'])){
	$Warning = "";
	$ExpenseNum = mysqli_real_escape_string($db, $_POST['EditExpense']);
	$ExpenseDate = mysqli_real_escape_string($db, $_POST['ExpenseDate']);
	$due_date = mysqli_real_escape_string($db, $_POST['due_date']);
	$ExpensesNotes = mysqli_real_escape_string($db, $_POST['ExpensesNotes']);
	
	if($ExpenseDate == NULL) { $Warning .= "Please select the expense date<br />";}
	
	if($Warning == "") {
		if ($_FILES["fileToUpload"]["name"] != "") {
			$target_dir = "uploads/";
			$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			$temp = explode(".", $_FILES["fileToUpload"]["name"]);
			$filename = time() . '.' . end($temp);
			$fileurl = $target_dir . $filename;
			$mediaurl = $filename;
			$UploadMessageText = "";
			// Check if file already exists
			if (file_exists($target_file)) {
				$UploadMessageText .= "Sorry, file already exists.";
				$uploadOk = 0;
			}
			// Check file size
			if ($_FILES["fileToUpload"]["size"] > 50000000) {
				$UploadMessageText .= "Sorry, your file is too large.";
				$uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "pdf"
			&& $imageFileType != "gif" && $imageFileType != "doc" && $imageFileType != "docx" ) {
				$UploadMessageText .= "Sorry, only JPG, JPEG, PNG, GIF & PDF & DOCX & DOC files are allowed.";
				$uploadOk = 0;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
				//$UploadMessageText .= "Sorry, your file was not uploaded.";
				$UploadMessage = base64_encode($UploadMessageText);
				echo "<meta http-equiv='refresh' content='0;url=../contacts-edit.php?id=$userid&action=edit&usertype=attachments&message=$UploadMessage'>";
			// if everything is ok, try to upload file
			}
		move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $fileurl);
		}
		$sql7 = "UPDATE expenses SET Expense_Date='$ExpenseDate',Due_Date='$due_date',Comments='$ExpensesNotes',Attachment='$mediaurl' WHERE Expense_Num=$ExpenseNum;";
		$sql7 .= "DELETE FROM expenses_line WHERE Expense_Num=$ExpenseNum;";
		$sql7 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','expenses','$theid','edited','$fullname');";
		
		$query = "";
		$ExpenseLine = 1;
		$ExpenseTotal = 0;
		for($count = 0; $count < count($_POST["exp_category"]); $count++) {
			$ExpCategory = $_POST["exp_category"][$count];
			$ExpAmount = $_POST["exp_amount"][$count];
			$ExpDesc = $_POST["exp_desc"][$count];
			$ExpenseTotal = $ExpenseTotal + $ExpAmount;
			$sql7 .= "INSERT INTO expenses_line 
					(Expense_Num, Expense_Line, Expense_Line_Category, Expense_Line_Amount, Expense_Line_CoA) 
					VALUES ('$ExpenseNum','$ExpenseLine','$ExpCategory','$ExpAmount','$ExpDesc');";
			$ExpenseLine++;
		}
		$sql7 .= "UPDATE expenses SET Expense_Amount='$ExpenseTotal' WHERE Expense_Num=$ExpenseNum;";

		if ($db->multi_query($sql7) === TRUE) {
			//Success first query
			//echo "<h3>Success</h3>The general expense has been edited! The page will be refreshed in seconds.";
			echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong><h3>Success!</h3>The general expense has been updated! The page will be refreshed in seconds.</strong></div>";
			echo "<meta http-equiv='refresh' content='3;expenses-edit.php?id=$ExpenseNum&action=edit'>";
		} else {
			echo $sql7 . "<br>" . $db->error;
		}
	} else {
		echo $Warning;
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong><h3>Error!</h3>".$Warning."</strong></div>";
	}
}


//Add a new general expense payment
if(isset($_POST['ExpenseAction']) AND $_POST['ExpenseAction'] == "add"){
	include "../session.php";

	$Warning = "";
	$Expense_Num = mysqli_real_escape_string($db, $_POST['ExpenseID']);
	$Payee_ID = mysqli_real_escape_string($db, $_POST['Payee_ID']);
	$Exp_Payment_Date = mysqli_real_escape_string($db, $_POST['Exp_Payment_Date']);
	$Exp_Payment_Method = mysqli_real_escape_string($db, $_POST['Exp_Payment_Method']);
	$Exp_Payment_Amount = mysqli_real_escape_string($db, $_POST['Exp_Payment_Amount']);
	$Exp_Payment_Comments = mysqli_real_escape_string($db, $_POST['Exp_Payment_Comments']);
	$GroupReferenceID = mysqli_real_escape_string($db, $_POST['GroupReferenceID']);
	
	if($Exp_Payment_Date == NULL) { $Warning .= "Please select the payment date<br />";}
	if($Exp_Payment_Method == NULL) { $Warning .= "Please select the payment method<br />";}
	if($Exp_Payment_Amount == NULL OR $Exp_Payment_Amount == "0.00") { $Warning .= "Please fill in the payment amount<br />";}
	
    
        
    $paymentAccountFrom = '';
    $paymentCheck = '';
    if( $Exp_Payment_Method == 'Cash' ) {
        $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_Cash']);
    } elseif( $Exp_Payment_Method == 'Bank Transfer' ) {
        $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_Bank']);
    } elseif( $Exp_Payment_Method == 'Check' ) {
        $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_Bank']);
        $paymentCheck = mysqli_real_escape_string($db, $_POST['Check_Number']);
        $AddNewCheckSQL = "INSERT INTO checks(Check_Type, Check_Number, Check_Customer_ID, Check_Bank_ID, Check_Amount, Check_Date, Check_Note, Check_Created_By) VALUES (2,'$paymentCheck','$Payee_ID','$paymentAccountFrom','$Exp_Payment_Amount','$Exp_Payment_Date','$Exp_Payment_Comments','$UserAdminID')";
        GDb::execute($AddNewCheckSQL);
        $IncreaseCheckNumSQL = "UPDATE checks_bank SET Bank_Check_Number=Bank_Check_Number+1 WHERE Bank_ID=$paymentAccountFrom";
        GDb::execute($IncreaseCheckNumSQL);
    } elseif( $Exp_Payment_Method == 'Credit Card' ) {
        $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_Card']);
    } elseif( $Exp_Payment_Method == 'Online Payment' ) {
        $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_PayPal']);
    }
    

	if($Warning == "") {

		$sql7 = "INSERT INTO expenses_payments(Expense_Num,Expense_Group_ID,Exp_Payment_Date,Exp_Payment_Amount,Exp_Payment_Account,Exp_Payment_Method,Exp_Payment_Check,Exp_Payment_Comments)
		VALUES ('$Expense_Num','$GroupReferenceID','$Exp_Payment_Date','$Exp_Payment_Amount','$paymentAccountFrom','$Exp_Payment_Method','$paymentCheck','$Exp_Payment_Comments');";
		$sql7 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','general expense payment','$Expense_Num','added','$Exp_Payment_Comments');";
		if ($db->multi_query($sql7) === TRUE) {
			echo "<h3>Success</h3>The general expense payment has been recorded! The page will be refreshed in seconds.";
			//echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong><h3>Success!</h3>The general expense has been recorded! The page will be refreshed in seconds.</strong></div>";
			echo "<meta http-equiv='refresh' content='3;expenses-edit.php?id=$Expense_Num&action=edit'>";
		} else {
			echo $sql7 . "<br>" . $db->error;
		}
	} else {
		//echo $Warning;
		echo "<h3>Error!</h3>".$Warning;
	}
}



?>
