<?php

include_once __DIR__ . '/../models/customer_account.php';
include_once __DIR__ . '/../models/customer_invoice.php';
include_once __DIR__ . '/../models/customer_payment.php';
include_once __DIR__ . '/../models/customer_invoice_line.php';
include_once __DIR__ . '/../models/customer_refund.php';
include_once __DIR__ . '/../models/transactions.php';

$code = 0;

try {
    //intialize dependencies..
    $customerInvoice     = new CustomerInvoice();
    $customerPayment     = new CustomerPayment();
    $customerInvoiceLine = new CustomerInvoiceLine();
    $customerRefund      = new CustomerRefund();
    $transactions        = new Transactions();

    //find invoice data first
    $invoiceId = $__REQUEST['invoiceId'];

    $invoiceData = $customerPayment->customerInvoiceDetails($invoiceId);

    Model::begin();

    if ($invoiceData['Additional_Invoice'] == 1) {
        $thePayee       = (new CustomerGroups())->getPrimaryTraveler($invoiceData['tourid'], $invoiceData['contactid']);
        $payeeInvoiceId = $customerInvoice->getScalar('Customer_Invoice_Num', ['Customer_Account_Customer_ID' => $thePayee, 'Group_ID' => $invoiceData['tourid']]);
    } else {
        $thePayee       = $invoiceData['contactid'];
        $payeeInvoiceId = $invoiceId;
    }
    //Store in payees Invoice ID
    //Should Cancel only if refund status IS CustomerRefund::$CREATED
    $refundInfo = $customerRefund->get(['Customer_Account_Customer_ID' => $thePayee, 'Customer_Invoice_Num' => $invoiceId, 'Payee_Invoice_Num' => $payeeInvoiceId]);
    if ($refundInfo['Status'] == CustomerRefund::$CREATED) {
        $customerRefund->update( [ 'Status' => CustomerRefund::$DELETED, 
                                    'Refund_Amount' => 0, 
                                    'Issued_Amount' => 0 ],
                                [ 'Customer_Account_Customer_ID' => $thePayee,
                                    'Customer_Invoice_Num' => $invoiceId ] ); /* WHERE - 'Payee_Invoice_Num' => $payeeInvoiceId */
        
        $transactions->delete(['Transaction_Type' => 5, 'Chart_of_Accounts_GL_Account_N' => '1200', '`Customer_Account _Customer_ID`' => $thePayee]);
    }
    $customerInvoiceLine->delete(['Customer_Invoice_Num' => $invoiceId, 'Customer_Invoice_Line' => 1, 'Product_Name' => 'Cancellation Charge']);
    $invoiceStatus = (($isJoint) ? CustomerInvoice::$JOINT : CustomerInvoice::$ACTIVE) ;
    $status = $customerInvoice->update([
        'status' => $invoiceStatus,
        'Mod_Date' => Model::sqlNoQuote('now()')
        ], ['Customer_Invoice_Num' => $invoiceId]);
    
} catch (\Exception $e) {
    $code = $e->getCode();
}
if ($status) {
    Model::commit();
    GUtils::jsonOk(['message' => 'Successfully restored.']);
} else {
    Model::rollback();
    GUtils::jsonFail(['message' => "Failed to save, Please try again (Error: $code)."]);
}



