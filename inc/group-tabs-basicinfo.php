				<form name="contact1" action="inc/group-functions.php" class="row" method="POST" id="contact1">
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Group's Status</label>
						<div class="can-toggle">
							<input id="e" name="status" value="1" type="checkbox" <?php if($data["Group_Status"] == "1") {echo "checked";} ?>>
							<label for="e">
								<div class="can-toggle__switch" data-checked="Active" data-unchecked="Inactive"></div>
							</label>
						</div>
					</div>
					<div class="form-group form-default form-static-label col-sm-2">
						<label class="float-label">Group's Code</label>
						<input type="text" name="groupcode" class="disabled form-control" value="<?php echo "T".$data["tourid"]; ?>" readonly>
						<input type="text" name="tourid" class="disabled form-control" value="<?php echo $data["tourid"]; ?>" hidden>
					</div>
					<div class="form-group form-default form-static-label col-sm-7">
						<label class="float-label">Group's Name</label>
						<input type="text" name="tourname" class="form-control" value="<?php echo $data["tourname"]; ?>" required>
					</div>
					<div class="form-group form-default form-static-label col-sm-8">
						<label class="float-label">Group Reference Number</label>
						<input type="text" name="tourdesc" class="form-control" value="<?php echo $data["tourdesc"]; ?>">
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Group's Destination</label>
						<input type="text" name="destination" class="form-control" value="<?php echo $data["destination"]; ?>">
					</div>
					<div class="form-group form-default form-static-label col-sm-12">
						<label class="float-label"><img src="<?php echo $crm_images; ?>/website-link.png" alt="Group Online Link" width="15" /> Online Group Link <?php if($data["Group_URL"] !=NULL) { echo "<a href='".$data["Group_URL"]."' target='_blank'> - <u>Click here to open the link.</u></a>"; } ?></label>
						<input type="text" name="Group_URL" class="form-control" value="<?php echo $data["Group_URL"]; ?>">
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Group's Start Date</label>
						<input type="date" name="startdate" class="form-control" value="<?php echo $data["startdate"]; ?>" required>
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Group's End Date</label>
						<input type="date" name="enddate" class="form-control" value="<?php echo $data["enddate"]; ?>" required>
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Church Name</label>
						<input type="text" name="ChurchName" class="form-control" value="<?php echo $data["Church_Name"]; ?>">
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Departure City</label>
						<input type="text" name="DepartureCity" class="form-control" value="<?php echo $data["Departure_City"]; ?>">
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Deposit Amount</label>
						<div class="dollar"><input type="text" name="DepositAmount" class="form-control" value="<?php echo $data["Group_Deposit_Amount"]; ?>"></div>
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">2nd Payment Amount</label>
						<div class="dollar"><input type="text" name="SecondPayment" class="form-control" value="<?php echo $data["2nd_Payment_Amount"]; ?>"></div>
					</div>
                    <div class="form-group form-default form-static-label col-sm-8">
						<label class="float-label">Itinerary</label>
                        <textarea  rows="5" name="itinerary" class="tinyMceEditor form-control"><?php echo $data["itinerary"]; ?></textarea>
					</div>
                    <?php /* 3 New columns for managing different due dates. 
                    <div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Invoice Due Date Before</label>
						<input type="number" name="DueBefore" class="form-control" value="<?php echo (isset($data["due_before"]) ? $data["due_before"] : 90); ?>" >
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">2nd Payment Due Date Before</label>
                        <input type="number" name="SecondDueBefore" class="form-control" value="<?php echo (isset($data["second_due_before"]) ? $data["second_due_before"] : 180); ?>" >
					</div>
					<div class="form-group form-default form-static-label col-sm-4">
						<label class="float-label">Final Due Date Before</label>
						<input type="number" name="FinalDueBefore" class="form-control" value="<?php echo (isset($data["final_due_before"]) ? $data["final_due_before"] : 90); ?>" >
					</div>
                     */?>
                    
                    <?php if( AclPermission::actionAllowed('GroupAgentInfo') ) { ?>
                    <div class="form-group form-default col-sm-12">
                        <?php include __DIR__ . '/group-tabs-basicinfo-agents.php' ; ?>
                    </div>
                    <?php } ?>
                    <div class="col-sm-12"><br />
                        <?php if( AclPermission::actionAllowed('GroupSave') ) { ?>
					<button type="submit" name="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Update</button>
                        <?php } ?>
					<button type="reset" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Cancel</button>
					</div>
				</form>

    <!--<script src="https://cdn.ckeditor.com/4.12.1/standard-all/ckeditor.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.2.2/tinymce.min.js" integrity="sha256-MYEJOVodtWOmhHp5ueLNwfCwBBGKWJWUucfLvXzdles=" crossorigin="anonymous"></script>
