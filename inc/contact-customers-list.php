<?php
	//include connection file 
	include "../config.php";
	 
    include_once __DIR__ . '/../models/acl_permission.php';
    
// { Acl Condition
$GroupID = AclPermission::userGroup() ;
$ACL_CONDITION = " WHERE 1 " ;
if( AclPermission::roleFlags() & AclRole::$FLAG_ONLY_GROUP ) {
    $ACL_CONDITION = " WHERE cus.tourid IN($GroupID) " ;
}
// }
// 
	// initilize all variable
	$params = $columns = $totalRecords = $data = array();

	$params = $_REQUEST;

	//define index of column
	$columns = array( 
		0 => 'id',
		1 => 'contactid', 
		2 => 'title', 
		3 => 'fname', 
		4 => 'mname',
		5 => 'lname',
		6 => 'tourid',
		7 => 'tourname',
		8 => 'passport',
		9 => 'phone',
		10 => 'updatetime'
	);

	$where = $sqlTot = $sqlRec = "";

	// check search value exist
	if( !empty($params['search']['value']) ) {   
		$theSearchText = $params['search']['value'];
		$theSearchText2 = str_replace(" ","%",$theSearchText);
		$where .= " AND concat(con.fname,con.mname,con.lname,cus.tourid) LIKE '%".$theSearchText2."%'";
	}
	//Advanced Filters for the Customer Accounts List Page
	$CustomerFilters = "";
	
	//The Status of the customer filter
	$cStatus = $__GET['status'];
	if($cStatus == "Disabled") {
		$CustomerFilters.= " AND cus.status='0' ";
	}elseif($cStatus == "Active") {
		$CustomerFilters.= " AND cus.status='1' ";
	}elseif($cStatus == "Cancelled") {
		$CustomerFilters.= " AND ci.Status='4' ";
	}elseif($cStatus == "both") {
		$CustomerFilters.= "  ";
	} else {
		$CustomerFilters.= " AND cus.status='1' ";
	}
	
	$cBirthday = $__GET['cb'];
	if($cBirthday == "NoBirth") {
		$CustomerFilters.= " AND con.birthday <= 1 ";
	}
	
	$cPassport = $__GET['cp'];
	if($cPassport == "NoPP") {
		$CustomerFilters.= " AND con.passport IN ('','PASSPORT PENDING','PENDING PASSPORT') ";
	}

	// getting total number records without any search
	$sql = "SELECT con.id,cus.contactid,con.title,con.fname,con.mname,con.lname,cus.tourid,gr.tourname,con.phone,con.email, CONCAT(DATE_FORMAT(cus.updatetime, '%m/%d/%Y - '),TIME_FORMAT(cus.updatetime, '%h:%i %p')) AS updatetime
FROM customer_account cus JOIN contacts con ON cus.contactid=con.id JOIN groups gr ON gr.tourid=cus.tourid $ACL_CONDITION ";
	//$sql = "SELECT id,title,fname FROM contacts ";
	$sqlTot .= $sql.$CustomerFilters;
	$sqlRec .= $sql.$CustomerFilters;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where.$CustomerFilters;
		$sqlRec .= $where.$CustomerFilters;
	}


 	$sqlRec .=  " GROUP BY con.id ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']." LIMIT ".$params['start']." ,".$params['length']." ";
 	//$sqlRec .=  " ORDER BY cus.updatetime DESC LIMIT ".$params['start']." ,".$params['length']." ";

	$queryTot = mysqli_query($db, $sqlTot) or die("database error:". mysqli_error($db));


	$totalRecords = mysqli_num_rows($queryTot);

	$queryRecords = mysqli_query($db, $sqlRec) or die("error to fetch employees data");

	//iterate on results row and create new index array of data
	while( $row = mysqli_fetch_row($queryRecords) ) { 
		$data[] = $row;
	}	

	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>
