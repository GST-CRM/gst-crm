<form  id="contact6" class="airline-parent" name="contact6" method="post" action="groups-edit.php?id=<?php echo $tourid; ?>&action=edit">
    <div class="row">
                    <div class="form-group col-sm-12">
                        <h3>Add a new Airfare Product</h3>
                    </div>
                    <div class="form-group col-sm-4">
                        <h4 class="sub-title">Product Status</h4>
						<div class="can-toggle">
							<input id="e" name="status" value="1" type="checkbox" checked>
							<label for="e">
								<div class="can-toggle__switch" data-checked="Active" data-unchecked="Inactive"></div>
							</label>
						</div>
                    </div>
                    <div class="col-sm-4">
						<h4 class="sub-title">Product's Supplier</h4>
						<select name="thesupplier" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<?php //here, we are checking the Agent ID with the contact column list to show their names
							$con7 = new mysqli($servername, $username, $password, $dbname);
							$result7 = mysqli_query($con7,"SELECT id, fname, mname, lname FROM contacts WHERE `usertype` LIKE '%2%'");
							while($row7 = mysqli_fetch_array($result7))
							{
								echo "<option value='".$row7['id']."'>".$row7['fname']." ".$row7['mname']." ".$row7['lname']."</option>";
							} ?>
						</select>
                    </div>
                    <div class="col-sm-4">
						<h4 class="sub-title">Product's Category</h4>
						<select name="thecategory" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<option value="1" selected>Airline Products</option>
						</select>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label">Airline Name</label>
                        <select class="select2-airline col-sm-12" name="productname" multiple="multiple" >
                        </select>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-5">
                        <label class="float-label">Product Discription</label>
                        <input type="text" name="description" class="form-control">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-2">
                        <label class="float-label">Cost $</label>
                        <input type="text" name="cost" class="form-control">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-2">
                        <label class="float-label">Price $</label>
                        <input type="text" name="price" class="form-control">
                    </div>
                    <div class="col-sm-12"><br />
					<button type="submit" name="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;"><i class="far fa-check-circle"></i>Add</button>
                    <button type="reset" class="btn waves-effect waves-light btn-inverse" data-dismiss="modal"><i class="fas fa-ban"></i>Cancel</button>
					</div>
	</div>
</form>
