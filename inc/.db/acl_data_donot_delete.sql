-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `acl_action`;
CREATE TABLE `acl_action` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Page_ID` int(11) NOT NULL,
  `Action` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `acl_action` (`ID`, `Page_ID`, `Action`) VALUES
(1,	1,	'Index'),
(2,	2,	'Index'),
(3,	3,	'Index'),
(4,	4,	'Index'),
(5,	5,	'Index'),
(6,	6,	'Index'),
(7,	7,	'Index'),
(8,	8,	'Index'),
(12,	5,	'Add'),
(13,	6,	'AirfareCost'),
(14,	6,	'AirlineCreditAmount'),
(15,	6,	'IsAirlineReady'),
(16,	6,	'GroupCost'),
(17,	6,	'LandInfoUpdate'),
(18,	6,	'LandCost'),
(19,	6,	'SubProductsAssigned'),
(20,	6,	'TabExpenses'),
(21,	6,	'PaxBox'),
(22,	6,	'GroupSave'),
(23,	6,	'GroupAddCustomer'),
(24,	9,	'Index'),
(25,	5,	'BoxEdit'),
(26,	5,	'SupplierInfo'),
(27,	6,	'GroupNotes'),
(28,	6,	'GroupEmail'),
(29,	6,	'TabSentEmails'),
(30,	6,	'NameBadges'),
(31,	6,	'ShippingLabels'),
(32,	6,	'EmergencyContacts'),
(33,	6,	'TourLeaderReport'),
(34,	6,	'TabPax'),
(35,	6,	'GroupAgentInfo'),
(36,	6,	'GroupAirlineDetails'),
(37,	6,	'TabGroupInvoice'),
(38,	10,	'Index'),
(39,	11,	'Index'),
(40,	12,	'Index'),
(41,	12,	'ContactSupplier'),
(42,	13,	'Index'),
(43,	14,	'Index'),
(44,	15,	'Index'),
(45,	16,	'Index'),
(46,    13, 'EditGroupNote'),
(47,	17,	'Index'),
(48,	18,	'Index'),
(49,	19,	'Index'),
(50,	20,	'Index'),
(51,	21,	'Index'),
(52,	22,	'Index'),
(53,	23,	'Index'),
(54,	24,	'Index'),
(55,	25,	'Index'),
(56,	26,	'Index'),
(57,	27,	'Index');

DROP TABLE IF EXISTS `acl_pages`;
CREATE TABLE `acl_pages` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Page` varchar(100) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `acl_pages` (`ID`, `Page`) VALUES
(1,	'sales.php'),
(2,	'customers.php'),
(3,	'sales-invoices.php'),
(4,	'payments.php'),
(5,	'groups.php'),
(6,	'groups-edit.php'),
(7,	'reports.php'),
(8,	'account_receivable.php'),
(9,	'dashboard.php'),
(10,	'group-tabs-reports-preview.php'),
(11,	'excel.php'),
(12,	'contacts-edit-direct.php'),
(13,	'group-functions.php'),
(14,	'customer-page-profile.php'),
(15,	'customer-page-invoice.php'),
(16,	'customer-page-payment.php'),
(17,	'customer-page-groups.php'),
(18,	'customer-save.php'),
(19,	'sales-invoice-print.php'),
(20,	'customer-page-attachment.php'),
(21,	'customer-upload.php'),
(22,	'tickets-action.php'),
(23,	'email-templates.php'),
(24,	'email-templates-add.php'),
(25,	'email-templates-edit.php'),
(26,	'email-template-functions.php'),
(27,	'group-tabs-mail.php');


DROP TABLE IF EXISTS `acl_permission`;
CREATE TABLE `acl_permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Role_ID` int(11) NOT NULL,
  `Action_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `acl_permission` (`ID`, `Role_ID`, `Action_ID`) VALUES
(10,	2,	1),
(11,	2,	2),
(12,	2,	3),
(13,	2,	4),
(14,	2,	5),
(15,	2,	6),
(16,	2,	7),
(17,	2,	8),
(19,	2,	24),
(23,	4,	3),
(24,	4,	4),
(25,	4,	5),
(26,	4,	6),
(29,	4,	24),
(33,	8,	3),
(34,	8,	4),
(35,	8,	5),
(36,	8,	6),
(39,	8,	24),
(41,	4,	28),
(42,	8,	38),
(43,	8,	39),
(44,	4,	38),
(45,	4,	39),
(46,	2,	38),
(47,	2,	39),
(48,	8,	40),
(49,	8,	41),
(50,	4,	34),
(51,	4,	37),
(52,	4,	33),
(53,	2,	33),
(54,	2,	27),
(55,	2,	34),
(56,	2,	37),
(57,	2,	42),
(58,	2,	46),
(59,	4,	53),
(60,	4,	54),
(61,	4,	55),
(62,	4,	56),
(63,	4,	57);


DROP TABLE IF EXISTS `acl_role`;
CREATE TABLE `acl_role` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Role` varchar(32) NOT NULL,
  `Permission` enum('Grant','Deny') NOT NULL,
  `Flags` smallint(6) NOT NULL DEFAULT '0' COMMENT '0x01 : Group Limited',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `acl_role` (`ID`, `Role`, `Permission`, `Flags`) VALUES
(1,	'Admin',	'Grant',	0),
(2,	'Agents',	'Deny',	1),
(4,	'GroupLeaders',	'Deny',	1),
(8,	'Suppliers',	'Deny',	2);

-- BELOW NEED TO RECONFIRM

INSERT INTO `acl_role` (`ID`, `Role`, `Permission`, `Flags`)
VALUES ('16', 'Customer', 'Deny', 2);


INSERT INTO `acl_permission` ( `Role_ID`, `Action_ID`) VALUES
(16,	3),
(16,	4),
(16,	5),
(16,	6),
(16,	24),
(16,	38),
(16,	39),
(16,	40),
(16,	41),
(16,	48),
(16,	43),
(16,	44),
(16,	45),
(16,	47),
(16,	48),
(16,	49),
(16,	50),
(16,	51),
(16,	52);

ALTER TABLE `acl_role`
CHANGE `Flags` `Flags` smallint(6) NOT NULL DEFAULT '0' COMMENT '0x01 : Group Limited, 0x02 : Only mine' AFTER `Permission`;

