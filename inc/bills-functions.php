<?php

//Add a new general expense and its lines
if(isset($_POST['NewBill'])){
	$Warning = "";
	$SupplierID = $_POST['SupplierID'];
	$GroupID = $_POST['GroupID'];
	$Bill_Date = $_POST['Bill_Date'];
	$Due_Date = $_POST['Due_Date'];
	$Bill_Comments = $_POST['Bill_Comments'];
	$BillTotal = $_POST['BillTotal'];
	
	if($SupplierID == NULL) { $Warning .= "Please select a supplier for this Supplier Bill<br />";}
	if($Bill_Date == NULL) { $Warning .= "Please select the create date of the Bill<br />";}
	if($Due_Date == NULL) { $Warning .= "Please select the due date of the Bill<br />";}
	
	if($Warning == "") {
		if ($_FILES["fileToUpload"]["name"] != "") {
			$target_dir = "uploads/";
			$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			$temp = explode(".", $_FILES["fileToUpload"]["name"]);
			$filename = time() . '.' . end($temp);
			$fileurl = $target_dir . $filename;
			$mediaurl = $filename;
			$UploadMessageText = "";
			// Check if file already exists
			if (file_exists($target_file)) {
				$UploadMessageText .= "Sorry, file already exists.";
				$uploadOk = 0;
			}
			// Check file size
			if ($_FILES["fileToUpload"]["size"] > 50000000) {
				$UploadMessageText .= "Sorry, your file is too large.";
				$uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "pdf"
			&& $imageFileType != "gif" && $imageFileType != "doc" && $imageFileType != "docx" ) {
				$UploadMessageText .= "Sorry, only JPG, JPEG, PNG, GIF & PDF & DOCX & DOC files are allowed.";
				$uploadOk = 0;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
				//$UploadMessageText .= "Sorry, your file was not uploaded.";
				$UploadMessage = base64_encode($UploadMessageText);
				echo "<meta http-equiv='refresh' content='0;url=../contacts-edit.php?id=$userid&action=edit&usertype=attachments&message=$UploadMessage'>";
			// if everything is ok, try to upload file
			}
		move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $fileurl);
		}
		$sql7 = "INSERT INTO suppliers_bill (Supplier_Bill_Date,Bill_Amount,Group_ID,Purchase_Order_Num,Transaction_Type_Transaction_ID,Supplier_ID,Due_Date,Comments,Attachment)
		VALUES ('$Bill_Date','$BillTotal','$GroupID','0','0','$SupplierID','$Due_Date','$Bill_Comments','$mediaurl');";
		$sql7 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','Supplier Bill','$SupplierID','added','Created a new supplier bill');";
		if ($db->multi_query($sql7) === TRUE) {
			//Success first query
			$SupplierBillNum = $db->insert_id;
			$query = "";
			$BillLine = 1;
			//$ExpenseTotal = 0;
			$ExpLinesConn = new mysqli($servername, $username, $password, $dbname);
			if ($ExpLinesConn->connect_error) {die("Connection failed: " . $ExpLinesConn->connect_error);}
			
			for($count = 0; $count < count($_POST["Bill_Product"]); $count++) {
				$Bill_Product_ID = $_POST["Bill_Product"][$count];
				$Bill_Product_Name = $_POST["product_name"][$count];
				$Bill_Quantity = $_POST["Bill_Line_Quantity"][$count];
				$Bill_Cost = $_POST["product_cost"][$count];
				$Bill_Category = $_POST["product_category"][$count];
					if($Bill_Category == 1) {$LineType = "Airline";}
					elseif($Bill_Category == 2) {$LineType = "Land";}
					elseif($Bill_Category == 3) {$LineType = "Insurance";}
					elseif($Bill_Category == 4) {$LineType = "Airline Upgrade";}
					elseif($Bill_Category == 5) {$LineType = "Land Upgrade";}
					elseif($Bill_Category == 6) {$LineType = "Ticket Discount";}
					elseif($Bill_Category == 7) {$LineType = "Single Supplement";}
				$query .= "INSERT INTO suppliers_bill_line
						(Supplier_Bill_Num, Supplier_Bill_Line, Line_Type, Product_Product_ID, Product_Name, Qty, Bill_Line_Total) 
						VALUES ('$SupplierBillNum','$BillLine','$LineType','$Bill_Product_ID','$Bill_Product_Name','$Bill_Quantity','$Bill_Cost');";
				$BillLine++;
			}

			//$query .= "UPDATE expenses SET Expense_Amount='$ExpenseTotal' WHERE Expense_Num=$SupplierBillNum;";
			if ($ExpLinesConn->multi_query($query) === TRUE) {

				GUtils::setSuccess("The Supplier Bill Record has been added successfully!");
			    if ($_POST['submitBtn'] == 'save_close') {
			        GUtils::redirect('bills.php');
			    } else {
			        GUtils::redirect('bills-add.php');
			    }

				//echo "<h3>Success</h3>The general expense has been recorded! The page will be refreshed in seconds.";
				// echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong><h3>Success!</h3>The Supplier Bill Record has been added successfully! The page will be refreshed in seconds.</strong></div>";
				// echo "<meta http-equiv='refresh' content='2;bills-edit.php?id=$SupplierBillNum&action=edit'>";
			} else {
			echo $query . "<br>" . $ExpLinesConn->error;
			}
			$ExpLinesConn->close();
		} else {
			echo $sql7 . "<br>" . $db->error;
		}
	} else {
		//echo $Warning;
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong><h3>Error!</h3>".$Warning."</strong></div>";
	}
}

//Edit a current general expense and its lines
if(isset($_POST['EditBill'])){
	$Warning = "";
	$SuppBillNum = $_POST['EditBill'];
	$Bill_Date = $_POST['Bill_Date'];
	$Due_Date = $_POST['Due_Date'];
	$Terms = $_POST['Terms'];
	$Bill_Comments = $_POST['Bill_Comments'];
	
	if($Bill_Date == NULL) { $Warning .= "Please specify the Supplier Bill Date<br />";}
	if($Due_Date == NULL) { $Warning .= "Please specify the Bill Due Date<br />";}
	
	if($Warning == "") {
		if ($_FILES["fileToUpload"]["name"] != "") {
			$target_dir = "uploads/";
			$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
			$uploadOk = 1;
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			$temp = explode(".", $_FILES["fileToUpload"]["name"]);
			$filename = time() . '.' . end($temp);
			$fileurl = $target_dir . $filename;
			$mediaurl = $filename;
			$UploadMessageText = "";
			// Check if file already exists
			if (file_exists($target_file)) {
				$UploadMessageText .= "Sorry, file already exists.";
				$uploadOk = 0;
			}
			// Check file size
			if ($_FILES["fileToUpload"]["size"] > 50000000) {
				$UploadMessageText .= "Sorry, your file is too large.";
				$uploadOk = 0;
			}
			// Allow certain file formats
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "pdf"
			&& $imageFileType != "gif" && $imageFileType != "doc" && $imageFileType != "docx" ) {
				$UploadMessageText .= "Sorry, only JPG, JPEG, PNG, GIF & PDF & DOCX & DOC files are allowed.";
				$uploadOk = 0;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
				//$UploadMessageText .= "Sorry, your file was not uploaded.";
				$UploadMessage = base64_encode($UploadMessageText);
				echo "<meta http-equiv='refresh' content='0;url=../contacts-edit.php?id=$userid&action=edit&usertype=attachments&message=$UploadMessage'>";
			// if everything is ok, try to upload file
			}
		move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $fileurl);
		}
		$sql7 = "UPDATE suppliers_bill SET Supplier_Bill_Date='$Bill_Date',Due_Date='$Due_Date',Comments='$Bill_Comments',Attachment='$mediaurl' WHERE Supplier_Bill_Num=$SuppBillNum;";
		$sql7 .= "DELETE FROM suppliers_bill_line WHERE Supplier_Bill_Num=$SuppBillNum;";
		$sql7 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','Supplier Bill','$SuppBillNum','edited','Edited the supplier bill #$SuppBillNum');";
		
		$query = "";
		$SuppBill_Line = 1;
		$Bill_Total = 0;
		for($count = 0; $count < count($_POST["Bill_Product"]); $count++) {
			$Bill_Product_ID = $_POST["Bill_Product"][$count];
			$Bill_Product_Name = $_POST["product_name"][$count];
			$Bill_Quantity = $_POST["Bill_Line_Quantity"][$count];
			$Bill_Cost = $_POST["product_cost"][$count];
			$Bill_Category = $_POST["product_category"][$count];
				if($Bill_Category == 1) {$LineType = "Airline";}
				elseif($Bill_Category == 2) {$LineType = "Land";}
				elseif($Bill_Category == 3) {$LineType = "Insurance";}
				elseif($Bill_Category == 4) {$LineType = "Airline Upgrade";}
				elseif($Bill_Category == 5) {$LineType = "Land Upgrade";}
				elseif($Bill_Category == 6) {$LineType = "Ticket Discount";}
				elseif($Bill_Category == 7) {$LineType = "Single Supplement";}
				
			$Bill_Total = $Bill_Total + ($Bill_Cost * $Bill_Quantity);
			$sql7 .= "INSERT INTO suppliers_bill_line
					(Supplier_Bill_Num, Supplier_Bill_Line, Line_Type, Product_Product_ID, Product_Name, Qty, Bill_Line_Total) 
					VALUES ('$SuppBillNum','$SuppBill_Line','$LineType','$Bill_Product_ID','$Bill_Product_Name','$Bill_Quantity','$Bill_Cost');";
			$SuppBill_Line++;
		}
		$sql7 .= "UPDATE suppliers_bill SET Bill_Amount='$Bill_Total' WHERE Supplier_Bill_Num=$SuppBillNum;";

		if ($db->multi_query($sql7) === TRUE) {
			//Success first query
			//echo "<h3>Success</h3>The general expense has been edited! The page will be refreshed in seconds.";
			echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong><h3>Success!</h3>The Supplier Bill has been updated! The page will be refreshed in seconds.</strong></div>";
			echo "<meta http-equiv='refresh' content='2;bills-edit.php?id=$SuppBillNum&action=edit'>";
		} else {
			echo $sql7 . "<br>" . $db->error;
		}
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong><h3>Error!</h3>".$Warning."</strong></div>";
	}
}



?>
