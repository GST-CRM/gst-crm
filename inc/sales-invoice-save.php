<?php

//Sample POST DATA
//---------
//CurrentCustomerSelected: 10048
//email: motherjean@aol.com
//billing_address: 109 Maple Avenue ::: 109 Maple Avenue,  ::: , 30064 ::: 30064, Marietta ::: Mariett, GA ::: GA, US
//invoice_date: 2019-02-23
//due_date: 2019-03-26
//product[94g7gsa]: 30003
//qty[94g7gsa]: 1
//product[dcgal4g]: 30004
//qty[dcgal4g]: 1

$invoiceId = 0 ;
if( isset($__GET['invid']) ) {
    $invoiceId = $__GET['invid'] ;
}

//$dueDate = GUtils::mysqlDateTime( $__REQUEST['due_date'] ) ;
$invoiceDate = GUtils::mysqlDateTime( $__REQUEST['invoice_date'] ) ;
$comments = $__REQUEST['comments'] ;
$action = $__REQUEST['submit'] ; 
$groupId = $__REQUEST['GroupID'] ; 
//upload file {
$config = [] ;
$config['max_size'] = 50000000 ;
$config['upload_path'] = 'invattach/';

GUtils::doUpload('filAttachment', $config) ;
$fileName = (isset($config['file_name']) ? $config['file_name'] : '') ;
//}

$cid = $__REQUEST['CurrentCustomerSelected'] ;
$accountId = $cid;

    Model::begin() ;

    if( $action == 'update_message_only' ) {
        $sqlcim = "UPDATE `customer_invoice` SET `Comments` = '$comments', `Mod_Date` = NOW() WHERE Customer_Invoice_Num ='$invoiceId' " ;
        $db->query($sqlcim) ;
        GUtils::setSuccess('Message updated successfully.') ;
        Model::commit() ;

        return ;
    }
    //when edit
    if( $action == 'update' || $action == 'update_close' || $action == 'update_new' || $action == 'update_only' ) {
        /*
         * CREATION NOT ALLOWED : Nithin (GST2-68)
         * ---------------------------------------
         */

        $qpart = '' ;
        if( $fileName ) {
            $qpart = " `Attachment`= '$fileName', " ;
        }
        //Due_Date='$dueDate',
        $sqlci = "UPDATE `customer_invoice` SET  Add_Date='$invoiceDate', `Comments` = '$comments', $qpart `Mod_Date` = NOW() WHERE Customer_Invoice_Num ='$invoiceId' " ;
        $db->query($sqlci) ;
    }
    
    GUtils::setSuccess('Invoice details saved successfully.') ;
    Model::commit() ;

//    //Send Option Removed from UI
//    
//    if( isset($__REQUEST['cbSend']) && $__REQUEST['cbSend'] == '1' ) {
//        $__REQUEST['inv'] = $invoiceId  ;
//        include 'sales-invoice-mail.php' ;
//    }