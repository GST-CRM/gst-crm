<?php
include "../session.php";

if(isset($_POST["New_Payee_Name"])) {
    $NewPayeeName = mysqli_real_escape_string($db, $_POST["New_Payee_Name"]);
    $Add_Query = "INSERT INTO expenses_payee(Payee_Name) VALUES('$NewPayeeName')";
    $Check_Query = "SELECT Payee_ID FROM expenses_payee WHERE Payee_Name='$NewPayeeName'";
    $Check_Query_Result = GDb::CountRows($Check_Query);
    if($Check_Query_Result > 0 ) {
        echo '<div class="alert alert-danger background-danger">This name ('.$NewPayeeName.') is already registered on our system.</div>';
    } else {
        if(GDb::execute($Add_Query)) {
            echo '<div class="alert alert-success background-success">The new payee ('.$NewPayeeName.') has been added successfully.</div>';
        } else {
            echo '<div class="alert alert-danger background-danger">There is a problem adding this payee: ('.$NewPayeeName.').</div>';
        }        
    }
}
?>