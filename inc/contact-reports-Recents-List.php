<?php
	//include connection file 
	include "../config.php";
	 
    
	// initilize all variable
	$params = $columns = $totalRecords = $data = array();

	$params = $_REQUEST;

	//define index of column
	$columns = array( 
		0 => 'id',
		1 => 'contactid', 
		2 => 'title', 
		3 => 'fname', 
		4 => 'mname',
		5 => 'lname',
		6 => 'tourid',
		7 => 'tourdesc',
		8 => 'tourname',
		9 => 'UserID',
        10 => 'createtime',
		11 => 'fname',
		12 => 'lname',
		13 => 'Cancel_Date'
		
	);

	$where = $sqlTot = $sqlRec = "";

	// check search value exist
	if( !empty($params['search']['value']) ) {   
		$theSearchText = $params['search']['value'];
		$theSearchText2 = str_replace(" ","%",$theSearchText);
		$where .= " AND concat(con.fname,con.mname,con.lname,gro.tourdesc,gro.tourname) LIKE '%".$theSearchText2."%'";
	}
	//Advanced Filters for the Customer Accounts List Page
	$CustomerFilters = "";
	
	//Get the values from the main page first
	$cStatus = $__GET['status'];
	$DateRangeFilter = $__GET['DateRange'];
	$FromDateFilter = $__GET['from'];
	$UntilDateFilter = $__GET['until'];
	$TodayDate = date("Y-m-d");

	//The Status of the customer filter
	if($cStatus == "Disabled") {
		$CustomerFilters.= " AND cus.status='0' ";
	}elseif($cStatus == "Active") {
		$CustomerFilters.= " AND cus.status='1' ";
	}elseif($cStatus == "Cancelled") {
		$CustomerFilters.= " AND cus.status='0' AND ci.Status='4' ";
	}elseif($cStatus == "both") {
		$CustomerFilters.= "  ";
	} else {
		$CustomerFilters.= " AND cus.status='1' ";
	}
	
	if($DateRangeFilter == "1day") {
		$QuickDateRange = date('Y-m-d', strtotime($TodayDate. ' - 1 days'));
		$TodayDateSearch = date('Y-m-d', strtotime($TodayDate. ' + 1 days'));
		$FromDateFilter = 0;
		$UntilDateFilter = 0;
		$CustomerFilters.= " AND cus.createtime >= '$QuickDateRange' AND cus.createtime <= '$TodayDateSearch' ";
	} elseif($DateRangeFilter == "7day") {
		$QuickDateRange = date('Y-m-d', strtotime($TodayDate. ' - 7 days'));
		$TodayDateSearch = date('Y-m-d', strtotime($TodayDate. ' + 1 days'));
		$FromDateFilter = 0;
		$UntilDateFilter = 0;
		$CustomerFilters.= " AND cus.createtime >= '$QuickDateRange' AND cus.createtime <= '$TodayDateSearch' ";
	} elseif($DateRangeFilter == "30day") {
		$QuickDateRange = date('Y-m-d', strtotime($TodayDate. ' - 30 days'));
		$TodayDateSearch = date('Y-m-d', strtotime($TodayDate. ' + 1 days'));
		$FromDateFilter = 0;
		$UntilDateFilter = 0;
		$CustomerFilters.= " AND cus.createtime >= '$QuickDateRange' AND cus.createtime <= '$TodayDateSearch' ";
	}

	if($FromDateFilter != 0) {
		$CustomerFilters.= " AND cus.createtime >= '$FromDateFilter' ";
	}
	
	if($UntilDateFilter != 0) {
		$CustomerFilters.= " AND cus.createtime <= '$UntilDateFilter' ";
	}
	
	
	if($cStatus == 1 AND $DateRangeFilter == 0 AND $FromDateFilter == 0 AND $UntilDateFilter == 0) {
		$CustomerFilters = " AND cus.status='1' AND cus.createtime >= '$TodayDate' ";
	}

	// getting total number records without any search
	$sql = "SELECT con.id,cus.contactid,con.title,con.fname,con.mname,con.lname,cus.tourid,gro.tourdesc,gro.tourname, us.UserID,CONCAT(DATE_FORMAT(cus.createtime, '%m/%d/%Y - '),TIME_FORMAT(cus.createtime, '%h:%i %p')) AS createtime, us.fname, us.lname, CONCAT(DATE_FORMAT(cr.Cancelled_On, '%m/%d/%Y - '),TIME_FORMAT(cr.Cancelled_On, '%h:%i %p')) AS Cancel_Date
FROM customer_account cus JOIN contacts con ON cus.contactid=con.id JOIN users us ON us.UserID=cus.Added_By 
JOIN groups gro ON gro.tourid=cus.tourid 
JOIN customer_invoice ci ON ci.Group_ID =cus.tourid  AND ci.Customer_Account_Customer_ID = con.id
LEFT JOIN customer_refund cr ON cr.Customer_Account_Customer_ID=cus.contactid WHERE 1 ";
	//$sql = "SELECT id,title,fname FROM contacts ";
	$sqlTot .= $sql.$CustomerFilters;
	$sqlRec .= $sql.$CustomerFilters;
	//concatenate search sql if value exist
	if(isset($where) && $where != '') {

		$sqlTot .= $where.$CustomerFilters;
		$sqlRec .= $where.$CustomerFilters;
	}


 	//$sqlRec .=  " ORDER BY ". $columns[$params['order'][0]['column']]."   ".$params['order'][0]['dir']." LIMIT ".$params['start']." ,".$params['length']." ";
 	$sqlRec .=  " ORDER BY cus.updatetime DESC LIMIT ".$params['start']." ,".$params['length']." ";

	$queryTot = mysqli_query($db, $sqlTot) or die("database error:". mysqli_error($db));


	$totalRecords = mysqli_num_rows($queryTot);

	$queryRecords = mysqli_query($db, $sqlRec) or die("error to fetch employees data");

	//iterate on results row and create new index array of data
	while( $row = mysqli_fetch_row($queryRecords) ) { 
		$data[] = $row;
	}	

	$json_data = array(
			"draw"            => intval( $params['draw'] ),   
			"recordsTotal"    => intval( $totalRecords ),  
			"recordsFiltered" => intval($totalRecords),
			"data"            => $data   // total data array
			);

	echo json_encode($json_data);  // send data as json format
?>
