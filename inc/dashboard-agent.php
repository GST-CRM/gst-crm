<?php

include_once __DIR__ . '/../models/groups.php' ;
include_once __DIR__ . '/../models/agents.php' ;
include_once __DIR__ . '/../models/group_agents.php' ;
include_once __DIR__ . '/../models/contacts.php' ;
include_once __DIR__ . '/../models/customer_payment.php' ;
//Find Logged in contactid
$agentId = AclPermission::userId() ;

//get agent tours
$agentObj = new Agents() ;
$groupAgentObj = new GroupAgents() ;
$agentGroups = $agentObj->groups($agentId) ;

$agentInfoMain = $agentObj->get(['contactid' => $agentId]) ;
$agentInfo = $groupAgentObj->getList(['agent_id' => $agentId]) ;

//group id to array
$groups = []; 
foreach( $agentGroups as $g ) {
    $groups[] = $g['tourid'] ;
}
$groupIn = GUtils::implodeIfValue($groups) ;


//Customer details
$customerSummary = (new Groups())->getMemberSummary($groupIn) ;
$agentCommission = 0 ;

$whereCondition = " AND ca.tourid IN($groupIn) " ;
$paymentSummary = (new CustomerPayment())->paymentSummary('') ;
//Output
$numTours = count($agentGroups) ;
$cancelled = $customerSummary['cancelled'] ;
$customers = $customerSummary['customers'] ;

?>
<div class="row">
    <div class="col-md-2">
        <div class="card stat-rev-card" >
            <div class="card-block gst-card-block-small">
                <a href="groups.php">
                    <div class="rev-icon bg-c-yellow"><i class="fas fa-user text-white"></i><span class="ring-sm"></span><span class="ring-lg"></span></div>
                    <h2 class="text-c-yellow" style="font-size: 20px;"><?php echo $numTours;?></h2>
                    <p class="m-b-0">Groups</p>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="card stat-rev-card">
            <div class="card-block gst-card-block-small">
                <a href="customers.php">
                    <div class="rev-icon bg-c-blue"><i class="fas fa-user text-white"></i><span class="ring-sm"></span><span class="ring-lg"></span></div>
                    <h2 class="text-c-blue" style="font-size: 20px;"><?php echo "$customers/$cancelled";?></h2>
                    <p class="m-b-0">Customers/Cancellations</p>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="card stat-rev-card">
            <div class="card-block gst-card-block-small">
                <a href="sales-invoices.php?filter=paid">
                    <div class="rev-icon bg-c-green"><i class="fas fa-money-bill-alt text-white"></i><span class="ring-sm"></span><span class="ring-lg"></span></div>
                    <h2 class="text-c-green" style="font-size: 20px;"><?php echo GUtils::formatMoney( $paymentSummary['summary']['paid']) ;?></h2>
                    <p class="m-b-0">Total Paid</p>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="card stat-rev-card">
            <div class="card-block gst-card-block-small">
                <a href="sales-invoices.php?filter=total_due">
                    <div class="rev-icon bg-c-yellow"><i class="fas fa-user text-white"></i><span class="ring-sm"></span><span class="ring-lg"></span></div>
                    <h2 class="text-c-yellow" style="font-size: 20px;"><?php echo GUtils::formatMoney( $paymentSummary['summary']['unpaid']) ;?></h2>
                    <p class="m-b-0">Total Unpaid</p>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="card stat-rev-card">
            <div class="card-block gst-card-block-small">
                <a href="sales-invoices.php?filter=over_due">
                    <div class="rev-icon bg-c-red"><i class="fas fa-shopping-cart text-white"></i><span class="ring-sm"></span><span class="ring-lg"></span></div>
                    <h2 class="text-c-red" style="font-size: 20px;"><?php echo GUtils::formatMoney( $paymentSummary['summary']['over_due']) ;?></h2>
                    <p class="m-b-0">Total Past Due</p>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="card stat-rev-card">
            <div class="card-block gst-card-block-small">
                <a href="payments.php">
                    <div class="rev-icon bg-c-green"><i class="fas fa-money-bill-alt text-white"></i><span class="ring-sm"></span><span class="ring-lg"></span></div>
                    <h2 id="idAgentCommission" class="text-c-green" style="font-size: 20px;"><?php echo GUtils::formatMoney($agentCommission);?></h2>
                    <p class="m-b-0">Total Commission</p>
                </a>
            </div>
        </div>
    </div>
</div>

<?php
$contactObj = new Contacts() ;
$groupObj = new Groups() ;
$paymentObj = new CustomerPayment() ;
$leaderIds = [] ;

foreach( $agentGroups as $one ) {
    $leaderIds = isset($one['groupleader']) ? $one['groupleader'] : 0 ;
    $contacts = $contactObj->in($leaderIds) ;
    
    //Tour leaders
    $leaders = GUtils::implodeColumn($contacts, ['fname', 'mname', 'lname']) ;
    $customerSummary = (new Groups())->getMemberSummary( $one['tourid']) ;
    $numParticipants = $customerSummary['customers'] ;
    $cancelled = $customerSummary['cancelled'] ;
    $activeParticipants = $numParticipants - $cancelled ;

    $tripCommission = 0 ;
    
    if( $agentInfoMain['incometype'] != 'Salary' /* or == Commission*/ ) {
            foreach( $agentInfo as $ai ) {
            if( $one['tourid'] == $ai['group_id'] ) {
                if( $ai['commissiontype'] == 'Per Person' ) {
                    $tripCommission = floatval($ai['commissionvalue']) * intval($activeParticipants) ;
                    $agentCommission += $tripCommission ;
                }
                else if( $agentInfo['commissiontype'] == 'Per Trip' ) {
                    $tripCommission = floatval($ai['commissionvalue']) ;
                    $agentCommission += $tripCommission ;
                }
                break ;
            }
            }
    }

    //Paid unpaid summary
    $whereCondition = ' AND ca.tourid =' . $one['tourid'] ;
    $singleSummary = $paymentObj->paymentSummary($whereCondition) ;
    
    //Find Trip Cost
    $packagePrice = $groupObj->tripPrice($one['tourid']) ;
    
    $statusClass = "bg-c-blue text-white" ;
    if( $one['InProgress'] ) {
        $statusClass = "bg-c-darkblue text-white" ;
    }
    ?>
<div class="card table-card">
	<div class="card-header pb-3">
		<h5><?php echo $one['tourname'];?> | <small>(From <?php echo GUtils::clientDate($one['startdate']); ?> to <?php echo GUtils::clientDate($one['enddate']); ?>)</small></h5>
	</div>
	<div class="card-body p-0">
        <table class="table text-center bg-white gst-three-colum-pair">
            <tbody>
                <tr class="<?php echo $statusClass;?>">
                    <td class="pt-2 pb-0">
						<span>Trip Destination</span>
                    </td>
					<td class="pt-2 pb-0">
						<label class="text-c-blue box-highlight">
							<?php echo $one['destination'];?>
						</label>
					</td>
                    <td class="pt-2 pb-0">
						<span>Participants</span>
                    </td>
					<td class="pt-2 pb-0">
						<label class="text-c-blue box-highlight">
							<?php echo $activeParticipants; ?>
						</label>
                    </td>
                    <td class="pt-2 pb-0 text-right">
						<span>Commission/pax</span>
                    </td>
                    <td class="pt-2 pb-0">
						<label class="text-c-blue box-highlight">
							<?php echo GUtils::formatMoney($ai['commissionvalue']); ?>
						</label>
                    </td>
                </tr>
                <tr>
					<td class="pt-2 pb-0">
						<span>Tour Leader</span>
					</td>
					<td class="pt-2 pb-0">
                        <label class="text-c-blue">
                            <?php echo $leaders ;?>
                        </label>
					</td>
					<td class="pt-2 pb-0">
                        <span>Package Price</span>
					</td>
					<td class="pt-2 pb-0">
                        <label class="text-c-blue ">
                            <?php echo GUtils::formatMoney($packagePrice); ?>
                        </label>
					</td>
					<td class="pt-2 pb-0">
                        <span>Trip Cancel Date</span>
					</td>
					<td class="pt-2 pb-0">
                        <label class="text-c-blue "><?php echo GUtils::clientDate($one['Cancel_Date']);?></label>
					</td>
                </tr>
                <tr>
                    <td class="pt-2 pb-0">
                        <span>Commission Amount</span>
					</td>
					<td class="pt-2 pb-0">
                        <label class="text-c-blue">
                            <?php echo $activeParticipants . ' (' . GUtils::formatMoney($tripCommission) . ')' ; ?>
                        </label>
					</td>
					<td class="pt-2 pb-0">
                        <span>Amount Paid</span>
					</td>
					<td class="pt-2 pb-0">
                        <label class="text-c-blue "><?php echo GUtils::formatMoney( $singleSummary['summary']['paid'] ); ?></label>
					</td>
					<td class="pt-2 pb-0">
                        <span>Amount Unpaid</span>
					</td>
					<td class="pt-2 pb-0">
                        <label class="text-c-blue "><?php echo GUtils::formatMoney( $singleSummary['summary']['unpaid'] ); ?></label>
					</td>
				</tr>
			</tbody>    
		</table>
	</div>
</div>
<?php } ?>
<script type="text/javascript">
document.getElementById('idAgentCommission').innerHTML = '<?php echo GUtils::formatMoney( $agentCommission );?>' ;
</script>
