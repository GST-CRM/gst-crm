<?php
//fetch.php
include "../session.php";

$connect = new mysqli($servername, $username, $password, $dbname);
$columns = array('first_name', 'last_name');

if(isset($_GET['status'])) {
    $Payee_Status = $_GET['status'];
} else {
    $Payee_Status = "";
}

if($Payee_Status == "voided") {
	$Payee_Where = "Status=0";
} else {
	$Payee_Where = "Status=1";
}

$query = "SELECT * FROM expenses_payee WHERE $Payee_Where ";

if(isset($_POST["search"]["value"])) {
	$query .= 'AND Payee_Name LIKE "%'.$_POST["search"]["value"].'%" ';
}
$query .= 'ORDER BY Payee_ID DESC ';

$query1 = '';

if($_POST["length"] != -1) {
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = mysqli_num_rows(mysqli_query($connect, $query));

$result = mysqli_query($connect, $query . $query1);

$data = array();

while($row = mysqli_fetch_array($result)) {
	//To put all the actions options for each payee in a PHP variable
	$ActionsList = '<div class="btn-group">';
	if($Payee_Status == "voided") {
	   $ActionsList .= '<a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick = \'deleteConfirm( "expenses-payee.php?action=unvoid&payee_id='.$row["Payee_ID"].'", "Unvoid")\' role="button">Unvoid</a>';
	} else {
	   $ActionsList .= '<a class="btn btn-primary btn-sm" href="javascript:void(0);" role="button" id="dropdownMenuLink">Actions</a>
		<button type="button" class="btn btn-sm btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<span class="sr-only">Toggle Dropdown</span>
		</button>
		<div class="dropdown-menu">
			<a class="dropdown-item" href="javascript:void(0);" onclick = \'deleteConfirm( "expenses-payee.php?action=delete&payee_id='.$row["Payee_ID"].'", "Delete")\'>Delete</a>
			<a class="dropdown-item" href="javascript:void(0);" onclick = \'deleteConfirm( "expenses-payee.php?action=void&payee_id='.$row["Payee_ID"].'", "Void")\'>Void</a>
		</div>';
	}
	$ActionsList .='</div>';
	
	//To show each TD for the Payee Table in array
	$sub_array = array();
	$sub_array[] = 'P' . $row["Payee_ID"];
	$sub_array[] = '<div class="input-group mb-0"><input type="text" class="update form-control PayeeNameField" data-id="'.$row["Payee_ID"].'" data-column="Payee_Name" value="' . $row["Payee_Name"] . '"><div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="far fa-edit"></i></span></div></div>';
	$sub_array[] = $row["Add_Date"];
	$sub_array[] = $row["Mod_Date"];
	$sub_array[] = $ActionsList;
	$data[] = $sub_array;
}

function get_all_data($connect) {
 $query = "SELECT * FROM expenses_payee";
 $result = mysqli_query($connect, $query);
 return mysqli_num_rows($result);
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($connect),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

?>