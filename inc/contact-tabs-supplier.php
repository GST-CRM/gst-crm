<?php if( AclPermission::actionAllowed('ContactSupplierInfo') ) { ?>
<form name="contact2" action="inc/contact-functions.php" method="POST" id="contact2">
	<div class="row">
		<div class="form-group col-sm-12 m-0">
			<h4 class="sub-title">Supplier Information</h4>
		</div>
		<div class="form-default form-static-label col-sm-3">
			<label class="float-label">Status</label>
			<div class="can-toggle">
			  <input type="checkbox" id="s" name="supplierstatus" value="1" <?php if($supplierdata["status"]==1) echo "checked"; ?>>
			  <label for="s">
				<div class="can-toggle__switch" data-checked="Active" data-unchecked="Disabled"></div>
			  </label>
			</div>
		</div>
		<div class="form-default form-static-label col-sm-3">
			<label class="float-label">Supplier #</label>
			<input type="text" name="suppliercode" class="form-control" value="<?php echo $supplierdata["contactid"]; ?>" readonly>
			<input type="text" name="contactidz" class="form-control" value="<?php echo $supplierdata["contactid"]; ?>" hidden>
			<input type="text" name="fullname" class="form-control" value="<?php echo $data['fname']." ".$data['mname']." ".$data['lname']; ?>" hidden>
		</div>
		<div id="suppliertype" class="form-default form-static-label col-sm-3">
			<label class="float-label">Supplier Type</label>
			<select name="suppliertype" class="form-control" onchange="SupplierTypeSelect(this);">
					<option id="0" <?php if($supplierdata["type"]==0) { echo "selected"; } ?>>N/A</option>
					<?php $meta2 = new mysqli($servername, $username, $password, $dbname);
					$metadata2 = mysqli_query($meta2,"SELECT * FROM metadata WHERE catid='2'");$metaselected ="";
					while($keyword2 = mysqli_fetch_array($metadata2))
					{
						if($supplierdata["type"] == $keyword2['meta']) {$metaselected="selected";} else {$metaselected="";}
						echo "<option value='".$keyword2['meta']."' ".$metaselected.">".$keyword2['meta']."</option>";
					} ?>
					<option id="admOption" value="0">Add New</option>
			</select>
		</div>
		<div id="suppliertypenew" class="form-default form-static-label col-sm-3" style="display:none;">
			<label class="float-label">New Supplier Type</label>
			<input type="text" name="newsuppliertype" class="form-control">
		</div>
		<div id="paymentterms" class="form-default form-static-label col-sm-3" style="display:block;">
			<label class="float-label">Payment Terms</label>
				<select name="paymentterms" class="form-control" onchange="SupPaymentTypeSelect(this);">
						<option value="0" <?php if($supplierdata["payment_terms"]==0) { echo "selected"; } ?>>N/A</option>
						<?php $meta3 = new mysqli($servername, $username, $password, $dbname);
						$metadata3 = mysqli_query($meta3,"SELECT * FROM metadata WHERE catid='3'");$metaselected ="";
						while($keyword3 = mysqli_fetch_array($metadata3))
						{
							if($supplierdata["payment_terms"] == $keyword3['meta']) {$metaselected="selected";} else {$metaselected="";}
							echo "<option value='".$keyword3['meta']."' ".$metaselected.">".$keyword3['meta']."</option>";
						} ?>
						<option id="admOption" value="0">Add New</option>
				</select>
		</div>
		<div id="paymenttermsnew" class="form-default form-static-label col-sm-3" style="display:none;">
			<label class="float-label">New Payment Term</label>
			<input type="text" name="newpaymentterms" class="form-control">
		</div>
		<div class="form-group form-default form-static-label col-sm-6">
			<label class="float-label">Description</label>
			<input type="text" name="supplierdesc" value="<?php echo $supplierdata["description"]; ?>" class="form-control">
		</div>
		<div class="form-group form-default form-static-label col-sm-2">
			<label class="float-label">Account Number</label>
			<input type="text" name="accountnumber" value="<?php echo $supplierdata["Account_number"]; ?>" class="form-control">
		</div>
		<div class="form-group form-default form-static-label col-sm-2">
			<label class="float-label">Bank Account #</label>
			<input type="text" name="bankaccount" value="<?php echo $supplierdata["Bank_Account_number"]; ?>" class="form-control">
		</div>
		<div class="form-group form-default form-static-label col-sm-2">
			<label class="float-label">Bank Routing #</label>
			<input type="text" name="bankrouting" value="<?php echo $supplierdata["Bank_Routing_number"]; ?>" class="form-control">
		</div>
        
        <div class="form-group col-sm-3">
			<label class="float-label">Enable Login</label>
			<div class="can-toggle">
				<input type="checkbox" class="supplier-active-disable-switch-checkbox" id="supplier-enable" name="supplierlogin" value="1" <?php if($supplierdata["login_active"]==1) echo"checked"; ?> >
				<label for="supplier-enable">
					<div class="can-toggle__switch" data-checked="Active" data-unchecked="Disabled"></div>
				</label>
			</div>
		</div>
        
		<div class="form-group form-default form-static-label col-sm-12">
			<input type="submit" name="updatesupplier" value="Update" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal">
		</div>
	</div>
</form>
<hr>

<?php } ?>

<div class="form-group row">
	<div class="col-sm-9">
		<h5>Supplier's Products Assosciated to the Groups</h5>
		<br />
	</div>
    <?php if( AclPermission::actionAllowed('ContactSupplierAddPayment') ) { ?>
	<div class="col-sm-3">
		<a href="suppliers-bill-payments-add.php?supplier=<?php echo $leadid; ?>" class="btn btn-mat waves-effect waves-light btn-primary float-right" style="margin-right: 5px; padding: 3px 13px;">Add Payment</a>
	</div>
    <?php } ?>
	<div class="col-sm-12">
		<table class="table table-hover table-striped">
			<thead>
				<th>Group Code</th>
				<th>Group Name</th>
				<th class="text-center">Balance</th>
				<th width="70" class="text-center">Pax</th>
			</thead>
			
			<tbody>
<?php 
//We create this 2 arrays to collect the following TWO queries results
$ToursListLandSupplierArray = array();
$ToursListAirSupplierArray = array();


//#1 This query to select all the groups for this supplier that has Land product
$LandProductsForSupplierSQL = mysqli_query($db,"SELECT gro.tourid,gro.startdate FROM groups gro
JOIN products pro 
ON gro.land_productid=pro.id
WHERE gro.status=1 AND pro.supplierid=$leadid
ORDER BY gro.startdate ASC");
$xxx1 = 0;
while($LandProductsList = mysqli_fetch_array($LandProductsForSupplierSQL)){
	$ToursListLandSupplierArray[$xxx1][1] = $LandProductsList['tourid']." ";
	$ToursListLandSupplierArray[$xxx1][2] = $LandProductsList['startdate']." ";
	$xxx1++;
}

//#2 This query to select all the groups for this supplier that has Air product
$AirProductsForSupplierSQL = mysqli_query($db,"SELECT gro.tourid,gro.startdate FROM groups gro
JOIN products pro 
ON gro.airline_productid=pro.id
WHERE gro.status=1 AND pro.supplierid=$leadid
ORDER BY gro.startdate ASC");
$xxx2 = 0;
while($AirProductsList = mysqli_fetch_array($AirProductsForSupplierSQL)){
	$ToursListAirSupplierArray[$xxx2][1] = $AirProductsList['tourid']." ";
	$ToursListAirSupplierArray[$xxx2][2] = $AirProductsList['startdate']." ";
	$xxx2++;
}

//This to delete any duplicated group in the Array
//$FilteredToursList = array_unique (array_merge ($ToursListLandSupplierArray, $ToursListAirSupplierArray));
$FilteredToursMerged = array_merge ($ToursListLandSupplierArray, $ToursListAirSupplierArray);
$FilteredToursList = array_map("unserialize", array_unique(array_map("serialize", $FilteredToursMerged)));
$FilteredToursSelectedCount = count($FilteredToursList);

//To calculate the total expenses for all the groups for this supplier
$TotalExpensesAllGroups = 0;
$TotalPaidAllGroups = 0;
$TR_Group_Row = NULL;
$TR_Group_Row_Paid = NULL;
$TR_Group_Row_Paid_More = NULL;
$TR_Group_Row_NotPaid = NULL;
$GroupsPaidCounter = 0;
$GroupsPaidMoreThanNeededCounter = 0;
$GroupsNotPaidCounter = 0;

foreach($FilteredToursList as $FilterOneTour){
	//To collect the data of the specific page
	$FilteredTourSQL = "SELECT * FROM groups WHERE tourid=$FilterOneTour[1]";
	$FilteredTourResult = $db->query($FilteredTourSQL);
	$FilteredTourData = $FilteredTourResult->fetch_assoc(); 
	$LandProductTotal = 0;
	$LandUpgradeTotal = 0;
	$AirlineProductTotal = 0;
	$AirlineUpgradeTotal = 0;
	
	//Get the Purchase Order Num and Supplier Bill Num for Each Group
	$GetPOandSBnumSQL = "SELECT po.Purchase_Order_Num,sb.Supplier_Bill_Num FROM purchase_order po
						LEFT JOIN suppliers_bill sb
						ON sb.Purchase_Order_Num=po.Purchase_Order_Num
						WHERE po.Supplier_ID=$leadid AND po.Group_ID=$FilterOneTour[1]";
	$getNUMSresult = $db->query($GetPOandSBnumSQL);
	$GetNUMSdata = $getNUMSresult->fetch_assoc();
	$PurchaseOrderNum = $GetNUMSdata['Purchase_Order_Num'];
	$SupplierBillNum = $GetNUMSdata['Supplier_Bill_Num'];
	
	//Check if the PO Num and SB Num are empty or filled up
	if($PurchaseOrderNum > 0) {
		//$EmptyPOLinessql = "DELETE FROM purchase_order_line WHERE Purchase_Order_Num=$PurchaseOrderNum" ;
		//GDb::execute($EmptyPOLinessql);
	} else {
		//Update the supplier bill for this record
		$TheGroupIDz = $FilterOneTour[1];
		$POcreateSQL = "INSERT INTO purchase_order (Group_ID,Supplier_ID,Purchase_Order_Date) VALUES ('$TheGroupIDz','$leadid',NOW())" ;
		GDb::execute($POcreateSQL);
		$PurchaseOrderNum = GDb::db()->insert_id;
	}
	if($SupplierBillNum > 0) {
		$EmptySBLinessql = "DELETE FROM suppliers_bill_line WHERE Supplier_Bill_Num=$SupplierBillNum" ;
		GDb::execute($EmptySBLinessql);
	} else {
		//Update the supplier bill for this record
		$TheGroupIDz = $FilterOneTour[1];
		$SBnumSQL = "INSERT INTO suppliers_bill (Supplier_Bill_Date,Purchase_Order_Num,Supplier_ID) VALUES (NOW(),'$PurchaseOrderNum','$leadid')" ;
		GDb::execute($SBnumSQL);
		$SupplierBillNum = GDb::db()->insert_id;
	}
	

				$TR_Group_Row .="<tr data-toggle=\"collapse\" data-target=\"#accordion".$FilteredTourData['tourid']."\" class=\"clickable\">
					<td style=\"padding: 10px .75rem;\"><i class=\"fas fa-arrow-circle-down\"></i>".$FilteredTourData['tourid']."</td>
					<td style=\"padding: 10px .75rem;\">
						<a href=\"groups-edit.php?id=".$FilteredTourData['tourid']."&action=edit\">
						<span style=\"font-weight:600;\">".$FilteredTourData['tourname']."</span>
						<br /><small>(".date('m/d/Y', strtotime($FilteredTourData['startdate']))." - ".date('m/d/Y', strtotime($FilteredTourData['enddate'])).")</small>
						</a>
					</td>
					<td id=\"StyleGroup".$FilteredTourData['tourid']."\" class=\"text-center\" style=\"padding: 10px .75rem;\">
					asd</td>
					<td style=\"padding: 10px .75rem;\"><a href=\"groups-edit.php?id=".$FilteredTourData['tourid']."&action=edit&tab=participants\">";
					//To collect the number of PAX for this group
					$PaxCountWithTicketSQL = "SELECT * FROM customer_account WHERE tourid=".$FilterOneTour[1]." AND status=1 AND hisownticket=0";
					$PaxCountWithTicketResult = $db->query($PaxCountWithTicketSQL);
					$WithTicketTotalCount = $PaxCountWithTicketResult->num_rows; 
					
					$WithoutTicketCountSql = "SELECT * FROM customer_account WHERE tourid=".$FilterOneTour[1]." AND status=1 AND hisownticket=1";
					$WithoutTicketCountResult = $db->query($WithoutTicketCountSql);
					$WithoutTicketTotalCount = $WithoutTicketCountResult->num_rows; 
					
					$TotalPaxTour = $WithTicketTotalCount + $WithoutTicketTotalCount;
					$TR_Group_Row .=$TotalPaxTour." <small>PAX</small></a>
					</td>
				</tr>
				<tr style=\"padding-bottom:0px;\">
					<td colspan=\"4\" style=\"padding:0px;\">
						<div id=\"accordion".$FilteredTourData['tourid']."\" class=\"collapse\" style=\"background-color:#3f4d67;\">
							<div style=\"padding:20px;\">
							<table class=\"table table-bordered table-hover table-striped mb-0\">
								<thead class=\"thead-dark\">
									<tr>
										<th class=\"pt-1 pb-1\">Product Name</th>
										<th class=\"pt-1 pb-1\">Cost</th>
										<th class=\"pt-1 pb-1\">Qty</th>
										<th class=\"pt-1 pb-1\">Total</th>
									</tr>
								</thead>
								<tbody>";
							//To collect the data of the land product
							$LandProductID = $FilteredTourData['land_productid'];
							$LandProductDataSQL = "SELECT * FROM products WHERE id=$LandProductID AND status=1 AND supplierid=$leadid";
							$LandProductDataResult = $db->query($LandProductDataSQL);
							$LandProductCount = $LandProductDataResult->num_rows;
							if($LandProductCount > 0) {
								$LandProductData = $LandProductDataResult->fetch_assoc();
								$LandProductID = $LandProductData['id'];
								$LandProductName = $LandProductData['name'];
								$LandProductCost = $LandProductData['cost'];
									$TR_Group_Row .="<tr>
										<td>
											<a href=\"products-edit.php?id=".$LandProductID."&action=edit\" style=\"font-weight: bold;\">
											Land Product<br />
											<small>".$LandProductName."</small>
											</a>
										</td>
										<td>".GUtils::formatMoney($LandProductCost)."</td>
										<td>".$TotalPaxTour."</td>
										<td>";
										$LandProductTotal = ($LandProductCost*$TotalPaxTour);
										$TR_Group_Row .=GUtils::formatMoney($LandProductTotal)."</td>
									</tr>";
									
								//Update the supplier bill for this record
								$SQLquery1 = "INSERT INTO suppliers_bill_line(Supplier_Bill_Num, Supplier_Bill_Line, Line_Type, Product_Product_ID, Product_Name, Qty, Bill_Line_Total)
								VALUES ('$SupplierBillNum','1','Land','$LandProductID','$LandProductName','$TotalPaxTour','$LandProductCost')" ;
								GDb::execute($SQLquery1);
							}
							//To collect the data of the Single Supplement product
							$SingleSupplementID = $LandProductData['Product_Reference'];
							$SingleProductDataSQL = "SELECT * FROM products WHERE id=$SingleSupplementID AND status=1 AND supplierid=$leadid";
							$SingleProductDataResult = $db->query($SingleProductDataSQL);
							$SingleProductCount = $SingleProductDataResult->num_rows;
							if($SingleProductCount > 0) {
								$SingleProductData = $SingleProductDataResult->fetch_assoc();
								$SingleProductID = $SingleProductData['id'];
								$SingleProductCost = $SingleProductData['cost'];
									$TR_Group_Row .="<tr>
										<td>
											<a href=\"products-edit.php?id=".$SingleProductID."&action=edit\" style=\"font-weight: bold;\">
											Single Supplement<br />
											<small>".$LandProductData['name']."</small>
											</a>
										</td>
										<td>".GUtils::formatMoney($SingleProductCost)."</td>
										<td>";
										
											$SingleCustomerSQL = "SELECT * FROM customer_account WHERE tourid=$FilterOneTour[1] AND status=1 AND roomtype='Single'";
											$SingleCustomerResult = $db->query($SingleCustomerSQL);
											$SingleCustomerCount = $SingleCustomerResult->num_rows; 

											$TR_Group_Row .=$SingleCustomerCount;
										
										$TR_Group_Row .="</td>
										<td>";
										$SingleProductTotal = ($SingleProductCost*$SingleCustomerCount);
										$TR_Group_Row .=GUtils::formatMoney($SingleProductTotal)."</td>
									</tr>";
								//Update the supplier bill for this record
								$SQLquery2 = "INSERT INTO suppliers_bill_line(Supplier_Bill_Num, Supplier_Bill_Line, Line_Type, Product_Product_ID, Product_Name, Qty, Bill_Line_Total)
								VALUES ('$SupplierBillNum','3','Single','$SingleProductID','Single Supplement','$SingleCustomerCount','$SingleProductCost')" ;
								GDb::execute($SQLquery2);
							}
							//To collect the data of the land upgrade product
							$LandUpgradeDataSQL = "SELECT COUNT(pro.id) AS Quantity,pro.id,pro.name,pro.supplierid,pro.cost FROM customer_account ca
							JOIN products pro 
							ON ca.upgrade_land_product=pro.id
							WHERE ca.tourid = $FilterOneTour[1] AND ca.upgrade_land_product > 0 AND ca.status=1 AND pro.supplierid=$leadid
							GROUP BY pro.id";
							$LandUpgradeDataResult = $db->query($LandUpgradeDataSQL);
							$LandUpgradeCount = $LandUpgradeDataResult->num_rows;
							$LandUpgradeX = 1;
							$LandUpgradeXquery = 10;
							if($LandUpgradeCount > 0) {
								while($LandUpgradeData = $LandUpgradeDataResult->fetch_assoc()) {
									$LandUpgradeID = $LandUpgradeData['id'];
									$LandUpgradeName = $LandUpgradeData['name'];
									$LandUpgradeCost = $LandUpgradeData['cost'];
									$LandUpgradeQty = $LandUpgradeData['Quantity'];
									
									$TR_Group_Row .="<tr>
										<td>
											<a href=\"products-edit.php?id=".$LandUpgradeID."&action=edit\" style=\"font-weight: bold;\">
											Land Upgrade<br />
											<small>".$LandUpgradeName."</small>
											</a>
										</td>
										<td>".GUtils::formatMoney($LandUpgradeCost)."</td>
										<td>".$LandUpgradeData['Quantity']."</td>
										<td>";
											$LandUpgradeSubTotal[$LandUpgradeX] = ($LandUpgradeCost*$LandUpgradeQty);
											$TR_Group_Row .=GUtils::formatMoney($LandUpgradeSubTotal[$LandUpgradeX]);
											$LandUpgradeTotal = $LandUpgradeTotal+$LandUpgradeSubTotal[$LandUpgradeX];
											$LandUpgradeX++;
											$LandUpgradeXquery++;
										$TR_Group_Row .="</td>
									</tr>";
								//Update the supplier bill for this record
								$SQLquery3 = "INSERT INTO suppliers_bill_line(Supplier_Bill_Num, Supplier_Bill_Line, Line_Type, Product_Product_ID, Product_Name, Qty, Bill_Line_Total)
								VALUES ('$SupplierBillNum','$LandUpgradeXquery','Land Upgrade','$LandUpgradeID','$LandUpgradeName','$LandUpgradeQty','$LandUpgradeCost')" ;
								GDb::execute($SQLquery3);
								}
							}
							//To collect the data of the airline product
							$AirProductID = $FilteredTourData['airline_productid'];
							$AirProductDataSQL = "SELECT * FROM products 
							WHERE id=$AirProductID AND status=1 AND supplierid=$leadid";
							$AirProductDataResult = $db->query($AirProductDataSQL);
							$AirProductCount = $AirProductDataResult->num_rows;
							if($AirProductCount > 0) {
								$AirProductData = $AirProductDataResult->fetch_assoc();
								$AirProductID = $AirProductData['id'];
								$AirProductName = $AirProductData['name'];
								$AirProductCost = $AirProductData['cost'];
									$TR_Group_Row .="<tr>
										<td>
											<a href=\"products-edit.php?id=".$AirProductID."&action=edit\" style=\"font-weight: bold;\">
											Airline Product<br />
											<small>".$AirProductName."</small>
											</a>
										</td>
										<td>".GUtils::formatMoney($AirProductCost)."</td>
										<td>".$WithTicketTotalCount."</td>
										<td>";
										$AirlineProductTotal = ($AirProductCost*$WithTicketTotalCount);
										$TR_Group_Row .=GUtils::formatMoney($AirlineProductTotal)."</td>
									</tr>";
								//Update the supplier bill for this record
								$SQLquery4 = "INSERT INTO suppliers_bill_line(Supplier_Bill_Num, Supplier_Bill_Line, Line_Type, Product_Product_ID, Product_Name, Qty, Bill_Line_Total)
								VALUES ('$SupplierBillNum','2','Airline','$AirProductID','$AirProductName','$WithTicketTotalCount','$AirProductCost')" ;
								GDb::execute($SQLquery4);
							}
							//To collect the data of the Airline upgrade product
							$AirUpgradeDataSQL = "SELECT COUNT(pro.id) AS Quantity,pro.id,pro.name,pro.supplierid,pro.cost FROM customer_account ca
							JOIN products pro 
							ON ca.upgradeproduct=pro.id
							WHERE ca.tourid=$FilterOneTour[1] AND ca.upgradeproduct > 0 AND ca.status=1 AND pro.supplierid=$leadid
							GROUP BY pro.id";
							$AirUpgradeDataResult = $db->query($AirUpgradeDataSQL);
							$AirUpgradeCount = $AirUpgradeDataResult->num_rows;
							$AirUpgradeX = 1;
							$AirUpgradeXquery = 20;
							if($AirUpgradeCount > 0) {
								while($AirUpgradeData = $AirUpgradeDataResult->fetch_assoc()) {
									$AirUpgradeID = $AirUpgradeData['id'];
									$AirUpgradeName = $AirUpgradeData['name'];
									$AirUpgradeCost = $AirUpgradeData['cost'];
									$AirUpgradeQty = $AirUpgradeData['Quantity'];
									$TR_Group_Row .="<tr>
										<td>
											<a href=\"products-edit.php?id=".$AirUpgradeID."&action=edit\" style=\"font-weight: bold;\">
											Airline Upgrade<br />
											<small>".$AirUpgradeName."</small>
											</a>
										</td>
										<td>".GUtils::formatMoney($AirUpgradeCost)."</td>
										<td>".$AirUpgradeQty."</td>
										<td>";
											$AirlineUpgradeSubTotal[$AirUpgradeX] = ($AirUpgradeCost*$AirUpgradeQty);
											$TR_Group_Row .=GUtils::formatMoney($AirlineUpgradeSubTotal[$AirUpgradeX]);
											$AirlineUpgradeTotal = $AirlineUpgradeTotal+$AirlineUpgradeSubTotal[$AirUpgradeX];
											$AirUpgradeX++;
											$AirUpgradeXquery++;
										$TR_Group_Row .="</td>
									</tr>";
								//Update the supplier bill for this record
								$SQLquery5 = "INSERT INTO suppliers_bill_line(Supplier_Bill_Num, Supplier_Bill_Line, Line_Type, Product_Product_ID, Product_Name, Qty, Bill_Line_Total)
								VALUES ('$SupplierBillNum','$AirUpgradeXquery','Airline Upgrade','$AirUpgradeID','$AirUpgradeName','$AirUpgradeQty','$AirUpgradeCost')" ;
								GDb::execute($SQLquery5);
								}
							}

											//To get how much paid for this group
											$TotalPaidSQL = "SELECT SUM(sp.Supplier_Payment_Amount) AS Total_Paid FROM suppliers_payments sp WHERE sp.Supplier_Payment_Purchase_Order_ID='$PurchaseOrderNum'";
											$TotalPaidResult  = GDb::fetchRow($TotalPaidSQL);
											$TotalPaidSoFarEach = $TotalPaidResult['Total_Paid'];
											
											
									$TR_Group_Row .="<tr style=\"border-top: double 3px #ccc;\">
										<td colspan=\"3\" class=\"text-right\" style=\"padding:5px;font-weight:bold;\"><a href=\"bills-edit.php?id=".$SupplierBillNum."&action=edit\">Paid So Far:</a></td>
										<td style=\"padding-top:5px;padding-bottom:5px;font-weight:bold;\">";
											$TR_Group_Row .="<a href=\"bills-edit.php?id=".$SupplierBillNum."&action=edit\">".GUtils::formatMoney($TotalPaidSoFarEach)."</a>";
											/*To calculate the total paid so far*/
											$TotalPaidAllGroups = $TotalPaidAllGroups+$TotalPaidSoFarEach;
										$TR_Group_Row .="</td>
									</tr>
									<tr style=\"border-top: double 3px #ccc;\">
										<td style=\"padding:5px;font-weight:bold;\"><a href=\"bills-edit.php?id=".$SupplierBillNum."&action=edit\" class=\"btn btn-mat waves-effect waves-light btn-inverse pt-1 pb-1 btn-block\">Go to Supplier Bill</a></td>
										<td colspan=\"2\" class=\"text-right\" style=\"padding:5px;font-weight:bold;\">Total Cost:</td>
										<td style=\"padding-top:5px;padding-bottom:5px;font-weight:bold;\">";
										$TotalExpensesEachGroup = ($LandProductTotal+$SingleProductTotal+$AirlineProductTotal+$LandUpgradeTotal+$AirlineUpgradeTotal);
										$TR_Group_Row .=GUtils::formatMoney($TotalExpensesEachGroup-$TotalPaidSoFarEach);
										/*To calculate the total expenses*/
										$TotalExpensesAllGroups = $TotalExpensesAllGroups+$TotalExpensesEachGroup;
										$TR_Group_Row .="<script>document.getElementById('StyleGroup".$FilteredTourData['tourid']."').firstChild.nodeValue = '".GUtils::formatMoney($TotalExpensesEachGroup-$TotalPaidSoFarEach)."';</script></td>
									</tr>
								</tbody>
							</table>
							</div>
						</div>
					</td>
				</tr>
				<tr style=\"display:none;padding-bottom:0px;\">
					<td colspan=\"4\" style=\"padding:0px;\">
					</td>
				</tr>";
				if((($TotalExpensesEachGroup-$TotalPaidSoFarEach) == 0) AND $TotalPaxTour != 0) {
					$GroupsPaidCounter++;
					$TR_Group_Row_Paid .=$TR_Group_Row;
				} elseif((($TotalExpensesEachGroup-$TotalPaidSoFarEach) < 0) AND $TotalPaxTour != 0) {
					$GroupsPaidMoreThanNeededCounter++;
					$TR_Group_Row_Paid_More .=$TR_Group_Row;
				} else {
					$GroupsNotPaidCounter++;
					$TR_Group_Row_NotPaid .=$TR_Group_Row;
				}
				$TR_Group_Row = NULL;
				
				//Update the supplier bill for this record
				$SQLquery6 = "UPDATE suppliers_bill SET Bill_Amount=$TotalExpensesEachGroup WHERE Supplier_Bill_Num=$SupplierBillNum" ;
				GDb::execute($SQLquery6);
} ?>
				<tr>
					<td class="bg-primary pt-1 pb-1" colspan="4">Groups with products for this supplier</td>
				</tr>
				<?php echo $TR_Group_Row_NotPaid; ?>
				<?php //To show the groups that has been paid for its product for the supplier
				if($GroupsPaidCounter > 0) { 
					echo "<tr><td class='bg-success pt-1 pb-1' colspan='4'>Paid Groups with products for this supplier</td></tr>";
					echo $TR_Group_Row_Paid;
				} 
				if($GroupsPaidMoreThanNeededCounter > 0) { 
					echo "<tr><td class='bg-danger pt-1 pb-1' colspan='4'>Groups with products for this supplier that Paid more than needed!</td></tr>";
					echo $TR_Group_Row_Paid_More;
				} ?>
			</tbody>
		</table>
	</div>
</div>