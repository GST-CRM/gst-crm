<style>
    h3{
    color: #000;
    font-size: 24px;
    line-height: 15px;
    font-weight: bold;
}
h2{
    font-size: 60px;
    font-weight: bold;
    line-height: 2px;
}

h5{
    font-size: 20px;
    font-weight: normal;
}

table tr td.main_td{
    text-align: center;
    height: 2.95in;
    width: 4in;
    border-width: 0;
    border-color: #000;
    border-style: solid;
    
}

</style>
<!-- Header Table -->
<?php
$tourid = $_GET['id'];
$sqlGroup = "SELECT tourname,destination FROM groups WHERE tourid=$tourid";
$queryGroupRecord = mysqli_query($db, $sqlGroup) or die("error to fetch group data");
$groupData = mysqli_fetch_row($queryGroupRecord);
$groupName = $groupData[0];
$groupDestination = $groupData[1];
$sqlBadge = "SELECT co.badgename FROM contacts co JOIN customer_account cus ON cus.contactid=co.id WHERE cus.tourid=$tourid AND cus.status=1";
$queryBadgeRecords = mysqli_query($db, $sqlBadge) or die("error to fetch badge data");
$numOfCols = 2;
$rowCount = 0;
$logo = __DIR__ . '/../files/assets/images/shipping-label-logo.jpg';
?>
<table style="width:100%;height:100%;" cellpadding="1" cellspacing="5" border="0" align="center">
<tr nobr="true">
<?php
while( $badgeRow = mysqli_fetch_row($queryBadgeRecords) ) {
    if(str_word_count($badgeRow[0])==1){
        $firstName = $badgeRow[0];
        $lastName = " ";
    }
    else{
        $firstName = strstr($badgeRow[0], ' ', true);
        $lastName = str_replace($firstName, "", $badgeRow[0]);
    }
?>
        <td class="main_td">
        <table cellpadding="0" cellspacing="10" style="" border="0" align="center">
        <tr>
            <table border="0">
				<tr style="line-height: 30px;" > 
					<td></td>
				</tr>
            </table>
            <td style="text-align: center;height: 2.50in;">
            <h3>Goodshepherd Travel</h3>
            <!--BLANK TABLE FOR SPACING -->
            <table border="0">
            <tr style="line-height: 30px;" > 
            <td></td>
            </tr>
            </table>
            <!--BLANK TABLE FOR SPACING -->

            <table cellpadding="0" cellspacing="0" style="" align="center">
            <tr>
                <td style="text-align: center;height: 1.1in;">
                <h2 style="margin:0;"><?php echo $firstName; ?></h2>
                <h5 style="margin:0;"><?php echo $lastName; ?></h5>
                </td>
            </tr>
            </table>
            <span><?php echo $groupName; ?></span>
            </td>
            </tr>
            </table>
        </td>
<?php
    $rowCount++;
    if($rowCount % $numOfCols == 0) echo '</tr><tr style="line-height:2px"><td style="height:100%;width:100%"></td></tr><tr nobr="true">';
}
?>
</tr>

</table>