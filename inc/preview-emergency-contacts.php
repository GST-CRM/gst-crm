<?php include "../session.php";
$PageTitle = "Emergency Contacts";
?>
<style>
#reportsDiv .special-heading{
    color: #dc3545;
}

#reportsDiv .main_td{
    border-width: 4px;
    border-style: solid;
    border-color: #342f21;
    height: 288px;
}
#reportsDiv h5{
    font-size: 20px;
    font-weight: normal;
    line-height: 60px;
    display:block;
}
</style>

<div id="results"></div>
<div class="row">
    <div class="col-md-12" id="emergency-contacts-print-content">
                <h3 class="text-center">Emergency Contacts</h3>
                <a href="inc/download-emergency-contacts.php?id=<?php echo $_REQUEST['id']; ?>" target="_blank" class="btn waves-effect waves-light btn-success" style="float:right;padding: 3px 13px;"><!--<i class="far fa-print"></i>-->Print</a>
                <a href="inc/download-emergency-contacts.php?id=<?php echo $_REQUEST['id']; ?>" download class="btn waves-effect waves-light btn-inverse mr-2" style="float:right;padding: 3px 13px;"><!--<i class="far fa-download"></i>-->Download</a>
                <?php
                $tourid = $_REQUEST['id'];
                
                $numOfCols = 2;
                $rowCount = 0;
                $bootstrapColWidth = 12 / $numOfCols;
                ?>
                <div class="table-responsive mt-3 mb-3">
                    <table style="width: 100%;" border="0" cellpadding="10">
                    <tr nobr="true">
                    <?php
                    $i=1;
                    while($i <= 6) {
                    ?>  
                        <td class="main_td">
                            <table cellpadding="0" cellspacing="10" style="" align="center">
                            <tr>
                            <td style="text-align: center;">
                            <h5>Emergency Contacts Abroad</h5>
                            
                            <table cellpadding="0" cellspacing="0" style="" align="center">
                            <tr>
                                <td style="text-align: center;">
                                <table cellpadding="0" cellspacing="0" style="" align="center" style="width:100%" class="emergency_contacts_entries">
                                
                                <?php
                                //Select land_productid 

                                $sql = "SELECT land_productid FROM groups WHERE tourid=$tourid";
                                $queryLandRecord = mysqli_query($db, $sql) or die("error to fetch land product id");
                                $landData = mysqli_fetch_row($queryLandRecord);
                                $landProductId = $landData[0];
                                //If land product id found
                                if($landProductId!="" && $landProductId!=0){
                                    
                                    //Select product info
                                    $sql = 'SELECT supplierid, producttype,name,subproduct,parentid,meta1,meta2,meta3,meta4 FROM products WHERE parentid = '.$landProductId.' AND producttype IN ("Hotels","Guides","Restaurants")';
                                    //echo $sql;
                                    $queryProduct = mysqli_query($db, $sql) or die("error to fetch product info");
                                    if (mysqli_num_rows($queryProduct) > 0) {
                                        while($row = mysqli_fetch_assoc($queryProduct)) {
                                            //echo $row['name']."<br>";
                                            $supplierid = $row['supplierid'];

                                            ?>
                                            <tr>
                                            <td><?php echo $row['name']; ?>: <?php echo $row['meta2']; ?>:</td>
                                            <td>Phone: <?php echo $row['meta1']; ?></td>
                                            </tr>
                                            <?php

                                        }
                                        //Select land operator
                                        $sql = 'SELECT fname,lname,mobile FROM contacts WHERE id='.$supplierid.'';
                                        $querylandOperator = mysqli_query($db, $sql) or die("error to fetch product info");
                                        $landOperatorData = mysqli_fetch_row($querylandOperator);
                                        $landOperatorName = $landOperatorData[0]." ".$landOperatorData[1];
                                        $landOperatorMobile = $landOperatorData[2];
                                        ?>
                                        <tr>
                                        <td>Land Operator: <?php echo $landOperatorName; ?>:</td>
                                        <td>Mobile: <?php echo $landOperatorMobile; ?></td>
                                        </tr>
                                        <?php

                                    }
                                    else{
                                        echo "<tr><td>No products with parentid ".$landProductId."</td></tr>";
                                    }


                                }
                                else{
                                    echo "<tr><td>Land product id is null.</td></tr>";
                                }
                            ?>
                                </table>

                                </td>
                            </tr>
                            </table>
                            </td>
                            </tr>
                            </table>
                        </td>
                    <?php
                        $rowCount++;
                        $i++;
                        if($rowCount % $numOfCols == 0) echo '</tr><tr nobr="true">';
                    }
                    ?>
                    </tr>
                    </table>
                </div>
    </div>
</div>
