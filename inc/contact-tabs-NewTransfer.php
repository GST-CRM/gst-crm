<form  id="contact6" name="contact6" method="post" action="contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=customer">
    <div class="row">
                    <div class="form-group col-sm-12">
                        <h3>Add a new Transfer Product</h3>
                    </div>
                    <div class="col-sm-8">
						<h4 class="sub-title">The Group Needed</h4>
						<select name="Transfer_GroupID" class="form-control form-control-default fill" required="">
							<?php 
							$con18 = new mysqli($servername, $username, $password, $dbname);
							$result18 = mysqli_query($con18,"SELECT gro.tourid,gro.tourname FROM groups gro JOIN customer_account ca ON ca.tourid=gro.tourid WHERE ca.contactid=$leadid");
							$GroupCountforCustomer = mysqli_num_rows($result18);
							$SelectedGroup = "";
							if($GroupCountforCustomer == 1) { $SelectedGroup = "selected"; } 
							while($row18 = mysqli_fetch_array($result18))
							{
								echo "<option value='".$row18['tourid']."' $SelectedGroup>".$row18['tourname']."</option>";
							} ?>
						</select>
                    </div>
                    <div class="col-sm-4">
						<h4 class="sub-title">Product's Supplier</h4>
						<select name="Transfer_supplier" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple" required="">
							<?php 
							$con17 = new mysqli($servername, $username, $password, $dbname);
							$result17 = mysqli_query($con17,"SELECT c.id, c.fname, c.mname, c.lname FROM contacts c JOIN suppliers s ON c.id=s.contactid WHERE c.usertype LIKE '%2%' AND s.status=1");
							while($row17 = mysqli_fetch_array($result17))
							{
								echo "<option value='".$row17['id']."'>".$row17['fname']." ".$row17['mname']." ".$row17['lname']."</option>";
							} ?>
						</select>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label">Transfer Product Name</label>
                        <input type="text" name="Transfer_productname" class="form-control" required="">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-5">
                        <label class="float-label">Transfer Product Discription</label>
                        <input type="text" name="Transfer_description" class="form-control">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-2">
                        <label class="float-label">Cost Price $</label>
                        <input type="text" name="Transfer_cost" class="form-control">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-2">
                        <label class="float-label">Selling Price $</label>
                        <input type="text" name="Transfer_price" class="form-control">
                    </div>
                    <div class="col-sm-12"><br />
					<button type="submit" name="Transfersubmit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" onclick="uploaddisablealert()"><i class="far fa-check-circle"></i>Add</button>
                    <button type="reset" class="btn waves-effect waves-light btn-inverse" data-dismiss="modal"><i class="fas fa-ban"></i>Cancel</button>
					</div>
	</div>
</form>
