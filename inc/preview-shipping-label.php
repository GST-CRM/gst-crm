<?php include "../session.php";
$PageTitle = "Shipping Label";
?>
<style>/*
h3{
    color: #3f4d67;
    font-size: 30px;
    line-height: 55px;
    font-weight: bold;
}

h2{
    font-size: 45px;
    font-weight: bold;
    line-height: normal;
}
.special-heading{
    color: #dc3545;
}
#name-badge-print-content table tr td{
    height: 288px;
    width: 384px;
    border-width: 6px;
    border-style: solid;
    border-color: #342f21;
}
table tr td{
    height: 288px;
    width: 384px;
    border-width: 4px 4px 4px 4px;
    border-style: double;
    border-color: #484747;
    
}*/
#reportsDiv h5{
    font-size: 20px;
    font-weight: normal;
    line-height: 40px;
    display:block;
}
</style>

<div id="results"></div>
<div class="row">
    <div class="col-md-12" id="name-badge-print-content">
                <h3 class="text-center">Shipping Labels</h3>
                <a href="inc/download-shipping-label.php?id=<?php echo $_REQUEST['id']; ?>" target="_blank" class="btn waves-effect waves-light btn-success" style="float:right;padding: 3px 13px;"><!--<i class="far fa-print"></i>-->Print</a>
                <a href="inc/download-shipping-label.php?id=<?php echo $_REQUEST['id']; ?>" download class="btn waves-effect waves-light btn-inverse mr-2" style="float:right;padding: 3px 13px;"><!--<i class="far fa-download"></i>-->Download</a>
                


            <?php
$logo = 'files/assets/images/shipping-label-logo.jpg';
$top_right_border = 'files/assets/images/shipping-label-top-border.jpg';
$bottom_left_border = 'files/assets/images/shipping-label-bottom-border.jpg';
?>
<style>
.logo{
    text-align: left;
    border-width: 1px 1px 1px 1px;
    border-color: #ccc;
    border-style: solid;
}
td.main_td{
    width: 384px;
    text-align: left;
    border-width: 1px 1px 1px 1px;
    border-color: #ccc;
    border-style: solid;
    margin: 0px 0px 0px 0px;
    margin-left:0%;
}
tr{
    text-align: left;
}
img{
    text-align: left;
    margin-left:0;
    padding-left:0;
}

</style>
<!-- Header Table -->
<?php
$tourid = $_REQUEST['id'];
$sql = "SELECT contactid,groupinvoice from customer_account WHERE tourid=$tourid AND status=1";
$queryCustomer = mysqli_query($db, $sql) or die("error to fetch customer_account data");
$numOfCols = 2;
$rowCount = 0;

$contactsCount = 0;
?>
<div class="table-responsive mt-3 mb-3">
<table style="width: 100%" align="center" cellpadding="3" cellspacing="5">
<tbody>
<tr nobr="true">
<?php
while( $customerRow = mysqli_fetch_row($queryCustomer) ) {
    $contactid = $customerRow[0];
    //Multiple customer details
    if($customerRow[1]=="1" || $customerRow[1]==1){
        //Select primary customer details
        $sql = "SELECT title,fname,lname,address1,city,state,zipcode,address2 FROM contacts where id = $contactid";
        $queryContacts = mysqli_query($db, $sql) or die("error to fetch customer contacts data");
        $ContactData = mysqli_fetch_row($queryContacts);
        $cname = $ContactData[0]." ".$ContactData[1]." ".$ContactData[2]."<br>";
		if($ContactData[7] != NULL) { $SecondAddress = $ContactData[7]."<br />"; } else { $SecondAddress = ""; }
        $caddress = $ContactData[3]."<br>".$SecondAddress.$ContactData[4]." ".$ContactData[5].", ".$ContactData[6];

        //Select additional traveler details
        $sql = "SELECT Additional_Traveler_ID_1,Additional_Traveler_ID_2,Additional_Traveler_ID_3 FROM customer_groups where Primary_Customer_ID = $contactid";
        $queryCustomerGroups = mysqli_query($db, $sql) or die("error to fetch customer groups data");
        //echo $sql;
        $groupData = mysqli_fetch_row($queryCustomerGroups);
        $addTravelerIDs = array($groupData[0],$groupData[1],$groupData[2]);
        $addTravelerIDs = array_filter($addTravelerIDs, 'strlen');
        $addTravelerIDs = array_filter($addTravelerIDs);
        $contactsCount = count($addTravelerIDs);
        if($contactsCount>0){
            foreach($addTravelerIDs as $addTravelerID){
                $sql = "SELECT title,fname,lname FROM contacts where id = $addTravelerID";
                $queryContacts = mysqli_query($db, $sql) or die("error to fetch customer_contacts data");
                $ContactData = mysqli_fetch_row($queryContacts);
                $cname .= $ContactData[0]." ".$ContactData[1]." ".$ContactData[2]."<br>";
            }
        }
        
    }
    //Single customer details
    else{
        
        $sql = "SELECT title,fname,lname,address1,city,state,zipcode,address2 FROM contacts where id = $contactid";
        $queryContacts = mysqli_query($db, $sql) or die("error to fetch customer groups data");
        $ContactData = mysqli_fetch_row($queryContacts);
        $cname = $ContactData[0]." ".$ContactData[1]." ".$ContactData[2]."<br>";
		if($ContactData[7] != NULL) { $SecondAddress = $ContactData[7]."<br />"; } else { $SecondAddress = ""; }
        $caddress = $ContactData[3]."<br>".$SecondAddress.$ContactData[4]." ".$ContactData[5].", ".$ContactData[6];
    }
?>
        <td class="main_td">
        <table style="width: 100%;">
        <tr>
        <td style="text-align: left;">
        <!--BLANK TABLE FOR SPACING -->
        <table border="0">
        <tr style="line-height: 10px;" > 
        <td></td>
        </tr>
        </table>
        <!--BLANK TABLE FOR SPACING -->
        <img src="<?php echo $logo; ?>" height="100" class="logo">
        <div style="text-align:center;">9021 Washington Ln <br>Lantana, Texas 76226</div>
        
        </td>
        <td style="text-align: right;    vertical-align: top;">
        <!--BLANK TABLE FOR SPACING -->
        <table border="0">
        <tr style="line-height: 8px;" > 
        <td></td>
        </tr>
        </table>
        <!--BLANK TABLE FOR SPACING -->
        <img src="<?php echo $top_right_border; ?>">
        </td>
        </tr>

        <tr>
        <td style="text-align: left;" width="37%">
        <!--BLANK TABLE FOR SPACING -->
        <table border="0">
        <tr style="line-height: 30px;" > 
        <td></td>
        </tr>
        </table>
        <!--BLANK TABLE FOR SPACING -->
        <img src="<?php echo $bottom_left_border; ?>">
        </td>
        <td style="text-align:left;" width="63%">
        <!--BLANK TABLE FOR SPACING -->
        <table border="0">
        <tr style="line-height: <?php echo ($contactsCount > 0 ? '15px' : '25px');?>;" > 
        <td></td>
        </tr>
        </table>
        <!--BLANK TABLE FOR SPACING -->
        <table>
        <tr>
        <td width="11%"><div style="line-height:15px;text-align:right">To: </div></td>
        <td width="80%"><div style="line-height:15px;text-align:right"><?php echo $cname; ?><br><?php echo $caddress; ?></div></td>
        <!--<td width="80%"><div style="line-height:15px;text-align:right">Pastor Harris Waller Seed lll<br>Ms. Lori H Seed<br>2050 Rancho Corte<br>Vista, CA  92084</div></td>-->
        <td width="9%"></td>
        </tr>
        </table>
        
        
        </td>
        </tr>

        </table>
        </td>
        
<?php
    $rowCount++;
    if($rowCount % $numOfCols == 0) echo '</tr><tr nobr="true">';
}
?>
</tr>
</tbody>
</table>
</div>
    </div>
</div>

