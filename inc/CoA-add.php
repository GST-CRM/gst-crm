<?php
include "../session.php";
$connect = new mysqli($servername, $username, $password, $dbname);
if(isset($_POST["Chart_Account"]))
{
 $GL_Group_Num = mysqli_real_escape_string($connect, $_POST["GL_Group_Num"]);
 $Chart_Account = mysqli_real_escape_string($connect, $_POST["Chart_Account"]);
 $NewCategoryInput = mysqli_real_escape_string($connect, $_POST["NewCategoryInput"]);
 $query = "";
 
 if($NewCategoryInput != NULL) {
	$query.= "INSERT INTO `gl_account_groups`(GL_Group_Num,GL_Group_Name,GL_Group_Status,GL_Account_Classes_Class_ID) SELECT MAX(GL_Group_Num)+1 AS GL_Group_Num, '$NewCategoryInput' AS GL_Group_Name, '1' AS GL_Group_Status, '6' AS GL_Account_Classes_Class_ID FROM gl_account_groups;";
	$query.= "INSERT INTO gl_chart_of_accounts(GL_Account_Name,GL_Account_Groups_Group_ID) SELECT '$Chart_Account' AS GL_Account_Name, MAX(GL_Group_Num) AS GL_Group_Num FROM gl_account_groups;";
 } else {
	$query.= "INSERT INTO gl_chart_of_accounts(GL_Account_Name,GL_Account_Groups_Group_ID) VALUES('$Chart_Account','$GL_Group_Num');";
 }
 if(mysqli_multi_query($connect, $query)) {
  echo 'The new Chart of Account ('.$Chart_Account.') has been added successfully.';
 }
}
?>