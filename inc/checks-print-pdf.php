<?php

if(isset($_GET['checkid']) && is_numeric($_GET['checkid'])) { 
	include_once "../session.php";
	require_once('helpers-pdf.php');
} else {
	require_once('files/pdf/tcpdf.php');
}


// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Element Media');
$pdf->SetTitle('GST Check');
$pdf->SetSubject('GST Check');
$pdf->SetKeywords('GST, CRM, Element, Media, Element.ps');

// remove default header/footer
$PDF_HEADER = isset($PDF_HEADER) ? $PDF_HEADER : false ;
$pdf->setPrintHeader($PDF_HEADER);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/../lang/eng.php')) {
    require_once(dirname(__FILE__).'/../lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
//$pdf->SetMargins(15, 25, 15);
$pdf->SetMargins(5, 0, 5);

// set font

$pdf->SetFont('MICREncoding', '', 30, '', false);
$pdf->SetFont('times', '', 10);
    // add a page
$pdf->AddPage('P','LETTER');

/*$fontname = TCPDF_FONTS::addTTFfont('MICR65.ttf', 'TrueTypeUnicode', '', 32);
$pdf->SetFont($fontname,'',10,false);*/

// set margins
$pdf->setPageMark();

$invoiceNumSet = [];

if( isset($__REQUEST['cbInvoice']) && isset($__REQUEST['batch-invaction']) && $__REQUEST['batch-invaction'] == 'P') {
    $invoiceNumSet = $__REQUEST['cbInvoice'] ;
}
else if( isset($__REQUEST['batch-invaction']) && $__REQUEST['batch-invaction'] == 'S') {
    $invoiceNumSet = [ $invoiceId ] ;
}
ob_start() ;
$invId = isset($__REQUEST['invid']) ? $__REQUEST['invid'] : 0;
if( ! isset($invoiceNumSet) || empty($invoiceNumSet) ) {
    $invoiceNumSet = [ $invId ] ;
}
if(isset($_GET['checkid']) && is_numeric($_GET['checkid'])) { 
	$invoiceNumSet = [ $_GET['checkid'] ] ;
}
$p = 0 ;
$ChecksID = array_unique($invoiceNumSet);
foreach( $ChecksID as $CheckID ) {
    if( $p > 0 ) {
        echo '<br pagebreak="true" style="height:0;" />';
    }
    include 'checks-print-pdf-content.php';
    $p ++ ;
}
$pdfData = ob_get_clean() ;

// output the HTML content
$pdf->writeHTML($pdfData, true, false, true, false, '');


// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
return $pdf->Output('Checks.pdf', 'I' );

//============================================================+
// END OF FILE
//============================================================+
