<?php
$logo = __DIR__ . '/../files/assets/images/shipping-label-logo.jpg';
$top_right_border = __DIR__ . '/../files/assets/images/shipping-label-top-border.jpg';
$bottom_left_border = __DIR__ . '/../files/assets/images/shipping-label-bottom-border.jpg';
$blank_img = __DIR__ . '/../files/assets/images/blank.jpg';
?>
<style>

.logo{
    text-align: left;
    /*border-width: 1px 1px 1px 1px;
    border-color: #ccc;
    border-style: solid;*/
}
td.main_td{
    /*text-align: left;
    height: 2.99in;
    width: 3.99in;
    border-width: 0.01in 0.01in 0.01in 0.01in;
    
    
    border-color: #484747;
    border-style: solid;
    border-right-width: 0.01in;
    border-right-color: #484747;
    border-right-style: solid;
    padding:0px;
    border-width: 0.01in 0.01in 0.01in 0.01in;
    border-color: #484747;
    border-style: solid;*/
}

tr{
    text-align: left;
    
}
img{
    margin:0;
}

.col{
    
}
.label_container{
}
td.main_td{
    margin:0;
    /*border: 1px solid #000;*/
    padding:0;
    width:4in;
}
tr{
    margin:0;
    padding:0;
}


</style>
<!-- Header Table -->
<?php
$tourid = $_GET['id'];
$sql = "SELECT contactid,groupinvoice from customer_account WHERE tourid=$tourid AND status=1";
$queryCustomer = mysqli_query($db, $sql) or die("error to fetch customer_account data");
$numOfCols = 2;
$rowCount = 0;

$contactsCount = 0;
?>

<table cellpadding="0" cellspacing="0">
<tr nobr="true">
<?php
while( $customerRow = mysqli_fetch_row($queryCustomer) ) {
    $contactid = $customerRow[0];
    //Multiple customer details
    if($customerRow[1]=="1" || $customerRow[1]==1){
        //Select primary customer details
        $sql = "SELECT title,fname,lname,address1,city,state,zipcode,address2 FROM contacts where id = $contactid";
        $queryContacts = mysqli_query($db, $sql) or die("error to fetch customer contacts data");
        $ContactData = mysqli_fetch_row($queryContacts);
        $cname = $ContactData[0]." ".$ContactData[1]." ".$ContactData[2]."<br>";
		if($ContactData[7] != NULL) { $SecondAddress = $ContactData[7]."<br />"; } else { $SecondAddress = ""; }
        $caddress = $ContactData[3]."<br>".$SecondAddress.$ContactData[4]." ".$ContactData[5].", ".$ContactData[6];

        //Select additional traveler details
        $sql = "SELECT Additional_Traveler_ID_1,Additional_Traveler_ID_2,Additional_Traveler_ID_3 FROM customer_groups where Primary_Customer_ID = $contactid";
        $queryCustomerGroups = mysqli_query($db, $sql) or die("error to fetch customer groups data");
        //echo $sql;
        $groupData = mysqli_fetch_row($queryCustomerGroups);
        $addTravelerIDs = array($groupData[0],$groupData[1],$groupData[2]);
        $addTravelerIDs = array_filter($addTravelerIDs, 'strlen');
        $addTravelerIDs = array_filter($addTravelerIDs);
        $contactsCount = count($addTravelerIDs);
        if($contactsCount>0){
            foreach($addTravelerIDs as $addTravelerID){
                $sql = "SELECT title,fname,lname FROM contacts where id = $addTravelerID";
                $queryContacts = mysqli_query($db, $sql) or die("error to fetch customer_contacts data");
                $ContactData = mysqli_fetch_row($queryContacts);
                $cname .= $ContactData[0]." ".$ContactData[1]." ".$ContactData[2]."<br>";
            }
        }
        
    }
    //Single customer details
    else{
        
        $sql = "SELECT title,fname,lname,address1,city,state,zipcode,address2 FROM contacts where id = $contactid";
        $queryContacts = mysqli_query($db, $sql) or die("error to fetch customer groups data");
        $ContactData = mysqli_fetch_row($queryContacts);
        $cname = $ContactData[0]." ".$ContactData[1]." ".$ContactData[2]."<br>";
		if($ContactData[7] != NULL) { $SecondAddress = $ContactData[7]."<br />"; } else { $SecondAddress = ""; }
        $caddress = $ContactData[3]."<br>".$SecondAddress.$ContactData[4]." ".$ContactData[5].", ".$ContactData[6];
    }
?>
        <td class="main_td">
        <div class="label_container">
        
        <table cellpadding="0" cellspacing="0">
        <tr class="row">
        <td class="col" style="width:70%">
        <img src="<?php echo $logo; ?>" class="logo" height="100">
        <div style="text-align:left;"><img src="<?php echo $blank_img;?>" width="10">9021 Washington Ln <br><img src="<?php echo $blank_img;?>" width="10">Lantana, Texas 76226</div>
        </td>

        <td class="col" style="text-align:right;width:26%">
        <img src="<?php echo $top_right_border; ?>" style="height:1.44in;">
        </td>
        </tr>


        
        <tr class="row">
        <td class="col" style="width:15%;">
        <img src="<?php echo $bottom_left_border; ?>" style="height:1.44in;">

        </td>

        <td class="col" style="width:85%;text-align:left;">
        <img src="<?php echo $blank_img; ?>" style="height:50px;width:10px;">
        <table>
        <tr>
        <td style="width:15%;text-align:left;">To: </td>
        <td style="width:75%"><div style="text-align:left;"><?php echo $cname; ?>
<?php echo $caddress; ?></div></td>
        <td style="width:10%"></td>
        </tr>
        </table>
        
        </td>
        </tr>

        </table>

        </div>
        </td>
        
<?php
    $rowCount++;
    //if($rowCount % $numOfCols == 0) echo '</tr><tr nobr="true">';
    if($rowCount % $numOfCols != 0) echo '<td style="width:2.3%"></td>';
    if($rowCount % $numOfCols == 0) echo '</tr><tr nobr="true"><td style="line-height:0px;"></td></tr><tr nobr="true">';
}
?>
</tr>
</table>