<?php

include_once "inc/helpers.php";

include_once __DIR__ . '/../models/customer_invoice.php' ;
include_once __DIR__ . '/../models/customer_refund.php' ;

$CALLING_PAGE = 'groups-edit.php?tab=invoice&id=' . $tourid . '&action=edit' ;

include __DIR__ . '/sales-invoice-query.php' ;


?>
<form action="inc/tickets-action.php" method="POST" name="TicketsListForm" class="row">
<input type="text" name="GroupTicketID" value="<?php echo $tourid; ?>" hidden>
		<div class="row">
			<div class="col-md-3">
				<div class="input-group input-group-sm mb-0">
					<span class="input-group-prepend mt-0">
						<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
					</span>
					<input type="text" class="outsideBorderSearchEx2 form-control" placeholder="Search...">
				</div>
			</div>
			<div class="col-md-6">
			</div>
			<div class="col-md-3">
				<span style="float:right;">
					<select name="batch-invaction" onchange="return batchTicketSubmit(this);" class="form-control form-control-default fill gst-ticket-batchaction" disabled="disabled">
						<option value="">Batch Action</option>
						<option value="S" disabled>Send Ticket Receipts</option>
						<option value="P">Print Ticket Receipts</option>
					</select>
				</span>
			</div>
			<div class="col-md-12">
			<table id="idSalesInvoiceTable2" class="table gst-table table-hover table-striped table-bordered nowrap responsive" data-page-length="100" style="width:100%;">
				<thead>
					<tr>
						<th class="gst-no-sort gst-has-events ">
							<input type="checkbox" class="gst-ticket-cb-parent" />
						</th>
						<th>Customer Name</th>
						<th>Ticket #</th>
						<th>Confirmation #</th>
					</tr>
				</thead>
			</table>
			</div>
		</div>
		</form>
<script type="text/javascript">
document.addEventListener('DOMContentLoaded', function () {
	//Just to move the search box to be on the top of the card section
	$('.outsideBorderSearchEx2').on( 'keyup click', function () {
		$('#idSalesInvoiceTable2').DataTable().search(
			$('.outsideBorderSearchEx2').val()
		).draw();
	});


    $('#idSalesInvoiceTable2').DataTable({
		"bProcessing": true,
        "serverSide": true,
		"bLengthChange": false, //To hide the Show X entries dropdown
		dom: 'Bfrtip',
		sDom: 'lrtip', //To Hide the search box
        buttons: [], //To hide the export buttons
        ajax : {
            url :"inc/datatable-confirmations.php?tourid=<?php echo isset($tourid) ? $tourid : '';?>", // json datasource
            complete : function(){
                delayedBindingCheckbox2() ;
            }
        },
		'columnDefs': [ {
			'targets': [0], /* column index */
			'orderable': false, /* true or false */
		}]
    }); 
});

    function batchTicketSubmit(obj) {
        var count = $('.gst-ticket-cb:checked').length ;
        if( count > 0 ) {
            
            obj.form.target = '_blank' ;
            if( obj.value == 'S' ) {
                msg = "Are you sure you want to sent " + count + " ticket(s) ?" ;
            }
            else if( obj.value == 'P' ) {
                msg = "Are you sure you want to print " + count + " ticket(s) ?" ;
            }
            else {
                return ;
            }
            showConfirm( "javascript:TicketsListForm.submit();", "Yes", msg, "Please Confirm", "modal-md") ;
//            obj.form.submit();
        }
    }

</script>
<?php
global $GLOBAL_SCRIPT ;
$GLOBAL_SCRIPT .= "bindCheckAll('.gst-ticket-cb-parent', '.gst-ticket-cb-children');" ;
?>