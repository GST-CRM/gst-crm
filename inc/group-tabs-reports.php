<div class="row">
	<div class="col-12">

	<div class="w3-row">
		<a href="javascript:void(0)" onclick="openReportCategory(event, 'Report_General');">
			<div class="w3-quarter tablink w3-bottombar w3-hover-light-grey w3-padding">General Reports</div>
		</a>
		<a href="javascript:void(0)" onclick="openReportCategory(event, 'Report_Supplier');">
			<div class="w3-quarter tablink w3-bottombar w3-hover-light-grey w3-padding">Supplier Reports</div>
		</a>
		<a href="javascript:void(0)" onclick="openReportCategory(event, 'Report_Agent');">
			<div class="w3-quarter tablink w3-bottombar w3-hover-light-grey w3-padding">Agent Reports</div>
		</a>
		<a href="javascript:void(0)" onclick="openReportCategory(event, 'Report_Leader');">
			<div class="w3-quarter tablink w3-bottombar w3-hover-light-grey w3-padding">Leader Reports</div>
		</a>
	</div>

	<div id="Report_Empty" class="w3-container pt-3" style="height:49px;width:200px;">
	</div>
	<div id="Report_General" class="w3-container ReportCategoryX pt-3" style="display:none">
		<?php if( AclPermission::actionAllowed('NameBadges') ) { ?>
		<a href="javascript:void(0)" style="float: left;margin-right: 15px;" data-id="<?php echo $_GET['id']; ?>" data-type="name_badges" class="tab" id="name_badges_tab">
			<h4 class="sub-title">Name Badges</h4>
		</a>
		<?php } ?>
		<?php if( AclPermission::actionAllowed('ShippingLabels') ) { ?>
		<a href="javascript:void(0)" style="float: left;margin-right: 15px;" data-id="<?php echo $_GET['id']; ?>" data-type="shipping_labels" class="tab">
			<h4 class="sub-title">Shipping Labels</h4>
		</a>
		<?php } ?>
		<?php if( AclPermission::actionAllowed('EmergencyContacts') ) { ?>
		<a href="javascript:void(0)" style="float: left;margin-right: 15px;" data-id="<?php echo $_GET['id']; ?>" data-type="emergency_contacts" class="tab">
			<h4 class="sub-title">Emergency Contacts</h4>
		</a>
		<?php } ?>
	</div>
	<div id="Report_Supplier" class="w3-container ReportCategoryX pt-3" style="display:none">
		<a href="javascript:void(0)" style="float: left;margin-right: 15px;" data-tourid="<?php echo $tourid; ?>" data-tourname="<?php echo $data["tourname"]; ?>" data-type="manifest" class="tab">
			<h4 class="sub-title">Manifest</h4>
		</a>	
		<a href="javascript:void(0)" style="float: left;margin-right: 15px;" data-tourid="<?php echo $tourid; ?>" data-tourname="<?php echo $data["tourname"]; ?>" data-type="rooming" class="tab">
			<h4 class="sub-title">Rooming</h4>
		</a>	
		<a href="javascript:void(0)" style="float: left;margin-right: 15px;" data-id="<?php echo $_GET['id']; ?>" data-type="tour_flights" class="tab">
			<h4 class="sub-title">Flight Report</h4>
		</a>
	</div>
	<div id="Report_Agent" class="w3-container ReportCategoryX pt-3" style="display:none">
	</div>
	<div id="Report_Leader" class="w3-container ReportCategoryX pt-3" style="display:none">
		<?php if( AclPermission::actionAllowed('TourLeaderReport') ) { ?>
		<a href="javascript:void(0)" style="float: left;margin-right: 15px;" data-id="<?php echo $_GET['id']; ?>" data-type="tour_leader" class="tab">
			<h4 class="sub-title">Tour Leader Report</h4>
		</a>
		<?php } ?>
	</div>

	</div>
	<div id="reportsDiv" class="col-12 text-center" style="min-height: 300px">
		<label>Please select the report you need to view here.</label>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		/*setTimeout(function(){
			$("#name_badges_tab").trigger("click");
		},100);*/
		
	    $('.tab').click(function() {
	    	$('#reportsDiv').html("<div class='spinner'></div>");
			var type 		= $(this).attr("data-type");
			var id 			= $(this).attr("data-id");
			var groupid 	= $(this).attr("data-tourid");
			var tourname 	= $(this).attr("data-tourname");
			$.ajax({
				type 	: "POST",
				data 	: {"type":type, "id":id, "groupid":groupid, "tourname":tourname},
				url 	: 'inc/group-tabs-reports-preview.php',
				success: function (data) {
					$('#reportsDiv').html(data);
				}
			});
			
	    })
	});
	// function show_report(id,tourid,tourname)
	// {
	// 	$.ajax({
	// 		type 	: "POST",
	// 		data 	: {"id":id, "tourid":tourid, "tourname":tourname},
	// 		url 	: 'groups-tabs-reports-preview.php',
	// 		success: function (data) {
	// 			$('#reportsDiv').html(data);
	// 		}
	// 	});
	// }
	
	
function openReportCategory(evt, ReportCat) {
  var i, x, tablinks;
  x = document.getElementsByClassName("ReportCategoryX");
  for (i = 0; i < x.length; i++) {
    //x[i].style.display = "none";
    $(x[i]).hide("fast");
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" w3-border-red", "");
  }
  //document.getElementById(ReportCat).style.display = "block";
  $("#"+ReportCat).show("slow");
  $("#Report_Empty").hide("fast");
  evt.currentTarget.firstElementChild.className += " w3-border-red";
}
</script>