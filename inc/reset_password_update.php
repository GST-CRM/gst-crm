<?php

include_once __DIR__ . '/../models/users.php' ;

$token = $__POST['token'] ;
$oldPassword = $__POST['old_password'] ;
$newPassword = $__POST['new_password'] ;
$confirmPassword = $__POST['confirm_password'] ;

$user = new Users() ;

$userData = null ;
if( $token ) {
    $userData = $user->get( " token='$token' AND token != '' ") ;
}

$fail = false ;
if( $newPassword != $confirmPassword || strlen($newPassword) < 1 ) {
    //password mismatch
    $fail = true ;
    GUtils::setWarning('New password and confirm password does not match.') ;
    return ;
}

if( !is_array($userData) || strlen($userData['Username']) < 1 ) {
    //token invalid 
    $fail = true ;
    GUtils::setWarning('Incorrect old password.') ;
    return ;
}
if( $userData['token_type'] == Users::$TOKEN_CHANGE_PASSWORD ) {
    if( $userData['Algorithm'] == 'MD5' ) {
        if( md5($oldPassword) != $userData['Password'] ) {
            $fail = true ;
            GUtils::setWarning('Incorrect old password.') ;
            return ;     
         }
    }
    else if( $userData['Algorithm'] == 'SHA256' ) {
        if( hash('sha256', $oldPassword) != $userData['Password'] ) {
            $fail = true ;
            GUtils::setWarning('Incorrect old password.') ;
            return ;     
         }
    }
}
if( ! $fail ) {
    $data = array(
        'Password' => hash('sha256', $newPassword),
        'token' => '',
        'Algorithm' => 'SHA256'
    ) ;
    $where = array (
        'token' => $token
    ); 
    
    if( $user->update($data, $where) ) {
        //success.
        GUtils::setSuccess('Password successfully updated.') ;
        GUtils::redirect('login.php') ;
        return ;
    }
}
GUtils::setWarning('Unable to change password.') ;
return ;
