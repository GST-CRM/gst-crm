<?php

// Include the main TCPDF library (search for installation path).
require_once('../files/pdf/tcpdf.php');

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Element Media');
$pdf->SetTitle('GST Customer Ticket Receipt');
$pdf->SetSubject('GST Customer Ticket Receipt');
$pdf->SetKeywords('GST, CRM, Element, Media, Element.ps');

// remove default header/footer
$PDF_HEADER = isset($PDF_HEADER) ? $PDF_HEADER : false ;
$pdf->setPrintHeader($PDF_HEADER);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/../lang/eng.php')) {
    require_once(dirname(__FILE__).'/../lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
$pdf->SetMargins(15, 25, 15);

// set font
$pdf->SetFont('Helvetica', '', 11);
    // add a page
$pdf->AddPage('P','LETTER');

// set margins
$pdf->setPageMark() ;

$invoiceNumSet = [] ;

if( isset($__REQUEST['TicketID']) && isset($__REQUEST['batch-invaction']) && $__REQUEST['batch-invaction'] == 'P') {
    $CustomerTicketIDs = $__REQUEST['TicketID'] ;
}
else if( isset($__REQUEST['batch-invaction']) && $__REQUEST['batch-invaction'] == 'S') {
    $CustomerTicketIDs = [ $invoiceId ] ;
}
ob_start() ;
$invId = isset($__REQUEST['invid']) ? $__REQUEST['invid'] : 0 ;
if( ! isset($CustomerTicketIDs) || empty($CustomerTicketIDs) ) {
    $CustomerTicketIDs = [ $invId ] ;
}
$p = 0 ;
$CustomerTicketIDs = array_unique($CustomerTicketIDs) ;
foreach( $CustomerTicketIDs as $CustomerTicketID ) {

    if( $p > 0 ) {
        echo '<br pagebreak="true" style="height:0;" />' ;
    }
    include 'tickets-pdf-content.php';
    $p ++ ;
}
$pdfData = ob_get_clean() ;

// output the HTML content
$pdf->writeHTML($pdfData, true, false, true, false, '');


// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

$PDF_OUTPUT = isset($PDF_OUTPUT ) ? $PDF_OUTPUT : 'I' ;
//Close and output PDF document
return $pdf->Output('Customer-Ticket-Confirmation.pdf', $PDF_OUTPUT );

//============================================================+
// END OF FILE
//============================================================+
