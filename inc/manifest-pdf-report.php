<?php

// Include the main TCPDF library (search for installation path).
require_once('files/pdf/tcpdf.php');
include_once __DIR__ . '/../models/groups.php' ;
include_once __DIR__ . '/../models/customer_payment.php' ;
include_once __DIR__ . '/../library/MYPDF2.php' ;

/*
$report_title = "";
$report_slug = "";
$report_slug = $_GET['report_slug'];*/
$report_title = str_replace("-"," ",$report_slug);


// create new PDF document
$pdf = new MYPDF2(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Element Media');
$pdf->SetTitle($report_title);
$pdf->SetSubject($report_title);
$pdf->SetKeywords('');

// remove default header/footer
$PDF_HEADER = isset($PDF_HEADER) ? $PDF_HEADER : false ;
$pdf->setPrintHeader($PDF_HEADER);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/../lang/eng.php')) {
    require_once(dirname(__FILE__).'/../lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
$pdf->SetMargins(10, 12, 10);

// set font
$pdf->SetFont('Helvetica', '', 10);
    // add a page
$pdf->AddPage('L','A3');

// set margins
$pdf->setPageMark() ;



ob_start();

?>
<style>
th{
	height:40px;
}
td{
	border:1px solid #000;
	min-height:25px;
	line-height:20px;
}
</style>
<?php

$tourid = $_REQUEST['id'];


$sql = "SELECT tourname FROM groups WHERE tourid=$tourid";
$queryGroup = mysqli_query($db, $sql) or die("error to fetch group name");
$groupRow = mysqli_fetch_row($queryGroup);
$groupName = $groupRow[0];

$query          = "SELECT * FROM customer_account cus JOIN contacts co ON co.id=cus.contactid WHERE cus.tourid=$tourid";
$result         = $db->query($query); 

$table_headings = array();
$before_table = '<h4>'.$groupName.'</h4>' ;

?>

<?php echo $before_table; ?>
	<table width="100%" align="center">
		<tr>
            <th style="background-color:#ccc;line-height:34px;">SNo.</th>
            <th style="background-color:#ccc;line-height:34px;">First Name</th>
            <th style="background-color:#ccc;line-height:34px;">Middle Name</th>
            <th style="background-color:#ccc;line-height:34px;">Last Name</th>
            <th style="background-color:#ccc;line-height:34px;">Date of Birth</th>
            <th style="background-color:#ccc;line-height:34px;">Gender</th>
            <th style="background-color:#ccc;line-height:34px;">Citizenship</th>
            <th style="background-color:#ccc;line-height:34px;">Passport #</th>
            <th style="background-color:#ccc;line-height:34px;">Passport Expiry Date</th>
            <th style="background-color:#ccc;line-height:34px;">Purchased separate ticket</th>
            <th style="background-color:#ccc;line-height:34px;">Known Traveller #</th>
            <th style="background-color:#ccc;line-height:34px;">Frequent Flyer</th>
            <th style="background-color:#ccc;line-height:34px;">Airline Special Requests</th>
            <th style="background-color:#ccc;line-height:34px;">Notes</th>
		</tr>
        <?php
        if ($result->num_rows > 0) 
        { 
            $i = 1;
            while ($row=$result->fetch_assoc()) { 

            ?>
                <tr nobr="true" >
                    <td><?php echo $i?></td>
                    <td><?php echo $row["fname"]?></td>
                    <td><?php echo $row["mname"]?></td>
                    <td><?php echo $row["lname"]?></td>
                    <td><?php echo date('m/d/Y', strtotime($row["birthday"]));?></td>
                    <td><?php echo $row["gender"]?></td>
                    <td><?php echo $row["citizenship"]?></td>
                    <td><?php echo $row["passport"]?></td>
                    <td><?php echo date('m/d/Y', strtotime($row["expirydate"]));?></td>
                    <td><?php if($row["hisownticket"] == 1) { echo "Yes"; } else { echo "No"; } ?></td>
                    <td><?php echo $row["travelerid"]?></td>
                    <td><?php echo $row["flyer"]?></td>
                    <td><?php echo $row["specialtext"]?></td>
                    <td><?php echo $row["notes"]?></td>
                </tr>
                <?php
                $i++;
            }
        }
        ?>

	</table>
	<div style="height:100px"></div>
	<div style="text-align:right;">Updated:<?php echo date("m/d/Y"); ?></div>
	
<?php



$pdfData = ob_get_clean() ;


// output the HTML content
$pdf->writeHTML($pdfData, true, false, true, false, '');


// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

$PDF_OUTPUT = isset($PDF_OUTPUT ) ? $PDF_OUTPUT : 'I' ;

if (ob_get_contents()) ob_end_clean();
//Close and output PDF document
return $pdf->Output($report_slug.'.pdf', $PDF_OUTPUT );

//============================================================+
// END OF FILE
//============================================================+
