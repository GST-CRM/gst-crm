<form  id="contact19" name="contact19" method="post" action="inc/group-functions.php">
    <div class="row">
		<div class="form-group col-sm-12">
			<h3>Add A Payment to the Supplier</h3>
		</div>
		<div class="col-sm-4">
			<label class="float-label">Date of Payment</label>
			<input type="text" name="LandSupplierPaymentGroupID" value="<?php echo $tourid; ?>" class="form-control" hidden>
			<input type="text" name="LandSupplierPaymentSuppID" value="<?php echo $LandSupplierDataTotal['id']; ?>" class="form-control" hidden>
			<input type="date" name="LandSupplierPaymentDate" class="form-control" required>
		</div>
		<div class="col-sm-4">
			<label class="float-label">Amount of Payment</label>
			<div class="input-group">
				<span class="input-group-prepend">
					<label class="input-group-text">$</label>
				</span>
				<input name="LandSupplierPaymentAmount" type="text" class="form-control" required>
			</div>
		</div>
		<div class="col-sm-4">
			<label class="float-label">Method of Payment</label>
			<select name="SuppLandMethod" class="form-control">
				<option value="0" disabled> </option>
				<option value="Cash">Cash</option>
				<option value="Wire Transfer">Wire Transfer</option>
				<option value="Credit Card">Credit Card</option>
				<option value="Other">Other</option>
			</select>
		</div>
		<div class="col-sm-12">
			<label class="float-label">Comments / Refrences (What is this payment for?)</label>
			<input type="text" name="LandSupplierPaymentComments" class="form-control" required>
		</div>
		<div class="col-sm-12"><br />
			<button type="submit" name="AddLandPaymentSubmit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Add</button>
			<button type="reset" class="btn waves-effect waves-light btn-inverse" data-dismiss="modal"><i class="fas fa-ban"></i>Cancel</button>
		</div>
	</div>
</form>
