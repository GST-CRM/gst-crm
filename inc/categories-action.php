<?php


if( isset($_REQUEST['action']) && $_REQUEST['action'] == 'deactivate' ) {
    $editId = $_REQUEST['category'] ;
    //void payment
    $sql =  "UPDATE metadata SET Status=0 WHERE id = '$editId' AND catid=1 LIMIT 1" ;
    GDb::execute($sql) ;

    if( GDb::execute($sql) ) {
        GUtils::setSuccess('The category deactivated successfully.') ;
    }
    else {
        GUtils::setWarning('Failed to Deactivate category, Plese try again later' ) ;
    }
    GUtils::redirect($CALLING_PAGE) ;
    return;
}
else if( isset($_REQUEST['action']) && $_REQUEST['action'] == 'activate' ) {
    $editId = $_REQUEST['category'] ;
    //void payment
    $sql =  "UPDATE metadata SET Status=1 WHERE id = '$editId' AND catid=1 LIMIT 1" ;
    if( GDb::execute($sql) ) {
        GUtils::setSuccess('The category activated successfully') ;
    }
    else {
        GUtils::setWarning('Failed to Activate category, Plese try again later' ) ;
    }

    GUtils::redirect($CALLING_PAGE) ;
    return;
}