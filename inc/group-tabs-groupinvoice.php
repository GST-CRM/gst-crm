<?php

include_once "inc/helpers.php";

include_once __DIR__ . '/../models/customer_invoice.php' ;
include_once __DIR__ . '/../models/customer_refund.php' ;

$CALLING_PAGE = 'groups-edit.php?tab=invoice&id=' . $tourid . '&action=edit' ;

include __DIR__ . '/sales-invoice-query.php' ;


?>

<form action="sales-invoices.php" method="POST" name="saleInvoiceListForm" class="row">
    <?php
    include __DIR__ . '/sales-invoice-table.php' ;
    ?>
    
</form>

<?php include 'inc/invoice-table-view.php'; ?>