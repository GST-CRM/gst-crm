<?php
    $custId = $__REQUEST['id'] ;
    if( is_array($custId) ) {
        $custId = reset($custId) ;
    }
    
    //find customer details
    $sql = "SELECT c.id,fname,mname,lname,address1, address2, zipcode, city, state, country, email, "
        . " SUM(Invoice_Amount) as due, SUM(Customer_Payment_Amount) as paid, tourid FROM contacts c"
        . " INNER JOIN customer_account ca ON ca.contactid = c.id "
        . " LEFT JOIN customer_invoice ci ON ci.Customer_Account_Customer_ID = ca.contactid "
        . " LEFT JOIN customer_payments cp ON cp.Customer_Invoice_Num = ci.Customer_Invoice_Num "
        . " WHERE c.id='$custId' GROUP BY c.id " ;
    
    $result = $db->query($sql);

    $data = $result->fetch_assoc() ;
    
    $ret = [] ;
    if( is_array($data) ) {
        $ret['id'] = $data['id'] ;
        $ret['name'] = $data['fname'] . ' ' . $data['mname'] . ' ' . $data['lname'] ;
        $ret['email'] = $data['email'] ;
        $ret['balance'] = $data['due'] - $data['paid'] ;
        $tourid = $data['tourid'] ;
        
        $sqlt = "SELECT DATE_SUB(startdate, INTERVAL 90 DAY) as due_date FROM groups WHERE tourid='$tourid'" ;
        $resulttour = $db->query($sqlt);
        if( $resulttour ) {
            $tourdata = $resulttour->fetch_assoc() ;
            $ret['due_date'] = $tourdata['due_date'] ;
        }
        else {
            $ret['due_date'] = '' ;
        }

        $ret['address'] = GUtils::buildGSTAddress( false, $data, ",", "\n" ) ;
    }
    
    //json response wiht customer data.
    GUtils::jsonResponse($ret) ;