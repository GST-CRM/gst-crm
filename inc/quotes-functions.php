<?php
include "../session.php";


//Create a new quote
if (isset($_POST['Group_Name'])) {
	$Group_Name 				= mysqli_real_escape_string($db, $_POST["Group_Name"]);
	$Proposal_Date 				= mysqli_real_escape_string($db, $_POST["Proposal_Date"]);
	$Start_Date 				= mysqli_real_escape_string($db, $_POST["Start_Date"]);
	$End_Date 					= mysqli_real_escape_string($db, $_POST["End_Date"]);
	$Group_Price 				= mysqli_real_escape_string($db, $_POST["Group_Price"]);
	$Single_Supplement 				= mysqli_real_escape_string($db, $_POST["Single_Supplement"]);
	$Departure_City 			= mysqli_real_escape_string($db, $_POST["Departure_City"]);
	$TourleaderID 				= mysqli_real_escape_string($db, $_POST["TourleaderID"]);
	$Church_Name 				= mysqli_real_escape_string($db, $_POST["Church_Name"]);
	$Itinerary_Title 			= mysqli_real_escape_string($db, $_POST["Itinerary_Title"]);

	if( !empty($Group_Name) OR !empty($Proposal_Date) OR !empty($Start_Date) OR !empty($End_Date) OR !empty($Group_Price) OR !empty($Single_Supplement) OR !empty($Departure_City) OR !empty($TourleaderID) OR !empty($Church_Name) OR !empty($Itinerary_Title)) {
		$MustFill = 0;
	} else {
		$MustFill = 1;
	}
	
	if ($MustFill == 0) {
		$sql = "INSERT INTO quotes(Group_Name, Proposal_Date, Start_Date, End_Date, Group_Price, Single_Supplement, Departure_City, TourleaderID, Church_Name, Itinerary_Title, Quote_Status)
				VALUES ('$Group_Name','$Proposal_Date','$Start_Date','$End_Date','$Group_Price','$Single_Supplement','$Departure_City','$TourleaderID','$Church_Name','$Itinerary_Title',1);";
		$sql .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','quote','0','created','$Group_Name');";

		if ($db->multi_query($sql) === TRUE) {
			echo "<h3 class='text-success'>Success</h3>The quote for <b>$Group_Name</b> has been created successfully! The page will be refreshed in seconds.";
			echo "<meta http-equiv='refresh' content='2;quotes.php'>";
		} else {
			echo $sql . "<br>" . $db->error."<br> error";
		}
	} else {
		echo "<h3 class='text-danger'>Error</h3>Some fields aren't filled yet!";
	}
	$db->close();
}



//Edit a quote
if (isset($_POST['Quote_ID'])) {
	$Quote_ID 				= mysqli_real_escape_string($db, $_POST["Quote_ID"]);
	$Group_Name 				= mysqli_real_escape_string($db, $_POST["Current_Group_Name"]);
	$Proposal_Date 				= mysqli_real_escape_string($db, $_POST["Proposal_Date"]);
	$Start_Date 				= mysqli_real_escape_string($db, $_POST["Start_Date"]);
	$End_Date 					= mysqli_real_escape_string($db, $_POST["End_Date"]);
	$Group_Price 				= mysqli_real_escape_string($db, $_POST["Group_Price"]);
	$Single_Supplement 				= mysqli_real_escape_string($db, $_POST["Single_Supplement"]);
	$Departure_City 			= mysqli_real_escape_string($db, $_POST["Departure_City"]);
	$TourleaderID 				= mysqli_real_escape_string($db, $_POST["TourleaderID"]);
	$Church_Name 				= mysqli_real_escape_string($db, $_POST["Church_Name"]);
	$Itinerary_Title 			= mysqli_real_escape_string($db, $_POST["Itinerary_Title"]);
	$Quote_Status 			= mysqli_real_escape_string($db, $_POST["Quote_Status"]);

	if( !empty($Group_Name) OR !empty($Proposal_Date) OR !empty($Start_Date) OR !empty($End_Date) OR !empty($Group_Price) OR !empty($Single_Supplement) OR !empty($Departure_City) OR !empty($TourleaderID) OR !empty($Church_Name) OR !empty($Itinerary_Title)) {
		$MustFill = 0;
	} else {
		$MustFill = 1;
	}
	
	if ($MustFill == 0) {
		$sql = "UPDATE quotes SET Group_Name='$Group_Name',Proposal_Date='$Proposal_Date',Start_Date='$Start_Date',End_Date='$End_Date',Group_Price='$Group_Price',Single_Supplement='$Single_Supplement',Departure_City='$Departure_City',TourleaderID='$TourleaderID',Church_Name='$Church_Name',Itinerary_Title='$Itinerary_Title',Quote_Status='$Quote_Status' WHERE Quote_ID=$Quote_ID;";
		$sql .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','quote','$Quote_ID','updated','$Group_Name');";

		if ($db->multi_query($sql) === TRUE) {
			echo "<h3 class='text-success'>Success</h3>The quote for <b>$Group_Name</b> has been updated successfully! The page will be refreshed in seconds.";
			echo "<meta http-equiv='refresh' content='2;quotes-edit.php?id=$Quote_ID&action=edit'>";
		} else {
			echo $sql . "<br>" . $db->error."<br> error";
		}
	} else {
		echo "<h3 class='text-danger'>Error</h3>Some fields aren't filled yet!";
	}
	$db->close();
}

?>
