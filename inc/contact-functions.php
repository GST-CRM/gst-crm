<?php
include __DIR__ . "/../session.php";
include_once __DIR__ . '/../models/agents.php' ;
include_once __DIR__ . '/../models/users.php' ;
include_once __DIR__ . '/../models/contacts.php' ;
include_once __DIR__ . '/../models/group_leaders.php' ;
include_once __DIR__ . '/../models/customer_invoice.php' ;

	
//Update the data of the Supplier
if(isset($_POST['suppliercode'])){
	$theid = $_POST["contactidz"];
	$fullname = $_POST["fullname"];
	if ($_POST["supplierstatus"]==1){$status = "1";} else {$status = "0";}
	$supplierdesc = $_POST["supplierdesc"];
	if($_POST['newsuppliertype']!=null){
		$type = $_POST["newsuppliertype"];
	} else {
		$type = $_POST["suppliertype"];
	}
	if($_POST['newpaymentterms']!=null){
		$paymentterms = $_POST["newpaymentterms"];
	} else {
		$paymentterms = $_POST["paymentterms"];
	}
	$accountnumber = $_POST["accountnumber"];
	$bankaccount = $_POST["bankaccount"];
	$bankrouting = $_POST["bankrouting"];
	$supplierid = $supplierdata["id"];
	//if ($_POST["status"]==1){$status = "1";} else {$status = "0";}

	$con10 = new mysqli($servername, $username, $password, $dbname);
	if ($con10->connect_error) {die("Connection failed: " . $con10->connect_error);} 
	$sql10 = "UPDATE suppliers SET type='$type', description='$supplierdesc', Account_number='$accountnumber', Bank_Account_number='$bankaccount', Bank_Routing_number='$bankrouting', payment_terms='$paymentterms', status='$status', updatetime=NOW() WHERE contactid=$theid;";
	$sql10 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','supplier','$theid','edited','$fullname');";
	if($_POST['newsuppliertype']!=null){
		$meta = $_POST['newsuppliertype'];
		$sql10 .= "INSERT INTO metadata (catid, catname, meta) VALUES ('2','Supplier Type','$meta');";
	}
	if($_POST['newpaymentterms']!=null){
		$meta = $_POST['newpaymentterms'];
		$sql10 .= "INSERT INTO metadata (catid, catname, meta) VALUES ('3','Supplier Payment Type','$meta');";
	}
if ($con10->multi_query($sql10) === TRUE) {
    
    enableLogin($theid, $__POST['supplierlogin'], AclRole::$SUPPLIER) ;

	echo "<h3>Success</h3> The supplier $fullname has been updated successfully!";
} else {
	echo $sql10 . "<br>" . $con10->error;
}
$con10->close();
}


//Update the data of the Agent
if(isset($_POST['agentcode'])){
	$theid = mysqli_real_escape_string($db, $_POST['contactcode']);
	$fullname = mysqli_real_escape_string($db, $_POST["fullname"]);
	if ($_POST["agentstatus"]==1){$status = "1";} else {$status = "0";}
	$agentdesc = mysqli_real_escape_string($db, $_POST["agentdesc"]);
	$incometype = mysqli_real_escape_string($db, $_POST["incometype"]);
    
    //include Commission details too.
    $updateCondition = '' ;
    if( $incometype == 'Commission' ) {
        if( isset($_POST["commissiontype"]) ) {
            $commissionType = mysqli_real_escape_string($db, $_POST["commissiontype"]);
            $commissionValue = mysqli_real_escape_string($db, $_POST["commissionvalue"]);

            $updateCondition = " commissiontype='$commissionType', commissionvalue='$commissionValue', "; 
        }
    }
	
	//Warning Message
	$Warning = NULL;

	$sql7 = "UPDATE agents SET $updateCondition description='$agentdesc', incometype='$incometype', status='$status', updatetime=NOW() WHERE contactid=$theid;";
	$sql7 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','agent','$theid','edited','$fullname');";
    
        $db->multi_query($sql7) ;
        Model::flush() ;

        $theid = $__POST["contactcode"];
        $status = mysqli_real_escape_string($db, $__POST['agentlogin']);
        $desc = mysqli_real_escape_string($db, $__POST["leaderdesc"]);

        //Nithin
        //Agent login enabled & password set
        enableLogin($theid, $__POST['agentlogin'], AclRole::$AGENT) ;
        
        echo "<h3>Success</h3> The agent has been updated successfully!";
        return;
	}

//Update the data of the Contact
if(isset($_POST['contactupdateid'])){
	$theid = $_POST["contactupdateid"];
	$fullname = mysqli_real_escape_string($db, $_POST["fullname"]);
	$title = mysqli_real_escape_string($db, $_POST["title"]);
	$fname = mysqli_real_escape_string($db, $_POST["FirstName"]);
	$mname = mysqli_real_escape_string($db, $_POST["MiddleName"]);
	$lname = mysqli_real_escape_string($db, $_POST["LastName"]);
	$email = mysqli_real_escape_string($db, $_POST["email"]);
	$secondary_email = mysqli_real_escape_string($db, $_POST["secondaryemail"]);
	$phone = mysqli_real_escape_string($db, $_POST["homephone"]);
	$businessphone = mysqli_real_escape_string($db, $_POST["businessphone"]);
	$fax = mysqli_real_escape_string($db, $_POST["fax"]);
	$mobile = mysqli_real_escape_string($db, $_POST["mobile"]);
	$company = mysqli_real_escape_string($db, $_POST["company"]);
	$jobtitle = mysqli_real_escape_string($db, $_POST["jobtitle"]);
	$website = mysqli_real_escape_string($db, $_POST["website"]);
	$emergencyname = mysqli_real_escape_string($db,$_POST["emergencyname"]);
	$emergencyrelation = mysqli_real_escape_string($db,$_POST["emergencyrelation"]);
	$emergencyphone = mysqli_real_escape_string($db,$_POST["emergencyphone"]);
	$emergency2name = mysqli_real_escape_string($db,$_POST["emergency2name"]);
	$emergency2relation = mysqli_real_escape_string($db,$_POST["emergency2relation"]);
	$emergency2phone = mysqli_real_escape_string($db,$_POST["emergency2phone"]);
	$address1 = mysqli_real_escape_string($db, $_POST["address1"]);
	$address2 = mysqli_real_escape_string($db, $_POST["address2"]);
	$zipcode = mysqli_real_escape_string($db, $_POST["zipcode"]);
	$city = mysqli_real_escape_string($db, $_POST["city"]);
	$state = mysqli_real_escape_string($db, $_POST["state"]);
	$country = mysqli_real_escape_string($db, $_POST["country"]);
	if ($_POST["WorkAddressButton"]==1){$Work_enabled = "1";} else {$Work_enabled = "0";}
	if ($Work_enabled == 1) {
		$Work_address1 = mysqli_real_escape_string($db, $_POST["Work_address1"]);
		$Work_address2 = mysqli_real_escape_string($db, $_POST["Work_address2"]);
		$Work_zipcode = mysqli_real_escape_string($db, $_POST["Work_zipcode"]);
		$Work_city = mysqli_real_escape_string($db, $_POST["Work_city"]);
		$Work_state = mysqli_real_escape_string($db, $_POST["Work_state"]);
		$Work_country = mysqli_real_escape_string($db, $_POST["Work_country"]);
	} else {
		$Work_address1 = $Work_address2 = $Work_zipcode = $Work_city = $Work_state = $Work_country = NULL;
	}
	$language = mysqli_real_escape_string($db, $_POST["language"]);
	
	if($_POST['newaffliationtype'] == null){
		$affiliation = mysqli_real_escape_string($db, $_POST["affiliation"]);
	} else {
        $New_affiliation_Text = mysqli_real_escape_string($db, $_POST["newaffliationtype"]);
        $New_affiliation_SQL = "INSERT INTO metadata (catid, catname, meta) VALUES ('1','Contact Category','$New_affiliation_Text');";
        GDb::execute($New_affiliation_SQL);
		$affiliation = mysqli_insert_id($db);
	}
	if($_POST['NewDenomination'] == null){
		$Denomination = mysqli_real_escape_string($db, $_POST["Denomination"]);
	} else {
        $New_Denomination_Text = mysqli_real_escape_string($db, $_POST["NewDenomination"]);
        $New_Denomination_SQL = "INSERT INTO metadata (catid, catname, meta) VALUES ('4','Denomination Name','$New_Denomination_Text');";
        GDb::execute($New_Denomination_SQL);
		$Denomination = mysqli_insert_id($db);
	}
	
	$preferredmethod = mysqli_real_escape_string($db, $_POST["preferredmethod"]);
	$notes = mysqli_real_escape_string($db, $_POST["notes"]);

	if($email=="" OR $fname=="" OR $lname=="" OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$missingfields = "";
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {$missingfields=$missingfields."<label class='MissingFieldWarning'>The Email format</label>";}
		if($email=="") {$missingfields=$missingfields."<label class='MissingFieldWarning'>The Email field</label>";}
		if($fname=="") {$missingfields=$missingfields."<label class='MissingFieldWarning'>The First name field</label>";}
		if($lname=="") {$missingfields=$missingfields."<label class='MissingFieldWarning'>The Last name field</label>";}
		
		echo "The following fields are mandatory:<br />".$missingfields;
	} else {
		$sql = "UPDATE contacts SET title='$title', fname='$fname', mname='$mname', lname='$lname', email='$email',secondary_email='$secondary_email', phone='$phone', businessphone='$businessphone', fax='$fax', mobile='$mobile', company='$company', jobtitle='$jobtitle', website='$website', emergencyname='$emergencyname',emergencyrelation='$emergencyrelation',emergencyphone='$emergencyphone',emergency2name='$emergency2name',emergency2relation='$emergency2relation',emergency2phone='$emergency2phone', address1='$address1', address2='$address2', zipcode='$zipcode', city='$city', state='$state', country='$country',Work_enabled='$Work_enabled', Work_address1='$Work_address1', Work_address2='$Work_address2', Work_zipcode='$Work_zipcode', Work_city='$Work_city', Work_state='$Work_state', Work_country='$Work_country', language='$language', affiliation='$affiliation', Denomination='$Denomination', preferredmethod='$preferredmethod',notes='$notes', updatetime=NOW() WHERE id=$theid;";
		$sql .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','contact','$theid','edited','$fullname');";

		if ($db->multi_query($sql) === TRUE) {
		echo "<h3>Success</h3> The contact $fullname has been updated successfully!";
		} else {
		echo $sql . "<br>" . $db->error;
		}
		$db->close();
	}
}

//Create a new contact
if (isset($_POST['newcontact']))
{
	$response 			= array();
	$response['error'] 	= 0;
	$response['url'] 	= "";
	$title 				= mysqli_real_escape_string($db, $_POST["title"]);
	$fname 				= mysqli_real_escape_string($db, $_POST["FirstName"]);
	$mname 				= mysqli_real_escape_string($db, $_POST["MiddleName"]);
	$lname 				= mysqli_real_escape_string($db, $_POST["LastName"]);
	$email 				= mysqli_real_escape_string($db, $_POST["email"]);
	$secondary_email 	= mysqli_real_escape_string($db, $_POST["secondaryemail"]);
	$phone 				= mysqli_real_escape_string($db, $_POST["homephone"]);
	$businessphone 		= mysqli_real_escape_string($db, $_POST["businessphone"]);
	$fax 				= mysqli_real_escape_string($db, $_POST["fax"]);
	$mobile 			= mysqli_real_escape_string($db, $_POST["mobile"]);
	$company 			= mysqli_real_escape_string($db, $_POST["company"]);
	$jobtitle 			= mysqli_real_escape_string($db, $_POST["jobtitle"]);
	$website 			= mysqli_real_escape_string($db, $_POST["website"]);
	$address1 			= mysqli_real_escape_string($db, $_POST["address1"]);
	$address2 			= mysqli_real_escape_string($db, $_POST["address2"]);
	$zipcode 			= mysqli_real_escape_string($db, $_POST["zipcode"]);
	$city 				= mysqli_real_escape_string($db, $_POST["city"]);
	$state 				= mysqli_real_escape_string($db, $_POST["state"]);
	$country 			= mysqli_real_escape_string($db, $_POST["country"]);
	$language 			= mysqli_real_escape_string($db, $_POST["language"]);

	if ($_POST['newaffliationtype'] == null) {
		$affiliation 	= $_POST["affiliation"];
	} else {
		$meta = $_POST['newaffliationtype'];
		$AddNewCategoryMeta = "INSERT INTO metadata (catid, catname, meta) VALUES ('1','Contact Category','$meta');";
		GDb::execute($AddNewCategoryMeta);
		$affiliation = mysqli_insert_id($db);
	}
	$preferredmethod 	= $_POST["preferredmethod"];
	$notes 			 	= mysqli_real_escape_string($db, $_POST["notes"]);

	if (!empty($fname)) {
		$sqlcheck 		= "SELECT * FROM contacts WHERE(fname = '$fname' AND mname = '$mname' AND lname = '$lname')";
		$result 		= mysqli_query($db,$sqlcheck);
		$row 			= mysqli_fetch_array($result,MYSQLI_ASSOC);
		$count 			= mysqli_num_rows($result);
		if($count == 1) {
			$response['error'] 	 = 1;
			$response['message'] = "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error: This contact name is already recorded on the system!</strong></div>";
		} else {
			$sql = "INSERT INTO contacts (title, fname, mname, lname, email,secondary_email, phone,businessphone, fax, mobile, company, jobtitle, website, address1, address2, zipcode, city, state, country, language, affiliation, preferredmethod, notes)
			VALUES ('$title','$fname','$mname','$lname','$email','$secondary_email','$phone','$businessphone','$fax','$mobile','$company','$jobtitle','$website','$address1','$address2','$zipcode','$city','$state','$country','$language','$affiliation','$preferredmethod','$notes');";
			$sql .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','contact','0','created','$fname $mname $lname');";

			if ($db->multi_query($sql) === TRUE) {
				$last_id = $db->insert_id;
				$_SESSION['flash-message'] 		= "The contact $fname $mname $lname has been created successfully!";
				$_SESSION['flash-message-type'] = "success";
				if ($_POST['addcontact'] == "save_new") {
					$response['url'] 			= GUtils::domainUrl("contacts-add.php");
				} else if ($_POST['addcontact'] == "save_only") {
					$response['url'] 			= GUtils::domainUrl("contacts-edit.php?id=$last_id&action=edit");
				} else if ($_POST['addcontact'] == "save_close") {
					$response['url'] 			= GUtils::domainUrl("contacts.php");
				}
				//echo "The contact $fname $mname $lname has been created successfully! <a style='font-weight: bold;' href='contacts-edit.php?id=$last_id&action=edit'>Click here to edit the added contact.</a>";
			} else {
				$response['error'] 	 			= 1;
				$_SESSION['flash-message'] 		= $sql . "<br>" . $db->error."<br> error";
				$_SESSION['flash-message-type'] = "warning";
				//echo $sql . "<br>" . $db->error."<br> error";
			}
		}
		$db->close();
		echo json_encode($response);
		exit;
	}
}
	
//Update the basic data of the Customer Account on Contacts Table
if(isset($_POST['CustomerAccountBasicDataID'])){
	$CustomerAccountBasicDataID = mysqli_real_escape_string($db,$_POST["CustomerAccountBasicDataID"]);
	$CustomerAccountBasicDataName = mysqli_real_escape_string($db,$_POST["CustomerAccountBasicDataName"]);
	$passport = mysqli_real_escape_string($db,$_POST["passport"]);
	$expirydate = mysqli_real_escape_string($db,$_POST["expirydate"]);
	$citizenship = mysqli_real_escape_string($db,$_POST["citizenship"]);
	$badgename = mysqli_real_escape_string($db,$_POST["badgename"]);
	$birthday = mysqli_real_escape_string($db,$_POST["birthday"]);
	$martialstatus = mysqli_real_escape_string($db,$_POST["martialstatus"]);
	$gender = mysqli_real_escape_string($db,$_POST["gender"]);
	$payment = mysqli_real_escape_string($db,$_POST["paymentterm"]);
	$travelerid = mysqli_real_escape_string($db,$_POST["travelerid"]);
	$flyer = mysqli_real_escape_string($db,$_POST["flyer"]);
	$missinginfo = mysqli_real_escape_string($db,$_POST["missinginfo"]);
	$customerLogin = mysqli_real_escape_string($db,$_POST["customerlogin"]);
        
    //Nithin
    //Agent login enabled & password set
    enableLogin($CustomerAccountBasicDataID, $__POST['customerlogin'], AclRole::$CUSTOMER) ;

	
	$SQLcommand = "UPDATE contacts SET passport='$passport',expirydate='$expirydate',citizenship='$citizenship',badgename='$badgename',birthday='$birthday',martialstatus='$martialstatus',gender='$gender',payment='$payment',travelerid='$travelerid',flyer='$flyer',missinginfo='$missinginfo' WHERE id=$CustomerAccountBasicDataID;";
	$SQLcommand .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','contact','$CustomerAccountBasicDataID','edited','$CustomerAccountBasicDataName');";
	
	if ($db->multi_query($SQLcommand) === TRUE) {
		echo "<h3>Success</h3> The customer account basic data ($CustomerAccountBasicDataName) has been updated successfully! The page will be refreshed in seconds.";
		echo "<meta http-equiv='refresh' content='2;contacts-edit.php?id=$CustomerAccountBasicDataID&action=edit&usertype=customer'>";
	} else {
		echo $SQLcommand . "<br>" . $db->error;
	}
}


//Update the data of the Customer Account xxxxxx
if(isset($_POST['CustomerLoopID'])){
	$theid = mysqli_real_escape_string($db,$_POST['CustomerLoopID']);
	$theaccountname = mysqli_real_escape_string($db,$_POST['CustomerLoopName']);
	$thetour = mysqli_real_escape_string($db,$_POST['CustomerTourID']);
	$SelectedTour = mysqli_real_escape_string($db,$_POST['CustomerTourID']);
	$status = mysqli_real_escape_string($db,$_POST["status"]);
	$invoice = 0; //mysqli_real_escape_string($db,$_POST["invoice"])
	$groupinvoice = mysqli_real_escape_string($db,$_POST["GroupInvoice"]);
	$insurance = mysqli_real_escape_string($db,$_POST["insurance"]);
	$hisownticket = mysqli_real_escape_string($db,$_POST["hisownticket"]);
	$Same_Itinerary = mysqli_real_escape_string($db, $_POST["Same_Itinerary"]);
	if($hisownticket == 0) {
		$$Ticket_Number = 0;
		$Confirmation_Number = 0;
		$Apply_Credit = 0;
	} else {
		$Ticket_Number = mysqli_real_escape_string($db, $_POST["Ticket_Number"]);
		$Confirmation_Number = mysqli_real_escape_string($db, $_POST["Confirmation_Number"]);
		$Apply_Credit = mysqli_real_escape_string($db, $_POST["Apply_Credit"]);
	}
	$hisownticket_ID = mysqli_real_escape_string($db,$_POST["hisownticket_ID"]);
	$hisownticket_price = mysqli_real_escape_string($db,$_POST["hisownticket_price"]);
	$hisownticket_Name = mysqli_real_escape_string($db,$_POST["hisownticket_Name"]);
	$hisownticket_Supplier = mysqli_real_escape_string($db,$_POST["hisownticket_Supplier"]);
	$upgrade = mysqli_real_escape_string($db,$_POST["upgrade"]);
	$extension = mysqli_real_escape_string($db,$_POST["extension"]);
	$transfer = mysqli_real_escape_string($db,$_POST["transfer"]);
	$invoiced_customer = mysqli_real_escape_string($db, $_POST["invoiced_customer"]);
	$invoiced_customer = !(empty($invoiced_customer))?$invoiced_customer:0;

	
	/*==================== PURCHASE ORDER CHECKS ====================*/
	//To get the group purchase order for the group
	$SelectGPOsql = "SELECT Group_PO_ID FROM group_purchase_order WHERE Group_Group_ID=$thetour";
	$SelectGPOResult = $db->query($SelectGPOsql);
	$SelectGPOCheck = mysqli_num_rows($SelectGPOResult);
	if($SelectGPOCheck > 0) { //If there is a purchase order for this customer, get it
		$CustomerGPOline = $SelectGPOResult->fetch_assoc();
		$PurchaseOrderNumID = $CustomerGPOline["Group_PO_ID"];
	} else { //To create a new group purchase order for the customer in this group id
		$CreateGPOconn = new mysqli($servername, $username, $password, $dbname);
		if ($CreateGPOconn->connect_error) {die("Connection failed: " . $CreateGPOconn->connect_error);} 
		$CreateGPOsql = "INSERT INTO `group_purchase_order`(`Group_Group_ID`, `PO_Date`) VALUES ($thetour,NOW());";
		$CreateGPOconn->query($CreateGPOsql);
		$PurchaseOrderNumID = $CreateGPOconn->insert_id;
		$CreateGPOconn->close();
	}
	
	//To see if there is already a Purchase Order Line for this Single Supplement Section
	$CheckSinglePurchaseOrderLineSQL = "SELECT * FROM group_po_line WHERE Group_PO_ID=$PurchaseOrderNumID AND Line_Type='Single Supplement' AND Customer_ID=$theid";
	$CheckSinglePurchaseOrderLineResult = $db->query($CheckSinglePurchaseOrderLineSQL);
	$Single_PO_Count = mysqli_num_rows($CheckSinglePurchaseOrderLineResult);
	if($Single_PO_Count > 0) {$Single_PO_Line = $CheckSinglePurchaseOrderLineResult->fetch_assoc();}

	//To see if there is already a Purchase Order Line for this AIR Upgrade Section
	$CheckAirPoLineSQL = "SELECT * FROM group_po_line WHERE Group_PO_ID=$PurchaseOrderNumID AND Line_Type='Air Upgrade' AND Customer_ID=$theid";
	$CheckAirPoLineResult = $db->query($CheckAirPoLineSQL);
	$AirUpgrade_PO_Count = mysqli_num_rows($CheckAirPoLineResult);

	//To see if there is already a Purchase Order Line for this LAND Upgrade Section
	$CheckLandPoLineSQL = "SELECT * FROM group_po_line WHERE Group_PO_ID=$PurchaseOrderNumID AND Line_Type='Land Upgrade' AND Customer_ID=$theid";
	$CheckLandPoLineResult = $db->query($CheckLandPoLineSQL);
	$LandUpgrade_PO_Count = mysqli_num_rows($CheckLandPoLineResult);
	
	/*==================== SALES ORDER CHECKS ====================*/
	//To see the Sales Order Number ID for this customer
	$SelectSalesOrderNumSQL = "SELECT Sales_Order_Num FROM sales_order WHERE Customer_Account_Customer_ID=$theid AND Group_ID=$thetour";
	$SelectSalesOrderNumResult = $db->query($SelectSalesOrderNumSQL);
	$SalesOrderCheck = mysqli_num_rows($SelectSalesOrderNumResult);
	if($SalesOrderCheck > 0) { //To get the sales order for the customer in this group id
		$CustomerOrderLine = $SelectSalesOrderNumResult->fetch_assoc();
		$SalesOrderNum = $CustomerOrderLine["Sales_Order_Num"];
	} else { //To create a new sales order for the customer in this group id
		$CreateSalesOrderConn = new mysqli($servername, $username, $password, $dbname);
		if ($CreateSalesOrderConn->connect_error) {die("Connection failed: " . $CreateSalesOrderConn->connect_error);} 
		$CusInv_New = "INSERT INTO `sales_order`(`Group_ID`, `Customer_Account_Customer_ID`, `Sales_Order_Date`) VALUES ($thetour,$theid,NOW());";
		$CreateSalesOrderConn->query($CusInv_New);
		$SalesOrderNum = $CreateSalesOrderConn->insert_id;
		$CreateSalesOrderConn->close();
	}
		

	//To see if there is already a Sales Order Line for this Ticket Section
	$CheckTicketSalesOrderLineSQL = "SELECT * FROM `sales_order_line` WHERE Sales_Order_Num=$SalesOrderNum AND Line_Type='Ticket Discount'";
	$CheckTicketSalesOrderLineResult = $db->query($CheckTicketSalesOrderLineSQL);
	$TicketCount = mysqli_num_rows($CheckTicketSalesOrderLineResult);
	if($TicketCount > 0) {
		$SalesOrderLineCheck = $CheckTicketSalesOrderLineResult->fetch_assoc();
	}

	//To see if there is already a Sales Order Line for this Single Supplement Section
	$CheckSingleSalesOrderLineSQL = "SELECT * FROM `sales_order_line` WHERE Sales_Order_Num=$SalesOrderNum AND Line_Type='Single Supplement'";
	$CheckSingleSalesOrderLineResult = $db->query($CheckSingleSalesOrderLineSQL);
	$SalesOrderLineCheck = $CheckSingleSalesOrderLineResult->fetch_assoc();
	$SingleCount = mysqli_num_rows($CheckSingleSalesOrderLineResult);

	//To see if there is already a Sales Order Line for this AIR Upgrade Section
	$conn6 = new mysqli($servername, $username, $password, $dbname);
	if ($conn6->connect_error) {die("Connection failed: " . $conn6->connect_error);} 
	$sql6 = "SELECT * FROM `sales_order_line` WHERE Sales_Order_Num=$SalesOrderNum AND Line_Type='Air Upgrade'";
	$result6 = $conn6->query($sql6);
	$AirUpgradeCount = mysqli_num_rows($result6);
	$conn6->close();

	//To see if there is already a Sales Order Line for this LAND Upgrade Section
	$conn7 = new mysqli($servername, $username, $password, $dbname);
	if ($conn7->connect_error) {die("Connection failed: " . $conn7->connect_error);} 
	$sql7 = "SELECT * FROM `sales_order_line` WHERE Sales_Order_Num=$SalesOrderNum AND Line_Type='Land Upgrade'";
	$result7 = $conn7->query($sql7);
	$LandUpgradeCount = mysqli_num_rows($result7);
	$conn7->close();
	
	/*==================== CUSTOMER INVOICE CHECK ====================*/
	//To create a Customer_Invoice for this customer if it doesn't exist ONLY
	$InvoiceConnCheckDB = new mysqli($servername, $username, $password, $dbname);
	if ($InvoiceConnCheckDB->connect_error) {die("Connection failed: " . $InvoiceConnCheckDB->connect_error);} 
	$InvoiceConnCheckSQL = "SELECT * FROM customer_invoice WHERE Customer_Order_Num=$SalesOrderNum AND Customer_Account_Customer_ID=$theid";
	$InvoiceConnCheckResult = $InvoiceConnCheckDB->query($InvoiceConnCheckSQL);
	if ($InvoiceConnCheckResult->num_rows == 0) {
		$CusInv_New = "INSERT INTO customer_invoice (Customer_Invoice_Date,Customer_Order_Num,Customer_Account_Customer_ID,Group_ID)
		VALUES (NOW(),'$SalesOrderNum','$theid','$SelectedTour');";
		$InvoiceConnCheckDB->query($CusInv_New);
		$CusInv_id = $InvoiceConnCheckDB->insert_id;
	} else {
		$CustomerInvoiceData = $InvoiceConnCheckResult->fetch_assoc();
		$CusInv_id = $CustomerInvoiceData['Customer_Invoice_Num'];
	}
	$InvoiceConnCheckDB->close();


	if($hisownticket == 1 AND $Apply_Credit == 1) {
		$AirTicketProductID = $hisownticket_ID;
		$AirTicketProductName = $hisownticket_Name;
		$AirTicketProductSuppID = $hisownticket_Supplier;
	}
	
	if($upgrade == 1) {
		$theupgrade = mysqli_real_escape_string($db,$_POST["theupgrade"]);
		$the_land_upgrade = mysqli_real_escape_string($db,$_POST["thelandupgrade"]);
		
		//So i can get the supplier ID and the price of the product of Airline Upgrade Products
		$conn = new mysqli($servername, $username, $password, $dbname);
		if ($conn->connect_error) {die("Connection failed: " . $conn->connect_error);} 
		$sql = "SELECT id,supplierid,cost,price,name FROM products WHERE id=$theupgrade";
		$result = $conn->query($sql);
		$AirUpdateCheckCount = $result->num_rows;
		if($AirUpdateCheckCount > 0) {
			$AirLineDetails = $result->fetch_assoc();
			$AirProductID = $AirLineDetails["id"];
			$AirProductSuppID = $AirLineDetails["supplierid"];
			$AirProductPrice = $AirLineDetails["price"];
			$AirProductCost = $AirLineDetails["cost"];
			$AirProductName = $AirLineDetails["name"];
		}
		$conn->close();
		
		//So i can get the supplier ID and the price of the product of Land Upgrade Products
		$conn2 = new mysqli($servername, $username, $password, $dbname);
		if ($conn2->connect_error) {die("Connection failed: " . $conn2->connect_error);} 
		$sql2 = "SELECT id,supplierid,cost,price,name FROM products WHERE id=$the_land_upgrade";
		$result2 = $conn2->query($sql2);
		$LandLineDetailsCount = mysqli_num_rows($result2);
		if($LandLineDetailsCount > 0) {$LandLineDetails = $result2->fetch_assoc();}
		$conn2->close();
		$LandUpProductID = $LandLineDetails["id"];
		$LandUpProductSuppID = $LandLineDetails["supplierid"];
		$LandUpProductPrice = $LandLineDetails["price"];
		$LandUpProductCost = $LandLineDetails["cost"];
		$LandUpProductName = $LandLineDetails["name"];
	} else {
		$theupgrade = 0;
		$the_land_upgrade = 0;
	}
	
	//To get the details of the extension product only if it is enabled in the customer account page
	if($extension == 1) {
		$TheExtensionProduct = mysqli_real_escape_string($db,$_POST["TheExtensionProduct"]);

		$conn = new mysqli($servername, $username, $password, $dbname);
		if ($conn->connect_error) {die("Connection failed: " . $conn->connect_error);} 
		$sql = "SELECT id,supplierid,cost,price,name FROM products WHERE id=$TheExtensionProduct";
		$result = $conn->query($sql);
		$ExtensionCheckCount = $result->num_rows;
		if($ExtensionCheckCount > 0) {
			$ExtensionDetails = $result->fetch_assoc();
			$ExtensionProductID = $ExtensionDetails["id"];
			$ExtensionProductSuppID = $ExtensionDetails["supplierid"];
			$ExtensionProductPrice = $ExtensionDetails["price"];
			$ExtensionProductCost = $ExtensionDetails["cost"];
			$ExtensionProductName = $ExtensionDetails["name"];
		}
		$conn->close();
	} else {
		$TheExtensionProduct = 0;
		$$ExtensionProductID = 0;
	}
	
	//To get the details of the insurance product only if it is enabled in the customer account page
	if($insurance == 1) {
		$TheInsuranceProduct = mysqli_real_escape_string($db,$_POST["TheInsuranceProduct"]);

		$conn = new mysqli($servername, $username, $password, $dbname);
		if ($conn->connect_error) {die("Connection failed: " . $conn->connect_error);} 
		$sql = "SELECT id,supplierid,cost,price,name FROM products WHERE id=$TheInsuranceProduct";
		$result = $conn->query($sql);
		$InsuranceCheckCount = $result->num_rows;
		if($InsuranceCheckCount > 0) {
			$InsuranceDetails = $result->fetch_assoc();
			$InsuranceProductID = $InsuranceDetails["id"];
			$InsuranceProductSuppID = $InsuranceDetails["supplierid"];
			$InsuranceProductPrice = $InsuranceDetails["price"];
			$InsuranceProductCost = $InsuranceDetails["cost"];
			$InsuranceProductName = $InsuranceDetails["name"];
		}
		$conn->close();
	} else {
		$TheInsuranceProduct = 0;
		$$InsuranceProductID = 0;
	}
	
	//To get the details of the transfer product only if it is enabled in the customer account page
	if($transfer == 1 AND $hisownticket == 1) {
		$TheTransferProduct = mysqli_real_escape_string($db,$_POST["TheTransferProduct"]);

		$conn = new mysqli($servername, $username, $password, $dbname);
		if ($conn->connect_error) {die("Connection failed: " . $conn->connect_error);} 
		$sql = "SELECT id,supplierid,cost,price,name FROM products WHERE id=$TheTransferProduct";
		$result = $conn->query($sql);
		$TransferCheckCount = $result->num_rows;
		if($TransferCheckCount > 0) {
			$TransferDetails = $result->fetch_assoc();
			$TransferProductID = $TransferDetails["id"];
			$TransferProductSuppID = $TransferDetails["supplierid"];
			$TransferProductPrice = $TransferDetails["price"];
			$TransferProductCost = $TransferDetails["cost"];
			$TransferProductName = $TransferDetails["name"];
		}
		$conn->close();
	} else {
		$$transfer = 0;
		$TheTransferProduct = 0;
		$$TransferProductID = 0;
	}
	
	$specialrequest = mysqli_real_escape_string($db,$_POST["specialrequest"]);
	if($specialrequest == 1) {
		$specialtext = mysqli_real_escape_string($db,$_POST["specialtext"]);
		$special_land_text = mysqli_real_escape_string($db,$_POST["speciallandtext"]);
	} else {
		$specialtext = null;
		$special_land_text = null;
	}
	
	$MealsAllergiesRequest = mysqli_real_escape_string($db,$_POST["Meals_Allergies"]);
	if($MealsAllergiesRequest == 1) {
		$MealsRequestText = mysqli_real_escape_string($db,$_POST["Meals_Request"]);
		$AllergiesRequestText = mysqli_real_escape_string($db,$_POST["Allergies_Request"]);
	} else {
		$MealsRequestText = null;
		$AllergiesRequestText = null;
	}
	
	$roomtype = mysqli_real_escape_string($db,$_POST["roomtype"]);
	$PrimaryRoomingContact = mysqli_real_escape_string($db,$_POST["PrimaryRoomingContact"]);
	
	//To get the fields of the traveling with (Roommates) contacts
	if($roomtype == "Single") {
		//Update the customer_groups table and update the customer_account roomtype column
		$RoomingListCommands0 = "INSERT INTO customer_groups(Primary_Customer_ID, Group_ID, Type, Additional_Traveler_ID_1, Additional_Traveler_ID_2)
								VALUES ('$theid','$SelectedTour','Rooming',0,0)
								ON DUPLICATE KEY UPDATE 
								Additional_Traveler_ID_1 = 0,
								Additional_Traveler_ID_2 = 0,
								Mod_Date = NOW();";
		GDb::execute($RoomingListCommands0);
	}
		
	if($PrimaryRoomingContact > 0) {
		$travelingwith = mysqli_real_escape_string($db,$_POST["travelingwith"]);
		$travelingwith2 = mysqli_real_escape_string($db,$_POST["travelingwith2"]);
		
		if($travelingwith > 0 AND ($roomtype == 'Double' OR $roomtype == 'Twin' OR $roomtype == 'Triple')) {$RoomingDouble=$travelingwith;}else{$RoomingDouble=0;}
		if($travelingwith2 > 0 AND $roomtype == 'Triple') {$RoomingTriple=$travelingwith2;}else{$RoomingTriple=0;}
		
		//Update the customer_groups table and update the customer_account roomtype column
		$RoomingListCommands = "INSERT INTO customer_groups(Primary_Customer_ID, Group_ID, Type, Additional_Traveler_ID_1, Additional_Traveler_ID_2)
								VALUES ('$PrimaryRoomingContact','$SelectedTour','Rooming','$RoomingDouble','$RoomingTriple')
								ON DUPLICATE KEY UPDATE 
								Additional_Traveler_ID_1 = '$RoomingDouble',
								Additional_Traveler_ID_2 = '$RoomingTriple',
								Mod_Date = NOW();";
		GDb::execute($RoomingListCommands);
		//I want to delete any customer that has a duplicate entry
		$RoomingListCommandsDelete = "DELETE FROM customer_groups WHERE Type='Rooming' AND Additional_Traveler_ID_1='$PrimaryRoomingContact' AND Group_ID='$SelectedTour';";
		$RoomingListCommandsDelete2 = "DELETE FROM customer_groups WHERE Type='Rooming' AND Primary_Customer_ID='$RoomingDouble' AND Group_ID='$SelectedTour';";
		GDb::execute($RoomingListCommandsDelete);
		GDb::execute($RoomingListCommandsDelete2);
		
		//Update the travelers roomtype value in their customer_account entry
		$RoomingListCommands1 = "UPDATE customer_account SET roomtype='$roomtype', travelingwith='$RoomingDouble', travelingwith2='$RoomingTriple', updatetime=NOW() WHERE contactid='$PrimaryRoomingContact' AND tourid='$SelectedTour';";
		GDb::execute($RoomingListCommands1);
		$RoomingListCommands2 = "UPDATE customer_account SET roomtype='$roomtype', travelingwith='$PrimaryRoomingContact', updatetime=NOW() WHERE contactid='$RoomingDouble' AND tourid='$SelectedTour';";
		GDb::execute($RoomingListCommands2);
		$RoomingListCommands3 = "UPDATE customer_account SET roomtype='$roomtype', travelingwith='$PrimaryRoomingContact', travelingwith2='$RoomingDouble', updatetime=NOW() WHERE contactid='$RoomingTriple' AND tourid='$SelectedTour';";
		GDb::execute($RoomingListCommands3);
		if($roomtype != 'Single') {
			//Get the Sales Order Num for the roommate #1
			$SONRoommate1 = "SELECT Sales_Order_Num FROM sales_order WHERE Group_ID='$SelectedTour' AND Customer_Account_Customer_ID='$RoomingDouble';" ;
			$Roommate_SO_Num = GDb::fetchScalar($SONRoommate1) ;
			//Delete the row for Single Supplement in the Sales Order Lines for the roommate #1
			$RemoveSingleRoommate1 = "DELETE FROM sales_order_line WHERE Line_Type='Single Supplement' AND Sales_Order_Num=$Roommate_SO_Num;";
			GDb::execute($RemoveSingleRoommate1);
			//Get the Sales Order Num for the roommate #2
			$SONRoommate2 = "SELECT Sales_Order_Num FROM sales_order WHERE Group_ID='$SelectedTour' AND Customer_Account_Customer_ID='$RoomingTriple';" ;
			$Roommate2_SO_Num = GDb::fetchScalar($SONRoommate1) ;
			//Delete the row for Single Supplement in the Sales Order Lines for the roommate #2
			$RemoveSingleRoommate2 = "DELETE FROM sales_order_line WHERE Line_Type='Single Supplement' AND Sales_Order_Num=$Roommate2_SO_Num;";
			GDb::execute($RemoveSingleRoommate2);
		}
		
		//Delete the row (if exist) in the customer_groups if there are no travelers with the primary contact
		if($travelingwith == 0 AND $travelingwith2 == 0 AND $roomtype != 'Single') {
			$RoomingListCommands4 = "DELETE FROM customer_groups WHERE Primary_Customer_ID=$PrimaryRoomingContact AND Group_ID=$SelectedTour AND Type='Rooming';";
			GDb::execute($RoomingListCommands4);
		}
	}

	if($roomtype == 'Single') {
		//To get the product information for the Single Supplement
		$conn8 = new mysqli($servername, $username, $password, $dbname);
		if ($conn8->connect_error) {die("Connection failed: " . $conn8->connect_error);} 
		$sql8 = "SELECT gro.tourid,pro.id,pro.name,pro.cost,pro.price,pro.supplierid FROM groups gro 
				JOIN products pro2 
				ON gro.land_productid=pro2.id
                JOIN products pro 
                ON pro2.Product_Reference=pro.id
				WHERE gro.tourid=$thetour";
		$result8 = $conn8->query($sql8);
		$SingleProductData = $result8->fetch_assoc();
		$conn8->close();
		$SingleProductID = $SingleProductData["id"];
		$SingleProductName = $SingleProductData["name"];
		$SingleProductSuppID = $SingleProductData["supplierid"];
		$SingleProductPrice = $SingleProductData["price"];
		$SingleProductCost = $SingleProductData["cost"];
	}

	/////////////// GROUP FLIGHT SECTION START //////////////////////
	if($hisownticket == 1 AND $Same_Itinerary == 0) {
		//Group Flight Info Data #1
		$AirlineID1 = mysqli_real_escape_string($db, $_POST["AirlineID1"]);
		if($AirlineID1 != NULL) {
			$GF_Line_ID1 = mysqli_real_escape_string($db, $_POST["GF_Line_ID1"]);
			$AirlineFlightID1 = mysqli_real_escape_string($db, $_POST["AirlineFlightID1"]);
			$DepartureAirportID1 = mysqli_real_escape_string($db, $_POST["DepartureAirportID1"]);
			$DEPtime1 = mysqli_real_escape_string($db, $_POST["DEPtime1"]);
			$ArrivalAirportID1 = mysqli_real_escape_string($db, $_POST["ArrivalAirportID1"]);
			$ARRtime1 = mysqli_real_escape_string($db, $_POST["ARRtime1"]);
			$SupplierTrigger1 = mysqli_real_escape_string($db, $_POST["SupplierTrigger1"]); //0 is not selected, 1 is arriving, 2 is leaving
			if($SupplierTrigger1 == "") { $SupplierTriggerValue = 0; } else { $SupplierTriggerValue = $SupplierTrigger1; }

			//Insert or Update the line for the Group's flight info
			$GroupFlightSQL1 =	"INSERT INTO  groups_flights (Group_ID, Specific_Customer_ID, GF_Line_ID, Flight_Airline_ID, Flight_Airline_Number, Departure_Airport, Departure_Time, Arrival_Airport, Arrival_Time, Supplier_Trigger)
								VALUES ($thetour,$theid,1,'$AirlineID1','$AirlineFlightID1','$DepartureAirportID1','$DEPtime1','$ArrivalAirportID1','$ARRtime1','$SupplierTriggerValue')
								ON DUPLICATE KEY
								UPDATE 
									Flight_Airline_ID = '$AirlineID1',
									Flight_Airline_Number = '$AirlineFlightID1',
									Departure_Airport = '$DepartureAirportID1',
									Departure_Time = '$DEPtime1',
									Arrival_Airport = '$ArrivalAirportID1',
									Arrival_Time = '$ARRtime1',
									Supplier_Trigger = '$SupplierTriggerValue';";
			GDb::execute($GroupFlightSQL1);
		}
		
		//Group Flight Info Data #2
		$AirlineID2 = mysqli_real_escape_string($db, $_POST["AirlineID2"]);
		if($AirlineID2 != NULL) {
			$GF_Line_ID2 = mysqli_real_escape_string($db, $_POST["GF_Line_ID2"]);
			$AirlineFlightID2 = mysqli_real_escape_string($db, $_POST["AirlineFlightID2"]);
			$DepartureAirportID2 = mysqli_real_escape_string($db, $_POST["DepartureAirportID2"]);
			$DEPtime2 = mysqli_real_escape_string($db, $_POST["DEPtime2"]);
			$ArrivalAirportID2 = mysqli_real_escape_string($db, $_POST["ArrivalAirportID2"]);
			$ARRtime2 = mysqli_real_escape_string($db, $_POST["ARRtime2"]);
			$SupplierTrigger2 = mysqli_real_escape_string($db, $_POST["SupplierTrigger2"]); //0 is not selected, 1 is arriving, 2 is leaving
			if($SupplierTrigger2 == "") { $SupplierTriggerValue = 0; } else { $SupplierTriggerValue = $SupplierTrigger2; }

			//Insert or Update the line for the Group's flight info
			$GroupFlightSQL1 =	"INSERT INTO  groups_flights (Group_ID, Specific_Customer_ID, GF_Line_ID, Flight_Airline_ID, Flight_Airline_Number, Departure_Airport, Departure_Time, Arrival_Airport, Arrival_Time, Supplier_Trigger)
								VALUES ($thetour,$theid,2,'$AirlineID2','$AirlineFlightID2','$DepartureAirportID2','$DEPtime2','$ArrivalAirportID2','$ARRtime2','$SupplierTriggerValue')
								ON DUPLICATE KEY
								UPDATE 
									Flight_Airline_ID = '$AirlineID2',
									Flight_Airline_Number = '$AirlineFlightID2',
									Departure_Airport = '$DepartureAirportID2',
									Departure_Time = '$DEPtime2',
									Arrival_Airport = '$ArrivalAirportID2',
									Arrival_Time = '$ARRtime2',
									Supplier_Trigger = '$SupplierTriggerValue';";
			GDb::execute($GroupFlightSQL1);
		}
		
		//Group Flight Info Data #3
		$AirlineID3 = mysqli_real_escape_string($db, $_POST["AirlineID3"]);
		if($AirlineID3 != NULL) {
			$GF_Line_ID3 = mysqli_real_escape_string($db, $_POST["GF_Line_ID3"]);
			$AirlineFlightID3 = mysqli_real_escape_string($db, $_POST["AirlineFlightID3"]);
			$DepartureAirportID3 = mysqli_real_escape_string($db, $_POST["DepartureAirportID3"]);
			$DEPtime3 = mysqli_real_escape_string($db, $_POST["DEPtime3"]);
			$ArrivalAirportID3 = mysqli_real_escape_string($db, $_POST["ArrivalAirportID3"]);
			$ARRtime3 = mysqli_real_escape_string($db, $_POST["ARRtime3"]);
			$SupplierTrigger3 = mysqli_real_escape_string($db, $_POST["SupplierTrigger3"]); //0 is not selected, 1 is arriving, 2 is leaving
			if($SupplierTrigger3 == "") { $SupplierTriggerValue = 0; } else { $SupplierTriggerValue = $SupplierTrigger3; }

			//Insert or Update the line for the Group's flight info
			$GroupFlightSQL3 =	"INSERT INTO  groups_flights (Group_ID, Specific_Customer_ID, GF_Line_ID, Flight_Airline_ID, Flight_Airline_Number, Departure_Airport, Departure_Time, Arrival_Airport, Arrival_Time, Supplier_Trigger)
								VALUES ($thetour,$theid,3,'$AirlineID3','$AirlineFlightID3','$DepartureAirportID3','$DEPtime3','$ArrivalAirportID3','$ARRtime3','$SupplierTriggerValue')
								ON DUPLICATE KEY
								UPDATE 
									Flight_Airline_ID = '$AirlineID3',
									Flight_Airline_Number = '$AirlineFlightID3',
									Departure_Airport = '$DepartureAirportID3',
									Departure_Time = '$DEPtime3',
									Arrival_Airport = '$ArrivalAirportID3',
									Arrival_Time = '$ARRtime3',
									Supplier_Trigger = '$SupplierTriggerValue';";
			GDb::execute($GroupFlightSQL3);
		}
		
		
		//Group Flight Info Data #4
		$AirlineID4 = mysqli_real_escape_string($db, $_POST["AirlineID4"]);
		if($AirlineID4 != NULL) {
			$GF_Line_ID4 = mysqli_real_escape_string($db, $_POST["GF_Line_ID4"]);
			$AirlineFlightID4 = mysqli_real_escape_string($db, $_POST["AirlineFlightID4"]);
			$DepartureAirportID4 = mysqli_real_escape_string($db, $_POST["DepartureAirportID4"]);
			$DEPtime4 = mysqli_real_escape_string($db, $_POST["DEPtime4"]);
			$ArrivalAirportID4 = mysqli_real_escape_string($db, $_POST["ArrivalAirportID4"]);
			$ARRtime4 = mysqli_real_escape_string($db, $_POST["ARRtime4"]);
			$SupplierTrigger4 = mysqli_real_escape_string($db, $_POST["SupplierTrigger4"]); //0 is not selected, 1 is arriving, 2 is leaving
			if($SupplierTrigger4 == "") { $SupplierTriggerValue = 0; } else { $SupplierTriggerValue = $SupplierTrigger4; }

			//Insert or Update the line for the Group's flight info
			$GroupFlightSQL4 =	"INSERT INTO  groups_flights (Group_ID, Specific_Customer_ID, GF_Line_ID, Flight_Airline_ID, Flight_Airline_Number, Departure_Airport, Departure_Time, Arrival_Airport, Arrival_Time, Supplier_Trigger)
								VALUES ($thetour,$theid,4,'$AirlineID4','$AirlineFlightID4','$DepartureAirportID4','$DEPtime4','$ArrivalAirportID4','$ARRtime4','$SupplierTriggerValue')
								ON DUPLICATE KEY
								UPDATE 
									Flight_Airline_ID = '$AirlineID4',
									Flight_Airline_Number = '$AirlineFlightID4',
									Departure_Airport = '$DepartureAirportID4',
									Departure_Time = '$DEPtime4',
									Arrival_Airport = '$ArrivalAirportID4',
									Arrival_Time = '$ARRtime4',
									Supplier_Trigger = '$SupplierTriggerValue';";
			GDb::execute($GroupFlightSQL4);
		}
	}
	/////////////// GROUP FLIGHT SECTION IS DONE //////////////////////

	
	$con12 = new mysqli($servername, $username, $password, $dbname);
	if ($con12->connect_error) {die("Connection failed: " . $con12->connect_error);} 
	if($hisownticket == 1 AND ($hisownticket_price == 0 OR $hisownticket_price == "")) {
		$missingfields = "";
		$missingfields=$missingfields."<label class='MissingFieldWarning'>The Ticket Credit Discount is missing.</label>";
		
		echo "The following fields are mandatory:<br />".$missingfields;
	} else {
	$sql12 = "UPDATE customer_account SET tourid='$SelectedTour',status='$status',invoice='$invoice',groupinvoice='$groupinvoice',insurance='$insurance',Insurance_Product_ID='$InsuranceProductID',hisownticket='$hisownticket',Apply_Credit='$Apply_Credit',Confirmation_Number='$Confirmation_Number',Ticket_Number='$Ticket_Number',Same_Itinerary='$Same_Itinerary',upgrade='$upgrade',upgradeproduct='$theupgrade',upgrade_land_product='$the_land_upgrade',Transfer='$transfer',Transfer_Product_ID='$TransferProductID',Extension='$extension',Extension_Product_ID='$ExtensionProductID',specialrequest='$specialrequest',specialtext='$specialtext',special_land_text='$special_land_text',Meals_Allergies='$MealsAllergiesRequest',Meals_Request='$MealsRequestText',Allergies_Request='$AllergiesRequestText',roomtype='$roomtype',invoiced_customer=$invoiced_customer, updatetime=NOW() WHERE contactid=$theid AND tourid=$SelectedTour;";	


	//Check and updated Airline Main product details for each customer
	$GetAirDetailsConn = new mysqli($servername, $username, $password, $dbname);
	if ($GetAirDetailsConn->connect_error) {die("Connection failed: " . $GetAirDetailsConn->connect_error);} 
	$GetAirDetailsSQL = "SELECT gro.tourid,pro.id,pro.name,pro.cost,pro.price,pro.supplierid FROM groups gro 
			JOIN products pro 
			ON gro.airline_productid=pro.id
			WHERE gro.tourid=$thetour";
	$GetAirDetailsResult = $GetAirDetailsConn->query($GetAirDetailsSQL);
	$MainAirlineDetails = $GetAirDetailsResult->fetch_assoc();
	$AirlineProductID = $MainAirlineDetails["id"];
	$AirlineProductName = $MainAirlineDetails["name"];
	$AirlineProductSuppID = $MainAirlineDetails["supplierid"];
	$AirlineProductPrice = $MainAirlineDetails["price"];
	$AirlineProductCost = $MainAirlineDetails["cost"];
	$GetAirDetailsConn->close();

	$sql12 .= 	"INSERT INTO sales_order_line (Sales_Order_Num, Line_Type, Product_Product_ID, Product_Name, Sales_Amount, Supplier_Supplier_ID)
				VALUES ('$SalesOrderNum','Airline Package','$AirlineProductID','$AirlineProductName','$AirlineProductPrice','$AirlineProductSuppID')
				ON DUPLICATE KEY UPDATE 
					Product_Product_ID = '$AirlineProductID',
					Product_Name = '$AirlineProductName',
					Sales_Amount = '$AirlineProductPrice',
					Supplier_Supplier_ID = '$AirlineProductSuppID';";
					
	$sql12 .= 	"INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
				VALUES ('$CusInv_id','1','1','$AirlineProductID','$AirlineProductName','1','$AirlineProductPrice')
				ON DUPLICATE KEY UPDATE 
					Product_Product_ID = '$AirlineProductID',
					Product_Name = '$AirlineProductName',
					Qty = '1',
					Invoice_Line_Total = '$AirlineProductPrice';";


	//Check and updated Land Main product details for each customer
	$GetLandDetailsConn = new mysqli($servername, $username, $password, $dbname);
	if ($GetLandDetailsConn->connect_error) {die("Connection failed: " . $GetLandDetailsConn->connect_error);} 
	$GetLandDetailsSQL = "SELECT gro.tourid,pro.id,pro.name,pro.cost,pro.price,pro.supplierid FROM groups gro 
			JOIN products pro 
			ON gro.land_productid=pro.id
			WHERE gro.tourid=$thetour";
	$GetLandDetailsResult = $GetLandDetailsConn->query($GetLandDetailsSQL);
	$MainLandDetails = $GetLandDetailsResult->fetch_assoc();
	$GetLandDetailsConn->close();
	$LandProductID = $MainLandDetails["id"];
	$LandProductName = $MainLandDetails["name"];
	$LandProductSuppID = $MainLandDetails["supplierid"];
	$LandProductPrice = $MainLandDetails["price"];
	$LandProductCost = $MainLandDetails["cost"];

	$sql12 .= 	"INSERT INTO sales_order_line (Sales_Order_Num, Line_Type, Product_Product_ID, Product_Name, Sales_Amount, Supplier_Supplier_ID)
				VALUES ('$SalesOrderNum','Land Package','$LandProductID','$LandProductName','$LandProductPrice','$LandProductSuppID')
				ON DUPLICATE KEY UPDATE 
					Product_Product_ID = '$LandProductID',
					Product_Name = '$LandProductName',
					Sales_Amount = '$LandProductPrice',
					Supplier_Supplier_ID = '$LandProductSuppID';";
					
	$sql12 .= 	"INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
				VALUES ('$CusInv_id','2','2','$LandProductID','$LandProductName','1','$LandProductPrice')
				ON DUPLICATE KEY UPDATE 
					Product_Product_ID = '$LandProductID',
					Product_Name = '$LandProductName',
					Qty = '1',
					Invoice_Line_Total = '$LandProductPrice';";


	//Add the ticket amount price in the Sales Order Line Table to discount it
	if($hisownticket == 1 AND $Apply_Credit == 1) {
		$sql12 .= 	"INSERT INTO sales_order_line (Sales_Order_Num, Line_Type, Product_Product_ID, Product_Name, Sales_Amount, Supplier_Supplier_ID)
					VALUES ('$SalesOrderNum','Ticket Discount','$AirTicketProductID','Booking his/her own ticket','$hisownticket_price','$AirTicketProductSuppID')
					ON DUPLICATE KEY UPDATE 
						Product_Product_ID = '$AirTicketProductID',
						Product_Name = 'Booking his/her own ticket',
						Sales_Amount = '$hisownticket_price',
						Supplier_Supplier_ID = '$AirTicketProductSuppID';";
						
		$sql12 .= 	"INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
					VALUES ('$CusInv_id','3','3','$AirTicketProductID','$AirTicketProductName','1','$hisownticket_price')
					ON DUPLICATE KEY UPDATE 
						Product_Product_ID = '$AirTicketProductID',
						Product_Name = '$AirTicketProductName',
						Qty = '1',
						Invoice_Line_Total = '$hisownticket_price';";
	} else {
		$sql12 .= "DELETE FROM sales_order_line WHERE Sales_Order_Num=$SalesOrderNum AND Line_Type='Ticket Discount';";
		$sql12 .= "DELETE FROM customer_invoice_line WHERE Customer_Invoice_Num=$CusInv_id AND Line_Type=3;";
	}
	//Add the Single supplement amount price in the Sales Order Line Table to increase it
	if($roomtype == 'Single') {
		//Commentted the following commands to use the advanced query "ON DUPLICATE KEY UPDATE"
		//if($SingleCount == 0) {
		//	$sql12 .= "INSERT INTO sales_order_line (Sales_Order_Num, Line_Type, Product_Product_ID, Product_Name, Sales_Amount, Supplier_Supplier_ID)
		//		VALUES ('$SalesOrderNum','Single Supplement','$SingleProductID','$SingleProductName','$SingleProductPrice','$SingleProductSuppID');";
		//	$sql12 .= "INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
		//		VALUES ('$CusInv_id','4','4','$SingleProductID','$SingleProductName','1','$SingleProductPrice');";
		//} else {
		//	$sql12 .= "UPDATE sales_order_line SET Product_Product_ID='$SingleProductID',Sales_Amount='$SingleProductPrice', Supplier_Supplier_ID='$SingleProductSuppID', Mod_Date=NOW() WHERE Sales_Order_Num=$SalesOrderNum AND Line_Type='Single Supplement';";
		//	$sql12 .= "UPDATE customer_invoice_line SET Product_Product_ID='$SingleProductID',Invoice_Line_Total='$SingleProductPrice', Mod_Date=NOW() WHERE Customer_Invoice_Num=$CusInv_id AND Line_Type=4;";
		//}
			$sql12 .= 	"INSERT INTO sales_order_line (Sales_Order_Num, Line_Type, Product_Product_ID, Product_Name, Sales_Amount, Supplier_Supplier_ID)
						VALUES ('$SalesOrderNum','Single Supplement','$SingleProductID','$SingleProductName','$SingleProductPrice','$SingleProductSuppID')
						ON DUPLICATE KEY UPDATE 
							Product_Product_ID = '$SingleProductID',
							Product_Name = '$SingleProductName',
							Sales_Amount = '$SingleProductPrice',
							Supplier_Supplier_ID = '$SingleProductSuppID';";
							
			$sql12 .= 	"INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
						VALUES ('$CusInv_id','4','4','$SingleProductID','$SingleProductName','1','$SingleProductPrice')
						ON DUPLICATE KEY UPDATE 
							Product_Product_ID = '$SingleProductID',
							Product_Name = '$SingleProductName',
							Qty = '1',
							Invoice_Line_Total = '$SingleProductPrice';";
	} else {
		$sql12 .= "DELETE FROM sales_order_line WHERE Sales_Order_Num=$SalesOrderNum AND Line_Type='Single Supplement';";
		$sql12 .= "DELETE FROM customer_invoice_line WHERE Customer_Invoice_Num=$CusInv_id AND Line_Type=4;";
	}
	//Add the Single supplement cost in the purchase Order Line Table to increase it
	if($roomtype == 'Single') {
		if($Single_PO_Count == 0) {
			$sql12 .= "INSERT INTO group_po_line (Group_PO_ID, Line_Type, Product_Product_ID, Cost_Amount, Supplier_Supplier_ID, Customer_ID)
			VALUES ('$PurchaseOrderNumID','Single Supplement','$SingleProductID','$SingleProductCost','$SingleProductSuppID','$theid');";
		} else {
			$sql12 .= "UPDATE group_po_line SET Product_Product_ID='$SingleProductID',Cost_Amount='$SingleProductCost', Supplier_Supplier_ID='$SingleProductSuppID', Mod_Date=NOW() WHERE Group_PO_ID=$PurchaseOrderNumID AND Line_Type='Single Supplement' AND Customer_ID=$theid;";
		}
	} else {
		$sql12 .= "DELETE FROM group_po_line WHERE Group_PO_ID=$PurchaseOrderNumID AND Line_Type='Single Supplement' AND Customer_ID=$theid;";
	}
	//Add the upgrade price and row inside the sales_order_line table for Air and Land
	if($upgrade == 1) {
		if($theupgrade > 0) {
			if($AirUpgradeCount == 0 AND $AirUpgrade_PO_Count == 0) {
				$sql12 .= "INSERT INTO sales_order_line (Sales_Order_Num, Line_Type, Product_Product_ID, Product_Name, Sales_Amount, Supplier_Supplier_ID)
				VALUES ('$SalesOrderNum','Air Upgrade','$theupgrade','$AirProductName','$AirProductPrice','$AirProductSuppID');";
				$sql12 .= "INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
				VALUES ('$CusInv_id','5','5','$theupgrade','$AirProductName','1','$AirProductPrice');";
				$sql12 .= "INSERT INTO group_po_line (Group_PO_ID, Line_Type, Product_Product_ID, Cost_Amount, Supplier_Supplier_ID, Customer_ID)
				VALUES ('$PurchaseOrderNumID','Air Upgrade','$theupgrade','$AirProductCost','$AirProductSuppID','$theid');";
			} else {
				$sql12 .= "UPDATE sales_order_line SET Line_Type='Air Upgrade', Product_Product_ID='$theupgrade', Product_Name='$AirProductName', Sales_Amount='$AirProductPrice', Supplier_Supplier_ID='$AirProductSuppID', Mod_Date=NOW() WHERE Sales_Order_Num=$SalesOrderNum AND Line_Type='Air Upgrade';";
				$sql12 .= "UPDATE customer_invoice_line SET Product_Product_ID='$theupgrade',Invoice_Line_Total='$AirProductPrice', Mod_Date=NOW() WHERE Customer_Invoice_Num=$CusInv_id AND Line_Type=5;";
				$sql12 .= "UPDATE group_po_line SET Line_Type='Air Upgrade', Product_Product_ID='$theupgrade', Cost_Amount='$AirProductCost', Supplier_Supplier_ID='$AirProductSuppID', Mod_Date=NOW() WHERE Group_PO_ID=$PurchaseOrderNumID AND Line_Type='Air Upgrade' AND Customer_ID=$theid;";
			}
		}
		if($the_land_upgrade > 0) {
			if($LandUpgradeCount == 0 AND $LandUpgrade_PO_Count == 0) {
				$sql12 .= "INSERT INTO sales_order_line (Sales_Order_Num, Line_Type, Product_Product_ID, Product_Name, Sales_Amount, Supplier_Supplier_ID)
				VALUES ('$SalesOrderNum','Land Upgrade','$the_land_upgrade','$LandUpProductName','$LandUpProductPrice','$LandUpProductSuppID');";
				$sql12 .= "INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
				VALUES ('$CusInv_id','6','6','$the_land_upgrade','$LandUpProductName','1','$LandUpProductPrice');";
				$sql12 .= "INSERT INTO group_po_line (Group_PO_ID, Line_Type, Product_Product_ID, Cost_Amount, Supplier_Supplier_ID,Customer_ID)
				VALUES ('$PurchaseOrderNumID','Land Upgrade','$the_land_upgrade','$LandUpProductCost','$LandUpProductSuppID','$theid');";
			} else {
				$sql12 .= "UPDATE sales_order_line SET Line_Type='Land Upgrade', Product_Product_ID='$the_land_upgrade', Product_Name='$LandUpProductName', Sales_Amount='$LandUpProductPrice', Supplier_Supplier_ID='$LandUpProductSuppID', Mod_Date=NOW() WHERE Sales_Order_Num=$SalesOrderNum AND Line_Type='Land Upgrade';";
				$sql12 .= "UPDATE customer_invoice_line SET Product_Product_ID='$the_land_upgrade',Invoice_Line_Total='$LandUpProductPrice', Mod_Date=NOW() WHERE Customer_Invoice_Num=$CusInv_id AND Line_Type=6;";
				$sql12 .= "UPDATE group_po_line SET Line_Type='Land Upgrade', Product_Product_ID='$the_land_upgrade', Cost_Amount='$LandUpProductCost', Supplier_Supplier_ID='$LandUpProductSuppID', Mod_Date=NOW() WHERE Group_PO_ID=$PurchaseOrderNumID AND Line_Type='Land Upgrade' AND Customer_ID=$theid;";
			}
		}
	} else {
		$sql12 .= "DELETE FROM sales_order_line WHERE Sales_Order_Num=$SalesOrderNum AND Line_Type='Air Upgrade';";
		$sql12 .= "DELETE FROM sales_order_line WHERE Sales_Order_Num=$SalesOrderNum AND Line_Type='Land Upgrade';";
		$sql12 .= "DELETE FROM group_po_line WHERE Group_PO_ID=$PurchaseOrderNumID AND Line_Type='Air Upgrade' AND Customer_ID=$theid;";
		$sql12 .= "DELETE FROM group_po_line WHERE Group_PO_ID=$PurchaseOrderNumID AND Line_Type='Land Upgrade' AND Customer_ID=$theid;";
		$sql12 .= "DELETE FROM customer_invoice_line WHERE Customer_Invoice_Num=$CusInv_id AND Line_Type=5;";
		$sql12 .= "DELETE FROM customer_invoice_line WHERE Customer_Invoice_Num=$CusInv_id AND Line_Type=6;";
	}
	//Add the extension price and row inside the sales_order_line table
	if($extension == 1) {
		if($ExtensionProductID > 0) {
			$sql12 .= 	"INSERT INTO sales_order_line (Sales_Order_Num, Line_Type, Product_Product_ID, Product_Name, Sales_Amount, Supplier_Supplier_ID)
						VALUES ('$SalesOrderNum','Extension','$ExtensionProductID','$ExtensionProductName','$ExtensionProductPrice','$ExtensionProductSuppID')
						ON DUPLICATE KEY UPDATE 
							Product_Product_ID = '$ExtensionProductID',
							Product_Name = '$ExtensionProductName',
							Sales_Amount = '$ExtensionProductPrice',
							Supplier_Supplier_ID = '$ExtensionProductSuppID';";
							
			$sql12 .= 	"INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
						VALUES ('$CusInv_id','8','8','$ExtensionProductID','$ExtensionProductName','1','$ExtensionProductPrice')
						ON DUPLICATE KEY UPDATE 
							Product_Product_ID = '$ExtensionProductID',
							Product_Name = '$ExtensionProductName',
							Qty = '1',
							Invoice_Line_Total = '$ExtensionProductPrice';";
		}
	} else {
		$sql12 .= "DELETE FROM sales_order_line WHERE Sales_Order_Num=$SalesOrderNum AND Line_Type='Extension';";
		$sql12 .= "DELETE FROM customer_invoice_line WHERE Customer_Invoice_Num=$CusInv_id AND Line_Type=8;";
	}
	
	//Add the insurance price and row inside the sales_order_line table
	if($insurance == 1) {
		if($InsuranceProductID > 0) {
			$sql12 .= 	"INSERT INTO sales_order_line (Sales_Order_Num, Line_Type, Product_Product_ID, Product_Name, Sales_Amount, Supplier_Supplier_ID)
						VALUES ('$SalesOrderNum','Insurance','$InsuranceProductID','$InsuranceProductName','$InsuranceProductPrice','$InsuranceProductSuppID')
						ON DUPLICATE KEY UPDATE 
							Product_Product_ID = '$InsuranceProductID',
							Product_Name = '$InsuranceProductName',
							Sales_Amount = '$InsuranceProductPrice',
							Supplier_Supplier_ID = '$InsuranceProductSuppID';";
							
			$sql12 .= 	"INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
						VALUES ('$CusInv_id','10','10','$InsuranceProductID','$InsuranceProductName','1','$InsuranceProductPrice')
						ON DUPLICATE KEY UPDATE 
							Product_Product_ID = '$InsuranceProductID',
							Product_Name = '$InsuranceProductName',
							Qty = '1',
							Invoice_Line_Total = '$InsuranceProductPrice';";
		}
	} else {
		$sql12 .= "DELETE FROM sales_order_line WHERE Sales_Order_Num=$SalesOrderNum AND Line_Type='Insurance';";
		$sql12 .= "DELETE FROM customer_invoice_line WHERE Customer_Invoice_Num=$CusInv_id AND Line_Type=10;";
	}
	
	//Add the transfer price and row inside the sales_order_line table
	if($transfer == 1 AND $hisownticket == 1) {
		if($TransferProductID > 0) {
			$sql12 .= 	"INSERT INTO sales_order_line (Sales_Order_Num, Line_Type, Product_Product_ID, Product_Name, Sales_Amount, Supplier_Supplier_ID)
						VALUES ('$SalesOrderNum','Transfer','$TransferProductID','$TransferProductName','$TransferProductPrice','$TransferProductSuppID')
						ON DUPLICATE KEY UPDATE 
							Product_Product_ID = '$TransferProductID',
							Product_Name = '$TransferProductName',
							Sales_Amount = '$TransferProductPrice',
							Supplier_Supplier_ID = '$TransferProductSuppID';";
							
			$sql12 .= 	"INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
						VALUES ('$CusInv_id','9','9','$TransferProductID','$TransferProductName','1','$TransferProductPrice')
						ON DUPLICATE KEY UPDATE 
							Product_Product_ID = '$TransferProductID',
							Product_Name = '$TransferProductName',
							Qty = '1',
							Invoice_Line_Total = '$TransferProductPrice';";
		}
	} else {
		$sql12 .= "DELETE FROM sales_order_line WHERE Sales_Order_Num=$SalesOrderNum AND Line_Type='Transfer';";
		$sql12 .= "DELETE FROM customer_invoice_line WHERE Customer_Invoice_Num=$CusInv_id AND Line_Type=9;";
	}
	$sql12 .= "UPDATE customer_invoice 
					INNER JOIN (
						SELECT SUM(cil.Qty * cil.Invoice_Line_Total) as Total, Customer_Invoice_Num FROM customer_invoice_line cil
						INNER JOIN products p ON p.id = cil.Product_Product_ID 
						GROUP BY Customer_Invoice_Num 
					) AS A ON A.Customer_Invoice_Num = customer_invoice.Customer_Invoice_Num 
					SET customer_invoice.Invoice_Amount=A.Total
				WHERE customer_invoice.Customer_Invoice_Num=$CusInv_id;";
	$sql12 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','customer account','$theid','updated','$theaccountname');";
	if ($con12->multi_query($sql12) === TRUE) {
	//echo $sql12;
	//I added the Joint Invoice query and all the code here for the sake of collecting all the data first and then executing the codes
	//To check if this customer has a group invoice and create a new one or update it
	if($groupinvoice == 1) {
		$WhereClauseRedo = "";
		$JointInvoiceID_Array = "";
		
		$PrimaryCustomer = mysqli_real_escape_string($db,$_POST["PrimaryCustomer"]);
		if($PrimaryCustomer > 0) {$JointInvoiceID_Array .=$PrimaryCustomer;} else {$PrimaryCustomer=0;}

		$Traveler1 = mysqli_real_escape_string($db,$_POST["Traveler1"]);
		$Traveler1old = mysqli_real_escape_string($db,$_POST["TravelerCurrent1"]);
		if($Traveler1 != $Traveler1old) { $WhereClauseRedo .= "$Traveler1old,"; }
		if($Traveler1 > 0) {$JointInvoiceID_Array .=",".$Traveler1;} else {$Traveler1=0;}
		
		$Traveler2 = mysqli_real_escape_string($db,$_POST["Traveler2"]);
		$Traveler2old = mysqli_real_escape_string($db,$_POST["TravelerCurrent2"]);
		if($Traveler2 != $Traveler2old) { $WhereClauseRedo .= "$Traveler2old,"; }
		if($Traveler2 > 0) {$JointInvoiceID_Array .=",".$Traveler2;} else {$Traveler2=0;}

		$Traveler3 = mysqli_real_escape_string($db,$_POST["Traveler3"]);
		$Traveler3old = mysqli_real_escape_string($db,$_POST["TravelerCurrent3"]);
		if($Traveler3 != $Traveler3old) { $WhereClauseRedo .= "$Traveler3old,"; }
		if($Traveler3 > 0) {$JointInvoiceID_Array .=",".$Traveler3;} else {$Traveler3=0;}

		$Traveler4 = mysqli_real_escape_string($db,$_POST["Traveler4"]);
		$Traveler4old = mysqli_real_escape_string($db,$_POST["TravelerCurrent4"]);
		if($Traveler4 != $Traveler4old) { $WhereClauseRedo .= "$Traveler4old,"; }
		if($Traveler4 > 0) {$JointInvoiceID_Array .=",".$Traveler4;} else {$Traveler4=0;}
		
		$Traveler5 = mysqli_real_escape_string($db,$_POST["Traveler5"]);
		$Traveler5old = mysqli_real_escape_string($db,$_POST["TravelerCurrent5"]);
		if($Traveler5 != $Traveler5old) { $WhereClauseRedo .= "$Traveler5old,"; }
		if($Traveler5 > 0) {$JointInvoiceID_Array .=",".$Traveler5;} else {$Traveler5=0;}
		
		$Traveler6 = mysqli_real_escape_string($db,$_POST["Traveler6"]);
		$Traveler6old = mysqli_real_escape_string($db,$_POST["TravelerCurrent6"]);
		if($Traveler6 != $Traveler6old) { $WhereClauseRedo .= "$Traveler6old,"; }
		if($Traveler6 > 0) {$JointInvoiceID_Array .=",".$Traveler6;} else {$Traveler6=0;}
		
		$Traveler7 = mysqli_real_escape_string($db,$_POST["Traveler7"]);
		$Traveler7old = mysqli_real_escape_string($db,$_POST["TravelerCurrent7"]);
		if($Traveler7 != $Traveler7old) { $WhereClauseRedo .= "$Traveler7old,"; }
		if($Traveler7 > 0) {$JointInvoiceID_Array .=",".$Traveler7;} else {$Traveler7=0;}
		
		$Traveler8 = mysqli_real_escape_string($db,$_POST["Traveler8"]);
		$Traveler8old = mysqli_real_escape_string($db,$_POST["TravelerCurrent8"]);
		if($Traveler8 != $Traveler8old) { $WhereClauseRedo .= "$Traveler8old,"; }
		if($Traveler8 > 0) {$JointInvoiceID_Array .=",".$Traveler8;} else {$Traveler8=0;}

		if($WhereClauseRedo != NULL) {$WhereClauseQuery = substr($WhereClauseRedo,0,-1);} else {$WhereClauseQuery ="";}
		
		//To see if this customer is already in the Customer_groups table or not xxxxxx
		$CGconn = new mysqli($servername, $username, $password, $dbname);
		if ($CGconn->connect_error) {die("Connection failed: " . $CGconn->connect_error);} 
		$CheckCustGroupSQL = "SELECT * FROM customer_groups WHERE Primary_Customer_ID=$PrimaryCustomer AND Group_ID=$thetour AND Type='Group Invoice'";
		$CheckCustGroupResult = $CGconn->query($CheckCustGroupSQL);
		$CustGroupCheckCount = $CheckCustGroupResult->num_rows;
		
		//To get the Invoice ID for the primary customer of this joint invoice
		$PrimaryInvoiceID = mysqli_real_escape_string($db,$_POST["PrimaryCustomerInvoiceID"]);
		
		if($CustGroupCheckCount == 0) { //If this customer is a primary but doesn't has a Customer_groups entry
			$CustomerGroupQuery = "INSERT INTO customer_groups (Primary_Customer_ID,Group_ID,Type,Additional_Traveler_ID_1,Additional_Traveler_ID_2,Additional_Traveler_ID_3,Additional_Traveler_ID_4,Additional_Traveler_ID_5,Additional_Traveler_ID_6,Additional_Traveler_ID_7,Additional_Traveler_ID_8)
			VALUES ($PrimaryCustomer,$thetour,'Group Invoice',$Traveler1,$Traveler2,$Traveler3,$Traveler4,$Traveler5,$Traveler6,$Traveler7,$Traveler8);";
			$CustomerGroupQuery .= "UPDATE customer_account SET groupinvoice='1',updatetime=NOW() WHERE (contactid=$Traveler1 OR contactid=$Traveler2 OR contactid=$Traveler3 OR contactid=$Traveler4 OR contactid=$Traveler5 OR contactid=$Traveler6 OR contactid=$Traveler7 OR contactid=$Traveler8) AND tourid=$SelectedTour;";
			$CustomerGroupQuery .= "UPDATE customer_invoice SET Group_Invoice='1',Mod_Date=NOW() WHERE Customer_Invoice_Num=$PrimaryInvoiceID;";
			$CustomerGroupQuery .= "UPDATE customer_invoice SET Status='2',Mod_Date=NOW() WHERE Customer_Account_Customer_ID IN ($Traveler1,$Traveler2,$Traveler3,$Traveler4,$Traveler5,$Traveler6,$Traveler7,$Traveler8) AND Group_ID=$SelectedTour;";
			//$CustomerGroupQuery .= "SET @p0='$PrimaryCustomer'; SET @p1='$CusInv_id'; CALL `UpsertGroupInvoiceLine`(@p0, @p1);";
			$CustomerGroupQuery .= "DELETE FROM customer_invoice_line WHERE Customer_Invoice_Num=$PrimaryInvoiceID;";
            // Nithin: Not using SUM(pro.price) AS Invoice_Line_Total since the customer_invoice_line is decided to use unit price.
			$CustomerGroupQuery .= "SET @cnt = 0;
									SET @InvoiceNum = $PrimaryInvoiceID;
									INSERT INTO customer_invoice_line(Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
									SELECT (@InvoiceNum) AS Customer_Invoice_Num,(@cnt := @cnt + 1) AS Customer_Invoice_Line,('Joint Invoice') AS Line_Type,SOL.Product_Product_ID,pro.name AS Product_Name,COUNT(SOL.Product_Product_ID) AS Qty,
                                    (pro.price) AS Invoice_Line_Total FROM sales_order_line SOL
									JOIN sales_order SO 
									ON SO.Sales_Order_Num=SOL.Sales_Order_Num
									JOIN products pro 
									ON pro.id=SOL.Product_Product_ID
									WHERE SO.Customer_Account_Customer_ID IN ($JointInvoiceID_Array) AND SO.Group_ID=$SelectedTour
									GROUP BY SOL.Product_Product_ID
									ORDER BY Customer_Invoice_Line;";
			$CustomerGroupQuery .= "UPDATE customer_invoice AS Inv,
									(
										SELECT SUM(pro.price) AS Invoice_Line_Total FROM sales_order_line SOL
										JOIN sales_order SO 
										ON SO.Sales_Order_Num=SOL.Sales_Order_Num
										JOIN products pro 
										ON pro.id=SOL.Product_Product_ID
										WHERE SO.Customer_Account_Customer_ID IN ($JointInvoiceID_Array) AND SO.Group_ID=$SelectedTour
									) AS Src
									SET Inv.Invoice_Amount=Src.Invoice_Line_Total WHERE Inv.Customer_Invoice_Num=$PrimaryInvoiceID;";
		
		} else { //If this customer is a primary and has a Customer_groups entry
			$CustomerGroupQuery = "UPDATE customer_groups SET Additional_Traveler_ID_1='$Traveler1',Additional_Traveler_ID_2='$Traveler2',Additional_Traveler_ID_3='$Traveler3',Additional_Traveler_ID_4='$Traveler4',Additional_Traveler_ID_5='$Traveler5',Additional_Traveler_ID_6='$Traveler6',Additional_Traveler_ID_7='$Traveler7',Additional_Traveler_ID_8='$Traveler8',Mod_Date=NOW() WHERE Primary_Customer_ID=$PrimaryCustomer AND Group_ID=$thetour AND Type='Group Invoice';";
			$CustomerGroupQuery .= "UPDATE customer_account SET groupinvoice='1',updatetime=NOW() WHERE (contactid=$Traveler1 OR contactid=$Traveler2 OR contactid=$Traveler3 OR contactid=$Traveler4 OR contactid=$Traveler5 OR contactid=$Traveler6 OR contactid=$Traveler7 OR contactid=$Traveler8) AND tourid=$SelectedTour;";
			$CustomerGroupQuery .= "UPDATE customer_invoice SET Group_Invoice='1',Mod_Date=NOW() WHERE Customer_Invoice_Num=$PrimaryInvoiceID;";
			$CustomerGroupQuery .= "UPDATE customer_invoice SET Status='2',Mod_Date=NOW() WHERE Customer_Account_Customer_ID IN ($Traveler1,$Traveler2,$Traveler3,$Traveler4,$Traveler5,$Traveler6,$Traveler7,$Traveler8) AND Group_ID=$SelectedTour;";
			//$CustomerGroupQuery .= "SET @p0='$PrimaryCustomer'; SET @p1='$CusInv_id'; CALL `UpsertGroupInvoiceLine`(@p0, @p1);";
			$CustomerGroupQuery .= "DELETE FROM customer_invoice_line WHERE Customer_Invoice_Num=$PrimaryInvoiceID;";
            // Nithin: Not using SUM(pro.price) AS Invoice_Line_Total since the customer_invoice_line is decided to use unit price.
			$CustomerGroupQuery .= "SET @cnt = 0;
									SET @InvoiceNum = $PrimaryInvoiceID;
									INSERT INTO customer_invoice_line(Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
									SELECT (@InvoiceNum) AS Customer_Invoice_Num,(@cnt := @cnt + 1) AS Customer_Invoice_Line,('Joint Invoice') AS Line_Type,SOL.Product_Product_ID,pro.name AS Product_Name,COUNT(SOL.Product_Product_ID) AS Qty,
                                    (pro.price) AS Invoice_Line_Total FROM sales_order_line SOL
									JOIN sales_order SO 
									ON SO.Sales_Order_Num=SOL.Sales_Order_Num
									JOIN products pro 
									ON pro.id=SOL.Product_Product_ID
									WHERE SO.Customer_Account_Customer_ID IN ($JointInvoiceID_Array) AND SO.Group_ID=$SelectedTour
									GROUP BY SOL.Product_Product_ID
									ORDER BY Customer_Invoice_Line;";
			$CustomerGroupQuery .= "UPDATE customer_invoice AS Inv,
									(
										SELECT SUM(pro.price) AS Invoice_Line_Total FROM sales_order_line SOL
										JOIN sales_order SO 
										ON SO.Sales_Order_Num=SOL.Sales_Order_Num
										JOIN products pro 
										ON pro.id=SOL.Product_Product_ID
										WHERE SO.Customer_Account_Customer_ID IN ($JointInvoiceID_Array) AND SO.Group_ID=$SelectedTour
									) AS Src
									SET Inv.Invoice_Amount=Src.Invoice_Line_Total WHERE Inv.Customer_Invoice_Num=$PrimaryInvoiceID;";
			
			//To return the value of "groupinvoice" to ZERO if the customer is taken out from the joint invoice list
			if($WhereClauseQuery != NULL) { 
			$CustomerGroupQuery .= "UPDATE customer_account SET groupinvoice='0',updatetime=NOW() WHERE contactid IN ($WhereClauseQuery) AND tourid=$SelectedTour;";
			$CustomerGroupQuery .= "UPDATE customer_invoice SET Group_Invoice='0',Mod_Date=NOW(),Status='1' WHERE Customer_Account_Customer_ID IN ($WhereClauseQuery) AND Group_ID=$SelectedTour;";
			}			
		}
        //remove all before updaing all new
        (new CustomerInvoice())->markAdditionalInvoices($SelectedTour, $PrimaryCustomer, false ) ;
        
		$CGconn->multi_query($CustomerGroupQuery);
		$CGconn->close();

//        (new CustomerInvoice())->markAdditionalInvoices($SelectedTour, $PrimaryCustomer) ;
        //Mark new invoices, after marking it in the table.
        (new CustomerInvoice())->markAdditionalInvoices($SelectedTour, $PrimaryCustomer, true,
            [ $Traveler1,
            $Traveler2,
            $Traveler3,
            $Traveler4,
            $Traveler5,
            $Traveler6,
            $Traveler7,
            $Traveler8
            ]) ;

	} else {
		//Here if the Joint Invoice button is turned OFF and there is a joint invoice record, so we remove it all
		$PrimaryCustomer = mysqli_real_escape_string($db,$_POST["PrimaryCustomer"]);
		         

		$JointInvoiceID_Array = "";
		
		$PrimaryCustomer = mysqli_real_escape_string($db,$_POST["PrimaryCustomer"]);
		if($PrimaryCustomer > 0) {$JointInvoiceID_Array .=$PrimaryCustomer;} else {$PrimaryCustomer=0;}

		$Traveler1 = mysqli_real_escape_string($db,$_POST["Traveler1"]);
		if($Traveler1 > 0) {$JointInvoiceID_Array .=",".$Traveler1;} else {$Traveler1=0;}
		$Traveler2 = mysqli_real_escape_string($db,$_POST["Traveler2"]);
		if($Traveler2 > 0) {$JointInvoiceID_Array .=",".$Traveler2;} else {$Traveler2=0;}
		$Traveler3 = mysqli_real_escape_string($db,$_POST["Traveler3"]);
		if($Traveler3 > 0) {$JointInvoiceID_Array .=",".$Traveler3;} else {$Traveler3=0;}
		$Traveler4 = mysqli_real_escape_string($db,$_POST["Traveler4"]);
		if($Traveler4 > 0) {$JointInvoiceID_Array .=",".$Traveler4;} else {$Traveler4=0;}
		$Traveler5 = mysqli_real_escape_string($db,$_POST["Traveler5"]);
		if($Traveler5 > 0) {$JointInvoiceID_Array .=",".$Traveler5;} else {$Traveler5=0;}
		$Traveler6 = mysqli_real_escape_string($db,$_POST["Traveler6"]);
		if($Traveler6 > 0) {$JointInvoiceID_Array .=",".$Traveler6;} else {$Traveler6=0;}
		$Traveler7 = mysqli_real_escape_string($db,$_POST["Traveler7"]);
		if($Traveler7 > 0) {$JointInvoiceID_Array .=",".$Traveler7;} else {$Traveler7=0;}
		$Traveler8 = mysqli_real_escape_string($db,$_POST["Traveler8"]);
		if($Traveler8 > 0) {$JointInvoiceID_Array .=",".$Traveler8;} else {$Traveler8=0;}
		
        //Close group, before updating new post values.
        (new CustomerInvoice())->markAdditionalInvoices($SelectedTour, $PrimaryCustomer, false) ;

		$CGconn = new mysqli($servername, $username, $password, $dbname);
		if ($CGconn->connect_error) {die("Connection failed: " . $CGconn->connect_error);} 
		$CheckCustGroupSQL = "SELECT * FROM customer_groups WHERE Primary_Customer_ID=$PrimaryCustomer AND Group_ID=$thetour AND Type='Group Invoice'";
		$CheckCustGroupResult = $CGconn->query($CheckCustGroupSQL);
		$CustGroupCheckCount = $CheckCustGroupResult->num_rows;
		if($CustGroupCheckCount > 0) {
			$CustomerGroupQuery = "DELETE FROM customer_groups WHERE Primary_Customer_ID=$PrimaryCustomer AND Group_ID=$thetour AND Type='Group Invoice';";
			$CustomerGroupQuery .= "UPDATE customer_account SET groupinvoice='0',Status='1' WHERE contactid IN ($JointInvoiceID_Array) AND tourid=$SelectedTour;";
            //Nithin. Reset invoice flag also. {
            $CustomerGroupQuery .= "UPDATE customer_invoice SET Group_Invoice=0 WHERE Customer_Account_Customer_ID=$PrimaryCustomer AND Group_ID=$thetour;";
            //}
		}		
		
		$CGconn->multi_query($CustomerGroupQuery);
		$CGconn->close();

	}
	//The end of the Joint Invoice code section
	echo "<h3>Success</h3> The customer account for $theaccountname has been updated successfully! The page will be refreshed in seconds.";
	echo "<meta http-equiv='refresh' content='2;contacts-edit.php?id=$theid&action=edit&usertype=customer'>";
} else {
	echo $sql12 . "<br>" . $con12->error;
}
}
$con12->close();
}

//Update the data of the dcoument attachment
if(isset($_POST['documentid'])){
	$fileid = $_POST["documentid"];
	$mediatype = $_POST["attachmenttype"];
	$medianame = $_POST["documentname"];
	$mediadesc = $_POST["description"];

	$con11 = new mysqli($servername, $username, $password, $dbname);
	if ($con11->connect_error) {die("Connection failed: " . $con11->connect_error);} 
	$sql11 = "UPDATE media SET mediatype='$mediatype', name='$medianame', mediadescription='$mediadesc', updatetime=NOW() WHERE id=$fileid;";
	$sql11 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','attachment','$fileid','edited','$medianame');";
if ($con11->multi_query($sql11) === TRUE) {
	echo "<h3>Success</h3> The attachment information has been updated successfully!";
} else {
	echo $sql11 . "<br>" . $con11->error;
}
$con11->close();
}

//Add a new payment for the Customer's Sales order
if(isset($_POST['PaymentAmount'])){
	$SalesOrderNumID = mysqli_real_escape_string($db, $_POST["SalesOrderNumID"]);
	$SalesOrderCustomerID = mysqli_real_escape_string($db, $_POST["SalesOrderCustomerID"]);
	$SalesOrderCustomerName = mysqli_real_escape_string($db, $_POST["SalesOrderCustomerName"]);
	$PaymentDate = mysqli_real_escape_string($db, $_POST["PaymentDate"]);
	$PaymentAmount = mysqli_real_escape_string($db, $_POST["PaymentAmount"]);
	$PaymentMethod = mysqli_real_escape_string($db, $_POST["PaymentMethod"]);
	$PaymentComments = mysqli_real_escape_string($db, $_POST["PaymentComments"]);

	$con13 = new mysqli($servername, $username, $password, $dbname);
	if ($con13->connect_error) {die("Connection failed: " . $con13->connect_error);} 
	$sql13 = "INSERT INTO customer_payments (Customer_Payment_Date, Customer_Account_Customer_ID, Customer_Payment_Sales_Order_ID, Customer_Payment_Amount, Customer_Payment_Method, Customer_Payment_Comments)
				VALUES ('$PaymentDate','$SalesOrderCustomerID','$SalesOrderNumID','$PaymentAmount','$PaymentMethod','$PaymentComments');";
	$sql13 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','payment for','$SalesOrderCustomerID','added','$SalesOrderCustomerName');";
if ($con13->multi_query($sql13) === TRUE) {
	echo "<h3>Success</h3> The payment has been added successfully!<br />The page will be refreshed in seconds.";
	echo "<meta http-equiv='refresh' content='2;contacts-edit.php?id=$SalesOrderCustomerID&action=edit&usertype=sales'>";
} else {
	echo $sql13 . "<br>" . $con13->error;
}
$con13->close();
}


//Record a new payment for the Agent
if(isset($_POST['Agent_Payment_Amount'])){
	$Agent_Payment_Date = mysqli_real_escape_string($db, $_POST["Agent_Payment_Date"]);
	$Agent_ID = mysqli_real_escape_string($db, $_POST["PaymentAgentID"]);
	$PaymentAgentName = mysqli_real_escape_string($db, $_POST["PaymentAgentName"]);
	$Agent_Payment_Amount = mysqli_real_escape_string($db, $_POST["Agent_Payment_Amount"]);
	$Agent_Payment_Method = mysqli_real_escape_string($db, $_POST["Agent_Payment_Method"]);
	$Agent_Group_ID = mysqli_real_escape_string($db, $_POST["AgentGroupID"]);
	$Agent_Payment_Comments = mysqli_real_escape_string($db, $_POST["Agent_Payment_Comments"]);
    $paymentAccountFrom = '';
    $paymentCheck = '';
    if( $Agent_Payment_Method == 'Cash' ) {
        $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_Cash']);
    } elseif( $Agent_Payment_Method == 'Bank Transfer' ) {
        $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_Bank']);
    } elseif( $Agent_Payment_Method == 'Check' ) {
        $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_Bank']);
        $paymentCheck = mysqli_real_escape_string($db, $_POST['Check_Number']);
        $AddNewCheckSQL = "INSERT INTO checks(Check_Type, Check_Number, Check_Customer_ID, Check_Bank_ID, Check_Amount, Check_Date, Check_Note, Check_Created_By) VALUES (3,'$paymentCheck','$Agent_ID','$paymentAccountFrom','$Agent_Payment_Amount','$Agent_Payment_Date','$Agent_Payment_Comments','$UserAdminID')";
        GDb::execute($AddNewCheckSQL);
        $IncreaseCheckNumSQL = "UPDATE checks_bank SET Bank_Check_Number=Bank_Check_Number+1 WHERE Bank_ID=$paymentAccountFrom";
        GDb::execute($IncreaseCheckNumSQL);
    } elseif( $Agent_Payment_Method == 'Credit Card' ) {
        $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_Card']);
    } elseif( $Agent_Payment_Method == 'Online Payment' ) {
        $paymentAccountFrom = mysqli_real_escape_string($db, $_POST['From_Account_PayPal']);
    }

	$InsertAgentPaymentSQL = "INSERT INTO agents_payments(Agent_Payment_Date, Agent_ID, Agent_Group_ID, Agent_Payment_Amount, Agent_Payment_Method, Agent_Payment_From_Account, Agent_Payment_Check, Agent_Payment_Comments, Transaction_Type_Transaction_ID) VALUES ('$Agent_Payment_Date','$Agent_ID','$Agent_Group_ID','$Agent_Payment_Amount','$Agent_Payment_Method','$paymentAccountFrom','$paymentCheck','$Agent_Payment_Comments','66');";
	$InsertAgentPaymentLogSQL = "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','Agent\'s payment for','$Agent_ID','recorded','$PaymentAgentName');";
    if (GDb::execute($InsertAgentPaymentSQL) && GDb::execute($InsertAgentPaymentLogSQL)) {
        echo "<h3>Success</h3> The payment has been recorded successfully!<br />The page will be refreshed in seconds.";
        echo "<meta http-equiv='refresh' content='2;contacts-edit.php?id=$Agent_ID&action=edit&usertype=agentPayments'>";
    } else {
        echo $db->error;
    }
}

//Create new invoice for the customer
/*if(isset($_POST['InvoiceDueDate'])){
	$SalesOrderNumID = mysqli_real_escape_string($db, $_POST["SalesOrderNumID"]);
	$SalesOrderCustomerID = mysqli_real_escape_string($db, $_POST["SalesOrderCustomerID"]);
	$SalesOrderCustomerName = mysqli_real_escape_string($db, $_POST["SalesOrderCustomerName"]);
	$InvoiceDate = mysqli_real_escape_string($db, $_POST["InvoiceDate"]);
	$InvoiceDueDate = mysqli_real_escape_string($db, $_POST["InvoiceDueDate"]);
	$InvoiceComments = mysqli_real_escape_string($db, $_POST["InvoiceComments"]);

	$con14 = new mysqli($servername, $username, $password, $dbname);
	if ($con14->connect_error) {die("Connection failed: " . $con14->connect_error);} 
	$sql14 = "INSERT INTO customer_invoice (Customer_Invoice_Date,Customer_Order_Num,Customer_Account_Customer_ID,Transaction_Type_Transaction_ID,Due_Date,Comments)
				VALUES ('$InvoiceDate','$SalesOrderNumID','$SalesOrderCustomerID','0','$InvoiceDueDate','$InvoiceComments');";
	$sql14 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','invoice for','$SalesOrderCustomerID','created','$SalesOrderCustomerName');";
if ($con14->multi_query($sql14) === TRUE) {
	echo "<h3>Success</h3> The invoice has been created successfully!<br />The page will be refreshed in seconds.";
	echo "<meta http-equiv='refresh' content='2;contacts-edit.php?id=$SalesOrderCustomerID&action=edit&usertype=sales'>";
} else {
	echo $sql14 . "<br>" . $con14->error;
}
$con14->close();
}*/


//Update the data of the products
//It is not used anymore, i moved it to the groups functions
/*if(isset($_POST['productid'])){

$productid = $_POST['productid'];
$pname = $_POST["name"];
$pdesc = $_POST["desc"];
$producttype = $_POST["producttype"];
$cost = $_POST["cost"];
$price = $_POST["price"];
$status = $_POST["status"];
$supplierid = $_POST['thesupplier'];
$categoryid = $_POST['thecategory'];

	$procon = new mysqli($servername, $username, $password, $dbname);
	if ($procon->connect_error) {die("Connection failed: " . $procon->connect_error);} 
$prosql = "UPDATE products SET name='$pname', description='$pdesc', producttype='$producttype', cost='$cost', price='$price', supplierid='$supplierid', categoryid='$categoryid', status='$status', updatetime=NOW() WHERE id=$productid;";
$prosql .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','product','$productid','edited','$pname');";
if ($procon->multi_query($prosql) === TRUE) {
echo "<h3>Success</h3> The product has been updated successfully!";
} else {
echo $prosql . "<br>" . $procon->error."<br> error";
}

$procon->close();
}*/

function enableLogin($theid, $enable, $type) {
    $fullname = ''; 
    $isFresh = false ;
    $userObj = new Users() ;
     $userDataOld = $userObj->get(['contactid' => $theid]) ;
	if($enable== '1'){
		$userdata = $userObj->upgradeUser($theid, $type, $isFresh) ;
        
	} else if ($enable != '1' ){
		$userObj->deactivateUserByContactId($theid, $type) ;
	}
        $reset_password_url = GUtils::domainUrl( 'reset_password.php?t=' . $userdata['token'] ) ;
	if($isFresh == 1 ) {
        //Send Email
        $params = array(
            'login_url' => GUtils::domainUrl(),
            'reset_password_url' => $reset_password_url ,
            'password' => $userdata['password'],
            'name' => $fullname
        ) ;
        GUtils::sendTemplateMailByCode($userdata['email'], 'ENABLE_LOGIN', $params) ;
    }
    else {
        if(($enable == '1') && (($isFresh == 2 && ($userdata['email'] != $userDataOld['EmailAddress']) ) || ($userDataOld['Role_ID'] & $type) == 0) ) {
        $params = array(
            'name' => $fullname,
            'reset_password_url' => $reset_password_url ,
            'login_url' => GUtils::domainUrl(),
        );
        GUtils::sendTemplateMailByCode($userdata['email'], 'ENABLE_MULTI_LOGIN', $params) ;
        }
    }
}

//Update the data of the Agent
if(isset($__POST['leadercode'])){
    
    $theid = $__POST["leadercode"];
	$status = mysqli_real_escape_string($db, $__POST['leaderstatus']);
	$desc = mysqli_real_escape_string($db, $__POST["leaderdesc"]);

    //update 
    $gl = new GroupLeaders() ;
    $gl->update([
            'status' => $status,
            'description' => $desc,
            'updatetime' => Model::sqlNoQuote('now()')
        ], 
        ['contactid' => $theid]);
    
    $c = new Contacts() ;
    $cdata = $c->get(['id' => $theid]) ;
    if( ! $cdata['email'] ) {
        echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error: This contact must have an email address!</strong></div>";
        return ;
    }
    
    enableLogin($theid, $__POST['leaderlogin'], AclRole::$LEADER) ;
    
    echo "<h3>Success</h3> The leader has been updated successfully!";
    return ;
}


//Refresh the supplier balance to get the payments updated and create a financial account for him as well
if(isset($__POST['Supplier_Refresh_ID'])) {
    $Supplier_ID = mysqli_real_escape_string($db, $__POST['Supplier_Refresh_ID']);
    $Supplier_Name = mysqli_real_escape_string($db, $__POST['Supplier_Refresh_Name']);

    $GetSupplierBankIDsql  = "SELECT Bank_ID FROM checks_bank WHERE Bank_Type='supplier' AND Bank_Name=$Supplier_ID";
    $SuppBankID = GDb::fetchRow($GetSupplierBankIDsql);
    if(count($SuppBankID) > 0) {
        $Supplier_Bank_ID = $SuppBankID['Bank_ID'];
    } else {
        $Create_Bank_ID_SQL = "INSERT INTO checks_bank(Bank_Type, Bank_Name, Bank_Address, Bank_Status, Bank_Notes) VALUES ('supplier',$Supplier_ID,'$Supplier_Name',1,'Generated Automatically from the Supplier Refresh button')";
        GDb::execute($Create_Bank_ID_SQL);
        $Supplier_Bank_ID = GDb::db()->insert_id;
    }
    
    $Update_Supplier_Payments_SQL = "UPDATE suppliers_payments SET Supplier_Payment_Financial_Account=$Supplier_Bank_ID WHERE Supplier_ID=$Supplier_ID";
    
    if(GDb::execute($Update_Supplier_Payments_SQL)) {
		echo "<h3>Success</h3> The supplier data has been refreshed successfully!";
        echo "<meta http-equiv='refresh' content='2;contacts-edit.php?id=$Supplier_ID&action=edit&usertype=supplierBalance'>";
	} else {
		echo "<h3>Error</h3> There was an error while refreshing the data <br />" . $db->error;
	}
}