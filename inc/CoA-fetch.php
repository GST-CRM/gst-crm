<?php
//fetch.php
include "../session.php";

$connect = new mysqli($servername, $username, $password, $dbname);
$columns = array('first_name', 'last_name');

$CoA_Status = $_GET['status'];
if($CoA_Status == "voided") {
	$CoA_Where = "GL_Account_Status=0";
} else {
	$CoA_Where = "GL_Account_Status=1";
}

$query = "SELECT COA.Account_ID,GLG.GL_Group_Num,GLG.GL_Group_Name,COA.GL_Account_Name,COA.Mod_Date FROM gl_chart_of_accounts COA JOIN gl_account_groups GLG ON GLG.GL_Group_Num=COA.GL_Account_Groups_Group_ID WHERE $CoA_Where ";

if(isset($_POST["search"]["value"])) {
	$query .= 'AND COA.GL_Account_Name LIKE "%'.$_POST["search"]["value"].'%" ';
}
$query .= 'ORDER BY GLG.GL_Group_Num DESC ';

$query1 = '';

if($_POST["length"] != -1)
{
 $query1 = 'LIMIT ' . $_POST['start'] . ', ' . $_POST['length'];
}

$number_filter_row = mysqli_num_rows(mysqli_query($connect, $query));

$result = mysqli_query($connect, $query . $query1);

$data = array();

while($row = mysqli_fetch_array($result))
{
	//To put all the actions options for each payee in a PHP variable
	$ActionsList = '<div class="btn-group">';
	if($_GET["status"] == "voided") {
	$ActionsList .= '<a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick = \'deleteConfirm( "expenses-CoA.php?action=unvoid&CoA_id='.$row["Account_ID"].'", "Unvoid")\' role="button">Unvoid</a>';
	} else {
	$ActionsList .= '<a class="btn btn-primary btn-sm" href="javascript:void(0);" role="button" id="dropdownMenuLink">Actions</a>
		<button type="button" class="btn btn-sm btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<span class="sr-only">Toggle Dropdown</span>
		</button>
		<div class="dropdown-menu">
			<a class="dropdown-item" href="javascript:void(0);" onclick = \'deleteConfirm( "expenses-CoA.php?action=delete&CoA_id='.$row["Account_ID"].'", "Delete")\'>Delete</a>
			<a class="dropdown-item" href="javascript:void(0);" onclick = \'deleteConfirm( "expenses-CoA.php?action=void&CoA_id='.$row["Account_ID"].'", "Void")\'>Void</a>
		</div>';
	}
	$ActionsList .='</div>';
	
	//To show each TD for the Payee Table in array
	$sub_array = array();
	$sub_array[] = $row["GL_Group_Num"];
	$sub_array[] = $row["GL_Group_Name"];
	$sub_array[] = '<div class="input-group mb-0"><input type="text" class="update form-control PayeeNameField" data-id="'.$row["Account_ID"].'" data-column="GL_Account_Name" value="' . $row["GL_Account_Name"] . '"><div class="input-group-append"><span class="input-group-text" id="basic-addon2"><i class="far fa-edit"></i></span></div></div>';
	$sub_array[] = $row["Mod_Date"];
	$sub_array[] = $ActionsList;
	$data[] = $sub_array;
}

function get_all_data($connect)
{
 $query = "SELECT COA.Account_ID,GLG.GL_Group_Num,GLG.GL_Group_Name,COA.GL_Account_Name,COA.Mod_Date FROM gl_chart_of_accounts COA JOIN gl_account_groups GLG ON GLG.GL_Group_Num=COA.GL_Account_Groups_Group_ID WHERE GL_Account_Status=1";
 $result = mysqli_query($connect, $query);
 return mysqli_num_rows($result);
}

$output = array(
 "draw"    => intval($_POST["draw"]),
 "recordsTotal"  =>  get_all_data($connect),
 "recordsFiltered" => $number_filter_row,
 "data"    => $data
);

echo json_encode($output);

?>