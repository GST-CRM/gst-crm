<?php

$dueDate = Helpers::mysqlDateTime( $__REQUEST['due_date'] ) ;
$products = $__REQUEST['product'] ;
$qtyset = $__REQUEST['qty'] ;

$lineSet = [] ;
$amount = 0 ;

//insert all products submitted.
foreach( $products AS $key => $prod ) {
    $productId = $prod ;
    $qty = $qtyset[$key] ;
    //find product price
    $sqlp = "SELECT price, name FROM products WHERE id='$productId'" ;
    $resultp = $db->query($sqlp) ;
    $prodp = $resultp->fetch_assoc() ;
    $price = $prodp['price'] ;
    $name = $prodp['price'] ;
    
    $amount += ($price * $qty) ;
    
    $lineSet[] = array(
        'id' => $productId,
        'qty' => $qty,
        'price' => $price,
        'name' => $name
    );
}
//Find account id from contactid
$cid = $__REQUEST['CurrentCustomerSelected'] ;
$sql = "SELECT id FROM customer_account WHERE contactid='$cid'" ;
$result = $db->query($sql) ;

if( $result ) {
    $data = $result->fetch_assoc() ;
    //got id
    $accountId = $data['id'] ;
    
    //insert into customer invoice & line
    $sqlci = "INSERT INTO `customer_invoice` 
        (`Customer_Invoice_Date`, `Customer_Order_Num`, `Invoice_Amount`, `Group_ID`, 
        `Customer_Account_Customer_ID`, `Transaction_Type_Transaction_ID`, `Due_Date`, `Comments`, `Add_Date`, `Mod_Date`)
        VALUES 
        (NOW(), 0, '$amount', 0, 
        '$accountId', 0, '" . $dueDate . "', '', NOW(), NOW() )" ;

    $db->query($sqlci) ;

    $invoiceId = $db->insert_id ;

    //insert all lines
    foreach( $lineSet as $v ) {
        $productId = $v['id'] ;
        $name = $v['name'] ;
        $qty = $v['qty'] ;
        $price = $v['price'] ;
        $total = $price * $qty ;

        $sqll = "INSERT INTO `customer_invoice_line` 
            (`Customer_Invoice _Num`, `Customer_Invoice _Line`, `Line_Type`, `Product_Product_ ID`, 
            `Product_Name`, `Qty`, `Invoice_Line_Total`, `Mod_Date`, `Add_Date`)
            VALUES ('$invoiceId', 0, 0, '$productId',
                '$name', '$qty', '$total', NOW(), NOW() )" ;

        $db->query($sqll) ;
    }
}
