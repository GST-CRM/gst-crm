<form name="contact1" action="inc/contact-functions.php" method="POST" id="contact1">
	<div class="row">
		<div class="form-group col-sm-12 m-0">
			<h4 class="sub-title">Agent Information</h4>
		</div>
		<div class="form-default form-static-label col-sm-3">
			<label class="float-label">Status</label>
			<div class="can-toggle">
			  <input type="checkbox" id="d" name="agentstatus" value="1" <?php if($agentdata["status"]==1) echo "checked"; ?>>
			  <label for="d">
				<div class="can-toggle__switch" data-checked="Active" data-unchecked="Disabled"></div>
			  </label>
			</div>
		</div>
		<div class="form-group row col-sm-12" style="display:none;">
			<label class="col-sm-4 col-form-label">Agent #</label>
			<div class="col-sm-8"><input type="text" name="agentcode" class="form-control" value="<?php echo $agentdata["id"]; ?>" readonly>
			<input type="text" name="contactcode" class="form-control" value="<?php echo $agentdata["contactid"]; ?>" hidden>
			<input type="text" name="fullname" class="form-control" value="<?php echo $data['fname']." ".$data['mname']." ".$data['lname']; ?>" hidden></div>
		</div>
		<div class="form-group col-sm-6">
			<label class="float-label">Description</label>
			<input type="text" name="agentdesc" value="<?php echo $agentdata["description"]; ?>" class="form-control">
		</div>
		<div class="form-group col-sm-3">
			<label class="float-label">Enable Login</label>
			<div class="can-toggle">
				<input type="checkbox" class="agent-active-disable-switch-checkbox" id="agentl" name="agentlogin" value="1" <?php if($agentdata["active"]==1) echo"checked"; ?> >
				<label for="agentl">
					<div class="can-toggle__switch" data-checked="Active" data-unchecked="Disabled"></div>
				</label>
			</div>
		</div>
        
		<div class="form-group col-sm-4">
			<label class="float-label">Income</label>
			<select name="incometype" id="idAgentIncomeType" class="form-control" onchange="updateCommissionType(this);" >
				<option value="Commission" <?php if($agentdata["incometype"] == "Commission") {echo "selected";} ?>>Commission</option>
				<option value="Salary" <?php if($agentdata["incometype"] == "Salary") {echo "selected";} ?>>Salary</option>
			</select>
		</div>
			
        <?php /*
		<div class="form-group col-sm-4">
			<label class="float-label">Commission Type</label>
			<select name="commissiontype" class="form-control gst-agent-commision-fields">
				<option value="Per Person" <?php if($agentdata["commissiontype"] == "Per Person") {echo "selected";} ?>>Per Person</option>
				<option value="Per Trip" <?php if($agentdata["commissiontype"] == "Per Trip") {echo "selected";} ?>>Per Trip</option>
			</select>
		</div>
		<div class="form-group col-sm-4">
			<label class="float-label">Commission Value</label>
			<input type="text" class="form-control  gst-agent-commision-fields" name="commissionvalue" value="<?php echo $agentdata["commissionvalue"];?>" />
		</div>
         * 
         */
        ?>
		<div class="form-group row col-sm-12">
			<div class="col-sm-12">
				<button type="submit" id="idAgentUpdateButton" name="updateagent" value="submit" class="btn waves-effect waves-light btn-success"style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Update</button>
			</div>
		</div>
	</div>
</form>
<hr>
		<br />
		<h6>Groups assigned to this agent</h6>
						<a href="#" class="btn btn-mat waves-effect waves-light btn-info float-right" style="padding: 3px 13px;" data-toggle="modal" data-target="#AgentPayments">New Agent Payment</a>
						<a href="contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=agent&status=inactive" class="btn btn-mat waves-effect waves-light btn-inverse float-right" style="margin-right: 5px; padding: 3px 13px;">Disabled</a>
						<a href="contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=agent&status=expired" class="btn btn-mat waves-effect waves-light btn-inverse float-right" style="margin-right: 5px; padding: 3px 13px;">Expired</a>
						<a href="contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=agent" class="btn btn-mat waves-effect waves-light btn-inverse float-right" style="margin-right: 5px; padding: 3px 13px;">Active</a>
						<a href="contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=agent&status=both" class="btn btn-mat waves-effect waves-light btn-inverse float-right" style="margin-right: 5px; padding: 3px 13px;">Show All</a>
		<br />
		<br />
		<table id="basic-btn2" class="table table-hover table-striped table-bordered responsive">
			<thead>
				<tr role="row">
					<th>Group Name</th>
					<th>Start Date</th>
					<th>End Date</th>
					<th class="text-center">PAX</th>
					<th class="text-center">Balance</th>
					<th class="text-center">Paid</th>
				</tr>
			</thead>
			<tbody>
			<?php
			$agentid = $agentdata["contactid"];
			//Get the status of the group requested from the URL
			if($_GET['status'] == "inactive"){
				$statuss = "WHERE status=0";
			} elseif($_GET['status'] == "both") {
				$statuss = "WHERE 1 ";
			} elseif($_GET['status'] == "expired") {
				$displaynone = "style='display:none;'";
				$statuss = "WHERE status=1 AND enddate < CURDATE() $leadergroups";
			} else {
				$statuss = "WHERE status=1 AND startdate >= CURDATE() $leadergroups";
			}
			//$sql = "select * from groups $statuss" ;
			$sql = "select tourid,tourname,startdate,enddate,commissiontype from groups JOIN group_agents ON group_id=tourid $statuss AND `agent` LIKE '%$agentid%' GROUP BY tourid ORDER BY startdate asc";
			$result = $db->query($sql);
			
			//To get the total of Agents balance owed so far 
			$TotalAgentBalanceOwed = 0;
			
			
			if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
				<tr>
					<td><a href="groups-edit.php?id=<?php echo $row["tourid"]; ?>&action=edit" style="color: #0000EE;"><?php echo $row["tourname"]; ?></a></td>
					<td><?php echo date('m/d/Y', strtotime($row["startdate"])); ?></td>
					<td><?php echo date('m/d/Y', strtotime($row["enddate"])); ?></td>
					<td class="text-center">
						<a href="groups-edit.php?id=<?php echo $row["tourid"]; ?>&action=edit&tab=participants" style="color: #0000EE;">
							<?php
							//To get the number of PAX in the group
							$TourID = $row['tourid'];
							$paxsql = 	"SELECT COUNT(ca.tourid) AS PaxTotal FROM customer_account ca 
										WHERE
											ca.tourid=$TourID
											AND ca.status=1
											AND ca.contactid not in (
												select cn.Customer_Account_Customer_ID
												from customer_credit_note cn
												JOIN customer_account ca
												ON ca.tourid=$TourID
												WHERE cn.Customer_Account_Customer_ID=ca.contactid
												AND cn.Full_Discount=1
											)";
							$paxresult = $db->query($paxsql);
							$paxtotal = $paxresult->fetch_assoc();
							echo $paxtotal['PaxTotal']." <small>PAX</small>";
							?>
						</a>
					</td>
					<td class="text-center">
							<?php
							//To get the balance needed for the agent in this group
							$CommissionType = $row['commissiontype'];
							if($CommissionType == "Per Trip") {
								$AgentGroupBalanceSQL = "SELECT commissionvalue AS TheBalance
														FROM group_agents
														WHERE group_id=$TourID;";
								$AgentGroupBalanceResult = $db->query($AgentGroupBalanceSQL);
								$AgentGroupBalance = $AgentGroupBalanceResult->fetch_assoc();
								echo "$".$AgentGroupBalance['TheBalance'];
								$TotalAgentBalanceOwed = $TotalAgentBalanceOwed+$AgentGroupBalance['TheBalance'];
							} elseif ($CommissionType == "Per Person") {
								$AgentGroupBalanceSQL = "SELECT (COUNT(ca.tourid) * ga.commissionvalue) AS TheBalance
														FROM customer_account ca 
														JOIN group_agents ga
														ON ga.group_id=ca.tourid AND agent_id=$leadid
														WHERE ca.tourid=$TourID AND ca.status=1
                                                        AND ca.contactid not in (
															select cn.Customer_Account_Customer_ID
															from customer_credit_note cn
															JOIN customer_account ca
															ON ca.tourid=$TourID
															WHERE cn.Customer_Account_Customer_ID=ca.contactid
															AND cn.Full_Discount=1
														);";
								$AgentGroupBalanceResult = $db->query($AgentGroupBalanceSQL);
								$AgentGroupBalance = $AgentGroupBalanceResult->fetch_assoc();
								echo "$".$AgentGroupBalance['TheBalance'];
								$TotalAgentBalanceOwed = $TotalAgentBalanceOwed+$AgentGroupBalance['TheBalance'];
							} else {
								echo "N/A";
							}
							?>
					</td>
					<td class="text-center">
							<?php
							$AgentPaymentSQL = "SELECT SUM(Agent_Payment_Amount) AS TotalAgentPayments FROM agents_payments WHERE Agent_Group_ID=$TourID AND Agent_ID=$agentid";
							$AgentPaymentResult = $db->query($AgentPaymentSQL);
							$AgentPaymentsTotal = $AgentPaymentResult->fetch_assoc();
							if($AgentPaymentsTotal['TotalAgentPayments'] > 0) {
								echo "$".$AgentPaymentsTotal['TotalAgentPayments'];
							} else {
								echo "$0.00";
							}
							?>
					</td>
				</tr>
			<?php }} else { ?>
				<tr>
					<td colspan="6" style="text-align:center;">There are no groups assigned for this agent yet.</td>
				</tr>
			<?php } ?>
			</tbody>
		</table>
		
<script>
$(document).ready(function() {
    $('#basic-btn2').DataTable( {
        "ordering": false,
		dom: 'Bfrtip',
        buttons: [
            'csv', 'excel'
        ]
    } );
} );
</script>

<?php

global $GLOBAL_SCRIPT;
$GLOBAL_SCRIPT .= "updateCommissionType(document.getElementById('idAgentIncomeType'));" ;