<style>
    h3{
    color: #000;
    font-size: 24px;
    line-height: 15px;
    font-weight: bold;
}
h2{
    font-size: 36px;
    font-weight: bold;
    line-height: 2px;
}
h5{
    font-size: 20px;
    font-weight: normal;
}
table tr td.main_td{
    text-align: center;
    height: 2.95in;
    width: 3.95in;
    border-width: .05in .05in .05in .05in;
    border-color: #000;
    border-style: solid;
    overflow:hidden;
}
u span{
    line-height:30px;
}
u{
    line-height:30px;
}
table.emergency_contacts_entries tr td{
    line-height:26px;
}
h5{
    color: #000;
    font-size: 18px;
    line-height: 15px;
    font-weight: bold;
}
</style>

<!-- Header Table -->
<?php

$tourid = $_GET['id'];
$numOfCols = 2;
$rowCount = 0;


?>
<table style="width:100%;height:100%" cellpadding="1" cellspacing="5" align="center">
<tr nobr="true">
<?php
$i=1;
while($i <= 6) {
?>
        <td class="main_td">
        <table cellpadding="0" cellspacing="10" style="" align="center">
        <tr>
            <td style="text-align: center;height: 2.70in;">
            <h5>Emergency Contacts Abroad</h5>
            <!--BLANK TABLE FOR SPACING -->
            <table border="0">
            <tr style="line-height: 20px;" > 
            <td></td>
            </tr>
            </table>
            <!--BLANK TABLE FOR SPACING -->
            
            <table cellpadding="0" cellspacing="0" style="" align="center">
            <tr>
                <td style="text-align: center;height: 1.2in;">
                <table cellpadding="0" cellspacing="0" style="" align="center" style="width:100%" class="emergency_contacts_entries">
                
                <?php
                //Select land_productid 

                $sql = "SELECT land_productid FROM groups WHERE tourid=$tourid";
                $queryLandRecord = mysqli_query($db, $sql) or die("error to fetch land product id");
                $landData = mysqli_fetch_row($queryLandRecord);
                $landProductId = $landData[0];
                //If land product id found
                if($landProductId!="" && $landProductId!=0){
                    
                    //Select product info
                    $sql = 'SELECT supplierid, producttype,name,subproduct,parentid,meta1,meta2,meta3,meta4 FROM products WHERE parentid = '.$landProductId.' AND producttype IN ("Hotels","Guides","Restaurants") LIMIT 4';
                    //echo $sql;
                    $queryProduct = mysqli_query($db, $sql) or die("error to fetch product info");
                    if (mysqli_num_rows($queryProduct) > 0) {
                        while($row = mysqli_fetch_assoc($queryProduct)) {
                            //echo $row['name']."<br>";
                            $supplierid = $row['supplierid'];

                            ?>
                            <tr>
                            <td><?php echo $row['name']; ?>: <?php echo $row['meta2']; ?>:</td>
                            <td>Phone: <?php echo $row['meta1']; ?></td>
                            </tr>
                            <?php

                        }
                        //Select land operator
                        $sql = 'SELECT fname,lname,mobile FROM contacts WHERE id='.$supplierid.'';
                        $querylandOperator = mysqli_query($db, $sql) or die("error to fetch product info");
                        $landOperatorData = mysqli_fetch_row($querylandOperator);
                        $landOperatorName = $landOperatorData[0]." ".$landOperatorData[1];
                        $landOperatorMobile = $landOperatorData[2];
                        ?>
                        <tr>
                        <td>Land Operator: <?php echo $landOperatorName; ?>:</td>
                        <td>Mobile: <?php echo $landOperatorMobile; ?></td>
                        </tr>
                        <?php

                    }
                    else{
                        echo "<tr><td>No products with parentid ".$landProductId."</td></tr>";
                    }


                }
                else{
                    echo "<tr><td>Land product id is null.</td></tr>";
                }
            ?>
                </table>

                </td>
            </tr>
            </table>
            </td>
            </tr>
            </table>
        </td>
<?php
    $rowCount++;
    $i++;
    if($rowCount % $numOfCols == 0) echo '</tr><tr style="line-height:2px"><td style="height:100%;width:100%"></td></tr><tr nobr="true">';
}
?>
</tr>

</table>