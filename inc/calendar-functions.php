<?php

 include_once __DIR__."/../session.php";
include_once __DIR__."/helpers.php";
//include_once __DIR__ . '/../models/calendar_events.php' ;

$fail                   = false ;

if (!empty($_REQUEST['addEvent']))
{
    $eventId            = !empty($_REQUEST['Event_ID'])?$_REQUEST['Event_ID']:"";
    $eventDate          = !empty($_REQUEST['Event_Date'])?$_REQUEST['Event_Date']:"";
    $eventType          = !empty($_REQUEST['Event_Type'])?$_REQUEST['Event_Type']:"";
    $eventDescription   = !empty($_REQUEST['Event_Description'])?$_REQUEST['Event_Description']:"";
    $eventNotes         = !empty($_REQUEST['Event_Notes'])?$_REQUEST['Event_Notes']:"";
    $recurring          = !empty($_REQUEST['Recurring'])?$_REQUEST['Recurring']:0;
    $period             = !empty($_REQUEST['Period'])?$_REQUEST['Period']:"";
    if (!$eventDate) {
        $fail = true ;
        GUtils::setWarning('Please select a date') ;
        return ;
    }
    if (!$eventType) {
        $fail = true ;
        GUtils::setWarning('Please select an event type') ;
        return ;
    }
    if (!$eventDescription) {
        $fail = true ;
        GUtils::setWarning('Please enter event description') ;
        return ;
    }


    if (!$fail) {

        if (empty($eventId))
        {
            $sql    = "INSERT INTO calendar_events(Event_Date, Event_Type, Event_Description, Event_Notes, Recurring, Period) VALUES('$eventDate','$eventType','$eventDescription','$eventNotes',$recurring,'$period')";
        }
        else
        {
            $sql    = "UPDATE calendar_events SET Event_Date = '$eventDate', Event_Type = '$eventType', Event_Description = '$eventDescription', Event_Notes = '$eventNotes', Recurring = $recurring, Period = '$period' where Event_ID = $eventId";
        }
        $result = $db->query($sql);
        GUtils::setSuccess('Event added successfully.') ;
        //GUtils::redirect('calendar.php') ;
        return ;

        //if( $calendar_events->addEvents($_POST) ) {
        //}
    }
    GUtils::setWarning('Unable to add event.') ;
    return ;
}
else if (!empty($_REQUEST['getEvent']))
{
    $sql    = "SELECT * FROM calendar_events order by Event_Date desc";
    $result = $db->query($sql);
    if ($result->num_rows > 0) 
    { 
        while($row = $result->fetch_assoc()) 
        {      
            if ($row['Event_Type'] == "Task")
            {
                $color = "#06aeee";
            }    
            if ($row['Event_Type'] == "Reminder")
            {
                $color = "#f1f343";
            } 
            if ($row['Event_Type'] == "Important")
            {
                $color = "#f0466b";
            }   

            $eventTmpArray = array(
                'id'                => $row['Event_ID'],
                'title'             => $row['Event_Description'],
                'start'             => $row['Event_Date'],
                'notes'             => $row['Event_Notes'],
                'enable_period'     => $row['Recurring'],
                'period'            => $row['Period'],
                'type'              => $row['Event_Type'],
                'backgroundColor'   => $color,
                'borderColor'       => "grey"
            );
            $eventArray[] = $eventTmpArray;
        }
        if (!empty($calendarPage))
        {
            return $eventArray;
        }
        else
        {
            echo json_encode($eventArray);
            exit;
        }
    }
    return;
}
else if (!empty($_REQUEST['delEvent']))
{
    if ($_REQUEST['Event_ID'] > 0) 
    {
        $sql    = "DELETE FROM calendar_events where Event_ID =".$_REQUEST['Event_ID'];
        $result = $db->query($sql);
        GUtils::setSuccess('Event deleted successfully.') ;
        return ;
    }
}