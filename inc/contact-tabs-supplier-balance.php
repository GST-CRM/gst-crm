<?php

$GetBankDataSQL = "SELECT Bank_ID, Bank_Type, Bank_Name, Bank_Address, Bank_Transit, Bank_Routing, Bank_Account_Number, Bank_Check_Number, Bank_Notes, Bank_Add_Time, Bank_Mod_Time FROM checks_bank WHERE Bank_Name=$leadid AND Bank_Type='supplier'";
$BankData = GDb::fetchRow($GetBankDataSQL);

if(count($BankData) > 0) {
    $BankID = $BankData['Bank_ID'];
    $ShowTable = true;
} else {
    $ShowTable = false;
}


?>
<div class="row">
    <div class="form-group col-sm-8 m-0">
        <h4 class="sub-title">Supplier Account Transactions</h4>
    </div>
    <div class="form-group col-sm-2 m-0">
        <a href="#" class="btn btn-mat waves-effect waves-light btn-primary btn-block" style="margin-right: 5px; padding: 3px 13px;" data-toggle="modal" data-target="#AddDepositModal">Add Deposit</a>
    </div>
    <div class="form-group col-sm-2 m-0">
        <form class="float-right" name="contact25" action="inc/contact-functions.php" method="POST" id="contact25">
            <input type="text" name="Supplier_Refresh_ID" value="<?php echo $leadid; ?>" hidden>
            <input type="text" name="Supplier_Refresh_Name" value="<?php echo $data['company']; ?>" hidden>
            <button type="submit" name="SupplierRefresh" class="btn btn-mat waves-effect waves-light btn-primary btn-block" style="margin-right: 5px; padding: 3px 13px;" data-toggle="modal" data-target="#resultsmodal">Refresh Data</button>
        </form>
    </div>
    <?php if($ShowTable) { ?>
    <div class="form-group col-sm-12 m-0">
        <table class="table table-hover table-striped table-smallz dataTable">
            <thead>
                <tr>
                    <th scope="col" width="100">Date</th>
                    <th scope="col" width="150">Type</th>
                    <th scope="col">Account</th>
                    <th scope="col" width="100">Credit</th>
                    <th scope="col" width="100">Debit</th>
                    <th scope="col" class="text-center" width="50"><i class="fa fa-cogs" aria-hidden="true"></i></th>
                </tr>
            </thead>
            <tbody>
                <?php
$TotalCredit = 0;
$TotalDebit = 0;
                    
$TransactionsSQL = "
(SELECT
	de.Deposit_ID AS Trans_ID,
    de.Deposit_Date AS Trans_Date,
    'Deposit' AS Trans_Type,
    de.Deposit_Added_By AS Trans_Person_ID,
    CONCAT(us.fname,' ',us.lname) AS Trans_Person,
    de.Deposit_Notes AS Trans_Details,
    de.Deposit_To AS Trans_Reference_ID,
    cb.Bank_Name AS Trans_Reference,
    de.Deposit_Amount AS Trans_Credit,
    '0.00' AS Trans_Debit,
    de.Deposit_Add_Time AS Trans_Time
FROM checks_bank_deposits de
JOIN checks_bank cb
ON cb.Bank_ID=de.Deposit_To
JOIN users us 
ON us.UserID=de.Deposit_Added_By
WHERE de.Deposit_To=$BankID)
UNION
(SELECT
	de.Deposit_ID AS Trans_ID,
    de.Deposit_Date AS Trans_Date,
    'Withdrawal' AS Trans_Type,
    de.Deposit_Added_By AS Trans_Person_ID,
    CONCAT(us.fname,' ',us.lname) AS Trans_Person,
    de.Deposit_Notes AS Trans_Details,
    de.Deposit_From AS Trans_Reference_ID,
    cb.Bank_Name AS Trans_Reference,
    '0.00' AS Trans_Credit,
    de.Deposit_Amount AS Trans_Debit,
    de.Deposit_Add_Time AS Trans_Time
FROM checks_bank_deposits de
JOIN checks_bank cb
ON cb.Bank_ID=de.Deposit_From
JOIN users us 
ON us.UserID=de.Deposit_Added_By
WHERE de.Deposit_From=$BankID)
UNION
(SELECT
	sp.Supplier_Payment_ID AS Trans_ID,
    sp.Supplier_Payment_Date AS Trans_Date,
    'Payment' AS Trans_Type,
    sp.Supplier_Bill_Num AS Trans_Person_ID,
    sp.Supplier_Bill_Num AS Trans_Person,
    sp.Supplier_Payment_Comments AS Trans_Details,
    sp.Supplier_Payment_From_Account AS Trans_Reference_ID,
    cb.Bank_Name AS Trans_Reference,
    '0.00' AS Trans_Credit,
    sp.Supplier_Payment_Amount AS Trans_Debit,
    sp.Add_Date AS Trans_Time
FROM suppliers_payments sp
JOIN contacts co 
ON co.id=sp.Supplier_ID
JOIN checks_bank cb
ON cb.Bank_ID=sp.Supplier_Payment_Financial_Account
WHERE sp.Supplier_Payment_Financial_Account=$BankID)
ORDER BY Trans_Date ASC, Trans_Time ASC;";
                    
                    $TransactionsData = GDb::fetchRowSet($TransactionsSQL);
                    foreach($TransactionsData AS $Transaction) { 
                    
                    $TransPersonLink = "";
                    $TransPersonText = "";
                        
                    if($Transaction['Trans_Type'] == 'Payment') {
                        $TransPersonLink = "bills-edit.php?id=".$Transaction['Trans_Person_ID']."&action=edit";
                        $TransPersonText = "Bill #";
                    } elseif($Transaction['Trans_Type'] == 'Agent Payment') {
                        $TransPersonLink = "contacts-edit.php?id=".$Transaction['Trans_Person_ID']."&action=edit&usertype=agentPayments";
                    } elseif($Transaction['Trans_Type'] == 'Customer Payment') {
                        $TransPersonLink = "contacts-edit.php?id=".$Transaction['Trans_Person_ID']."&action=edit&usertype=sales";
                    } elseif($Transaction['Trans_Type'] == 'Customer Refund Payment') {
                        $TransPersonLink = "contacts-edit.php?id=".$Transaction['Trans_Person_ID']."&action=edit&usertype=sales";
                    } elseif($Transaction['Trans_Type'] == 'General Expense Payment') {
                        $TransPersonLink = "expenses-edit.php?id=".$Transaction['Trans_Person_ID']."&action=edit";
                    } elseif($Transaction['Trans_Type'] == 'Deposit' OR $Transaction['Trans_Type'] == 'Withdrawal') {
                        $TransPersonLink = "users-edit.php?id=".$Transaction['Trans_Person_ID'];
                    } else {
                        $TransPersonLink = "";
                        $TransPersonText = "";
                    }
                    ?>
                <tr>
                    <td><?php echo GUtils::clientDate($Transaction['Trans_Date']); ?></td>
                    <td><?php echo $Transaction['Trans_Type']; ?></td>
                    <td><?php echo "<a href='".$TransPersonLink."' target='_blank'>".$TransPersonText.$Transaction['Trans_Person']."</a>"; ?></td>
                    <td><?php if($Transaction['Trans_Credit'] > 0) {echo GUtils::formatMoney($Transaction['Trans_Credit']);} else { echo "--"; } $TotalCredit += $Transaction['Trans_Credit']; ?></td>
                    <td class="text-danger"><?php if($Transaction['Trans_Debit'] > 0) {echo GUtils::formatMoney($Transaction['Trans_Debit']);} else { echo "--"; }
                        $TotalDebit += $Transaction['Trans_Debit']; ?></td>
                    <td class="text-center"><?php if($Transaction['Trans_Type'] == 'Deposit' OR $Transaction['Trans_Type'] == 'Withdrawal') { ?>
                        <a href="javascript:void(0);" onclick='deleteConfirm( "contacts-edit.php?id=<?php echo $leadid; ?>&action=DepositDelete&Deposit=<?php echo $Transaction['Trans_ID']; ?>", "Delete")'><i class="fa fa-trash" aria-hidden="true"></i></a>
                        <?php } else { ?>
                        <i class="fa fa-trash" style="opacity:0.2" aria-hidden="true"></i>
                        <?php } ?></td>
                </tr>
                <?php } ?>
            </tbody>
            <tfoot>
                <tr style="border-top: double 3px #999;">
                    <td class="text-right" colspan="3"><b>Sub Totals:</b></td>
                    <td><?php echo GUtils::formatMoney($TotalCredit); ?></td>
                    <td><?php echo GUtils::formatMoney($TotalDebit); ?></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="text-right" colspan="4"><b>Balance:</b></td>
                    <td colspan="3"><?php echo GUtils::formatMoney($TotalCredit - $TotalDebit); ?></td>
                </tr>
            </tfoot>
        </table>
    </div>
    <?php } else { ?>
    <div class="form-group mt-4 col-sm-12 text-center">
        <label>This supplier doesn't have a financial account yet, please create one by clicking on "Refresh Data" button.</label><br />
        <small>(The refresh button will create a financial account, and update all the payments to be assigned to that account as well.)</small>
    </div>
    <?php } ?>
</div>



<div class="modal fade" id="AddDepositModal" tabindex="-1" role="dialog" style="top:50px;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
                <form id="DepositForm" name="DepositForm" class="not-common-form" method="post" action="inc/checks-functions.php">
                    <div class="row">
                        <div class="form-group col-sm-12">
                            <h3>Add a Deposit</h3>
                        </div>
                        <div class="col-sm-4">
                            <label class="float-label">Date of Deposit</label>
                            <input type="text" name="Deposit_Add" value="ADD" class="form-control" hidden>
                            <input type="text" name="Deposit_Type" value="<?php echo $leadid; ?>" class="form-control" hidden>
                            <input type="text" name="Deposit_To" value="<?php echo $BankID; ?>" class="form-control" hidden>
                            <input type="text" name="Deposit_Added_By" value="<?php echo $UserAdminID; ?>" class="form-control" hidden>
                            <input type="date" name="Deposit_Date" class="form-control" value="<?php echo date("Y-m-d"); ?>" required>
                        </div>
                        <div class="col-sm-4">
                            <label class="float-label">Amount of Deposit</label>
                            <div class="input-group">
                                <span class="input-group-prepend">
                                    <label class="input-group-text">$</label>
                                </span>
                                <input name="Deposit_Amount" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label class="float-label">From Account</label>
                            <select name="Deposit_From" class="form-control">
                                <option value="">Select</option>
                                <?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name FROM checks_bank WHERE Bank_Status=1 AND Bank_Type != 'supplier' ";
                                    $BanksData = GDb::fetchRowSet($GetBankAccountSQL);
                                    foreach ($BanksData AS $BankData) { ?>
                                <option value="<?php echo $BankData['Bank_ID']; ?>"><?php echo $BankData['Bank_Name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-sm-12">
                            <label class="float-label">Comments / Refrences</label>
                            <input type="text" name="Deposit_Notes" class="form-control">
                        </div>
                        <div class="col-sm-12"><br />
                            <button type="submit" name="AddDepositSubmit" class="btn waves-effect waves-light btn-success mr-3" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Add</button>
                            <button type="reset" class="btn waves-effect waves-light btn-inverse" data-dismiss="modal"><i class="fas fa-ban"></i>Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $("#DepositForm").submit(function() {
            // Getting the form ID
            var formID = $(this).attr('id');
            var formDetails = $('#' + formID);
            //To not let the form of uploading attachments included in this
            if (formID != 'contact6' && formID != 'idGroupEmailForm') {
                $.ajax({
                    type: "POST",
                    url: 'inc/checks-functions.php',
                    data: formDetails.serialize(),
                    success: function(data) {
                        // Inserting html into the result div
                        $('#results').html(data);
                        //$("form")[0].reset();
                        formmodified = 0;
                    },
                    error: function(jqXHR, text, error) {
                        // Displaying if there are any errors
                        $('#result').html(error);
                    }
                });
                return false;
            }
        });
    });
</script>