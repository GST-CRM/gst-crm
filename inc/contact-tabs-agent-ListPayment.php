<div class="row">
	<div class="form-group col-sm-12 m-0">
		<h4 class="sub-title">Agent Payment List</h4>
	</div>
	<div class="col-md-3">
		<div class="input-group input-group-sm mb-0">
			<span class="input-group-prepend mt-0">
				<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
			</span>
			<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
		</div>
	</div>
	<div class="col-md-9" style="margin-top:5px;">
		<div>
			<a href="#" class="btn btn-mat waves-effect waves-light btn-info float-right" style="padding: 3px 13px;" data-toggle="modal" data-target="#AgentPayments">New Agent Payment</a>
		</div>
	</div>
	<div class="form-group col-sm-12 m-0">
<table id="TableWithNoButtons" class="table table-hover table-striped table-smallz dataTable" style="width:100%">
        <thead>
            <tr>
                <th class="d-none">#</th>
                <th>Payment Date</th>
                <th>Group #</th>
                <th>Amount</th>
                <th>Payment Method</th>
                <th>Comments</th>
            </tr>
        </thead>
        <tbody>
			<?php 
			//To get the total of Agents - how much we paid for him so far 
			$TotalAgentBalancePaid = 0;
			
			$GetAllAgentPaymentsSQL = "SELECT ap.Agent_Payment_Date,ap.Agent_Group_ID,ap.Agent_Payment_Amount,ap.Agent_Payment_Method,ap.Agent_Payment_Comments
									FROM agents_payments ap
									WHERE ap.Agent_ID=$leadid AND ap.Agent_Status=1 ORDER BY Agent_Payment_Date DESC;";
			$GetAllAgentPaymentsResult = $db->query($GetAllAgentPaymentsSQL);
			if ($GetAllAgentPaymentsResult->num_rows > 0) { 
			$AgentsPaymentNum = 1;
			while($AllAgentPaymentsRow = $GetAllAgentPaymentsResult->fetch_assoc()) { ?>
            <tr>
                <td class="d-none"><?php echo $AgentsPaymentNum; ?></td>
                <td><?php echo date('m/d/Y', strtotime($AllAgentPaymentsRow['Agent_Payment_Date'])); ?></td>
                <td><a href="groups-edit.php?id=<?php echo $AllAgentPaymentsRow['Agent_Group_ID']; ?>&action=edit"><?php echo $AllAgentPaymentsRow['Agent_Group_ID']; ?></a></td>
                <td><?php echo GUtils::formatMoney($AllAgentPaymentsRow['Agent_Payment_Amount']); ?></td>
                <td><?php echo $AllAgentPaymentsRow['Agent_Payment_Method']; ?></td>
                <td><?php echo $AllAgentPaymentsRow['Agent_Payment_Comments']; ?></td>
            </tr>
			<?php $TotalAgentBalancePaid=$TotalAgentBalancePaid+$AllAgentPaymentsRow['Agent_Payment_Amount'];
			$AgentsPaymentNum++;
			}} else { ?>
            <tr>
                <td colspan="5" class="text-center">There are no payments sent to this agent yet.</td>
            </tr>
			<?php } ?>
        </tbody>
		<tfoot>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th style="text-align:right;">Total:</th>
                <th style="text-align:left;"><?php echo GUtils::formatMoney($TotalAgentBalancePaid); ?></th>
            </tr>
		</tfoot>
    </table>
	</div>
</div>
<script>
//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#TableWithNoButtons').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
});

$(document).ready(function() {
    $('#TableWithNoButtons').DataTable( {
        "ordering": false, //To disable the ordering of the tables
		"bLengthChange": false, //To hide the Show X entries dropdown
		dom: 'Bfrtip',
		sDom: 'lrtip', //To Hide the search box
        buttons: [] //To hide the export buttons
    } );
} );
</script>
