<?php

use PHPMailer\PHPMailer\PHPMailer ;

$groupID            = !empty($_REQUEST['groupid'])?$_REQUEST['groupid']:"";
$groupName          = !empty($_REQUEST['groupname'])?$_REQUEST['groupname']:"";
$emailTemplate      = !empty($_REQUEST['email_template'])?$_REQUEST['email_template']:"";
$subject            = !empty($_REQUEST['email_subject'])?$_REQUEST['email_subject']:"";
$message            = !empty($_REQUEST['email_message'])?$_REQUEST['email_message']:"";
$toEmails           = array();  

//To collect the data of the above Product id, and show it in the fields below
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {die("Connection failed: " . $conn->connect_error);} 

$statusCondition = ' AND status=1 ' ;
$statusSentFlag = 1;
if( isset($__REQUEST['activeMembers']) && $__REQUEST['activeMembers'] == 1 && isset($__REQUEST['inactiveMembers']) && $__REQUEST['inactiveMembers'] == 1 ) {
    $statusCondition = '' ;
    $statusSentFlag = null;
}
else if( isset($__REQUEST['inactiveMembers']) && $__REQUEST['inactiveMembers'] == 1 ) {
    $statusCondition = ' AND status=0 ' ;
    $statusSentFlag = 0;
}
$sql                = "SELECT * FROM customer_account WHERE tourid=$groupID $statusCondition";
$result             = $db->query($sql);

if ($result->num_rows > 0) 
{
    while ($customer_account = $result->fetch_assoc()) 
    {
        $contactid          = $customer_account["contactid"];
        $contactsql         = "SELECT id,email FROM contacts WHERE id=$contactid";
        $contactresult      = $db->query($contactsql);
        $contactcustomer    = $contactresult->fetch_assoc();
        $toEmails[]         = $contactcustomer["email"];
    }

    $toEmailString          = implode(',',$toEmails);
    $template               = (new Email_Templates())->getById($emailTemplate);
    $templateSubject        = $template['template_subject'];
    $templateBody           = str_replace('Â ', ' ', $template['template_body']);

    //Insert to groups_email table
    $insertsql              = "INSERT INTO groups_email(tourid,email_to,email_sent,template_code,email_subject,email_content) VALUES('$groupID','$toEmailString','1','$emailTemplate','$templateSubject','$templateBody')";
    $saveGroupEmail         = $db->query($insertsql);

    if (!empty($toEmails))
    {
        $config                 = [] ;
        $config['max_size']     = 50000000 ;
        $config['upload_path']  = 'groupattach/';
        $uploadFile             = false;
        if (!empty($_FILES['groupAttachment']['name']))
        {
            $uploadFile         = GUtils::doUpload('groupAttachment', $config) ;
        }
        $attachmentName         = (isset($config['file_name']) ? $config['file_name'] : '') ;
        $uploadPath             = (isset($config['upload_path']) ? $config['upload_path'] : '') ;
        $attachmentData         = "";

        if ($attachmentName)
        {
            $attachmentData     = __DIR__ ."/../uploads/groupattach/".$attachmentName;
        }

        sleep(5);


        $sql = "SELECT EmailAddress as admin_login FROM users WHERE Username = '".$_SESSION['login_user']."'";
        //echo $sql."\r\n";
        $sqlResult = mysqli_query($db, $sql) or die("database error:". mysqli_error($db));
        $row = mysqli_fetch_array($sqlResult,MYSQLI_ASSOC);
        $userLoginEmail[] = $row['admin_login'];

        $sqlOtherEmailList = "SELECT Staff_ID, agent, groupleader from groups where tourid = {$groupID}";
        //echo $sqlOtherEmailList."\r\n";
        $sqlOtherEmailListResult = mysqli_query($db, $sqlOtherEmailList) or die("database error:". mysqli_error($db));
        $sqlOtherEmailListRow = mysqli_fetch_array($sqlOtherEmailListResult,MYSQLI_ASSOC);

        if ($sqlOtherEmailListRow['Staff_ID'] != "") {
            $getEmailsForThisUsers = "SELECT EmailAddress FROM users WHERE UserID IN ({$sqlOtherEmailListRow['Staff_ID']})";
            //echo $getEmailsForThisUsers."\r\n";
            $getEmailsForThisUsersResult = mysqli_query($db, $getEmailsForThisUsers) or die("database error:". mysqli_error($db));
            while ($getEmailsForThisUsersRow = $getEmailsForThisUsersResult->fetch_assoc()) 
            {
                $userLoginEmail[] = $getEmailsForThisUsersRow['EmailAddress'];
        }
        }

        if ($sqlOtherEmailListRow['agent'] != "") {
            $getEmailsForThisAgents = "SELECT email FROM contacts WHERE id IN ({$sqlOtherEmailListRow['agent']})";
            //echo $getEmailsForThisAgents."\r\n";
            $getEmailsForThisAgentsResult = mysqli_query($db, $getEmailsForThisAgents) or die("database error:". mysqli_error($db));
            while ($getEmailsForThisAgentsRow = $getEmailsForThisAgentsResult->fetch_assoc()) 
            {
                $userLoginEmail[] = $getEmailsForThisAgentsRow['email'];
            }
        }

        if ($sqlOtherEmailListRow['groupleader'] != "") {
            $getEmailsForThisGLs = "SELECT email as email_1 FROM contacts WHERE id IN ({$sqlOtherEmailListRow['groupleader']})";
            //echo $getEmailsForThisGLs."\r\n";
            $getEmailsForThisGLsResult = mysqli_query($db, $getEmailsForThisGLs) or die("database error:". mysqli_error($db));
            while ($getEmailsForThisGLsRow = $getEmailsForThisGLsResult->fetch_assoc()) 
            {
                $userLoginEmail[] = $getEmailsForThisGLsRow['email_1'];
            }
        }


        if ( (!empty($_FILES['groupAttachment']['name']) && $uploadFile && file_exists($attachmentData)) || 
             (empty($_FILES['groupAttachment']['name']) && !$uploadFile) )
        {
            if ($emailTemplate)
            {
                //Send Email
                $params = array(
                    'groupname' => $groupName,
                );
                GUtils::sendTemplateMailById($toEmails, $emailTemplate, $params, $attachmentName, $attachmentData, $userLoginEmail) ;

            }
            else
            {
                GUtils::sendMail($toEmails, $subject, $message, $attachmentName, $attachmentData, $userLoginEmail) ;
            }
            if( $statusSentFlag === null ) {
                GUtils::setSuccess('Email sent to all participants in this group.') ;
            }
            else if( $statusSentFlag == 1 ) {
                GUtils::setSuccess('Email sent to all active participants in this group.') ;
            }
            else {
                GUtils::setSuccess('Email sent to all cancelled participants in this group.') ;
            }
        }
        else
        {
            GUtils::setWarning('Error in sending email. Try again later') ;
        }

        
    }
}

header("location:../groups-edit.php?id=".$groupID."&action=edit");
// echo json_encode("ok");
// exit;