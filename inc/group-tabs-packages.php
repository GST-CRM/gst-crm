<div class="col-sm-12">
	<h4 class="sub-title">Airfare Package <span style="font-style:italic;font-size:12px;">[Costs: $<?php echo $cost1['cost']; ?>.00]</span></h4>
	<table class="table table-hover table-striped table-smallz">
		<tbody>
			<?php 
			$parentidx = $data["product_air"];
			$sql = "SELECT * FROM products WHERE parentid=$parentidx AND subproduct=1";
			$result = $db->query($sql);
			if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
			<tr>
				<td style="width:25px;"><img src="<?php echo $crm_images; ?>/<?php echo $row["producttype"]; ?>.png" alt="<?php echo $row["producttype"]; ?>" style="width:22px; height:auto;" /></td>
				<td><a href="products-edit.php?id=<?php echo $row["id"]; ?>&action=edit" target="_blank"><?php echo $row["name"]; ?></a></td>
				<td><?php echo $row["meta1"]; ?></td>
				<td><?php echo $row["meta2"]; ?></td>
				<td><?php echo $row["meta3"]; ?></td>
			</tr>
			<?php }} else { ?>
			<tr>
				<td colspan="5">There are no subproducts for this package!</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<div class="col-sm-12"><br /></div>
<div class="col-sm-12">
	<h4 class="sub-title">Land Package <span style="font-style:italic;font-size:12px;">[Costs: $<?php echo $cost2['cost']; ?>.00]</span></h4>
	<table class="table table-hover table-striped table-smallz">
		<tbody>
			<?php 
			$parentidx2 = $data["product_land"];
			$sql = "SELECT * FROM products WHERE parentid=$parentidx2 AND subproduct=1";
			$result = $db->query($sql);
			if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
			<tr>
				<td style="width:25px;"><img src="<?php echo $crm_images; ?>/<?php echo $row["producttype"]; ?>.png" alt="<?php echo $row["producttype"]; ?>" style="width:22px; height:auto;" /></td>
				<td><a href="products-edit.php?id=<?php echo $row["id"]; ?>&action=edit" target="_blank"><?php echo $row["name"]; ?></a></td>
				<td><?php echo $row["meta1"]; ?></td>
				<td><?php echo $row["meta2"]; ?></td>
				<td><?php echo $row["meta3"]; ?></td>
			</tr>
			<?php }} else { ?>
			<tr>
				<td colspan="5">There are no subproducts for this package!</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<div class="col-sm-12"><br /></div>
<div class="col-sm-12">
	<h4 class="sub-title">Insurance Package <span style="font-style:italic;font-size:12px;">[Costs: $<?php echo $cost3['cost']; ?>.00]</span></h4>
	<table class="table table-hover table-striped table-smallz">
		<tbody>
			<?php 
			$parentidx3 = $data["product_insurance"];
			$sql = "SELECT * FROM products WHERE parentid=$parentidx3 AND subproduct=1";
			$result = $db->query($sql);
			if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
			<tr>
				<td style="width:25px;"><img src="<?php echo $crm_images; ?>/<?php echo $row["producttype"]; ?>.png" alt="<?php echo $row["producttype"]; ?>" style="width:22px; height:auto;" /></td>
				<td><a href="products-edit.php?id=<?php echo $row["id"]; ?>&action=edit" target="_blank"><?php echo $row["name"]; ?></a></td>
				<td><?php echo $row["meta1"]; ?></td>
				<td><?php echo $row["meta2"]; ?></td>
				<td><?php echo $row["meta3"]; ?></td>
			</tr>
			<?php }} else { ?>
			<tr>
				<td colspan="5">There are no subproducts for this package!</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
