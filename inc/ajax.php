<?php

include '../session.php' ;
include_once 'helpers.php' ;

error_reporting(0) ;
ini_set('display_errors', 0) ;

$action = $__REQUEST['action'] ;

if( $action == 'customer-details' ) {
    include 'ajax-customer-invoice-details.php' ;
}
if( $action == 'supplier-details' ) {
    include 'ajax-supplier-bill-details.php' ;
}
else if( $action == 'create-invoice' || $action == 'update-invoice' ) {
    include 'sales-invoice-save.php';
}
else if( $action == 'customer-list-html' ) {
    include 'ajax-group-customers.php' ;
}

else if( $action == 'reminder' ) {
    include 'sales-invoice-reminder.php' ;
        GUtils::jsonResponse(array('x' => 10)) ;
        die;
}
else if( $action == 'ajax-cancel-invoice' ) {
    include 'ajax-invoice-cancel-refund.php' ;
}
else if( $action == 'airline-list' ) {
    include 'ajax-airlines.php' ;
}
else if( $action == 'airport-list' ) {
    include 'ajax-airports.php' ;
}
else if( $action == 'airfare-list' ) {
    include 'ajax-airfare.php' ;
}
else if( $action == 'ajax-remove-customer' ) {
    $REVERSE = false ;
    include 'ajax-remove-customer.php' ;
}
else if( $action == 'ajax-restore-customer' ) {
    $REVERSE = true ;
    include 'ajax-remove-customer.php' ;
}
else if( $action == 'ajax-airline-products' ) {
    include 'ajax-airlines-products.php' ;
}
else if( $action == 'ajax-land-products' ) {
    include 'ajax-land-products.php' ;
}
else if( $action == 'ajax-current-customer' ) {
    include 'ajax-current-customer.php' ;
}
else if( $action == 'ajax-group-leaders' ) {
    include 'ajax-group-leaders.php' ;
}
else if( $action == 'ajax-the-tours' ) {
    include 'ajax-the-tours.php' ;
}
else if( $action == 'ajax-the-tours' ) {
    include 'ajax-the-tours.php' ;
}
else if( $action == 'ajax-agent-groups' ) {
    include 'ajax-agent-groups.php' ;
}