<?php

include "../session.php";
//include_once "inc/helpers.php";
include_once __DIR__ . '/../models/customer_payment.php';

require_once('../files/pdf/tcpdf.php');
include_once __DIR__ . '/../library/MYPDF2.php' ;

$printTitle = 'Tour Leader Report' ;

$PageTitle = "Tour Leader Report";

$tourid = $_REQUEST['id'];


$sql = "SELECT tourname FROM groups WHERE tourid=$tourid";
$queryGroup = mysqli_query($db, $sql) or die("error to fetch group name");
$groupRow = mysqli_fetch_row($queryGroup);
$groupName = $groupRow[0];

//Groupname And Leader name
//SELECT groups.tourname,contacts.fname,contacts.lname FROM groups JOIN contacts ON groups.groupleader=contacts.id AND groups.tourid=20050
$sql = "SELECT groups.tourid,groups.tourname,groups.land_productid,CONCAT(contacts.fname,' ', contacts.lname) AS leader_name,contacts.email,groups.airline_productid,SUM(products.price) AS trip_package
FROM groups 
LEFT JOIN products ON products.id = groups.land_productid OR products.id = groups.airline_productid 
LEFT JOIN contacts ON contacts.id = groups.groupleader
WHERE groups.tourid = $tourid";

$queryGroup = mysqli_query($db, $sql) or die("error to fetch group data");
$groupData = mysqli_fetch_assoc($queryGroup);


?>
<style>
td{
    font-size: 14px;
}
</style>
        <!-- [ page content ] start -->
        <div class="row">
            <div class="col-sm-12">
                <!-- HTML5 Export Buttons table start -->
                            <h3 class="text-center">Tour Leader Report</h3>
                            <a href="excel.php?type=tour_leader&name=<?php echo $groupName;?>&groupid=<?php echo $tourid; ?>" class="btn waves-effect waves-light btn-success" style="float:right;padding: 3px 13px;">Download</a>
                            <a href="javascript:void(0);" onclick = "return emailPDFconfirmTourLeader();" class="btn waves-effect waves-light btn-inverse mr-2" style="float:right;padding: 3px 13px;">Email pdf</a>
                    <?php //if( isset($_REQUEST['submit']) ) { ?>
						<br />
                        <h4><?php echo $groupName; ?></h4>
                        <h5>Tour Leader: <?php echo $groupData['leader_name']; ?></h5> <br>
                        <div class="dt-responsive table-responsive">
                            <table id="no-btn" class="datatable-report table gst-table table-hover table-striped table-bordered nowrap responsive" data-page-length="100" style="width:100%;">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Full Name</th>
                                    <th>Join Date</th>
                                    <th>Mobile Phone</th>
                                    <th>Email</th>
                                    <th>Passport</th>
                                    <th>Passport Expiry Date</th>
                                    <th>Paid to Date</th>
                                    <th>Balance</th>
                                    <th>Extras</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    
                                    $sql = "SELECT customer_account.status, contacts.id,contacts.title,contacts.fname,contacts.mname,contacts.lname,contacts.badgename,contacts.passport,
                                    contacts.expirydate,customer_account.createtime,customer_account.specialtext,customer_account.special_land_text,contacts.mobile,contacts.email FROM contacts 
                                    LEFT JOIN customer_account ON contacts.id=customer_account.contactid WHERE customer_account.tourid = $tourid "; /* "AND customer_account.status=1" removed to list canceled also */

                                    $queryContacts = mysqli_query($db, $sql) or die("error to fetch contacts data");

                                    //$queryContacts = GDb::fetchRowSet($sql);

                                    //While loop
                                    $no = 1;

                                    while( $contactsData = mysqli_fetch_assoc($queryContacts) ) {
                                        //foreach( $queryContacts as $contactsData ) { 

                                        $contactId = $contactsData['id'];
                                        $extras = $contactsData['specialtext']."<br>".$contactsData['special_land_text'];

                                        $invoiceData = (new CustomerPayment())->customerPaymentDetails($contactId,$tourid) ;
    
                                        $style='' ;
                                        $statusTitle ='' ;
                                        if( $contactsData['status'] == 0 ) {
                                            $style='style="color:red"' ;
                                            $statusTitle ='Canceled Customer' ;
                                        }
                                        ?>
                                        <tr <?php echo "title='$statusTitle' $style";?> >
                                        <td><?php echo $no; ?></td>
                                        <td><?php echo $contactsData['title']. " ".$contactsData['fname']. " ".$contactsData['mname']. " ".$contactsData['lname']; ?></td>
                                        <td><?php echo date('m/d/Y', strtotime($contactsData["createtime"]));?></td>
                                        <td><?php echo $contactsData['mobile']; ?></td>
                                        <td><?php echo $contactsData['email']; ?></td>
                                        <td><?php if($contactsData['passport'] != "") { echo $contactsData['passport']; } else { echo "Still Missing"; } ?></td>
                                        <td><?php echo date('m/d/Y', strtotime($contactsData["expirydate"]));?></td>
                                        <td><?php echo GUtils::formatMoney( $invoiceData['total']); ?></td>
                                        <td><?php echo GUtils::formatMoney( $invoiceData['balance'] ); ?></td>
                                        <td><?php echo $extras; ?></td>
                                        </tr>
                                        <?php
                                        $no++;

                                    }

                                    
                                    ?>

                                </tbody>
                                
                            </table>
                        </div>
                    <?php //} ?>
                <!-- HTML5 Export Buttons end -->
            </div>
        </div>

    <!-- [ page content ] end -->

<script>
    function emailPDFconfirmTourLeader(){
        var divExtra = "<div class='row mt-4'> "+
    //"<div class='text-center col-sm-12 h6'>Please choose or type recipient(s)</div>" +
    "<div class='col-sm-12'>" +
        "<input type='checkbox' name='tour-leader' value='tour-leader' /> Tour Leader &nbsp;&nbsp;" +
        "<input type='checkbox' name='staff' value='staff' /> Staff &nbsp;&nbsp;" +
        "<input type='checkbox' name='agent' value='agent' /> Agent &nbsp;&nbsp;" +
        "<input type='checkbox' name='user'  value='user'/> User &nbsp;&nbsp; <br/>" +
    "</div>" +
    "<div class='col-sm-12'>" +
            "<span class='text-right'>To Email </span>" +
            "<input type='text' name='additional' value='' class='form-control small-text'  placeholder='Comma separated email' />"+
    "</div>" +        
"</div>" ;
        divExtra = encodeURIComponent(divExtra) ;
        showConfirm( "groups-edit.php?action=edit&tab=reports&report_slug=To-Date&email_report=yes&name=<?php echo $groupName;?>&id=<?php echo $tourid; ?>", "Send", "Please choose or type recipient(s)", "Please Confirm", "modal-md", divExtra);
    }
    
//function emailPDFconfirmTourLeader(){
//    showConfirm( "groups-edit.php?action=edit&tab=reports&report_slug=To-Date&email_report=yes&name=<?php echo $groupName;?>&id=<?php echo $tourid; ?>", "Send", "Are you sure you want to sent this report to tour leader?", "Please Confirm", "modal-md");
//    showConfirm( "tour-leader-email-report.php?report_slug=To-Date&email_report=yes&name=<?php echo $groupName;?>&id=<?php echo $tourid; ?>", "Send", "Are you sure you want to sent this report to tour leader?", "Please Confirm", "modal-md");
//}
//showConfirm( "javascript:customerPaymentsListForm.submit();", "Yes", msg, "Please Confirm", "modal-md") ;
        $('#no-btn').DataTable({
            dom: 'Bfrtip',
            buttons:[]
        });
</script>
<?php include 'notificiations.php'; ?>
