<form  id="contact31" name="contact31" method="post" action="inc/group-functions.php">
    <div class="row">
                    <div class="form-group col-sm-12">
                        <h3>Add a new Sub Product / Land Arrangements</h3>
                    </div>
                    <div class="form-group col-sm-4">
                        <h4 class="sub-title">Product Status</h4>
						<div class="can-toggle">
							<input type="text" name="ParentProductID" value="<?php echo $data["land_productid"]; ?>" class="form-control" hidden>
							<input type="text" name="GroupCurrentID" value="<?php echo $data["tourid"]; ?>" class="form-control" hidden>
							<input id="e" name="status" value="1" type="checkbox" checked>
							<label for="e">
								<div class="can-toggle__switch" data-checked="Active" data-unchecked="Inactive"></div>
							</label>
						</div>
                    </div>
                    <div class="col-sm-4">
						<h4 class="sub-title">Product's Supplier</h4>
						<select name="thesupplier" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<?php //here, we are checking the Agent ID with the contact column list to show their names
							$SupplierSelected = "";
							$con7 = new mysqli($servername, $username, $password, $dbname);
							$result7 = mysqli_query($con7,"SELECT id, fname, mname, lname FROM contacts WHERE `usertype` LIKE '%2%'");
							while($row7 = mysqli_fetch_array($result7))
							{
								if($row7['id'] == $LandProData['supplierid']) {$SupplierSelected = "selected";} else {$SupplierSelected = "";}
								echo "<option value='".$row7['id']."' $SupplierSelected>".$row7['fname']." ".$row7['mname']." ".$row7['lname']."</option>";
							} ?>
						</select>
                    </div>
                    <div class="col-sm-4">
						<h4 class="sub-title">Product's Category</h4>
						<select name="thecategory" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
							<option value="2" selected>Land Products</option>
						</select>
                    </div>
					<div class="form-group form-default form-static-label col-sm-3">
						<label class="float-label">Product Type</label>
						<select name="producttype" id="producttype" class="form-control form-control-default">
							<option value="0" disabled selected></option>
							<option id="Hotels" value="Hotels">Hotels</option>
							<option id="Guides" value="Guides">Tour Guides</option>
							<option id="Restaurants" value="Restaurants">Restaurants</option>
						</select>
					</div>
                    <div class="form-group form-default form-static-label col-sm-3" id="subproduct_dropbox" style="display:none;">
                        <label class="float-label" id="SubProductName">Sub Product Name</label>
                        <label class="float-label" id="GuidezName" style="display:none;">Tour Guide Name</label>
                        <label class="float-label" id="HotelzName" style="display:none;">Hotel Name</label>
                        
                        <!-- <input type="text" name="subproductname" class="form-control" required=""> -->
                        <select name="subproductname" id="subproductname" class="js-example-basic-multiple-limit" multiple="multiple" onchange="getSubProductDetails();">
                            <span id="sub_product_name">
                            </span>
                        </select>
                    </div>

                    <div class="form-group form-default form-static-label col-sm-3" id="subproduct_textbox">
                        <label class="float-label" id="SubProductName">Sub Product Name</label>
                        <label class="float-label" id="GuidezName" style="display:none;">Tour Guide Name</label>
                        <label class="float-label" id="HotelzName" style="display:none;">Hotel Name</label>
                        <input type="text" name="subproductname_textbox" id="subproductname_textbox" class="form-control" required="">
                    </div>

                    <input type="hidden" name="check_which_item_selected" class="check_which_item_selected" value="0">

                    <div class="form-group form-default form-static-label col-sm-6">
                        <label class="float-label">Sub Product Description</label>
                        <input type="text" name="description" id="description" class="form-control">
                    </div>
                    <div id="hotelphone" class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label">Hotel's Phone</label>
                        <input type="text" name="hotelphone" id="hotelphonevalue" class="form-control">
                    </div>
                    <div id="hotelcity" class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label">Hotel's City</label>
                        <input type="text" name="hotelcity" id="hotelcityvalue" class="form-control">
                    </div>
                    <div id="checkindate" class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label">Check in Date</label>
                        <input type="date" name="checkindate" class="form-control">
                    </div>
                    <div id="checkoutdate" class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label">Check out Date</label>
                        <input type="date" name="checkoutdate" class="form-control">
                    </div>
                    <div id="GuidePhone" class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label">Tour Guide Phone #</label>
                        <input type="text" name="GuidePhone" id="GuidePhonevalue" class="form-control">
                    </div>
                    <div id="GuideCity" class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label">Tour Guide City</label>
                        <input type="text" name="GuideCity" id="GuideCityvalue" class="form-control">
                    </div>
                    <div class="col-sm-12"><br />
					<button type="submit" name="subsubmit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Add</button>
                    <button type="reset" class="btn waves-effect waves-light btn-inverse" data-dismiss="modal"><i class="fas fa-ban"></i>Cancel</button>
					</div>
	</div>
</form>

