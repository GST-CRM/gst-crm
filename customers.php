<?php
include "session.php";
$PageTitle = "Customer Accounts";
include "header.php"; 

$cStatus = $_GET['status'];
$cBirth = $_GET['birthday'];
$cPassport = $_GET['passport'];

include_once __DIR__ . '/models/acl_permission.php';
$contactAllowed = (new AclPermission())->isActionAllowed('Index', 'contact-edit.php') ;

?>
<!--<script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>	 -->
<style type="text/css">
#customers_list_filter { display:none;}
</style>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
		<div class="card">
            <div class="row">
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
					</div>
				</div>
				<div class="col-md-2" style="margin-top:15px;">
				</div>
				<div class="col-md-7" style="margin-top:15px;">
				<form id="AdvancedFilter" class="col-12 row" action="customers.php" method="GET">
					<div class="col-md-3 text-right pt-1">
						<b>Advanced Filter:</b>
					</div>
					<div class="form-check form-check-inline col-md-2">
						<input class="form-check-input" id="cbBIRTH" type="checkbox" name="birthday" value="NoBirth" <?php if(!empty($_GET['birthday']) AND $_GET['birthday'] == "NoBirth") {echo "checked";} ?>>
						<label class="form-check-label" for="cbBIRTH">No Birthday</label>
					</div>
					<div class="form-check form-check-inline col-md-2">
						<input class="form-check-input" id="cbPassport" type="checkbox" name="passport" value="NoPP" <?php if(!empty($_GET['passport']) AND $_GET['passport'] == "NoPP") {echo "checked";} ?>>
						<label class="form-check-label" for="cbPassport">No Passport</label>
					</div>
					<div class="col-md-3">
						<select id="status" name="status" class="form-control form-control-default fill">
							<option value="">Status</option>
							<option value="both" <?php if(!empty($_GET['status']) AND $_GET['status'] == "both") {echo "selected";} ?>>All</option>
							<option value="Active" <?php if(!empty($_GET['status']) AND $_GET['status'] == "Active") {echo "selected";} ?>>Active</option>
							<option hidden value="Cancelled" <?php if(!empty($_GET['status']) AND $_GET['status'] == "Cancelled") {echo "selected";} ?>>Cancelled</option>
							<option value="Disabled" <?php if(!empty($_GET['status']) AND $_GET['status'] == "Disabled") {echo "selected";} ?>>Disabled</option>
						</select>
					</div>
					<div class="col-md-1">
						<button type="submit" class="btn btn-info" style="padding: 5px 13px;">Filter</button>
					</div>
				</form>
				</div>
			</div>
            <div class="card-block">
				<div class="dt-responsive table-responsive">
                    <table id="customers_list" class="table table-hover responsive table-striped table-bordered nowrap" style="width:100%;" data-page-length="20">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>ContactID</th>
                                <th>Full Name</th>
                                <th>First Name</th>
                                <th>Middle Name</th>
                                <th>Last Name</th>
                                <th>Tour Code</th>
                                <th>Group Name</th>
                                <th>Phone Number</th>
                                <th>Email</th>
                                <th>Modified Date</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<!-- [ page content ] end -->
<script>
    <?php
    if( $contactAllowed ) {
        echo 'var CONTACT_EDIT=true;' ;
    }
    else {
        echo 'var CONTACT_EDIT=false;' ;
    }
    ?>
//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#customers_list').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
});


$( document ).ready(function() {
	$('#customers_list').DataTable({
		 "bProcessing": true,
         "serverSide": true,
		 "order": [[ 9, "desc" ]],
		 dom: 'Bfrtip',
		 "columnDefs": [
			  { 
			  //"visible": false,
			  "targets": 2,
				 "render": function(data, type, row, meta){
					if(type === 'display'){
                        if( ! CONTACT_EDIT) { data = row[2] + ' ' + row[3] + ' ' + row[4] + ' ' + row[5] ; }
                        else {
                            data = '<a href="contacts-edit.php?id=' + row[1] + '&action=edit&usertype=customer" style="color: #0000EE;">' + row[2] + ' ' + row[3] + ' ' + row[4] + ' ' + row[5] + '</a>';
                        }
					}
					return data;
				 }
			  },
			  { 
			  //"visible": false,
			  "targets": 6,
				 "render": function(data, type, row, meta){
					if(type === 'display'){
						data = '<a href="groups-edit.php?id=' + row[6] + '&action=edit" style="color: #0000EE;">T' + row[6] +'</a>';
					}
					return data;
				 }
			  },
			  { 
			  "targets": 7,
				 "render": function(data, type, row, meta){
					if(type === 'display'){
						data = '<a href="groups-edit.php?id=' + row[6] + '&action=edit&tab=participants" style="color: #0000EE;">' + row[7] + '</a>';
					}
					return data;
				 }
			  },
			  { 
			  "visible": false,
			  "targets": [0,1,3,4,5,6]
			  }
			  
		   ],
		 buttons: [],
         "ajax":{
            url :"inc/contact-customers-list.php?status=<?php echo isset($cStatus) ? $cStatus : '1';?>&cb=<?php echo isset($cBirth) ? $cBirth : '0';?>&cp=<?php echo isset($cPassport) ? $cPassport : '0';?>", // json datasource
            type: "post",  // type of method  , by default would be get
            error: function(){  // error handling code
              $("#customers_list_processing").css("display","none");
            }
          }
        });   
});
</script>
<?php include "footer.php"; ?>