<?php include "session.php";

include_once __DIR__ . '/models/agents.php' ;
include_once __DIR__ . '/models/users.php' ;
include_once __DIR__ . '/models/contacts.php' ;
include_once __DIR__ . '/models/group_leaders.php' ;
include_once __DIR__ . '/models/customer_payment.php';
include_once __DIR__ . '/models/customer_refund.php';
include_once __DIR__ . '/models/customer_groups.php';

//To make several url options in the same page
$allowed = array(
			'edit', 'update', 'delete',
			'docedit', 'docdelete',
			'DeletePayment','DepositDelete',
			'DeleteCreditNote',
			'makeagent', 'disableagent', 'enableagent',
			'makesupplier', 'disablesupplier', 'enablesupplier',
			'makeleader', 'disableleader', 'enableleader',
			'makeaccount'
		);
if ( ! isset($_GET['action'])) {header("location: contacts.php");die('Please go back to the main page.');}
$action = $_GET['action'];
if ( ! in_array($action, $allowed)) {header("location: contacts.php");die('Please go back to the main page.');}

$leadid = $_GET["id"];

$ciObj = new Contacts() ;
$cidata = $ciObj->get( ['id' => $leadid] ) ;
$agentEmail = $cidata['email'] ;
$userObj = new Users() ;
$userData = $userObj->get(['Username' => $agentEmail]) ;


//To collect the data of the above customer id, and show it in the fields below
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {die("Connection failed: " . $conn->connect_error);} 
$sql = "SELECT * FROM contacts WHERE id=$leadid";
$result = $conn->query($sql);
$data = $result->fetch_assoc();
$conn->close();

// Hay el general header information fe kol el pages
if( AclPermission::activeRoleId() == AclRole::$SUPPLIER ) {
    $PageTitle = "Contact - ".$data["title"]." ".$data["fname"]." ".$data["mname"]." ".$data["lname"];
}
else {
    $PageTitle = "Edit Contact - ".$data["title"]." ".$data["fname"]." ".$data["mname"]." ".$data["lname"];
}
include "header.php";


//Collect the full name of the contact to show it in the following notifications; Delete and Update
$contactname = $data['fname']." ".$data['mname']." ".$data['lname'];


//To redirect the admin and users directly to the tab by using the url
if(isset($_GET['usertype'])){
	$urlusertype = mysqli_real_escape_string($db, $_GET['usertype']);
} else {
	$urlusertype = "contact";
}
$tabactive1 = $tabactive2 = $tabactive2b = $tabactive3 = $tabactive3b = $tabactive3c = $tabactive4 = $tabactive5 = $tabactive6 = $SalesTabActive = '';
if($urlusertype == 'contact') {$tabactive1 = 'active';}
elseif ($urlusertype == 'agent') {$tabactive2 = 'active';}
elseif ($urlusertype == 'agentPayments') {$tabactive2b = 'active';}
elseif ($urlusertype == 'supplier') {$tabactive3 = 'active';}
//elseif ($urlusertype == 'supplierPayments') {$tabactive3b = 'active';}
elseif ($urlusertype == 'supplierBalance') {$tabactive3c = 'active';}
elseif ($urlusertype == 'customer') {$tabactive4 = 'active';}
elseif ($urlusertype == 'attachments') {$tabactive5 = 'active';}
elseif ($urlusertype == 'leader') {$tabactive6 = 'active';}
elseif ($urlusertype == 'sales') {$SalesTabActive = 'active';}
else {$tabactive1 = 'active'; $tabactive2 = $tabactive2b = $tabactive3 = $tabactive3b = $tabactive3c = $tabactive4 = $tabactive5 = $tabactive6 = $SalesTabActive = '';}



// To Create connection and collect the Agent data
$con6 = new mysqli($servername, $username, $password, $dbname);
if ($con6->connect_error) {die("Connection failed: " . $con6->connect_error);} 
$sql6 = "SELECT * FROM agents WHERE contactid=$leadid";
$result6 = $con6->query($sql6);
$agentdata = $result6->fetch_assoc();

if( $userData['Role_ID'] & AclRole::$AGENT ) {
    $agentdata["active"] = 1 ;
}
else {
    $agentdata["active"] = 0 ;
}
if( strlen($userData['Password']) > 0 ) {
    $agentPassword = HAS_PASSWORD ;
}
else {
    $agentPassword = NO_PASSWORD ;
    $agentdata["active"] = 0 ;
}

//leader data
$gl = new GroupLeaders() ;
$glData = $gl->get(['contactid' => $leadid]) ;
if( $userData['Role_ID'] & AclRole::$LEADER ) {
    $leaderdata["active"] = 1 ;
}
else {
    $leaderdata["active"] = 0 ;
}
$leaderdata["status"] = $glData['status'] ;
$leaderdata["id"] = $leadid;
$leaderdata["description"] = $glData['description'] ;

if( $userData['Role_ID'] & AclRole::$CUSTOMER) {
    $customerData["customeractive"] = 1 ;
}
else {
    $customerData["customeractive"] = 0 ;
}

$con6->close();


// To Create a Customer Account for the contact and redirects to the contacts list
if ($_GET['action'] == "makeaccount") {
	$usertypeupdate = "0";
	$thetourid = $_POST['thetourid'];
	date_default_timezone_set('America/Chicago');
	$NowDate = date("Y-m-d");
	if($data["usertype"] == 0){$usertypeupdate = "4";}
	else{$usertypeupdate = $data["usertype"].",4";}
	
		$sql2 = "SELECT (SELECT price FROM products WHERE id=airline_productid) AS Airline,(SELECT price FROM products WHERE id=land_productid) AS Land, startdate FROM groups WHERE tourid=$thetourid";
		$result2 = $db->query($sql2);
		$groupdata = $result2->fetch_assoc();
		$Group_price = $groupdata["Airline"] + $groupdata["Land"];
		$Due_Date = date('Y-m-d', strtotime($groupdata["startdate"]. ' - 90 days'));
		
	$sql14 = "INSERT INTO sales_order (Group_ID,Customer_Account_Customer_ID,Sales_Order_Date,Sales_Order_Stop,Total_Sale_Price) VALUES ('$thetourid','$leadid','$NowDate',0,'$Group_price');";
	$sql14 .= "INSERT INTO customer_account (contactid,status,tourid,Added_By) VALUES ('$leadid','1','$thetourid','$UserAdminID');";
	//$sql14 .= "INSERT INTO customer_invoice (Customer_Invoice_Date,Customer_Order_Num,Customer_Account_Customer_ID,Group_ID) VALUES (NOW(),'$SalesOrderCustomer','$leadid','$thetourid');";
	$sql14 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','customer account','$leadid','created','$contactname');";
	$sql14 .= "UPDATE contacts SET usertype='$usertypeupdate', updatetime=NOW() WHERE id=$leadid;";
	if ($db->multi_query($sql14) === TRUE) {
		// To Create connection and collect the Supplier data
		$NewSalesOrderNum = $db->insert_id;
		$GetSalesOrderConn = new mysqli($servername, $username, $password, $dbname);
		if ($GetSalesOrderConn->connect_error) {die("Connection failed: " . $GetSalesOrderConn->connect_error);} 
		$AddNewInvoiceSQL = "INSERT INTO customer_invoice (Customer_Invoice_Date,Customer_Order_Num,Sales_Order_Reference,Customer_Account_Customer_ID,Group_ID,Invoice_Amount,Due_Date) VALUES (NOW(),'$NewSalesOrderNum','$NewSalesOrderNum','$leadid','$thetourid','$Group_price','$Due_Date');";
		if ($GetSalesOrderConn->query($AddNewInvoiceSQL) === TRUE) {
			//Success
			$CusInv_id = $GetSalesOrderConn->insert_id;
			
			$CreateLinesConn = new mysqli($servername, $username, $password, $dbname);
			if ($CreateLinesConn->connect_error) {die("Connection failed: " . $CreateLinesConn->connect_error);} 

			//To collect the information of the Airline Product for this group
			$GetAirProductSQL = "SELECT pro.id,pro.name,pro.supplierid,pro.price
			FROM products pro 
			JOIN groups gro
			ON gro.airline_productid=pro.id
			WHERE gro.tourid=$thetourid";
			$GetAirProductResult = $CreateLinesConn->query($GetAirProductSQL);
			$GetAirProductCount = $GetAirProductResult->num_rows;
			if($GetAirProductCount > 0) {
				$GetAirProductData = $GetAirProductResult->fetch_assoc();
					$AirProductID = $GetAirProductData['id'];
					$AirProductName = $GetAirProductData['name'];
					$AirProductSupplier = $GetAirProductData['supplierid'];
					$AirProductPrice = $GetAirProductData['price'];
			}

			//To collect the information of the Land Product for this group
			$GetLandProductSQL = "SELECT pro.id,pro.name,pro.supplierid,pro.price
			FROM products pro 
			JOIN groups gro
			ON gro.land_productid=pro.id
			WHERE gro.tourid=$thetourid";
			$GetLandProductResult = $CreateLinesConn->query($GetLandProductSQL);
			$GetLandProductCount = $GetLandProductResult->num_rows;
			if($GetLandProductCount > 0) {
				$GetLandProductData = $GetLandProductResult->fetch_assoc();
					$LandProductID = $GetLandProductData['id'];
					$LandProductName = $GetLandProductData['name'];
					$LandProductSupplier = $GetLandProductData['supplierid'];
					$LandProductPrice = $GetLandProductData['price'];
			}
			
			//To fill the sales order line insert query for AIR and LAND
			$SaleOrderLineQuerySQL = "";
			
			//For Airline product and line
			if($GetAirProductCount > 0) {
				$SaleOrderLineQuerySQL .= "INSERT INTO sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
				VALUES ('$NewSalesOrderNum','Airline Package','$AirProductID','$AirProductName','$AirProductPrice','$AirProductSupplier');";
				$SaleOrderLineQuerySQL .= "INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
				VALUES ('$CusInv_id','1','1','$AirProductID','$AirProductName','1','$AirProductPrice');";
			}
			//For Land product and line
			if($GetLandProductCount > 0) {
				$SaleOrderLineQuerySQL .= "INSERT INTO sales_order_line (Sales_Order_Num,Line_Type,Product_Product_ID,Product_Name,Sales_Amount,Supplier_Supplier_ID)
				VALUES ('$NewSalesOrderNum','Land Package','$LandProductID','$LandProductName','$LandProductPrice','$LandProductSupplier');";
				$SaleOrderLineQuerySQL .= "INSERT INTO customer_invoice_line (Customer_Invoice_Num,Customer_Invoice_Line,Line_Type,Product_Product_ID,Product_Name,Qty,Invoice_Line_Total)
				VALUES ('$CusInv_id','2','2','$LandProductID','$LandProductName','1','$LandProductPrice');";
			}
			
			if ($CreateLinesConn->multi_query($SaleOrderLineQuerySQL) === TRUE) {
				echo "<div class='upload-loader-small'></div> Adding the contact $fullname to this group ...";
				echo "<meta http-equiv='refresh' content='2;url=groups-edit.php?id=$thetourid&action=edit&tab=participants'>";
			} else {
				echo $SaleOrderLineQuerySQL . "<br>" . $CreateLinesConn->error;
			}
		
			echo "<meta http-equiv='refresh' content='0;url=contacts-edit.php?id=$leadid&action=edit&usertype=customer'>";
		}
		$CreateLinesConn->close();
		$GetSalesOrderConn->close();
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error creating a customer account: " . $con14->error . " >> Create lines >> " . $CreateLinesConn->error . " >> Get Sales order >> " . $GetSalesOrderConn->error . "</strong></div>";
	}
}

//To Delete a supplier deposit
if(isset($_GET['action']) AND $_GET['action'] == 'DepositDelete') {
	$DepositID = mysqli_real_escape_string($db, $_GET['Deposit']);
	$DepositDelete = "DELETE FROM checks_bank_deposits WHERE Deposit_ID=$DepositID;";
	if(GDb::execute($DepositDelete)) {
		echo "<div class='alert alert-success background-success mt-4 mr-3 ml-3 mb-1'><button type='button' class='mt-1 close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected transaction record has been deleted successfully.</strong></div>";
	} else {
		echo "<div class='alert alert-danger background-danger mt-4 mr-3 ml-3 mb-1'><button type='button' class='mt-1 close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting record: " . $db->error . "</strong></div>";
	}
}

// To Create connection and collect the Supplier data
$con9 = new mysqli($servername, $username, $password, $dbname);
if ($con9->connect_error) {die("Connection failed: " . $con9->connect_error);} 
$sql9 = "SELECT * FROM suppliers WHERE contactid=$leadid";
$result9 = $con9->query($sql9);
$supplierdata = $result9->fetch_assoc();

//leader data

$sl= new Suppliers() ;
$slData = $gl->get(['contactid' => $leadid]) ;
if( $userData['Role_ID'] & AclRole::$SUPPLIER ) {
    $supplierdata["login_active"] = 1 ;
}
else {
    $supplierdata["login_active"] = 0 ;
}

$con9->close();


//To Delete the customer payment and show notification
if ($_GET['action'] == "DeletePayment") {
	$DeletedPaymentID = $_GET['PaymentID'];
	
	$con19 = new mysqli($servername, $username, $password, $dbname);
	if ($con19->connect_error) {die("Connection failed: " . $con19->connect_error);} 
	$sql19 = "DELETE FROM customer_payments WHERE Customer_Payment_ID=$DeletedPaymentID;";
	$sql19 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','payment for','$leadid','removed','$contactname');";
	if ($con19->multi_query($sql19) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The customer payment has been deleted successfully!</div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting record: " . $con19->error . "</strong></div>";
	}
	$con19->close();
}

//To Delete the customer credit note and show notification
if ($_GET['action'] == "DeleteCreditNote") {
	$CreditNoteID = $_GET['CreditNoteID'];
	
	$con19 = new mysqli($servername, $username, $password, $dbname);
	if ($con19->connect_error) {die("Connection failed: " . $con19->connect_error);} 
	$sql19 = "DELETE FROM customer_credit_note WHERE Note_ID=$CreditNoteID;";
	$sql19 .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','credit note for','$leadid','removed','$contactname');";
	if ($con19->multi_query($sql19) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The customer credit note has been deleted successfully!</div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting record: " . $con19->error . "</strong></div>";
	}
	$con19->close();
}


//To Delete the attachment document and show notification
if ($_GET['action'] == "docdelete") {
	$fileid = $_GET['fileid'];
	
	$con16 = new mysqli($servername, $username, $password, $dbname);
	if ($con16->connect_error) {die("Connection failed: " . $con16->connect_error);} 
	$sql16 = "SELECT * FROM media WHERE id=$fileid";
	$result16 = $con16->query($sql16);
	$attachmentdata = $result16->fetch_assoc();
	$con16->close();

	$mediaurl = $attachmentdata["mediaurl"];
	$uploadpath = "uploads/";
	$fileurl = $uploadpath . $mediaurl;
	
	$con17 = new mysqli($servername, $username, $password, $dbname);
	if ($con17->connect_error) {die("Connection failed: " . $con17->connect_error);} 
	$sql7 = "DELETE FROM media WHERE id=$fileid";
	if ($con17->query($sql7) === TRUE) {
		unlink($fileurl);
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The attachment has been removed successfully!</div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting record: " . $con4->error . "</strong></div>";
	}
	$con17->close();
}

//if the tour isn't mentioned in the customer account tab, show a notification
if (strpos($data["usertype"], '4') !== false) {
	$tourzsql = "SELECT * FROM customer_account WHERE contactid=$leadid";
	$tourzresult = $db->query($tourzsql);
	$tourmissing = $tourzresult->fetch_assoc();
	if($tourmissing['tourid'] == "") {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Warning: This contact must have a group assigned in the customer account section.</strong></div>";
	}
}
include_once('inc/contact-edit-commands.php');

?>
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->

<div class="row">
	<div class="col-md-9">
		<div class="card" style="overflow:hidden;">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs /*md-tabs*/" role="tablist">
				<li class="nav-item" style="display:none;">
					<a class="nav-link" data-toggle="tab" href="#home3" role="tab">View Contact</a>
					<div class="slide"></div>
				</li>
				
                <?php if( AclPermission::actionAllowed('Contact') ) { ?>
                <li class="nav-item">
					<a class="nav-link <?php echo $tabactive1; ?>" data-toggle="tab" href="#contacttab" role="tab" onclick="history.pushState('data to be passed', 'Contact Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=contact');">Edit Contact</a>
					<div class="slide"></div>
				</li>
                <?php } ?>
                
                <?php if( AclPermission::actionAllowed('ContactAgent') ) { ?>
				<?php if (strpos($data["usertype"], '1') !== false) { ?>
				<li class="nav-item">
					<a class="nav-link <?php echo $tabactive2; ?>" data-toggle="tab" href="#agenttab" role="tab" onclick="history.pushState('data to be passed', 'Contact Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=agent');">Edit Agent</a>
					<div class="slide"></div>
				</li>
				<li class="nav-item">
					<a class="nav-link <?php echo $tabactive2b; ?>" data-toggle="tab" href="#AgentPaymentstab" role="tab" onclick="history.pushState('data to be passed', 'Contact Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=agentPayments');">Agent's Payments</a>
					<div class="slide"></div>
				</li>
				<?php } ?>
				<?php } ?>
                
                <?php if( AclPermission::actionAllowed('ContactSupplier') ) { ?>
				<?php if (strpos($data["usertype"], '2') !== false) { ?>                
				<li class="nav-item">
					<a class="nav-link <?php echo $tabactive3; ?>" data-toggle="tab" href="#suppliertab" role="tab" onclick="history.pushState('data to be passed', 'Contact Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=supplier');">
                        <?php if( AclPermission::activeRoleId() == AclRole::$SUPPLIER ) { ?>
                        Supplier
                        <?php } else { ?>
                        Edit Supplier
                        <?php } ?>
                    </a>
					<div class="slide"></div>
				</li>
				<li class="nav-item d-none">
					<a class="nav-link <?php echo $tabactive3b; ?>" data-toggle="tab" href="#supplierPaymentstab" role="tab" onclick="history.pushState('data to be passed', 'Contact Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=supplierPayments');">Supplier's Payments</a>
					<div class="slide"></div>
				</li>
				<li class="nav-item">
					<a class="nav-link <?php echo $tabactive3c; ?>" data-toggle="tab" href="#supplierBalancetab" role="tab" onclick="history.pushState('data to be passed', 'Contact Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=supplierBalance');">Supplier's Balance</a>
					<div class="slide"></div>
				</li>
				<?php } ?>
				<?php } ?>
                
                <?php if( AclPermission::actionAllowed('ContactLeader') ) { ?>
				<?php if (strpos($data["usertype"], '3') !== false) { ?>
				<li class="nav-item">
					<a class="nav-link <?php echo $tabactive6; ?>" data-toggle="tab" href="#leadertab" role="tab" onclick="history.pushState('data to be passed', 'Contact Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=leader');">Edit Group Leader</a>
					<div class="slide"></div>
				</li>
				<?php } ?>
				<?php } ?>
                
                <?php if( AclPermission::actionAllowed('ContactCustomer') ) { ?>
				<?php if (strpos($data["usertype"], '4') !== false) { ?>
				<li class="nav-item">
					<a class="nav-link <?php echo $tabactive4; ?>" data-toggle="tab" href="#customertab" role="tab" onclick="history.pushState('data to be passed', 'Contact Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=customer');">Edit Customer</a>
					<div class="slide"></div>
				</li>
				<?php } ?>
				<?php if (strpos($data["usertype"], '4') !== false) { ?>
				<li class="nav-item">
					<a class="nav-link <?php echo $SalesTabActive; ?>" data-toggle="tab" href="#salestab" role="tab" onclick="history.pushState('data to be passed', 'Contact Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=sales');">Sales & Payments</a>
					<div class="slide"></div>
				</li>
				<?php } ?>
				<?php } ?>
                
                <?php if( AclPermission::actionAllowed('ContactAttachment') ) { ?>
				<li class="nav-item">
					<a class="nav-link <?php echo $tabactive5; ?>" data-toggle="tab" href="#attachments" role="tab" onclick="history.pushState('data to be passed', 'Contact Edit Page', 'https://<?php echo $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/"; ?>contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=attachments');">Attachments</a>
					<div class="slide"></div>
				</li>
                <?php } ?>
			</ul>
			<!-- Tab panes -->
			<div class="tab-content card-block">
				<div class="tab-pane form-material" id="home3" role="tabpanel" style="display:none;">
					<?php //include 'inc/contact-tabs-show-contact-info.php'; ?>
				</div>

				<div class="tab-pane <?php echo $tabactive1; ?>" id="contacttab" role="tabpanel">
					<?php include 'inc/contact-tabs-contact-info.php'; ?>
				</div>
                
				<?php if (strpos($data["usertype"], '1') !== false) { ?>
				<div class="tab-pane <?php echo $tabactive2; ?>" id="agenttab" role="tabpanel">
					<?php include 'inc/contact-tabs-agent.php'; ?>
				</div>
				<div class="tab-pane <?php echo $tabactive2b; ?>" id="AgentPaymentstab" role="tabpanel">
					<?php include 'inc/contact-tabs-agent-ListPayment.php'; ?>
				</div>
				<?php } ?>

                
                <?php if( AclPermission::actionAllowed('ContactSupplier') ) { ?>
				<?php if (strpos($data["usertype"], '2') !== false) { ?>
                    
                    <div class="tab-pane <?php echo $tabactive3; ?>" id="suppliertab" role="tabpanel">
                        <?php include 'inc/contact-tabs-supplier.php'; ?>
                    </div>
                    <div class="d-none tab-pane <?php echo $tabactive3b; ?>" id="supplierPaymentstab" role="tabpanel">
                        <?php //include 'inc/contact-tabs-supplier-payments.php'; ?>
                    </div>
                    <div class="tab-pane <?php echo $tabactive3c; ?>" id="supplierBalancetab" role="tabpanel">
                        <?php include 'inc/contact-tabs-supplier-balance.php'; ?>
                    </div>
                    <?php } ?>
				<?php } ?>
                
                
				<?php if (strpos($data["usertype"], '3') !== false) { ?>
				<div class="tab-pane <?php echo $tabactive6; ?>" id="leadertab" role="tabpanel">
					<?php include 'inc/contact-tabs-leader.php'; ?>
				</div>
				<?php } ?>
                
                
				<?php if (strpos($data["usertype"], '4') !== false) { ?>
				<div class="tab-pane <?php echo $tabactive4; ?>" id="customertab" role="tabpanel">
					<?php include 'inc/contact-tabs-customer.php'; ?>
				</div>
				<?php } ?>
                
                
				<?php if (strpos($data["usertype"], '4') !== false) { ?>
				<div class="tab-pane <?php echo $SalesTabActive; ?>" id="salestab" role="tabpanel">
					<?php include 'inc/contact-tabs-sales.php'; ?>
				</div>
				<?php } ?>
                
                
				<div class="tab-pane <?php echo $tabactive5; ?>" id="attachments" role="tabpanel">
					<?php include 'inc/contact-tabs-attachments.php'; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-3">
        
        <?php if( AclPermission::activeRoleId() != AclRole::$SUPPLIER ) { ?>
        
            <?php if (strpos($data["usertype"], '4') !== false) { ?>
            <div class="card">
                <div class="card-header">
                        <h5>Customer Billing Status</h5>
                </div>
                <div class="card-block row" style="padding-top: 0px;">
                    <?php
                    $GetInvoiceShortDetailsSQL = "SELECT
                                                    ci.Status AS Invoice_Status,
                                                    ci.Invoice_Sent,
                                                    ci.Customer_Invoice_Num,
                                                    ci.Group_ID,
                                                    ci.Customer_Account_Customer_ID,
                                                    ci.Group_Invoice,
                                                    ca.groupinvoice,
                                                    if(
                                                        ca.groupinvoice = 1, (
                                                            SELECT ci2.Customer_Invoice_Num
                                                            FROM customer_groups cg
                                                            JOIN customer_invoice ci2
                                                            ON ci2.Customer_Account_Customer_ID=cg.Primary_Customer_ID AND ci2.Group_ID=cg.Group_ID
                                                            WHERE (ci.Customer_Account_Customer_ID) IN (cg.Primary_Customer_ID,cg.Additional_Traveler_ID_1,cg.Additional_Traveler_ID_2,cg.Additional_Traveler_ID_3) AND cg.Type='Group Invoice' AND cg.Group_ID=ca.tourid
                                                        ) , ci.Customer_Invoice_Num
                                                    ) AS Customer_Invoice_Num,
                                                    co.email,
                                                    co.title, co.fname, co.mname, co.lname
                                                FROM customer_invoice ci
                                                LEFT JOIN customer_account ca 
                                                ON ca.contactid=ci.Customer_Account_Customer_ID AND ca.tourid=ci.Group_ID
                                                JOIN contacts co 
                                                    ON co.id=ci.Customer_Account_Customer_ID
                                                WHERE ci.Customer_Account_Customer_ID=$leadid
												ORDER BY ca.tourid ASC;";
                    $GetInvoiceShortDetailsResult = $db->query($GetInvoiceShortDetailsSQL);
                    $InvoiceCounterX = 1;
					$InvoicesList = NULL;
                    $invoiceIds = [] ;

                    $customerPayment = new CustomerPayment();
                    $customerRefund  = new CustomerRefund();
                    while($InvoiceShortDetails = $GetInvoiceShortDetailsResult->fetch_assoc()) {
                        $name = $InvoiceShortDetails['title'].' '.$InvoiceShortDetails['fname'].' '.$InvoiceShortDetails['mname'].' '.$InvoiceShortDetails['lname'];
                        $nameAppend = ((strlen(trim($name)) > 0) ? $name : '');
                        $name_email = '<b>' . $nameAppend  . (isset($InvoiceShortDetails['email']) ? ' ('.$InvoiceShortDetails['email'] .')' : '') . '</b>' ;

                        //Find Invoice details after consider all Deductions & Adjustments.
                        $oneGroupInvoiceDetail = $customerPayment->customerInvoiceDetails($InvoiceShortDetails['Customer_Invoice_Num']) ;
                        $cancellationInfo = CustomerRefund::calculateRefund($oneGroupInvoiceDetail['Invoice_Amount'], $oneGroupInvoiceDetail['charges'], $oneGroupInvoiceDetail["total"], $oneGroupInvoiceDetail['trip_start_days'], $oneGroupInvoiceDetail['Cancellation_Charge_Others'], $oneGroupInvoiceDetail['Customer_Invoice_Num']) ;
                        $hideCancelation = '' ;
                        if( $oneGroupInvoiceDetail['Invoice_Status'] == CustomerInvoice::$CANCELLED ) {
                            $hideCancelation = 'd-none' ;
                        }
                        //Disbale for primary customer.
                        $primaryCustomer = (new CustomerGroups())->isPrimaryTraveler($oneGroupInvoiceDetail["tourid"], $oneGroupInvoiceDetail["contactid"]) ;
                        $dim = '' ;
                        $title = '' ;
                        if( $primaryCustomer ) {
                            $dim = 'style="opacity:0.5;cursor: default;"' ;
                            $title = 'A Primary Traveller of Joint Invoice' ;
                        }
						
						//To get the invoices list if there are more than one invoice to get the Billing Balance
						$InvoicesList .=$InvoiceShortDetails["Customer_Invoice_Num"].",";
                        $invoiceIds[] = $InvoiceShortDetails["Customer_Invoice_Num"] ;
                    ?>
                    <div class="col-4 pb-3">
                        <div class="dropdown-info dropdown">
                            <button title="<?php echo $oneGroupInvoiceDetail['tourname'];?>" class="btn btn-mini btn-inverse dropdown-toggle waves-effect waves-light " type="button" id="dropdown-4" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Group <?php echo $InvoiceCounterX; ?>
                            </button>
                            <div class="dropdown-menu pt-0 pb-0" aria-labelledby="dropdown-4" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut" x-placement="top-start">
                                <a class="dropdown-item waves-light waves-effect pt-1 pb-1" href="groups-edit.php?id=<?php echo $InvoiceShortDetails["Group_ID"]; ?>&action=edit&tab=participants">View Group</a>
                                <a class="dropdown-item waves-light waves-effect pt-1 pb-1" href="contacts-edit.php?invid=<?php echo $InvoiceShortDetails["Customer_Invoice_Num"]; ?>&id=<?php echo $leadid; ?>&action=edit&usertype=sales">Edit Invoice</a>
                                <a <?php echo $dim;?> class="dropdown-item waves-light waves-effect pt-1 pb-1 <?php echo $hideCancelation;?>" href="javascript:void(0);" 
                                                                 data-name="<?php echo $oneGroupInvoiceDetail["title"]." ".$oneGroupInvoiceDetail["fname"]." ".$oneGroupInvoiceDetail["mname"]." ".$oneGroupInvoiceDetail["lname"]; ?>" 
                                                                 data-id="<?php echo $oneGroupInvoiceDetail["Customer_Invoice_Num"]; ?>"
                                                                 data-tourid="<?php echo $oneGroupInvoiceDetail["tourid"]; ?>"
                                                                 data-redirect-url="" 
                                                                 data-cancel-amount="<?php echo GUtils::formatMoney( $cancellationInfo['Cancellation_Charge']);?>" 
                                                                 data-cancel-refund="<?php echo GUtils::formatMoney( $cancellationInfo['Refund_Amount']) ;?>" 
                                                                 data-cancel-outcome="<?php echo $cancellationInfo['Cancellation_Outcome'];?>" 
                                                                 data-cancel-paid="<?php echo ($oneGroupInvoiceDetail['paid'] - $oneGroupInvoiceDetail['charges']);?>" 
                                                                 <?php if( ! $primaryCustomer ) { ?>
                                                               onclick = 'removeTripCustomerHandler(this)'
                                                                 <?php } ?>
                                                               >Cancel & Refund</a>
                                </a>
                                <a class="dropdown-item waves-light waves-effect pt-1 pb-1" target="_blank" href="sales-invoice-print.php?print=invoice&invid=<?php echo $InvoiceShortDetails["Customer_Invoice_Num"]; ?>">Print Invoice</a>
                                <a class="dropdown-item waves-light waves-effect pt-1 pb-1" href="javascript:void(0);" onclick = 'showConfirm( "sales-invoices.php?invaction=send&invid=<?php echo $InvoiceShortDetails["Customer_Invoice_Num"]; ?>", "Send", "Are you sure you want to sent this email to <?php echo htmlspecialchars($name_email, ENT_QUOTES);?> ?", "Please Confirm", "modal-md")'>
                                    <?php if( $InvoiceShortDetails['Invoice_Sent'] > 0 ) { echo 'Invoice Sent (' . $InvoiceShortDetails['Invoice_Sent'] . ')'; } else { echo 'Send Invoice'; } ?>
                                </a>
                                <a class="dropdown-item waves-light waves-effect pt-1 pb-1" href="javascript:void(0);" data-name="<?php echo $name; ?>" data-id="<?php echo $InvoiceShortDetails["Customer_Invoice_Num"]; ?>" data-email="<?php echo $InvoiceShortDetails['email'];?>" onclick = 'sendReminderHandler(this)'>Send Reminder</a>
                                <a class="dropdown-item waves-light waves-effect pt-1 pb-1" href="credit_notes_add.php?invid=<?php echo $InvoiceShortDetails["Customer_Invoice_Num"]; ?>">Credit Note</a>
                                <a class="d-none dropdown-item waves-light waves-effect pt-1 pb-1" href="javascript:void(0);" onclick = 'deleteConfirm( "sales-invoices.php?invaction=delete&invid=<?php echo $InvoiceShortDetails["Customer_Invoice_Num"]; ?>", "Delete")'>Delete Invoice</a>
                                <a class="d-none dropdown-item waves-light waves-effect pt-1 pb-1" href="javascript:void(0);" onclick = 'deleteConfirm( "sales-invoices.php?invaction=void&invid=<?php echo $InvoiceShortDetails["Customer_Invoice_Num"]; ?>", "Void")' >Void Invoice</a>
                            </div>
                        </div>
                    </div>
                    <?php $InvoiceCounterX++; } ?>
                    <script>
                        function sendReminderHandler(obj) {
                            $("#showSendReminder").modal("show");

                            $('.gst-reminder-id').val( $(obj).data('id') ) ;
                            $('.gst-reminder-class').html( $(obj).data('name') ) ;
                            $('.gst-reminder-email').val( $(obj).data('email') ) ;
                            $('.gst-reminder-subject').val( '' ) ;
                            $('.gst-reminder-message').val( '' ) ;
                        }
                    </script>
					
					<?php 	
                    
                        $billingStatus = (new CustomerPayment())->CustomerBillingStatus($invoiceIds) ;
						$InvoicesList = substr($InvoicesList, 0, -1);
                        
                        $InvoicesListA = explode(',', $InvoicesList) ;
                        $InvoicesListA = array_filter($InvoicesListA)  ;
                        $InvoicesList = implode(',', $InvoicesListA) ;
                                                
                         $tempQueryInvoiceList = ($InvoicesList) ? $InvoicesList : 0 ;

						$CustomerBillingSQL = "SELECT (SELECT SUM(cil.Qty * cil.Invoice_Line_Total) as Total FROM customer_invoice_line cil WHERE cil.Customer_Invoice_Num IN ($tempQueryInvoiceList)) AS Total_Owed,
						(SELECT SUM(Customer_Payment_Amount) FROM customer_payments WHERE Customer_Invoice_Num IN ($tempQueryInvoiceList)) AS Total_Paid, (SELECT SUM(charges) FROM customer_payments WHERE Customer_Account_Customer_ID = $leadid) AS Charges";

                        $CustomerBillingResult = $db->query($CustomerBillingSQL);
						$CustomerBillingData = $CustomerBillingResult->fetch_assoc();
						if($CustomerBillingData['Total_Owed']>0){ $TotalOwedBalance = $CustomerBillingData['Total_Owed']; } else { $TotalOwedBalance = 0.00;}
						if($CustomerBillingData['Total_Paid']>0){ $TotalPaidBalance = $CustomerBillingData['Total_Paid']; } else { $TotalPaidBalance = 0.00;}
						if($CustomerBillingData['Charges']>0){ $TotalCharges = $CustomerBillingData['Charges']; } else { $TotalCharges = 0.00;}
					?>
                    <div class="col-sm-12">
                        <h6>Owed: <span style="font-weight:400;">
                        <?php echo GUtils::formatMoney( $billingStatus['owed'] ); ?>
                        </span></h6>
                        <h6>Paid: <span style="font-weight:400;"><?php echo GUtils::formatMoney($billingStatus['paid']); ?></span></h6>
                        <h6>Remaining: <span style="font-weight:400;"><?php echo GUtils::formatMoney($billingStatus['remaining']); ?></span></h6>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php if (strpos($data["usertype"], '1') !== false) { ?>
            <div class="card">
                <div class="card-header">
                    <h5>Agent Billing Status</h5>
                </div>
                <div class="card-block row" style="padding-top: 0px;">
                    <div class="col-sm-12">
                        <h6>Owed: <span style="font-weight:400;"><?php echo GUtils::formatMoney($TotalAgentBalanceOwed); ?></span></h6>
                        <h6>Paid: <span style="font-weight:400;"><?php echo GUtils::formatMoney($TotalAgentBalancePaid); ?></span></h6>
                        <?php if($TotalAgentBalanceOwed < $TotalAgentBalancePaid) {$DangerStyle = "class='text-danger'";} else {$DangerStyle="";} ?>
                        <h6>Balance: <span <?php echo $DangerStyle; ?> style="font-weight:400;"><?php echo GUtils::formatMoney($TotalAgentBalanceOwed-$TotalAgentBalancePaid); ?></span></h6>
                    </div>
                </div>
            </div>
            <?php } ?>

        <?php } ?>
        
		<?php if (strpos($data["usertype"], '2') !== false) { ?>
		<div class="card">
			<div class="card-header">
				<h5>Supplier Billing Status</h5>
			</div>
			<div class="card-block row" style="padding-top: 0px;">
				<div class="col-sm-12">
					<h6>Owed: <span style="font-weight:400;"><?php echo GUtils::formatMoney($TotalExpensesAllGroups); ?></span></h6>
					<h6>Paid: <span style="font-weight:400;"><?php echo GUtils::formatMoney($TotalPaidAllGroups); ?></span></h6>
					<?php if($TotalExpensesAllGroups < $TotalPaidAllGroups) {$DangerStyle = "class='text-danger'";} else {$DangerStyle="";} ?>
					<h6>Balance: <span <?php echo $DangerStyle; ?> style="font-weight:400;"><?php echo GUtils::formatMoney($TotalExpensesAllGroups-$TotalPaidAllGroups); ?></span></h6>
				</div>
			</div>
		</div>
		<?php } ?>
        
        <?php if( AclPermission::activeRoleId() != AclRole::$SUPPLIER ) { ?>
        
		<div class="card">
			<div class="card-header">
				<h5>Advanced Actions</h5>
			</div>
			<div class="card-block row" style="padding-top:0px;">
			<?php /* If the contact is an agent */
			if (strpos($data["usertype"], '1') !== false) {
			if ($agentdata["status"] == 1) {
				//If the contact is agent and is enabled
				$Button_Agent_Class = "success btn-square";
				$Button_Agent_Action = "disableagent";
				$Button_Agent_Label = "Disable the Agent";
				$Button_Agent_Text = "Already An Agent";
			} else {
				//If the contact is agent and is disabled
				$Button_Agent_Class = "inverse btn-square";
				$Button_Agent_Action = "enableagent";
				$Button_Agent_Label = "Enable the Agent";
				$Button_Agent_Text = "The Agent is Disabled";
			}
			} else {
				//If the contact is not an agent
				$Button_Agent_Class = "inverse btn-outline-inverse";
				$Button_Agent_Action = "makeagent";
				$Button_Agent_Label = "Upgrade to an Agent";
				$Button_Agent_Text = "Upgrade to Agent";
			} ?>
			<a class="btn waves-effect waves-light btn-<?php echo $Button_Agent_Class; ?> btn-block" href="javascript:void(0);" onclick = 'deleteConfirm( "contacts-edit.php?id=<?php echo $leadid; ?>&action=<?php echo $Button_Agent_Action; ?>", "<?php echo $Button_Agent_Label; ?>")' role="button"><i class="feather icon-users"></i> <?php echo $Button_Agent_Text; ?></a>
			
			
			
			<?php /* If the contact is a supplier */
			if (strpos($data["usertype"], '2') !== false) {
			if ($supplierdata["status"] == 1) {
				//If the contact is Supplier and is enabled
				$Button_Supplier_Class = "success btn-square";
				$Button_Supplier_Action = "disablesupplier";
				$Button_Supplier_Label = "Disable the Supplier";
				$Button_Supplier_Text = "Already A Supplier";
			} else {
				//If the contact is Supplier and is disabled
				$Button_Supplier_Class = "inverse btn-square";
				$Button_Supplier_Action = "enablesupplier";
				$Button_Supplier_Label = "Enable the Supplier";
				$Button_Supplier_Text = "The Supplier is Disabled";
			}
			} else {
				//If the contact is not a Supplier
				$Button_Supplier_Class = "inverse btn-outline-inverse";
				$Button_Supplier_Action = "makesupplier";
				$Button_Supplier_Label = "Upgrade to Supplier";
				$Button_Supplier_Text = "Upgrade to Supplier";
			} ?>
			<a class="btn waves-effect waves-light btn-<?php echo $Button_Supplier_Class; ?> btn-block" href="javascript:void(0);" onclick = 'deleteConfirm( "contacts-edit.php?id=<?php echo $leadid; ?>&action=<?php echo $Button_Supplier_Action; ?>", "<?php echo $Button_Supplier_Label; ?>")' role="button"><i class="feather icon-users"></i> <?php echo $Button_Supplier_Text; ?></a>
			
			
			
			<?php /* If the contact is a leader */
			if (strpos($data["usertype"], '3') !== false) {
			if ($leaderdata["status"] == 1) {
				//If the contact is Leader and is enabled
				$Button_Leader_Class = "success btn-square";
				$Button_Leader_Action = "disableleader";
				$Button_Leader_Label = "Disable the Leader";
				$Button_Leader_Text = "Already A Group Leader";
			} else {
				//If the contact is Leader and is disabled
				$Button_Leader_Class = "inverse btn-square";
				$Button_Leader_Action = "enableleader";
				$Button_Leader_Label = "Enable the Leader";
				$Button_Leader_Text = "Group Leader is Disabled";
			}
			} else {
				//If the contact is not a Leader
				$Button_Leader_Class = "inverse btn-outline-inverse";
				$Button_Leader_Action = "makeleader";
				$Button_Leader_Label = "Upgrade to Leader";
				$Button_Leader_Text = "Upgrade to a Leader";
			} ?>
			<a class="btn waves-effect waves-light btn-<?php echo $Button_Leader_Class; ?> btn-block" href="javascript:void(0);" onclick = 'deleteConfirm( "contacts-edit.php?id=<?php echo $leadid; ?>&action=<?php echo $Button_Leader_Action; ?>", "<?php echo $Button_Leader_Label; ?>")' role="button"><i class="feather icon-sunrise"></i> <?php echo $Button_Leader_Text; ?></a>

			<?php /* If the contact is a Customer Account */
			//if (strpos($data["usertype"], '4') !== false) {
			//	echo "<a href='javascript:void(0);' class='btn waves-effect waves-light btn-success btn-square btn-block'><i class='feather icon-sunrise'></i> Has a Customer Account</a>"; }
			//else {
			//	echo "<a href='contacts-edit.php?id=$leadid&action=makeaccount' class='btn waves-effect waves-light btn-inverse btn-outline-inverse btn-block'><i class='feather icon-user-check'></i> Create an Account</a>";
			//} ?>
			<?php /* If the contact is a Customer Account */
				if (strpos($data["usertype"], '4') !== false) {
					echo "<a href='javascript:void(0);' class='btn waves-effect waves-light btn-success btn-square btn-block'><i class='feather icon-sunrise'></i> Has a Customer Account</a>"; }
				else {
					echo "<a href='javascript:void(0);' class='btn waves-effect waves-light btn-inverse btn-outline-inverse btn-block' data-toggle='modal' data-target='#makecustomersmodal'><i class='feather icon-user-check'></i> Create an Account</a>";
			} ?>
				<button type="button" class="btn waves-effect waves-light btn-danger btn-outline-danger btn-block" data-toggle="modal" data-target="#delete"><i class="feather icon-user-x"></i> Delete Contact</button>
			</div>
		</div>
        
        
	<?php
            //Find attachments for this contact from all groups.
            $sqla = "SELECT Customer_Payment_ID, Customer_Payment_Date, Attachment, Customer_Payment_Comments, Add_Date FROM customer_payments 
                WHERE Customer_Account_Customer_ID=$leadid AND (Attachment IS NOT NULL AND LENGTH(TRIM(Attachment)) > 0)"; 
            $records = GDb::fetchRowSet($sqla) ;
            if( !is_array($records)) {
                $records = [] ;
            }
            //Find attachments for this contact from all groups.
            $sqla = "SELECT Customer_Invoice_Num, Customer_Invoice_Date, Attachment, Comments, Add_Date FROM customer_invoice 
                WHERE Customer_Account_Customer_ID=$leadid AND (Attachment IS NOT NULL AND LENGTH(TRIM(Attachment)) > 0)"; 
            $recordsInv = GDb::fetchRowSet($sqla) ;
            if( !is_array($recordsInv)) {
                $recordsInv = [] ;
            }
            //Find attachments for this contact from all groups.
            $sqla = "SELECT Refund_ID, Refund_Date, Attachment, Cancelation_Reason, Add_Date FROM customer_refund
                WHERE Customer_Account_Customer_ID=$leadid AND (Attachment IS NOT NULL AND LENGTH(TRIM(Attachment)) > 0)"; 
            $recordsRefund = GDb::fetchRowSet($sqla) ;
            if( !is_array($recordsRefund)) {
                $recordsRefund = [] ;
            }
            
	$sql = "select * from media WHERE userid='$leadid'";
	$result = $db->query($sql);
	if ($result->num_rows > 0 || count($records) > 0|| count($recordsInv) > 0 ) { ?>
	<div class="card">
		<div class="card-header" style="padding-bottom:10px;">
			<h5>Attachments</h5>
		</div>
		<div class="card-block" style="padding-top:0px;">
		<?php while($row = $result->fetch_assoc()) { ?>
			<img src="<?php echo $crm_images; ?>/attachment.png" alt="Attachments" style="width:20px; height:20px;" /> <a href="contacts-edit.php?id=<?php echo $leadid; ?>&usertype=attachments&fileid=<?php echo $row["id"]; ?>&action=docedit" target="_blank" style="color: #0000EE;"><?php echo $row["name"]; ?></a>
			<br />
		<?php }
                foreach( $records as $row ) { ?>
                        <img src="<?php echo $crm_images; ?>/attachment.png" alt="Attachments" style="width:20px; height:20px;" /> <a href="contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=attachments&fileid=<?php echo $row["Customer_Payment_ID"]; ?>&action=docedit&mode=paymentView" target="_blank" style="color: #0000EE;">
                            <?php 
                            if( $row['Customer_Payment_Comments'] ) {
                                echo $row["Customer_Payment_Comments"] ;
                            }
                            else {
                                echo 'Payment_' . GUtils::clientDate($row["Customer_Payment_Date"]);
                            }
                            ?>
                        </a>
                        <br />
                        <?php
                }
                foreach( $recordsInv as $row ) { ?>
                        <img src="<?php echo $crm_images; ?>/attachment.png" alt="Attachments" style="width:20px; height:20px;" /> <a href="contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=attachments&fileid=<?php echo $row["Customer_Invoice_Num"]; ?>&action=docedit&mode=invoiceView" target="_blank" style="color: #0000EE;">
                            <?php
                            if( $row['Comments'] ) {
                                echo $row["Comments"] ;
                            }
                            else {
                                echo 'Invoice_' . GUtils::clientDate($row["Customer_Invoice_Date"]);
                            }
                            ?>
                        </a>
                        <br />
                        <?php
                }
                
                foreach( $recordsRefund as $row ) { ?>
                        <img src="<?php echo $crm_images; ?>/attachment.png" alt="Attachments" style="width:20px; height:20px;" /> <a href="contacts-edit.php?id=<?php echo $leadid; ?>&action=edit&usertype=attachments&fileid=<?php echo $row["Refund_ID"]; ?>&action=docedit&mode=refundView" target="_blank" style="color: #0000EE;">
                            <?php
                            if( $row['Cancelation_Reason'] ) {
                                echo 'Refund - '. $row["Cancelation_Reason"] ;
                            }
                            else {
                                echo 'Refund_' . GUtils::clientDate($row["Refund_Date"]);
                            }
                            ?>
                        </a>
                        <br />
                        <?php
                }
                ?>
		</div>
	</div>
	<?php } ?>
	<div class="card">
		<div class="card-header" style="padding-bottom:10px;">
			<a data-toggle="collapse" href="#collapse1"><h5>Customer Change log</h5></a>
		</div>
		<div class="card-block" style="padding-top:0px; padding-bottom: 5px;">
			<ul id="collapse1" class="panel-collapse collapse">
			<?php
			date_default_timezone_set('America/Chicago');
			$UsersLog = "select * from logs WHERE contactid='$leadid' ORDER BY time DESC";
			$UsersLogResult = $db->query($UsersLog);
			if ($UsersLogResult->num_rows > 0) { ?>
			<?php while($UsersLogRow = $UsersLogResult->fetch_assoc()) { ?>
				<li style="border-bottom:solid 1px #ccc;font-size:12px;padding-bottom:5px;padding-top:5px;">
					<a href="javascript:void(0)" data-id="<?php echo $leadid; ?>" data-type="Customer" data-date="<?php echo $UsersLogRow["time"]; ?>" class="changelogline" id="name_badges_tab" data-toggle="modal" data-target="#ChangeLogTrack">
						<strong style="font-weight:bold;"><?php echo $UsersLogRow['username']; ?></strong> updated this contact
						<br /><small>on <?php
						$LogzTime = new DateTime($UsersLogRow["time"]);
						echo date('m/d/Y - h:i A',($LogzTime->getTimestamp()-28800)); //28800 seconds means 8 hours
						?></small>
					</a>
				</li>
			<?php }} ?>
			</ul>
		</div>
	</div>
        
        
        <?php } ?>
</div>
<?php include 'inc/notificiations.php'; ?>

<?php require('inc/metadata-js.php'); ?>
<script type="text/javascript">
	$(document).ready(function() {
		/*setTimeout(function(){
			$("#name_badges_tab").trigger("click");
		},100);*/
		
	    $('.changelogline').click(function() {
	    	$('#ChangeLogTrackDiv').html("<div class='spinner'></div>");
			var type 		= $(this).attr("data-type");
			var id 			= $(this).attr("data-id");
			var ChangeDate 	= $(this).attr("data-date");
			$.ajax({
				type 	: "POST",
				data 	: {"type":type, "id":id, "ChangeDate":ChangeDate},
				url 	: 'inc/contact-tabs-changelog.php',
				success: function (data) {
					$('#ChangeLogTrackDiv').html(data);
				}
			});
			
	    })
	});

$(document).ready(function() {
	$("form:not(.not-common-form)").submit(function() {
        $('#results').html('');
		// Getting the form ID
		var  formID = $(this).attr('id');
		var formDetails = $('#'+formID);
		//To not let the form of uploading attachments included in this
		if (formID != 'contact6') {
			$.ajax({
				type: "POST",
				url: 'inc/contact-functions.php',
				data: formDetails.serialize(),
				success: function (data) {	
					// Inserting html into the result div
					$('#results').html(data);
					$('.confirmation-message-result-area').html(data);
					//$("form")[0].reset();
					formmodified = 0;
				},
				error: function(jqXHR, text, error){
				// Displaying if there are any errors
					$('#result').html(error);           
				}
			});
			return false;
		}
	});
});


function uploaddisablealert() {
  formmodified=0;
}

$(document).ready(function() {
    formmodified=0;
    $('form *').change(function(){
        formmodified=1;
    });
    window.onbeforeunload = confirmExit;
    function confirmExit() {
        if (formmodified == 1) {
            return "New information not saved. Do you wish to leave the page?";
        }
    }
});
</script>
<!-- sweet alert js -->
<script type="text/javascript" src="files/bower_components/sweetalert/js/sweetalert.min.js"></script>
<!-- modalEffects js nifty modal window effects -->
<script type="text/javascript" src="files/assets/js/modalEffects.js"></script>
<?php include "footer.php"; ?>