<?php include "session.php";
$PageTitle = "Edit Sales Order";

//To make several url options in the same page
$allowed = array('edit', 'extra');
$action = mysqli_real_escape_string($db, $_GET['action']);
$SalesOrderID = mysqli_real_escape_string($db, $_GET['id']);
if (isset($_GET['group'])) {$HasGroupRecord = mysqli_real_escape_string($db, $_GET['group']);} else { $HasGroupRecord = 0; }
if (!isset($action)) {header("location: sales.php");die('Please go back to the main page.');}
if (!in_array($action, $allowed)) {header("location: sales.php");die('Please go back to the main page.');}


include "header.php";
include 'inc/sales-order-functions.php';


$WhereCustomer = "";

//If the form has been submitted but there was error, to get the post, or read from GET action
if(!empty($SalesOrderID) AND $SalesOrderID > 0) {
	$GetSalesOrderDataSQL = "SELECT
							so.Sales_Order_Num,
							so.Customer_Account_Customer_ID, 
							co.fname,
							co.mname,
							co.lname,
							co.email,
							co.address1,
							co.address2,
							co.city,
							co.state,
							co.zipcode,
							so.Sales_Order_Date, 
							so.Sales_Order_Due_Date, 
							so.Sales_Order_Stop, 
							so.Sales_Order_Comments, 
							so.Total_Sale_Price, 
							so.Sales_Order_Attachment
						FROM sales_order so
						JOIN contacts co 
						ON co.id=so.Customer_Account_Customer_ID
						WHERE so.Sales_Order_Num='$SalesOrderID'";
	$GetSalesOrderData = GDb::fetchRowSet($GetSalesOrderDataSQL);
	$CustomerzDataID = $GetSalesOrderData[0]['Customer_Account_Customer_ID'];
	$CustomerzDataName = $GetSalesOrderData[0]['fname']." ".$GetSalesOrderData[0]['mname']." ".$GetSalesOrderData[0]['lname'];
	$CustomerzDataEmail = $GetSalesOrderData[0]['email'];
	$CustomerzDataAddress = $GetSalesOrderData[0]['address1']."\n".$GetSalesOrderData[0]['address2']."\n".$GetSalesOrderData[0]['city'].", ".$GetSalesOrderData[0]['state'].", ".$GetSalesOrderData[0]['zipcode'];
	$Sales_Order_Date = $GetSalesOrderData[0]['Sales_Order_Date'];
	$Sales_Order_Due_Date = $GetSalesOrderData[0]['Sales_Order_Due_Date'];
	$Total_Sale_Price = GUtils::formatMoney($GetSalesOrderData[0]['Total_Sale_Price']);
	$Sales_Order_Comments = $GetSalesOrderData[0]['Sales_Order_Comments'];
	$Sales_Order_Attachment = $GetSalesOrderData[0]['Sales_Order_Attachment'];
	$Sales_Order_Stop = $GetSalesOrderData[0]['Sales_Order_Stop'];
	if($Sales_Order_Stop == 1) { $DisabledFlag = "disabled"; } else { $DisabledFlag = ""; }
	if($HasGroupRecord == 1) { $DisabledFlag = "disabled"; } else { $DisabledFlag = ""; }
	
}
?>
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {background:white;color:#333;padding:2px 30px 3px 20px;border-radius:4px;}
.select2-container--default .select2-selection--single .select2-selection__arrow {top:4px;}
.select2-container--default .select2-selection--single .select2-selection__arrow b {border-color:black transparent transparent transparent;}
.select2-selection {height:35px;padding:0px !important;}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-block">
				<form name="contact5" action="" method="POST" class="row" id="contact5" enctype="multipart/form-data">
                    <div class="form-default form-static-label col-sm-3">
						<input type="hidden" id="submitBtn" name="submitBtn" value="" >
                        <label id="CustomerNameLabel" for="SpecificCustomer" class="float-label">Customer Name</label>
						<input value="EDIT" name="SalesOrderType" type="hidden">
						<input value="<?php echo $SalesOrderID; ?>" name="SalesOrderID" type="hidden">
						<a href="contacts-edit.php?id=<?php echo $CustomerzDataID; ?>&action=edit"><input <?php echo $DisabledFlag; ?> type="text" class="form-control" value="<?php echo $CustomerzDataName; ?>" style="cursor: pointer;" readonly></input></a>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-2">
                        <label class="float-label gst-label" for="Sales_Order_Date">Sales Order Date</label>
                        <input <?php echo $DisabledFlag; ?> type="date" id="Sales_Order_Date" name="Sales_Order_Date" value="<?php echo $Sales_Order_Date; ?>" class="form-control date-picker" required>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-2">
                        <label class="float-label gst-label" for="Sales_Order_Due_Date">Sales Order Due Date</label>
                        <input <?php echo $DisabledFlag; ?> type="date" id="Sales_Order_Due_Date" name="Sales_Order_Due_Date" value="<?php echo $Sales_Order_Due_Date; ?>" class="form-control" required>
                    </div>
                    <div class="form-default form-static-label col-sm-3 text-center">
						<?php if($Sales_Order_Stop==1) { echo "<h1><i style='font-size:70px;' class='text-danger fas fa-exclamation-triangle'></i></h1>";} ?>
						<?php if($HasGroupRecord==1) { echo "<h1><i style='font-size:70px;' class='text-primary fas fa-info-circle'></i></h1>";} ?>
                    </div>
                    <div class="form-default form-static-label col-sm-2 text-center">
                        <h5>Amount</h5>
                        <h3 class="text-danger" id="SupplierBillTotal"><?php echo $Total_Sale_Price; ?></h3>
						<input name="SalesOrderTotal" type="hidden" value="" class="HiddenTotal">
						<?php if($HasGroupRecord == 1) { ?>
								<a class="btn btn-primary btn-sm" href="contacts-edit.php?id=<?php echo $CustomerzDataID; ?>&action=edit&usertype=sales" role="button">View Invoice</a>
						<?php } else { ?>
							<?php if($Sales_Order_Stop==1) { ?>
								<a class="btn btn-primary btn-sm" href="javascript:void(0);" onclick = 'deleteConfirm( "sales.php?action=unvoid&SalesID=<?php echo $SalesOrderID; ?>", "Unvoid")' role="button">Unvoid</a>
							<?php } else { ?>
								<a class="btn btn-success btn-sm" href="javascript:void(0);" onclick = 'showConfirm( "sales.php?action=CreateInvoice&SalesID=<?php echo $SalesOrderID; ?>&CustomerID=<?php echo $CustomerzDataID; ?>", "Create Invoice", "An invoice will be created for this sales order.", "Please Confirm")'>Create an Invoice</a>
								<a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick = 'deleteConfirm( "sales.php?action=void&SalesID=<?php echo $SalesOrderID; ?>", "Void")' role="button">Void</a>
							<?php } ?>
						<?php } ?>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label" for="BillingAddress">Billing Address</label>
						<textarea <?php echo $DisabledFlag; ?> id="BillingAddress" class="form-control" rows="3" readonly><?php echo $CustomerzDataAddress; ?></textarea>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label gst-label" for="EmailAddress">Email Address</label>
						<input <?php echo $DisabledFlag; ?> type="email" id="EmailAddress" value="<?php echo $CustomerzDataEmail; ?>" class="form-control" readonly>
                    </div>
                    <div class="form-default form-static-label col-sm-3 text-center">
						<?php if($Sales_Order_Stop==1) { echo "<h5 class='text-danger'>This record is</h5><h3 class='text-danger'>Voided</h3>";} ?>
						<?php if($HasGroupRecord==1) { echo "<h5 class='text-primary'>This record is for a</h5><h3 class='text-primary'>customer in a group</h3>";} ?>
                    </div>
                    <div class="form-default form-static-label col-sm-3 text-center"></div>
                    <div class="col-sm-12"><br /></div>
                    <div class="col-sm-12 table-responsive">
					<h4 class="sub-title">Sales Order Details</h4>
						<table class="table table-hover table-striped table-bordered nowrap" id="item_table">
							<thead>
								<tr>
									<th width="30" class="text-center">#</th>
									<th width="20%">Product / Service</th>
									<th>Description</th>
									<th width="100">Quantity</th>
									<th width="150">Amount</th>
									<th width="150">Total</th>
									<th width="50"><button <?php echo $DisabledFlag; ?> type="button" name="add" class="btn btn-success btn-sm add"><i class="fas fa-plus mr-0"></i></button></th>
								</tr>
							</thead>
							<tbody>
							<?php
							$sql = "SELECT
										sol.Sales_Order_Num, 
										sol.Product_Product_ID, 
										pro.name, 
										pro.description,
										sol.Sales_Quantity,
										pro.price, 
										pro.supplierid,
										pro.categoryid
									FROM sales_order_line sol
									JOIN products pro 
									ON pro.id=sol.Product_Product_ID
									WHERE sol.Sales_Order_Num='$SalesOrderID'";
							$result = $db->query($sql);

							//To make the list of products in PHP Variable
							$ProductSelect = '';
							$ProductSelect .= '<option value="0">Select a Product</option>';
							$ProductsListResult = mysqli_query($db,"SELECT * FROM products WHERE status=1");
							while($SupplierProducts = mysqli_fetch_array($ProductsListResult)) {
								$ProductName = str_replace("'"," ",$SupplierProducts['name']);
								$ProductDescription = str_replace("'"," ",$SupplierProducts['description']);
								$ProductSelect .='<option value="'.$SupplierProducts["id"].'" data-description="' . $ProductDescription . '" data-cost="' . $SupplierProducts['price'] . '" data-quantity="0" data-category="' . $SupplierProducts['categoryid'] . '" data-name="' .$ProductName. '">'.$ProductDescription.'</option>';
							}
							
							$TableCounter = 1;
							if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
								<tr class="bill-line-tr">
									<td class="text-center"><?php echo $TableCounter; ?><input type="hidden" name="exp_id[]" class="form-control exp_id" value="<?php echo $TableCounter; ?>" /></td>
									<td>
										<select readonly id="Product_<?php echo $TableCounter; ?>" name="SO_Product[]" class="form-control Bill_Line_Product">
											<option value="<?php echo $row['Product_Product_ID']; ?>" data-description="<?php echo $row['description']; ?>" data-cost="<?php echo $row['price']; ?>" data-quantity="<?php echo $row['Sales_Quantity']; ?>" selected><?php echo $row['name']; ?></option>
										</select>
									</td>
									<td><span class="product_desc"><?php echo $row['description']; ?></span></td>
									<td><input type="number" onchange="Bill_Line_Qty_Change(this)" name="SO_line_quantity[]" class="form-control SO_line_quantity" value="<?php echo $row['Sales_Quantity']; ?>" /></td>
									<td><span class="product_price"><?php echo GUtils::formatMoney($row['price']); ?></td>
									<td><span class="bill_line_total"><?php echo GUtils::formatMoney($row['Sales_Quantity']*$row['price']); ?></span></td>
									<td><button <?php echo $DisabledFlag; ?> type="button" name="remove" class="btn btn-danger btn-sm remove"><i class="fas fa-trash-alt mr-0"></i></button></td>
									<td style="display:none;">
										<input type="text" class="product_name" name="product_name[]" value="<?php echo $row['name']; ?>">
										<input type="text" class="product_price" name="product_price[]" value="<?php echo $row['price']; ?>">
										<input type="text" class="product_category" name="product_category[]" value="<?php echo $row['categoryid']; ?>">
									</td>
								</tr>
							<?php $TableCounter++; }} ?>
							</tbody>
							<tfoot>
								<tr>
									<th colspan="5" class="pt-2 pb-2 text-right">Total</th>
									<th colspan="2" class="pt-2 pb-2 FooterTotal"><?php echo $Total_Sale_Price; ?></th>
								</tr>
							</tfoot>
						</table>
					</div>
                    <div class="col-sm-12"><br /></div>
					<div class="form-group form-default form-static-label col-md-4">
                        <label class="float-label" for="Sales_Order_Comments">Sales Order Comments</label>
                        <textarea <?php echo $DisabledFlag; ?> id="Sales_Order_Comments" rows="6" name="Sales_Order_Comments" class="form-control"><?php echo $Sales_Order_Comments; ?></textarea>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label gst-label" for="Sales_Order_Attachment">Attachment <?php if($Sales_Order_Attachment != NULL) { echo "<a href='uploads/".$Sales_Order_Attachment."' target='_blank' style='margin-left:100px;' download><i class='fas fa-cloud-download-alt'></i> <small>Download the attachment</small></a>"; } ?></label>
                        <div class="Attachment_Box">
                            <input <?php echo $DisabledFlag; ?> type="file" id="Sales_Order_Attachment" name="fileToUpload" class="Attachment_Input" >
                            <p id="Sales_Order_Attachment_Text" class="Attachment_Text">Drag the file here or click in this area.</p>
                        </div>
                    </div>
                    <div class="col-sm-12"><br /><br />
						<button <?php echo $DisabledFlag; ?> type="submit" name="addcontact" value="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Submit</button>
                        <button <?php echo $DisabledFlag; ?> type="reset" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Clear</button>
					</div>
				</form>
			</div>
        </div>
    </div>
</div>
<script type="text/javascript">
//To make the numbers as a currency with two decimels
const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 2
})

//This is to make the attachment box update its content when a file is selected
$(document).ready(function(){
  $('#Sales_Order_Attachment').change(function () {
    $('#Sales_Order_Attachment_Text').text("A file has been selected");
  });
});

//This is to get the details of each product and show it in that line
function BillLineSelected(obj) {
	var val = $(obj).val();
	var description = $(obj).find("option[value='" + val + "']").data('description');
	var cost = $(obj).find("option[value='" + val + "']").data('cost');
	var quantity = $(obj).find("option[value='" + val + "']").data('quantity');
	var name = $(obj).find("option[value='" + val + "']").data('name');
	var category = $(obj).find("option[value='" + val + "']").data('category');
	$(obj).closest('.bill-line-tr').find('.product_desc').html(description);
	$(obj).closest('.bill-line-tr').find('.product_price').html(formatMoney(cost));
	$(obj).closest('.bill-line-tr').find('.product_price').val(cost);
	$(obj).closest('.bill-line-tr').find('.product_name').val(name);
	$(obj).closest('.bill-line-tr').find('.product_category').val(category);
	$(obj).closest('.bill-line-tr').find('.SO_line_quantity').val(quantity);
	var BillLineTotal = quantity * cost;
	$(obj).closest('.bill-line-tr').find('.bill_line_total').html(formatMoney(BillLineTotal));
	$(obj).closest('.bill-line-tr').find('.bill_line_total').data('cost', BillLineTotal);

	Bill_Line_Qty_Change($(obj).closest('.bill-line-tr').find('.SO_line_quantity'));
}


function Bill_Line_Qty_Change(textobj) {
	var qty = $(textobj).val();
	var sel = $(textobj).closest('.bill-line-tr').find('.Bill_Line_Product');
	var val = $(sel).val();

	var cost = $(sel).find("option[value='" + val + "']").data('cost');
	$(sel).closest('.bill-line-tr').find('.product_price').html(formatMoney(cost));
	var TotalLineCost = cost * qty;

	$(sel).closest('.bill-line-tr').find('.bill_line_total').html(formatMoney(TotalLineCost));
	$(sel).closest('.bill-line-tr').find('.bill_line_total').data('cost', TotalLineCost);

	updateTotal();
}

function updateTotal() {
	var total = 0;

	$('.bill-line-tr .bill_line_total').each(function () {
		total += parseFloat($(this).data('cost'));
	});
	$('#SupplierBillTotal').html(formatMoney(total));
	$('.FooterTotal').html(formatMoney(total));
	$('.HiddenTotal').val(total);

}

//To be able to add new rows or remove old ones
$(document).ready(function(){
	var TDnum = <?php echo $TableCounter; ?>;
	$(document).on('click', '.add', function(){
		//if ($("#SpecificCustomer option").is(":selected")) {
		var html = '';
		html += '<tr class="bill-line-tr">';
		html += '<td class="text-center">'+ TDnum +'<input type="hidden" name="exp_id[]" class="form-control exp_id" value="'+ TDnum +'" /></td>';
		html += '<td><select id="Product_'+ TDnum +'" name="SO_Product[]" class="form-control Bill_Line_Product js-example-basic-single select2" onchange="BillLineSelected(this)">';
		html += '<?php echo $ProductSelect; ?>';
		html += '</select></td>';
		html += '<td><span class="product_desc"></span></td>';
		html += '<td><input type="number" onchange="Bill_Line_Qty_Change(this)" name="SO_line_quantity[]" class="form-control SO_line_quantity" value="" /></td>';
		html += '<td><span class="product_price"></td>';
		html += '<td><span class="bill_line_total"></span></td>';
		html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm remove"><i class="fas fa-trash-alt mr-0"></i></button></td>';
		html += '<td style="display:none;"><input type="text" class="product_name" name="product_name[]" value="">';
		html += '<input type="text" class="product_price" name="product_price[]" value="">';
		html += '<input type="text" class="product_category" name="product_category[]" value="">';
		html += '</td></tr>';
		$('#item_table').append(html);
		
		$('#Product_'+ TDnum).select2({closeOnSelect: true});
		$(".invalid-feedback").css("display", "none");
		TDnum++;
	});

	$(document).on('click', '.remove', function(){
		$(this).closest('tr').remove();
	});

    $('.btn').click(function() {
          var buttonval    = $(this).attr('data-val');
          $("#submitBtn").val(buttonval);
    }) 
});
</script>
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>