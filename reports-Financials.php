<?php
include 'session.php';

$PageTitle = 'Group Financial Report';
include 'header.php';
include_once __DIR__ . '/models/Financial-Report.php' ;

//This page is showing too many PHP notices because the variables aren't indexed
//but they can't be indexed, so i am ignoring them from this line
error_reporting(E_ALL & ~E_NOTICE);

// Get the values from the main page first
if(isset($_GET['Group'])) {$GroupSelected = mysqli_real_escape_string($db, $_GET['Group']);}
if(isset($_GET['Month'])) {$MonthFilter = mysqli_real_escape_string($db, $_GET['Month']);}
if(isset($_GET['Year'])) {$YearFilter = mysqli_real_escape_string($db, $_GET['Year']);}

// Show the Table with the results if the filter is active
if (!empty($GroupSelected) AND $GroupSelected != 0) {
    $GroupzDataSQL = 'SELECT tourid,tourdesc,tourname FROM groups WHERE tourid='.$GroupSelected;
    $GroupzData = GDb::fetchRow($GroupzDataSQL);
    $GroupzDataID = $GroupzData['tourid'];
    $GroupzDataRef = $GroupzData['tourdesc'];
    $GroupzDataName = $GroupzData['tourname'];
    $ResultType = "Group";
	$ShowTheResults = true;
} elseif(!empty($MonthFilter)) {
    $YearMonthData = explode('-', $MonthFilter);
    $Filter_Year = $YearMonthData[0];
    $Filter_Month = $YearMonthData[1];
    $ResultType = "Monthly";
	$ShowTheResults = true;
} elseif(!empty($YearFilter)) {
    $Filter_Year = $YearFilter;
    $ResultType = "Yearly";
	$ShowTheResults = true;
} else {
	$ShowTheResults = false;
}

$FinancialReport = new FinancialReport();

?>
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {background:white;color:#333;padding:5px 30px 4px 20px;border-radius:4px;}
.select2-container--default .select2-selection--single .select2-selection__arrow {top:6px;}
.select2-container--default .select2-selection--single .select2-selection__arrow b {border-color:black transparent transparent transparent;}
</style>
<div class='row'>
    <div class='col-sm-12'>
        <div class='card'>
            <form class="row mb-3" action="reports-Financials.php" method="GET">
                <?php if($ShowTheResults === true) {
                    $DisplayStyle="";
                    $DisplayStyle2="";
                    $DisplayCol = "col-md-3";
                } else {
                    $DisplayStyle="display:none;";
                    $DisplayStyle2="padding-left:45px;";
                    $DisplayCol = "col-md-6";
                } ?>
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;<?php echo $DisplayStyle; ?>">
				    <label class="float-label" for="SearchField">Search for a Specific Record</label>
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input id="SearchField" type="text" class="outsideBorderSearch form-control" placeholder="Search...">
					</div>
				</div>
				<div class="<?php echo $DisplayCol; ?>" style="margin-top:15px;<?php echo $DisplayStyle2; ?>">
                    <label class="float-label" for="SpecificGroup">specific Group Report</label>
                    <select id="SpecificGroup" name="Group" class="col-sm-12">
                        <option value='0'>-</option>
                    </select>
				</div>
				<div class="col-md-6" style="margin-top:15px;">
					<div class="col-12 row">
						<div class="form-group form-default form-static-label col-md-5">
							<label class="float-label" for="Monthly">Monthly Groups Report</label>
							<input class="form-control" id="Monthly" type="month" name="Month">
						</div>
						<div class="form-group form-default form-static-label col-md-5">
							<label class="float-label" for="Yearly">Yearly Groups Report</label>
							<select id="Yearly" class="form-control" name="Year">
                                <option value="0"></option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                                <option value="2021">2021</option>
                                <option value="2022">2022</option>
                                <option value="2023">2023</option>
                                <option value="2024">2024</option>
                                <option value="2025">2025</option>
                                <option value="2026">2026</option>
                                <option value="2027">2027</option>
                                <option value="2028">2028</option>
                                <option value="2029">2029</option>
                                <option value="2030">2030</option>
                            </select>
						</div>
						<div class="form-group form-default form-static-label col-md-2 pr-0">
							<hr style="border: 0;height: 1px;background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0));margin-top: 14px;margin-bottom: 13px;" />
							<button type="submit" class="btn btn-info btn-block waves-effect waves-light" style="padding: 5px 13px;">Filter</button>
						</div>
					</div>
				</div>
			</form>
            <?php if($ShowTheResults === true AND $ResultType == "Group") { ?>
            <div class='card-block pt-0'>
                <?php echo "<h4 class='pb-3'>Group Financial Report: <a style='font-size:20px;' href='groups-edit.php?id=".$GroupzDataID."&action=edit'>".$GroupzDataRef." | ".$GroupzDataName."</a></h4>"; ?>
                <div class="dt-responsive table-responsive " style="position: relative;">
                    <table id="TableWithNoButtons" class="table table-striped table-bordered nowrap" style="width:100%;" data-page-length="500">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Customer Name</th>
                                <th>Status</th>
                                <th>Trip Price</th>
                                <th>Air Cost</th>
                                <th>Land Cost</th>
                                <th>Agent Fees</th>
                                <th>Extra Costs</th>
                                <th>Group Exp.</th>
                                <th>Total Costs</th>
                                <th>Amount Received</th>
                                <th>Credit Notes</th>
                                <th>Balance</th>
                                <th>Profit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php //print_r($GroupPax);
                            
                            $GroupPax = $FinancialReport->GetGroupData($GroupzDataID);

                            $Product_Land_Price = $GroupPax[0]['Land_Price'];
                            $Product_Land_Cost = $GroupPax[0]['Land_Cost'];
                            $Product_Air_Price = $GroupPax[0]['Air_Price'];
                            $Product_Air_Cost = $GroupPax[0]['Air_Cost'];
                            $Airline_Credit = $GroupPax[0]['Ticket_Credit'];
                            $General_Expenses = $GroupPax[0]['TotalExpenses'];

                            $GroupPaxList = array();
                            if($General_Expenses > 0) {
                                $General_Expenses_Each = $General_Expenses / count($GroupPax);
                            } else {
                                $General_Expenses_Each = 0.00;
                                $General_Expenses = 0.00;
                            }
                            $Total_PackagePrice = 0;
                            $Total_AirCost = 0;
                            $Total_LandCost = 0;
                            $Total_AgentFees = 0;
                            $Total_ExtraCost = 0;
                            $Total_FullCost = 0;
                            $Total_PaidAmount = 0;
                            $Total_CreditAmount = 0;
                            $Total_Balance = 0;
                            $Total_Profit = 0;

                            $CustomerNum = 1;
    
                            foreach($GroupPax AS $EachPax) {
                                
                                //Make the calculations of the Extra Expenses
                                $CostCredit = $EachPax['Airline_Credit'];
                                $CostSingleRoom = $EachPax['Single_Room_Cost'];
                                $CostAir = $EachPax['Air_Upgrade_Cost'];
                                $CostLand = $EachPax['Land_Upgrade_Cost'];
                                $CostInsurance = $EachPax['Insurance_Cost'];
                                $CostTransfer = $EachPax['Transfer_Cost'];
                                $CostExtension = $EachPax['Extension_Cost'];
                                if($EachPax['Complimentary'] == 1) { $Agent_Fees = 0; } else { $Agent_Fees = $EachPax['Agent_Fees']; }
                                
                                $Cost_Total_Extra = $CostCredit+$CostAir+$CostLand+$CostInsurance+$CostTransfer+$CostExtension+$CostSingleRoom;
                                $Cost_Total = $Product_Air_Cost+$Product_Land_Cost+$Cost_Total_Extra;
                                
                                //Make the calculations of the Extra Prices
                                $PriceCredit = $EachPax['Airline_Credit'];
                                $PriceSingleRoom = $EachPax['Single_Room_Price'];
                                $PriceAir = $EachPax['Air_Upgrade_Price'];
                                $PriceLand = $EachPax['Land_Upgrade_Price'];
                                $PriceInsurance = $EachPax['Insurance_Price'];
                                $PriceTransfer = $EachPax['Transfer_Price'];
                                $PriceExtension = $EachPax['Extension_Price'];
                                $Price_Total_Extra = $PriceCredit+$PriceAir+$PriceLand+$PriceInsurance+$PriceTransfer+$PriceExtension+$PriceSingleRoom;
                                $Price_Total = $Product_Land_Price+$Product_Air_Price+$Price_Total_Extra;
                                
                                //Calculations
                                $Balance = $Price_Total - ($EachPax['Paid_Amount']-$EachPax['Refund_Amount']) - $EachPax['Credit_Amount'];
                                $Profit = $Price_Total - $Cost_Total;
                                
                                //The new way, two arrays concept!!!!
                                if($EachPax['status'] == 1 AND is_numeric($EachPax['Joint_Invoice']) AND $EachPax['Joint_Invoice'] > 0) {
                                    $GroupPaxList[$EachPax['Joint_Invoice']]['Package_Price'] += $Product_Land_Price+$Product_Air_Price+$Price_Total_Extra;
                                    $GroupPaxList[$EachPax['Joint_Invoice']]['Product_Air_Cost'] += $Product_Air_Cost;
                                    $GroupPaxList[$EachPax['Joint_Invoice']]['Product_Land_Cost'] += $Product_Land_Cost;
                                    $GroupPaxList[$EachPax['Joint_Invoice']]['Agent_Fees'] += $Agent_Fees;
                                    $GroupPaxList[$EachPax['Joint_Invoice']]['Extra_Cost'] += $Cost_Total_Extra;
                                    $GroupPaxList[$EachPax['Joint_Invoice']]['Extra_Price'] += $Price_Total_Extra;
                                    $GroupPaxList[$EachPax['Joint_Invoice']]['Paid_Amount'] += $EachPax['Paid_Amount'] - $EachPax['Refund_Amount'];
                                    $GroupPaxList[$EachPax['Joint_Invoice']]['Credit_Amount'] += $EachPax['Credit_Amount'];
                                    
                                    $GroupPaxList[$EachPax['id']]['Customer_ID'] = $EachPax['id'];
                                    $GroupPaxList[$EachPax['id']]['Customer_Name'] = $EachPax['Customer_Name'];
                                    $GroupPaxList[$EachPax['id']]['Package_Price'] += 0;
                                    $GroupPaxList[$EachPax['id']]['Product_Air_Cost'] += 0;
                                    $GroupPaxList[$EachPax['id']]['Product_Land_Cost'] += 0;
                                    $GroupPaxList[$EachPax['id']]['Agent_Fees'] += 0;
                                    $GroupPaxList[$EachPax['id']]['Extra_Cost'] += 0;
                                    $GroupPaxList[$EachPax['id']]['Extra_Price'] += 0;
                                    $GroupPaxList[$EachPax['id']]['Paid_Amount'] += 0;
                                    $GroupPaxList[$EachPax['id']]['Credit_Amount'] += 0;
                                    $GroupPaxList[$EachPax['id']]['Joint_Invoice'] = 'Joint';
                                    
                                } elseif($EachPax['status'] == 1 AND $EachPax['Joint_Invoice'] == 'Yes') {
                                    $GroupPaxList[$EachPax['id']]['Customer_ID'] = $EachPax['id'];
                                    $GroupPaxList[$EachPax['id']]['Customer_Name'] = $EachPax['Customer_Name'];
                                    $GroupPaxList[$EachPax['id']]['Package_Price'] += $Product_Land_Price+$Product_Air_Price+$Price_Total_Extra;
                                    $GroupPaxList[$EachPax['id']]['Product_Air_Cost'] += $Product_Air_Cost;
                                    $GroupPaxList[$EachPax['id']]['Product_Land_Cost'] += $Product_Land_Cost;
                                    $GroupPaxList[$EachPax['id']]['Agent_Fees'] += $Agent_Fees;
                                    $GroupPaxList[$EachPax['id']]['Extra_Cost'] += $Cost_Total_Extra;
                                    $GroupPaxList[$EachPax['id']]['Extra_Price'] += $Price_Total_Extra;
                                    $GroupPaxList[$EachPax['id']]['Paid_Amount'] += $EachPax['Paid_Amount'] - $EachPax['Refund_Amount'];
                                    $GroupPaxList[$EachPax['id']]['Credit_Amount'] += $EachPax['Credit_Amount'];
                                    $GroupPaxList[$EachPax['id']]['Joint_Invoice'] = 'Primary';
                                    
                                } elseif($EachPax['status'] == 1) {
                                    $GroupPaxList[$EachPax['id']]['Customer_ID'] = $EachPax['id'];
                                    $GroupPaxList[$EachPax['id']]['Customer_Name'] = $EachPax['Customer_Name'];
                                    $GroupPaxList[$EachPax['id']]['Package_Price'] += $Product_Land_Price+$Product_Air_Price+$Price_Total_Extra;
                                    $GroupPaxList[$EachPax['id']]['Product_Air_Cost'] += $Product_Air_Cost;
                                    $GroupPaxList[$EachPax['id']]['Product_Land_Cost'] += $Product_Land_Cost;
                                    $GroupPaxList[$EachPax['id']]['Agent_Fees'] += $Agent_Fees;
                                    $GroupPaxList[$EachPax['id']]['Extra_Cost'] += $Cost_Total_Extra;
                                    $GroupPaxList[$EachPax['id']]['Extra_Price'] += $Price_Total_Extra;
                                    $GroupPaxList[$EachPax['id']]['Paid_Amount'] += $EachPax['Paid_Amount'] - $EachPax['Refund_Amount'];
                                    $GroupPaxList[$EachPax['id']]['Credit_Amount'] += $EachPax['Credit_Amount'];
                                    $GroupPaxList[$EachPax['id']]['Joint_Invoice'] = 'Alone';
                                } else {
                                    $GroupPaxList[$EachPax['id']]['Customer_ID'] = $EachPax['id'];
                                    $GroupPaxList[$EachPax['id']]['Customer_Name'] = $EachPax['Customer_Name'];
                                    $GroupPaxList[$EachPax['id']]['Package_Price'] = $EachPax['Cancellation_Charge'];
                                    $GroupPaxList[$EachPax['id']]['Product_Air_Cost'] = 0;
                                    $GroupPaxList[$EachPax['id']]['Product_Land_Cost'] = 0;
                                    $GroupPaxList[$EachPax['id']]['Agent_Fees'] = 0;
                                    $GroupPaxList[$EachPax['id']]['Extra_Cost'] = 0;
                                    $GroupPaxList[$EachPax['id']]['Extra_Price'] = 0;
                                    $GroupPaxList[$EachPax['id']]['Paid_Amount'] += $EachPax['Paid_Amount'] - $EachPax['Refund_Amount'];
                                    //$GroupPaxList[$EachPax['id']]['Credit_Amount'] += $EachPax['Credit_Amount'];
                                    $GroupPaxList[$EachPax['id']]['Credit_Amount'] = 0;
                                    $GroupPaxList[$EachPax['id']]['Joint_Invoice'] = 'Cancelled';
                                }
                            }
                            foreach ($GroupPaxList AS $EachGroupPax) {
                            
                                $Status = $EachGroupPax['Joint_Invoice'];
                                
                            if($Status == 'Primary' OR $Status == 'Joint') {
                                $BoldStyle= "style='color:blue;font-weight:600;'";
                            } else {
                                $BoldStyle = "style='color:blue;'";
                            }
                            if($Status == 'Cancelled') {
                                $TD_Style = "style='color:red;'";
                                $BoldStyle= "style='color:red;font-weight:600;'";
                            } else {
                                $TD_Style = "";
                            }
                                
                            ?>
                            <tr>
                                <td class="p-1">
                                    <?php echo $CustomerNum; ?>
                                </td>
                                <td class="p-1" <?php echo $TD_Style; ?>>
                                    <a href="contacts-edit.php?id=<?php echo $EachGroupPax['Customer_ID']; ?>&action=edit&usertype=customer" <?php echo $BoldStyle; ?>><?php echo $EachGroupPax['Customer_Name']; ?></a>
                                </td>
                                <td class="p-1" <?php echo $TD_Style; ?>>
                                    <?php echo $Status; ?>
                                </td>
                                <td class="p-1">
                                    <?php echo GUtils::formatMoney($EachGroupPax['Package_Price']); ?>
                                </td>
                                <td class="p-1">
                                    <?php echo ($Status=='Primary' OR $Status=='Alone') ? GUtils::formatMoney($EachGroupPax['Product_Air_Cost']) : "-"; ?>
                                </td>
                                <td class="p-1">
                                    <?php echo ($Status=='Primary' OR $Status=='Alone') ? GUtils::formatMoney($EachGroupPax['Product_Land_Cost']) : "-"; ?>
                                </td>
                                <td class="p-1">
                                    <?php echo ($Status=='Primary' OR $Status=='Alone') ? GUtils::formatMoney($EachGroupPax['Agent_Fees']) : "-"; ?>
                                </td>
                                <td class="p-1">
                                    <?php echo ($Status=='Primary' OR $Status=='Alone') ? GUtils::formatMoney($EachGroupPax['Extra_Cost']) : "-"; ?>
                                </td>
                                <td class="p-1">
                                    <?php echo GUtils::formatMoney($General_Expenses_Each); ?>
                                </td>
                                <td class="p-1" data-toggle="tooltip" data-html="true" title="" data-original-title="
                                                        Airline Product: <?php echo GUtils::formatMoney($EachGroupPax['Product_Air_Cost']); ?><br />
                                                        Land Product: <?php echo GUtils::formatMoney($EachGroupPax['Product_Land_Cost']); ?><br />
                                                        Upgrades: <?php echo GUtils::formatMoney($EachGroupPax['Extra_Cost']); ?><br />
                                                        Agents Fee: <?php echo GUtils::formatMoney($EachGroupPax['Agent_Fees']); ?><br />
                                                         ">
                                    <?php $TotalCosts_Each = $EachGroupPax['Product_Air_Cost'] + $EachGroupPax['Product_Land_Cost'] + $EachGroupPax['Extra_Cost'] + $EachGroupPax['Agent_Fees'];
                                    echo ($Status=='Primary' OR $Status=='Alone') ? GUtils::formatMoney($TotalCosts_Each) : "-"; ?>
                                </td>
                                <td class="p-1" data-toggle="tooltip" data-html="true" title="" data-original-title="Paid Amount - CC Charges"><?php echo GUtils::formatMoney($EachGroupPax['Paid_Amount']); ?></td>
                                <td class="p-1"><?php echo GUtils::formatMoney($EachGroupPax['Credit_Amount']); ?></td>
                                <td class="p-1"><?php echo GUtils::formatMoney(($EachGroupPax['Package_Price'] - ($EachGroupPax['Paid_Amount']-$EachGroupPax['Refund_Amount']) - $EachGroupPax['Credit_Amount']),'$',true); ?></td>
                                <?php if($Status=='Cancelled' OR $Status=='Joint') {
                                $Profit_Each_Customer = $EachGroupPax['Package_Price']-($EachGroupPax['Extra_Cost']+$EachGroupPax['Credit_Amount']+$General_Expenses_Each);
                                ?>
                                <td class="p-1"><?php echo GUtils::formatMoney($Profit_Each_Customer,'$',true); ?></td>
                                <?php } else {
                                $Profit_Each_Customer = $EachGroupPax['Package_Price']-($EachGroupPax['Product_Air_Cost']+$EachGroupPax['Product_Land_Cost']+$EachGroupPax['Extra_Cost']+$EachGroupPax['Credit_Amount']+$General_Expenses_Each);
                                ?>
                                <td class="p-1">
                                    <?php echo GUtils::formatMoney($Profit_Each_Customer,'$',true); ?></td>
                                <?php } ?>
                            </tr>
                            <?php
                                $Total_PackagePrice += $EachGroupPax['Package_Price'];
                                $Total_AirCost += $Product_Air_Cost;
                                $Total_LandCost += $Product_Land_Cost;
                                $Total_AgentFees += $EachGroupPax['Agent_Fees'];
                                $Total_ExtraCost += $EachGroupPax['Extra_Cost'];
                                $Total_FullCost += $TotalCosts_Each;
                                $Total_PaidAmount += $EachGroupPax['Paid_Amount'];
                                $Total_CreditAmount += $EachGroupPax['Credit_Amount'];
                                $Total_Balance += $EachGroupPax['Package_Price'] - ($EachGroupPax['Paid_Amount']-$EachGroupPax['Refund_Amount']) - $EachGroupPax['Credit_Amount']; 
                                $Total_Profit += $Profit_Each_Customer;
                                
                                $CustomerNum++;
                            } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th>Totals:</th>
                                <th><?php echo GUtils::formatMoney($Total_PackagePrice,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_AirCost,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_LandCost,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_AgentFees,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_ExtraCost,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($General_Expenses,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_FullCost,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_PaidAmount,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_CreditAmount,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_Balance,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_Profit,'$',true); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <?php } elseif($ShowTheResults === true AND $ResultType == "Monthly") { ?>
            <div class='card-block pt-0'>
                <?php echo "<h3 class='pb-3'>Groups monthly Financial Report: ".date('F', mktime(0, 0, 0, $Filter_Month, 1)).", ".$Filter_Year."</h3>"; ?>
                <div class="dt-responsive table-responsive " style="position: relative;">
                    <table id="TableWithNoButtons" class="table table-striped table-bordered nowrap" style="width:100%;" data-page-length="500">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Group Name</th>
                                <th>Pax</th>
                                <th>Start Date</th>
                                <th>End Date</th>
                                <th>Trip Price</th>
                                <th>Group Exp.</th>
                                <th>Total Costs</th>
                                <th>Amount Received</th>
                                <th>Credit Notes</th>
                                <th>Balance</th>
                                <th>Profit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
    
                            $GroupsList = $FinancialReport->GetGroupsPerMonth($Filter_Month,$Filter_Year);
                            $GroupNum = 1;
    
                            $Total_PackagePrice = 0;
                            $General_Expenses = 0;
                            $Total_FullCost = 0;
                            $Total_PaidAmount = 0;
                            $Total_CreditAmount = 0;
                            $Total_Balance = 0;
                            $Total_Profit = 0;
    
                            foreach($GroupsList AS $EachGroup) { ?>
                            <tr>
                                <td class="p-1 text-center">
                                    <?php echo $GroupNum; ?>
                                </td>
                                <td class="p-1" data-toggle="tooltip" data-html="true" title="" data-original-title="
                                                        Church Name: <?php echo $EachGroup['Tour_Church_Name']; ?>">
                                    <a href="groups-edit.php?id=<?php echo $EachGroup['Tour_ID']; ?>&action=edit&tab=participants" style="color:blue;font-weight:600;"><?php echo $EachGroup['Tour_Reference']; ?></a>
                                </td>
                                <td class="p-1 text-center"><?php echo $EachGroup['Tour_Pax_Count']; ?></td>
                                <td class="p-1"><?php echo GUtils::clientDate($EachGroup['Tour_Start_Date']); ?></td>
                                <td class="p-1"><?php echo GUtils::clientDate($EachGroup['Tour_End_Date']); ?></td>
                                <td class="p-1"><?php echo GUtils::formatMoney($EachGroup['Tour_PackagePrice'],'$',true); ?></td>
                                <td class="p-1"><?php echo GUtils::formatMoney($EachGroup['Tour_GeneralExpense'],'$',true); ?></td>
                                <td class="p-1"><?php echo GUtils::formatMoney($EachGroup['Tour_FullCost'],'$',true); ?></td>
                                <td class="p-1"><?php echo GUtils::formatMoney(($EachGroup['Tour_PaidAmount'] - $EachGroup['Tour_RefundAmount']),'$',true); ?></td>
                                <td class="p-1"><?php echo GUtils::formatMoney($EachGroup['Tour_CreditAmount'],'$',true); ?></td>
                                <td class="p-1"><?php echo GUtils::formatMoney($EachGroup['Tour_Balance'],'$',true); ?></td>
                                <td class="p-1"><?php echo GUtils::formatMoney($EachGroup['Tour_Profit'],'$',true); ?></td>
                            </tr>
                            <?php
                                                                
                                $Total_PackagePrice += $EachGroup['Tour_PackagePrice'];
                                $General_Expenses += $EachGroup['Tour_GeneralExpense'];
                                $Total_FullCost += $EachGroup['Tour_FullCost'];
                                $Total_PaidAmount += $EachGroup['Tour_PaidAmount'] - $EachGroup['Tour_RefundAmount'];
                                $Total_CreditAmount += $EachGroup['Tour_CreditAmount'];
                                $Total_Balance += $EachGroup['Tour_Balance'];
                                $Total_Profit += $EachGroup['Tour_Profit'];
                                    
                                $GroupNum++;
                            } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th class="text-right">Totals:</th>
                                <th><?php echo GUtils::formatMoney($Total_PackagePrice,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($General_Expenses,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_FullCost,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_PaidAmount,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_CreditAmount,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_Balance,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_Profit,'$',true); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <?php } elseif($ShowTheResults === true AND $ResultType == "Yearly") { ?>
            <div class='card-block pt-0'>
                <?php echo "<h3 class='pb-3'>Yearly Groups Financial Report: ".$Filter_Year."</h3>"; ?>
                <div class="dt-responsive table-responsive " style="position: relative;">
                    <table id="TableWithNoButtons" class="table table-striped table-bordered nowrap" style="width:100%;" data-page-length="500">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Month</th>
                                <th>Groups #</th>
                                <th>Total Sales</th>
                                <th>Group Exp.</th>
                                <th>Total Costs</th>
                                <th>Amount Received</th>
                                <th>Credit Notes</th>
                                <th>Balance</th>
                                <th>Profit</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
    
                            $GroupsList = $FinancialReport->GetGroupsPerYear($Filter_Year);
                            $GroupNum = 1;
                            $Total_Groups = 0;
                            $Total_PackagePrice = 0;
                            $General_Expenses = 0;
                            $Total_FullCost = 0;
                            $Total_PaidAmount = 0;
                            $Total_CreditAmount = 0;
                            $Total_Balance = 0;
                            $Total_Profit = 0;
    
                            foreach($GroupsList AS $EachGroup) { ?>
                            <tr>
                                <td class="p-1 text-center">
                                    <?php echo $GroupNum; ?>
                                </td>
                                <td class="p-1"><?php echo $EachGroup['Month']; ?></td>
                                <td class="p-1 text-center"><?php echo $EachGroup['Total_Groups']; ?></td>
                                <td class="p-1"><?php echo GUtils::formatMoney($EachGroup['Total_PackagePrice'],'$',true); ?></td>
                                <td class="p-1"><?php echo GUtils::formatMoney($EachGroup['General_Expenses'],'$',true); ?></td>
                                <td class="p-1"><?php echo GUtils::formatMoney($EachGroup['Total_FullCost'],'$',true); ?></td>
                                <td class="p-1"><?php echo GUtils::formatMoney(($EachGroup['Total_PaidAmount']-$EachGroup['Total_RefundAmount']),'$',true); ?></td>
                                <td class="p-1"><?php echo GUtils::formatMoney($EachGroup['Total_CreditAmount'],'$',true); ?></td>
                                <td class="p-1"><?php echo GUtils::formatMoney($EachGroup['Total_Balance'],'$',true); ?></td>
                                <td class="p-1"><?php echo GUtils::formatMoney($EachGroup['Total_Profit'],'$',true); ?></td>
                            </tr>
                            <?php
                                                                
                                $Total_Groups += $EachGroup['Total_Groups'];
                                $Total_PackagePrice += $EachGroup['Total_PackagePrice'];
                                $General_Expenses += $EachGroup['General_Expenses'];
                                $Total_FullCost += $EachGroup['Total_FullCost'];
                                $Total_PaidAmount += $EachGroup['Total_PaidAmount'] - $EachGroup['Total_RefundAmount'];
                                $Total_CreditAmount += $EachGroup['Total_CreditAmount'];
                                $Total_Balance += $EachGroup['Total_Balance'];
                                $Total_Profit += $EachGroup['Total_Profit'];
                                    
                                $GroupNum++;
                            } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th class="text-right">Totals:</th>
                                <th class="text-center"><?php echo $Total_Groups; ?></th>
                                <th><?php echo GUtils::formatMoney($Total_PackagePrice,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($General_Expenses,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_FullCost,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_PaidAmount,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_CreditAmount,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_Balance,'$',true); ?></th>
                                <th><?php echo GUtils::formatMoney($Total_Profit,'$',true); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<script>
//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#TableWithNoButtons').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
});
    
    $(document).ready(function() {
        $('#TableWithNoButtons').DataTable({
            //"ordering": false, //To disable the ordering of the tables
            "bLengthChange": false, //To hide the Show X entries dropdown
            "bInfo": false,
            "bPaginate": false,
            dom: 'Bfrtip',
            sDom: 'Blrtip', //To Hide the search box
            buttons: ['csv','excel'], //To hide the export buttons
            <?php if($ShowTheResults === true AND $ResultType == "Group") { ?>
            "columnDefs": [
                {
                    "targets": [ 4,5,6,7 ],
                    "visible": false,
                    "searchable": false
                }
            ]
            <?php } ?>
        });
    });

//To create select2 function for the groups dropdown
$(document).ready(function(){
   $("#SpecificGroup").select2({
      ajax: {
        url: "inc/select2-groups.php",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
           return {
              searchTerm: params.term,
              FinancialReport: 'active'
           };
        },
        processResults: function (response) {
           return {
              results: response
           };
        },
        cache: true
      }
   });
});

$('#ClearFilterOptions').click(function(){
	document.getElementById("Monthly").value = "0000-00-00";
	document.getElementById("Yearly").value = "0000-00-00";
	$('#type').val('all');
	//$("#SpecificGroup").empty();
	$("#SpecificGroup").select2("val", "0");
});
</script>
<?php include 'footer.php'; ?>
