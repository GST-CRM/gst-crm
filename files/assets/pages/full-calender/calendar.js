	"use strict";
	$(document).ready(function() {
	    $('#external-events .fc-event').each(function() {

	        // store data so the calendar knows to render an event upon drop
	        $(this).data('event', {
	            title: $.trim($(this).text()), // use the element's text as the event title
	            stick: true // maintain when user navigates (see docs on the renderEvent method)
	        });

	        // make the event draggable using jQuery UI
	        $(this).draggable({
	            zIndex: 999,
	            revert: true, // will cause the event to go back to its
	            revertDuration: 0 //  original position after the drag
	        });

	    });


		var calendar = $('#calendar').fullCalendar({
	        editable: true,
	        displayEventTime: false,
	        selectable: true,
	        selectHelper: true,
	        //droppable:true,
	        //events: ['inc/calendar-functions.php?getEvent=yes'],
	        eventSources: ['inc/calendar-functions.php?getEvent=yes'],

	        dayClick: function(date, jsEvent, view) {

				$("#event_date").val(date.format());
				$("#calendarForm").trigger("reset");
				$("#enable_period").removeAttr("checked");
				$("#eventModal").modal("show");
		        $("#period").attr("disabled",true);
			},
	        eventDrop: function (event, delta) {
                var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD");
                //var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                $.ajax({
                    url	: 'inc/calendar-functions.php',
                    data: {'addEvent':'yes','Event_Description':event.title,'Event_Date':start,
                    		'Event_ID':event.id,'Event_Type':event.type,
                    		'Event_Notes':event.notes,'Recurring':event.enable_period,
                    		'Period':event.period},
                    type: "POST",
                    success: function (response) {
                        //displayMessage("Updated Successfully");
                    }
                });
	        }, 
	        eventClick: function (event) {
	        	var start 		= new Date(event.start);
				var dd 			= String(start.getDate()).padStart(2, '0'); 
				var mm 			= String(start.getMonth() + 1).padStart(2, '0'); 
				var yyyy 		= start.getFullYear(); 
	        	var event_date 	= yyyy+"-"+mm+"-"+dd;

				$("#event_id").val(event.id);
				$("#event_date").val(event_date);
				$("#event_type").val(event.type);
				$("#event_description").val(event.title);
				$("#event_notes").val(event.notes);
				$("#eventModal").modal("show");
				$("#deleteEventBtn").show();

				if (event.enable_period == 1)
		        {
		        	$("#enable_period").attr("checked",true)
		            $("#period").removeAttr("disabled");
		            $("#period").val(event.period);
		        }
		        else
		        {
		            $("#period").attr("disabled",true);
		        }
	        },
	        /*drop: function() {
			    $(this).remove();
			  },*/
			/*eventReceive: function(event) {
                var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD");
                //var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                console.log(event);
                $.ajax({
                    url	: 'inc/calendar-functions.php',
                    data: {'addEvent':'yes','Event_Description':event.title,'Event_Date':start,
                    		'Event_ID':event.id,'Event_Type':event.type,
                    		'Event_Notes':event.notes,'Recurring':event.enable_period,
                    		'Period':event.period},
                    type: "POST",
                    success: function (response) {
                        //displayMessage("Updated Successfully");
                    }
                });
			}*/

	    });

	});
