<?php
//============================================================+
// File name   : example_006.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 006 for TCPDF class
//               WriteHTML and RTL support
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: WriteHTML and RTL support
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Element Media');
$pdf->SetTitle('GST Customer Invoice');
$pdf->SetSubject('GST Customer Invoice');
$pdf->SetKeywords('GST, CRM, Element, Media, Element.ps');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);
// set margins
$pdf->SetMargins(15, 15, 15);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('Helvetica', '', 11);

// add a page
$pdf->AddPage('P','LETTER');

// writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
// writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

// create some HTML content
$html = '
<table cellpadding="7" cellspacing="0" border="0">
	<tbody>
		<tr>
			<td width="65%">
				<img src="images/logo-invoice.png" width="300">
			</td>
			<td width="35%">
				<b>Invoice #:</b> 20180001<br>
				<b>Created:</b> Dec 03, 2018<br>
				<b>Due:</b> Dec 12, 2018								
			</td>
		</tr>
		<tr>
			<td colspan="2" height="25">
			</td>
		</tr>
		<tr>
			<td>
				<strong style="font-weight:bold;">Father Benjamin Kneib</strong><br>
				1111 Trenton Street,<br>
				Chillicothe, MO, 64601
			</td>
			<td>
				<strong style="font-weight:bold;">Good Shepherd Travels</strong><br>
				9021 Washington Ln<br>Lantana, Texas 76226<br>
				info@goodshepherdtravel.com
			</td>
		</tr>
		<tr>
			<td colspan="2" height="25">
			</td>
		</tr>
		<tr bgcolor="#eee">
			<td>
				<b>Item</b>
			</td>    
			<td align="right">
				<b>Price</b>
			</td>
		</tr>
		<tr>
			<td>Package Fee</td>
			<td align="right">3700</td>
		</tr>
		<tr bgcolor="#f9f9f9">
			<td>Air Upgrade</td>
			<td align="right">1050</td>
		</tr>
		<tr>
			<td>Land Upgrade</td>
			<td align="right">800</td>
		</tr>
		<tr bgcolor="#f9f9f9">
			<td>Single Supplement</td>
			<td align="right">0</td>
		</tr>
		<tr bgcolor="#f2f2f2">
			<td colspan="2" align="right">
				<b>Total: $5550</b>
			</td>
		</tr>
		<tr>
			<td colspan="2" height="25"></td>
		</tr>
		<tr bgcolor="#eee">
			<td><b>Payment Method</b></td>
			<td align="right"><b>Amount</b></td>
		</tr>
		<tr>
			<td>Cash <small>(Oct 03, 2018)</small></td>
			<td align="right">$100</td>
		</tr>
		<tr bgcolor="#f9f9f9">
			<td>Cash <small>(Oct 11, 2018)</small></td>
			<td align="right">$570</td>
		</tr>
		<tr>
			<td>Bank Transfer <small>(Nov 25, 2018)</small></td>
			<td align="right">$750</td>
		</tr>
		<tr bgcolor="#f9f9f9">
			<td>Cash <small>(Nov 26, 2018)</small></td>
			<td align="right">$340</td>
		</tr>
		<tr>
			<td>Check <small>(Dec 10, 2018)</small></td>
			<td align="right">$775</td>
		</tr>
		<tr bgcolor="#f2f2f2">
			<td colspan="2" align="right"><b>Total: $2835</b></td>
		</tr>
		<tr bgcolor="#333">
			<td colspan="2" align="center"><b><span color="white">Total Amount Needed: $2715</span></b></td>
		</tr>
		<tr>
			<td colspan="2" height="25">
			</td>
		</tr>
		<tr>
			<td colspan="2"><strong style="font-weight:bold;">Comments:</strong> testing to add comments</td>
		</tr>
	</tbody>
</table>
<p align="center">If you have any questions about this invoice, please contact<br />
Good Shepherd Travels at info@goodshepherdtravel.com<br />
<b>Thank you for participating with our pilgrimages!</b></p>
';

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');


// reset pointer to the last page
$pdf->lastPage();

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('Customer-Invoice.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
