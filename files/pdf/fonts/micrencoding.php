<?php
$type='TrueTypeUnicode';
$name='MICREncoding';
$desc=array('Ascent'=>469,'Descent'=>0,'CapHeight'=>469,'Flags'=>32,'FontBBox'=>'[0 0 596 469]','ItalicAngle'=>0,'StemV'=>70,'MissingWidth'=>600);
$up=0;
$ut=0;
$dw=600;
$cw=array(
32=>500,48=>500,49=>500,50=>500,51=>500,52=>500,53=>500,54=>500,55=>500,56=>500,
57=>500,65=>500,66=>500,67=>500,68=>500,97=>500,98=>500,99=>500,100=>500,224=>626);
$enc='';
$diff='';
$file='micrencoding.z';
$ctg='micrencoding.ctg.z';
$originalsize=30076;
// --- EOF ---