<?php include "session.php";
$PageTitle = "Edit the General Expense";
include "header.php";

//To make several url options in the same page
$allowed = array('update', 'edit', 'delete');
if ( ! isset($_GET['action'])) {header("location: expenses.php");die('Please go back to the main page.');}
$action = mysqli_real_escape_string($db, $_GET['action']);
if ( ! in_array($action, $allowed)) {header("location: expenses.php");die('Please go back to the main page.');}


//To Delete a payment
if ($action == "delete") {
	$ExpensePaymentID = mysqli_real_escape_string($db, $_GET['PaymentID']);
	$ExpPaymentDelete = "DELETE FROM `expenses_payments` WHERE Exp_Payment_ID=$ExpensePaymentID";
	GDb::execute($ExpPaymentDelete);
	if(GDb::execute($ExpPaymentDelete)) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected expense payment has been deleted successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting record: " . $db->error . "</strong></div>";
	}
}

$Expense_Num = mysqli_real_escape_string($db, $_GET["id"]);
$TotalPaidSoFar = 0;

//To collect the data of the above customer id, and show it in the fields below
$sql = "SELECT exp.*,gro.tourname,payee.Payee_Name, (SELECT SUM(Exp_Payment_Amount) FROM expenses_payments WHERE Expense_Num=exp.Expense_Num) AS TotalPaid FROM expenses exp LEFT JOIN groups gro ON exp.Group_ID=gro.tourid JOIN expenses_payee payee ON payee.Payee_ID=exp.Payee_ID WHERE exp.Expense_Num=$Expense_Num";
$result = $db->query($sql);
$data = $result->fetch_assoc();

include 'inc/expenses-functions.php';

?>
  <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />-->
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-block">
				<form name="contact5" action="" method="POST" class="row" id="contact5" enctype="multipart/form-data">
                    <div class="form-default form-static-label col-sm-4">
                        <label class="float-label gst-label">Payee Reference</label>
                        <input type="text" name="EditExpense" value="<?php echo $Expense_Num; ?>" hidden>
                        <a href="">
							<input class="form-control" type="text" name="GroupReference" value="<?php echo $data['Payee_Name']; ?>" style="cursor:pointer;" readonly>
						</a>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-2">
                        <label class="float-label gst-label">Expense Date</label>
                        <input type="date" name="ExpenseDate" id='id_invoice_date' value="<?php echo $data['Expense_Date']; ?>" class="form-control date-picker">
                    </div>
                    <div class="form-default form-static-label col-sm-4 text-center">
					<?php if($data['Status']==0) { echo "<h1><i class='text-danger fas fa-exclamation-triangle'></i></h1>";} ?>
                    </div>
                    <div class="form-default form-static-label col-sm-2 text-center">
                        <h5>Amount</h5>
                        <h3 class="text-danger" id="ExpensesTotalText"><?php echo GUtils::formatMoney($data['Expense_Amount']-$data['TotalPaid']); ?></h3>
                    </div>
                    <div class="form-default form-static-label col-sm-4">
                        <label class="float-label gst-label">Group Reference</label>
						<?php if($data['Group_ID'] == 0) { ?>
							<input class="form-control" type="text" name="GroupReference" value="This expense isn't assigned to a group" readonly>
						<?php } else { ?>
                        <a href="groups-edit.php?id=<?php echo $data['Group_ID']; ?>&action=edit&tab=expenses">
							<input class="form-control" type="text" name="GroupReference" value="<?php echo $data['tourname']; ?>" style="cursor:pointer;" readonly>
						</a>
						<?php } ?>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-2">
                        <label class="float-label gst-label" >Due Date</label>
                        <input type="date" name="due_date" id="id_due_date" value="<?php echo $data['Due_Date']; ?>" class="form-control">
                    </div>
                    <div class="form-default form-static-label col-sm-4 text-center">
						<?php if($data['Status']==0) { echo "<h5 class='text-danger'>This record is</h5><h3 class='text-danger'>Voided</h3>";} ?>
                    </div>
                    <div class="form-default form-static-label col-sm-2 text-center">
						<?php if($data['Status']==0) { ?>
						<a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick = 'deleteConfirm( "expenses.php?action=unvoid&exp_id=<?php echo $data["Expense_Num"]; ?>", "Unvoid")' role="button">Unvoid</a>
						<?php } else { ?>
						<a class="btn btn-primary btn-sm" href="javascript:void(0);" onclick = 'deleteConfirm( "expenses.php?action=void&exp_id=<?php echo $data["Expense_Num"]; ?>", "Void")' role="button">Void</a>
						<a class="btn btn-danger btn-sm" href="expenses-payment-action.php?action=add&id=<?php echo $data["Expense_Num"]; ?>" role="button">Add Payment</a>
						<?php } ?>
                    </div>
                    <div class="col-sm-12"><br /></div>
                    <div class="col-sm-12">
						<table class="table table-hover table-striped table-bordered nowrap" id="item_table">
							<thead>
								<tr>
									<th width="30" class="text-center">#</th>
									<th width="20%">Expense Category</th>
									<th>Expense Description</th>
									<th width="15%">Expense Amount</th>
									<th width="50"><button type="button" name="add" class="btn btn-success btn-sm add"><i class="fas fa-plus mr-0"></i></button></th>
								</tr>
							</thead>
							<tbody>
							<?php
							$sql = "SELECT * from expenses_line WHERE Expense_Num=$Expense_Num";
							$result = $db->query($sql);
							$ExpenseLine = 1;
							if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
								<tr>
									<td class="text-center">
										<?php echo $ExpenseLine; ?>
										<input type="hidden" name="exp_id[]" class="form-control exp_id" value="<?php echo $ExpenseLine; ?>" />
									</td>
									<td>
										<select name="exp_category[]" class="form-control exp_category" onchange="ChartOfAccountSelect(this)">
											<?php
												$ExpensesSelect = "";
												$GLaccountResult = mysqli_query($db,"SELECT GL_Group_Num,GL_Group_Name FROM gl_account_groups WHERE GL_Group_Status=1 AND GL_Account_Classes_Class_ID=6");
												while($GLaccountData = mysqli_fetch_array($GLaccountResult))
												{
													$ExpensesSelect .='<option value="'.$GLaccountData["GL_Group_Num"].'">'.$GLaccountData["GL_Group_Name"].'</option>';
													//if($GLaccountData["GL_Group_Num"] == $row['Expense_Line_Category']) {$CoA_Group_Selected="selected";} else {$CoA_Group_Selected="";}
													//echo '<option value="'.$GLaccountData["GL_Group_Num"].'" '.$CoA_Group_Selected.'>'.$GLaccountData["GL_Group_Name"].'</option>';
													if($GLaccountData["GL_Group_Num"] == $row['Expense_Line_Category']) {
														echo '<option value="'.$GLaccountData["GL_Group_Num"].'" '.$CoA_Group_Selected.'>'.$GLaccountData["GL_Group_Name"].'</option>';
													}
												}
											?>
										</select>
									</td>
									<td>
										<select name="exp_desc[]" class="form-control exp_desc">
											<?php
												$ChartAccountResult = mysqli_query($db,"SELECT Account_ID,GL_Account_Name FROM gl_chart_of_accounts WHERE GL_Account_Status=1 AND Account_ID=".$row['Expense_Line_CoA']."");
												while($ChartAccountData = mysqli_fetch_array($ChartAccountResult))
												{
													echo '<option value="'.$ChartAccountData["Account_ID"].'" '.$CoA_Selected.'>'.$ChartAccountData["GL_Account_Name"].'</option>';
												}
											?>
										</select>
									</td>
									<td><div class="dollar"><input type="text" name="exp_amount[]" onblur="findTotal()" class="form-control exp_amount" value="<?php echo $row["Expense_Line_Amount"]; ?>" /></div></td>
									<td><button type="button" name="remove" class="btn btn-danger btn-sm remove"><i class="fas fa-trash-alt mr-0"></i></button></td>
								</tr>
							<?php	
							$ExpenseLine = ++$ExpenseLine;
							$ExpensesAmountTotal += $row["Expense_Line_Amount"];
							}} ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="3"><b>Total Amount</b></td>
									<td colspan="2"><b id="ExpensesTotalFooter"><?php echo GUtils::formatMoney($ExpensesAmountTotal); ?></b></td>
								</tr>
							</tfoot>
						</table>
						
						<table class="table table-hover table-striped table-bordered nowrap mt-5">
							<thead>
								<tr>
									<th width="15%">Payment Date</th>
									<th width="15%">Payment Type</th>
									<th>Payment Description</th>
									<th width="15%">Payment Amount</th>
									<th width="50">Action</th>
								</tr>
							</thead>
							<tbody>
							<?php if($data['TotalPaid'] > 0) { 
							$PaymentsListSQL = "SELECT * from expenses_payments WHERE Expense_Num=$Expense_Num";
							$PaymentsListResult = $db->query($PaymentsListSQL);
							if ($PaymentsListResult->num_rows > 0) { while($PaymentList = $PaymentsListResult->fetch_assoc()) { ?>
								<tr>
									<td><?php echo date('m/d/Y', strtotime($PaymentList['Exp_Payment_Date'])); ?></td>
									<td><?php echo $PaymentList['Exp_Payment_Method']; ?></td>
									<td><?php echo $PaymentList['Exp_Payment_Comments']; ?></td>
									<td><?php echo GUtils::formatMoney($PaymentList['Exp_Payment_Amount']); ?></td>
									<td class="text-center"><a  href="javascript:void(0);" onclick = 'deleteConfirm( "expenses-edit.php?id=<?php echo $Expense_Num; ?>&action=delete&PaymentID=<?php echo $PaymentList["Exp_Payment_ID"]; ?>", "Delete")'><i class="fas fa-trash-alt mr-0"></i></a></td>
								</tr>
							<?php $TotalPaidSoFar += $PaymentList['Exp_Payment_Amount'];
							} } } else { ?>
							<tr><td colspan="5" class="text-center">There are no payments recorded for this expense at this moment, <a href="expenses-payment-action.php?action=add&id=<?php echo $data["Expense_Num"]; ?>"><b>Click here to add one.</b></a></td></tr>
							<?php } ?>
							</tbody>
							<tfoot>
								<tr>
									<td class="text-right" colspan="3"><b>Total Paid</b></td>
									<td colspan="2">
										<input type="text" id="PaymentAmount" value="<?php echo $TotalPaidSoFar; ?>" hidden>
										<b><?php echo GUtils::formatMoney($TotalPaidSoFar); ?></b>
									</td>
								</tr>
								<tr>
									<td class="text-right" colspan="3"><b>Balance Due</b></td>
									<td colspan="2"><b id="BalanceDueFooter"><?php echo GUtils::formatMoney($ExpensesAmountTotal-$TotalPaidSoFar); ?></b></td>
								</tr>
							</tfoot>
						</table>
					</div>
                    <div class="col-sm-12"><br /></div>
					<div class="form-group form-default form-static-label col-md-4">
                        <label class="float-label">Expense Comments</label>
                        <textarea name="ExpensesNotes" class="form-control" rows="6"><?php echo $data["Comments"]; ?></textarea>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label gst-label" for="Expenses_Attachment">Attachment <?php if($data["Attachment"] != NULL) { echo "<a href='uploads/".$data['Attachment']."' target='_blank' style='margin-left:100px;' download><i class='fas fa-cloud-download-alt'></i> <small>Download the attachment</small></a>"; } ?></label>
                        <div class="Attachment_Box">
                            <input type="file" id="Expenses_Attachment" name="fileToUpload" class="Attachment_Input" >
                            <p id="Expenses_Attachment_Text" class="Attachment_Text">Drag the file here or click in this area.</p>
                        </div>
                    </div>
                    <div class="col-sm-12"><br /><br />
						<button type="submit" name="addcontact" value="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Edit Expense</button>
                        <button type="reset" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Clear</button>
					</div>
				</form>
			</div>
        </div>
    </div>
</div>
<script type="text/javascript">
//To make the numbers as a currency with two decimels
const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 2
})

//This is to make the attachment box update its content when a file is selected
$(document).ready(function(){
  $('#Expenses_Attachment').change(function () {
    $('#Expenses_Attachment_Text').text("A file has been selected");
  });
});

//To generate the total amount for the expense in the right top corner
function findTotal(){
	//To get each expense line amount
	var SingleExpenseAmt = document.getElementsByName('exp_amount[]');
	var TotalExpenses=0;
	for(var i=0;i<SingleExpenseAmt.length;i++){
		if(parseFloat(SingleExpenseAmt[i].value))
			TotalExpenses += parseFloat(SingleExpenseAmt[i].value);
	}
	//To get the total paid so far amount
	var PaymentsAmount = parseFloat(document.getElementById('PaymentAmount').value);
	//Calculate the balance due Expense minus total paid
	var BalanceDueAmount = parseFloat(TotalExpenses-PaymentsAmount);
	//Format the numbers to be with 2 decimals
	TotalExpenses = formatter.format(TotalExpenses);
	BalanceDueAmount = formatter.format(BalanceDueAmount);
    //And then just show them on the fields listed below
	document.getElementById('ExpensesTotalText').innerHTML = BalanceDueAmount;
    document.getElementById('ExpensesTotalFooter').innerHTML = TotalExpenses;
    document.getElementById('BalanceDueFooter').innerHTML = BalanceDueAmount;
}

//To be able to add new rows or remove old ones
$(document).ready(function(){
 var TDnum = <?php echo $ExpenseLine; ?>;
 $(document).on('click', '.add', function(){
  var html = '';
  html += '<tr class="expensesLines">';
  html += '<td class="text-center">' + TDnum + '</td>';
  html += '<td><select name="exp_category[]" class="form-control exp_category" onchange="ChartOfAccountSelect(this)"><option value="">Select Category</option><?php echo $ExpensesSelect ?></select></td>';
  html += '<td><select name="exp_desc[]" class="form-control exp_desc"><option value="">Select the main category first</option></select></td>';
  html += '<td><div class="dollar"><input type="text" onblur="findTotal()" name="exp_amount[]" class="form-control exp_amount"></div></td>';
  html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm remove"><i class="fas fa-trash-alt mr-0"></i></button></td></tr>';
  $('#item_table').append(html);
  TDnum++;
 });

 $(document).on('click', '.remove', function(){
  $(this).closest('tr').remove();
  findTotal();
 }); 
});
function ChartOfAccountSelect(obj) {
		$(obj).closest('.expensesLines').find('.exp_desc').load("inc/expenses-categories.php?choice=" + $(obj).closest('.expensesLines').find('.exp_category').val());
}
</script>
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>