<?php
include "session.php";
$PageTitle = "Customer Payment";
include "header.php";

include_once __DIR__ . '/models/customer_payment.php' ;
include_once __DIR__ . '/models/contacts.php' ;
include_once __DIR__ . '/models/groups.php' ;
include_once __DIR__ . '/models/customer_invoice.php' ;

global $GLOBAL_SCRIPT;
$SubmitAction = 'create';

$customerId = 0 ;
if( isset($__GET['CustomerID']) ) {
    $customerId = $__GET['CustomerID'] ;
}
else if( isset($__GET['customer']) ) {
    $customerId = $__GET['customer'] ;
}
else if( isset($__GET['invoice'])) {
    $ciData = (new CustomerInvoice())->get(['Customer_Invoice_Num' => $__GET['invoice']]) ;
    $customerId = $ciData['Customer_Account_Customer_ID'] ;
}

if ( isset($__REQUEST['submit']) && isset($__POST['hidPaymentSave']) && $__POST['hidPaymentSave'] == '1') {

    include 'inc/payments-save.php';	

    if ($__POST['submit'] == 'save-back') {
		// if clicked save and go back to customer
        $customerId = $__POST['CustomerID'];
		$TheRedirectLink = "contacts-edit.php?id=".$customerId."&action=edit&usertype=sales";
        GUtils::redirect($TheRedirectLink);
    } elseif ($__POST['submit'] == 'save-new') {
		// if clicked save and new //TODO; invoiceid/customerid missing some cases, eithor one should present normally.
		$TheRedirectLink = "payments-add.php?invoice=".$__GET['invoice']."&CustomerID=".$customerId;
        GUtils::redirect($TheRedirectLink);
    } else {
		// if only "save" then save and go to the payments list
        GUtils::redirect('payments.php');
    }
}

$custRows = (new Contacts())->forSelect() ;

$invoiceId         = '';
$customerId        = '';
$where             = ' AND 0';
$paymentData       = [];
$editPaymentAmount = 0;
$isEditMode        = false;
$paymentMethodEdit = '';
$paymentAttachment = '';
$paymentDate = Date('Y-m-d');
$paymentMethod = '';
$paymentDepositTo = '';

if (isset($__GET['payment'])) {
    $isEditMode = true;
    $paymentId  = $__GET['payment'];

    $paymentData = (new CustomerPayment())->get(['Customer_Payment_ID' =>$paymentId]) ;

    $__GET['invoice']   = $paymentData['Customer_Invoice_Num'];
    $editPaymentAmount = $paymentData['Customer_Payment_Amount'];
    $paymentMethodEdit = $paymentData['Customer_Payment_Method'];
    $paymentAttachment = $paymentData['Attachment'];
    $paymentDate = $paymentData['Customer_Payment_Date'];
    $paymentMethod = $paymentData['Customer_Payment_Method'];
    $paymentDepositTo = $paymentData['Customer_Payment_Deposit_To'];

}
//TODO; must remove $__GET['customer'], since it may come from multiple groups.
$tripId = 0 ;
if (isset($__GET['invoice'])) {
    $invoiceId  = $__GET['invoice'];
    $where      = " AND ci.Customer_Invoice_Num='$invoiceId' ";

    $trips = (new Groups())->invoiceTripDetails($invoiceId) ;
    //find customer id 
    foreach ($trips as $trip) {
        $customerId = $trip['Customer_Account_Customer_ID'];
        $tripId = $trip['Group_ID'];
        break;
    }
} else if (isset($__GET['customer'])) {
    $customerId = $__GET['customer'];
    $where      = " AND ci.Customer_Account_Customer_ID ='$customerId' ";

    $trips = (new Groups())->invoicesDetailsPerCustomer($customerId) ;
    //We should fix this issue here where it only shows one invoice even if the customer has more than one invoice!!
    foreach ($trips as $trip) {
        $tripId = $trip['Group_ID'];
        break;
    }
}

//overwrite where 
if (isset($__GET['payment'])) {
        $where = " AND cp.Customer_Payment_ID='$paymentId' ";
        $groupby = ' ';
    } else {
        $groupby = ' GROUP BY ci.Customer_Invoice_Num';
}

if( $invoiceId ) {
    $record = (new CustomerPayment())->invoiceDetails($invoiceId) ;
    $records = [$record] ;
} else {
    $record = (new CustomerPayment())->customerPaymentDetails($customerId, $tripId) ; //This function acept invoice only
    $records = [$record] ;
}

?>
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->

<style>
    .dollar:before {
        position: absolute;
        font-size: 16px;
        content: "$";
        left: 5px;
        top: 2px;
    }

</style>
<div class="col-md-12 row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5>
                    <?php echo (isset($__GET['payment']) ? 'Edit payment #' . $__GET['payment'] : 'Receive Payment'); ?>
                </h5>
            </div>
            <form name="invoiceAddForm" action="" method="POST" id="contact1" class="card-block gst-block  row gst-no-right-p" enctype="multipart/form-data">

                <div class="col-md-12 row">
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Customer Name</label>
                        <select onclick="formmodified=0;" onchange="onCustomerChange(this)" required="required" name="CurrentCustomerSelected" class="js-example-basic-multiple-limit col-sm-12" multiple="multiple">
                            <?php
foreach ($custRows as $row7) {
    $selected = '';
    if ($row7['id'] == $customerId) {
        $selected = 'selected';
    }
    echo "<option $selected value='" . $row7['id'] . "' $selected>" . $row7['fname'] . " " . $row7['mname'] . " " . $row7['lname'] . "</option>";
}
?>
                        </select>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Find By Invoice ID</label>

                        <div class="input-group  col-centered gst-input-group">
                            <input type="text" name="invoice" id="idFindByInvoice" class="form-control" value="<?php echo $invoiceId; ?>" style="padding-left: 10px">
                            <span class="input-group-btn">
                                <button onclick="formmodified = 0; loadByInvoice() " type="button" class="btn btn-default">
                                    <div class="fa-search fas"></div>
                                </button>
                            </span>
                        </div>


                    </div>
                    <div class="form-group form-default form-static-label col-sm-6 text-right">
                        <h5 class="">Amount Received</h5> <br />
                        <h3 class="text-danger" id='id_received_amount' data-paid="0" data-balance="0"><?php echo GUtils::formatMoney($editPaymentAmount); ?></h3>
                    </div>
                </div>


                <div class="col-md-12 row">
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Payment Date</label>
                        <input type="date" required="required" name="invoice_date" id='id_invoice_date' value='<?php echo GUtils::clientDate($paymentDate, 'Y-m-d'); ?>' class="form-control date-picker">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Trip Reference</label>
                        <select name="trip_reference" class="form-control">
                            <?php foreach ($trips as $trip) { ?>
                            <option value="<?php echo $trip['tourid']; ?>"><?php echo $trip['tourname']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Payment Method</label>
                        <select name="payment_method" id="idSelectPaymentMethod" class="form-control" required onchange="paymentMethodChange();">
                            <option value="">Select</option>
                            <option <?php echo (($paymentMethod == 'Cash') ? 'selected' : '');?> value="Cash">Cash</option>
                            <option <?php echo (($paymentMethod == 'Bank Transfer') ? 'selected' : '');?> value="Bank Transfer">Bank Transfer</option>
                            <option <?php echo (($paymentMethod == 'Check') ? 'selected' : '');?> value="Check">Check</option>
                            <option <?php echo (($paymentMethod == 'Credit Card') ? 'selected' : '');?> value="Credit Card">Credit Card</option>
                            <option <?php echo (($paymentMethod == 'Online Payment') ? 'selected' : '');?> value="Online Payment">Online Payment</option>
                        </select>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Deposit To</label>
                        <select name="Deposit_To_None" id="Accounts_None" class="form-control" style="<?php if($paymentMethod != '') { echo 'display:none;'; } ?>">
                            <option value="0" disabled selected>Select Payment Method</option>
                        </select>
                        <select name="Deposit_To_Cash" id="Accounts_cash" class="form-control" style="<?php if($paymentMethod != 'Cash') { echo 'display:none;'; } ?>">
                            <option value="">Select</option>
                            <?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name FROM checks_bank WHERE Bank_Type='cash' AND Bank_Status=1";
					$BanksData = GDb::fetchRowSet($GetBankAccountSQL);
					foreach ($BanksData AS $BankData) { ?>
                            <option <?php if($BankData['Bank_ID'] == $paymentDepositTo) { echo "selected"; } ?> value="<?php echo $BankData['Bank_ID']; ?>"><?php echo $BankData['Bank_Name']; ?></option>
                            <?php } ?>
                        </select>
                        <select name="Deposit_To_Bank" id="Accounts_bank" class="form-control" style="<?php if($paymentMethod == 'Bank Transfer' OR $paymentMethod == 'Check') {} else { echo 'display:none;'; } ?>">
                            <option value="" data-checknumber="N/A">Select</option>
                            <?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name, Bank_Check_Number FROM checks_bank WHERE Bank_Type='bank' AND Bank_Status=1";
					$BanksData = GDb::fetchRowSet($GetBankAccountSQL);
					foreach ($BanksData AS $BankData) { ?>
                            <option <?php if($BankData['Bank_ID'] == $paymentDepositTo) { echo "selected"; } ?> value="<?php echo $BankData['Bank_ID']; ?>" data-checknumber='<?php echo $BankData['Bank_Check_Number']; ?>'><?php echo $BankData['Bank_Name']; ?></option>
                            <?php } ?>
                        </select>
                        <select name="Deposit_To_Card" id="Accounts_cards" class="form-control" style="<?php if($paymentMethod != 'Credit Card') { echo 'display:none;'; } ?>">
                            <option value="">Select</option>
                            <?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name FROM checks_bank WHERE Bank_Type='cards' AND Bank_Status=1";
					$BanksData = GDb::fetchRowSet($GetBankAccountSQL);
					foreach ($BanksData AS $BankData) { ?>
                            <option <?php if($BankData['Bank_ID'] == $paymentDepositTo) { echo "selected"; } ?> value="<?php echo $BankData['Bank_ID']; ?>"><?php echo $BankData['Bank_Name']; ?></option>
                            <?php } ?>
                        </select>
                        <select name="Deposit_To_PayPal" id="Accounts_paypal" class="form-control" style="<?php if($paymentMethod != 'Online Payment') { echo 'display:none;'; } ?>">
                            <option value="">Select</option>
                            <?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name FROM checks_bank WHERE Bank_Type='paypal' AND Bank_Status=1";
					$BanksData = GDb::fetchRowSet($GetBankAccountSQL);
					foreach ($BanksData AS $BankData) { ?>
                            <option <?php if($BankData['Bank_ID'] == $paymentDepositTo) { echo "selected"; } ?> value="<?php echo $BankData['Bank_ID']; ?>"><?php echo $BankData['Bank_Name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div id="CheckNumberDiv" class="form-group form-default form-static-label col-sm-3" style="display:none;">
                        <label class="float-label gst-label">Check Number <small>(Below is the automatic number)</small></label>
                        <input type="text" name="Check_Number" id='Check_Number' placeholder="Select a bank to get the check number" class="form-control">
                    </div>
                </div>

                <div class="col-md-12 row">

                    <div class="dt-responsive table-responsive col-sm-12">

                        <b style="padding-bottom: 10px;float: left;width: 100%;">Outstanding Transaction</b>
                        <table id="basic-btn" class="table table-hover table-striped table-bordered nowrap gst-table-margin" data-page-length="20">
                            <thead>
                                <tr>
                                    <th style="display: none" width="1%"></th>
                                    <th width="10%">Invoice #</th>
                                    <th>Trip Name</th>
                                    <th>Description</th>
                                    <th width="10%">Due Date</th>
                                    <th width="10%">Original Amount</th>
                                    <th width="10%">Open Balance</th>
                                    <th width="1%">Payment Amount</th>
                                </tr>
                            </thead>
                            <tbody id='idInvoiceLineSection'>
                                <?php 

foreach ($records as $row) {    

    if (isset($__GET['payment'])) {
        $totalPayment = (new CustomerPayment())->paymentTotal($row['Customer_Invoice_Num']) ;
    }
    else {
        $totalPayment = $row['total'] ;
    }
    $balanceAmount = $row["balance"] ;
    ?>
                                <tr>
                                    <td style="display: none">
                                        <?php if (($row["Invoice_Amount"] - $row['payments']) > 0.0) { ?>
                                        <input name="cbPayment[<?php echo $row["Customer_Invoice_Num"]; ?>]" value="<?php echo $row["Customer_Invoice_Num"]; ?>" type="checkbox" class="payment-checkbox" onclick="togglePaymentInput(this)" checked="checked" />
                                        <?php } ?>
                                    </td>
                                    <td><?php echo $row["Customer_Invoice_Num"]; ?></td>
                                    <td><?php echo $row["tourname"]; ?></td>
                                    <td><input type="text" name="description[<?php echo $row["Customer_Invoice_Num"]; ?>]" value="<?php echo (($isEditMode) ? $row['Customer_Payment_Comments'] : '') ;?>" /></td>
                                    <td><?php echo GUtils::clientDate($row["Due_Date"]); ?></td>
                                    <td><?php echo GUtils::formatMoney($row["Invoice_Amount_Revised"]); ?></td>
                                    <td><?php echo GUtils::formatMoney($balanceAmount); ?></td>
                                    <td onclick="autoshowPayment(this)">
                                        <div class="dollar"><input data-balance="<?php echo $balanceAmount; ?>" type="text" name="payment[<?php echo $row["Customer_Invoice_Num"]; ?>]" class="payment-input" style=" <?php /*echo ( ($isEditMode) ? '' : ': hidden');*/ ?>" onchange="onChangePayment(this)" value="<?php echo $editPaymentAmount; ?>" /></div>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                            <tfoot>
                                <tr class="payment-charges">
                                    <td class="text-right" colspan="6">Credit Card Charges (4%)</td>
                                    <td id="idCreditCardCharges"></td>
                                </tr>
                            </tfoot>
                        </table>

                        <input type="hidden" name="hidPaymentSave" value="1" />
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label gst-label" for="Payment_Attachment">Attachment <?php if ( strlen($paymentAttachment) > 0) { ?><a href='<?php echo GUtils::doDownload($paymentAttachment, 'invattach/'); ?>' target='_blank' style='margin-left:100px;' download><i class='fas fa-cloud-download-alt'></i> <small>Download the attachment</small></a><?php } ?></label>
                        <div class="Attachment_Box">
                            <input type="file" id="Payment_Attachment" name="filAttachment" class="Attachment_Input" >
                            <p id="Payment_Attachment_Text" class="Attachment_Text">Drag the file here or click in this area.</p>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 row">
                    <div class="btn-group mr-2">
                        <input type="text" value="<?php echo $customerId; ?>" name="CustomerID" hidden>
                        <button type="submit" onclick="formmodified = 0;window.onbeforeunload = null;" value="save-back" name="submit" class="btn btn-success"><i class="far fa-check-circle"></i> Save & Back to Customer</button>
                        <button type="button" class="btn btn-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <div class="dropdown-menu">
                            <button type="submit" onclick="formmodified = 0;window.onbeforeunload = null;" value="save-new" name="submit" class="dropdown-item">Save & New</button>
                            <button type="submit" onclick="formmodified = 0; window.onbeforeunload = null;" value="save" name="submit" class="dropdown-item">Save & Close</button>
                        </div>
                    </div>
                    <button type="button" onclick="window.onbeforeunload = null; window.location.href = 'payments.php'" class="btn waves-effect waves-light btn-inverse mr-2"><i class="fas fa-ban"></i>Cancel</button>
                    <?php if( isset($row["Customer_Invoice_Num"]) ) { ?>
                    <a target="_blank" href="sales-invoice-print.php?print=payment&invid=<?php echo $row["Customer_Invoice_Num"] ; ?>">
                        <button type="button" class="btn waves-effect waves-light btn-info"><i class="fas fa-print"></i>Print</button>
                    </a>
                    <?php } ?>


                </div>
                <div class="col-md-12 row">
                    <div class="gst-spacer-10"></div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-md-2">
    </div>
</div>




<?php include 'inc/notificiations.php'; ?>

<script type="text/javascript">
    function loadByInvoice() {
        window.location.href = 'payments-add.php?invoice=' + $('#idFindByInvoice').val();
    }

    function autoshowPayment(obj) {
        if (!$(obj).parent().find('.payment-checkbox').prop('checked')) {
            $(obj).parent().find('.payment-checkbox').trigger('click');
        }
    }

    function togglePaymentInput(obj) {

        //        if ($(obj).prop('checked')) {
        //            $(obj).parent().parent().find('.payment-input').css('visibility', 'visible');
        //            $(obj).parent().parent().find('.payment-input').focus();
        //        }
        //        else {
        //            $(obj).parent().parent().find('.payment-input').css('visibility', 'hidden');
        //        }
    }

    function onSaveInvoice() {
        $('#contact1').submit(function(e) {


            $('#resultsmodal #results').html('Saving ...');
            $('#resultsmodal').modal('show');
            return true;
        });
    }
    onSaveInvoice();

    //$(document).ready(function () {
    //    $("form").submit(function () {
    //        // Getting the form ID
    //        var formID = $(this).attr('id');
    //        var formDetails = $('#' + formID);
    //        //To not let the form of uploading attachments included in this
    //        var formData = formDetails.serialize() ;
    //        formData = "action=<?php //echo $SubmitAction;  ?>//-invoice&" + formData ;
    //
    //        if (formID != 'contact6') {
    //            $.ajax({
    //                type: "POST",
    //                url: 'inc/ajax.php',
    //                data: formData,
    //                success: function (data) {
    //                    // Inserting html into the result div
    //                    $('#results').html(data);
    //                    //$("form")[0].reset();
    //                    formmodified = 0;
    //                },
    //                error: function (jqXHR, text, error) {
    //                    // Displaying if there are any errors
    //                    $('#result').html(error);
    //                }
    //            });
    //            return false;
    //        }
    //    });
    //});

    //Notification if the form isnt saved before leaving the page
    $(document).ready(function() {
        formmodified = 0;
        $('form *').change(function() {
            formmodified = 1;
        });
        window.onbeforeunload = confirmExit;

        function confirmExit() {
            if (formmodified == 1) {
                return "New information not saved. Do you wish to leave the page?";
            }
        }
    });

$(document).ready(function(){
    $('#idSelectPaymentMethod').on('change', function() {
      if ( this.value == 'Cash') {
        $("#Accounts_cash").show('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").hide('slow');
        $("#Accounts_bank").hide('slow');
        $("#Accounts_paypal").hide('slow');
        $("#CheckNumberDiv").hide('slow');
      } else if ( this.value == 'Bank Transfer') {
        $("#Accounts_cash").hide('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").hide('slow');
        $("#Accounts_bank").show('slow');
        $("#Accounts_paypal").hide('slow');
        $("#CheckNumberDiv").hide('slow');
      } else if ( this.value == 'Check') {
        $("#Accounts_cash").hide('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").hide('slow');
        $("#Accounts_bank").show('slow');
        $("#Accounts_paypal").hide('slow');
        $("#CheckNumberDiv").show('slow');
      } else if ( this.value == 'Credit Card') {
        $("#Accounts_cash").hide('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").show('slow');
        $("#Accounts_bank").hide('slow');
        $("#Accounts_paypal").hide('slow');
        $("#CheckNumberDiv").hide('slow');
      } else if ( this.value == 'Online Payment') {
        $("#Accounts_cash").hide('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").hide('slow');
        $("#Accounts_bank").hide('slow');
        $("#Accounts_paypal").show('slow');
        $("#CheckNumberDiv").hide('slow');
      }
    });
});


//This is to make the attachment box update its content when a file is selected
$(document).ready(function(){
  $('#Payment_Attachment').change(function () {
    $('#Payment_Attachment_Text').text("A file has been selected");
  });
});
    

var selection = document.getElementById("Accounts_bank");
selection.onchange = function(event){
  var checknumber = event.target.options[event.target.selectedIndex].dataset.checknumber;
  document.getElementById("Check_Number").value = checknumber;
};
</script>

<!-- sweet alert js -->
<script type="text/javascript" src="files/bower_components/sweetalert/js/sweetalert.min.js"></script>
<!-- modalEffects js nifty modal window effects -->
<script type="text/javascript" src="files/assets/js/modalEffects.js"></script>

<script type="text/javascript">
    function onChangePayment(obj) {

        var amount = $(obj).val();
        var bal = $(obj).data('balance');

        if (bal - amount < 0) {
            //            $(obj).val(bal);
            $(obj).addClass('text-danger');
        } else {
            $(obj).removeClass('text-danger');
        }

        updateTotal();
    }

    function updateTotal() {
        //To make the value of money minus if the GST staff choose Refund option
        if ($('#idSelectPaymentMethod').val() === 'Refund') {
            var inputVal = $(".payment-input").val();
            var MakeItMinus = 0 - inputVal;
            document.getElementsByClassName("payment-input")[0].value = MakeItMinus;
        } else {
            var inputVal = $(".payment-input").val();
            document.getElementsByClassName("payment-input")[0].value = Math.abs(inputVal);
        }

        var total = 0.0;
        $('.payment-input').each(function() {
            total += parseFloat($(this).val()) || 0.0;
        });

        var ccharge = 0;
        if ($('#idSelectPaymentMethod').val() === 'Credit Card') {
            ccharge = total * 4 / 100;
        }
        // Total = total + ccharge ;
        // Comment this line if you dont want to update total invoice amount.
        $('#id_received_amount').data('balance', total);
        $('#id_received_amount').html(formatMoney(total));
        $('#idCreditCardCharges').html(formatMoney(ccharge));
    }

    function paymentMethodChange() {
        if ($('#idSelectPaymentMethod').val() === 'Credit Card') {
            $('.payment-charges').show();
        } else {
            $('.payment-charges').hide();
        }

        updateTotal();
    }
    paymentMethodChange();
    <?php
        global $GLOBAL_SCRIPT ;
        $GLOBAL_SCRIPT .= 'updateTotal();' ;
    ?>

    function loadCustomerDetails(val) {
        //reset first
        $('#id_email').val('');
        $('#id_billing_address').val('');
        $('#id_due_date').val('');
        $('#id_received_amount').html(formatMoney(0.0));
        $('#id_received_amount').data('balance', '0.0');

        if (val !== undefined) {
            ajaxCall('inc/ajax.php', {
                'action': 'customer-details',
                'id': val
            }, function(data) {
                if (typeof data === 'object') {
                    $('#id_email').val(data.email);
                    $('#id_billing_address').val(data.address);
                    $('#id_due_date').val(data.due_date);
                    $('#id_received_amount').html(formatMoney(data.balance));
                    $('#id_received_amount').data('balance', data.balance);
                }
            });
        }
    }

    function invoiceLineSelected(obj) {
        var val = $(obj).val();
        var description = $(obj).find("option[value='" + val + "']").data('description');
        var quantity = $(obj).find("option[value='" + val + "']").data('quantity');
        $(obj).closest('.invoice-line-tr').find('.description').html(description);
        $(obj).closest('.invoice-line-tr').find('.quantity').val(quantity);

        invoiceLineUpdatePrice($(obj).closest('.invoice-line-tr').find('.quantity'));
    }

    function invoiceLineUpdatePrice(textobj) {
        var qty = $(textobj).val();
        var sel = $(textobj).closest('.invoice-line-tr').find('.productSelect');
        var val = $(sel).val();

        var price = $(sel).find("option[value='" + val + "']").data('price');
        $(sel).closest('.invoice-line-tr').find('.price').html(formatMoney(price));
        var newprice = price * qty;

        $(sel).closest('.invoice-line-tr').find('.total').html(formatMoney(newprice));
        $(sel).closest('.invoice-line-tr').find('.total').data('price', newprice);

        updateTotal();
    }

    function setInvoiceProductSelection(rowid, prodid) {
        $('.row-' + rowid + " select option[value='" + prodid + "']").prop("selected", true);
        $('.row-' + rowid + " select").change();
    }

    function onCustomerChange(obj) {
        formmodified = 0;
        window.onbeforeunload = null;
        if (obj.value) {
            window.location.href = 'payments-add.php?customer=' + obj.value;
        }
    }

</script>
<?php include "footer.php"; ?>
