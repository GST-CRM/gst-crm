<?php include "session.php";
$PageTitle = "Add A Sales Order";
include "header.php";

include 'inc/sales-order-functions.php';

$WhereCustomer = "";

if(isset($_POST['Sales_Order_Date'])) {$Sales_Order_Date = mysqli_real_escape_string($db, $_POST['Sales_Order_Date']);}
if(isset($_POST['Sales_Order_Due_Date'])) {$Sales_Order_Due_Date = mysqli_real_escape_string($db, $_POST['Sales_Order_Due_Date']);}

//If the form has been submitted but there was error, to get the post, or read from GET action
if(isset($_POST['CustomerID'])) {
	$CustomerSelected = mysqli_real_escape_string($db, $_POST['CustomerID']);
} elseif(isset($_GET['CustomerID'])) {
	$CustomerID = mysqli_real_escape_string($db, $_GET['CustomerID']);
} else {
	$CustomerID = 0;
}

if ($CustomerID != "" AND $CustomerID > 0) {
	$WhereCustomer = "AND (LandPro.supplierid=".$CustomerID." OR AirPro.supplierid=".$CustomerID.")";
	$CustomerSelected = $CustomerID;
}
if(!empty($CustomerSelected)) { 
	$CustomerzData = explode(':', $CustomerSelected);
	$CustomerzDataID = $CustomerzData[0];
	$ShowMe = "";
	$GetTheCustomerContactDetails = "SELECT co.fname,co.mname,co.lname,co.email,co.address1,co.address2,co.zipcode,co.state,co.city FROM contacts co WHERE co.id=$CustomerzDataID";
	$ContactDetailz = GDb::fetchRowSet($GetTheCustomerContactDetails);
	$CustomerzDataName = $ContactDetailz[0]['fname']." ".$ContactDetailz[0]['mname']." ".$ContactDetailz[0]['lname'];
	$CustomerzDataEmail = $ContactDetailz[0]['email'];
	$CustomerzDataAddress = $ContactDetailz[0]['address1']."\n".$ContactDetailz[0]['address2']."\n".$ContactDetailz[0]['city'].", ".$ContactDetailz[0]['state'].", ".$ContactDetailz[0]['zipcode'];

	$GetSalesOrderIdentifierSQL = "SELECT MAX(Sales_Order_Identifier)+1 AS NewIdentifier FROM sales_order WHERE Customer_Account_Customer_ID='$CustomerzDataID'";
	$GetSalesOrderIdentifier = GDb::fetchRowSet($GetSalesOrderIdentifierSQL);
	$LatestSalesOrderIdentifier = $GetSalesOrderIdentifier[0]['NewIdentifier'];
	
} else {
	$ShowMe = "d-none";
	$CustomerzDataName = "";
	$CustomerzDataEmail = "";
	$CustomerzDataAddress = "";
	$LatestSalesOrderIdentifier = 0;
}

//Trying to make the information pass through URL, not needed for now
/* $URLfeed = array();
$URLfeed['Order_Date'] = $Sales_Order_Date;
$URLfeed['Due_Date'] = $Sales_Order_Due_Date;
$URLfeed['Term'] = $Term;
$URLfeed['ShowMe'] = $ShowMe;
$URLfeed['CustomerID'] = $CustomerzDataID;
$URLfeed['CustomerzDataName'] = $CustomerzDataName;
$URLfeed['CustomerzDataEmail'] = $CustomerzDataEmail;
$URLfeed['CustomerzDataAddress'] = $CustomerzDataAddress;
//print_r($URLfeed);
$URLfeedSerialize = serialize($URLfeed);
$URLfeedEncode = base64_encode($URLfeedSerialize);

$Result = "YTo4OntzOjEwOiJPcmRlcl9EYXRlIjtzOjA6IiI7czo4OiJEdWVfRGF0ZSI7czowOiIiO3M6NDoiVGVybSI7TjtzOjY6IlNob3dNZSI7czowOiIiO3M6MTA6IkN1c3RvbWVySUQiO3M6NToiMTUzMDIiO3M6MTc6IkN1c3RvbWVyekRhdGFOYW1lIjtzOjMxOiJMYXRvbnlhIFNoZXJyaWUgU3RlcGhlbnNvbi1OZWFsIjtzOjE4OiJDdXN0b21lcnpEYXRhRW1haWwiO3M6MjE6ImxhdG9ueWFuZWFsQGdtYWlsLmNvbSI7czoyMDoiQ3VzdG9tZXJ6RGF0YUFkZHJlc3MiO3M6MzM6IjQ5MTIgQ2F0aGF5IFN0CgpEZW52ZXIsIENPLCA4MDI0OSI7fQ";
$URLfeedDecode = base64_decode($Result);
$URLfeedUnserialize = unserialize($URLfeedDecode);
 *///echo $URLfeedUnserialize['CustomerID'];
?>
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {background:white;color:#333;padding:2px 30px 3px 20px;border-radius:4px;}
.select2-container--default .select2-selection--single .select2-selection__arrow {top:4px;}
.select2-container--default .select2-selection--single .select2-selection__arrow b {border-color:black transparent transparent transparent;}
.select2-selection {height:35px;padding:0px !important;}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-block">
				<form name="contact5" action="" method="POST" class="row" id="contact5" enctype="multipart/form-data">
                    <div class="form-default form-static-label col-sm-3">
						<input type="hidden" id="submitBtn" name="submitBtn" value="" >
                        <label id="CustomerNameLabel" for="SpecificCustomer" class="float-label">Customer Name</label>
						<input value="NEW" name="SalesOrderType" type="hidden">
						<input value="<?php echo $LatestSalesOrderIdentifier; ?>" name="LatestSalesOrderIdentifier" type="hidden">
						<select id="SpecificCustomer" name="CustomerID" class="col-sm-12" style="padding:0px" onChange="doReload(this.value);">
							<?php
							//To show the selected customer if it was selected in the GET variable
							if(!empty($CustomerSelected)) { 
								echo "<option value='".$CustomerzDataID."' selected>".$CustomerzDataName."</option>";
							} else { 
								echo "<option>-</option>";
							}
							?>
						</select>
						<div class="invalid-feedback">
							Please choose a customer.
						</div>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-2">
                        <label class="float-label gst-label" for="Sales_Order_Date">Sales Order Date</label>
                        <input type="date" id="Sales_Order_Date" name="Sales_Order_Date" value="<?php if($Sales_Order_Date==""){ echo date("Y-m-d"); } else { echo $Sales_Order_Date; } ?>" class="form-control date-picker" required>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-2">
                        <label class="float-label gst-label" for="Sales_Order_Due_Date">Sales Order Due Date</label>
                        <input type="date" id="Sales_Order_Due_Date" name="Sales_Order_Due_Date" value="<?php echo $Sales_Order_Due_Date; ?>" class="form-control" required>
                    </div>
                    <div class="form-default form-static-label col-sm-3 text-center">
                    </div>
                    <div class="form-default form-static-label col-sm-2 text-center">
                        <h5>Amount</h5>
                        <h3 class="text-danger" id="SupplierBillTotal">$0.00</h3>
						<input name="SalesOrderTotal" type="hidden" value="" class="HiddenTotal">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3 <?php echo $ShowMe; ?>">
                        <label class="float-label gst-label" for="BillingAddress">Billing Address</label>
						<textarea id="BillingAddress" class="form-control" rows="3" readonly><?php echo $CustomerzDataAddress; ?></textarea>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4 <?php echo $ShowMe; ?>">
                        <label class="float-label gst-label" for="EmailAddress">Email Address</label>
						<input type="email" id="EmailAddress" value="<?php echo $CustomerzDataEmail; ?>" class="form-control" readonly>
                    </div>
                    <div class="col-sm-12"><br /></div>
                    <div class="col-sm-12 table-responsive">
					<h4 class="sub-title">Sales Order Details</h4>
						<table class="table table-hover table-striped table-bordered nowrap" id="item_table">
							<thead>
								<tr>
									<th width="30" class="text-center">#</th>
									<th width="20%">Product / Service</th>
									<th>Description</th>
									<th width="100">Quantity</th>
									<th width="150">Amount</th>
									<th width="150">Total</th>
									<th width="50"><button type="button" name="add" class="btn btn-success btn-sm add"><i class="fas fa-plus mr-0"></i></button></th>
								</tr>
							</thead>
							<?php
							//To make the list of products in PHP Variable
							$ProductSelect = '';
							$ProductSelect .= '<option value="0">Select a Product</option>';
							$ProductsListResult = mysqli_query($db,"SELECT * FROM products WHERE status=1");
							while($SupplierProducts = mysqli_fetch_array($ProductsListResult)) {
								$ProductName = str_replace("'"," ",$SupplierProducts['name']);
								$ProductDescription = str_replace("'"," ",$SupplierProducts['description']);
								$ProductSelect .='<option value="'.$SupplierProducts["id"].'" data-description="' . $ProductDescription . '" data-cost="' . $SupplierProducts['price'] . '" data-quantity="0" data-category="' . $SupplierProducts['categoryid'] . '" data-name="' .$ProductName. '">'.$ProductDescription.'</option>';
							} ?>
							<tfoot>
								<tr>
									<th colspan="5" class="pt-2 pb-2 text-right">Total</th>
									<th colspan="2" class="pt-2 pb-2 FooterTotal">$0.00</th>
								</tr>
							</tfoot>
						</table>
					</div>
                    <div class="col-sm-12"><br /></div>
					<div class="form-group form-default form-static-label col-md-4">
                        <label class="float-label" for="Sales_Order_Comments">Sales Order Comments</label>
                        <textarea id="Sales_Order_Comments" name="Sales_Order_Comments" class="form-control" rows="6"><?php echo $Sales_Order_Comments; ?></textarea>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <label class="float-label gst-label" for="Sales_Order_Attachment">Attachment</label>
                        <div class="Attachment_Box">
                            <input type="file" id="Sales_Order_Attachment" name="fileToUpload" class="Attachment_Input" >
                            <p id="Sales_Order_Attachment_Text" class="Attachment_Text">Drag the file here or click in this area.</p>
                        </div>
                    </div>
                    <div class="col-sm-12"><br /><br />
						<button type="submit" name="addcontact" value="submit" class="btn waves-effect waves-light btn-success mr-1 mt-2" data-toggle="modal" data-target="#resultsmodal" data-val="save_new"><i class="far fa-check-circle"></i>Save & New</button>
						<button type="submit" name="addcontact" value="submit" class="btn waves-effect waves-light btn-info mr-1 mt-2" data-toggle="modal" data-target="#resultsmodal" data-val="save_close"><i class="far fa-check-circle"></i>Save & Close</button>
                        <button type="reset" class="btn waves-effect waves-light btn-inverse mt-2"><i class="fas fa-ban"></i>Clear</button>
					</div>
				</form>
			</div>
        </div>
    </div>
</div>
<script type="text/javascript">
//To make the numbers as a currency with two decimels
const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 2
})

//This is to make the attachment box update its content when a file is selected
$(document).ready(function(){
  $('#Sales_Order_Attachment').change(function () {
    $('#Sales_Order_Attachment_Text').text("A file has been selected");
  });
});

//This is to get the details of each product and show it in that line
function BillLineSelected(obj) {
	var val = $(obj).val();
	var description = $(obj).find("option[value='" + val + "']").data('description');
	var cost = $(obj).find("option[value='" + val + "']").data('cost');
	var quantity = $(obj).find("option[value='" + val + "']").data('quantity');
	var name = $(obj).find("option[value='" + val + "']").data('name');
	var category = $(obj).find("option[value='" + val + "']").data('category');
	$(obj).closest('.bill-line-tr').find('.product_desc').html(description);
	$(obj).closest('.bill-line-tr').find('.product_price').html(formatMoney(cost));
	$(obj).closest('.bill-line-tr').find('.product_price').val(cost);
	$(obj).closest('.bill-line-tr').find('.product_name').val(name);
	$(obj).closest('.bill-line-tr').find('.product_category').val(category);
	$(obj).closest('.bill-line-tr').find('.SO_line_quantity').val(quantity);
	var BillLineTotal = quantity * cost;
	$(obj).closest('.bill-line-tr').find('.bill_line_total').html(formatMoney(BillLineTotal));
	$(obj).closest('.bill-line-tr').find('.bill_line_total').data('cost', BillLineTotal);

	Bill_Line_Qty_Change($(obj).closest('.bill-line-tr').find('.SO_line_quantity'));
}


function Bill_Line_Qty_Change(textobj) {
	var qty = $(textobj).val();
	var sel = $(textobj).closest('.bill-line-tr').find('.Bill_Line_Product');
	var val = $(sel).val();

	var cost = $(sel).find("option[value='" + val + "']").data('cost');
	$(sel).closest('.bill-line-tr').find('.product_price').html(formatMoney(cost));
	var TotalLineCost = cost * qty;

	$(sel).closest('.bill-line-tr').find('.bill_line_total').html(formatMoney(TotalLineCost));
	$(sel).closest('.bill-line-tr').find('.bill_line_total').data('cost', TotalLineCost);

	updateTotal();
}

function updateTotal() {
	var total = 0;

	$('.bill-line-tr .bill_line_total').each(function () {
		total += parseFloat($(this).data('cost'));
	});
	var PaidSoFar = 0;
	var FinalAmount = total - PaidSoFar;
	$('#SupplierBillTotal').html(formatMoney(FinalAmount));
	$('.FooterTotal').html(formatMoney(total));
	$('.HiddenTotal').val(total);

}

//To be able to add new rows or remove old ones
$(document).ready(function(){
	var TDnum = 1;
	var CustomerSelectDropDown = document.getElementById('SpecificCustomer').value;
	$(document).on('click', '.add', function(){
		//if ($("#SpecificCustomer option").is(":selected")) {
		if (CustomerSelectDropDown > 0) {
			var html = '';
			html += '<tr class="bill-line-tr">';
			html += '<td class="text-center">'+ TDnum +'<input type="hidden" name="exp_id[]" class="form-control exp_id" value="'+ TDnum +'" /></td>';
			html += '<td><select id="Product_'+ TDnum +'" name="SO_Product[]" class="form-control Bill_Line_Product js-example-basic-single select2" onchange="BillLineSelected(this)">';
			html += '<?php echo $ProductSelect; ?>';
			html += '</select></td>';
			html += '<td><span class="product_desc"></span></td>';
			html += '<td><input type="number" onchange="Bill_Line_Qty_Change(this)" name="SO_line_quantity[]" class="form-control SO_line_quantity" value="" /></td>';
			html += '<td><span class="product_price"></td>';
			html += '<td><span class="bill_line_total"></span></td>';
			html += '<td><button type="button" name="remove" class="btn btn-danger btn-sm remove"><i class="fas fa-trash-alt mr-0"></i></button></td>';
			html += '<td style="display:none;"><input type="text" class="product_name" name="product_name[]" value="">';
			html += '<input type="text" class="product_price" name="product_price[]" value="">';
			html += '<input type="text" class="product_category" name="product_category[]" value="">';
			html += '</td></tr>';
			$('#item_table').append(html);
			
			$('#Product_'+ TDnum).select2({closeOnSelect: true});
			$(".invalid-feedback").css("display", "none");
			TDnum++;
			
		} else {
			$(".invalid-feedback").css("display", "block");
			var element = document.getElementById("CustomerNameLabel");
			element.classList.add("text-danger");
		}
	});

	$(document).on('click', '.remove', function(){
		$(this).closest('tr').remove();
	});

    $('.btn').click(function() {
          var buttonval    = $(this).attr('data-val');
          $("#submitBtn").val(buttonval);
    }) 
});

//To refresh the page everytime a customer is selected
function doReload(CusID){
	document.location = 'sales-add.php?CustomerID=' + CusID;
}


//To create select2 function for the customers dropdown
$(document).ready(function(){
   $("#SpecificCustomer").select2({
      ajax: {
        url: "inc/select2-customers.php",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
           return {
              searchTerm: params.term
           };
        },
        processResults: function (response) {
           return {
              results: response
           };
        },
        cache: true
      }
   });
});

</script>
<?php include "footer.php"; ?>