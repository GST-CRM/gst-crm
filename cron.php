<?php 
include "session.php";
require_once "inc/helpers.php"; 

$CronGetListSQL = "SELECT * FROM cron_schedular WHERE Cron_Status=1;";
$CronGetListData = GDb::fetchRowSet($CronGetListSQL);

echo "<h2>The number of cron jobs in the system is ".count($CronGetListData).", and they are as following:</h2>";
foreach($CronGetListData as $CronJob) {
	echo "<strong>Cron Job:</strong> ".$CronJob['Cron_Name'];
	echo "<br />";
	
	if($CronJob['Cron_Loop'] == 99) {
		$Cron_Period = $CronJob['Cron_Period'];
		$Cron_Days = $CronJob['Cron_Days'];
		
		if($Cron_Period == "Before") {
			$GroupsDateDiffList = "SELECT tourid, tourname, startdate FROM groups WHERE status=1 AND DATEDIFF(startdate,NOW())=$Cron_Days;";
		} elseif($Cron_Period == "After") {
			$GroupsDateDiffList = "SELECT tourid, tourname, enddate FROM groups WHERE status=1 AND DATEDIFF(CURDATE(),(DATE_ADD(enddate, INTERVAL $Cron_Days DAY)))=0;";
		}
		$GroupDiff = GDb::fetchRowSet($GroupsDateDiffList);
		echo "<strong>Initiate Time:</strong> <i>".$Cron_Period." ".$Cron_Days." days</i><br />";
		echo "<strong>Cron Job Note:</strong> <i>There are ".count($GroupDiff)." groups that this cron job fits with.</i><br />";
		echo "<strong>Results:</strong> <small>"; print_r($GroupDiff);echo "</small>";
	} elseif ($CronJob['Cron_Loop'] == 1) {
		echo "<strong>Initiate Time:</strong> <i>This cron job will send the emails on ".$CronJob['Cron_Schedule_Date']."</i>";
	} else {
		if($CronJob['Cron_Loop'] == 2) { $scheduled_for = "Hourly";}
		if($CronJob['Cron_Loop'] == 3) { $scheduled_for = "Daily";}
		if($CronJob['Cron_Loop'] == 4) { $scheduled_for = "Weekly";}
		if($CronJob['Cron_Loop'] == 5) { $scheduled_for = "Monthly";}
		echo "<strong>Initiate Time:</strong> <i>This cron job will send the emails on a ".$scheduled_for." basis.</i>";
	}
	echo "<hr >";
	echo "<br />";
}


?>