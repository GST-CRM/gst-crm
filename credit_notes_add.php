<?php
include "session.php";
$PageTitle = "Credit Notes";
include "header.php";

include_once __DIR__ . '/models/customer_credit_note.php' ;
include_once __DIR__ . '/models/customer_invoice.php' ;
include_once __DIR__ . '/models/contacts.php' ;

global $GLOBAL_SCRIPT;
$SubmitAction = 'create';
$BACK_URL = 'credit_notes.php' ; //TODO; use credit-notes.php
$invoiceId = '' ;
//If post, then include the action file.
if (isset($__POST['hidInvoiceSave']) && $__POST['hidInvoiceSave'] == '1') {
    include __DIR__ . '/inc/credit-notes-save.php';
    
    $afterPayment = '' ;
    if( isset($__GET['CustomerID']) ) {
        $afterPayment = "contacts-edit.php?id=".$__GET['CustomerID']."&action=edit&usertype=sales" ;
        GUtils::redirect($afterPayment);
    }
    else {
        GUtils::redirect($BACK_URL);
    }
}

$editContactId = 0 ;

//this page can be loaded basd on customerId or invoice, in both case dynamically bulid curresponding sql condition.
if (isset($__GET['customerId'])) {
    $editContactId = $__GET['customerId'] ;
    $creditCond = " Status=1 AND Customer_Account_Customer_ID=" . $__GET['customerId'] ;
}
else if (isset($__GET['invid'])) {
    $invoiceId = $__GET['invid'] ;
    $creditCond = " Status=1 AND Customer_Invoice_Num=" . $__GET['invid'] ;
}
else if (isset($__GET['noteid'])) {
    $creditCond = " Note_ID=" . $__GET['noteid'] ;
}
else {
    $creditCond = " 1 = 0 " ;
}
//TODO; edit amount if only credit_note_edit via $__GET['noteid']
$noteId = isset($__GET['noteid']) ? $__GET['noteid'] : null ;

//get the credit note details.
$creditNotes = (new CustomerCreditNote())->getList($creditCond) ;

//find total credit amount.
$creditTotal = 0 ;
$paymentMethodEdit = 0 ;
foreach( $creditNotes as $row ) {
    $creditTotal += $row['Credit_Amount'] ;
    $paymentMethodEdit = $row['Full_Discount'] ;
}

//Edit Mode {

    //get the credit and invoice details.
    $sqle = '' ;
    if( isset($__GET['invid']) ) {
        //by invoice id
        //' GROUP BY ci.Customer_Invoice_Num '  will prevent editing the credit note, but so far we dont have editing
        $sqle = "SELECT ci.Invoice_Amount, ci.Customer_Invoice_Num, cn.Credit_Amount, cn.Credit_Reason FROM customer_invoice ci
                INNER JOIN customer_account ca ON ca.contactid = ci.Customer_Account_Customer_ID AND ca.status=1
                LEFT JOIN customer_credit_note cn ON cn.Customer_Invoice_Num = ci.Customer_Invoice_Num
                WHERE ci.Customer_Invoice_Num=" . $__GET['invid'] . ' GROUP BY ci.Customer_Invoice_Num '  ;
        
        $invoiceData = (new CustomerInvoice())->get(['Customer_Invoice_Num' => $__GET['invid']]) ;
        $editContactId = $invoiceData['Customer_Account_Customer_ID'] ;
    }
    else if( isset($__GET['customerId']) ) {
        //by cust id
        $sqle = "SELECT ci.Invoice_Amount, ci.Customer_Invoice_Num, cn.Credit_Amount, cn.Credit_Reason FROM customer_invoice ci
                INNER JOIN customer_account ca ON ca.contactid = ci.Customer_Account_Customer_ID AND ca.status=1
                LEFT JOIN customer_credit_note cn ON cn.Customer_Invoice_Num = ci.Customer_Invoice_Num
                WHERE ca.contactid =" . $__GET['customerId'] ;
        
        $editContactId = $__GET['customerId']; 
    }
    else if( isset($__GET['noteid']) ) {
        //by cust id
        $sqle = "SELECT ci.Invoice_Amount, ci.Customer_Invoice_Num, cn.Credit_Amount, cn.Credit_Reason FROM customer_invoice ci
                INNER JOIN customer_account ca ON ca.contactid = ci.Customer_Account_Customer_ID AND ca.status=1
                INNER JOIN customer_credit_note cn ON cn.Customer_Invoice_Num = ci.Customer_Invoice_Num
                WHERE cn.Note_ID=" . $__GET['noteid'] ;
        
        $creditOneData = (new CustomerCreditNote())->get(['Note_ID' => $__GET['noteid']]) ;
        $editContactId = $creditOneData['Customer_Account_Customer_ID'] ;
    }

    if( $sqle ) {
        $invoices = GDb::fetchRowSet($sqle);
    }
    else {
        $invoices = [] ;
    }
    
    $contactInfo = (new Contacts())->get(['id' => $editContactId]) ;
    
    //formate billing address.
    $address = GUtils::buildGSTAddress( false, $contactInfo, ",", "\n" ) ;
    
//} Edit Mode

//find all customers list for select box
$custRows = (new Contacts())->forSelect() ;
?>

<form name="invoiceAddForm" action="" method="POST" id="contact1" enctype="multipart/form-data" >
            <div class="card">
                <div class="card-header">
                    <h5>Issue Credit Note</h5>
                </div>
                <div class="card-block gst-block row">
                    <div class="form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Customer Name</label>
                        <select onchange="return loadCustomerDetails(this.value);" required="required" name="CurrentCustomerSelected" class="js-example-basic-multiple-limit col-sm-12 select2-hidden-accessible" multiple="multiple">
						<?php
						foreach ($custRows as $row7) {
							$selected = '';
							if ($row7['id'] == $editContactId && $editContactId) {
								$selected = 'selected="selected"';
							}
							echo "<option $selected value='" . $row7['id'] . "'>" . $row7['fname'] . " " . $row7['mname'] . " " . $row7['lname'] . "</option>";
						}
						?>
                        </select>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Find By Invoice ID</label>

                            <div class="input-group  col-centered gst-input-group">
                                <input type="text" name="invoice" id="idFindByInvoice" class="form-control" value="<?php echo $invoiceId; ?>" style="padding-left: 10px">
                                <span class="input-group-btn">
                                    <button onclick="formmodified = 0; loadByInvoice() " type="button" class="btn btn-default">
                                        <div class="fa-search fas"></div>
                                    </button>
                                </span>
                            </div>


                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        
                    </div>
                    <div class="form-default form-static-label col-sm-3 text-right">
                        <h5 style="margin-bottom:10px;">Amount</h5>
                        <h3 class="text-danger" id='id_balance_due' data-balance="<?php echo $creditTotal; ?>"><?php echo GUtils::formatMoney($creditTotal); ?></h3>
                    </div>
                </div>
                <div class="card-block gst-block row">
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Credit Note Date</label>
                        <input type="date" required="required" name="credit_date" id='id_invoice_date' value='<?php echo GUtils::clientDate(Date('Y-m-d H:i:s'), 'Y-m-d'); ?>' class="form-control date-picker" required="">
                    </div>
                    <div class="form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Email Address</label>
                        <input value="<?php echo (isset($contactInfo['email']) ? $contactInfo['email'] : ''); ?>" type="text" name="email" id='id_email' class="form-control" required="">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Billing Address</label>
                        <textarea name="billing_address" id='id_billing_address' class="form-control" rows="3" required="required" ><?php echo $address ;?></textarea>
                    </div>
                    
                </div>
                
               <div class="card-block pt-0 pb-0 row">
                   
                    <div class="form-group form-default form-static-label col-sm-3">
                        <label class="float-label gst-label">Pay From</label>
                        <select name="credit_from" id="PayFrom" class="form-control" required="required">
                            <option value="">Select</option>
                            <option value="Checking Account"  >Checking Account</option>
                            <option value="Saving Account"  >Saving Account</option>
                        </select>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-2">
                        <label class="float-label gst-label">100% Tour Leader Discount?</label>
						<div class="can-toggle">
							<input name="TourLeaderDiscount" value="1" type="checkbox" id="LeaderDiscount" onclick="PaymentTypeLeader(this)" <?php echo (($paymentMethodEdit == '1') ? 'checked' : '');?>>
							<label for="LeaderDiscount">
								<div class="can-toggle__switch" data-checked="Yes" data-unchecked="No"></div>
							</label>
						</div>
                    </div>
                </div>

                <div class="card-block gst-block">
                        <table id="basic-btnzz" class="table table-hover table-striped table-bordered nowrap gst-table-margin" style="border: 0px;" data-page-length="20">
                            <thead>
                                <tr>
                                    <th style="white-space: nowrap; width: 10%;" >Invoice Num</th>
                                    <th style="width: 20%;">Invoice Amount</th>
                                    <th>Reason</th>
                                    <th style="width: 25%;" >Discount</th>
                                </tr>
                            </thead>
                            <tbody id='idInvoiceLineSection'>
                            <?php 
                            if(is_array($invoices) && count($invoices) > 0 ) {
                            foreach ($invoices as $row) { ?>
                                    <tr class="credit_note-line-tr">
                                        <td style="width: 10%;">
                                            <input type="hidden" name="cbNote[]" value="<?php 
                                            if( ! empty($__GET['noteid']) ) {
                                                $row['Note_ID'] = $__GET['noteid'] ;
                                            }
                                            echo intval($row['Note_ID']);
                                            
                                            ?>" />
                                            <?php echo $row["Customer_Invoice_Num"]; ?>
                                            <input type="hidden" name="cbInvoice[<?php echo  $row['Note_ID'];?>]" value="<?php echo $row["Customer_Invoice_Num"]; ?>" />
                                        </td>
                                        <td style="width: 20%;">
                                            <?php echo GUtils::formatMoney( $row["Invoice_Amount"] ); ?>
                                        </td>
                                        <td>
                                            <input class="form-control" type="text" 
                                                   name="credit_reason[<?php echo $row["Note_ID"]; ?>]"
												   id="CreditReason"
                                                   value="<?php echo ($noteId) ? $row["Credit_Reason"] : ''; ?>" />
                                        </td>
                                        <td style="width: 25%;" class="amount" data-price="<?php echo floatval($row["Credit_Amount"]);?>">
                                            <div class="dollar"><input class="amount-val" id="myText" onchange="updateTotal()" class="form-control" type="text" 
                                                   name="credit_amount[<?php echo $row["Note_ID"]; ?>]" 
                                                   value="<?php echo ($noteId) ? floatval($row["Credit_Amount"]) : '' ; ?>" style="height: 34px;" /></div>
                                        </td>
                                    </tr>
                            <?php }
                            }
                            else {
                                ?>
                                    <tr>
                                        <td class="text-center" colspan="4">No record found !</td>
                                    </tr>
                                    <?php
                            }
?>
                            </tbody>
                            
                        </table>
                </div>
                
                <div class="card-block gst-block row">
                    <div class="col-sm-12">
                        <br />
                        <input type="hidden" name="hidInvoiceSave" value="1" />
                        
                            <button type="submit" onclick="formmodified = 0;onSaveCreditNote();" value="<?php echo $SubmitAction; ?>" name="submit" 
                                class="btn waves-effect waves-light btn-success" style="margin-right:20px;" >
                            <i class="far fa-check-circle"></i>Save</button>
                        
                        <button type="button" onclick="window.location.href = '<?php echo $BACK_URL;?>'" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Cancel</button>
                    </div>
                </div>
                <div class="card-block gst-block row">
                    <div class="gst-spacer-10"></div>
                </div>

            </div>
</form>


<?php include 'inc/notificiations.php'; ?>

<script type="text/javascript">
    function onSaveCreditNote() {
        $('#contact1').submit(function (e) {


            $('#resultsmodal #results').html('Saving ...');
            $('#resultsmodal').modal('show');
            return true ;
        });
    }
    

    //Notification if the form isnt saved before leaving the page
    $(document).ready(function () {
        formmodified = 0;
        $('form *').change(function () {
            formmodified = 1;
        });
        window.onbeforeunload = confirmExit;
        function confirmExit() {
            if (formmodified == 1) {
                return "New information not saved. Do you wish to leave the page?";
            }
        }
    });


    function updateTotal() {
        var total = 0;

        $('.credit_note-line-tr .amount-val').each(function () {
            total += parseFloat($(this).val());
        });

        $('#id_balance_due').html( formatMoney(total) );
        
    }
    function loadCustomerDetails(val) {
        
        //reset first
        $('#id_email').val('');
        $('#id_billing_address').val('');
        $('#id_due_date').val('');
        $('#id_received_amount').html(formatMoney(0.0));
        $('#id_received_amount').data('balance', '0.0');

        formmodified= 0; 
        window.onbeforeunload = null;
        if( val ) {
            window.location.href = 'credit_notes_add.php?customerId=' + val ;
        }
        return true;
    }
    function loadByInvoice() {
        window.location.href = 'credit_notes_add.php?invid=' + $('#idFindByInvoice').val() ;
    }

//To activate the Tour Leader Discount auto selection data
function PaymentTypeLeader(LeaderDiscount) {
	if(document.getElementById("LeaderDiscount").checked == true) {
		document.getElementById("myText").value = <?php echo intval($row["Invoice_Amount"]); ?>;
		//document.getElementById('myText').setAttribute('readonly', true);
		//document.getElementById("myText").disabled = true;
		document.getElementById("CreditReason").value = "100% Discount - Tour Leader Complimntary Discount";
		document.getElementById('PayFrom').selectedIndex = 2;
		updateTotal();
	} else {
		document.getElementById("myText").value = 0;
		//document.getElementById('myText').setAttribute('readonly', false);
		//document.getElementById("myText").disabled = true;
		document.getElementById("CreditReason").value = "";
		document.getElementById('PayFrom').selectedIndex = 0;
		updateTotal();
	}
}
</script>

<?php include "footer.php"; ?>
