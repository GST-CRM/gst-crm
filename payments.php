<?php

include "session.php";
include_once "inc/helpers.php";

$paymentsEditAllowed = (new AclPermission())->isActionAllowed('Index', 'payments-add.php') ;

// { Acl Condition
$GroupID = AclPermission::userGroup() ;
$ACL_CONDITION = "" ;
if( AclPermission::roleFlags() & AclRole::$FLAG_ONLY_GROUP ) {
    $ACL_CONDITION = " AND ca.tourid IN($GroupID) " ;
}
// }

$printTitle = 'Receipt' ;
if( isset($__REQUEST['action']) && $__REQUEST['action'] == 'delete' ) {
    $editId = $__GET['id'] ;

    //Fidn transaction id
    $sqlts = "SELECT Transaction_Type_Transaction_ID FROM customer_payments WHERE Customer_Payment_ID ='$editId'" ;
    $transactionId = GDb::fetchScalar($sqlts) ;
    //delete transaction id
    $sqltd =  "DELETE FROM transactions WHERE Transaction_ID='$transactionId' LIMIT 1" ;
    GDb::execute($sqltd) ;
    //payment
    $sqll =  "DELETE FROM customer_payments WHERE Customer_Payment_ID = '$editId'" ;
    GDb::execute($sqll) ;
    //Todo delete other table too
    GUtils::redirect('payments.php') ;
}
else if( isset($__REQUEST['action']) && $__REQUEST['action'] == 'void' ) {
    $editId = $__GET['id'] ;
    //Fidn transaction id
    $sqlts = "SELECT Transaction_Type_Transaction_ID FROM customer_payments WHERE Customer_Payment_ID ='$editId' AND Status=1 " ;
    $transactionId = GDb::fetchScalar($sqlts) ;
    //void payment
    $sql =  "UPDATE customer_payments SET Status=0 WHERE Customer_Payment_ID = '$editId' LIMIT 1" ;
    GDb::execute($sql) ;
    //delete transaction id
    $sqltd =  "UPDATE transactions SET Voided_Flag=1 WHERE Transaction_ID='$transactionId' LIMIT 1" ;
    GDb::execute($sqltd) ;

    GUtils::redirect('payments.php') ;
}
else if( isset($__REQUEST['action']) && $__REQUEST['action'] == 'send' ) {
    $FileTitlePrefix = 'receipt' ;
    include 'inc/sales-invoice-mail.php' ;
    GUtils::redirect('payments.php') ;
}
else if( isset($__REQUEST['action']) && $__REQUEST['action'] == 'reminder' ) {
    $FileTitlePrefix = 'receipt' ;
    include 'inc/sales-invoice-reminder.php' ;
    GUtils::redirect('payments.php') ;
}
else if( isset($__REQUEST['action']) && $__REQUEST['action'] == 'ajax-reminder' ) {
    $FileTitlePrefix = 'receipt' ;
    include 'inc/sales-invoice-reminder.php' ;
    $data = ['status' => 'OK'] ;
    GUtils::jsonResponse($data) ;
}

else if( isset($__REQUEST['batch-invaction']) ) {
    if( $__REQUEST['batch-invaction'] == 'P' ) {
        include 'inc/sales-invoice-pdf.php';
    }
    else if( $__REQUEST['batch-invaction'] == 'S' ) {
        $FileTitlePrefix = 'receipt' ;
        include 'inc/sales-invoice-mail.php';
        echo '<script type="text/javascript">window.close();</script>' ;
        die;
    }
}

$PageTitle = "Customer Payments ";
include "header.php";

?>

    <form action="payments.php" method="POST" name="customerPaymentsListForm" >
        <!-- [ page content ] start -->
        <div class="row">
            <div class="col-sm-12">
                <!-- HTML5 Export Buttons table start -->
                <div class="card">
					<div class="row">
						<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
							<div class="input-group input-group-sm mb-0">
								<span class="input-group-prepend mt-0">
									<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
								</span>
								<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
							</div>
						</div>
						<div class="col-md-4" style="margin-top:15px;"></div>
						<div class="col-md-2" style="margin-top:15px;">
							<span style="margin-left: 30px;float:left; ">
								<select name="batch-invaction" onchange="return batchSubmit(this);" class="form-control form-control-default fill gst-invoice-batchaction"
										disabled="disabled">
									<option value="">Batch Action</option>
									<option value="S">Send Receipts</option>
									<option value="P">Print Receipts</option>
								</select>
							</span>
						</div>
						<div class="col-md-3" style="margin-top:15px;padding-right:45px;">
                            <?php if( AclPermission::actionAllowed('Add') ) { ?>
                            <a href="payments-add.php" class="btn waves-effect waves-light btn-success"
                               style="float:right; padding: 3px 13px;"><i
                                        class="far fa-check-circle"></i>New Customer Payment</a>
                            <?php } ?>
						</div>
					</div>
                    <div class="row">
                        <div class="col-md-3">
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="dt-responsive table-responsive">
                            <table id="idPaymentReceivedTable" class="table gst-table table-hover table-striped table-bordered nowrap"
                                   data-page-length="10">
                                <thead>
                                <tr>
                                    <th class="gst-no-sort gst-has-events">
                                        <input type="checkbox" class="gst-invoice-cb-parent" />
                                    </th>
                                    <th width="1%">Payment#</th>
                                    <th>Customer Account</th>
                                    <th>Group Reference #</th>
                                    <th>Payment Date</th>
                                    <th>Amount</th>
                                    <th width="1%" class="gst-no-sort">Actions</th>
                                </tr>
                                </thead>
                                <?php /*
                                <tbody>
                                <?php
                                foreach ($records as $row) {
                                    
                                    $name = $row['fname'] . ' ' . $row['mname'] . ' ' . $row['lname'];
                                    $nameAppend = ((strlen(trim($name)) > 0) ? $name : '');
                                    
                                    //name & email
                                    $name_email = '<b>' . $nameAppend  . (isset($row['email']) ? ' ('.$row['email'] .')' : '') . '</b>' ;

                                    ?>
                                    <tr>
                                        <td><input name="cbInvoice[]" value="<?php echo $row["Customer_Invoice_Num"];?>" type="checkbox" class="gst-invoice-cb"/></td>
                                        <td>
                                            <a style="color: #0000EE;" href="payments-add.php?payment=<?php echo $row["Customer_Payment_ID"]; ?>">
                                                <?php echo $row["Customer_Payment_ID"]; ?>
                                            </a>
                                        </td>
                                        <td>
                                            <?php if ($row["contactid"]) { ?>
                                            <a style="color: #0000EE;" href='<?php echo 'contacts-edit.php?id=' . $row["contactid"] . '&action=edit&usertype=sales'; ?>'>
                                            <?php
                                            }
                                            //print text
                                            echo $nameAppend;

                                            if ($row["contactid"]) {
                                            ?>
                                            </a>
                                            <?php } ?>
                                        </td>
                                        <td><?php echo GUtils::clientDate($row["Customer_Payment_Date"]); ?></td>
                                        <td><?php echo GUtils::formatMoney($row["Customer_Payment_Amount"]); ?></td>
                                        <td >
                                            <div style="width: 150px;" class="btn-group gst-btns ">
                                                <?php if( $row['Status'] ) { ?>                                               
                                                <button onclick="window.location.href='payments-add.php?payment=<?php echo $row["Customer_Payment_ID"]; ?>';"
                                                        type="button" class="btn btn-info">Edit/View
                                                </button>
                                                <button type="button" class="btn btn-info dropdown-toggle"
                                                        data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" style="white-space: nowrap">
                                                    <!--<li><a href="payments-add.php?id=<?php echo $row["Customer_Invoice_Num"]; ?>">Edit/View</a></li>-->
                                                    <li><a target="_blank" href="sales-invoice-print.php?print=payment&invid=<?php echo $row["Customer_Invoice_Num"]; ?>">Print</a></li>
                                                    <li><a href="javascript:void(0);" onclick = 'showConfirm( "payments.php?action=send&invid=<?php echo $row["Customer_Invoice_Num"]; ?>", "Send", "Are you sure you want to sent this email to <?php echo $name_email;?> ?", "Please Confirm", "modal-md")' >Send</a></li>
                                                    <li><a href="javascript:void(0);" onclick = 'deleteConfirm( "payments.php?action=delete&id=<?php echo $row["Customer_Payment_ID"]; ?>", "Delete")' >Delete</a></li>
                                                    <li><a href="javascript:void(0);" onclick = 'deleteConfirm( "payments.php?action=void&id=<?php echo $row["Customer_Payment_ID"]; ?>", "Void")' >Void</a></li>
                                                </ul>
                                                <?php } else {
                                                    ?>
                                                    <button onclick = 'deleteConfirm( "payments.php?action=delete&id=<?php echo $row["Customer_Payment_ID"]; ?>")' type="button" class="btn btn-danger">
                                                        Delete
                                                    </button>
                                                    <?php
                                                } ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                                 * 
                                 */ ?>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- HTML5 Export Buttons end -->
            </div>
        </div>
    </form>

<script type="text/javascript">
    
    function sendReminderHandler(obj) {
        
        $("#showSendReminder").modal("show");
        
        $('.gst-reminder-id').val( $(obj).data('id') ) ;
        $('.gst-reminder-class').html( $(obj).data('name') ) ;
        $('.gst-reminder-email').val( $(obj).data('email') ) ;
        $('.gst-reminder-subject').val( '' ) ;
        $('.gst-reminder-message').val( '' ) ;
    }
    
    function batchSubmit(obj) {
        var count = $('.gst-invoice-cb:checked').length ;
        if( count > 0 ) {
            
            obj.form.target = '_blank' ;
            if( obj.value == 'S' ) {
                msg = "Are you sure you want to sent " + count + " receipt(s) ?" ;
            }
            else if( obj.value == 'P' ) {
                msg = "Are you sure you want to print " + count + " receipt(s) ?" ;
            }
            else {
                return ;
            }
            showConfirm( "javascript:customerPaymentsListForm.submit();", "Yes", msg, "Please Confirm", "modal-md") ;
//            obj.form.submit();
        }
    }

    document.addEventListener('DOMContentLoaded', function () {
    $('#idPaymentReceivedTable').DataTable({
		 "bProcessing": true,
         "serverSide": true,
		 dom: 'Bfrtip',
		 sDom: 'lrtip', //To Hide the search box
		 "bLengthChange": false, //To hide the Show X entries dropdown
		 "columnDefs": [
                     {
                        "targets": 0,
                        "orderable": false
                        } 
                        <?php if( ! $paymentsEditAllowed ) { 
                            echo ',{
                                    "targets": 6,
                                    "visible": false
                                    }' ;
                        } ?>

		   ],
		 buttons: [ ],
         ajax : {
            url :"inc/datatable-payments.php", // json datasource
            complete : function(){
                delayedBindingCheckbox() ;

              }
          }
        }); 
    } ) ;
	
	
//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#idPaymentReceivedTable').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
});
</script>

<?php
global $GLOBAL_SCRIPT ;
$GLOBAL_SCRIPT .= "bindCheckAll('.gst-invoice-cb-parent', '.gst-invoice-cb');" ;
?>

<?php include 'inc/notificiations.php'; ?>
    <!-- [ page content ] end -->
<?php include "footer.php"; ?>