<?php
include "session.php";
$PageTitle = "GST Calendar";
include "header.php"; 

$_REQUEST['getEvent']   = "yes";
$calendarPage           = 1;
$allEvents              = include_once __DIR__ . '/inc/calendar-functions.php' ;

$eventArray             = array();
if (!empty($allEvents))
{ 
    foreach($allEvents as $events) 
    { 
        $eventArray[$events['start']][] = $events;
    }
}

//$sql          = "SELECT * FROM Calendar_Events";
//$allEvents    = $db->query($sql);


// $opts = array('http' =>
//     array(
//         'method'  => 'POST',
//         'header'  => 'Content-Type: application/x-www-form-urlencoded',
//         'content' => array("getEvent"=>"yes")
//     )
// );

// $context  = stream_context_create($opts);

// $allEvents      = file_get_contents(__DIR__."/inc/calendar-functions.php",false.$context);

?>
    <!-- Calender css -->
    <link rel="stylesheet" type="text/css" href="files/bower_components/fullcalendar/css/fullcalendar.css">
    <link rel="stylesheet" type="text/css" href="files/bower_components/fullcalendar/css/fullcalendar.print.css" media='print'>
    <link rel="stylesheet" type="text/css" href="files/assets/pages/datepicker/jquery.datetimepicker.css">
    <script src="files/assets/pages/datepicker/jquery.datetimepicker.full.js"></script>
        <div class="pcoded-inner-content full-calender">
            <div class="main-body">
                <div class="page-wrapper">

                    <div class="page-body">
                        <div class="card">
                            <div class="card-header">
                                <h5>Full Calendar</h5>
                            </div>
                            <a href="#" onclick='$("#eventModal").modal("show");' style="padding: 20px;margin: 10px 10px;background: #4ec3f7;width: 200px;text-align: center;color: white;">test adding event</a><br><br>
                            <iframe src="https://calendar.google.com/calendar/b/5/embed?height=800&amp;wkst=1&amp;bgcolor=%23ffffff&amp;ctz=America%2FChicago&amp;src=ZGF5YXQubmV0XzJodmo4b3EwMDBuM2RhanRmODhnZjA2OTI4QGdyb3VwLmNhbGVuZGFyLmdvb2dsZS5jb20&amp;src=ZW4udXNhI2hvbGlkYXlAZ3JvdXAudi5jYWxlbmRhci5nb29nbGUuY29t&amp;color=%234ec3f7&amp;color=%234ec3f7&amp;showTitle=0&amp;showNav=1&amp;showPrint=0&amp;showCalendars=0" style="border-width:0" width="800" height="800" frameborder="0" scrolling="no"></iframe>
                            <?php /*
                            <div class="card-block">
                                <div class="row">
                                    <div class="col-xl-2 col-md-12">
                                        <div id="external-events">
                                            <h6 class="m-b-30 m-t-20">Events</h6>
                                            <?php
                                            if (!empty($eventArray))
                                            { 
                                                $n = 1;
                                                foreach($eventArray as $id => $event) 
                                                { 
                                                    ?>
                                                    <p>
                                                      <button class="btn btn-dark" type="button" data-toggle="collapse" data-target="#event<?php echo $n?>" aria-expanded="false" aria-controls="event<?php echo $n?>" style="padding:8px 16px;">
                                                        <?php echo date("m/d/Y",strtotime($id));?>&nbsp;&nbsp;<i class="fa fa-angle-double-down" aria-hidden="true" style="font-size:10px"></i>

                                                      </button>
                                                    </p>
                                                    <div class="collapse" id="event<?php echo $n?>">
                                                    <?php
                                                    foreach ($event as $eve) 
                                                    {
                                                        $eventDesc  = $eve['title'];
                                                        $eventColor = $eve['backgroundColor']; 
                                                        
                                                        ?>
                                                        <div class='fc-event' data-event='<?php echo json_encode($eve)?>' style="background-color:<?php echo $eventColor?>"><?php echo $eventDesc?></div>
                                                    <?php
                                                    }
                                                    //print_r($eve);
                                                    // $eventDesc  = $eve['title'];
                                                    // $eventColor = $eve['backgroundColor'];
                                                    ?>
                                                    </div>
                                                    <!-- <div class="fc-event ui-draggable ui-draggable-handle"> 
                                                    <div class="fc-day-grid-event fc-h-event fc-event fc-start fc-end fc-draggable fc-resizable"><div class="fc-content">
                                                        <?php echo $row['Event_Description']?>
                                                    </div>
                                                    </div> -->
                                                    <!-- <div class='fc-event' data-event='<?php echo json_encode($eve)?>' style="background-color:<?php echo $eventColor?>"><?php echo $eventDesc?></div> -->
                                                    <?php 
                                                    $n++;
                                                }
                                            }
                                            else
                                            {
                                                echo "No events";
                                            }
                                            ?>




                                            <!-- <div class="checkbox-fade fade-in-primary m-t-10">
                                                <label>
                                                    <input type="checkbox" value="">
                                                    <span class="cr">
                                                        <i class="cr-icon fas fa-check txt-primary"></i>
                                                    </span>
                                                    <span>Remove After Drop</span>
                                                </label>
                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="col-xl-10 col-md-12">
                                        <div id='calendar'></div>
                                    </div>
                                </div>
                            </div>
                            */ ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page-error">
                <div class="card text-center">
                    <div class="card-block">
                        <div class="m-t-10">
                            <i class="icofont icofont-warning text-white bg-c-yellow"></i>
                            <h4 class="f-w-600 m-t-25">Not supported</h4>
                            <p class="text-muted m-b-0">Full Calendar not supported in this device</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <!-- calender js -->
    <script type="text/javascript" src="files/bower_components/moment/js/moment.min.js"></script>
    <script type="text/javascript" src="files/bower_components/fullcalendar/js/fullcalendar.min.js"></script>
    <!-- Custom js -->
    <!--<script type="text/javascript" src="files/assets/pages/full-calender/calendar.js"></script>-->


<?php include "footer.php"; ?>



<div class="modal" id="eventModal" tabindex="-1" role="dialog" style="top: 50px; padding-right: 15px; display: none;" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="box-shadow:none!important;">
                <span class="col-md-12 text-center"><h3>Events</h3></span>
                <button type="button" class="close" data-dismiss="modal" style="font-size:25px; padding:3px">&times;</button>
            </div>
            <div class="modal-body" style="text-align:center;">
                <div id="results">
                    <form id="calendarForm" action="" method="post">
                        <?php /*
                        <input type="type" name="Event_Date" id="event_date"> 
                          */ ?>
                        <div class="row text-left">
                            <div class="col-md-5">Event Date Start</div>
                            <div class="col-md-7">
                                <input type="text" name="Event_Date" id="event_date">
                            </div>
                        </div>
                        <br>
                        <div class="row text-left">
                            <div class="col-md-5">Event Date End</div>
                            <div class="col-md-7">
                                <input type="text" name="Event_Date_end" id="event_date_end">
                            </div>
                        </div>
                        <br>
                        <input type="hidden" name="addEvent" value="yes"> 
                        <input type="hidden" name="Event_ID" id="event_id" value=""> <br>
                        <?php /*
                        <div class="row text-left">
                            <div class="col-md-5">Event Type</div>
                            <div class="col-md-7">
                                <select name="Event_Type" id="event_type" class="form-control">
                                    <option value="Important">Important</option>
                                    <option value="Task">Task</option>
                                    <option value="Reminder">Reminder</option>
                                </select>
                            </div>
                        </div>
                        <br>*/?>
                        <div class="row text-left">
                            <div class="col-md-5">Event Description</div>
                            <div class="col-md-7">
                                <input type="text" name="Event_Description" id="event_description" class="form-control">
                                <span id="eventDescErr" style="color:red"></span>
                            </div>
                        </div>
                        <br>
                        <div class="row text-left">
                            <div class="col-md-5">Event Notes</div>
                            <div class="col-md-7">
                                <input type="text" name="Event_Notes" id="event_notes" class="form-control">
                            </div>
                        </div>
                        <br>
                        <div class="row text-left">
                            <div class="col-md-5">Recurring</div>
                            <div class="col-md-1">
                                <input type="checkbox" name="Recurring" id="enable_period" value="1" >
                            </div>

                            <div class="col-md-2 pull-right">Period</div>
                            <div class="col-md-4">
                                <select name="Period" id="period" class="form-control" disabled="">
                                    <option value="every_day">Every Day</option>
                                    <option value="every_week">Every Week</option>
                                    <option value="every_month">Every Month</option>
                                </select>
                            </div>
                        </div>
                </div>      
            </div>
            <div class="modal-footer2">
                <button type="button" class="btn btn-success waves-effect waves-light" onclick="add_event()">Save</button>
                &nbsp;
                <button type="button" name="delEvent" id="deleteEventBtn" class="btn btn-danger waves-effect waves-light" onclick="delete_event()" style="display:none">Delete</button>
            </div>
        </div>
    </div>
</div>



<script type="text/javascript">

$(document).ready(function(){
    $('#event_date_end').datetimepicker();
    $('#event_date').datetimepicker();
    
    $("#enable_period").change(function(){
        if (this.checked == true)
        {
            $("#period").removeAttr("disabled");
        }
        else
        {
            $("#period").attr("disabled",true);
        }
    });


    $('#external-events .fc-event').each(function() {

        // store data so the calendar knows to render an event upon drop
        $(this).data('event', {
            title: $.trim($(this).text()), // use the element's text as the event title
            stick: true // maintain when user navigates (see docs on the renderEvent method)
        });

        // make the event draggable using jQuery UI
        $(this).draggable({
            zIndex: 999,
            revert: true, // will cause the event to go back to its
            revertDuration: 0 //  original position after the drag
        });

    });

    var $eventDates = $('#external-events');
    $('#external-events').on('show.bs.collapse','.collapse', function() {
        $('#external-events').find('.collapse.show').collapse('hide');
    });



    var calendar = $('#calendar').fullCalendar({
        editable: true,
        displayEventTime: false,
        selectable: true,
        selectHelper: true,
        //droppable:true,
        //events: ['inc/calendar-functions.php?getEvent=yes'],
        eventSources: ['inc/calendar-functions.php?getEvent=yes'],

        dayClick: function(date, jsEvent, view) {

            $("#event_date").val(date.format());
            $("#calendarForm").trigger("reset");
            $("#enable_period").removeAttr("checked");
            $("#eventModal").modal("show");
            $("#period").attr("disabled",true);
        },
        eventDrop: function (event, delta) {
            var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD");
            //var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
            $.ajax({
                url : 'inc/calendar-functions.php',
                data: {'addEvent':'yes','Event_Description':event.title,'Event_Date':start,
                        'Event_ID':event.id,'Event_Type':event.type,
                        'Event_Notes':event.notes,'Recurring':event.enable_period,
                        'Period':event.period},
                type: "POST",
                success: function (response) {
                    //displayMessage("Updated Successfully");
                }
            });
        }, 
        eventClick: function (event) {
            var start       = new Date(event.start);
            var dd          = String(start.getDate()).padStart(2, '0'); 
            var mm          = String(start.getMonth() + 1).padStart(2, '0'); 
            var yyyy        = start.getFullYear(); 
            var event_date  = yyyy+"-"+mm+"-"+dd;

            $("#event_id").val(event.id);
            $("#event_date").val(event_date);
            $("#event_type").val(event.type);
            $("#event_description").val(event.title);
            $("#event_notes").val(event.notes);
            $("#eventModal").modal("show");
            $("#deleteEventBtn").show();

            if (event.enable_period == 1)
            {
                $("#enable_period").attr("checked",true)
                $("#period").removeAttr("disabled");
                $("#period").val(event.period);
            }
            else
            {
                $("#period").attr("disabled",true);
            }
        },

    });


});
 
function add_event()
{
    var date_start = $('#event_date').val();
    var date_end = $('#event_date_end').val();
    var description = $('#event_description').val();
    var note = $('#event_notes').val();
    var recurring = $("#enable_period").is(':checked');
    var periods = $('#period').val();
    
    $.get('/calendar/index.php?add_event=asdf1234&description=' + description + '&summary=' + note + '&start_string=' + date_start + '&end_string=' + date_end + '&timezone=America/Chicago', function(data) {
      alert('Item added');  
    });    
    
    //var formDetails = $("#calendarForm");
    //var eventDesc   = $("#event_description").val();
    //if (eventDesc == "")
    //{
    //    $("#eventDescErr").html("Please enter event description");
   // }
    //else
    //{
    //    $.ajax({
    //        type    : "POST",
    //        url     : 'inc/calendar-functions.php',
    //        data    : formDetails.serialize(),
    //        success : function (data) {  
    //            location.reload();
    //            //$('#results').html(data);
    //            //formmodified = 0;
    //        },
    //        error: function(jqXHR, text, error){
    //            console.log(error);           
    //        }
    //    });
    //}
}
function delete_event()
{
    $.ajax({
        type    : "POST",
        url     : 'inc/calendar-functions.php',
        data    : {"delEvent":"yes", "Event_ID":$("#event_id").val()},
        success : function (data) {  
            location.reload();
        },
        error: function(jqXHR, text, error){
            console.log(error);           
        }
    });
}




</script>
<style>
    iframe {
        width: 98% !important;
        display: block !important;
        margin: auto !important;
        margin-bottom: 40px !important;
    }
</style>