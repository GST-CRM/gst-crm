<?php include "session.php";
$PageTitle = "Settings";
include "header.php";
?>
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->
<head>
    <link href="files/DateTimeDropdown/bootstrap-datatimepicker-main.css" rel="stylesheet" media="screen">
    <link href="files/DateTimeDropdown/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
	<script type="text/javascript" src="files/DateTimeDropdown/bootstrap-datetimepicker.js" charset="UTF-8"></script>

</head>
	


<div id="results"></div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="card">
            <div class="card-header">
                <h5>Settings</h5>
            </div>
            <div class="card-block  row">
				<form id="main" name="main" method="post" action="inc/contact-functions.php" class="row">
                    <div class="col-sm-3">
						
							<div class="input-group date form_datetime" data-date="2019-12-25T05:25:07Z" data-date-format="M dd @ HH:iip" data-link-field="dtp_input1">
								<input class="form-control fill" size="16" type="text" value="">
								<span class="input-group-text input-group-addon" style="border-radius: 0px 5px 5px 0px;border-left:0px;"><span class="glyphicon glyphicon-th"></span></span>
							</div>
							<input type="hidden" id="dtp_input1" value="" />


<script type="text/javascript">
    $('.form_datetime').datetimepicker({
        weekStart: 1, todayBtn:  0, autoclose: 1, todayHighlight: true, startView: 3, forceParse: 0, minuteStep: 1, showMeridian: 0
    });
</script>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <input type="text" id="fname" name="FirstName" class="form-control" required="">
                        <span class="form-bar"></span>
                        <label class="float-label">First Name</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <input type="text" id="mname" name="MiddleName" class="form-control" required="">
                        <span class="form-bar"></span>
                        <label class="float-label">Middle Name</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <input type="text" id="lname" name="LastName" class="form-control" required="">
                        <span class="form-bar"></span>
                        <label class="float-label">Last Name</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <input type="text" name="email" class="form-control" required="">
                        <span class="form-bar"></span>
                        <label class="float-label">Email</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <input type="date" name="birthday" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">Birthday</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <input id="tel" type="tel" name="homephone" placeholder="(XXX) XXX-XXXX" pattern="\(\d{3}\) \d{3}\-\d{4}" class="masked form-control" required="">
                        <span class="form-bar"></span>
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Home Phone #</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
						<input id="mobile" type="tel" name="mobile" placeholder="(XXX) XXX-XXXX" pattern="\(\d{3}\) \d{3}\-\d{4}" class="masked form-control" required="">
                        <span class="form-bar"></span>
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Mobile Phone #</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <input type="text" name="company" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">Company Name</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <input type="text" name="address1" class="form-control" required="">
                        <span class="form-bar"></span>
                        <label class="float-label">Address Line 1</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <input type="text" name="address2" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">Address Line 2</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <input type="text" name="zipcode" class="form-control" required="">
                        <span class="form-bar"></span>
                        <label class="float-label">ZIP Code</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <input type="text" name="city" class="form-control" required="">
                        <span class="form-bar"></span>
                        <label class="float-label">City</label>
                    </div>
                    <div class="col-sm-3">
                        <select name="state" class="form-control form-control-default">
							<option value="0" disabled selected> </option>
							<?php $con2 = new mysqli($servername, $username, $password, $dbname);
							$result2 = mysqli_query($con2,"SELECT name, abbrev FROM states");
							while($row2 = mysqli_fetch_array($result2))
							{
								echo "<option value='".$row2['abbrev']."'>".$row2['name']."</option>";
							} ?>
                        </select>
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">State</label>
                    </div>
                    <div class="col-sm-3">
                        <select name="country" class="form-control form-control-default">
                            <option value="0" disabled selected> </option>
							<?php $con3 = new mysqli($servername, $username, $password, $dbname);
							$result3 = mysqli_query($con3,"SELECT name, abbrev FROM countries");
							while($row3 = mysqli_fetch_array($result3))
							{
								echo "<option value='".$row3['abbrev']."'>".$row3['name']."</option>";
							} ?>
                        </select>
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Country</label>
                    </div>
                    <div class="col-sm-3">
						<div class="form-group form-default form-static-label">
							<input type="text" name="emergencyname" class="form-control">
							<span class="form-bar"></span>
							<label class="float-label">Emergency Contact Name</label>
						</div>
                    </div>
                    <div class="col-sm-3">
						<div class="form-group form-default form-static-label">
							<input type="text" name="emergencyrelation" class="form-control">
							<span class="form-bar"></span>
							<label class="float-label">Emergency Contact Relation</label>
						</div>
                    </div>
                    <div class="col-sm-3">
						<div class="form-group form-default form-static-label">
							<input type="text" name="emergencyphone" class="form-control">
							<span class="form-bar"></span>
							<label class="float-label">Emergency Contact #</label>
						</div>
                    </div>
                    <div class="col-sm-3">
                        <select name="martialstatus" class="form-control form-control-default">
                            <option value="0" disabled selected> </option>
                            <option value="Married">Married</option>
                            <option value="Single">Single</option>
                        </select>
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Martial Status</label>
                    </div>
                    <div class="col-sm-3">
                        <select name="gender" class="form-control form-control-default">
                            <option value="0" disabled selected> </option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
						<label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Gender</label>
                    </div>
                    <div class="col-sm-3">
                        <select name="language" class="form-control form-control-default">
                            <option value="0" disabled selected> </option>
                            <option value="English">English</option>
                            <option value="French">French</option>
                            <option value="German">German</option>
                            <option value="Arabic">Arabic</option>
                             <option value="Italian">Italian</option>
                            <option value="Spanish">Spanish</option>
                        </select>
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Language</label>
                    </div>
                    <div class="col-sm-3">
                        <select name="affiliation" class="form-control form-control-default">
                            <option value="0" disabled selected> </option>
                            <option value="Flyers">Flyers</option>
                            <option value="Emails">Emails</option>
                            <option value="Churches">Churches</option>
                            <option value="Conventions">Conventions</option>
                        </select>
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Affiliation Type</label>
                    </div>
                    <div class="col-sm-3">
                        <select name="preferredmethod" class="form-control form-control-default">
                            <option value="0" disabled selected> </option>
                            <option value="Email">Email</option>
                            <option value="Phone">Phone</option>
                            <option value="Mobile">Mobile</option>
                        </select>
                        <label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Preferred Method</label>
                    </div>
                    <div class="col-sm-12"><br /></div>
					<div class="form-group form-default form-static-label col-md-12">
                        <textarea name="notes" class="form-control"><?php echo $data["notes"]; ?></textarea>
                        <span class="form-bar"></span>
                        <label class="float-label">Notes for this Contact</label>
                    </div>
                    <div class="col-sm-12"><br /><br />
						<button type="submit" name="addcontact" value="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;"><i class="far fa-check-circle"></i>Add</button>
                        <button type="reset" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Clear</button>
					</div>
				</form>
			</div>
        </div>
    </div>
    <div class="col-md-1"></div>
</div>

<?php include "footer.php"; ?>