<?php

$query="SELECT * FROM customer_account cus JOIN contacts co ON co.id=cus.contactid WHERE cus.tourid=$groupid ORDER BY cus.status DESC, TRIM(co.lname) ASC";
    $result = $db->query($query); 
    $header1 = array($groupName." - #".$groupid);
    //$header2 = array('First Name','Middle Name','Last Name','Date of Birth','Citizenship','Passport #','Passport Expiry Date','Known Traveler #','Frequent Flyer');
    $header2 = array(
      'Last Name'=>'string',
      'First Name'=>'string',
      'Middle Name'=>'string',
      'Date of Birth'=>'date',
      'Gender'=>'string',
      'Citizenship'=>'string',
      'Passport #'=>'string',
      'Passport Expiry Date'=>'date',
      'Purchased separate ticket'=>'string',
      'Known Traveller #'=>'string',
      'Frequent Flyer'=>'string',
      'Need Extension?'=>'string',
      'Need Transfer?'=>'string',
      'Airline Special Requests'=>'string',
      'Meals Requests'=>'string',
      'Allergies Requests'=>'string',
      'Notes'=>'string',
    );
	$styles4 = array('font-style'=>'bold', 'fill'=>'#eee', 'halign'=>'center','border'=>'left,right,top,bottom');
    $stylesCancel = array('font-style'=>'bold', 'fill'=>'#eee', 'halign'=>'center','border'=>'left,right,top,bottom');

	$writer->setTitle($filetitle);
	$writer->setSubject($filesubject);
	$writer->setAuthor($fileauthor);
	$writer->setCompany($filecompany);
	$writer->setDescription($filedesc);
	
	$writer->writeSheetHeader('Manifest', $header2, $styles4);
	$writer->writeSheetRow('Manifest', $header1, $styles4);
	$i = 1;
    $iRoomingHeadingLines = 1 ;
    $array = array();
    $canceled = false;

    while ($row=$result->fetch_assoc())
    {
		if( ! $canceled && $row['status'] == 0 ) {
            $writer->writeSheetRow('Manifest', ['Canceled Members'], $stylesCancel);
            $canceled = true ;
            $writer->markMergedCell('Manifest', $i+$iRoomingHeadingLines, 0, $i+$iRoomingHeadingLines, 14) ;
        }

        $array[1] = $row["lname"];
        $array[2] = $row["fname"];
        $array[3] = $row["mname"];
        $array[4] = $row["birthday"];
        $array[5] = $row["gender"];
        $array[6] = $row["citizenship"];
        $array[7] = $row["passport"];
        $array[8] = $row["expirydate"];
		if($row["hisownticket"] == 1) { $PurchasedHisOwnTicket = "Yes"; } else { $PurchasedHisOwnTicket = "No"; };
        $array[9] = $PurchasedHisOwnTicket;
        $array[10] = $row["travelerid"];
        $array[11] = $row["flyer"];
        if($row["Extension"]==1) { $array[12] = "Yes"; } else { $array[12] = "No"; }
        if($row["Transfer"]==1) { $array[13] = "Yes"; } else { $array[13] = "No"; }
		$array[14] = $row["specialtext"];
		$array[15] = $row["Meals_Request"];
		$array[16] = $row["Allergies_Request"];
        $array[17] = $row["notes"];

			
        $writer->writeSheetRow('Manifest', $array);
		$i++;
    };
	$writer->markMergedCell('Manifest', $start_row=1, $start_col=0, $end_row=1, $end_col=14);
