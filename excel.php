<?php
include "session.php";
include_once __DIR__ . '/models/customer_payment.php'; ;

include_once("files/excel/xlsxwriter.class.php");

	$filetitle = "Rooming List Report";
	if(isset($_GET["name"])) { $groupname = mysqli_real_escape_string($db, $_GET["name"]);} else { $groupname = ''; }
	if(isset($_GET["groupid"])) { $groupid = mysqli_real_escape_string($db, $_GET["groupid"]);} else { $groupid = ''; }
	if(isset($_GET["type"])) { $reporttype = mysqli_real_escape_string($db, $_GET["type"]);} else { $reporttype = ''; }
	$filesubject ="Rooming List for the Group #$groupid";
	$fileauthor ="Good Shepherd Travel";
	$filecompany ="Element Media";
	$filedesc ="Rooming List for the Group #$groupid";

if( $reporttype == "rooming" ) {
    $filename = "Rooming-List-Report-Group-$groupid.xlsx";
    header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');   
    $writer = new XLSXWriter();

    include __DIR__ . '/excel-rooming-sheet.php' ;
    
    $writer->writeToStdOut();
    exit(0);
} elseif( $reporttype == "sales" ) {
	$filetitle = "Sales Orders Report";
    $filename = "Sales-Orders-List.xlsx";
	$filedesc ="Detailed full list of the sales orders";
	header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
	header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	header('Content-Transfer-Encoding: binary');
	header('Cache-Control: must-revalidate');
	header('Pragma: public');   
	$writer = new XLSXWriter();
    include __DIR__ . '/excel-sales-orders.php' ;
    $writer->writeToStdOut();
    exit(0);
} elseif($reporttype == "manifest") {
    $filename = "Manifest-Report-Group$groupid.xlsx";
    header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');   
    
    $writer = new XLSXWriter();
    
    include __DIR__ . '/excel-manifest.php' ;

    $writer->writeToStdOut();
    exit(0);
} 
elseif($reporttype == "tour_leader"){
    $filename = "Tour-Leader-Report-$groupid.xlsx";
    header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
    header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
    header('Content-Transfer-Encoding: binary');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    
    
    $tourid = $groupid;
    //Groupname And Leader name
    //SELECT groups.tourname,contacts.fname,contacts.lname FROM groups JOIN contacts ON groups.groupleader=contacts.id AND groups.tourid=20050
    $sql = "SELECT groups.tourid,groups.tourname,groups.land_productid,CONCAT(contacts.fname,' ', contacts.lname) AS leader_name,groups.airline_productid,SUM(products.price) AS trip_package
    FROM groups 
    LEFT JOIN products ON products.id = groups.land_productid OR products.id = groups.airline_productid 
    LEFT JOIN contacts ON contacts.id = groups.groupleader
    WHERE groups.tourid = $tourid";
    $queryGroup = mysqli_query($db, $sql) or die("error to fetch group data");
    $groupData = mysqli_fetch_assoc($queryGroup);


    $sql = "SELECT customer_account.status AS contact_status,
                contacts.id,
                contacts.title,
                contacts.fname,
                contacts.mname,
                contacts.lname,
                contacts.badgename,
                contacts.passport,
                contacts.expirydate,
                customer_account.createtime,
                customer_account.specialtext,
                customer_account.special_land_text,
                contacts.mobile,
                contacts.email ,
                contacts.address1,
                contacts.address2,
                contacts.city,
                contacts.state,
                contacts.zipcode
         FROM contacts
         LEFT JOIN customer_account ON contacts.id=customer_account.contactid
         WHERE customer_account.tourid = $tourid
         ORDER BY customer_account.status DESC ";


    $queryContacts = mysqli_query($db, $sql) or die("error to fetch contacts data");


    
    $trip_package = "TRIP PACKAGE: $".number_format($groupData['trip_package'],2);


    //$rows = $result->fetch_assoc();
    //$header1 = array($groupname." - #".$groupid);
    $header_to_date1 = array($groupname);
    $header_to_date2 = array("Tour Leader:".$groupData['leader_name']);
    $header_to_date3 = array('#','Full Name','Join Date','Mobile Phone','Email', 'Address');

    $header_paid_to_date1 = array($groupname,"","","","",$trip_package);
    $header_paid_to_date2 = array('#','Full Name','Join Date','Mobile Phone','Email','Paid to Date','Balance','Extras');

    $header_name_badges1 = array($groupname);
    $header_name_badges2 = array('#','Full Name','Join Date','Ticket','Final','Packet');

    $header_all_info1 = array($groupname);
    $header_all_info2 = array("Tour Leader:".$groupData['leader_name']);
    $header_all_info3 = array('#','Full Name','Join Date','Passport #','PP Exp','Mobile Phone','Email',);

    
	
    $style1 = array('font-size'=>'16','font-style'=>'bold', 'fill'=>'#fff', 'halign'=>'center','border'=>'left,right,top,bottom','height'=>30,'wrap_text'=>true,'collapsed'=>true);
    $style4 = array('font-style'=>'bold', 'fill'=>'#eee', 'halign'=>'center','border'=>'left,right,top,bottom');
    $style5 = array('wrap_text'=>true,'height'=>30,'halign'=>'left','valign'=>'center','wrap_text'=>true,'collapsed'=>true);
    $style3 = array('font-style'=>'bold');
    $stylesCancel = array('font-style'=>'bold', 'fill'=>'#eee', 'halign'=>'center','border'=>'left,right,top,bottom');
    $writer = new XLSXWriter();
	
	$writer->setTitle($filetitle);
	$writer->setSubject($filesubject);
	$writer->setAuthor($fileauthor);
	$writer->setCompany($filecompany);
    $writer->setDescription($filedesc);
    

    $header = array(
        ''=>'string',
        ''=>'string',
        ''=>'string',
        ''=>'string',
        ''=>'string',
        ''=>'string',
        ''=>'string',
        ''=>'string'
      );
    //To Date
    $writer->writeSheetHeader('To Date', $header, $col_options = ['widths'=>[5,30,25,20,35,35]] );
    //$writer->writeSheetHeader('To Date', $rowdata = array(100,200,300,400,500,600), $col_options = ['widths'=>[10,20,30,40,50]] );

    $writer->writeSheetRow('To Date', $header_to_date1, $style1);
    $writer->writeSheetRow('To Date', $header_to_date2, $style3);

    $writer->writeSheetRow('To Date', $header_to_date3, $style4);
    $array1 = array();


    //Name badges
    $writer->writeSheetHeader('Name Badges', $header, $col_options = ['widths'=>[5,30,25,30,30,30]] );
    $writer->writeSheetRow('Name Badges', $header_name_badges1, $style1);
    $writer->writeSheetRow('Name Badges', $header_name_badges2, $style4);
    $array2 = array();


    //Paid to Date
    $writer->writeSheetHeader('Paid To Date', $header, $col_options = ['widths'=>[5,30,25,20,35,20,20,40]] );
	$writer->writeSheetRow('Paid To Date', $header_paid_to_date1, $style1);
    $writer->writeSheetRow('Paid To Date', $header_paid_to_date2, $style4);
    $array3 = array();
    
    //All info
    $writer->writeSheetHeader('All Info', $header, $col_options = ['widths'=>[5,30,25,20,20,20,35]] );
    $writer->writeSheetRow('All Info', $header_all_info1, $style1);
    $writer->writeSheetRow('All Info', $header_all_info2, $style3);
    $writer->writeSheetRow('All Info', $header_all_info3, $style4);
    $array4 = array();

    $iTodateHeadingLines = 3 ;
    $iNameBadgeHeadingLines = 2 ;
    $iPaidToDateHeadingLines = 2 ;
    $iAllInfoHeadingLines = 3 ;
    
	$i = 1;
    $array = array();
    $canceled = false ;
    //while ($row=$result->fetch_assoc())
    foreach( $queryContacts as $contactsData )
    {

        $contactId = $contactsData['id'];
        $extras = "";
        if($contactsData['specialtext']!="" && $contactsData['special_land_text']!=""){
            $extras = $contactsData['specialtext'].", ".$contactsData['special_land_text'];
        }
        else if($contactsData['specialtext']!=""){
            $extras = $contactsData['specialtext'];
        }
        else if($contactsData['special_land_text']!=""){
            $extras = $contactsData['special_land_text'];
        }
        
        if( ! $canceled && $contactsData['contact_status'] == 0 ) {
            $writer->writeSheetRow('To Date', ['Canceled Members'], $stylesCancel);
            $writer->writeSheetRow('Paid To Date', ['Canceled Members'], $stylesCancel);
            $writer->writeSheetRow('Name Badges', ['Canceled Members'], $stylesCancel);
            $writer->writeSheetRow('All Info', ['Canceled Members'], $stylesCancel);
            $canceled = true ;
            $writer->markMergedCell('To Date', $i+$iTodateHeadingLines, 0, $i+$iTodateHeadingLines, 4) ;
            $writer->markMergedCell('Paid To Date', $i+$iPaidToDateHeadingLines, 0, $i+$iPaidToDateHeadingLines, 4) ;
            $writer->markMergedCell('Name Badges', $i+$iNameBadgeHeadingLines, 0, $i+$iNameBadgeHeadingLines, 2) ;
            $writer->markMergedCell('All Info', $i+$iAllInfoHeadingLines, 0, $i+$iAllInfoHeadingLines, 6) ;
        }
        
        $invoiceData = (new CustomerPayment())->customerPaymentDetails($contactId, $tourid) ;

        $array1[1] = $i;
        $array1[2] = $contactsData['title']. " ".$contactsData['fname']. " ".$contactsData['mname']. " ".$contactsData['lname'];
        //$array1[3] = $contactsData['createtime'];
        $array1[3] = date('m/d/Y', strtotime($contactsData['createtime']));
        $array1[4] = $contactsData['mobile'];
        $array1[5] = $contactsData['email'];
        $array1[6] = GUtils::buildGSTAddress(true, $contactsData, ",", " ");

        $array2[1] = $i;
        $array2[2] = $contactsData['fname']. " ".$contactsData['lname'];
        //$array2[3] = $contactsData['createtime'];
        $array2[3] = date('m/d/Y', strtotime($contactsData['createtime']));
        $array2[4] = $contactsData['mobile'];
        $array2[5] = $contactsData['email'];
        if(!empty($invoiceData['payment_amount'])){ $array2[6] = "$".number_format($invoiceData['payment_amount'],2); } else{ $array2[6] = ""; }
        if(!empty($invoiceData['balance'])){ $array2[7] = "$".number_format($invoiceData['balance'],2); } else{ $array2[7] = ""; }

        if(!empty($contactsData['special_land_text']) || !empty($contactsData['specialtext']) ){ $array2[8] = $extras; } else{ $array2[8] = ""; }

        $array3[1] = $i;
        //$array3[2] = $contactsData['fname']. " ".$contactsData['lname'];
        $array3[2] = $contactsData['badgename'];
        //$array3[3] = $contactsData['createtime'];
        $array3[3] = date('m/d/Y', strtotime($contactsData['createtime']));
        $array3[4] = "";
        $array3[5] = "";
        $array3[6] = "";


        $array4[1] = $i;
        $array4[2] = $contactsData['fname']. " ".$contactsData['lname'];
        //$array4[3] = $contactsData['createtime'];
        $array4[3] = date('m/d/Y', strtotime($contactsData['createtime']));
        $array4[4] = $contactsData['passport'];
        $array4[5] = $contactsData['expirydate'];
        $array4[6] = $contactsData['mobile'];
        $array4[7] = $contactsData['email'];
        

        $writer->writeSheetRow('To Date', $array1, $style5);
        $writer->writeSheetRow('Paid To Date', $array2, $style5);
        $writer->writeSheetRow('Name Badges', $array3, $style5);
        $writer->writeSheetRow('All Info', $array4, $style5);

		$i++;
    }
    $writer->markMergedCell('To Date', $start_row=1, $start_col=0, $end_row=1, $end_col=4);
    $writer->markMergedCell('To Date', $start_row=2, $start_col=0, $end_row=2, $end_col=3);

    $writer->markMergedCell('Paid To Date', $start_row=1, $start_col=0, $end_row=1, $end_col=4);
    $writer->markMergedCell('Paid To Date', $start_row=1, $start_col=5, $end_row=1, $end_col=9);

    
    $writer->markMergedCell('Name Badges', $start_row=1, $start_col=0, $end_row=1, $end_col=4);
    $writer->markMergedCell('All Info', $start_row=1, $start_col=0, $end_row=1, $end_col=4);

    //Include rooming sheet
    include __DIR__ . '/excel-rooming-sheet.php' ;
    
    $writer->writeToStdOut();
    exit(0);


} else {
	echo "You can't access this page directly.";
	exit(0);
}