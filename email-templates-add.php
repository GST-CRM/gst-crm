<?php
include "session.php";
$PageTitle = "Create New Template";
include "header.php";

$template_name = mysqli_real_escape_string($db, $_POST["template_name"]);
$template_type_id = mysqli_real_escape_string($db, $_POST["template_type_id"]);
$template_subject = mysqli_real_escape_string($db, $_POST["template_subject"]);
$template_description = mysqli_real_escape_string($db, $_POST["template_description"]);
$template_body = mysqli_real_escape_string($db, $_POST["template_body"]);
//$status = mysqli_real_escape_string($db, $_POST["status"]);
if ($_POST["status"]==1){ $status = "1"; } else { $status = "0"; }

if(isset($_POST['submit']))
{
    date_default_timezone_set('America/Chicago');
    $NowDate    = date("Y-m-d");
    //$template_body = base64_encode(trim($template_body));
    $userId = (new Users())->getLoggedInUserId();
    $sql = "INSERT INTO email_templates_new (template_name, template_type_id, template_subject, template_description, template_body,status,createdon, created_userid)
    VALUES ('$template_name', '$template_type_id', '$template_subject', '$template_description', '$template_body','$status','$NowDate', '$userId')";

    if ($db->query($sql) === TRUE) 
    {
        $last_id    = $db->insert_id;
        if ($_POST['submitBtn'] == 'save_close') {
            GUtils::setSuccess("Email Template has been created successfully! <a style='color: white;font-weight: bold;' href='email-templates-edit.php?id=$last_id&action=edit'>Click here to edit the Template.</a>");
            GUtils::redirect('email-templates.php');
        } else {
            GUtils::setSuccess("Email Template has been created successfully!");
            GUtils::redirect('email-templates-add.php');
        }
    } 
    else 
    {
        echo $sql . "<br>" . $db->error."<br> error";
    }

    $db->close();
}

?>
<!--<script src="https://cdn.ckeditor.com/4.12.1/standard-all/ckeditor.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.2.2/tinymce.min.js" integrity="sha256-MYEJOVodtWOmhHp5ueLNwfCwBBGKWJWUucfLvXzdles=" crossorigin="anonymous"></script>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5>
					<?php echo (isset($_GET['payment']) ? 'Edit Template #' . $_GET['payment'] : 'Create New Template'); ?>
                </h5>
            </div>
            <form name="templateAddForm" action="" method="POST" id="contact1" class="card-block gst-block  row gst-no-right-p" >
                <input type="hidden" id="submitBtn" name="submitBtn" value="" >
                <div class="col-md-12 row">
                    <div class="form-group form-default form-static-label col-sm-8">
                        <label class="float-label gst-label">Folder</label>
                        <select name="template_type_id" class="form-control" required>
                            <?php
                                $templateTypeResult = mysqli_query($db,"SELECT ett.type_id,ett.name FROM email_templates_type ett WHERE ett.status=1");
                                while($ResultData = mysqli_fetch_array($templateTypeResult))
                                {   
                                    echo "<option value='".$ResultData['type_id']."'>".$ResultData['name']."</option>";
                                }
                            ?>
                        </select>
                    </div>

                    <div class="col-sm-4">
                        <label class="float-label gst-label">Available For Use</label>
                        <div class="can-toggle">
                          <input id="status" name="status" value="1" type="checkbox" checked="">
                          <label for="status">
                            <div class="can-toggle__switch" data-checked="Active" data-unchecked="Disabled"></div>
                          </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 row">
                    <div class="form-group form-default form-static-label col-sm-6">
                        <label class="float-label gst-label">Email Template Name</label>
                        <input type="text" name="template_name" id="template_name" class="form-control" value="<?php echo $template_name; ?>" style="padding-left: 10px" required>
                    </div>

                    <div class="form-group form-default form-static-label col-sm-6">
                        <label class="float-label gst-label">Description</label>
                        <input type="text" name="template_description" id="template_description" class="form-control" value="<?php echo $template_description; ?>" style="padding-left: 10px">
                    </div>
                </div>

                <div class="col-md-12 row">
                    <div class="form-group form-default form-static-label col-sm-12">
                        <label class="float-label gst-label">Subject</label>
                        <input type="text" name="template_subject" id="template_subject" class="form-control" value="<?php echo $template_subject; ?>" style="padding-left: 10px" required>
                    </div>
                </div>

                <div class="col-md-12 row">
                    <div class="form-group form-default form-static-label col-sm-12">
                        <label class="float-label gst-label">Email Body</label>
                        <!--<textarea cols="80" id="template_body" name="template_body" class="ckeditor" rows="10" required data-sample-short></textarea>-->
                        <textarea cols="80" id="tinyMce" name="template_body" rows="10" data-sample-short></textarea>
                    </div>
                </div>

                <div class="col-md-12 row">
                    <div class="form-group form-default form-static-label col-sm-12">
                        <button type="submit" name="submit" class="btn waves-effect waves-light btn-success mr-1" data-val="save_new"><i class="far fa-check-circle"></i>Save & New</button>
                        <button type="submit" name="submit" class="btn waves-effect waves-light btn-info mr-1" data-val="save_close"><i class="far fa-check-circle"></i>Save & Close</button>
                        <button type="reset" class="btn waves-effect waves-light btn-inverse" onclick="window.onbeforeunload = null; window.location.href = 'email-templates.php'"><i class="fas fa-ban"></i>Cancel</button>
                    </div>
                </div>
                <div class="col-md-12 row">
                    <div class="gst-spacer-10"></div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $('.btn').click(function() {
              var buttonval    = $(this).attr('data-val');
              $("#submitBtn").val(buttonval);
        })
    });
</script>
<script type="text/javascript">
   // CKEDITOR.replace('template_body');
</script>

<script language="javascript" type="text/javascript">
  tinyMCE.init({    
    selector: "#tinyMce",
    plugins: [
        "image","code","autoresize","media","link","imagetools"
    ],
    link_class_list: [
        {title: 'None', value: ''},
        {title: 'PDF', value: 'pdf'}
    ],
    default_link_target: "_blank",
    toolbar1: "insertfile undo | redo | styleselect | bold | italic | alignleft | aligncenter | alignright | alignjustify | bullist | numlist | outdent | indent | link | image | media | code",
    toolbar2: "",
    menubar : true,
    automatic_uploads: true,
    statusbar : false,
    convert_urls: false,
    width: 800,
    height: 300,
    min_height: 300,
    images_upload_handler: function (blobInfo, success, failure) {
        var xhr, formData;

        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', 'inc/uploadImage.php');

        xhr.onload = function() {
          var json;

          if (xhr.status != 200) {
            failure('HTTP Error: ' + xhr.status);
            return;
          }

          json = JSON.parse(xhr.responseText);

          if (!json || typeof json.location != 'string') {
            failure('Invalid JSON: ' + xhr.responseText);
            return;
          }

          success(json.location);
        };

        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());

        xhr.send(formData);
      }
});
</script>

<?php include "footer.php"; ?>