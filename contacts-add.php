<?php include "session.php";
$PageTitle = "Add a Contact";
include "header.php";
?>
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->

<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <div class="card">
            <div class="card-header">
                <h5>Add a New Contact</h5>
            </div>
            <div class="card-block  row">
				<form id="main" name="main" method="post" action="inc/contact-functions.php" class="form-material row" onsubmit="setFormSubmitting()">
                    <div class="form-group form-default form-static-label col-sm-3">
                        <input type="text" name="newcontact" value="createnew" class="form-control" hidden>
						<select name="title" id="title" class="form-control form-control-default">
                            <option value="0" disabled selected> </option>
                            <option value="Mr.">Mr.</option>
                            <option value="Ms.">Ms.</option>
                            <option value="Mrs.">Mrs.</option>
                            <option value="Dr.">Dr.</option>
                            <option value="Rev.">Rev.</option>
                            <option value="Father">Father</option>
                            <option value="Pastor">Pastor</option>
                            <option value="Deacon">Deacon</option>
                        </select>
                        <span class="form-bar"></span>
                        <label class="float-label">Title</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <input type="text" id="fname" name="FirstName" class="form-control" required="">
                        <span class="form-bar"></span>
                        <label class="float-label">First Name</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <input type="text" id="mname" name="MiddleName" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">Middle Name</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
                        <input type="text" id="lname" name="LastName" class="form-control" required="">
                        <span class="form-bar"></span>
                        <label class="float-label">Last Name</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <input id="email" type="email" name="email" class="form-control" required="" onchange="validate()">
                        <span class="form-bar"></span>
                        <label class="float-label">Email <span id='results'></span></label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <input type="text" name="secondaryemail" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">Secondary Email</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
						<input id="mobile" type="tel" name="mobile" value="" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">Mobile Phone #</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <input id="tel" type="tel" name="homephone" value="" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">Home Phone #</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <input id="businessphone" type="tel" name="businessphone" value="" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">Business Phone #</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
						<input id="fax" type="tel" name="fax" value="" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">Fax #</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <input type="text" name="company" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">Company Name</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <input type="text" name="jobtitle" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">Job Title</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <input type="text" name="website" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">Website</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <input type="text" name="address1" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">Address Line 1</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <input type="text" name="address2" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">Address Line 2</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <input type="text" name="city" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">City</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <select name="state" class="form-control">
							<option value="0" disabled selected> </option>
							<?php $con2 = new mysqli($servername, $username, $password, $dbname);
							$result2 = mysqli_query($con2,"SELECT name, abbrev FROM states");
							while($row2 = mysqli_fetch_array($result2))
							{
								echo "<option value='".$row2['abbrev']."'>".$row2['name']."</option>";
							} ?>
                        </select>
                        <span class="form-bar"></span>
                        <label class="float-label">State</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <input type="text" name="zipcode" class="form-control">
                        <span class="form-bar"></span>
                        <label class="float-label">ZIP Code</label>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <select name="country" class="form-control form-control-default">
                            <option value="0" disabled selected> </option>
							<?php $con3 = new mysqli($servername, $username, $password, $dbname);
							$result3 = mysqli_query($con3,"SELECT name, abbrev FROM countries");
							while($row3 = mysqli_fetch_array($result3))
							{
								$selected = '';
								if($row3['abbrev'] == 'US') {$selected = 'selected';}
								echo "<option value='".$row3['abbrev']."' $selected>".$row3['name']."</option>";
							} ?>
                        </select>
                        <span class="form-bar"></span>
                        <label class="float-label">Country</label>
                    </div>
					<div class="col-sm-12" style="width:100%;height:15px;"></div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <select name="language" class="form-control form-control-default">
                            <option value="English" selected>English</option>
                            <option value="French">French</option>
                            <option value="German">German</option>
                            <option value="Arabic">Arabic</option>
                             <option value="Italian">Italian</option>
                            <option value="Spanish">Spanish</option>
                        </select>
                        <span class="form-bar"></span>
                        <label class="float-label">Language</label>
                    </div>
                    <div id="affliationtype" class="form-group form-default form-static-label col-sm-4">
                        <select name="affiliation" class="form-control form-control-default" onchange="admSelectCheck(this);">
                            <option value="0" disabled selected> </option>
							<option id="admOption" value="0">Add New</option>
							<?php $meta1 = new mysqli($servername, $username, $password, $dbname);
							$metadata1 = mysqli_query($meta1,"SELECT * FROM metadata WHERE catid='1' AND Status=1");
							while($keyword1 = mysqli_fetch_array($metadata1))
							{
								if($keyword1['id'] == 1) { $Selected = "selected"; } else { $Selected = ""; }
								echo "<option value='".$keyword1['id']."' $Selected>".$keyword1['meta']."</option>";
							} ?>
                        </select>
                        <span class="form-bar"></span>
                        <label class="float-label">Category</label>
                    </div>
					<div id="affliationtypenew" class="form-group form-default form-static-label col-sm-4" style="display:none;">
						<label class="float-label" style="top: 0px;font-size: 11px;color:#888888;">Add New Category</label>
						<input type="text" name="newaffliationtype" value="" class="form-control form-control-default">
					</div>
                    <div class="form-group form-default form-static-label col-sm-4">
                        <select name="preferredmethod" class="form-control form-control-default">
                            <option value="0" disabled selected> </option>
                            <option value="Email">Email</option>
                            <option value="Phone">Phone</option>
                            <option value="Mobile">Mobile</option>
                        </select>
                        <span class="form-bar"></span>
                        <label class="float-label">Preferred Method</label>
                    </div>
                    <div class="col-sm-12"><br /></div>
					<div class="form-group form-default form-static-label col-md-12">
                        <textarea name="notes" class="form-control"><?php echo $data["notes"]; ?></textarea>
                        <span class="form-bar"></span>
                        <label class="float-label">Notes for this Contact</label>
                    </div>
                    <div class="col-sm-12"><br /><br />
                        <button type="submit" name="addcontact" value="save_only" class="btn waves-effect waves-light btn-success mr-1"><i class="far fa-check-circle"></i>Save</button>
						<button type="submit" name="addcontact" value="save_new" class="btn waves-effect waves-light btn-success mr-1"><i class="far fa-check-circle"></i>Save & New</button>
                        <button type="submit" name="addcontact" value="save_close" class="btn waves-effect waves-light btn-info mr-1"><i class="far fa-check-circle"></i>Save & Close</button>
                        <button type="reset" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Clear</button>
					</div>
				</form>
			</div>
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<?php require('inc/metadata-js.php'); ?>

<script type="text/javascript">
$(document).ready(function() {

    var buttonname;
    var buttonval;
    $('.btn').click(function() {
          buttonname    = $(this).attr('name');
          buttonval     = $(this).val();
    })

	$("form").submit(function() {
		// Getting the form ID
		var  formID           = $(this).attr('id');
		var formDetails       = $('#'+formID);
        var buttonURI         = '&' + encodeURI(buttonname) + '=' + encodeURI(buttonval);


		$.ajax({
			type     : "POST",
			url      : 'inc/contact-functions.php',
            data     : formDetails.serialize()+buttonURI,
			dataType : "json",
			success: function (data) {	
                if (data.error == 0)
                {
                    window.location = data.url;
                }
                else
                {
    				// Inserting html into the result div
                    $('#resultsmodal').modal("show");
    				$('#results').html(data.message);
    				//$("form")[0].reset();
                }
			},
			error: function(jqXHR, text, error){
            // Displaying if there are any errors
            	$('#result').html(error);           
            }
        });
		return false;
	});
});

var formSubmitting = false;
var setFormSubmitting = function() { formSubmitting = true; };

window.onload = function() {
    window.addEventListener("beforeunload", function (e) {
        if (formSubmitting) {
            return undefined;
        }

        var confirmationMessage = 'It looks like you have been editing something. '
                                + 'If you leave before saving, your changes will be lost.';

        (e || window.event).returnValue = confirmationMessage; //Gecko + IE
        return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
    });
};

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function validate() {
  var $result = $("#results");
  var email = $("#email").val();
  $result.text("");

  if (validateEmail(email)) {
    $result.text(email + " is valid");
    $result.css("color", "green");
  } else {
    $result.text(" | " + email + " is not valid!");
    $result.css("color", "red");
    $("#email").val("");
  }
  return false;
}
</script>
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>