<?php
include "session.php";
$PageTitle = "Group Leader List";
include "header.php"; 

?>
<!-- [ page content ] start -->
<div class="row">
    <div class="col-sm-12">
        <!-- HTML5 Export Buttons table start -->
        <div class="card">
            <div class="row">
                <div class="col-md-3" style="margin-top:15px;padding-left:45px;">
                    <div class="input-group input-group-sm mb-0">
                        <span class="input-group-prepend mt-0">
                            <label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
                        </span>
                        <input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
                    </div>
                </div>
                <div class="col-md-5" style="margin-top:15px;"></div>
                <div class="col-md-2" style="margin-top:15px;">
                    <select id="status" name="status" class="form-control form-control-default fill" onChange="doReload(this.value);">
                        <option value="">Status</option>
                        <option value="active" <?php if(!empty($_GET['status']) AND $_GET['status'] == "active") {echo "selected";} ?>>Active Leaders</option>
                        <option value="disabled" <?php if(!empty($_GET['status']) AND $_GET['status'] == "disabled") {echo "selected";} ?>>Disabled Leaders</option>
                    </select>
                </div>
                <div class="col-md-2" style="margin-top:15px;padding-right:45px;">
                    <a href="contacts-add.php" class="btn waves-effect float-right waves-light btn-success" style="padding: 3px 13px;"><i class="fas fa-plus-circle"></i>Add New Contact</a>
                </div>
            </div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="TableWithNoButtons" class="table table-hover table-striped table-bordered nowrap">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Full Name</th>
                                <th>Groups Leading</th>
                                <th>Phone</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th style="display:none;">Company</th>
                                <th style="display:none;">Birthday</th>
                                <th style="display:none;">Address 1</th>
                                <th style="display:none;">Address 2</th>
                                <th style="display:none;">ZIP Code</th>
                                <th style="display:none;">City</th>
                                <th style="display:none;">State</th>
                                <th style="display:none;">Country</th>
                                <th style="display:none;">Emergency Contact Name</th>
                                <th style="display:none;">Emergency Contact Relation</th>
                                <th style="display:none;">Emergency Contact Phone #</th>
                                <th style="display:none;">Martial Status</th>
                                <th style="display:none;">Gender</th>
                                <th style="display:none;">Language</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
						if ($_GET['status'] == "disabled") {
							$sql = "SELECT contacts.*, group_leaders.*
								FROM contacts
								INNER JOIN group_leaders
								ON contacts.id=group_leaders.contactid
								WHERE group_leaders.status ='0'";
						} else {
							$sql = "SELECT contacts.*, group_leaders.*
								FROM contacts
								INNER JOIN group_leaders
								ON contacts.id=group_leaders.contactid
								WHERE group_leaders.status ='1'";
						}

						$result = $db->query($sql);
						if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
                            <tr>
                                <td><?php echo "G".$row["id"]; ?></td>
                                <td><a href="contacts-edit.php?id=<?php echo $row["id"]; ?>&action=edit&usertype=leader" style="color: #0000EE;">
                                        <?php echo $row["fname"]." ".$row["mname"]." ".$row["lname"]; ?></a></td>
                                <td style="text-align:center;"><a href="groups.php?leader=<?php echo $contactidz=$row["id"]; ?>" style="color: #0000EE;">View groups</a></td>
                                <td><?php echo $row["phone"]; ?></td>
                                <td><?php echo $row["mobile"]; ?></td>
                                <td><?php echo $row["email"]; ?></td>
                                <td style="display:none;"><?php echo $row["company"]; ?></td>
                                <td style="display:none;"><?php echo $row["birthday"]; ?></td>
                                <td style="display:none;"><?php echo $row["address1"]; ?></td>
                                <td style="display:none;"><?php echo $row["address2"]; ?></td>
                                <td style="display:none;"><?php echo $row["zipcode"]; ?></td>
                                <td style="display:none;"><?php echo $row["city"]; ?></td>
                                <td style="display:none;"><?php echo $row["state"]; ?></td>
                                <td style="display:none;"><?php echo $row["country"]; ?></td>
                                <td style="display:none;"><?php echo $row["emergencyname"]; ?></td>
                                <td style="display:none;"><?php echo $row["emergencyrelation"]; ?></td>
                                <td style="display:none;"><?php echo $row["emergencyphone"]; ?></td>
                                <td style="display:none;"><?php echo $row["martialstatus"]; ?></td>
                                <td style="display:none;"><?php echo $row["gender"]; ?></td>
                                <td style="display:none;"><?php echo $row["language"]; ?></td>
                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
    </div>
</div>
<script>
    //Just to move the search box to be on the top of the card section
    $('.outsideBorderSearch').on('keyup click', function() {
        $('#TableWithNoButtons').DataTable().search(
            $('.outsideBorderSearch').val()
        ).draw();
    });

    $(document).ready(function() {
        $('#TableWithNoButtons').DataTable({
            //"ordering": false, //To disable the ordering of the tables
            "bLengthChange": true, //To hide the Show X entries dropdown
            dom: 'Blrtip',
            buttons: ['csv', 'excel'],
        });
    });

    function doReload(status) {
        document.location = 'leaders.php?status=' + status;
    }

</script>
<!-- [ page content ] end -->
<?php include "footer.php"; ?>
