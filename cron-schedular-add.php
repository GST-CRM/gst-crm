<?php
include "session.php";
$PageTitle = "Create New Job";
include "header.php";

$Cron_Loop = mysqli_real_escape_string($db, $_POST["Cron_Loop"]);
$Cron_Name = mysqli_real_escape_string($db, $_POST["Cron_Name"]);
$Email_Template_ID = mysqli_real_escape_string($db, $_POST["Email_Template_ID"]);
$Cron_Period = mysqli_real_escape_string($db, $_POST["Cron_Period"]);
$Cron_Days = mysqli_real_escape_string($db, $_POST["Cron_Days"]);
$Cron_Schedule_Date = mysqli_real_escape_string($db, $_POST["Cron_Schedule_Date"]);

if($Cron_Period != "After") {$Cron_Period = "Before";}
if($Cron_Loop != 99) { $Cron_Period = NULL; $Cron_Days = 0; }
if($Cron_Loop != 1) { $Cron_Schedule_Date = "0000-00-00"; }

if(isset($_POST['submit'])) {
    $sql = "INSERT INTO `cron_schedular`(Cron_Name, Email_Template_ID, Cron_Period, Cron_Days, Cron_Loop, Cron_Schedule_Date, Cron_Status, Cron_Created_By)
			VALUES ('$Cron_Name','$Email_Template_ID','$Cron_Period','$Cron_Days','$Cron_Loop','$Cron_Schedule_Date',1,'$UserAdminID')";

    if ($db->query($sql) === TRUE) {
        $last_id    = $db->insert_id;
        if ($_POST['submitBtn'] == 'save_close') {
            GUtils::setSuccess("Job has been created successfully! <a style='color: white;font-weight: bold;' href='cron-schedular-edit.php?id=$last_id&action=edit'>Click here to edit the Job.</a>");
            GUtils::redirect('cron-schedular.php');
        } else {
            GUtils::setSuccess("Job has been created successfully!");
            GUtils::redirect('cron-schedular-add.php');
        }
    } else  {
        echo $sql . "<br>" . $db->error."<br> error";
    }
    $db->close();
}

?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<form name="templateAddForm" action="" method="POST" id="contact1" class="card-block row">
				<input type="hidden" id="submitBtn" name="submitBtn" value="" >
				<div class="form-group form-default form-static-label col-sm-2">
					<label class="float-label gst-label">Job Loop</label>
					<select name="Cron_Loop" class="form-control" required onChange="CronLoop(this.value);">
						<option value="0" selected disabled>Select</option>
						<option value="99" <?php if($Cron_Loop == 99) { echo "selected"; } ?>>Before/After</option>
						<option value="1" <?php if($Cron_Loop == 1) { echo "selected"; } ?>>Specific Date</option>
						<option value="2" <?php if($Cron_Loop == 2) { echo "selected"; } ?>>Hourly</option>
						<option value="3" <?php if($Cron_Loop == 3) { echo "selected"; } ?>>Daily</option>
						<option value="4" <?php if($Cron_Loop == 4) { echo "selected"; } ?>>Weekly</option>
						<option value="5" <?php if($Cron_Loop == 5) { echo "selected"; } ?>>Monthly</option>
					</select>
				</div>
				<div class="form-group form-default form-static-label col-sm-4">
					<label class="float-label gst-label">Job Name</label>
					<input type="text" name="Cron_Name" id="Cron_Name" class="form-control" value="<?php echo $Cron_Name; ?>" style="padding-left: 10px" required>
				</div>
				<div class="form-group form-default form-static-label col-sm-6">
					<label class="float-label gst-label">Template Name</label>
					<select name="Email_Template_ID" class="form-control" required>
						<?php
						$templateTypeResult = mysqli_query($db,"SELECT etn.template_id,etn.template_name FROM email_templates_new etn WHERE etn.status=1");
						while($ResultData = mysqli_fetch_array($templateTypeResult)) {
							if($Email_Template_ID == $ResultData['template_id'] ) { $Selected = "selected"; } else { $Selected = ""; }
							echo "<option value='".$ResultData['template_id']."' $Selected>".$ResultData['template_name']."</option>";
						} ?>
					</select>
				</div>
				<div id="cron_scheduled_date" style="display:none;" class="form-group form-default form-static-label col-sm-2">
					<label class="float-label gst-label">Add Date</label>
					<input type="date" name="Cron_Schedule_Date" class="form-control" value="<?php echo $Cron_Schedule_Date; ?>" <?php if($Cron_Loop == 1) { echo "disabled"; } ?>>
				</div>
				<div id="Cron_Days" style="display:none;" class="form-group form-default form-static-label col-sm-2">
					<label class="float-label gst-label">Number of Days</label>
					<input type="number" name="Cron_Days" class="form-control" value="<?php echo $Cron_Days; ?>">
				</div>
				<div id="Cron_AfterBefore" style="display:none;" class="col-sm-2">
				<style>.can-toggle label .can-toggle__switch {background: #70c767 !important;font-weight:bold;}</style>
					<label class="float-label gst-label">Before/After</label>
					<div class="can-toggle">
						<input id="Cron_Period" name="Cron_Period" value="After" type="checkbox" <?php if($Cron_Period == 1) { echo "checked"; } ?>>
						<label for="Cron_Period">
						<div class="can-toggle__switch" data-checked="After" data-unchecked="Before"></div>
						</label>
					</div>
				</div>
				<div class="col-md-12 mb-2 mt-3">
					<button type="submit" name="submit" class="btn waves-effect waves-light btn-success mr-1" data-val="save_new"><i class="far fa-check-circle"></i>Save & New</button>
					<button type="submit" name="submit" class="btn waves-effect waves-light btn-info mr-1" data-val="save_close"><i class="far fa-check-circle"></i>Save & Close</button>
					<button type="reset" class="btn waves-effect waves-light btn-inverse" onclick="window.onbeforeunload = null; window.location.href = 'cron-schedular.php'"><i class="fas fa-ban"></i>Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
$(document).ready(function() {
	$('.btn').click(function() {
		var buttonval    = $(this).attr('data-val');
		$("#submitBtn").val(buttonval);
	})
});

function CronLoop(value) {
	if (value == 1) {
		$("#cron_scheduled_date").show();
	} else {
		$("#cron_scheduled_date").hide();
	}
	if (value == 99) {
		$("#Cron_AfterBefore").show();
		$("#Cron_Days").show();
	} else {
		$("#Cron_AfterBefore").hide();
		$("#Cron_Days").hide();
	}
}
</script>
<?php include "footer.php"; ?>