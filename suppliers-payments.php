<?php

include "session.php";
include_once "inc/helpers.php";


$printTitle = 'Receipt' ;
if( isset($_REQUEST['action']) && $_REQUEST['action'] == 'delete' ) {
    $editId = $_GET['id'] ;

    //Fidn transaction id
    $sqlts = "SELECT Transaction_Type_Transaction_ID FROM suppliers_payments WHERE Supplier_Payment_ID ='$editId'" ;
    $transactionId = GDb::fetchScalar($sqlts) ;
    //delete transaction id
    $sqltd =  "DELETE FROM transactions WHERE Transaction_ID='$transactionId' LIMIT 1" ;
    GDb::execute($sqltd) ;
    //payment
    $sqll =  "DELETE FROM suppliers_payments WHERE Supplier_Payment_ID = '$editId'" ;
    GDb::execute($sqll) ;
    //Todo delete other table too
    GUtils::redirect('suppliers-payments.php') ;
}
else if( isset($_REQUEST['action']) && $_REQUEST['action'] == 'void' ) {
    $editId = $_GET['id'] ;
    //Fidn transaction id
    $sqlts = "SELECT Transaction_Type_Transaction_ID FROM suppliers_payments WHERE Supplier_Payment_ID ='$editId' AND Status=1 " ;
    $transactionId = GDb::fetchScalar($sqlts) ;
    //void payment
    $sql =  "UPDATE suppliers_payments SET Status=0 WHERE Supplier_Payment_ID = '$editId' LIMIT 1" ;
    GDb::execute($sql) ;
    //delete transaction id
    $sqltd =  "UPDATE transactions SET Voided_Flag=1 WHERE Transaction_ID='$transactionId' LIMIT 1" ;
    GDb::execute($sqltd) ;

    GUtils::redirect('suppliers-payments.php') ;
}
else if( isset($_REQUEST['action']) && $_REQUEST['action'] == 'unvoid' ) {
    $editId = $_GET['id'] ;
    //Fidn transaction id
    $sqlts = "SELECT Transaction_Type_Transaction_ID FROM suppliers_payments WHERE Supplier_Payment_ID ='$editId' AND Status=0 " ;
    $transactionId = GDb::fetchScalar($sqlts) ;
    //void payment
    $sql =  "UPDATE suppliers_payments SET Status=1 WHERE Supplier_Payment_ID = '$editId' LIMIT 1" ;
    GDb::execute($sql) ;
    //delete transaction id
    $sqltd =  "UPDATE transactions SET Voided_Flag=0 WHERE Transaction_ID='$transactionId' LIMIT 1" ;
    GDb::execute($sqltd) ;

    GUtils::redirect('suppliers-payments.php') ;
}
else if( isset($_REQUEST['action']) && $_REQUEST['action'] == 'send' ) {
    $FileTitlePrefix = 'receipt' ;
    include 'inc/suppliers-bill-mail.php' ;
    GUtils::redirect('suppliers-payments.php') ;
}

else if( isset($_REQUEST['batch-invaction']) ) {
    if( $_REQUEST['batch-invaction'] == 'P' ) {
        include 'inc/bills-print.php';
    }
    else if( $_REQUEST['batch-invaction'] == 'S' ) {
        $FileTitlePrefix = 'receipt' ;
        include 'inc/suppliers-bill-mail.php';
        echo '<script type="text/javascript">window.close();</script>' ;
        die;
    }
}

$PageTitle = "Supplier Payments ";
include "header.php";

if($_GET['status'] == "voided") {
	$StatusWhere = "sp.Status=0";
} else {
	$StatusWhere = "sp.Status=1";
}

$sql = "SELECT c.fname, c.lname, c.mname, sup.contactid, sp.* FROM suppliers_payments AS sp 
		INNER JOIN suppliers_bill sb ON sb.Supplier_Bill_Num = sp.Supplier_Bill_Num AND sb.Status IN (1,2)
		LEFT JOIN suppliers AS sup ON sup.contactid = sp.Supplier_ID AND sup.status=1
		LEFT JOIN contacts AS c ON c.id = sup.contactid 
		WHERE $StatusWhere GROUP BY sp.Supplier_Payment_ID ORDER BY sp.Supplier_Payment_ID DESC" ;

$records = GDb::fetchRowSet($sql);
?>

    <form action="bills-print.php" method="POST" name="customerPaymentsListForm">
        <!-- [ page content ] start -->
        <div class="row">
            <div class="col-sm-12">
                <!-- HTML5 Export Buttons table start -->
				<div class="card">
                    <div class="row" style="position: relative">
						<div class="col-md-3" style="position: absolute; top: 50px; margin-top:15px;padding-left:45px;">
							<div class="input-group input-group-sm mb-0">
								<span class="input-group-prepend mt-0">
									<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
								</span>
								<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
							</div>
						</div>
                        
						<div class="col-md-7" style="margin-top:15px;"></div>
						<div class="col-md-2" style="margin-top:15px;">
							<select id="status" name="status" class="form-control form-control-default fill" onChange="doReload(this.value);">
								<option value="">Status</option>
								<option value="active" <?php if(!empty($_GET['status']) AND $_GET['status'] == "active") {echo "selected";} ?>>Active Payments</option>
								<option value="voided" <?php if(!empty($_GET['status']) AND $_GET['status'] == "voided") {echo "selected";} ?>>Voided Payments</option>
							</select>
						</div>
						<div class="col-md-3" style="margin-top:15px;padding-right:45px;">
							<a href="suppliers-bill-payments-add.php" class="btn waves-effect float-right waves-light btn-success" style="padding: 3px 13px;"><i class="far fa-check-circle"></i>New Supplier Payment</a>
						</div>
					</div>
                    <div class="row">
                        <div class="col-md-3">
                        <span style="margin-left: 30px;float:left;display:none;">
                            <select name="batch-invaction" onchange="return batchSubmit(this);" class="form-control form-control-default fill gst-invoice-batchaction"
                                    disabled="disabled">
                                <option value="">Batch Action</option>
                                <option value="S">Send Receipts</option>
                                <option value="P">Print Receipts</option>
                            </select>
                        </span>
                        </div>
                    </div>
                    <div class="card-block">
                        <div class="dt-responsive table-responsive">
                            <table id="TableWithNoButtons1" class="table gst-table table-hover table-striped table-bordered nowrap"
                                   data-page-length="20">
                                <thead>
                                <tr>
                                    <th style="display:none;" class="gst-no-sort"></th>
                                    <th width="1%">Payment#</th>
                                    <th>Supplier Name</th>
                                    <th>Payment Date</th>
                                    <th>Amount</th>
                                    <th width="1%" class="gst-no-sort">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($records as $row) {
                                    
                                    $name = $row['fname'] . ' ' . $row['mname'] . ' ' . $row['lname'];
                                    $nameAppend = ((strlen(trim($name)) > 0) ? $name : '');
                                    ?>
                                    <tr>
                                        <td style="display:none;"><input name="cbInvoice[]" value="<?php echo $row["Supplier_Bill_Num"];?>" type="checkbox" class="gst-invoice-cb"/></td>
                                        <td>
                                            <a style="color: #0000EE;" href="suppliers-bill-payments-add.php?payment=<?php echo $row["Supplier_Payment_ID"]; ?>">
                                                <?php echo $row["Supplier_Payment_ID"]; ?>
                                            </a>
                                        </td>
                                        <td>
                                            <?php if ($row["contactid"]) { ?>
                                            <a style="color: #0000EE;" href='<?php echo 'contacts-edit.php?id=' . $row["contactid"] . '&action=edit&usertype=supplier'; ?>'>
                                            <?php
                                            }
                                            //print text
                                            echo $nameAppend;

                                            if ($row["contactid"]) {
                                            ?>
                                            </a>
                                            <?php } ?>
                                        </td>
                                        <td><?php echo GUtils::clientDate($row["Add_Date"]); ?></td>
                                        <td><?php echo GUtils::formatMoney($row["Supplier_Payment_Amount"]); ?></td>
                                        <td >
                                            <div style="width: 150px;" class="btn-group gst-btns ">
                                                <?php if( $row['Status'] ) { ?>                                               
                                                <button onclick="window.location.href='suppliers-bill-payments-add.php?payment=<?php echo $row["Supplier_Payment_ID"]; ?>';"
                                                        type="button" class="btn btn-info">Edit/View
                                                </button>
                                                <button type="button" class="btn btn-info dropdown-toggle"
                                                        data-toggle="dropdown">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul class="dropdown-menu" role="menu" style="white-space: nowrap">
                                                    <!--<li><a href="suppliers-bill-payments-add.php?id=<?php echo $row["Supplier_Bill_Num"]; ?>">Edit/View</a></li>-->
                                                    <li><a target="_blank" href="bills-print.php?print=payment&Bill_ID=<?php echo $row["Supplier_Bill_Num"]; ?>">Print</a></li>
                                                    <li><a href="javascript:void(0);" onclick = 'showConfirm( "suppliers-bill-payments.php?action=send&Bill_ID=<?php echo $row["Supplier_Bill_Num"]; ?>", "Send", "Are you sure you want to sent this mail ?", "Please Confirm", "modal-md")' >Send</a></li>
                                                    <li><a href="javascript:void(0);" onclick = 'deleteConfirm( "suppliers-payments.php?action=delete&id=<?php echo $row["Supplier_Payment_ID"]; ?>", "Delete")' >Delete</a></li>
                                                    <li><a href="javascript:void(0);" onclick = 'deleteConfirm( "suppliers-payments.php?action=void&id=<?php echo $row["Supplier_Payment_ID"]; ?>", "Void")' >Void</a></li>
                                                </ul>
                                                <?php } else {
                                                    ?>
                                                    <button onclick = 'deleteConfirm( "suppliers-payments.php?action=unvoid&id=<?php echo $row["Supplier_Payment_ID"]; ?>")' type="button" class="btn btn-danger">
                                                        Unvoid
                                                    </button>
                                                    <?php
                                                } ?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- HTML5 Export Buttons end -->
            </div>
        </div>
    </form>
<style type="text/css">
#TableWithNoButtons1_filter { display:none;}
#TableWithNoButtons1_wrapper .dt-buttons {
    float: right;
    
}
</style>

<script type="text/javascript">
    
    function sendReminderHandler(obj) {
        
        $("#showSendReminder").modal("show");
        
        $('.gst-reminder-id').val( $(obj).data('id') ) ;
        $('.gst-reminder-class').html( $(obj).data('name') ) ;
        $('.gst-reminder-email').val( $(obj).data('email') ) ;
        $('.gst-reminder-subject').val( '' ) ;
        $('.gst-reminder-message').val( '' ) ;
    }
    
    function batchSubmit(obj) {
        var count = $('.gst-invoice-cb:checked').length ;
        if( count > 0 ) {
            
            obj.form.target = '_blank' ;
            if( obj.value == 'S' ) {
                msg = "Are you sure you want to sent " + count + " receipt(s) ?" ;
            }
            else if( obj.value == 'P' ) {
                msg = "Are you sure you want to print " + count + " receipt(s) ?" ;
            }
            else {
                return ;
            }
            showConfirm( "javascript:customerPaymentsListForm.submit();", "Yes", msg, "Please Confirm", "modal-md") ;
//            obj.form.submit();
        }
    }


//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#TableWithNoButtons1').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
});

$(document).ready(function() {
    $('#TableWithNoButtons1').DataTable( {
        //"ordering": false, //To disable the ordering of the tables
		"bLengthChange": false, //To hide the Show X entries dropdown
		dom: 'Bfrtip',
		sDom: 'Blrtip', //To Hide the search box
        buttons: [
                    {
                       extend: 'csv',
                       className: 'btn btn-default m-2',
                       exportOptions: {
                          columns: 'th:not(:last-child)'
                       }
                    },
                    {
                       extend: 'excel',
                       className: 'btn btn-default m-2',
                       exportOptions: {
                          columns: 'th:not(:last-child)'
                       }
                    }
                 ]
    } );
} );

function doReload(status){
	document.location = 'suppliers-payments.php?status=' + status;
}
</script>
<?php include 'inc/notificiations.php'; ?>
    <!-- [ page content ] end -->
<?php include "footer.php"; ?>