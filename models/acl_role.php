<?php

include_once __DIR__ . '/model.php' ;

class AclRole extends Model {

    public $table = 'acl_role';
    public $pk = 'id';
    
    public static $ADMIN = 1 ;
    public static $AGENT = 2 ;
    public static $LEADER = 4 ;
    public static $SUPPLIER = 8 ;
    public static $CUSTOMER = 16 ;
    
    public static $FLAG_ONLY_GROUP = 0x01 ;
    public static $FLAG_ONLY_MINE = 0x02 ;
}
