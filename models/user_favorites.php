<?php

include_once __DIR__ . '/model.php';
include_once __DIR__ . '/users.php';

class UserFavorites extends Model {

    public $table = 'user_favorites';
    private $userInfo;

    public function __construct() {
        $this->userInfo = (new Users())->getLoggedInUserDetail();
    }

    public function getFavoriteList() {
        $list = $this->getList(['User_ID' => $this->userInfo['UserID']]);
        foreach ($list as $k => $l) {
            $list[$k]['URI_PRINT'] = self::fixToPrint($l['URI']);
        }
        return $list;
    }

    public function add($uri, $title = '') {
        $uri = self::fixToStore($uri);
        if (!$this->userInfo['UserID']) {
            //cant add without a user id
            return;
        }
        if ($this->isFavorite($uri)) {
            //Already marked as favorite
            return;
        }
        $title = strlen($title) < 1 ? $uri : $title;
        $data  = [
            'URI'     => $uri,
            'Title'   => $title,
            'User_ID' => $this->userInfo['UserID'],
        ];

        $this->insert($data);
        return true;
    }

    public function remove($uri) {
        $userID = $this->userInfo['UserID'];
        $uri    = self::fixToStore($uri);
        if ($userID && $uri) {
            $sql = "DELETE FROM user_favorites WHERE LOWER(URI)='" . strtolower($uri) . "' AND User_ID=$userID";
            $this->execute($sql);
            return true;
        }
        return false;
    }

    public function isFavorite($uri) {
        $userID = $this->userInfo['UserID'];
        $uri    = self::fixToStore($uri);
        if ($userID && $uri) {
            $sql = "SELECT COUNT(*) AS total FROM user_favorites WHERE LOWER(URI)='" . strtolower($uri) . "' AND User_ID=$userID";
            if ($this->executeScalar($sql) > 0) {
                return true;
            }
        }
        return false;
    }

    public function findFavorite($uri) {
        $userID = $this->userInfo['UserID'];
        $uri    = self::fixToStore($uri);
        if ($userID && $uri) {
            $sql              = "SELECT * FROM user_favorites WHERE LOWER(URI)='" . strtolower($uri) . "' AND User_ID=$userID ";
            $one              = $this->executeOne($sql);
            $one['URI_PRINT'] = self::fixToPrint($one['URI']);
            return $one;
        }
        return false;
    }

    public function isRequestFavorite() {
        return $this->isFavorite(GUtils::requestUri());
    }

    public static function handleFavoriteAction($title) {
        if (isset($_REQUEST['favorite-action'])) {
            if ($_REQUEST['favorite-action'] == 'add') {
                self::handleAddFavoriteAction($title);
            } else {
                self::handleRemoveFavoriteAction();
            }
            $redirectTo = urldecode($_REQUEST['favorite-uri']) ;
            if( isset($_REQUEST['favorite-redirect']) && $_REQUEST['favorite-redirect'] ) {
                $redirectTo = urldecode($_REQUEST['favorite-redirect']) ;
            }
            GUtils::redirect($redirectTo);
        }
    }

    public static function handleAddFavoriteAction($title) {
        if (isset($_REQUEST['favorite-action']) && $_REQUEST['favorite-uri'] && $_REQUEST['favorite-action'] == 'add') {
            (new UserFavorites())->add($_REQUEST['favorite-uri'], $title);
        }
    }

    public static function handleRemoveFavoriteAction() {
        if (isset($_REQUEST['favorite-action']) && $_REQUEST['favorite-uri'] && $_REQUEST['favorite-action'] == 'remove') {
            (new UserFavorites())->remove($_REQUEST['favorite-uri']);
        }
    }

    private static function fixToPrint($url) {
        $noSlashUrl = ltrim($url, '/');
        $http       = ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http' );
        return $http . '://' . $_SERVER['SERVER_NAME'] . '/' . $noSlashUrl;
    }

    private static function fixToStore($url) {
        $noSlashUrl = ltrim($url, '/');
        return '/' . $noSlashUrl;
    }

    public static function requestUriToPrint() {
        $uri = GUtils::requestUri();
        return self::fixToPrint($uri);
    }

    public static function requestUriToStore() {
        $uri = GUtils::requestUri();
        return self::fixToStore($uri);
    }

}
