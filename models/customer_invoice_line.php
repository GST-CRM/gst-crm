<?php

include_once __DIR__ . '/model.php' ;

class CustomerInvoiceLine extends Model {
       
    public $table = 'customer_invoice_line' ;
    
    public function getListWithPackageName($invoiceId, $groupName) {
        $sqll          = "SELECT cil.Customer_Invoice_Num,cil.Customer_Invoice_Line,cil.Line_Type,cil.Product_Product_ID,('$groupName') AS Product_Name,cil.Qty, SUM(cil.Invoice_Line_Total) AS Invoice_Line_Total, SUM(cil.Qty * cil.Invoice_Line_Total) AS PackageTotal,cil.Mod_Date,cil.Add_Date FROM customer_invoice_line cil
        JOIN products pro 
        ON cil.Product_Product_ID=pro.id
        WHERE cil.Customer_Invoice_Num='$invoiceId' AND pro.categoryid IN (1,2)
        UNION
        SELECT cil.Customer_Invoice_Num,cil.Customer_Invoice_Line,cil.Line_Type,cil.Product_Product_ID, IF( isnull(pro.description) OR LENGTH(TRIM(pro.description)) < 1,cil.Product_Name, pro.description) AS Product_Name,cil.Qty,cil.Invoice_Line_Total, (cil.Qty * cil.Invoice_Line_Total) AS PackageTotal ,cil.Mod_Date,cil.Add_Date FROM customer_invoice_line cil
        JOIN products pro 
        ON cil.Product_Product_ID=pro.id
        WHERE cil.Customer_Invoice_Num='$invoiceId' AND pro.categoryid NOT IN (1,2)";
        return GDb::fetchRowSet($sqll);

    }

    public function getProductLines($invoiceId) {
        $sql = "SELECT * FROM customer_invoice_line WHERE Customer_Invoice_Num='$invoiceId'" ;
        return GDb::fetchRowSet($sql);
    }

    public function updateProductQuantity($invoiceId, $productId, $qty) {
        $sql = "UPDATE customer_invoice_line SET Qty=$qty WHERE Product_Product_ID = $productId AND Customer_Invoice_Num='$invoiceId' " ;
        $this->execute($sql) ;
    }
    public function getInvoiceLines($invoiceId) {
        $sqll          = "SELECT *, p.description FROM customer_invoice_line ci
            INNER JOIN products p ON ci.Product_Product_ID=p.id AND ci.Customer_Invoice_Num='$invoiceId'
            WHERE ci.`Customer_Invoice_Num`='$invoiceId' 
                            ORDER BY ci.Customer_Invoice_Line ASC";
        return GDb::fetchRowSet($sqll);
    }
}