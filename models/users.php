<?php

include_once __DIR__ . '/model.php' ;
include_once __DIR__ . '/contacts.php' ;
include_once __DIR__ . '/acl_role.php' ;
include_once __DIR__ . '/agents.php';
include_once __DIR__ . '/group_leaders.php';
include_once __DIR__ . '/suppliers.php';
include_once __DIR__ . '/customer_account.php';


if( ! defined('NO_PASSWORD') ) {
    define('NO_PASSWORD', '' ) ;
}

if( ! defined('HAS_PASSWORD') ) {
    define('HAS_PASSWORD', 'GST_HAS _PASSWORD' ) ;
}
class Users extends Model {
    public $table = 'users' ;
    
    public static $TOKEN_CHANGE_PASSWORD = 1 ;
    public static $TOKEN_FORGOT_PASSWORD = 2 ;
    
    public static function loggedUser() {
        $userName = GUtils::getSession('login_user') ;
        $sql = "SELECT UserID, Username, Role_ID, contactid FROM users WHERE username='$userName' LIMIT 1 " ;
        return GDb::fetchRow($sql) ;
    }
    public function getLoggedInUserDetail() {
        $userName = GUtils::getSession('login_user') ;
        return $this->get(['username' => $userName]) ;
    }
    public function getLoggedInUserId() {
        $userName = GUtils::getSession('login_user') ;
        $userInfo = $this->get(['username' => $userName]) ;
        
        if( isset($userInfo['UserID']) && $userInfo['UserID'] ) {
            return $userInfo['UserID'] ;
        }
        return null ;
    }
    public function setUserRole($email, $role, $add = true) {

        $user = $this->get(['Username' => $email]) ;
        if( $add ) {
            $newRole = $user['Role_ID'] | $role ;
        }
        else {
            $newRole = $user['Role_ID'] & ~ $role ;
        }
        $userData = [
                    'Role_ID' => $newRole
                ];
        

        if( $user['Username'] != null ) {
            //update
            return $this->update($userData, ['Username' => $email]) ;
        }
    }
    public function activateUserRole($email, $role) {
        return $this->setUserRole($email, $role, true) ;
    }
    public function deactivateUserRole($email, $role) {
        return $this->setUserRole($email, $role, false) ;
    }
    
    public function deactivateUserByContactId($contactId, $roleId) {
        $contacts = new Contacts() ;
        $c = $contacts->getById($contactId) ;
        
        $email = $c['email'] ;

        return $this->deactivateUserRole($email, $roleId) ;
    }
    public function activateUserByContactId($contactId) {
        $contacts = new Contacts() ;
        $c = $contacts->getById($contactId) ;
        
        $email = $c['email'] ;

        return $this->activateUser($email) ;
    }
    public function checkContactUserExists($contactId) {
        $contacts = new Contacts() ;
        $c = $contacts->getById($contactId) ;
        
        if( $c['email'] ) {
            return true ;
        }
        return false ;
    }
    /**
     * 
     * @param type $contactId
     * @param type $newUserRole
     * @param type $isFresh  1 : new login, 2: new email or new login type
     * @return type
     */
    public function upgradeUser($contactId, $newUserRole, &$isFresh = null) {
        
        $contacts = new Contacts() ;
        $c = $contacts->getById($contactId) ;

        
        $email = $c['email'] ;
		$fname = $c['fname'] ;
		$lname = $c['lname'] ;
        $agentpwd = GUtils::generatePassword(10) ;
        $token = GUtils::getUniqId() ;

        $userData = [];
        
        $isFresh = false ;
        $ua = $this->get(['contactid' => $contactId]) ;
        if( $ua['Username'] != null ) {
            //upgrade
            $userData[ 'Username'] = $email ;
            $userData[ 'EmailAddress'] = $email ;
            $userData[ 'token'] = $token ;
            $userData[ 'token_type'] = Users::$TOKEN_FORGOT_PASSWORD;
            $userData['Role_ID'] = intval($ua['Role_ID']) | $newUserRole  ;
            $this->update($userData, ['contactid' => $contactId]) ;
            $isFresh = 2 ;
        }
        else {
            $userData = [
                    'Username' => $email,
                    'fname' => $fname, 
                    'lname' => $lname,
                    'active' => 1,
                    'EmailAddress' => $email,
                    'Password' => hash( 'sha256', $agentpwd ),
                    'Algorithm' => 'SHA256',
                    'token' => $token,
                    'contactid' => $contactId,
                    'token_type' => Users::$TOKEN_CHANGE_PASSWORD
                ];
            //insert new user
            $userData['Role_ID'] = $newUserRole ;
            $this->insert($userData) ;
            $isFresh = 1 ;
        }
        
        return array (
            'password' => $agentpwd,
            'token' => $token,
            'email' => $email
        ) ;
    }
    
    public function roleIdToLabel($roleId) {

        switch( $roleId ) {
            case AclRole::$AGENT :
                return 'agent' ;
            case AclRole::$LEADER :
                return 'leader' ;
            case AclRole::$SUPPLIER :
                return 'supplier' ;
            case AclRole::$ADMIN :
                return 'admin' ;
            case AclRole::$CUSTOMER:
                return 'customer' ;
        }
        return '' ;
    }
    public static function explainRoles($roleId) {

        $str = '' ;
        if( $roleId & AclRole::$ADMIN ) {
            if( $str ) {
                $str .= ', ';
            }
            $str .= 'Admin' ;
        }
        if( $roleId & AclRole::$AGENT ) {
            if( $str ) {
                $str .= ', ';
            }
            $str .= 'Agent' ;
        }
        if( $roleId & AclRole::$LEADER ) {
            if( $str ) {
                $str .= ', ';
            }
            $str .= 'Tour Leader' ;
        }
        if( $roleId & AclRole::$SUPPLIER ) {
            if( $str ) {
                $str .= ', ';
            }
            $str .= 'Supplier' ;
        }
        if( $roleId & AclRole::$CUSTOMER ) {
            if( $str ) {
                $str .= ', ';
            }
            $str .= 'Customer' ;
        }
        return $str ;
    }
    public function labelToRoleId($label) {

        switch( $label ) {
            case 'agent' :
                return AclRole::$AGENT ;
            case 'leader' :
                return AclRole::$LEADER ;
            case 'supplier' :
                return AclRole::$SUPPLIER ;
            case 'admin' :
                return AclRole::$ADMIN ;
            case 'customer' :
                return AclRole::$CUSTOMER ;
        }
        return '' ;
    }
    
    public static function switchToRoleByLabel($label) {
        $roleId = self::labelToRoleId($label) ;
        return self::switchToRole($roleId) ;
    }
    public static function switchToRole($roleId) {
        
        $RoleSet = intval($_SESSION['Role_ID']) ;
        $roles = (new AclRole())->getList(" ID & $RoleSet " ) ;

        $LoginUserName = $_SESSION['login_user'] ;
        $roleSet = [] ;
        foreach( $roles as $role ) {
             $roleSet[] = $role['ID'] ;
        }
            
        if( $roleId == AclRole::$AGENT ) {

            if(in_array(AclRole::$AGENT, $roleSet) ) {
                self::switchToAgent($LoginUserName) ;
            }
        }
        else if( $roleId == AclRole::$CUSTOMER) {

            if(in_array(AclRole::$CUSTOMER, $roleSet) ) {
                self::switchToCustomer($LoginUserName) ;
            }
        }
        else if( $roleId == AclRole::$LEADER ) {

            if(in_array(AclRole::$LEADER, $roleSet) ) {
                self::switchToLeader($LoginUserName) ;
            }
        }
        else if( $roleId == AclRole::$SUPPLIER ) {

            if(in_array(AclRole::$SUPPLIER, $roleSet) ) {
                self::switchToSupplier($LoginUserName) ;
            }
        }
        else if( $roleId == AclRole::$ADMIN ) {

            if(in_array(AclRole::$ADMIN, $roleSet) ) {
                self::switchToAdmin($LoginUserName) ;
            }
        }
        
    }
    public static function switchToAgent($LoginUserName) {
        $_SESSION['Active_Role'] = AclRole::$AGENT ;
        $roleData = (new AclRole())->getById(AclRole::$AGENT) ;
        $_SESSION['Role_Flags'] = $roleData['Flags'] ;

        $agentObj = new Agents() ;
        $loginInfo = $agentObj->detail($LoginUserName);
        $contactId = $loginInfo['contactid'];
        $groups = $agentObj->groups($contactId);
        $tourIds = GUtils::implodeColumn($groups, ['tourid']);
        if( ! $tourIds ) {
            $tourIds = 'NULL' ;
        }
        $_SESSION['Group_ID'] = $tourIds;
        $_SESSION['User_ID'] = $contactId;        
    }
    public static function switchToLeader($LoginUserName) {
        $_SESSION['Active_Role'] = AclRole::$LEADER ;
        $roleData = (new AclRole())->getById(AclRole::$LEADER) ;
        $_SESSION['Role_Flags'] = $roleData['Flags'] ;

        $agentObj = new GroupLeaders() ;
        $loginInfo = $agentObj->detail($LoginUserName);
        $contactId = $loginInfo['contactid'];

        $groups = $agentObj->groups($contactId);
        $tourIds = GUtils::implodeColumn($groups, ['tourid']);
        if( ! $tourIds ) {
            $tourIds = 'NULL' ;
        }

        $_SESSION['Group_ID'] = $tourIds;
        $_SESSION['User_ID'] = $contactId;
    }
    public static function switchToSupplier($LoginUserName) {
        $_SESSION['Active_Role'] = AclRole::$SUPPLIER;
        $roleData = (new AclRole())->getById(AclRole::$SUPPLIER) ;
        $_SESSION['Role_Flags'] = $roleData['Flags'] ;

        $supplierObj = new Suppliers() ;
        $loginInfo = $supplierObj->detail($LoginUserName);
        $contactId = $loginInfo['contactid'];
        $_SESSION['Group_ID'] = '';
        $_SESSION['User_ID'] = $contactId;
        
    }
        public static function switchToCustomer($LoginUserName) {
        $_SESSION['Active_Role'] = AclRole::$CUSTOMER;
        $roleData = (new AclRole())->getById(AclRole::$SUPPLIER) ;
        $_SESSION['Role_Flags'] = $roleData['Flags'] ;

        $customerObj = new CustomerAccount() ;
        $loginInfo = $customerObj->detail($LoginUserName);
        $contactId = $loginInfo['contactid'];
        $_SESSION['Group_ID'] = '';
        $_SESSION['User_ID'] = $contactId;
        
    }
    public static function switchToAdmin($LoginUserName) {
        $_SESSION['Active_Role'] = AclRole::$ADMIN ;
        $_SESSION['Role_Flags'] = 0 ;

        $_SESSION['Group_ID'] = '';
        $_SESSION['User_ID'] = '';
    }
}
