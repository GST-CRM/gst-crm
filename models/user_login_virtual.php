<?php

class UserLogin extends Model {

    public function checkLogin($username, $password) {
        GDb::db() ;
        //Already sha256 
        $shaHash = hash('sha256', $password);
        $sql = "SELECT * FROM users WHERE Username = '$username' AND Password = '$shaHash' AND Algorithm='SHA256' ";            
        $record = GDb::fetchRow($sql) ;
        
        if( is_array($record)) {
			$this->markLoginTime($record['UserID']) ;
            return $record ;
        }
            
        //still md5 ?
        $md5hash = md5($password);
        $sqlMd5 = "SELECT * FROM users WHERE Username = '$username' AND Password = '$md5hash' AND Algorithm='MD5' ";
        $recordmd5 = GDb::fetchRow($sqlMd5) ;

        if(is_array($recordmd5 )) {
            if( $recordmd5['Algorithm'] == "MD5") {
                $this->autoUpgradeToSha256($recordmd5['UserID'], $password) ;
            }
            $this->markLoginTime($recordmd5['UserID']) ;
        }
        return $recordmd5;
    }
    private function autoUpgradeToSha256($userId, $password) {
        $shaHash = hash('sha256', $password);
        $sql = "UPDATE users SET Password='$shaHash', Algorithm='SHA256' WHERE UserID='$userId' LIMIT 1" ;
        GDb::execute($sql) ;
    }
    private function markLoginTime($userId) {
        //Add the login time for the current user
        $UpdateLoginTime = "UPDATE users SET Last_Login=NOW() WHERE UserID='$userId'  " ;
        GDb::execute($UpdateLoginTime);
    }
}
