<?php

include_once __DIR__ . '/model.php';

class Groups extends Model {

    public $table = 'groups';

    public function getMemberSummary($groups) {
        if (!trim($groups)) {
            return array(
                'customers' => 0,
                'cancelled' => 0,
                );
        }
        $sql = "SELECT 
                    SUM(IF(ci.Status=4 OR ci.Status=5,1,0)) AS cancelled,
                    SUM(1) AS customers 
                FROM customer_account ca
                LEFT JOIN ( SELECT * FROM customer_invoice ci WHERE ci.status=4 OR ci.status=5 GROUP BY Customer_Account_Customer_ID ) AS ci 
                    ON ca.contactid=ci.Customer_Account_Customer_ID AND ci.Group_ID=ca.tourid 
                INNER JOIN `groups` AS g ON g.tourid=ca.tourid
                WHERE ca.tourid IN($groups) AND g.status=1";
        $row = $this->executeOne($sql);

        return array(
            'customers' => intval($row['customers']),
            'cancelled' => intval($row['cancelled']),
            );
    }

    public function tripPrice($groupId) {
        $sql = "SELECT price FROM products p
                INNER JOIN `groups` g ON g.land_productid = p.id WHERE g.tourid=$groupId ";

        $landPrice = GDb::fetchScalar($sql);
        $sqla      = "SELECT price FROM products p
                INNER JOIN `groups` g ON g.airline_productid = p.id WHERE g.tourid=$groupId";

        $airlinePrice = GDb::fetchScalar($sqla);

        return floatval($landPrice + $airlinePrice);
    }

    public function getAgentsNameString($agentIdsIn) {
        $agentname = '' ;
        if (strlen($agentIdsIn) > 0) {
            
            $sqlga    = "SELECT c.fname,c.mname,c.lname FROM contacts c WHERE c.id IN($agentIdsIn)";
            $groupset = GDb::fetchRowSet($sqlga);

            foreach ($groupset as $one) {
                if ($agentname) {
                    $agentname .= ', ';
                }
                $agentname .= $one['fname'] . ' ' . $one['lname'];
            }
        }
        return $agentname ;
    }
    
    public function getGroupAgentsNameList($groupId) {
        $sqlga    = "SELECT ga.id, ga.agent_id, ga.commissiontype, ga.commissionvalue, c.fname,c.mname,c.lname, c.email FROM group_agents ga
                    INNER JOIN contacts c ON c.id=ga.agent_id AND ga.group_id=$groupId ";
        return GDb::fetchRowSet($sqlga);
    }
    public function getGroupAgentsNames($groupId) {
        $name = [] ;
        $agents = $this->getGroupAgentsNameList($groupId) ;
        if(is_array($agents)) {
            foreach( $agents as $agent ) {
                $name[] = $agent['fname'] . ' ' . $agent['mname'] . ' ' . $agent['lname'] ;
            }
        }
        return $name ;
    }
    public function getGroupAgentsString($groupId, $glue = ',') {
        $names = $this->getGroupAgentsNames($groupId) ;
        return implode($glue, $names) ;
    }
    public function getGroupLeadersDetail($groupId) {
        $sqlga    = "SELECT c.* FROM contacts c WHERE id IN (SELECT groupleader FROM `groups` WHERE tourid='$groupId')" ;
        return GDb::fetchRowSet($sqlga);
    }
    public function getGroupLeadersNames($groupId) {
        $name = [] ;
        $leaders = $this->getGroupLeadersDetail($groupId) ;
        if(is_array($leaders)) {
            foreach( $leaders as $leader ) {
                $name[] = $leader['fname'] . ' ' . $leader['mname'] . ' ' . $leader['lname'] ;
            }
        }
        return $name ;
    }
    public function getGroupLeadersNameString($groupId, $glue = ',') {
        $names = $this->getGroupLeadersNames($groupId) ;
        return implode($glue, $names) ;
    }
    
    public function getRoomingList($groupId, $contactId= null) {
        $sql = $this->getRoomingListQuery($groupId, $contactId) ;
        return GDb::fetchRowSet($sql);
    }
    public function getRoomingListQuery($groupId, $contactId= null) {
        $contactCondition = '' ;
        if( $contactId ) {
            $contactCondition = " AND ca.contactid=$contactId " ;
        }
         return "(SELECT
                        co.id,
                        co.title,
                        co.fname,
                        co.mname,
                        co.lname,
                        IF(ca.roomtype = NULL OR ca.roomtype = 0, 'Not Assigned', ca.roomtype) AS roomtype,
                        NULL AS Roommate1ID,
                        NULL AS Roommate1,
                        NULL AS Roommate2ID,
                        NULL AS Roommate2,
						if(ca.Transfer=1, (SELECT description FROM products WHERE id=ca.Transfer_Product_ID) ,'No') AS Need_Transfer,
						if(ca.Extension=1, (SELECT description FROM products WHERE id=ca.Extension_Product_ID) ,'No') AS Need_Extension,
                        ca.special_land_text,
                        ca.Meals_Allergies,
                        ca.Meals_Request,
                        ca.Allergies_Request,
                        ca.status
                    FROM contacts co
                    JOIN customer_account ca 
                        ON ca.contactid=co.id AND ca.tourid=$groupId
                    WHERE
                        /* Khader: I have enabled this line again, Nithin or Kiran, if you have a reason why you want to disable it, please tell me asap */
						/*ca.status=1 AND */
                        ca.tourid=$groupId
                            $contactCondition
                        AND NOT EXISTS (SELECT *
                                        FROM customer_groups
                                        WHERE Type='Rooming'
                                        AND co.id IN(Primary_Customer_ID,Additional_Traveler_ID_1,Additional_Traveler_ID_2)
                                       )
                    )
                    UNION
                    (SELECT
                        co.id,
                        co.title,
                        co.fname,
                        co.mname,
                        co.lname,
                        ca.roomtype,
                        cg.Additional_Traveler_ID_1 AS Roommate1ID,
                        (SELECT CONCAT(title,' ',fname,' ',mname,' ',lname) FROM contacts WHERE id=cg.Additional_Traveler_ID_1) AS Roommate1,
                        cg.Additional_Traveler_ID_2 AS Roommate2ID,
						(SELECT CONCAT(title,' ',fname,' ',mname,' ',lname) FROM contacts WHERE id=cg.Additional_Traveler_ID_2) AS Roommate2,
						if(ca.Transfer=1, (SELECT description FROM products WHERE id=ca.Transfer_Product_ID) ,'No') AS Need_Transfer,
						if(ca.Extension=1, (SELECT description FROM products WHERE id=ca.Extension_Product_ID) ,'No') AS Need_Extension,
						ca.special_land_text,
                        ca.Meals_Allergies,
                        ca.Meals_Request,
                        ca.Allergies_Request,
                        ca.status
                    FROM contacts co 
                    JOIN customer_account ca 
                        ON ca.contactid=co.id AND ca.tourid=$groupId
                    JOIN customer_groups cg
                        ON co.id=cg.Primary_Customer_ID AND cg.Type='Rooming' AND cg.Group_ID=$groupId
					WHERE
                     	1 /*-- ca.status=1*/
                        $contactCondition
                    )ORDER BY status DESC "; /* ca.status=1 : is removed to list canceled customers with red mark in both side of union query. */
    }
    
    public function getActiveTrips($where, $orderby, $limit) {
        $sql = "select status,tourid,airline_productid,startdate,tourname,enddate from groups WHERE status=1 AND startdate >= CURDATE() $where $orderby $limit"; 
        return GDb::fetchRowSet($sql) ;
    }
    public function getActiveTripCount($where, $orderby) {
        $sql = "select COUNT(*) AS total from groups WHERE status=1 AND startdate >= CURDATE() $where $orderby"; 
        return GDb::fetchScalar($sql) ;
    }
    public function getActiveTours($where) {
        $sql = "select startdate, enddate, tourid, tourname FROM groups WHERE $where "; 
        return GDb::fetchRowSet($sql) ;
    }

    public function getDueDate($tourId) {
        $sqlt = "SELECT DATE_SUB(startdate, INTERVAL 90 DAY) as due_date FROM groups WHERE tourid='$tourId'" ;
        return GDb::fetchScalar($sqlt) ;
    }
    
    public function invoiceTripDetails($invoiceId) {
            $tripref = "SELECT ci.Group_ID, if(ci.Group_ID=999,'There is no group for this invoice',g.tourname) AS tourname, ci.Customer_Account_Customer_ID FROM customer_invoice AS ci
                        LEFT JOIN `groups` AS g ON g.tourid=ci.Group_ID WHERE ci.Customer_Invoice_Num='$invoiceId' ";
        return GDb::fetchRowSet($tripref);
    }
    
    public function invoicesDetailsPerCustomer($customerId) {
            $InvoicesList = "SELECT ci.Group_ID, if(ci.Group_ID=999,'There is no group for this invoice',g.tourname) AS tourname, ci.Customer_Account_Customer_ID FROM customer_invoice AS ci
                        LEFT JOIN `groups` AS g ON g.tourid=ci.Group_ID WHERE ci.Customer_Account_Customer_ID='$customerId' ";
        return GDb::fetchRowSet($InvoicesList);
    }
    
    public function customerTripDetails($customerId) {
            $tripref = "SELECT 
                CONCAT( t1.fname, ' ', t1.mname, ' ', t1.lname ) AS travellingwith1name,
                CONCAT( t2.fname, ' ', t2.mname, ' ', t2.lname ) AS travellingwith2name,                
                ca.roomtype, g.tourid, g.tourname, g.destination, g.startdate, g.enddate, g.listprice FROM customer_account AS ca 
                LEFT JOIN `groups` AS g ON g.tourid=ca.tourid 
                LEFT JOIN `contacts` AS t1 ON ca.travelingwith=t1.id
                LEFT JOIN `contacts` AS t2 ON ca.travelingwith2=t2.id
                WHERE ca.contactid='$customerId' ";

        return GDb::fetchRowSet($tripref);
    }
    public function customerTripDetail($customerId, $tripId) {
            $tripref = "SELECT 
                CONCAT( t1.fname, ' ', t1.mname, ' ', t1.lname ) AS travellingwith1name,
                CONCAT( t2.fname, ' ', t2.mname, ' ', t2.lname ) AS travellingwith2name,                
                ca.roomtype, g.tourid, g.tourname, g.destination, g.startdate, g.enddate, g.listprice FROM customer_account AS ca 
                LEFT JOIN `groups` AS g ON g.tourid=ca.tourid 
                LEFT JOIN `contacts` AS t1 ON ca.travelingwith=t1.id
                LEFT JOIN `contacts` AS t2 ON ca.travelingwith2=t2.id
                WHERE ca.contactid='$customerId' AND g.tourid='$tripId' ";

        return GDb::fetchRow($tripref);
    }
    public function airlineSupplierEmail($groupId) {
        $sql = "SELECT DISTINCT c.email FROM `groups` g
                INNER JOIN products p ON p.id = g.airline_productid
                INNER JOIN contacts c ON c.id = p.supplierid 
                where g.tourid='$groupId'" ;
        
        return GDb::fetchScalar($sql) ;
    }
    public function staffEmail($groupId) {
        $sql = "SELECT DISTINCT EmailAddress FROM `groups` g
                INNER JOIN users u ON u.UserId = g.Staff_ID
                where g.tourid='$groupId'" ;
        
        return GDb::fetchScalar($sql) ;
    }
    public function getUserCountByStatus($tripId) {
        $sql = "SELECT SUM( IF(status=0,1,0) ) AS inactive, SUM( IF(status=1,1,0) ) AS active FROM 
            customer_account ca WHERE ca.tourid='$tripId' " ;
        return GDb::fetchRow($sql) ;        
    }
}
