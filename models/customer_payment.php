<?php

include_once __DIR__ . '/model.php';
include_once __DIR__ . '/acl_permission.php';
include_once __DIR__ . '/acl_role.php';
include_once __DIR__ . '/customer_groups.php';
include_once __DIR__ . '/customer_invoice.php';
include_once __DIR__ . '/../models/customer_payment.php';

class CustomerPayment extends Model {

    public $table = 'customer_payments';

    /**
     * The invoice query.
     * 
     * @param type $condition
     * @param type $groupby
     * @param type $filter
     * @param type $orderbyPart
     * @param type $limitPart
     * @param type $accountStatus
     * @return type
     */
    public function query($condition, $groupby = '', $filter = '', $orderbyPart = '', $limitPart = '', $accountStatus = '', $fromInvoice = true) {
        $invoiceToContact = 'LEFT';
        if ($fromInvoice == false) {
            $invoiceToContact = 'RIGHT';
        }
        return "
        SELECT 
                ci.Customer_Invoice_Num,
                ci.Status AS Invoice_Status,
                ci.Additional_Invoice,
                ci.Invoice_Amount,
                ci.Due_Date,
                ci.Add_Date,
                ci.Invoice_Amount,
                ci.Comments,
                ci.Invoice_Sent,
				ca.createtime,
                ci.Attachment,
                DATE_SUB(ci.Due_Date, INTERVAL 90 DAY) as second_due,
                SUM(cp.Charges) AS charges,
                /* cp.Customer_Payment_Comments, This will not work ! */
                SUM(cn.Credit_Amount) AS Credit_Amount,
                cr.Refund_ID,
                cr.Cancellation_Charge,
                cr.Cancellation_Charge_Others,
                cr.Refund_Amount,
                cr.Active_Refund,
                cr.Refund_Status AS Refund_Status,
                cr.Pending_Refund_Amount,
                g.agent,
                g.Church_Name,
                if(ci.Group_ID=999,'There is no group for this invoice',g.tourname) AS tourname,
                g.tourdesc,
				g.startdate,
				g.enddate,
                g.2nd_Payment_Amount,
                DATEDIFF(Due_Date, NOW()) AS due_diff,
                DATEDIFF(g.startdate, NOW()) AS trip_start_days,
                DATEDIFF(g.startdate, NOW()) as days_left,
                DATEDIFF(c.expirydate, g.enddate) AS expire_diff,
                SUM(IFNULL(cp.Customer_Payment_Amount, 0)) AS total,
                SUM(IFNULL(cp.Customer_Payment_Amount, 0)) AS paid,
                c.passport,
                c.expirydate AS Passport_Expiry_Date,
                ca.tourid,
                ca.contactid,
                ca.status AS Account_Status,
                ca.contactid AS id,
                ca.invoiced_customer,
                ca.groupinvoice,
                ca.hisownticket,
                ca.insurance,
                ca.upgrade,
                ca.upgradeproduct,
                ca.upgrade_land_product,
                ca.Transfer,
                ca.Transfer_Product_ID,
                ca.Extension,
                ca.Extension_Product_ID,
                ca.roomtype,
                ca.travelingwith,
                ca.travelingwith2,
                cn.Full_Discount AS TourLeaderDiscount,
                so.Sales_Order_Num,
                c.title,
                c.email,
                c.fname,
                c.mname,
                c.lname,
                c.address1,
                c.address2,
                c.zipcode,
                c.city,
                c.state,
                c.country,
                cr.Add_Date as Refund_Date,
                cr.Cancellation_Outcome,
                DATE(cr.Cancelled_On) as Canceled_On,
				UNIX_TIMESTAMP(ci.Mod_Date) AS Last_Update
         FROM customer_invoice AS ci
         $invoiceToContact JOIN customer_account AS ca ON ca.contactid = ci.Customer_Account_Customer_ID $accountStatus
                AND ca.tourid=ci.Group_ID /* AND ca.status=1 */

         LEFT JOIN ( SELECT Sales_Order_Num, Customer_Account_Customer_ID, Group_ID FROM sales_order) AS so ON so.Customer_Account_Customer_ID=ca.contactid AND so.Group_ID=ci.Group_ID
         LEFT JOIN contacts AS c ON c.id = ci.Customer_Account_Customer_ID
         LEFT JOIN `groups` g ON g.tourid=ca.tourid /* AND g.status=1 */

         LEFT JOIN
           (SELECT cp.Customer_Payment_Comments,
                   cp.Customer_Payment_ID,
                   cp.Customer_Account_Customer_ID,
                   SUM(cp.Charges) AS Charges,
                   cp.Customer_Invoice_Num,
                   cp.status,
                   SUM(cp.Customer_Payment_Amount) AS Customer_Payment_Amount
            FROM customer_payments AS cp
            WHERE cp.status=1
            GROUP BY cp.Customer_Invoice_Num) AS cp ON ci.Customer_Invoice_Num=cp.Customer_Invoice_Num
            

         LEFT JOIN
           (SELECT 
                   Refund_ID,
                   Add_Date,
                   Cancelled_On,
                   Cancellation_Outcome, 
                   Customer_Invoice_Num,
                   Payee_Invoice_Num,
                   Customer_Account_Customer_ID,
                   SUM(Refund_Amount) AS Refund_Amount,
                   SUM(Active_Refund) AS Active_Refund,
                   SUM(Cancellation_Charge) AS Cancellation_Charge,
                             SUM(IF( Payee_Invoice_Num = Customer_Invoice_Num OR Payee_Invoice_Num IS NULL OR Payee_Invoice_Num = 0, 0, Cancellation_Charge) ) AS Cancellation_Charge_Others,
                   SUM(r.Refund_Amount - r.Issued_Amount) AS Pending_Refund_Amount,
                   r.Status AS Refund_Status
            FROM customer_refund r
            GROUP BY Customer_Account_Customer_ID) AS cr ON 
                cr.Customer_Account_Customer_ID = ci.Customer_Account_Customer_ID 
                AND ( cr.Customer_Invoice_Num = ci.Customer_Invoice_Num 
                        OR 
                     cr.Payee_Invoice_Num = ci.Customer_Invoice_Num
                    )

         LEFT JOIN
           (SELECT cn.Customer_Account_Customer_ID,
                   cn.Customer_Invoice_Num,
                   cn.Status,
                   cn.Full_Discount,
                   SUM(cn.Credit_Amount) AS Credit_Amount
            FROM customer_credit_note cn
            WHERE cn.Status=1
            GROUP BY cn.Customer_Invoice_Num) AS cn ON cn.Customer_Invoice_Num = ci.Customer_Invoice_Num

         WHERE 1
         /* ci.invoice_amount != 0 */
         $condition
         $groupby $filter $orderbyPart $limitPart
          ";
        
    }

    // <editor-fold defaultstate="collapsed" desc="Core functions">

    /**
     * Process single row.
     * 
     * @param type $data
     * @return type
     */
    private function processInvoiceListingOne($data) {
        $output = $this->processInvoiceListing([$data]);

        if (is_array($output)) {
            return reset($output);
        }
        return [];
    }

    /**
     * Process a set of rows.
     * 
     * @param type $data
     * @return int
     */
    private function processInvoiceListing($data) {
        foreach ($data as $k => $pax) {

            /*
             * Invoice_Amount_Reverted : For showing -$1000 in pdf.
             * balance : Balance left for customers.
             */

            //Normal case, no cancellation
            $InvoiceTotalAmount                  = $pax['Invoice_Amount'];
            $charges                             = $pax["charges"];
            $totalPaymentAmount                  = $pax["total"];
            $balance                             = ($InvoiceTotalAmount + $charges - $pax["Credit_Amount"] ) - $pax["total"];
            $data[$k]['Invoice_Amount_Reverted'] = 0;
            $data[$k]['Invoice_Amount_Revised_V2'] = $pax['Invoice_Amount'];

            //There is no such scenario, Payee reufnd amount. The primary alwasys pays the amount, and refund goes to him. 
            //but not the cancellation charge its added to additional customers
//            if (floatval($pax['Payee_Refund_Amount']) > 0) {
//                $balance -= $pax['Payee_Pending_Refund_Amount'];
//            } else 
            

            //Cancellation possible only if not a primary customer.
            if ($pax['Invoice_Status'] == CustomerInvoice::$CANCELLED) {
                //Just canceled. It is equal to $pax['Refund_Status'] == 1
                $data[$k]['Invoice_Amount_Reverted'] = 0 - $InvoiceTotalAmount;
                $InvoiceTotalAmount                  = 0;
                $balance                             = $pax['Cancellation_Charge'] - $pax["total"];
                $data[$k]['Invoice_Amount_Revised_V2'] = $pax['Cancellation_Charge'] ;

                if ($pax['Refund_Status'] == 2) {
                    //Refund issued.
                    $InvoiceTotalAmount = 0;
                    $balance            = 0;
                    $totalPaymentAmount = $pax['Cancellation_Charge'];
                } else if ($pax['Refund_Status'] == 1 || $pax['Refund_Status'] == 3) {
                    //Has refund amount, but not given.
                    $InvoiceTotalAmount = 0;
                    $balance            = 0 - $pax['Pending_Refund_Amount'];
                    $totalPaymentAmount = $pax['Cancellation_Charge'] + $pax['Pending_Refund_Amount'];
                } else if ($pax['Refund_Status'] == 1) {
                    //No refund amount; Eithor canceleation charge covers payment amount or payment amount is less than cancelation charge( this never happens as per tony )
                    $InvoiceTotalAmount = 0;
                    $balance            = $pax['Cancellation_Charge'] - $pax['total'];
                    $totalPaymentAmount = $pax['total'];
                }
            }
            
            if ($pax['Cancellation_Charge_Others'] > 0) {
                $InvoiceTotalAmount += 0;
                $balance            += $pax['Cancellation_Charge_Others'];
                $data[$k]['Invoice_Amount_Revised_V2'] += $pax['Cancellation_Charge_Others'];
            }
            
            if ($pax['Active_Refund'] != 0 && ($pax['Invoice_Status'] != CustomerInvoice::$CANCELLED) ) {
                $totalPaymentAmount -= $pax['Active_Refund'] ;
                $balance += $pax['Active_Refund'] ;
            }
            //Store back to original values.
            $data[$k]['payment_amount']         = $totalPaymentAmount;
            $data[$k]['balance']                = $balance;
            $data[$k]['Invoice_Amount_Revised'] = $InvoiceTotalAmount;

            if ($pax['Additional_Invoice']) {
                $data[$k]['balance']                = 0;
                $data[$k]['Invoice_Amount_Revised'] = 0;
            }
        }
        return $data;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Invoice Details">
    /*
     * All about an invoice
     */
    public function customerInvoiceDetails($invoiceId) {

        $sql = $this->customerInvoiceDetailsQuery($invoiceId);
                
        $row = GDb::fetchRow($sql);

        return $this->processInvoiceListingOne($row);
    }

    /**
     * All about an invoice
     * 
     * @param type $invoiceId
     * @return type
     */
    public function customerInvoiceDetailsQuery($invoiceId) {
        $condition = " AND ci.Customer_Invoice_Num = $invoiceId ";
        $groupby   = "GROUP BY ci.Customer_Invoice_Num";
        return $this->query($condition, $groupby);
    }

    /**
     * Alias for customer invoice details.
     * 
     * @param type $invoiceId
     * @return type
     */
    public function invoiceDetails($invoiceId) {

        return $this->customerInvoiceDetails($invoiceId);
    }

    /**
     * Alias for customer invoice details.
     * 
     * @param type $invoiceId
     * @return type
     */
    public function invoiceDetailsQuery($invoiceId) {
        return $this->customerInvoiceDetailsQuery($invoiceId);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Payment Details">
    /**
     * All about a customer & tourid.
     * 
     * @param type $contactId
     * @param type $GroupID
     * @return type
     */
    public function customerPaymentDetails($contactId, $GroupID) {
        $sql = $this->customerPaymentDetailsQuery($contactId, $GroupID);
        $row = GDb::fetchRow($sql);

        return $this->processInvoiceListingOne($row);
    }

    /**
     * All about a customer & tourid.
     * 
     * @param type $contactIds
     * @param type $GroupID
     * @return type
     */
    public function customerPaymentDetailsQuery($contactIds, $GroupID) {

        if (is_array($contactIds)) {
            $contactIds = GUtils::implodeIfValue($contactIds, ',');
            $condition  = " AND ci.Customer_Account_Customer_ID IN($contactIds)  AND ci.Group_ID=$GroupID ";
        } else {
            $condition = " AND ci.Customer_Account_Customer_ID = $contactIds  AND ci.Group_ID=$GroupID ";
        }
        $groupby = "GROUP BY ci.Customer_Invoice_Num";
        return $this->query($condition, $groupby);
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Pax Details">
    /**
     * All pax in a group
     * 
     * @param type $tourid
     * @param type $filter
     * @param type $orderbyPart
     * @param type $limitPart
     * @return type
     */
    public function paxListing($tourid, $filter = "", $orderbyPart = "", $limitPart = "") {
        $sql = $this->paxListingQuery($tourid, $filter, $orderbyPart, $limitPart);

        $data = GDb::fetchRowSet($sql);

        return $this->processInvoiceListing($data);
    }

    /**
     * All pax in a group
     * 
     * @param type $tourid
     * @param type $filter
     * @param string $orderbyPart
     * @param type $limitPart
     */
    public function paxListingQuery($tourid, $filter = "", $orderbyPart = "", $limitPart = "") {

        if (!$orderbyPart) {
            $orderbyPart = " ORDER BY ci.Customer_Invoice_Num DESC ";
        }

        //TODO; No need to distribute credit amout here, since indigual payment amount not used here.
        $condition = "AND ca.tourid =$tourid ";
        $groupby   = " GROUP BY ca.contactid ";

        return $this->query($condition, $groupby, $filter, $orderbyPart, $limitPart, '', false);
    }

// </editor-fold>

    /**
     * Customer invoice summary
     * 
     * @param type $whereCondition
     * @param type $filter
     * @param type $orderbyPart
     * @param type $limitPart
     * @return type
     */
    public function customerSummary( $customerId ) {

        $whereCondition = " AND contactid=$customerId " ;
        return $this->paymentSummary($whereCondition) ;
    }
    /**
     * Customer invoice listing
     * 
     * @param type $customerId
     * @return type
     */
    public function customerListing( $customerId ) {

        $whereCondition = " AND contactid=$customerId " ;
        return $this->paymentListing($whereCondition, "") ;
    }
    /**
     * Payment listing page
     * 
     * @param type $whereCondition
     * @param type $filter
     * @param type $orderbyPart
     * @param type $limitPart
     * @return type
     */
    public function paymentListing($whereCondition, $filter, $orderbyPart = null, $limitPart = 0) {

        $sql  = $this->paymentListingQuery($whereCondition, $filter, $orderbyPart, $limitPart);

        $data = GDb::fetchRowSet($sql);
        return $this->processInvoiceListing($data);
    }
    /**
     * Customer billing status
     * 
     * @param type $whereCondition
     * @param type $filter
     * @param type $orderbyPart
     * @param type $limitPart
     * @return type
     */
    public function CustomerBillingStatus($invoiceIds) {

        $invoiceIdString = implode(',', $invoiceIds) ;
        $whereCondition = " AND ci.Customer_Invoice_Num IN($invoiceIdString)" ;
        $sql  = $this->paymentListingQuery($whereCondition, "");

        $data = GDb::fetchRowSet($sql);
        $billingStatus = $this->processInvoiceListing($data);

        $due = 0 ;
        $paid = 0 ;
        $remaining = 0 ;
        foreach( $billingStatus as $row ) {
            $paid += $row['payment_amount'] ;
            $remaining += $row['balance'] ;
            $due += $row['Invoice_Amount_Revised_V2'] ;
        }
        
        return [
            'owed' => $due,
            'paid' => $paid,
            'remaining' => $remaining,
        ];
    }

    /**
     * Payment listing
     * @param type $whereCondition
     * @param type $filter
     * @param string $orderbyPart
     * @param string $limitPart
     * @param type $accountStatus
     * @return type
     */
    public function paymentListingQuery($whereCondition, $filter, $orderbyPart = null, $limitPart = null, $accountStatus = 1) {

        if (!$orderbyPart) {
            $orderbyPart = " ORDER BY ci.Customer_Invoice_Num DESC ";
        }
        if (!$limitPart) {
            $limitPart = '';
        }

        $status = " ca.status=1 AND ";
        if ($accountStatus == 0) {
            $status = " ca.status=0 AND ";
        } else {
            $status = " ";
        }
        $condition = $whereCondition;
        $groupby   = ' GROUP BY ci.Customer_Invoice_Num ';
        return $this->query($condition, $groupby, $filter, $orderbyPart, $limitPart, $status);
    }

    /**
     * Group listing
     * 
     * @param type $whereCondition
     * @param type $filter
     * @param type $orderbyPart
     * @param type $limitPart
     * @return type
     */
    public function groupListing($whereCondition, $filter, $orderbyPart = null, $limitPart = 0) {

        $sql = $this->groupListingQuery($whereCondition, $filter, $orderbyPart, $limitPart);

        $data = GDb::fetchRowSet($sql);

        return $this->processInvoiceListing($data);
    }

    /**
     * Group listing
     * 
     * @param type $whereCondition
     * @param type $filter
     * @param string $orderbyPart
     * @param string $limitPart
     * @param type $accountStatus
     * @return type
     */
    public function groupListingQuery($whereCondition, $filter, $orderbyPart = null, $limitPart = null, $accountStatus = 1) {

        if (!$orderbyPart) {
            $orderbyPart = " ORDER BY ci.Customer_Invoice_Num DESC ";
        }
        if (!$limitPart) {
            $limitPart = '';
        }

        $status = " ca.status=1 AND ";
        if ($accountStatus == 0) {
            $status = " ca.status=0 AND ";
        } else {
            $status = " ";
        }
        $groupByPart = " GROUP BY ci.Customer_Invoice_Num ";
        //No need to distribute credit amout here, since indigual payment amount not used here.
        return $this->query($whereCondition, $groupByPart, $filter, $orderbyPart, $limitPart, $status);
    }

    /**
     * Payment listing count.
     * 
     * @param type $whereCondition
     * @param type $filter
     * @return type
     */
    public function paymentListingCountQuery($whereCondition, $filter) {
        $sql = "SELECT COUNT(*) as total FROM customer_invoice AS ci 
            LEFT JOIN customer_account AS ca ON ca.contactid = ci.Customer_Account_Customer_ID AND ca.status=1 AND ci.Group_ID=ca.tourid 
                WHERE ci.Status IN (1,4) AND ci.invoice_amount != 0 
                $whereCondition 
                $filter ;";

        return $sql;
    }

    //TODO consider additional customer and remove this function
    public function paymentDetailListing($where, $groupby, $primaryCustomerId, $tripId) {

        $cg           = new CustomerGroups();
        $allIds       = $cg->getAdditionalCustomerIds($tripId, $primaryCustomerId);
        $numCustomers = count($allIds);
        if ($numCustomers < 1) {
            $numCustomers = 1;
        }

        $sql = "SELECT SUM(cp.Charges) AS charges, SUM(IFNULL(cp.Customer_Payment_Amount,0)) AS payments, cp.Customer_Payment_Comments, c.fname, c.lname, c.mname, ca.contactid, tourname,
            ci.Customer_Invoice_Num, ci.Additional_Invoice, ci.Due_Date, ci.Invoice_Amount, ((cn.Credit_Amount)/$numCustomers) AS Credit_Amount FROM customer_invoice AS ci
        LEFT JOIN customer_account AS ca ON ca.contactid = ci.Customer_Account_Customer_ID AND ca.status=1 AND ci.Group_ID=ca.tourid
        LEFT JOIN contacts AS c ON c.id = ca.contactid
        LEFT JOIN groups AS g ON g.tourid=ci.Group_ID
        LEFT JOIN (
                SELECT cp.Customer_Payment_Comments , cp.Customer_Payment_ID, cp.Customer_Account_Customer_ID, (cp.Charges) AS Charges, cp.Customer_Invoice_Num, cp.status, (cp.Customer_Payment_Amount) AS Customer_Payment_Amount FROM customer_payments AS cp WHERE cp.status=1 
                ) AS  cp ON ci.Customer_Invoice_Num=cp.Customer_Invoice_Num 
        LEFT JOIN ( 
                SELECT cn.Customer_Account_Customer_ID, cn.Customer_Invoice_Num, cn.Status, SUM(cn.Credit_Amount) AS Credit_Amount FROM customer_credit_note cn WHERE cn.Status=1 GROUP BY cn.Customer_Invoice_Num
                ) AS cn ON cn.Customer_Invoice_Num = ci.Customer_Invoice_Num 
        WHERE ci.Status = 1 $where $groupby ";
        return GDb::fetchRowSet($sql);
    }

    public function paymentTotal($invoiceId) {
        $sql = "SELECT Sum(cp.Customer_Payment_Amount) AS total FROM customer_payments AS cp WHERE cp.Customer_Invoice_Num='$invoiceId' AND cp.Status=1";
        return GDb::fetchScalar($sql);
    }

    public function paymentSummary($whereCondition) {

        //TODO; need to rewrite
        // { Acl Condition
        $GroupID               = AclPermission::userGroup();
        $ACL_CONDITION_SUMMARY = '';
        if (AclPermission::roleFlags() & AclRole::$FLAG_ONLY_GROUP) {
            $ACL_CONDITION_SUMMARY = " AND ca.tourid IN($GroupID) ";
        }
        // }

        /*
         * when ci.Status = 5 ( deliquent : status value not decided yet may be 5). So coding !=4 for getting records with 5.
         * SUM(IF( ci.Status != 4 AND cr.Status = 1, Cancellation_Charge ,0)) AS Cancellation,  
         */

        //Query Due and Payment Summary {
        $sqlsummary = "SELECT SUM(CancellationPaid) AS CancellationPaid,    
        SUM(RefundPaid) AS RefundPaid,    
        SUM(RefundUnPaid) AS RefundUnPaid,    
        SUM(AmountNotYetDueCharges) AS AmountNotYetDueCharges, 
        SUM(AmountInDueCharges) AS AmountInDueCharges,
        SUM(AmountNotYetDueCredit) AS AmountNotYetDueCredit, 
        SUM(AmountInDueCredit) AS AmountInDueCredit,

                SUM(AmountInDuePayee_Refund) AS AmountInDuePayee_Refund, 
                SUM(AmountInDueCancellation_Charge) AS AmountInDueCancellation_Charge, 
                SUM(AmountInDuePending_Refund_Amount) AS AmountInDuePending_Refund_Amount, 

        SUM(IFNULL(NotYetDue,0)-IFNULL(AmountNotYetDue,0)) AS NotYetDue, 
        SUM(IFNULL(InDue,0)-IFNULL(AmountInDue,0)) AS InDue, 
        SUM(IFNULL(Paid,0)) AS Paid FROM (

            SELECT (IF(Due_Date > CURDATE() AND cr.Status IS NULL, Invoice_Amount, 0)) AS NotYetDue,
                   (IF(Due_Date <= CURDATE() AND cr.Status IS NULL, Invoice_Amount, 0)) AS InDue,
                   SUM(IF(Due_Date > CURDATE() AND cr.Status IS NULL, Customer_Payment_Amount, 0)) AS AmountNotYetDue,
                   SUM(IF(Due_Date <= CURDATE() AND cr.Status IS NULL, Customer_Payment_Amount, 0)) AS AmountInDue,

                   SUM(IF(Due_Date > CURDATE() AND cr.Status IS NULL, Charges, 0)) AS AmountNotYetDueCharges,
                   SUM(IF(Due_Date <= CURDATE() AND cr.Status IS NULL, Charges, 0)) AS AmountInDueCharges,
                   SUM(IF(Due_Date > CURDATE() AND ci.Status = 1 AND cn.Status = 1, Credit_Amount, 0)) AS AmountNotYetDueCredit, 
                   SUM(IF(Due_Date <= CURDATE()  AND ci.Status = 1 AND cn.Status = 1, Credit_Amount, 0)) AS AmountInDueCredit,

                   SUM(IF(ci.Status = 1 , cr2.Payee_Refund_Amount, 0)) AS AmountInDuePayee_Refund, 
                   SUM(IF(ci.Status = 1 , cr2.Payee_Cancellation_Charge, 0)) AS AmountInDueCancellation_Charge, 
                   SUM(IF(ci.Status = 1 , cr2.Payee_Pending_Refund_Amount, 0)) AS AmountInDuePending_Refund_Amount, 
                   
                   cr.CancellationPaid,
                   cr.RefundPaid,
                   cr.RefundUnPaid,
                   cr2.Payee_Refund_Amount,
                   cr2.Payee_Cancellation_Charge,
                   cr2.Payee_Pending_Refund_Amount,
                   SUM(IF(cr.Status IS NULL, Customer_Payment_Amount, 0)) AS Paid, 
                   ci.Additional_Invoice,
                   SUM(IF(ci.Additional_Invoice IS NOT NULL AND ci.Additional_Invoice != 0, 1, 0)) AS SelfCanceled
            FROM customer_invoice ci

            LEFT JOIN
              (SELECT SUM(cp.Charges) AS Charges,
                      cp.Customer_Invoice_Num,
                      SUM(cp.Customer_Payment_Amount) AS Customer_Payment_Amount
               FROM customer_payments cp
               WHERE cp.Status = 1
               GROUP BY Customer_Invoice_Num) AS cp ON cp.Customer_Invoice_Num = ci.Customer_Invoice_Num

            LEFT JOIN 
              (SELECT 
                      SUM(IF(r.Status != 0, Cancellation_Charge, 0)) AS CancellationPaid,
                      SUM(IF(r.Status = 2 OR r.Status = 3, Refund_Amount, 0)) AS RefundPaid,
                      SUM(IF(r.Status = 1, Refund_Amount, 0)) AS RefundUnPaid,
                      Customer_Invoice_Num,
                      SUM(Refund_Amount) AS Refund_Amount,
                      SUM(Cancellation_Charge) AS Cancellation_Charge,
                      (IF (r.Status = 1, r.Refund_Amount, 0)) AS Pending_Refund_Amount,
                      r.Status
               FROM customer_refund r
               WHERE r.Status !=0
               GROUP BY Customer_Invoice_Num ) AS cr ON cr.Customer_Invoice_Num = ci.Customer_Invoice_Num

            LEFT JOIN
              (SELECT Payee_Invoice_Num, SUM(Refund_Amount) AS Payee_Refund_Amount, SUM(Cancellation_Charge) AS Payee_Cancellation_Charge, SUM(IF (r.Status = 1, r.Refund_Amount, 0)) AS Payee_Pending_Refund_Amount
               FROM customer_refund r
               GROUP BY Payee_Invoice_Num) AS cr2 ON cr2.Payee_Invoice_Num = ci.Customer_Invoice_Num

            LEFT JOIN
              (SELECT cn.Status, cn.Customer_Invoice_Num, SUM(Credit_Amount) AS Credit_Amount
               FROM customer_credit_note cn
               WHERE cn.Status !=0
               GROUP BY cn.Customer_Invoice_Num) AS cn ON cn.Customer_Invoice_Num= ci.Customer_Invoice_Num

            LEFT JOIN customer_account AS ca ON ca.contactid = ci.Customer_Account_Customer_ID

            WHERE ci.Status IN (1, 4) AND ci.invoice_amount != 0 AND ca.invoiced_customer = 1
            $whereCondition 
                            $ACL_CONDITION_SUMMARY 
            GROUP BY ci.Customer_Invoice_Num

        ) AS a";

        /*
         * when ci.Status = 5 ( deliquent : status value not decided yet may be 5). So coding !=4 for getting records with 5.
         * SUM(IF( ci.Status != 4 AND cr.Status = 1, Cancellation_Charge ,0)) AS Cancellation,  
         */

        //TOOD; need to rewrite
        $summary     = GDb::fetchRow($sqlsummary);
        $TotalDue    = ($summary['NotYetDue'] + $summary['InDue'] + $summary['AmountNotYetDueCharges'] + $summary['AmountInDueCharges']) - ($summary['AmountNotYetDueCredit'] + $summary['AmountInDueCredit']) - $summary['RefundUnPaid'];
        $NotYetDue   = $summary['NotYetDue'] + $summary['AmountNotYetDueCharges'] - $summary['AmountNotYetDueCredit'];
        $OverDue     = $summary['InDue'] + $summary['AmountInDueCharges'] - $summary['AmountInDueCredit'] - $summary['RefundUnPaid'];
        $TotalAmount = ($summary['Paid'] + $summary['CancellationPaid']);

        $TotalAmount += $summary['AmountInDuePayee_Refund'];

        $TotalDue += $summary['AmountInDueCancellation_Charge'];
        $OverDue  += $summary['AmountInDueCancellation_Charge'];

        $TotalDue += $summary['AmountInDuePending_Refund_Amount'];
        $OverDue  += $summary['AmountInDuePending_Refund_Amount'];

        $summary['summary'] = array(
            'due'         => $TotalDue,
            'not_yet_due' => $NotYetDue,
            'over_due'    => $OverDue,
            'paid'        => $TotalAmount,
            'unpaid'      => $TotalDue,
        );

        return $summary;
    }

    /**
     * Payments listing.
     *
     * 
     * @param type $invoiceId
     * @return type
     */
    public function invoicePaymentsList($invoiceId) {
        $sqlp = "SELECT cp.Attachment, cp.Customer_Payment_ID, cp.Customer_Invoice_Num	, cp.Customer_Payment_Date, cp.Customer_Account_Customer_ID,
        cp.Customer_Payment_Method, cp.Customer_Payment_Comments, cp.Transaction_Type_Transaction_ID, 
        SUM(cp.Charges) AS Charges, SUM(cp.Customer_Payment_Amount) AS Customer_Payment_Amount, 
        IF(ci.Group_Invoice=1, cp.Payment_Unique_ID, cp.Customer_Payment_ID) AS groupingSelection FROM customer_payments cp
            INNER JOIN customer_invoice ci ON ci.Customer_Invoice_Num = cp.Customer_Invoice_Num
        WHERE cp.Customer_Invoice_Num='$invoiceId' AND cp.Status = 1 GROUP BY groupingSelection ORDER BY cp.Customer_Payment_Date DESC ";

        return GDb::fetchRowSet($sqlp);
    }

    public function customerPaymentListingCount($where, $acl) {
        $sqlCount = "SELECT COUNT(*) as total FROM customer_payments AS cp 
                INNER JOIN customer_invoice ci ON ci.Customer_Invoice_Num = cp.Customer_Invoice_Num AND cp.Status=1 -- AND ci.Status =1
                LEFT JOIN customer_account AS ca ON ca.contactid = cp.Customer_Account_Customer_ID -- AND ca.status=1
                LEFT JOIN contacts AS c ON c.id = ca.contactid 
                WHERE cp.Status=1 
                $acl    
                $where
                ";

        return $totalRecords = GDb::fetchScalar($sqlCount);
    }
    public function customerPaymentListing($where, $acl, $orderBy, $start = null, $length  = null) {


        $str =  "" ;
        if( $start ||  $length ) {
            $str = " LIMIT $start ,$length " ;
        }

        $sql = "SELECT c.fname, c.lname, c.mname, ca.contactid, g.tourdesc, cp.* FROM customer_payments AS cp 
                INNER JOIN customer_invoice ci ON ci.Customer_Invoice_Num = cp.Customer_Invoice_Num AND cp.Status=1 -- AND ci.Status =1
                LEFT JOIN customer_account AS ca ON ca.contactid = cp.Customer_Account_Customer_ID -- AND ca.status=1
                LEFT JOIN contacts AS c ON c.id = ca.contactid 
                        LEFT JOIN groups g ON g.tourid=cp.Group_ID
                WHERE cp.Status=1 
                $acl    
                $where
                GROUP BY cp.Customer_Payment_ID $orderBy $str ";


        return GDb::fetchRowSet($sql);
    }
}
