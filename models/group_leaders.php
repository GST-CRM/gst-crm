<?php

include_once __DIR__ . '/model.php' ;

class GroupLeaders extends Model {
    public $table = 'group_leaders' ;
    
    public function detail($email) {
        $sql = "SELECT * FROM group_leaders g
                INNER JOIN contacts c ON g.contactid = c.id WHERE c.email='$email' " ;
        
        return $this->executeOne($sql) ;
    }
    public function groups($leaderId, $activeOnly = true) {
        $where = " CONCAT(',', groupleader, ',') LIKE '%,$leaderId,%' " ;
        if( $activeOnly ) {
            $where .= ' AND status=1 ' ;
        }
        $sql = "SELECT *, DATE_SUB(startdate, INTERVAL 90 DAY) as Cancel_Date, IF( startdate >= CURDATE() AND enddate <= CURDATE(), 1,0) AS InProgress
                FROM `groups` WHERE $where " ;
        
        return $this->executeMany($sql) ;
    }
}