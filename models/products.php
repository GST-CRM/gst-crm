<?php

include_once __DIR__ . '/model.php' ;
include_once __DIR__ . '/customer_invoice.php' ;
include_once __DIR__ . '/customer_invoice_line.php' ;

class Products extends Model {

    public $table = 'products';
    
    /**
     * Update invoice amount when price is changed.
     */
    public function raisePriceChangedEvent($productId, $price = null) {
        $id = intval($productId) ;
        if( ! $price ) {
            $sql = "SELECT price FROM ". $this->table . " WHERE id=" . $id ;
            $price = $this->executeScalar($sql) ;
        }
        
        //update all invoice_line with new price
        $sqll = "UPDATE customer_invoice_line SET Invoice_Line_Total= ($price) WHERE Product_Product_ID=$id" ;
        $this->execute($sqll) ;
        
        //update price total Includes invoice have ginve id and excludes void invoices
        $sqlu = "UPDATE customer_invoice 
                INNER JOIN (
                        SELECT SUM(cil.Qty * cil.Invoice_Line_Total) as Total, Customer_Invoice_Num FROM customer_invoice_line cil
                            INNER JOIN products p ON p.id = cil.Product_Product_ID 
                            WHERE Customer_Invoice_Num IN ( 
                                    SELECT DISTINCT Customer_Invoice_Num FROM customer_invoice_line WHERE Product_Product_ID=$id
                                ) GROUP BY Customer_Invoice_Num 
                        ) AS A ON A.Customer_Invoice_Num = customer_invoice.Customer_Invoice_Num 
                    SET customer_invoice.Invoice_Amount=A.Total
                WHERE customer_invoice.Status != 0" ;
        $this->execute($sqlu) ;

    }
}
