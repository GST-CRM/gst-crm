<?php

include_once __DIR__ . '/model.php';
include_once __DIR__ . '/../models/customer_groups.php';
include_once __DIR__ . '/../models/customer_invoice.php';
include_once __DIR__ . '/../models/customer_invoice_line.php';

class CustomerInvoice extends Model {

    public static $VOID      = 0;
    public static $ACTIVE    = 1;
    public static $JOINT     = 2; //NO longer in use.
    public static $CANCELLED = 4;
    public $table = 'customer_invoice';

    public function getInvoiceDetails($invoiceId, $skipCanceled = true) {
        $skipCanceledCond = '';
        if ($skipCanceled) {
            $skipCanceledCond = " ci.Status=1 AND  ";
        }
        $sqle = "SELECT ci.*, ca.contactid, c.email, c.address1, c.address2, c.zipcode, c.city, c.state, c.country, DATEDIFF(Due_Date , NOW()) AS due_diff,
        DATEDIFF(g.startdate , NOW()) AS trip_start_days,
        Invoice_Amount as due, SUM(Customer_Payment_Amount) as paid, ca.tourid FROM contacts c
        INNER JOIN customer_account ca ON ca.contactid = c.id 
        LEFT JOIN customer_invoice ci ON ci.Customer_Account_Customer_ID = ca.contactid  AND ci.Group_ID = ca.tourid
        LEFT JOIN customer_payments cp ON cp.Customer_Invoice_Num = ci.Customer_Invoice_Num AND cp.Status=1 
        LEFT JOIN `groups` AS g ON g.tourid=ca.tourid 
        WHERE $skipCanceledCond ci.Customer_Invoice_Num='$invoiceId' GROUP BY ci.Customer_Invoice_Num ";

        //removed -- AND ca.status=1
        // INNER JOIN customer_account ca ON ca.contactid = c.id AND ca.status=1 

        return GDb::fetchRow($sqle);
    }

    public function withContactDetails($invoiceId) {
        $sqle = "SELECT ci.*, ca.contactid, c.email FROM customer_invoice ci
            INNER JOIN customer_account ca ON ca.contactid=ci.Customer_Account_Customer_ID AND ci.Group_ID = ca.tourid
            INNER JOIN contacts c ON c.id=ca.contactid
            WHERE Customer_Invoice_Num='$invoiceId'";
        return GDb::fetchRow($sqle);
    }

    public function withTripContactDetails($invoiceId) {
        $sqlg = "SELECT ci.Status, ci.Add_Date, c.passport, ca.tourid, ca.contactid AS id, Comments, Invoice_Amount, g.Church_Name, g.tourname, g.tourdesc, ci.Due_Date, g.2nd_Payment_Amount,
        DATE_SUB(ci.Due_Date, INTERVAL 90 DAY) as second_due, DATEDIFF(g.startdate, NOW()) as days_left, DATEDIFF(c.expirydate, g.enddate) AS expire_diff,
        c.fname,c.mname,c.lname,c.address1, c.address2, c.zipcode, c.city, c.state, c.country, g.agent,
        r.Add_Date as Refund_Date, r.Cancellation_Outcome, r.Cancellation_Charge, r.Refund_Amount, IF(r.Pending_Refund_Amount>0,1,0) AS Refund_Status, r.Pending_Refund_Amount ,
        cr2.Cancellation_Charge AS Payee_Cancellation_Charge, cr2.Refund_Amount AS Payee_Refund_Amount, IF(cr2.Pending_Refund_Amount>0,1,0) AS Payee_RefundStatus, cr2.Pending_Refund_Amount AS Payye_Pending_Refund_Amount
        FROM customer_account ca 
        INNER JOIN contacts c ON c.id = ca.contactid -- AND ca.status=1
        INNER JOIN customer_invoice ci ON ci.Customer_Account_Customer_ID=ca.contactid AND ci.Status IN (1,4) AND ci.Group_ID=ca.tourid
        LEFT JOIN 
        LEFT JOIN (
                    SELECT Customer_Invoice_Num, SUM(Refund_Amount) AS Refund_Amount, SUM(Cancellation_Charge) AS Cancellation_Charge,
                    SUM(r.Refund_Amount - r.Issued_Amount) AS Pending_Refund_Amount FROM customer_refund r  WHERE r.Status != 0 GROUP BY Customer_Invoice_Num
                ) AS cr ON cr.Customer_Invoice_Num = ci.Customer_Invoice_Num
        LEFT JOIN (
                    SELECT Payee_Invoice_Num, SUM(Refund_Amount) AS Refund_Amount, SUM(Cancellation_Charge) AS Cancellation_Charge,
                    SUM(r.Refund_Amount - r.Issued_Amount) AS Pending_Refund_Amount FROM customer_refund r GROUP BY Payee_Invoice_Num
                ) AS cr2 ON cr2.Payee_Invoice_Num = ci.Customer_Invoice_Num

        LEFT JOIN `groups` g ON g.tourid=ca.tourid AND g.status=1
        WHERE ci.Customer_Invoice_Num='$invoiceId'";

        return GDb::fetchRow($sqlg);
    }
    
    public function restoreAdditioalMemberFromGroupInvoice($tripId, $someOneInGroup) {
        
        return $this->moveAdditioalMemberFromGroupInvoice($tripId, $someOneInGroup, '-') ;
    }
    public function undoRestoreAdditioalMemberFromGroupInvoice($tripId, $someOneInGroup) {
      return $this->moveAdditioalMemberFromGroupInvoice($tripId, $someOneInGroup, '+') ;  
    }
    public function moveAdditioalMemberFromGroupInvoice($tripId, $someOneInGroup, $operation) {
        
        $group = new CustomerGroups() ;
        $primaryid = $group->getPrimaryTraveler($tripId, $someOneInGroup) ;
        
        if( $primaryid != $someOneInGroup ) {
            $targetInvoiceData = (new CustomerInvoice())->get(['Group_ID' => $tripId, 'Customer_Account_Customer_ID' => $primaryid]) ;
            $targetInvoiceId = $targetInvoiceData['Customer_Invoice_Num'] ;
            $targetProducts = (new CustomerInvoiceLine())->getProductLines($targetInvoiceId) ;
            
            $sourceInvoiceData = (new CustomerInvoice())->get(['Group_ID' => $tripId, 'Customer_Account_Customer_ID' => $someOneInGroup]) ;
            $sourceInvoiceId = $sourceInvoiceData['Customer_Invoice_Num'] ;
            $sourceProducts = (new CustomerInvoiceLine())->getProductLines($sourceInvoiceId) ;


            $srcProducts = [] ;
            foreach( $sourceProducts as $sourceProduct ) {
                $srcProducts[ $sourceProduct['Product_Product_ID'] ] = $sourceProduct['Qty'] ;
            }

            $tgProducts = [] ;
            foreach( $targetProducts as $targetProduct ) {
                $tgProducts[ $targetProduct['Product_Product_ID'] ] = $targetProduct['Qty'] ;
            }
            
            $productLine = new CustomerInvoiceLine() ;
            foreach( $srcProducts as $p => $q ) {
                if( $operation == '-' ) {                    
                    $qNew = $tgProducts[$p] - $q ;
                }
                else {
                    $qNew = $tgProducts[$p] + $q ;
                }
                if( $qNew < 0 ) {
                    $qNew = 0 ;
                }
                $productLine->updateProductQuantity($targetInvoiceId, $p, $qNew) ;
            }
            //update primary invoice without this customer.
            $isUpdated = $this->updateInvoiceTotal($targetInvoiceId);
        }
    }

    public function updateInvoiceTotal($invoiceId) {
        $sql = " UPDATE customer_invoice 
              INNER JOIN (
                        SELECT SUM(cil.Qty * cil.Invoice_Line_Total) as Total, Customer_Invoice_Num FROM customer_invoice_line cil
                            INNER JOIN products p ON p.id = cil.Product_Product_ID 
                            GROUP BY Customer_Invoice_Num 
                        ) AS A ON A.Customer_Invoice_Num = customer_invoice.Customer_Invoice_Num 
                    SET customer_invoice.Invoice_Amount=A.Total
               WHERE customer_invoice.Customer_Invoice_Num=$invoiceId " ;
        
        return $this->execute($sql) ;
    }
    public function isGroupInvoice($invoiceId) {
        
        $invData = $this->get(['Customer_Invoice_Num' => $invoiceId]) ;
        if( $invData['Group_Invoice'] == 1 ) {
            return 1 ;
        }
        else if( $invData['Additional_Invoice'] == 1 ) {
            return 1 ;
        }
        else if( $invData['Status'] == 0 ) {
            $cg = new CustomerGroups() ;
            $tourId = $invData['Group_ID'] ;
            $travelerId = $invData['Customer_Account_Customer_ID'] ;
            return $cg->isGroupMember($tourId, $travelerId) ;
        }
        return false ;
    }
    public function markAdditionalInvoices($tourId, $primaryId, $markEnabled, $empIds = []) {
        
        $customers = (new CustomerGroups())->getAdditionalCustomerIds($tourId, $primaryId, false ) ;
        
        $oldEmpIdSql = GUtils::implodeIfValue($customers, ',') ;
        $sqlOld = "UPDATE customer_invoice SET Additional_Invoice=0 WHERE Group_ID=$tourId AND
            Customer_Account_Customer_ID IN($oldEmpIdSql)" ;
        $this->execute($sqlOld) ;
        
        $empIdSql = GUtils::implodeIfValue($empIds, ',') ;
        
        $flag = ($markEnabled ? 1 : 0) ;
        $sql = "UPDATE customer_invoice SET Additional_Invoice=$flag WHERE Group_ID=$tourId AND
            Customer_Account_Customer_ID IN($empIdSql)" ;
        $this->execute($sql) ;
        
//        if( $closure ) {
//            if( $empIds ) {
//                //Mark as additional
//                $sqlS = "UPDATE customer_invoice  SET Additional_Invoice=0
//                    WHERE Group_ID=$tourId
//                    AND Customer_Account_Customer_ID IN($empIds)" ;
//                $this->execute($sqlS) ;
//            }
//            //Remove additional if set. Do not use NOT IN as it may affect other groups in the same trip also.
//            $sqlR = "UPDATE customer_invoice  SET Additional_Invoice=0
//                WHERE Group_ID=$tourId
//                AND Customer_Account_Customer_ID= $primaryId" ;
//            $this->execute($sqlR) ;
//        }
//        else {
//            if( $empIds ) {
//                //Mark as additional
//                $sqlS = "UPDATE customer_invoice  SET Additional_Invoice=1
//                    WHERE Group_ID=$tourId
//                    AND Customer_Account_Customer_ID IN($empIds)" ;
//                $this->execute($sqlS) ;
//            }
//            //Remove additional if set. Do not use NOT IN as it may affect other groups in the same trip also.
//            $sqlR = "UPDATE customer_invoice  SET Additional_Invoice=0
//                WHERE Group_ID=$tourId
//                AND Customer_Account_Customer_ID= $primaryId" ;
//            $this->execute($sqlR) ;
//        }
    }
    
    
}
