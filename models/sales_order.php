<?php

include_once __DIR__ . '/model.php' ;

class SalesOrder extends Model {
    public $table = 'sales_order' ;
    
    public function orderStop($tourId, $contactId) {
        $sql = "UPDATE sales_order SET Sales_Order_Stop=1 WHERE Customer_Account_Customer_ID=$contactId AND Group_ID=$tourId LIMIT 1" ;
        return $this->execute($sql) ;
    }
    public function orderResume($tourId, $contactId) {
        $sql = "UPDATE sales_order SET Sales_Order_Stop=0 WHERE Customer_Account_Customer_ID=$contactId AND Group_ID=$tourId LIMIT 1" ;
        return $this->execute($sql) ;
    }
}