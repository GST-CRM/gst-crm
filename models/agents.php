<?php

include_once __DIR__ . '/model.php' ;

class Agents extends Model {
    public $table = 'agents' ;
    
    public function detail($email) {
        $sql = "SELECT * FROM agents a
                INNER JOIN contacts c ON a.contactid = c.id WHERE c.email='$email' " ;
        
        return $this->executeOne($sql) ;
    }
    
    public function groups($agentId, $activeOnly = true) {
        $where = " CONCAT(',', agent, ',') LIKE '%,$agentId,%' " ;
        if( $activeOnly ) {
            $where .= ' AND status=1 ' ;
        }
        $sql = "SELECT *, DATE_SUB(startdate, INTERVAL 90 DAY) as Cancel_Date, IF( startdate >= CURDATE() AND enddate <= CURDATE(), 1,0) AS InProgress
                FROM `groups` WHERE $where " ;
        
        return $this->executeMany($sql) ;
    }
}