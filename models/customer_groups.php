<?php

include_once __DIR__ . '/model.php' ;
include_once __DIR__ . '/acl_permission.php' ;
include_once __DIR__ . '/acl_role.php' ;

class CustomerGroups extends Model {
    public $table = 'customer_groups' ;
    
    public function getPrimaryTraveler($tripId, $additionalId) {
        
        $sql = "SELECT Primary_Customer_ID FROM customer_groups WHERE Group_ID=$tripId AND (
            Additional_Traveler_ID_1=$additionalId OR
            Additional_Traveler_ID_2=$additionalId OR
            Additional_Traveler_ID_3=$additionalId OR
            Additional_Traveler_ID_4=$additionalId OR
            Additional_Traveler_ID_5=$additionalId OR
            Additional_Traveler_ID_6=$additionalId OR
            Additional_Traveler_ID_7=$additionalId OR
            Additional_Traveler_ID_8=$additionalId OR
            Additional_Traveler_ID_9=$additionalId OR
            Additional_Traveler_ID_10=$additionalId OR 
                Primary_Customer_ID=$additionalId )
            " ;
        
        return $this->executeScalar($sql) ;
    }
    
    public function isGroupMember($tourId, $travelerId) {
        $sql = "SELECT COUNT(*) as total FROM customer_groups WHERE
            Group_ID = $tourId AND (
            Additional_Traveler_ID_1=$travelerId OR
            Additional_Traveler_ID_2=$travelerId OR
            Additional_Traveler_ID_3=$travelerId OR
            Additional_Traveler_ID_4=$travelerId OR
            Additional_Traveler_ID_5=$travelerId OR
            Additional_Traveler_ID_6=$travelerId OR
            Additional_Traveler_ID_7=$travelerId OR
            Additional_Traveler_ID_8=$travelerId OR
            Additional_Traveler_ID_9=$travelerId OR
            Additional_Traveler_ID_10=$travelerId OR 
                Primary_Customer_ID=$travelerId ) " ;
        
        if( $this->executeScalar($sql) > 0 ) {
            return true ;
        }
        return false ;
    }
    public function isPrimaryTraveler($tourId, $travellerId) {
        
        $sql = "SELECT Primary_Customer_ID FROM customer_groups WHERE
            Group_ID = $tourId AND Primary_Customer_ID=$travellerId AND Type='Group Invoice' " ;
        $thePrimary = $this->executeScalar($sql) ;
        if( $travellerId && $thePrimary == $travellerId ) {
            return true ;
        }
        return false ;
    }
    
    
    public function getGroupCustomerIds($tripId) {
        $customers = [] ;        
        $primaryId  = null ;
        
        $row = $this->get(" Group_ID= $tripId ") ;
        if( is_array($row) ) {
            
            if( intval($row['Additional_Traveler_ID_1']) ) {
                $customers[] = $row['Additional_Traveler_ID_1'] ;
            }
            if( intval($row['Additional_Traveler_ID_2']) ) {
                $customers[] = $row['Additional_Traveler_ID_2'] ;
            }
            if( intval($row['Additional_Traveler_ID_3']) ) {
                $customers[] = $row['Additional_Traveler_ID_3'] ;
            }
            if( intval($row['Additional_Traveler_ID_4']) ) {
                $customers[] = $row['Additional_Traveler_ID_4'] ;
            }
            if( intval($row['Additional_Traveler_ID_5']) ) {
                $customers[] = $row['Additional_Traveler_ID_5'] ;
            }
            if( intval($row['Additional_Traveler_ID_6']) ) {
                $customers[] = $row['Additional_Traveler_ID_6'] ;
            }
            if( intval($row['Additional_Traveler_ID_7']) ) {
                $customers[] = $row['Additional_Traveler_ID_7'] ;
            }
            if( intval($row['Additional_Traveler_ID_8']) ) {
                $customers[] = $row['Additional_Traveler_ID_8'] ;
            }
            if( intval($row['Additional_Traveler_ID_9']) ) {
                $customers[] = $row['Additional_Traveler_ID_9'] ;
            }
            if( intval($row['Additional_Traveler_ID_10']) ) {
                $customers[] = $row['Additional_Traveler_ID_10'] ;
            }
            if( intval($row['Primary_Customer_ID']) ) {
                $primaryId = $row['Primary_Customer_ID'] ;
            }
            return ['primary' => $primaryId, 'additionals' => $customers ] ;
        }
        return null ;
    }
    public function getAdditionalCustomerIds($tripId, $primaryCustomerId, $includePrimary = true) {
        $customers = [] ;        
        
        $row = $this->get(" `Primary_Customer_ID` = '$primaryCustomerId' AND Group_ID= $tripId ") ;
        if( is_array($row) ) {
            
            if( intval($row['Additional_Traveler_ID_1']) ) {
                $customers[] = $row['Additional_Traveler_ID_1'] ;
            }
            if( intval($row['Additional_Traveler_ID_2']) ) {
                $customers[] = $row['Additional_Traveler_ID_2'] ;
            }
            if( intval($row['Additional_Traveler_ID_3']) ) {
                $customers[] = $row['Additional_Traveler_ID_3'] ;
            }
            if( intval($row['Additional_Traveler_ID_4']) ) {
                $customers[] = $row['Additional_Traveler_ID_4'] ;
            }
            if( intval($row['Additional_Traveler_ID_5']) ) {
                $customers[] = $row['Additional_Traveler_ID_5'] ;
            }
            if( intval($row['Additional_Traveler_ID_6']) ) {
                $customers[] = $row['Additional_Traveler_ID_6'] ;
            }
            if( intval($row['Additional_Traveler_ID_7']) ) {
                $customers[] = $row['Additional_Traveler_ID_7'] ;
            }
            if( intval($row['Additional_Traveler_ID_8']) ) {
                $customers[] = $row['Additional_Traveler_ID_8'] ;
            }
            if( intval($row['Additional_Traveler_ID_9']) ) {
                $customers[] = $row['Additional_Traveler_ID_9'] ;
            }
            if( intval($row['Additional_Traveler_ID_10']) ) {
                $customers[] = $row['Additional_Traveler_ID_10'] ;
            }
            
            $ids = GUtils::implodeIfValue($customers, ',');
            $sql = "SELECT contactid FROM customer_account WHERE status=1 AND tourid=$tripId AND contactid IN($ids)" ;
            $records = GDb::fetchRowSet($sql) ;
            $values = array_column($records, 'contactid') ;

            $customers = array_values($values) ;
        }
        
        if( $includePrimary ) {
            $customers[] = $primaryCustomerId ;
        }
        return $customers ;
    }
    
    
    public function unlinkFromGroup($tourId, $additionalCustomerId, $isPrimary = false) {
        
        
        $removeCustomer = "SELECT * FROM customer_groups WHERE (

                    Additional_Traveler_ID_1=$additionalCustomerId OR 
                    Additional_Traveler_ID_2=$additionalCustomerId OR 
                    Additional_Traveler_ID_3=$additionalCustomerId OR 
                    Additional_Traveler_ID_4=$additionalCustomerId OR 
                    Additional_Traveler_ID_5=$additionalCustomerId OR 
                    Additional_Traveler_ID_6=$additionalCustomerId OR 
                    Additional_Traveler_ID_7=$additionalCustomerId OR 
                    Additional_Traveler_ID_8=$additionalCustomerId OR 
                    Additional_Traveler_ID_9=$additionalCustomerId OR 
                    Additional_Traveler_ID_10=$additionalCustomerId )
                AND Group_ID=$tourId AND Type='Group Invoice';";
        
        $row = $this->executeOne($removeCustomer) ;
        
        $set = '' ;
        if( intval($row['Additional_Traveler_ID_1']) == $additionalCustomerId ) {
            $set = " Additional_Traveler_ID_1='NULL' " ;
        }
        if( intval($row['Additional_Traveler_ID_2']) == $additionalCustomerId ) {
            $set = " Additional_Traveler_ID_2='NULL' " ;
        }
        if( intval($row['Additional_Traveler_ID_3']) == $additionalCustomerId ) {
            $set = " Additional_Traveler_ID_3='NULL' " ;
        }
        if( intval($row['Additional_Traveler_ID_4']) == $additionalCustomerId ) {
            $set = " Additional_Traveler_ID_4='NULL' " ;
        }
        if( intval($row['Additional_Traveler_ID_5']) == $additionalCustomerId ) {
            $set = " Additional_Traveler_ID_5='NULL' " ;
        }
        if( intval($row['Additional_Traveler_ID_6']) == $additionalCustomerId ) {
            $set = " Additional_Traveler_ID_6='NULL' " ;
        }
        if( intval($row['Additional_Traveler_ID_7']) == $additionalCustomerId ) {
            $set = " Additional_Traveler_ID_7='NULL' " ;
        }
        if( intval($row['Additional_Traveler_ID_8']) == $additionalCustomerId ) {
            $set = " Additional_Traveler_ID_8='NULL' " ;
        }
        if( intval($row['Additional_Traveler_ID_9']) == $additionalCustomerId ) {
            $set = " Additional_Traveler_ID_9='NULL' " ;
        }
        if( intval($row['Additional_Traveler_ID_10']) == $additionalCustomerId ) {
            $set = " Additional_Traveler_ID_10='NULL' " ;
        }


        if( $set ) {
            $unlinkGroup = "UPDATE customer_groups SET $set WHERE Group_ID=$tourId AND Type='Group Invoice' LIMIT 1;";
            $this->execute($unlinkGroup) ;
        }
        if( $isPrimary ) {
            $deleteGroup = "DELETE FROM customer_groups WHERE Primary_Customer_ID=$additionalCustomerId AND Group_ID=$tourId AND Type='Group Invoice' LIMIT 1" ;
            $this->execute($deleteGroup) ;
        }
        $unlinkAccount = "UPDATE customer_account SET groupinvoice='0' WHERE contactid=$additionalCustomerId AND tourid=$tourId;" ;
        $this->execute($unlinkAccount) ;

        $unlinkInvoice = "UPDATE customer_invoice SET Additional_Invoice='0', Group_Invoice=0 WHERE Customer_Account_Customer_ID =$additionalCustomerId AND Group_ID=$tourId;" ;
        $this->execute($unlinkInvoice ) ;
    }
    
    
    public function closeGroup($tourId, $primaryCustomerId) {
        //TODO; implement tour id in this function.
        $customers = $this->getAdditionalCustomerIds($tourId, $primaryCustomerId, false) ;
        if( count($customers) > 0 ) {
            $JointInvoiceID_Array = GUtils::implodeIfValue($customers, ',') ;

            $CustomerGroupQuery = "DELETE FROM customer_groups WHERE Primary_Customer_ID=$primaryCustomerId AND Group_ID=$tourId AND Type='Group Invoice';";
            $CustomerGroupQuery .= "UPDATE customer_account SET groupinvoice='0',Status='1' WHERE contactid IN ($JointInvoiceID_Array) AND tourid=$SelectedTour;";
        }
    }
     
    public function removePrimaryTraverlerFromGroup($tourId, $primaryId) {
        $removeCustomer = "DELETE FROM customer_groups WHERE Primary_Customer_ID=$primaryId
                            AND Group_ID=$tourId AND Type='Group Invoice';";
        
        $this->execute($removeCustomer) ;

        $removeFlag = "UPDATE customer_account SET groupinvoice='0',Status='1' WHERE contactid =$primaryId AND tourid=$tourId;";
        $this->execute($removeFlag) ;
    }
    public function removeAdditionalTraverlerFromGroup($tourId, $additionalCustomerId) {

        //TODO; will it remove everyone?
        $removeCustomer = "DELETE FROM customer_groups WHERE 
                    ( Additional_Traveler_ID_1=$additionalCustomerId OR
                    Additional_Traveler_ID_2=$additionalCustomerId OR 
                    Additional_Traveler_ID_3=$additionalCustomerId OR 
                    Additional_Traveler_ID_4=$additionalCustomerId OR 
                    Additional_Traveler_ID_5=$additionalCustomerId OR 
                    Additional_Traveler_ID_6=$additionalCustomerId OR 
                    Additional_Traveler_ID_7=$additionalCustomerId OR 
                    Additional_Traveler_ID_8=$additionalCustomerId OR 
                    Additional_Traveler_ID_9=$additionalCustomerId OR 
                    Additional_Traveler_ID_10=$additionalCustomerId )
                AND Group_ID=$tourId AND Type='Group Invoice';";
        
        $this->execute($removeCustomer) ;

        $removeFlag = "UPDATE customer_account SET groupinvoice='0',Status='1' WHERE contactid =$additionalCustomerId AND tourid=$tourId;";
        $this->execute($removeFlag) ;
    }
    public function getJointMemberDetails($tripId, $primaryCustomerId) {
        $customerIds = $this->getAdditionalCustomerIds($tripId, $primaryCustomerId, true) ;
        return $this->getBulkCustomerDetails($customerIds) ;
    }
    public function getBulkCustomerDetails($customerIds) {
        $customerIdString = implode(',', $customerIds) ;
        $sql = "SELECT ca.contactid, c.fname, c.mname, c.lname, c.email, c.address1, c.address2, c.zipcode, c.city, c.state, c.country 
                    FROM customer_account ca 
                    INNER JOIN contacts c ON c.id = ca.contactid
                    WHERE ca.contactid IN ($customerIdString) AND ca.Status=1 " ;
        
        return GDb::fetchRowSet($sql) ;
    }

    
    public function getJointMemberOptions($tripId, $primaryCustomerId) {
        $customerIds = $this->getAdditionalCustomerIds($tripId, $primaryCustomerId,  false) ;
        return $this->getBulkCustomerOptions($tripId, $customerIds) ;
    }
    public function getBulkCustomerOptions($tripId, $customerIds) {
        $customerIdString = implode(',', $customerIds) ;

        $sql = "SELECT ca.contactid AS id, CONCAT( c.fname, ' ', c.mname, ' ', c.lname ) AS name
                    FROM customer_account ca 
                    INNER JOIN contacts c ON c.id = ca.contactid
                    WHERE ca.contactid IN ($customerIdString) AND tourid=$tripId AND ca.Status=1 " ;
        
        $records = GDb::fetchRowSet($sql) ;
        $options = "<option value=''>Select</option>" ;
        foreach( $records as $one ) {
            $value = $one['id'] ;
            $name = $one['name'] ;
            $options .= "<option value='$value'>$name</option>" ;
        }
        return $options ;
    }
    public function tripInvoiceId($tripId, $customerId) {
        $sql = "SELECT Customer_Invoice_Num FROM customer_invoice WHERE Customer_Account_Customer_ID=$customerId
                AND Group_ID=$tripId " ;
        return GDb::fetchScalar($sql) ;
    }
    /**
     * Switch primary with an additional customer
     */
    public function switchPrimary($tripId, $oldPrimary, $newPrimary) {
        
        $oldInvoiceId = $this->tripInvoiceId($tripId, $oldPrimary) ;
        $newInvoiceId = $this->tripInvoiceId($tripId, $newPrimary) ;

//        ini_set('display_errors', 1) ;
//        error_reporting(0xFFFF) ;
        GDb::execute("SET  FOREIGN_KEY_CHECKS=0;") ;
        //move credit_notes
        $this->moveCreditNotes($oldPrimary, $oldInvoiceId, $newPrimary, $newInvoiceId ) ;
        //move customer_payments
        $this->moveInvoices($oldPrimary, $oldInvoiceId, $newPrimary, $newInvoiceId ) ;
        $this->moveInvoiceLines($oldPrimary, $oldInvoiceId, $newPrimary, $newInvoiceId ) ;
        //move customer_payments
        $this->movePayments($oldPrimary, $oldInvoiceId, $newPrimary, $newInvoiceId ) ;
        //move customer_refund
        $this->moveRefunds($oldPrimary, $oldInvoiceId, $newPrimary, $newInvoiceId ) ;
        
        //sales_order ?
        //swap primary and additional in customer_groups
        $this->swapPrimaryAndAdditional($tripId, $oldPrimary, $newPrimary) ;
        
        GDb::execute("SET FOREIGN_KEY_CHECKS=1;") ;
        return $newInvoiceId ;
    }
    private function moveCustomerRecords($query, $oldPrimary, $oldInvoiceId, $newPrimary, $newInvoiceId) {
        $oldTemp = [
            -1, -2, //temp data
            $oldPrimary, $oldInvoiceId, //old data
        ] ;
        $newTemp = [
            -3, -4, //temp data
            $newPrimary, $newInvoiceId, //old data
        ] ;
        $new = [
            $oldPrimary, $oldInvoiceId, //temp data
            -3, -4, //old data
        ] ;
        $old = [
            $newPrimary, $newInvoiceId, //temp data
            -1, -2, //old data
        ] ;
        
        $sqlOldTemp = vsprintf( $query , $oldTemp) ;
        GDb::execute_multi($sqlOldTemp) ;    
        
        $sqlNewTemp = vsprintf( $query , $newTemp) ;
        GDb::execute_multi($sqlNewTemp) ;    
        
        $sqlNew = vsprintf( $query , $new) ;
        GDb::execute_multi($sqlNew) ;    

        $sqlOld = vsprintf( $query , $old) ;
        GDb::execute_multi($sqlOld) ;    
    }
    private function moveCreditNotes($oldPrimary, $oldInvoiceId, $newPrimary, $newInvoiceId) {
        $query = "SET FOREIGN_KEY_CHECKS=0; UPDATE customer_credit_note SET Customer_Account_Customer_ID= %s, Customer_Invoice_Num=%s
                    WHERE Customer_Account_Customer_ID= %s AND Customer_Invoice_Num=%s; ";

        return $this->moveCustomerRecords( $query, $oldPrimary, $oldInvoiceId, $newPrimary, $newInvoiceId) ;        
    }
    private function movePayments($oldPrimary, $oldInvoiceId, $newPrimary, $newInvoiceId) {

        $query = "SET FOREIGN_KEY_CHECKS=0; UPDATE customer_payments SET Customer_Account_Customer_ID=%s, Customer_Invoice_Num=%s
            WHERE Customer_Account_Customer_ID=%s AND Customer_Invoice_Num=%s; " ;

        return $this->moveCustomerRecords( $query, $oldPrimary, $oldInvoiceId, $newPrimary, $newInvoiceId) ;        
    }
    private function moveRefunds($oldPrimary, $oldInvoiceId, $newPrimary, $newInvoiceId) {
                
        $query = "SET FOREIGN_KEY_CHECKS=0; UPDATE customer_refund SET Customer_Account_Customer_ID=%s, Customer_Invoice_Num=%s
            WHERE Customer_Account_Customer_ID=%s AND Customer_Invoice_Num=%s; " ;

        return $this->moveCustomerRecords( $query, $oldPrimary, $oldInvoiceId, $newPrimary, $newInvoiceId) ;        
    }
        private function moveInvoiceLines( $oldPrimary, $oldInvoiceId, $newPrimary, $newInvoiceId) {
        
         $query = "SET FOREIGN_KEY_CHECKS=0; SET @x = %s; UPDATE customer_invoice_line SET Customer_Invoice_Num=%s
            WHERE -999 != %s AND Customer_Invoice_Num=%s; " ;

        return $this->moveCustomerRecords( $query, $oldPrimary, $oldInvoiceId, $newPrimary, $newInvoiceId) ;        
        }
    private function moveInvoices( $oldPrimary, $oldInvoiceId, $newPrimary, $newInvoiceId) {
        
         $query = "SET FOREIGN_KEY_CHECKS=0; UPDATE customer_invoice SET Customer_Account_Customer_ID=%s, Customer_Invoice_Num=%s
            WHERE Customer_Account_Customer_ID=%s AND Customer_Invoice_Num=%s; " ;

        return $this->moveCustomerRecords( $query, $oldPrimary, $oldInvoiceId, $newPrimary, $newInvoiceId) ;        
        
        
//        $oldTemp = [
//            -1, //temp data
//            $tripId, $oldPrimary, //old data
//        ] ;
//        $newTemp = [
//            -3, //temp data
//            $tripId, $newPrimary, //old data
//        ] ;
//        $new = [
//            $oldInvoiceId, 
//            $tripId, -3,
//        ] ;
//        $old = [
//            $newInvoiceId, //temp data
//            $tripId, -1, //old data
//        ] ;
//
//        $query = "SET FOREIGN_KEY_CHECKS=0; UPDATE customer_invoice SET Customer_Invoice_Num=%s
//            WHERE Group_ID=%s AND Customer_Account_Customer_ID=%s; " ;
//        
//        
//        $sqlOldTemp = vsprintf( $query , $oldTemp) ;
//        GDb::execute_multi($sqlOldTemp) ;    
//        
//        $sqlNewTemp = vsprintf( $query , $newTemp) ;
//        GDb::execute_multi($sqlNewTemp) ;    
//        
//        
//        $queryNew = "SET FOREIGN_KEY_CHECKS=0; UPDATE customer_invoice SET Group_Invoice=1, Additional_Invoice=0, Customer_Invoice_Num=%s
//            WHERE Group_ID=%s AND Customer_Invoice_Num=%s; " ;
//        
//        $sqlNew = vsprintf( $queryNew , $new) ;
//        GDb::execute_multi($sqlNew) ;    
//
//        $queryOld = "SET FOREIGN_KEY_CHECKS=0; UPDATE customer_invoice SET Group_Invoice=0, Additional_Invoice=1, Customer_Invoice_Num=%s
//            WHERE Group_ID=%s AND Customer_Invoice_Num=%s ;" ;
//                
//        $sqlOld = vsprintf( $queryOld , $old) ;
//        GDb::execute_multi($sqlOld) ;   
                
    }

    private function swapPrimaryAndAdditional($tripId, $oldPrimary, $newPrimary) {
        $sql = "SELECT * FROM customer_groups WHERE Group_ID=$tripId AND (
                Primary_Customer_ID=$oldPrimary ) " ;
        
        $row = GDb::fetchRow($sql) ;
        
        $set = '' ;
        if( intval($row['Additional_Traveler_ID_1']) == $newPrimary ) {
            $set = " Additional_Traveler_ID_1='$oldPrimary' " ;
        }
        if( intval($row['Additional_Traveler_ID_2']) == $newPrimary ) {
            $set = " Additional_Traveler_ID_2='$oldPrimary' " ;
        }
        if( intval($row['Additional_Traveler_ID_3']) == $newPrimary ) {
            $set = " Additional_Traveler_ID_3='$oldPrimary' " ;
        }
        if( intval($row['Additional_Traveler_ID_4']) == $newPrimary ) {
            $set = " Additional_Traveler_ID_4='$oldPrimary' " ;
        }
        if( intval($row['Additional_Traveler_ID_5']) == $newPrimary ) {
            $set = " Additional_Traveler_ID_5='$oldPrimary' " ;
        }
        if( intval($row['Additional_Traveler_ID_6']) == $newPrimary ) {
            $set = " Additional_Traveler_ID_6='$oldPrimary' " ;
        }
        if( intval($row['Additional_Traveler_ID_7']) == $newPrimary ) {
            $set = " Additional_Traveler_ID_7='$oldPrimary' " ;
        }
        if( intval($row['Additional_Traveler_ID_8']) == $newPrimary ) {
            $set = " Additional_Traveler_ID_8='$oldPrimary' " ;
        }
        if( intval($row['Additional_Traveler_ID_9']) == $newPrimary ) {
            $set = " Additional_Traveler_ID_9='$oldPrimary' " ;
        }
        if( intval($row['Additional_Traveler_ID_10']) == $newPrimary ) {
            $set = " Additional_Traveler_ID_10='$oldPrimary' " ;
        }

        if( $set ) {
            $sqlSwitch1 = "UPDATE customer_groups SET $set 
                    WHERE Primary_Customer_ID=$oldPrimary AND Type='Group Invoice' AND Group_ID=$tripId " ;
            GDb::execute($sqlSwitch1) ;

            $sqlSwitch2 = "UPDATE customer_groups SET Primary_Customer_ID=$newPrimary 
                    WHERE Primary_Customer_ID=$oldPrimary AND Type='Group Invoice' AND Group_ID=$tripId " ;
            GDb::execute($sqlSwitch2) ;
            
            return true ;
        }
        return false ;
    }
}