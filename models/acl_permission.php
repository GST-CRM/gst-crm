<?php

include_once __DIR__ . '/model.php' ;
include_once __DIR__ . '/acl_role.php' ;

class AclPermission extends Model {

    public $table = 'acl_permission';
    
    public static function roleId() {
        
        GUtils::sessionStart() ;
        
        if( isset($_SESSION['Role_ID'])) {
            return $_SESSION['Role_ID'] ;
        }
        return null ;
    }
    public static function activeRoleId() {
        
        GUtils::sessionStart() ;
        
        if( isset($_SESSION['Active_Role'])) {
            return $_SESSION['Active_Role'] ;
        }
        return null ;
    }
    
    public static function roleFlags() {
        
        GUtils::sessionStart() ;
        
        if( isset($_SESSION['Role_Flags'])) {
            return $_SESSION['Role_Flags'] ;
        }
        return 0 ;
    }
    public static function userGroup() {
        
        GUtils::sessionStart() ;
        
        if( isset($_SESSION['Group_ID'])) {
            return $_SESSION['Group_ID'] ;
        }
        return 0 ;
    }
    
    public static function userId() {
        
        GUtils::sessionStart() ;
        
        if( isset($_SESSION['User_ID'])) {
            return $_SESSION['User_ID'] ;
        }
        return 0 ;
    }
    /**
     * 
     * @param type $action
     * @param type $page
     * @param type $role
     */
    public function isActionAllowed($action, $page = null, $roleId = null) {
        if( ! $roleId ) {
            $roleId = self::activeRoleId() ;
        }
        if( ! $page ) {
            $page = basename($_SERVER['PHP_SELF']) ;
        }
        //get page id
        $sqlPage = "SELECT ID FROM acl_pages WHERE Page='$page' LIMIT 1" ;
        $pageID = $this->executeScalar($sqlPage) ;
        
        //get action id
        $sqlAction = "SELECT ID FROM acl_action WHERE Action='$action' AND Page_ID=$pageID LIMIT 1" ;
        $actionID = $this->executeScalar($sqlAction) ;
        
        //get grant type
        $sqlRole = "SELECT Permission FROM acl_role WHERE ID='$roleId' LIMIT 1" ;
        $permission = $this->executeScalar($sqlRole) ;
        
        //Is permission Record Exists ?
        $sql = "SELECT COUNT(*) FROM acl_permission WHERE Action_ID=$actionID AND Role_ID=$roleId" ;
        $found = $this->executeScalar($sql) ;
        
        //When permission is grant the acl_permission table contains denied items. So $found must be false.
        if( $permission == 'Grant' && ! $found ) {
            return true ;
        }
        
        //When permission is deny the acl_permission table contains allowed items. So $found must be true.
        if( $permission == 'Deny' && $found ) {
            return true ;
        }
        return false ;
    }
    public function isPageAllowed($page, $role = null) {
        if( $page == 'account-switch.php') {
            return true ;
        }
        return $this->isActionAllowed('Index', $page, $role) ;
    }
    
    public static function actionAllowed($action, $page = null, $roleId = null) {
        $obj = new AclPermission() ;
        return $obj->isActionAllowed($action, $page, $roleId) ;
    }
    
    public static function pageAllowed($page, $role = null) {
        $obj = new AclPermission() ;
        return $obj->isPageAllowed($page, $role) ;
    }
    
    
    public static function anyPageAllowed($page, $role = null) {
        $obj = new AclPermission() ;
        $ret = false ;
        foreach( $page as $one ) {
            $ret = $obj->isPageAllowed($one, $role) ;
            if( $ret ){
                return true ;
            }
        }
        return $ret ;
    }
    public static function allPageAllowed($page, $role = null) {
        $obj = new AclPermission() ;
        $ret = false ;
        foreach( $page as $one ) {
            $ret = $obj->isPageAllowed($one, $role) ;
            if( ! $ret ){
                return false ;
            }
        }
        return $ret ;
    }
}
