<?php

include_once __DIR__ . '/model.php' ;

class CustomerRefund extends Model {

    public $table = 'customer_refund';
    
    public static $DELETED = 0 ;
    public static $CREATED = 1 ;
    public static $ISSUED = 2 ;
    public static $PARTIALLY_ISSUED = 3 ;
    public static $CUSTOM_REFUND = 4 ;
    
    private static $outcome = [
        'Trip canceled within 60 days of trip start date - No Refund',
        'Trip canceled within 61 - 90 days of trip start date - Partial Refund',
        'Trip canceled within 91 -120 days of trip start date - Partial Refund',
        'Trip canceled within > 120 days of trip start date - Partial Refund'
        ];

    public static $NO_FEE_CANCELLATION_MSG = 'No Fee Cancellation' ;

    public static function calculateRefund($invoiceAmount, $charges, $paymentAmount, $daysPeriod, $additionalCharges, $invoiceId = null ) {
                
        $previousRefundAny = 0 ;
        if( $invoiceId ){ 
            $ri = (new CustomerRefund())->get(['Customer_Invoice_Num' => $invoiceId ]) ;
            if( is_array($ri) ) {
                $previousRefundAny = $ri['Active_Refund'] ;
            }
        }
        $paymentAmount -= $charges ;    //deduct any charges.
        $paymentAmount -= $previousRefundAny ;  //deduct any previous refunds.
        
        if( $daysPeriod <= 60 ) {
            $cancelCharge = $invoiceAmount ;
            $outcome = self::$outcome[0] ;
        }
        else if( $daysPeriod > 60 && $daysPeriod <= 90 ) {
            $cancelCharge = $invoiceAmount / 2 ;
            $outcome = self::$outcome[1] ;
        }
        else if( $daysPeriod > 90 && $daysPeriod <= 120 ) {
            $cancelCharge = 250 ;
            $outcome = self::$outcome[2] ;
        }
        else {
            $cancelCharge = 100 ;
            $outcome = self::$outcome[3] ;
        }
        
        if( $paymentAmount > $cancelCharge ) {
            $refund = $paymentAmount - $cancelCharge ;
        }
        else if( $paymentAmount == $cancelCharge ) {
            $refund = 0 ;
        }
        else {
            $refund = 0 ;
        }
        
        if( $refund >= $additionalCharges ) {
            $refund -= $additionalCharges ;
        }
        else {
            $cancelCharge += $additionalCharges ;
        }

        return array(
            'Cancellation_Charge' => $cancelCharge,
            'Refund_Amount' => $refund,
            'Cancellation_Outcome' => $outcome,
        ) ;
    }
    
    public function invoiceRefundDetail($invoiceId) {
        $sql = "SELECT *, crl.Attachment FROM customer_refund_line crl
                RIGHT JOIN customer_refund cr ON cr.Refund_ID = crl.Refund_ID 
                WHERE cr.Customer_Invoice_Num='$invoiceId' ORDER BY Refund_Line_ID ASC" ;
        return GDb::fetchRowSet($sql) ;
    }
        
    public function refundList($customerId, $groupId) {
        $sql = "SELECT cr.* FROM customer_refund cr 
                    INNER JOIN customer_account ca ON ca.contactid=cr.Customer_Account_Customer_ID
                    WHERE ca.tourid=$groupId AND ca.contactid=$customerId  AND ca.status=1" ;
        
        return GDb::fetchRowSet($sql) ;
    }
    public function refundAlways($cond) {
        $sql = "SELECT cr.*, ci.Customer_Invoice_Num FROM customer_invoice ci 
            LEFT JOIN customer_refund cr ON cr.Customer_Invoice_Num = ci.Customer_Invoice_Num AND cr.Customer_Invoice_Num!=0
            WHERE $cond
            " ;
        
        return GDb::fetchRow($sql) ;
    }
}
