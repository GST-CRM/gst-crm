<?php

include_once __DIR__ . '/model.php' ;

class TourMembership extends Model {

    public $table = 'tour_membership';
    
    public static $ACTIVE = 1 ;
    public static $INACTIVE = 2 ;
 
    
    public function paxTotal($tourId) {
        $sql = "SELECT COUNT(*) as total FROM customer_account ca 
                        /* LEFT JOIN tour_membership tm ON tm.Customer_Account_Customer_ID = ca.contactid */
                        WHERE ca.tourid=$tourId AND ca.status=1 
                            /* AND IFNULL(tm.status,0) != 2 */";
        return GDb::fetchScalar($sql) ;
    }
    public function paxTotalWithTickets($tourId) {
        $sql = "SELECT COUNT(*) as total FROM customer_account ca 
/*                        LEFT JOIN tour_membership tm ON tm.Customer_Account_Customer_ID = ca.contactid */
                        WHERE ca.tourid=$tourId AND ca.status=1 AND ca.hisownticket=0 
                            /*  AND IFNULL(tm.status,0) != 2*/ ";
        return GDb::fetchScalar($sql) ;
    }
    public function paxTotalWithoutTickets($tourId) {
        $sql = "SELECT COUNT(*) as total FROM customer_account ca 
/*                        LEFT JOIN tour_membership tm ON tm.Customer_Account_Customer_ID = ca.contactid */
                        WHERE ca.tourid=$tourId AND ca.status=1 AND ca.hisownticket=1 
                            /* AND IFNULL(tm.status,0) != 2 */ ";
        return GDb::fetchScalar($sql) ;
    }
}