<?php

include_once __DIR__ . '/model.php' ;

class CustomerRefundLine extends Model {

    public $table = 'customer_refund_line';

    public function findTotal($refundId) {
        $sql  = "SELECT SUM(Issue_Amount) as Total FROM {$this->table} WHERE Refund_ID='$refundId' WHERE Status=1" ;
        return $this->executeScalar($sql) ;
    }
}
