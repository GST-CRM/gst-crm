<?php

include_once __DIR__ . '/model.php' ;

class Contacts extends Model {
    public $table = 'contacts' ;
    public $pk = 'id' ;
    
    public function in($contactIdsInString) {
        $sql = 'SELECT * FROM ' . $this->table . ' WHERE id IN(' . $contactIdsInString . ')' ;
        return $this->executeMany($sql) ;
    }
    
    public function forSelect() {
        $sqlCust  = "(SELECT c.id,fname,mname,lname FROM contacts c
                    INNER JOIN customer_account ca ON c.id = ca.contactid 
                    INNER JOIN `customer_invoice` ci ON ci.Customer_Account_Customer_ID=ca.contactid 
                    INNER JOIN `groups` g ON g.tourid=ca.tourid AND g.status=1 
                    WHERE ca.status=1 AND ci.Status != 0
                    GROUP BY c.id ORDER BY c.fname, c.mname, c.lname)
                    UNION
                    (SELECT c.id,fname,mname,lname FROM contacts c 
                    INNER JOIN `customer_invoice` ci ON ci.Customer_Account_Customer_ID=c.id AND ci.Group_ID=999
                    GROUP BY c.id)";
        return GDb::fetchRowSet($sqlCust);   
    }
    public function getDetail($id) {
        $sqlCust  = "SELECT c.id, fname, mname,lname FROM contacts c WHERE id=$id";
        return GDb::fetchRow($sqlCust);   
    }
    
    public function forInvoicedCustomersList($CustomerID=0) {
		if($CustomerID != 0) { $CustomerIDwhere = " AND ca.contactid=$CustomerID "; } else { $CustomerIDwhere = ""; }
        $sqlCust  = "SELECT c.id,fname,mname,lname FROM contacts c
            INNER JOIN customer_account ca ON c.id = ca.contactid 
            INNER JOIN `groups` g ON g.tourid=ca.tourid AND g.status=1 WHERE ca.invoiced_customer =1 $CustomerIDwhere GROUP BY c.id ORDER BY c.fname, c.mname, c.lname ";
        return GDb::fetchRowSet($sqlCust);
    }
    
    
    public function disableUserType($id, $typeNo) {
        $data = $this->getById($id) ;
        $usertypeupdate = "0";
        if($data["usertype"] == $typeNo || $data["usertype"] == "$typeNo"){
            $usertypeupdate = "0";
        }
        else{
            $usertypeupdate = str_replace(",$typeNo","",$data["usertype"]);
            $usertypeupdate = str_replace("$typeNo,","",$usertypeupdate);
        }

        $con10 = new mysqli($servername, $username, $password, $dbname);
        if ($con10->connect_error) {die("Connection failed: " . $con10->connect_error);} 
        $sql = "UPDATE contacts SET usertype='$usertypeupdate', updatetime=NOW() WHERE id=$id;";
        return $this->execute($sql) ;
    }
    public function customerFullName($record) {
        return $record["title"]." ".$record["fname"]." ".$record["mname"]." ".$record["lname"] ;
    }
}