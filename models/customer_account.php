<?php

include_once __DIR__ . '/model.php' ;
include_once __DIR__ . '/customer_invoice.php' ;

class CustomerAccount extends Model {
    public $table = 'customer_account' ;
    
    public function cancelAccount($tourId, $contactId) {
        $sql = "UPDATE customer_account SET status=0 WHERE contactid=$contactId AND tourid=$tourId LIMIT 1" ;
        return $this->execute($sql) ;
    }
    public function restoreAccount($tourId, $contactId) {
        $sql = "UPDATE customer_account SET status=1 WHERE contactid=$contactId AND tourid=$tourId LIMIT 1" ;
        return $this->execute($sql) ;
    }
    public function detail($email) {
        $sql = "SELECT * FROM customer_account ca
                INNER JOIN contacts c ON ca.contactid = c.id WHERE c.email='$email' " ;
        
        return $this->executeOne($sql) ;
    }
    public function customerGroupCount($customerId) {
        $sql = "SELECT COUNT(*) FROM customer_account WHERE contactid=$customerId AND status=1 " ;
        return $this->executeScalar($sql) ;
    }
    public function customerInvoices($customerId, $status = null ) {
        $statusCond = '' ;
        if( $status ) {
            $statusCond = " AND ca.Status=$status " ;
        }
        $sql = "SELECT Customer_Invoice_Num FROM customer_invoice ci
            INNER JOIN customer_account ca ON ca.contactid = ci.Customer_Account_Customer_ID
            WHERE ca.contactid=$customerId $statusCond " ;
        
        return GDb::fetchRowSet($sql) ;
    }
    public function customerGroups($customerId) {
        $sql = "SELECT tourid FROM customer_account WHERE contactid=$customerId AND Status=1" ;
        $trips = GDb::fetchRowSet($sql) ;
        $groupIds = [] ;
        foreach( $trips as $trip ) {
            $groupIds[] = $trip['tourid'] ;
        }
        return $groupIds ;
    }
}