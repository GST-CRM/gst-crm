<?php

require_once __DIR__ . "/../inc/helpers.php"; 

if( ! defined('SQLNOQUOTE_TOKEN') ) {
    define('SQLNOQUOTE_TOKEN', '$#NOQUOTE#$') ;
}
class Model {
    
    public $table ;
    public $pk ;

    public function setTable($table) {
        $this->table = $table ;
    }
    public function setPk($pk) {
        $this->pk = $pk ;
    }
    public static function self() {
        return new get_class($this) ;
    }

    public static function begin() {
        GDb::execute('BEGIN;') ;
    }
    public static function commit() {
        GDb::execute('COMMIT;') ;
    }
    public static function rollback() {
        GDb::execute('ROLLBACK;') ;
    }

    public function lastInsertId() {
        return GDb::db()->insert_id ;
    }
    public function getLastError() {
        return GDb::db()->error ;
    }
    public function getList($where = '') {
        $whereCondition = $this->buildWhereClause($where) ;
        
        $sql = "SELECT * FROM " . $this->table . " $whereCondition" ;
        
        return $this->executeMany($sql) ;
    }
    public function GetListWithGroupBy($where = '') {
        $whereCondition = $this->buildWhereClause($where) ;
        
        $sql = "SELECT Customer_Payment_ID, Customer_Invoice_Num, Customer_Payment_Date, Customer_Account_Customer_ID, Customer_Payment_Sales_Order_ID, SUM(Customer_Payment_Amount) AS Customer_Payment_Amount, Customer_Payment_Method, Customer_Payment_Comments, Transaction_Type_Transaction_ID, SUM(Charges) AS Charges, Payment_Unique_ID, Status, Add_Date, Mod_Date FROM " . $this->table . " $whereCondition GROUP BY Payment_Unique_ID ORDER BY Customer_Payment_Date ASC " ;
        
        return $this->executeMany($sql) ;
    }
    public function get($where = '', $columns = '*') {
        $whereCondition = $this->buildWhereClause($where) ;
        
        $sql = "SELECT $columns FROM " . $this->table . " $whereCondition" ;
        
        return $this->executeOne($sql) ;
    }
    public function getScalar($column, $where = '') {
        $row = $this->get($where) ;
        if( is_array($row) ) {
            if( isset($row[$column]) ) {
                return $row[$column] ;
            }
        }
        return null ;
    }
    public function count($where = '', $countWhere = 'COUNT(*) AS total') {
        $whereCondition = $this->buildWhereClause($where) ;
        
        $sql = "SELECT $countWhere FROM " . $this->table . " $whereCondition" ;
        
        return $this->executeScalar($sql) ;
    }
    public function getById($id) {
        $whereCondition = " WHERE " . $this->pk . " = '" . $id . "' LIMIT 1" ;
        $sql = "SELECT * FROM " . $this->table . " $whereCondition" ;
        
        return $this->executeOne($sql) ;
    }
    
    function upsert( $data, $where ) {
        if( $this->count($where) > 0 ) {
            return $this->update($data, $where) ;
        }
        else {
            return $this->insert($data) ;
        }
    }
    
    function insert($data)
	{
		$sql = "INSERT INTO `{$this->table}` " ;
		$keys = "`" . implode("`,`", array_keys($data)) . "`" ;
		
		$valuestr = '' ;
		foreach( $data as $v )
		{
			if( $valuestr )
			{
				$valuestr .= ', ' ;
			}
			if( strpos($v, SQLNOQUOTE_TOKEN) === 0 )
			{
				$valuestr .= str_replace(SQLNOQUOTE_TOKEN, '', $v) ;
			}
			else
			{
				$valuestr .= "'" . $v . "'" ;
			}
		}

		$sql .= '(' . $keys . ')' ;
		$sql .= ' VALUES(' . $valuestr. ')' ;
		return GDb::execute($sql) ;
	}
    function delete($where)
	{
		//NOTE;There is not related deletion. Please set delete cascade in database table.
		$whereStr = $this->buildWhereClause($where) ;
		$sql = "DELETE FROM `{$this->table}` $whereStr" ;
		if( GDb::execute($sql) )
		{
			return true ;
		}
		return false ;
	}
    function update($data, $where )
	{
		$sql = "UPDATE `{$this->table}` " ;
		
		$u = '' ;
		foreach( $data as $k => $v )
		{
			if( $u )
			{
				$u .= ', ' ;
			}
			$va = $v ;
			if( strpos($va, SQLNOQUOTE_TOKEN) === 0 )
			{
				$va = str_replace(SQLNOQUOTE_TOKEN, '', $va) ;
			}
			else
			{
				$va = "'" . $va . "'" ;
			}

			$u .= "$k = $va" ;
		}
		$sql .= ' SET ' . $u ;
		$sql .= ' ' . $this->buildWhereClause( $where ) ;

		return GDb::execute($sql) ;
	}
    public static function sqlNoQuote($value)
    {
        return SQLNOQUOTE_TOKEN . $value ;
    }
    public function executeOne($sql) {
        return GDb::fetchRow($sql) ;
    }
    
    public function execute($sql) {
        return GDb::execute($sql) ;
    }
    
    public function executeMany($sql) {
        return GDb::fetchRowSet($sql) ;
    }
    public function executeScalar($sql) {
        return GDb::fetchScalar($sql) ;
    }
    private function buildWhereClause($where)
	{
		$whereStr = '' ;
		//build where... {
		if( is_array($where) )
		{
			foreach( $where as $k => $v )
			{
				if( $whereStr )
				{
					$whereStr .= ' AND ' ;
				}
				$whereStr .= " $k='$v' " ;
			}
		}
		else if( $where )
		{
			$whereStr = $where ;
		}
		//}
		if( $whereStr )
		{
			$whereStr = ' WHERE ' . $whereStr ;
		}
		return $whereStr ;
	}
    
    public static function flush() {
        $db = GDb::db() ;
        do {
            if ($res = $db->store_result()) {
                $res->free();
            }
        } while ($db->more_results() && $db->next_result());
    
    }
}