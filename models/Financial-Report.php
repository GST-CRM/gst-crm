<?php

include_once __DIR__ . '/model.php';

class FinancialReport extends Model {

    public $table = 'customer_account';

    public function GetGroupPaxData( $Tour_ID = '', $Airline_Credit = 0, $Product_Land_Price = 0, $Product_Land_Cost = 0, $Product_Air_Price = 0, $Product_Air_Cost = 0, $General_Expenses = 0 ) {

        $GetGroupPaxData = "SELECT
            '$Product_Land_Price' AS Land_Price,
            '$Product_Land_Cost' AS Land_Cost,
            '$Product_Air_Price' AS Air_Price,
            '$Product_Air_Cost' AS Air_Cost,
            '$Airline_Credit' AS Ticket_Credit,
            '$General_Expenses' AS TotalExpenses,
            co.id,
            ci.Customer_Invoice_Num,
            REPLACE(CONCAT(co.fname,' ',co.mname,' ',co.lname), '  ', ' ') AS Customer_Name,
            if(ca.hisownticket=1,'$Airline_Credit','0.00') AS Airline_Credit,
            if(ca.roomtype='Single',(SELECT pro2.cost FROM products pro JOIN groups gro ON gro.land_productid=pro.id LEFT JOIN products pro2 ON pro2.id=pro.Product_Reference WHERE gro.tourid=$Tour_ID),'0.00') AS Single_Room_Cost,
            if(ca.roomtype='Single',(SELECT pro2.price FROM products pro JOIN groups gro ON gro.land_productid=pro.id LEFT JOIN products pro2 ON pro2.id=pro.Product_Reference WHERE gro.tourid=$Tour_ID),'0.00') AS Single_Room_Price,
            if(ca.insurance=1,(SELECT cost FROM products WHERE id=ca.Insurance_Product_ID),'0.00') AS Insurance_Cost,
            if(ca.insurance=1,(SELECT price FROM products WHERE id=ca.Insurance_Product_ID),'0.00') AS Insurance_Price,
            if(ca.upgrade=1,if(ca.upgradeproduct>0,(SELECT cost FROM products WHERE id=ca.upgradeproduct),'0.00'),'0.00') AS Air_Upgrade_Cost,
            if(ca.upgrade=1,if(ca.upgradeproduct>0,(SELECT price FROM products WHERE id=ca.upgradeproduct),'0.00'),'0.00') AS Air_Upgrade_Price,
            if(ca.upgrade=1,if(ca.upgrade_land_product>0,(SELECT cost FROM products WHERE id=ca.upgrade_land_product),'0.00'),'0.00') AS Land_Upgrade_Cost,
            if(ca.upgrade=1,if(ca.upgrade_land_product>0,(SELECT price FROM products WHERE id=ca.upgrade_land_product),'0.00'),'0.00') AS Land_Upgrade_Price,
            if(ca.Transfer=1,(SELECT cost FROM products WHERE id=ca.Transfer_Product_ID),'0.00') AS Transfer_Cost,
            if(ca.Transfer=1,(SELECT price FROM products WHERE id=ca.Transfer_Product_ID),'0.00') AS Transfer_Price,
            if(ca.Extension=1,(SELECT cost FROM products WHERE id=ca.Extension_Product_ID),'0.00') AS Extension_Cost,
            if(ca.Extension=1,(SELECT price FROM products WHERE id=ca.Extension_Product_ID),'0.00') AS Extension_Price,
            if(co.id=cg.Primary_Customer_ID,'Yes',cg.Primary_Customer_ID) AS Joint_Invoice,
            if(cp.Customer_Payment_Amount>0,(cp.Customer_Payment_Amount-cp.Charges),'0.00') AS Paid_Amount,
            if(cn.Credit_Amount>0,cn.Credit_Amount,'0.00') AS Credit_Amount,
            cn.Full_Discount AS Complimentary,
            if(cr.Refund_Amount>0,cr.Refund_Amount,'0.00') AS Refund_Amount,
            if(cr.Cancellation_Charge>0,cr.Cancellation_Charge,'0.00') AS Cancellation_Charge,
            ga.Agent_Fees,
            ca.status
        FROM customer_account ca 
        JOIN contacts co 
        ON co.id=ca.contactid
        JOIN customer_invoice ci
        ON ci.Customer_Account_Customer_ID=co.id AND ci.Group_ID=$Tour_ID
        LEFT JOIN (
            SELECT
                SUM(cp.Customer_Payment_Amount) AS Customer_Payment_Amount,
                SUM(cp.Charges) AS Charges,
                cp.Customer_Account_Customer_ID
            FROM customer_payments cp
            WHERE cp.Group_ID=$Tour_ID
            GROUP BY cp.Customer_Account_Customer_ID
        ) AS cp ON cp.Customer_Account_Customer_ID=co.id
        LEFT JOIN (
            SELECT SUM(
                cn.Credit_Amount) AS Credit_Amount,
                cn.Customer_Account_Customer_ID,
                cn.Customer_Invoice_Num,
                cn.Full_Discount
            FROM customer_credit_note cn
            WHERE cn.Status=1
            GROUP BY cn.Customer_Account_Customer_ID
        ) AS cn ON cn.Customer_Account_Customer_ID=co.id AND cn.Customer_Invoice_Num=ci.Customer_Invoice_Num
        LEFT JOIN (
            SELECT SUM(ga.commissionvalue) AS Agent_Fees,
            ga.group_id
            FROM group_agents ga
            WHERE ga.group_id=$Tour_ID AND ga.incometype='Commission'
        ) AS ga ON ga.group_id=ca.tourid
        LEFT JOIN (
            SELECT
                SUM(crl.Amount) AS Refund_Amount,
            cr.Customer_Account_Customer_ID,
            cr.Customer_Invoice_Num,
            cr.Cancellation_Charge
            FROM customer_refund cr 
            JOIN customer_refund_line crl
            ON crl.Refund_ID=cr.Refund_ID
            WHERE 1
            GROUP BY cr.Customer_Account_Customer_ID
        ) AS cr ON cr.Customer_Account_Customer_ID=co.id
        LEFT JOIN (SELECT 
            Primary_Customer_ID,
            Additional_Traveler_ID_1,
            Additional_Traveler_ID_2,
            Additional_Traveler_ID_3,
            Additional_Traveler_ID_4
        FROM customer_groups
        WHERE Type='Group Invoice' AND Group_ID=$Tour_ID) AS cg
        ON co.id IN
            (cg.Primary_Customer_ID,
             cg.Additional_Traveler_ID_1,
             cg.Additional_Traveler_ID_2,
             cg.Additional_Traveler_ID_3,
             cg.Additional_Traveler_ID_4)
        WHERE ca.tourid=$Tour_ID
        GROUP BY co.id
        ORDER BY status DESC, Joint_Invoice DESC";
        $GroupPaxData = Gdb::FetchRowSet( $GetGroupPaxData );
        return $GroupPaxData;
    }

    public function GetGroupData( $Tour_ID ) {
        $GetGroupDetails = "SELECT
            gr.tourid AS Trip_ID,
            gr.tourdesc AS Trip_Reference,
            gr.listprice,
            gr.airline_productid AS Air_ID,
            pro1.price AS Air_Price,
            pro1.cost AS Air_Cost,
            if(pro1.Product_Reference>0,pro3.price,'0') AS Ticket_Credit,
            gr.land_productid AS Land_ID,
            pro2.price AS Land_Price,
            pro2.cost AS Land_Cost,
            (SELECT SUM(ex.Expense_Amount) FROM expenses ex WHERE ex.Group_ID=gr.tourid) AS TotalExpenses,
            gr.startdate,
            gr.enddate
        FROM groups gr
        LEFT JOIN products pro1
        ON pro1.id=gr.airline_productid
        LEFT JOIN products pro2
        ON pro2.id=gr.land_productid
        LEFT JOIN products pro3
        ON pro3.id=pro1.Product_Reference
        WHERE gr.tourid=$Tour_ID";
        $GroupDetails = Gdb::FetchRow( $GetGroupDetails );

        $Product_Land_Price = $GroupDetails['Land_Price'];
        $Product_Land_Cost = $GroupDetails['Land_Cost'];
        $Product_Air_Price = $GroupDetails['Air_Price'];
        $Product_Air_Cost = $GroupDetails['Air_Cost'];
        $Airline_Credit = $GroupDetails['Ticket_Credit'];
        $General_Expenses = $GroupDetails['TotalExpenses'];

        //return $GroupDetails;
        return $this->GetGroupPaxData( $Tour_ID, $Airline_Credit, $Product_Land_Price, $Product_Land_Cost, $Product_Air_Price, $Product_Air_Cost, $General_Expenses );
    }

    public function GetGroupSum( $Tour_ID ) {
        
        $GroupPaxList = array();
        $Total_PackagePrice = 0;
        $Total_AirCost = 0;
        $Total_LandCost = 0;
        $Total_ExtraCost = 0;
        $Total_FullCost = 0;
        $Total_PaidAmount = 0;
        $Total_CreditAmount = 0;
        $Total_RefundAmount = 0;
        $Total_Balance = 0;
        $Total_Profit = 0;
        
        $GroupPax = $this->GetGroupData( $Tour_ID );
        
        foreach ( $GroupPax AS $EachPax ) {
            //Make the calculations of the Extra Expenses
            $CostCredit = $EachPax['Airline_Credit'];
            $CostSingleRoom = $EachPax['Single_Room_Cost'];
            $CostAir = $EachPax['Air_Upgrade_Cost'];
            $CostLand = $EachPax['Land_Upgrade_Cost'];
            $CostInsurance = $EachPax['Insurance_Cost'];
            $CostTransfer = $EachPax['Transfer_Cost'];
            $CostExtension = $EachPax['Extension_Cost'];
            $Product_Land_Price = $EachPax['Land_Price'];
            $Product_Land_Cost = $EachPax['Land_Cost'];
            $Product_Air_Price = $EachPax['Air_Price'];
            $Product_Air_Cost = $EachPax['Air_Cost'];
            $Airline_Credit = $EachPax['Ticket_Credit'];
            $General_Expenses = $EachPax['TotalExpenses'];
            $Agent_Fees = $EachPax['Agent_Fees'];
            $Cost_Total_Extra = $CostCredit+$CostAir+$CostLand+$CostInsurance+$CostTransfer+$CostExtension+$CostSingleRoom;
            $Cost_Total = $Product_Air_Cost+$Product_Land_Cost+$Cost_Total_Extra;

            //Make the calculations of the Extra Prices
            $PriceCredit = $EachPax['Airline_Credit'];
            $PriceSingleRoom = $EachPax['Single_Room_Price'];
            $PriceAir = $EachPax['Air_Upgrade_Price'];
            $PriceLand = $EachPax['Land_Upgrade_Price'];
            $PriceInsurance = $EachPax['Insurance_Price'];
            $PriceTransfer = $EachPax['Transfer_Price'];
            $PriceExtension = $EachPax['Extension_Price'];
            $Price_Total_Extra = $PriceCredit+$PriceAir+$PriceLand+$PriceInsurance+$PriceTransfer+$PriceExtension+$PriceSingleRoom;
            $Price_Total = $Product_Land_Price+$Product_Air_Price+$Price_Total_Extra;

            //Calculations
            $Balance = $Price_Total - ( $EachPax['Paid_Amount']-$EachPax['Refund_Amount'] ) - $EachPax['Credit_Amount'];
            $Profit = $Price_Total - $Cost_Total;

            //The new way, two arrays concept!!!!
            if ( $EachPax['status'] == 1 AND is_numeric( $EachPax['Joint_Invoice'] ) AND $EachPax['Joint_Invoice'] > 0 ) {
                $GroupPaxList[$EachPax['Joint_Invoice']]['Package_Price'] += $Product_Land_Price+$Product_Air_Price+$Price_Total_Extra;
                $GroupPaxList[$EachPax['Joint_Invoice']]['Product_Air_Cost'] += $Product_Air_Cost;
                $GroupPaxList[$EachPax['Joint_Invoice']]['Product_Land_Cost'] += $Product_Land_Cost;
                $GroupPaxList[$EachPax['Joint_Invoice']]['Extra_Cost'] += $Cost_Total_Extra+$Agent_Fees;
                $GroupPaxList[$EachPax['Joint_Invoice']]['Extra_Price'] += $Price_Total_Extra;
                $GroupPaxList[$EachPax['Joint_Invoice']]['Paid_Amount'] += $EachPax['Paid_Amount'];
                $GroupPaxList[$EachPax['Joint_Invoice']]['Credit_Amount'] += $EachPax['Credit_Amount'];
                $GroupPaxList[$EachPax['Joint_Invoice']]['Refund_Amount'] += $EachPax['Refund_Amount'];

                $GroupPaxList[$EachPax['id']]['Customer_ID'] = $EachPax['id'];
                $GroupPaxList[$EachPax['id']]['Package_Price'] += 0;
                $GroupPaxList[$EachPax['id']]['Product_Air_Cost'] += 0;
                $GroupPaxList[$EachPax['id']]['Product_Land_Cost'] += 0;
                $GroupPaxList[$EachPax['id']]['Extra_Cost'] += 0;
                $GroupPaxList[$EachPax['id']]['Extra_Price'] += 0;
                $GroupPaxList[$EachPax['id']]['Paid_Amount'] += 0;
                $GroupPaxList[$EachPax['id']]['Credit_Amount'] += 0;
                $GroupPaxList[$EachPax['id']]['Refund_Amount'] += 0;
                $GroupPaxList[$EachPax['id']]['Joint_Invoice'] = 'Joint';

            } elseif ( $EachPax['status'] == 1 AND $EachPax['Joint_Invoice'] == 'Yes' ) {
                $GroupPaxList[$EachPax['id']]['Customer_ID'] = $EachPax['id'];
                $GroupPaxList[$EachPax['id']]['Package_Price'] += $Product_Land_Price+$Product_Air_Price+$Price_Total_Extra;
                $GroupPaxList[$EachPax['id']]['Product_Air_Cost'] += $Product_Air_Cost;
                $GroupPaxList[$EachPax['id']]['Product_Land_Cost'] += $Product_Land_Cost;
                $GroupPaxList[$EachPax['id']]['Extra_Cost'] += $Cost_Total_Extra+$Agent_Fees;
                $GroupPaxList[$EachPax['id']]['Extra_Price'] += $Price_Total_Extra;
                $GroupPaxList[$EachPax['id']]['Paid_Amount'] += $EachPax['Paid_Amount'];
                $GroupPaxList[$EachPax['id']]['Credit_Amount'] += $EachPax['Credit_Amount'];
                $GroupPaxList[$EachPax['id']]['Refund_Amount'] += $EachPax['Refund_Amount'];
                $GroupPaxList[$EachPax['id']]['Joint_Invoice'] = 'Primary';

            } elseif ( $EachPax['status'] == 1 ) {
                $GroupPaxList[$EachPax['id']]['Customer_ID'] = $EachPax['id'];
                $GroupPaxList[$EachPax['id']]['Package_Price'] += $Product_Land_Price+$Product_Air_Price+$Price_Total_Extra;
                $GroupPaxList[$EachPax['id']]['Product_Air_Cost'] += $Product_Air_Cost;
                $GroupPaxList[$EachPax['id']]['Product_Land_Cost'] += $Product_Land_Cost;
                $GroupPaxList[$EachPax['id']]['Extra_Cost'] += $Cost_Total_Extra+$Agent_Fees;
                $GroupPaxList[$EachPax['id']]['Extra_Price'] += $Price_Total_Extra;
                $GroupPaxList[$EachPax['id']]['Paid_Amount'] += $EachPax['Paid_Amount'];
                $GroupPaxList[$EachPax['id']]['Credit_Amount'] += $EachPax['Credit_Amount'];
                $GroupPaxList[$EachPax['id']]['Refund_Amount'] += $EachPax['Refund_Amount'];
                $GroupPaxList[$EachPax['id']]['Joint_Invoice'] = 'Alone';
            } else {
                $GroupPaxList[$EachPax['id']]['Customer_ID'] = $EachPax['id'];
                $GroupPaxList[$EachPax['id']]['Customer_Name'] = $EachPax['Customer_Name'];
                $GroupPaxList[$EachPax['id']]['Package_Price'] = $EachPax['Cancellation_Charge'];
                $GroupPaxList[$EachPax['id']]['Product_Air_Cost'] = 0;
                $GroupPaxList[$EachPax['id']]['Product_Land_Cost'] = 0;
                $GroupPaxList[$EachPax['id']]['Extra_Cost'] = 0;
                $GroupPaxList[$EachPax['id']]['Extra_Price'] = 0;
                $GroupPaxList[$EachPax['id']]['Paid_Amount'] += $EachPax['Paid_Amount'];
                //$GroupPaxList[$EachPax['id']]['Credit_Amount'] += $EachPax['Credit_Amount'];
                //I am commented this field, because the credit note is a discount, and since it is cancelled, the discount is useless
                $GroupPaxList[$EachPax['id']]['Credit_Amount'] = 0;
                $GroupPaxList[$EachPax['id']]['Refund_Amount'] += $EachPax['Refund_Amount'];
                $GroupPaxList[$EachPax['id']]['Joint_Invoice'] = 'Cancelled';
            }
        }
        foreach ( $GroupPaxList AS $EachGroupPax ) {
            $Status = $EachGroupPax['Joint_Invoice'];
            $Total_PackagePrice += $EachGroupPax['Package_Price'];
            $Total_AirCost += $EachGroupPax['Product_Air_Cost'];
            $Total_LandCost += $EachGroupPax['Product_Land_Cost'];
            $Total_ExtraCost += $EachGroupPax['Extra_Cost'];
            $Total_FullCost += $EachGroupPax['Product_Air_Cost'] + $EachGroupPax['Product_Land_Cost'] + $EachGroupPax['Extra_Cost'];
            $Total_PaidAmount += $EachGroupPax['Paid_Amount'];
            $Total_CreditAmount += $EachGroupPax['Credit_Amount'];
            $Total_RefundAmount += $EachGroupPax['Refund_Amount'];
            $Total_Balance += $EachGroupPax['Package_Price'] - ( $EachGroupPax['Paid_Amount']-$EachGroupPax['Refund_Amount'] ) - $EachGroupPax['Credit_Amount'];

            if ( $EachGroupPax['Joint_Invoice'] == 'Cancelled' OR $EachGroupPax['Joint_Invoice'] == 'Joint' ) {
                $Total_Profit += $EachGroupPax['Package_Price']-($EachGroupPax['Extra_Cost']+$EachGroupPax['Credit_Amount']);
            } else {
                $Total_Profit += $EachGroupPax['Package_Price']-($EachGroupPax['Product_Air_Cost']+$EachGroupPax['Product_Land_Cost']+$EachGroupPax['Extra_Cost']+$EachGroupPax['Credit_Amount']);
            }

        }
        $GroupSums['Total_PackagePrice'] = $Total_PackagePrice;
        //$GroupSums['Total_AirCost'] = $Total_AirCost;
        //$GroupSums['Total_LandCost'] = $Total_LandCost;
        //$GroupSums['Total_ExtraCost'] = $Total_ExtraCost;
        $GroupSums['Total_GeneralExpense'] = $General_Expenses;
        $GroupSums['Total_FullCost'] = $Total_FullCost;
        $GroupSums['Total_PaidAmount'] = $Total_PaidAmount;
        $GroupSums['Total_CreditAmount'] = $Total_CreditAmount;
        $GroupSums['Total_RefundAmount'] = $Total_RefundAmount;
        $GroupSums['Total_Balance'] = $Total_Balance;
        $GroupSums['Total_Profit'] = $Total_Profit;
        
        return $GroupSums;
    }
    
    public function GetGroupsPerMonth($Month = 1, $Year = 2020) {
        
        $GetGroupsFromDate = "SELECT
            gro.tourid, 
            gro.tourdesc,
            gro.Church_Name,
            gro.startdate,
            gro.enddate,
            COUNT(ca.contactid) AS PaxTotal
        FROM groups gro
        LEFT JOIN customer_account ca
        ON ca.tourid=gro.tourid AND ca.status != 0
        WHERE MONTH(gro.startdate)=$Month AND YEAR(gro.startdate)=$Year
        GROUP BY gro.tourid";
        
        $GroupsListTime = Gdb::FetchRowSet( $GetGroupsFromDate );
        $List_Groups_Custom = array();
        $x = 0;
        
        foreach($GroupsListTime AS $EachGroup) {
            $List_Groups_Custom[$x]['Tour_ID'] = $EachGroup['tourid'];
            $List_Groups_Custom[$x]['Tour_Reference'] = $EachGroup['tourdesc'];
            $List_Groups_Custom[$x]['Tour_Church_Name'] = $EachGroup['Church_Name'];
            $List_Groups_Custom[$x]['Tour_Pax_Count'] = $EachGroup['PaxTotal'];
            $List_Groups_Custom[$x]['Tour_Start_Date'] = $EachGroup['startdate'];
            $List_Groups_Custom[$x]['Tour_End_Date'] = $EachGroup['enddate'];
            
            $SpecficGroup = $this->GetGroupSum($List_Groups_Custom[$x]['Tour_ID']);
            
            $List_Groups_Custom[$x]['Tour_PackagePrice'] = $SpecficGroup['Total_PackagePrice'];
            $List_Groups_Custom[$x]['Tour_GeneralExpense'] = ($SpecficGroup['Total_GeneralExpense'] > 0 ? $SpecficGroup['Total_GeneralExpense'] : 0.00);
            $List_Groups_Custom[$x]['Tour_FullCost'] = $SpecficGroup['Total_FullCost'];
            $List_Groups_Custom[$x]['Tour_PaidAmount'] = $SpecficGroup['Total_PaidAmount'];
            $List_Groups_Custom[$x]['Tour_CreditAmount'] = $SpecficGroup['Total_CreditAmount'];
            $List_Groups_Custom[$x]['Tour_RefundAmount'] = $SpecficGroup['Total_RefundAmount'];
            $List_Groups_Custom[$x]['Tour_Balance'] = $SpecficGroup['Total_Balance'];
            $List_Groups_Custom[$x]['Tour_Profit'] = $SpecficGroup['Total_Profit'];
            
            $x++;
        }
        
        return $List_Groups_Custom;
    }
    
    public function GetGroupsPerYear($Year = 2020) {
        
        $MonthlyRecords = array();
        
        for($x = 1; $x <= 12; $x++) {
            $MonthlyRecords[$x]['Month'] = date('F', mktime(0, 0, 0, $x, 10));
            $MonthlyRecords[$x]['Total_PackagePrice'] = 0;
            $MonthlyRecords[$x]['General_Expenses'] = 0;
            $MonthlyRecords[$x]['Total_FullCost'] = 0;
            $MonthlyRecords[$x]['Total_PaidAmount'] = 0;
            $MonthlyRecords[$x]['Total_CreditAmount'] = 0;
            $MonthlyRecords[$x]['Total_RefundAmount'] = 0;
            $MonthlyRecords[$x]['Total_Balance'] = 0;
            $MonthlyRecords[$x]['Total_Profit'] = 0;
            
            $Monthly_Groups = $this->GetGroupsPerMonth($x, $Year);
            $MonthlyRecords[$x]['Total_Groups'] = count($Monthly_Groups);
            
            foreach($Monthly_Groups AS $EachGroup) {
                $MonthlyRecords[$x]['Total_PackagePrice'] += $EachGroup['Tour_PackagePrice'];
                $MonthlyRecords[$x]['General_Expenses'] += $EachGroup['Tour_GeneralExpense'];
                $MonthlyRecords[$x]['Total_FullCost'] += $EachGroup['Tour_FullCost'];
                $MonthlyRecords[$x]['Total_PaidAmount'] += $EachGroup['Tour_PaidAmount'];
                $MonthlyRecords[$x]['Total_CreditAmount'] += $EachGroup['Tour_CreditAmount'];
                $MonthlyRecords[$x]['Total_RefundAmount'] += $EachGroup['Tour_RefundAmount'];
                $MonthlyRecords[$x]['Total_Balance'] += $EachGroup['Tour_Balance'];
                $MonthlyRecords[$x]['Total_Profit'] += $EachGroup['Tour_Profit'];

            } 
        }
        
        return $MonthlyRecords;
    }
}