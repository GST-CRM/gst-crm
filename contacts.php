<?php
include "session.php";
$PageTitle = "Contact List";
include "header.php"; 

if(isset($_GET['action'])){
	$action = mysqli_real_escape_string($db, $_GET['action']);
} else {
	$action = "";
}

//To show the Update success notification with the url
if ($action == "created") {
	$fullname = $_GET["fullname"];
	echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The contact ".$fullname." has been created successfully!</strong></div>";
}
if ($action == "update") {
	$fullname = $_GET["fullname"];
	echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The contact ".$fullname." has been updated successfully!</strong></div>";
}
if ($action == "delete") {
	$fullname = $_GET["fullname"];
	$contactid = $_GET["cid"];
	echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The contact ".$fullname." (id# ".$contactid.") has been deleted!</strong></div>";
}
?>
<!--<script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>	 -->
<style type="text/css">
#employee_grid_filter { display:none;}
</style>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
		<div class="card">
            <div class="row">
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input type="text" class="outsideBorderSearch form-control" placeholder="Search..." value="<?php if(isset($_POST['GeneralSearchInput'])) { echo $_POST['GeneralSearchInput']; } ?>" tabindex=1>
					</div>
				</div>
				<?php echo GUtils::flashMessage() ; ?>
				<div class="col-md-6">
				</div>
				<div class="col-md-3" style="margin-top:15px;">
					<a href="contacts-add.php" class="btn waves-effect waves-light btn-success" style="margin-right: 30px;float:right; padding: 3px 13px;"><i class="far fa-check-circle"></i>Add New Contact</a>
				</div>
			</div>
            <div class="card-block">
				<div class="dt-responsive table-responsive">
				<!-- basic-btn -->
                    <table id="employee_grid" class="table table-hover table-striped table-bordered nowrap responsive" style="width: 100% !important;" data-page-length="100">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Full Name</th>
                                <th>First Name</th>
                                <th>Middle Name</th>
                                <th>Last Name</th>
                                <th>Phone</th>
                                <th>Mobile</th>
                                <th>Email</th>
                                <th>Company</th>
                                <th>Street Address</th>
                                <th>Additional Address</th>
                                <th>Zip Code</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Country</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<!-- [ page content ] end -->
<script>
//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#employee_grid').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
});


$( document ).ready(function() {
	$('#employee_grid').DataTable({
		"bProcessing": true,
		"serverSide": true,
		dom: 'Bflrtip',
		"columnDefs": [
		{ 
			//"visible": false,
			"targets": 1,
			"render": function(data, type, row, meta){
				if(type === 'display'){
					data = '<a href="contacts-edit.php?id=' + row[0] + '&action=edit" style="color: #0000EE;">' + row[1] + ' ' + row[2] + ' ' + row[3] + ' ' + row[4] + '</a>';
				}
				return data;
			}
		},
		{ 
			"visible": false,
			"targets": [2,3,4,5,8,9,10,11]
		}],
		buttons: ['csv', 'excel'],
		"ajax":{
			url :"inc/contact-list.php", // json datasource
			type: "post",  // type of method  , by default would be get
			error: function(){  // error handling code
				$("#employee_grid_processing").css("display","none");
			}
		}
		<?php if(isset($_POST['GeneralSearchInput'])) { ?>
		,
		"search": {
			"search": "<?php echo $_POST['GeneralSearchInput']; ?>"
		}
		<?php } ?>
        });	
		$('div.dataTables_filter input').focus();
});
</script>
<?php include "footer.php"; ?>