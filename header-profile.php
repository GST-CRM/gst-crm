<?php
include_once __DIR__ . '/models/user_favorites.php';
UserFavorites::handleFavoriteAction($PageTitle) ;

?>
<nav class="navbar header-navbar pcoded-header">
                <div class="navbar-wrapper">
                    <div class="navbar-logo">
                        <a href="dashboard.php">
                            <img class="img-fluid" src="files/assets/images/logo.png" alt="Theme-Logo" />
                        </a>
                        <a class="mobile-menu" id="mobile-collapse" href="#!">
                            <i class="feather icon-menu"></i>
                        </a>
                        <a class="mobile-options waves-effect waves-light">
                            <i class="feather icon-more-horizontal"></i>
                        </a>
                    </div>
                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
                            <li <?php if(basename($_SERVER['PHP_SELF']) == "groups-edit.php") { ?> class="header-screen" <?php } ?>>
                                <a href="#!" onclick="javascript:toggleFullScreen()" class="waves-effect waves-light">
                                    <i class="full-screen feather icon-maximize"></i>
                                </a>
                            </li>
                            <li class="header-search">
                                <div class="main-search morphsearch-search">
									<form id="contact6" name="GeneralSearchForm" action="contacts.php" method="post">
										<div class="input-group">
											<span class="input-group-prepend search-close">
												<i class="feather icon-x input-group-text"></i>
											</span>
											<input name="GeneralSearchInput" type="text" class="form-control" placeholder="Enter Keyword">
											<span class="input-group-append search-btn">
												<i class="feather icon-search input-group-text"></i>
											</span>
										</div>
									</form>
                                </div>
                            </li>
                            <li style="font-weight:bold;margin-top: 3px;">
                                <a href="changelog.php" style="font-size:.875rem;"><?php echo $GST_CRM_Version; ?></a>
                            </li>
                        </ul>
                        <ul class="nav-right">
                            <li class="header-notification d-none">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="feather icon-bell"></i>
                                        <span class="badge bg-c-red">5</span>
                                    </div>
                                    <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        <li>
                                            <h6>Notifications</h6>
                                            <label class="label label-danger">New</label>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="img-radius" src="" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">John Doe</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="img-radius" src="" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">Joseph William</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="img-radius" src="" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">Sara Soudein</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li class="header-notification d-none">
                                <div class="dropdown-primary dropdown">
                                    <div class="displayChatbox dropdown-toggle" data-toggle="dropdown">
                                        <i class="feather icon-message-square"></i>
                                        <span class="badge bg-c-green">3</span>
                                    </div>
                                </div>
                            </li>
                            <li class="user-profile header-notification">
                                <div class="dropdown-primary dropdown">
                                    <?php
                                        $userFavorite = new UserFavorites() ;
                                        $query = '' ;
                                        $favorite_title;
                                        $favorite_icon ;
                                        $symbol = '?' ;
                                        if( stripos(GUtils::requestUri(), '?') !== false ) {
                                            $symbol = '&' ;
                                        }
                                        if( $userFavorite->isRequestFavorite() ) {
                                            $query = UserFavorites::requestUriToPrint() . $symbol . 'favorite-action=remove&favorite-uri=' . urlencode(UserFavorites::requestUriToStore()) ;
                                            $favorite_title = 'Remove Favorite' ;
                                            $favorite_icon = 'icon-star-on' ;
                                        }
                                        else {
                                            $query = UserFavorites::requestUriToPrint() . $symbol . 'favorite-action=add&favorite-uri=' . urlencode(UserFavorites::requestUriToStore()) ;
                                            $favorite_title = 'Add Favorite' ;
                                            $favorite_icon = 'icon-star' ;
                                        }
                                        ?>
                                    <div>
                                        <a href="<?php echo $query;?>">
                                            <i title="Favorite Pages" class="icon feather <?php echo $favorite_icon;?>"></i>
                                        </a>
                                    </div>
                                    
                                </div>
                            </li>
                            <li class="user-profile header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="icon feather icon-settings"></i>
                                    </div>
                                    <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        <li class="drp-u-details">
											<?php if($UserProfile == "") { ?>
												<img src="files/assets/images/default-user-thumb.jpg" class="img-radius" alt="User-Profile-Image">
                                            <?php } else { ?>
											<img src="uploads/profile/<?php echo $UserProfile; ?>" class="img-radius" alt="User-Profile-Image">
                                            <?php } ?>
                                            <span><?php echo $adminname; ?></span>
                                            <a href="logout.php" class="dud-logout" title="Logout">
                                                <i class="feather icon-log-out"></i>
                                            </a>
                                        </li>
                                        <?php
                                        global $G_ROLE_ID ;

                                        if( GUtils::maskCount($G_ROLE_ID) > 1 ) {
                                            
                                            if($G_ROLE_ID & AclRole::$ADMIN ) {
                                        ?>
                                        <li>
                                            <a href="account-switch.php?account-switch=admin">
                                                <i class="feather icon-user"></i> Admin
                                            </a>
                                        </li>
                                        <?php
                                            }
                                            if($G_ROLE_ID & AclRole::$AGENT ) {
                                        ?>
                                        <li>
                                            <a href="account-switch.php?account-switch=agent">
                                                <i class="feather icon-user"></i> Agent
                                            </a>
                                        </li>
                                        <?php
                                            }
                                            if($G_ROLE_ID & AclRole::$LEADER ) {
                                            ?>
                                        <li>
                                            <a href="account-switch.php?account-switch=leader">
                                                <i class="feather icon-user"></i> Group Leader
                                            </a>
                                        </li>
                                        <?php 
                                            }
                                            if($G_ROLE_ID & AclRole::$SUPPLIER ) {
                                            ?>
                                        <li>
                                            <a href="account-switch.php?account-switch=supplier">
                                                <i class="feather icon-user"></i> Supplier
                                            </a>
                                        </li>
                                        <?php 
                                            }
                                            if($G_ROLE_ID & AclRole::$CUSTOMER) {
                                            ?>
                                        <li>
                                            <a href="account-switch.php?account-switch=customer">
                                                <i class="feather icon-user"></i> Customer
                                            </a>
                                        </li>
                                        <?php 
                                            }
                                        }
                                        ?>
                                        <li>
                                            <a href="#!">
                                                <i class="feather icon-settings"></i> Settings
                                            </a>
                                        </li>
                                        <li>
                                            <a href="users-edit.php?id=<?php echo $UserAdminID; ?>">
                                                <i class="feather icon-user"></i> Profile
                                            </a>
                                        </li>
                                        <li>
                                            <a href="logout.php">
                                                <i class="feather icon-log-out"></i> Logout
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
