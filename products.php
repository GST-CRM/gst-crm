<?php
include "session.php";
$PageTitle = "Products";
include "header.php"; 

if(isset($_GET['action'])) {
    $action = $_GET['action'];
} else {
    $action = "";
}

if(isset($_GET['status'])) {
    $status = $_GET['status'];
} else {
    $status = "";
}


if ($action == "success") {
	$fullname = $_GET["fullname"];
	echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The agent account for ".$fullname." has been created successfully!</strong></div>";
}
if ($action == "update") {
	$fullname = $_GET["fullname"];
	echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The agent ".$fullname." has been updated successfully!</strong></div>";
}
if ($action == "delete") {
	$fullname = $_GET["fullname"];
	$contactid = $_GET["cid"];
	echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The agent ".$fullname." (id# ".$contactid.") has been deleted!</strong></div>";
}

?>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
        <div class="card">
            <div class="row">
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
					</div>
				</div>
				<div class="col-md-5" style="margin-top:15px;"></div>
				<div class="col-md-2" style="margin-top:15px;">
					<select id="status" name="status" class="form-control form-control-default fill" onChange="doReload(this.value);">
						<option value="0">Status</option>
						<?php $StatusGET = mysqli_real_escape_string($db, $_GET['status']); ?>
						<option value="all" <?php if(!empty($StatusGET) AND $StatusGET == "all") {echo "selected";} ?>>Active Products</option>
                        <option value="disabled" <?php if(!empty($StatusGET) AND $StatusGET == "disabled") {echo "selected";} ?>>Disabled Products</option>
						<option value="subproduct" <?php if(!empty($StatusGET) AND $StatusGET == "subproduct") {echo "selected";} ?>>Sub Products</option>
					</select>
				</div>
				<div class="col-md-2" style="margin-top:15px;padding-right:45px;">
					<a href="products-add.php" class="btn waves-effect float-right waves-light btn-success" style="padding: 3px 13px;"><i class="fas fa-plus-circle"></i>Add New Product</a>
				</div>
			</div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="TableWithNoButtons" class="table table-striped table-bordered nowrap">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th style="display:none;">Description</th>
                                <th>Supplier</th>
                                <th>Cost</th>
                                <th>Category</th>
                            </tr>
                        </thead>
                        <tbody>
						<?php
						if($status == "disabled"){
							$statuss = "WHERE status=0";
							$supplierid = "";
							$showsub = "";
						} else {
							$statuss = "WHERE status=1";
						}
						if($status == "subproduct"){
							$showsub = "AND subproduct=1";
						} elseif($status == "disabled") {
							$showsub = "";
						} else {
							$showsub = "AND subproduct=0";
						}
						if(isset($_GET['supplierid'])){
							$thesupplierid = $_GET['supplierid'];
							$supplierid = "AND supplierid=$thesupplierid";
						} else {
							$supplierid = "";
						}
						$sql = "SELECT * FROM products $statuss $showsub $supplierid";
						$result = $db->query($sql);
						if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
                            <tr>
                                <td><?php echo "P".$row["id"]; ?></td>
                                <td><a href="products-edit.php?id=<?php echo $row["id"]; ?>&action=edit" style="color: #0000EE;">
								<?php echo $row["name"]; ?></a></td>
                                <td style="display:none;"><?php echo $row["description"]; ?></td>
                                <td>
								<a href="contacts-edit.php?id=<?php echo $row["supplierid"]; ?>&action=edit&usertype=supplier">
								<?php $supplieridz = $row["supplierid"];
								$supresult = mysqli_query($db,"SELECT fname, lname FROM contacts WHERE id = '$supplieridz' ");
								$supplierz = mysqli_fetch_array($supresult); echo $supplierz["fname"] . " " . $supplierz["lname"]; ?></a>
								</td>
                                <?php if($status == "subproduct"){ ?>
								<td>
								<a href="products-edit.php?id=<?php echo $row["parentid"]; ?>&action=edit">
								<?php $parentidz = $row["parentid"];
								$parentresult = mysqli_query($db,"SELECT name FROM products WHERE id = '$parentidz' ");
								$parentpro = mysqli_fetch_array($parentresult); echo $parentpro["name"]; ?></a>
								</td>
								<?php } else { ?>
								<td><?php echo GUtils::formatMoney($row["cost"]); ?></td>
								<?php } ?>
                                <td>
								<?php $categoryid = $row["categoryid"];
								$catresult = mysqli_query($db,"SELECT * FROM products_category WHERE id = '$categoryid' ");
								$category = mysqli_fetch_array($catresult); echo $category['name']; ?>
								</td>
							</tr>
						<?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<script>
$(document).ready(function() {

    //Just to move the search box to be on the top of the card section
    $('.outsideBorderSearch').on( 'keyup click', function () {
        $('#TableWithNoButtons').DataTable().search(
            $('.outsideBorderSearch').val()
        ).draw();
    });
});

$(document).ready(function() {
    $('#TableWithNoButtons').DataTable( {
        //"ordering": false, //To disable the ordering of the tables
		"bLengthChange": false, //To hide the Show X entries dropdown
		dom: 'Bfrtip',
		sDom: 'lrtip', //To Hide the search box
        buttons: [], //To hide the export buttons
		"order": [[ 0, "desc" ]]
    } );
} );

function doReload(status){
	document.location = 'products.php?status=' + status;
}
</script>
<!-- [ page content ] end -->
<?php include "footer.php"; ?>