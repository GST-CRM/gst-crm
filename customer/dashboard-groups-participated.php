<?php
    include_once __DIR__ . '/../models/groups.php';
    include_once __DIR__ . '/../models/users.php';
    
    $loggedUser = Users::loggedUser() ;
    $userContactId = $loggedUser['contactid'] ;
    $tourSet = (new Groups())->customerTripDetails($userContactId) ;
?>
<div class="card exp-build-card">
            <div class="card-header">
                <h5>List of Groups you participated in</h5>
            </div>
            <div class="pt-0 card-block accordion-block">
                <div id="accordion" role="tablist" aria-multiselectable="true">
                    <?php 
                    if(is_array($tourSet) && count($tourSet) > 0 ) { 
                    $xx = 0 ;
                    foreach ( $tourSet as $tour ) {
                        $xx ++ ;
                        $travellingWithNames = implode(',', array_unique([$tour['travellingwith1name'], $tour['travellingwith2name']])) ;
                    ?>
                    <div class="accordion-panel">
                        <div class="accordion-heading" role="tab" id="heading<?php echo $xx; ?>">
                            <h3 class="card-title accordion-title">
                                <a class="accordion-msg waves-effect waves-dark" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $xx; ?>" aria-expanded="true" aria-controls="collapse<?php echo $xx; ?>">
                                    Group #<?php echo $xx; ?>: <?php echo $tour['tourname'];?>
                                </a>
                            </h3>
                        </div>
                        <div id="collapse<?php echo $xx; ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading<?php echo $xx; ?>">
                            <div class="accordion-content accordion-desc">
                                <div class="pt-3 row no-gutters">
                                    <div class="col-2">
                                        <img src="https://cdn.shopify.com/s/files/1/2092/5523/products/church_of_the_holy_sepulchre_105209441-W1000_530x@2x.jpg?v=1524254599" alt="" class="img-thumbnail img-fluid" />
                                    </div>
                                    <div class="col-4 pl-3">
                                        <h6>Trip Date: <?php echo GUtils::clientDate($tour['startdate']); ?> to <?php echo GUtils::clientDate($tour['enddate']); ?></h6>
                                        <h6>Trip Destination: <?php echo $tour['destination'];?></h6>
                                        <h6>Trip Price: <?php echo GUtils::formatMoney($tour['listprice']); ?></h6>
                                    </div>
                                    <div class="col-4 pl-3">
                                        <h6>Room Type: <?php echo $tour['roomtype'];?></h6>
                                        <h6>Travelling With: <?php echo $travellingWithNames ;?></h6>
                                    </div>
                                    <div class="col-2">
                                        <a href="customer-page-groups.php?GroupID=<?php echo $tour['tourid']; ?>" class="btn btn-block btn-primary">View Details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php }
                    }
                    else { ?>
                    <div>You are not participating any group.</div>
                    <?php } ?>
                </div>
            </div>
        </div>