<?php
    include_once __DIR__ . '/../models/customer_payment.php';
    include_once __DIR__ . '/../models/customer_account.php';
    include_once __DIR__ . '/../models/users.php';
    
    $loggedUser = Users::loggedUser() ;
    $userContactId = $loggedUser['contactid'] ;
    
    $payments = (new CustomerPayment())->customerPaymentListing(" and contactid = $userContactId ", "", "", 0) ;
    
    $CUSTOMER_SUMMARY = (new CustomerPayment())->customerSummary($userContactId) ;
?>
<div class="row">
    <div class="col-sm-12" style="margin-bottom:20px;">
        <h5 style="margin-top: 10px;">My Payments & Transactions</h5>
    </div>
    <div class="col-sm-12">
        <table class="table table-hover table-striped table-smallz" width="100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Date</th>
                    <th>Method</th>
                    <th>Invoice #</th>
                    <th>Transaction ID</th>
                    <th>Amount</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(is_array($payments) && count($payments) > 0 ) {
                foreach( $payments as $payment ) { ?>
                <tr>
                    <td><a href="#"><b><?php echo $payment['Customer_Payment_ID'];?></b></a></td>
                    <td><?php echo GUtils::clientDate($payment['Customer_Payment_Date']) ;?></td>
                    <td><?php echo $payment['Customer_Payment_Method'] ;?></td>
                    <td><?php echo $payment['Customer_Invoice_Num'] ;?></td>
                    <td><?php echo $payment['Transaction_Type_Transaction_ID'] ;?></td>
                    <td><?php echo GUtils::formatMoney($payment['Customer_Payment_Amount']) ;?></td>
                </tr>
                <?php }
                }
                else {?>
                <tr>
                    <td align='center' colspan="6">There are no records !</td>
                </tr>
                <?php } ?>
            </tbody>
            <tfoot>
                <tr style="border-top: double 4px #ccc;background: #f9f9f9;">
                    <td colspan="5" class="text-right">Total Paid:</td>
                    <td><b><?php echo GUtils::formatMoney($CUSTOMER_SUMMARY['summary']['paid']);?></b></td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>