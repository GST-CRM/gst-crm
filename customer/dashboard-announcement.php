<div class="card exp-build-card">
            <div class="card-header">
                <h5>Announcements</h5>
            </div>
            <div class="pt-0 card-block accordion-block">
                <ul style="list-style: circle inside;">
                    <li style="border-bottom:solid 1px #ccc;padding-bottom:10px;"><a href="#">Regarding corona and the situation of our pilgrimages</a></li>
                    <li style="border-bottom:solid 1px #ccc;padding-bottom:10px;padding-top:10px;"><a href="#">The safety of our pilgrims is our main concern and priority</a></li>
                    <li style="border-bottom:solid 1px #ccc;padding-bottom:10px;padding-top:10px;"><a href="#">The cancellation of the trips from March 2020 until July 2020 for the safety of the pilgrims</a></li>
                    <li style="border-bottom:solid 1px #ccc;padding-bottom:10px;padding-top:10px;"><a href="#">Your contribution would be highly appreciated in this matter</a></li>
                </ul>
            </div>
        </div>