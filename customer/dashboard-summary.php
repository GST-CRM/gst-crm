<?php
    include_once __DIR__ . '/../models/customer_payment.php';
    include_once __DIR__ . '/../models/customer_account.php';
    include_once __DIR__ . '/../models/users.php';
    
    $loggedUser = Users::loggedUser() ;
    $userContactId = $loggedUser['contactid'] ;
    $CUSTOMER_SUMMARY = (new CustomerPayment())->customerSummary($userContactId) ;
    $group_count = (new CustomerAccount())->customerGroupCount($userContactId) ;
?>
<div class="col-md-3">
    <div class="card stat-rev-card">
        <div class="card-block">
            <div class="rev-icon bg-c-red"><i class="fas fa-shopping-cart text-white"></i><span class="ring-sm"></span><span class="ring-lg"></span></div>
            <h2 class="text-c-red" style="font-size: 30px;"><?php echo intval($group_count);?></h2>
            <p class="m-b-0">Groups Participated</p>
        </div>
    </div>
</div>
<div class="col-md-3">
    <div class="card stat-rev-card">
        <div class="card-block">
            <div class="rev-icon bg-c-green"><i class="fas fa-money-bill-alt text-white"></i><span class="ring-sm"></span><span class="ring-lg"></span></div>
            <h2 class="text-c-green" style="font-size: 30px;"><?php echo GUtils::formatMoney($CUSTOMER_SUMMARY['summary']['due']); ?></h2>
            <p class="m-b-0">Invoices Due</p>
        </div>
    </div>
</div>
<div class="col-md-3">
    <div class="card stat-rev-card">
        <div class="card-block">
            <div class="rev-icon bg-c-blue"><i class="fas fa-cloud-download-alt text-white"></i><span class="ring-sm"></span><span class="ring-lg"></span></div>
            <h2 class="text-c-blue" style="font-size: 30px;"><?php echo GUtils::formatMoney($CUSTOMER_SUMMARY['summary']['unpaid']); ?></h2>
            <p class="m-b-0">Amount Money Owed</p>
        </div>
    </div>
</div>
<div class="col-md-3">
    <div class="card stat-rev-card">
        <div class="card-block">
            <div class="rev-icon bg-c-yellow"><i class="fas fa-user text-white"></i><span class="ring-sm"></span><span class="ring-lg"></span></div>
            <h2 class="text-c-yellow" style="font-size: 30px;">0</h2>
            <p class="m-b-0">Opened Support Messages</p>
        </div>
    </div>
</div>