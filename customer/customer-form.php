<?php
$loggedUser = Users::loggedUser() ;
$userContactId = $loggedUser['contactid'] ;
if( isset($__REQUEST['customer']) ) {
    $userContactId = $__REQUEST['customer'] ;
}

$contacObj = new Contacts() ;
$data = $contacObj->getById($userContactId) ;
$fullName = $contacObj->customerFullName($data) ;
$PageTitle = "Welcome ". $fullName ;

?>
<?php
$TAB_DESTINATION = 'customer-page-profile.php' ;
include __DIR__ . '/fragment-subtabs.php' ;
?>

<form name="contact0" action="customer/customer-save.php" method="POST" id="contact0">
    <div class="row">
        <div class="form-group col-sm-12 m-0">
            <h4 class="sub-title">Basic Information</h4>
        </div>
        <div class="form-group col-sm-3">
            <label class="float-label">Title</label>
            <select name="title" class="form-control">
                <option value="0" disabled> </option>
                <option value="Mr." <?php if($data["title"] == "Mr.") {echo "selected";} ?>>Mr.</option>
                <option value="Ms." <?php if($data["title"] == "Ms.") {echo "selected";} ?>>Ms.</option>
                <option value="Mrs." <?php if($data["title"] == "Mrs.") {echo "selected";} ?>>Mrs.</option>
                <option value="Dr." <?php if($data["title"] == "Dr.") {echo "selected";} ?>>Dr.</option>
                <option value="Rev." <?php if($data["title"] == "Rev.") {echo "selected";} ?>>Rev.</option>
                <option value="Father" <?php if($data["title"] == "Father") {echo "selected";} ?>>Father</option>
                <option value="Pastor" <?php if($data["title"] == "Pastor") {echo "selected";} ?>>Pastor</option>
                <option value="Deacon" <?php if($data["title"] == "Deacon") {echo "selected";} ?>>Deacon</option>
            </select>
        </div>
        <div class="form-group form-default form-static-label col-sm-3">
            <label class="float-label">First Name</label>
            <input type="text" name="FirstName" value="<?php echo $data["fname"]; ?>" class="form-control">
            <input type="text" name="fullname" value="<?php echo $data['fname']." ".$data['mname']." ".$data['lname']; ?>" class="form-control" hidden>
            <input type="text" name="contactupdateid" value="<?php echo $data["id"]; ?>" class="form-control" hidden>
        </div>
        <div class="form-group form-default form-static-label col-sm-3">
            <label class="float-label">Middle Name</label>
            <input type="text" name="MiddleName" value="<?php echo $data["mname"]; ?>" class="form-control">
        </div>
        <div class="form-group form-default form-static-label col-sm-3">
            <label class="float-label">Last Name</label>
            <input type="text" name="LastName" value="<?php echo $data["lname"]; ?>" class="form-control">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Email</label>
            <input type="text" name="email" value="<?php echo $data["email"]; ?>" class="form-control">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Secondary Email</label>
            <input type="text" name="secondaryemail" value="<?php echo $data["secondary_email"]; ?>" class="form-control">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Mobile Phone #</label>
            <input type="tel" id="mobile" name="mobile" value="<?php echo $data["mobile"]; ?>" class="form-control">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Home Phone #</label>
            <input id="tel" type="tel" name="homephone" value="<?php echo $data["phone"]; ?>" class="form-control">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Business Phone #</label>
            <input id="businessphone" type="tel" name="businessphone" value="<?php echo $data["businessphone"]; ?>" class="form-control">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Fax Phone #</label>
            <input id="fax" type="tel" name="fax" value="<?php echo $data["fax"]; ?>" class="form-control">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Company Name</label>
            <input type="text" name="company" value="<?php echo $data["company"]; ?>" class="form-control">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Job Title</label>
            <input type="text" name="jobtitle" value="<?php echo $data["jobtitle"]; ?>" class="form-control">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Website</label>
            <input type="text" name="website" value="<?php echo $data["website"]; ?>" class="form-control">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Emergency Contact #1 Name</label>
            <input type="text" name="emergencyname" class="form-control" value="<?php echo $data['emergencyname'];?>">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Emergency Contact #1 Relation</label>
            <input type="text" name="emergencyrelation" class="form-control" value="<?php echo $data['emergencyrelation'];?>">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Emergency Contact #1 Phone</label>
            <input type="text" name="emergencyphone" class="form-control" value="<?php echo $data['emergencyphone'];?>">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Emergency Contact #2 Name</label>
            <input type="text" name="emergency2name" class="form-control" value="<?php echo $data['emergency2name'];?>">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Emergency Contact #2 Relation</label>
            <input type="text" name="emergency2relation" class="form-control" value="<?php echo $data['emergency2relation'];?>">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Emergency Contact #2 Phone</label>
            <input type="text" name="emergency2phone" class="form-control" value="<?php echo $data['emergency2phone'];?>">
        </div>
        <div class="form-group col-sm-12 m-0" style="margin-top:15px !important;">
            <h4 class="sub-title">Home Address Information</h4>
        </div>
        <div class="form-group form-default form-static-label col-sm-3">
            <label class="float-label">Extended Address</label>
            <textarea rows="4" class="form-control" readonly><?php echo $data["title"]." ".$data["fname"]." ".$data["mname"]." ".$data["lname"]."\r\n".$data["address1"]."\r\n".$data["address2"]."\r\n".$data["city"].", ".$data["state"]." ".$data["zipcode"]; ?></textarea>
        </div>
        <div class="form-group form-default form-static-label col-sm-3">
            <div class="form-group">
                <label class="float-label">Address Line 1</label>
                <input type="text" name="address1" value="<?php echo $data["address1"]; ?>" class="form-control">
            </div>
            <div class="form-group">
                <label class="float-label">Address Line 2</label>
                <input type="text" name="address2" value="<?php echo $data["address2"]; ?>" class="form-control">
            </div>
        </div>
        <div class="form-group form-default form-static-label col-sm-3">
            <div class="form-group">
                <label class="float-label">City</label>
                <input type="text" name="city" value="<?php echo $data["city"]; ?>" class="form-control">
            </div>
            <div class="form-group">
                <label class="float-label">State</label>
                <select name="state" class="form-control form-control-default">
                    <option value="opt1" disabled>State</option>
                    <option value="NA" <?php if($data["state"] == "NA") { echo "selected"; } ?>>Not Applicable</option>
                    <?php $con2 = new mysqli($servername, $username, $password, $dbname);
                        $result2 = mysqli_query($con2,"SELECT name, abbrev FROM states");$stateselected ="";
                        while($row2 = mysqli_fetch_array($result2))
                        {
                            if($data["state"] == $row2['abbrev']) {$stateselected="selected";} else {$stateselected="";}
                            echo "<option value='".$row2['abbrev']."' ".$stateselected.">".$row2['name']."</option>";
                        } ?>
                </select>
            </div>
        </div>
        <div class="form-group form-default form-static-label col-sm-3">
            <div class="form-group">
                <label class="float-label">ZIP Code</label>
                <input type="text" name="zipcode" value="<?php echo $data["zipcode"]; ?>" class="form-control">
            </div>
            <div class="form-group">
                <label class="float-label">Country</label>
                <select name="country" class="form-control form-control-default">
                    <option value="0" disabled> </option>
                    <?php $con3 = new mysqli($servername, $username, $password, $dbname);
                        $result3 = mysqli_query($con3,"SELECT name, abbrev FROM countries");$countryselected ="";
                        while($row3 = mysqli_fetch_array($result3))
                        {
                            if($data["country"] == $row3['abbrev']) {$countryselected="selected";} else {$countryselected="";}
                            echo "<option value='".$row3['abbrev']."' ".$countryselected.">".$row3['name']."</option>";
                        } ?>
                </select>
            </div>
        </div>
        <div class="form-group col-sm-9 m-0" style="margin-top:15px !important;">
            <h4 class="sub-title">Work Address Information</h4>
        </div>
        <div class="form-group col-sm-3 m-0 text-right" style="margin-top:15px !important;">
            <div class="checkbox-fade fade-in-success">
                <label>
                    <input id="WorkAddressButton" name="WorkAddressButton" type="checkbox" value="1" <?php if($data["Work_enabled"] == 1) {echo "checked";} ?>>
                    <span class="cr">
                        <i class="cr-icon fas fa-check txt-success"></i>
                    </span>
                    <span>Enable?</span>
                </label>
            </div>
        </div>
        <div id="WorkAddressBlock" class="col-sm-12 row p-0 m-0" style="<?php if($data["Work_enabled"] == 1) {echo "";} else {echo "display:none";} ?>;">
            <div class="form-group form-default form-static-label col-sm-4">
                <label class="float-label">Address Line 1</label>
                <input type="text" name="Work_address1" value="<?php echo $data["Work_address1"]; ?>" class="form-control">
            </div>
            <div class="form-group form-default form-static-label col-sm-4">
                <label class="float-label">Address Line 2</label>
                <input type="text" name="Work_address2" value="<?php echo $data["Work_address2"]; ?>" class="form-control">
            </div>
            <div class="form-group form-default form-static-label col-sm-4">
                <label class="float-label">City</label>
                <input type="text" name="Work_city" value="<?php echo $data["Work_city"]; ?>" class="form-control">
            </div>
            <div class="col-sm-4">
                <label class="float-label">State</label>
                <select name="Work_state" class="form-control form-control-default">
                    <option value="opt1" disabled>State</option>
                    <?php $con2 = new mysqli($servername, $username, $password, $dbname);
                    $result2 = mysqli_query($con2,"SELECT name, abbrev FROM states");$stateselected ="";
                    while($row2 = mysqli_fetch_array($result2))
                    {
                        if($data["Work_state"] == $row2['abbrev']) {$stateselected="selected";} else {$stateselected="";}
                        echo "<option value='".$row2['abbrev']."' ".$stateselected.">".$row2['name']."</option>";
                    } ?>
                </select>
            </div>
            <div class="form-group form-default form-static-label col-sm-4">
                <label class="float-label">ZIP Code</label>
                <input type="text" name="Work_zipcode" value="<?php echo $data["Work_zipcode"]; ?>" class="form-control">
            </div>
            <div class="col-sm-4">
                <label class="float-label">Country</label>
                <select name="Work_country" class="form-control form-control-default">
                    <option value="0" disabled> </option>
                    <?php $con3 = new mysqli($servername, $username, $password, $dbname);
                    $result3 = mysqli_query($con3,"SELECT name, abbrev FROM countries");$countryselected ="";
                    while($row3 = mysqli_fetch_array($result3))
                    {
                        if($data["Work_country"] == $row3['abbrev']) {$countryselected="selected";} else {$countryselected="";}
                        echo "<option value='".$row3['abbrev']."' ".$countryselected.">".$row3['name']."</option>";
                    } ?>
                </select>
            </div>
        </div>
        <div class="col-sm-12"><br /></div>
        <div class="form-group col-sm-12 m-0">
            <h4 class="sub-title">Customer Information</h4>
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Passport Number</label>
            <input type="text" id="passportID" name="passport" class="form-control" value="<?php echo $data['passport'];?>">
            <input type="text" name="CustomerAccountBasicDataID" class="form-control" value="<?php echo $userContactId; ?>" hidden>
            <input type="text" name="CustomerAccountBasicDataName" class="form-control" value="<?php echo $data["fname"]." ".$data["mname"]." ".$data["lname"]; ?>" hidden>
        </div>
        <div class="form-group form-default form-static-label col-sm-1">
            <label class="float-label">Pending?</label>
            <div class="checkbox-fade fade-in-success pt-2">
                <label>
                    <input id="PassportPending" name="PassportPending" type="checkbox" value="1" <?php if($data['passport']=="PASSPORT PENDING") { echo "checked";} ?>>
                    <span class="cr">
                        <i class="cr-icon fas fa-check txt-success"></i>
                    </span>
                </label>
            </div>
        </div>
        <div class="form-group form-default form-static-label col-sm-3">
            <label class="float-label">Passport Expiration Date</label>
            <input type="date" id="expirydateID" name="expirydate" class="form-control" value="<?php echo $data['expirydate'];?>" min="<?php echo date('Y-m-d', strtotime('+1 day')); ?>" onblur="return this.reportValidity();">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Country of Citizenship</label>
            <select name="citizenship" class="form-control form-control-default">
                <?php $con3 = new mysqli($servername, $username, $password, $dbname);
    $result3 = mysqli_query($con3,"SELECT name, abbrev FROM countries");
    while($row3 = mysqli_fetch_array($result3))
    {
            $selectedd = "";
            if($data['citizenship'] == "") {
                    if($row3['abbrev'] == "US") {
                            $selectedd = "selected";
                    } else {
                            $selectedd = "";
                    }
            } elseif($data['citizenship'] == $row3['abbrev']) {
                    $selectedd = "selected";
            } else {
                    $selectedd = "";
            }
            echo "<option value='".$row3['abbrev']."' $selectedd>".$row3['name']."</option>";
    } ?>
            </select>
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Birthday</label>
            <input type="date" name="birthday" value="<?php echo $data["birthday"]; ?>" class="form-control" max="<?php echo date('Y-m-d', strtotime('-1 day')); ?>" min="1925-01-01" onblur="return this.reportValidity();">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Name on the Badge</label>
            <input type="text" name="badgename" class="form-control" value="<?php echo $data['badgename'];?>">
        </div>
        <div class="col-sm-4">
            <label class="float-label">Martial Status</label>
            <select name="martialstatus" class="form-control form-control-default">
                <option value="NA" <?php if($data["martialstatus"] == "NA") {echo "selected";} ?>>N/A</option>
                <option value="Married" <?php if($data["martialstatus"] == "Married") {echo "selected";} ?>>Married</option>
                <option value="Single" <?php if($data["martialstatus"] == "Single") {echo "selected";} ?>>Single</option>
            </select>
        </div>
        <div class="col-sm-4">
            <label class="float-label">Gender</label>
            <select name="gender" class="form-control form-control-default">
                <option value="0" disabled <?php if($data["gender"] != "Male" OR $data["gender"] != "Female") {echo "selected";} ?>> </option>
                <option value="Male" <?php if($data["gender"] == "Male") {echo "selected";} ?>>Male</option>
                <option value="Female" <?php if($data["gender"] == "Female") {echo "selected";} ?>>Female</option>
            </select>
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Payment Terms</label>
            <input type="text" name="paymentterm" class="form-control" value="<?php echo $data['payment'];?>">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Known Traveler #</label>
            <input type="text" name="travelerid" class="form-control" value="<?php echo $data['travelerid'];?>">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Frequent Flyer</label>
            <input type="text" name="flyer" class="form-control" value="<?php echo $data['flyer'];?>">
        </div>
        <div class="form-group form-default form-static-label col-sm-4">
            <label class="float-label">Missing Information?</label>
            <input type="text" name="missinginfo" class="form-control" value="<?php echo $data['missinginfo'];?>">
        </div>
        <div class="form-group col-sm-12 m-0" style="margin-top:15px !important;">
            <h4 class="sub-title">Other Information</h4>
        </div>
        <div id="affliationtype" class="col-sm-6" style="display:block;">
            <label class="float-label"><span style="color: red;font-weight: bold;">Category</span></label>
            <select name="affiliation" class="js-example-basic-multiple-limit form-control form-control-default" multiple="multiple" onchange="admSelectCheck(this);">
                <option value="0" <?php if($data["affiliation"]==0) { echo "selected"; } ?>>N/A</option>
                <option id="admOption" value="0">Add New</option>
                <?php $meta1 = new mysqli($servername, $username, $password, $dbname);
                    $metadata1 = mysqli_query($meta1,"SELECT * FROM metadata WHERE catid='1' AND Status=1");$metaselected ="";
                    while($keyword1 = mysqli_fetch_array($metadata1))
                    {
                        //Here it was the value META not ID, but just for the manual categories i made it like this
                        if($data["affiliation"] == $keyword1['id']) {$metaselected="selected";} else {$metaselected="";}
                        echo "<option value='".$keyword1['id']."' ".$metaselected.">".$keyword1['meta']."</option>";
                    } ?>
            </select>
        </div>
        <div id="affliationtypenew" class="form-group form-default form-static-label col-sm-6" style="display:none;">
            <label class="float-label">Add New Category</label>
            <input type="text" name="newaffliationtype" value="" class="form-control">
        </div>
        <div class="col-sm-3">
            <label class="float-label">Language</label>
            <select name="language" class="form-control form-control-default">
                <option value="English" <?php if($data["language"] == "English") {echo "selected";} ?>>English</option>
                <option value="French" <?php if($data["language"] == "French") {echo "selected";} ?>>French</option>
                <option value="German" <?php if($data["language"] == "German") {echo "selected";} ?>>German</option>
                <option value="Arabic" <?php if($data["language"] == "Arabic") {echo "selected";} ?>>Arabic</option>
                <option value="Italian" <?php if($data["language"] == "Italian") {echo "selected";} ?>>Italian</option>
                <option value="Spanish" <?php if($data["language"] == "Spanish") {echo "selected";} ?>>Spanish</option>
            </select>
        </div>
        <div class="col-sm-3">
            <label class="float-label">Preferred Method</label>
            <select name="preferredmethod" class="form-control form-control-default">
                <option value="0" disabled> </option>
                <option value="Email" <?php if($data["preferredmethod"] == "Email") {echo "selected";} ?>>Email</option>
                <option value="Phone" <?php if($data["preferredmethod"] == "Phone") {echo "selected";} ?>>Phone</option>
                <option value="Mobile" <?php if($data["preferredmethod"] == "Mobile") {echo "selected";} ?>>Mobile</option>
            </select>
        </div>
        <div class="col-sm-12"><br /></div>
        <div class="form-group form-default form-static-label col-md-12">
            <label class="float-label">Notes for this Contact</label>
            <textarea name="notes" class="form-control"><?php echo $data["notes"]; ?></textarea>
        </div>
        <div class="col-sm-12"><br /><br />
            <button type="submit" name="updatecontact" value="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Update</button>
            <button type="reset" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Cancel</button>
        </div>
    </div>
</form>