<!-- [ page content ] start -->
<!--<script type="text/javascript" charset="utf8" src="https:/ajax.aspnetcdn.com/ajax/jQuery/jquery-1.11.1.min.js"></script>	 -->
<div class="row">
    <?php include __DIR__ . '/dashboard-summary.php';?>
    <div class="col-sm-9">
        <?php include __DIR__ . '/dashboard-groups-participated.php';?>
        <?php include __DIR__ . '/dashboard-invoice-list.php';?>

    </div>
    <div class="col-sm-3">
        <?php include __DIR__ . '/dashboard-announcement.php';?>
        <?php include __DIR__ . '/dashboard-important-links.php';?>

    </div>
</div>
