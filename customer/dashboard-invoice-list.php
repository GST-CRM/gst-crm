<?php
include_once __DIR__ . '/../models/customer_invoice.php';

$CUSTOMER_LISTING = (new CustomerPayment())->customerListing($userContactId) ;

function printInvoiceStatus($row) {
    $balance = $row['balance'] ;
    if ($row['Invoice_Status'] == CustomerInvoice::$CANCELLED) {
        echo '<label class="label label-danger">Cancelled</label>';
    } else {
        if ($balance > 0) {
            if ($row['due_diff'] > 0) {
                echo '<label class="label label-warning">Due ' . $row['due_diff'] . ' Day(s)</label>';
            } else if ($row['due_diff'] == 0) {
                echo 'Due Today';
            } else {
                echo '<label class="label label-danger">Past Due ' . abs($row['due_diff']) . ' Day(s)</label>';
            }
        } else if ($balance == 0) {
            echo '<label class="label label-success">Invoice Paid</label>';
        } else {
            echo '<label class="label label-info">Pending Refund</label>';
        }
    }
}
?>

<div class="card exp-build-card">
    <div class="card-header">
        <h5>Invoices List</h5>
    </div>
    <div class="pt-0 card-block accordion-block">
        <table class="table table-striped table-hover">
            <thead>
                <tr>
                    <th scope="col">Invoice #</th>
                    <th scope="col">Invoice Date</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Paid so Far</th>
                    <th scope="col">Balance</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                if( is_array($CUSTOMER_LISTING) && count($CUSTOMER_LISTING) > 0 ) {
                foreach ($CUSTOMER_LISTING as $invoice) { ?>
                    <tr>
                        <td><b>#<?php echo $invoice['Customer_Invoice_Num']; ?></b></td>
                        <td><?php echo GUtils::clientDate($invoice['Due_Date']); ?></td>
                        <td><?php echo GUtils::formatMoney($invoice['Invoice_Amount_Revised']); ?></td>
                        <td><?php echo GUtils::formatMoney(doubleval($invoice['payment_amount'])); ?></td>
                        <td><?php echo GUtils::formatMoney($invoice['balance']); ?></td>
                        <td><?php echo printInvoiceStatus($invoice); ?></td>
                        <td><a href="customer-page-invoice.php?invoiceId=<?php echo $invoice['Customer_Invoice_Num'];?>" class="btn btn-primary btn-sm pt-0 pb-0">View</a></td>
                </tr>
                <?php }
                }
                else {?>
                <tr>
                    <td align='center' colspan="7">There are no records !</td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>