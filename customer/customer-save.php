<?php

include __DIR__ . "/../session.php";
include_once __DIR__ . '/../models/agents.php' ;
include_once __DIR__ . '/../models/users.php' ;
include_once __DIR__ . '/../models/contacts.php' ;
include_once __DIR__ . '/../models/group_leaders.php' ;
include_once __DIR__ . '/../models/customer_invoice.php' ;
error_reporting(E_ALL) ;
ini_set('display_errors', 1) ;


$theid              = $_POST["contactupdateid"];
$fullname           = mysqli_real_escape_string($db, $_POST["fullname"]);
$title              = mysqli_real_escape_string($db, $_POST["title"]);
$fname              = mysqli_real_escape_string($db, $_POST["FirstName"]);
$mname              = mysqli_real_escape_string($db, $_POST["MiddleName"]);
$lname              = mysqli_real_escape_string($db, $_POST["LastName"]);
$email              = mysqli_real_escape_string($db, $_POST["email"]);
$secondary_email    = mysqli_real_escape_string($db, $_POST["secondaryemail"]);
$phone              = mysqli_real_escape_string($db, $_POST["homephone"]);
$businessphone      = mysqli_real_escape_string($db, $_POST["businessphone"]);
$fax                = mysqli_real_escape_string($db, $_POST["fax"]);
$mobile             = mysqli_real_escape_string($db, $_POST["mobile"]);
$company            = mysqli_real_escape_string($db, $_POST["company"]);
$jobtitle           = mysqli_real_escape_string($db, $_POST["jobtitle"]);
$website            = mysqli_real_escape_string($db, $_POST["website"]);
$emergencyname      = mysqli_real_escape_string($db, $_POST["emergencyname"]);
$emergencyrelation  = mysqli_real_escape_string($db, $_POST["emergencyrelation"]);
$emergencyphone     = mysqli_real_escape_string($db, $_POST["emergencyphone"]);
$emergency2name     = mysqli_real_escape_string($db, $_POST["emergency2name"]);
$emergency2relation = mysqli_real_escape_string($db, $_POST["emergency2relation"]);
$emergency2phone    = mysqli_real_escape_string($db, $_POST["emergency2phone"]);
$address1           = mysqli_real_escape_string($db, $_POST["address1"]);
$address2           = mysqli_real_escape_string($db, $_POST["address2"]);
$zipcode            = mysqli_real_escape_string($db, $_POST["zipcode"]);
$city               = mysqli_real_escape_string($db, $_POST["city"]);
$state              = mysqli_real_escape_string($db, $_POST["state"]);
$country            = mysqli_real_escape_string($db, $_POST["country"]);
if ( isset($_POST["WorkAddressButton"]) && $_POST["WorkAddressButton"] == 1) {
    $Work_enabled = "1";
} else {
    $Work_enabled = "0";
}
if ($Work_enabled == 1) {
    $Work_address1 = mysqli_real_escape_string($db, $_POST["Work_address1"]);
    $Work_address2 = mysqli_real_escape_string($db, $_POST["Work_address2"]);
    $Work_zipcode  = mysqli_real_escape_string($db, $_POST["Work_zipcode"]);
    $Work_city     = mysqli_real_escape_string($db, $_POST["Work_city"]);
    $Work_state    = mysqli_real_escape_string($db, $_POST["Work_state"]);
    $Work_country  = mysqli_real_escape_string($db, $_POST["Work_country"]);
} else {
    $Work_address1 = $Work_address2 = $Work_zipcode  = $Work_city     = $Work_state    = $Work_country  = NULL;
}
$language = mysqli_real_escape_string($db, $_POST["language"]);

if ($_POST['newaffliationtype'] == null) {
    $affiliation = mysqli_real_escape_string($db, $_POST["affiliation"]);
} else {
    $affiliation = mysqli_real_escape_string($db, $_POST["newaffliationtype"]);
}

$preferredmethod = mysqli_real_escape_string($db, $_POST["preferredmethod"]);
$notes           = mysqli_real_escape_string($db, $_POST["notes"]);

if ($email == "" OR $fname == "" OR $lname == "" OR ! filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $missingfields = "";
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $missingfields = $missingfields . "<label class='MissingFieldWarning'>The Email format</label>";
    }
    if ($email == "") {
        $missingfields = $missingfields . "<label class='MissingFieldWarning'>The Email field</label>";
    }
    if ($fname == "") {
        $missingfields = $missingfields . "<label class='MissingFieldWarning'>The First name field</label>";
    }
    if ($lname == "") {
        $missingfields = $missingfields . "<label class='MissingFieldWarning'>The Last name field</label>";
    }

    echo "The following fields are mandatory:<br />" . $missingfields;
} else {
    $SQLcommand = "UPDATE contacts SET title='$title', fname='$fname', mname='$mname', lname='$lname', email='$email',secondary_email='$secondary_email', phone='$phone', businessphone='$businessphone', fax='$fax', mobile='$mobile', company='$company', jobtitle='$jobtitle', website='$website', emergencyname='$emergencyname',emergencyrelation='$emergencyrelation',emergencyphone='$emergencyphone',emergency2name='$emergency2name',emergency2relation='$emergency2relation',emergency2phone='$emergency2phone', address1='$address1', address2='$address2', zipcode='$zipcode', city='$city', state='$state', country='$country',Work_enabled='$Work_enabled', Work_address1='$Work_address1', Work_address2='$Work_address2', Work_zipcode='$Work_zipcode', Work_city='$Work_city', Work_state='$Work_state', Work_country='$Work_country', language='$language', affiliation='$affiliation', preferredmethod='$preferredmethod',notes='$notes', updatetime=NOW() WHERE id=$theid;";
    $SQLcommand .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','contact','$theid','edited','$fullname');";

    //if(isset($_POST['newaffliationtype'])){
    if ($_POST['newaffliationtype'] != null) {
        $meta = $_POST['newaffliationtype'];
        $SQLcommand .= "INSERT INTO metadata (catid, catname, meta) VALUES ('1','Contact Category','$meta');";
    }

    $CustomerAccountBasicDataID   = mysqli_real_escape_string($db, $_POST["CustomerAccountBasicDataID"]);
    $CustomerAccountBasicDataName = mysqli_real_escape_string($db, $_POST["CustomerAccountBasicDataName"]);
    $passport                     = mysqli_real_escape_string($db, $_POST["passport"]);
    $expirydate                   = mysqli_real_escape_string($db, $_POST["expirydate"]);
    $citizenship                  = mysqli_real_escape_string($db, $_POST["citizenship"]);
    $badgename                    = mysqli_real_escape_string($db, $_POST["badgename"]);
    $birthday                     = mysqli_real_escape_string($db, $_POST["birthday"]);
    $martialstatus                = mysqli_real_escape_string($db, $_POST["martialstatus"]);
    $gender = null ;
    if( isset($_POST["gender"]) ){
        $gender                       = mysqli_real_escape_string($db, $_POST["gender"]);
    }
    $payment                      = mysqli_real_escape_string($db, $_POST["paymentterm"]);
    $travelerid                   = mysqli_real_escape_string($db, $_POST["travelerid"]);
    $flyer                        = mysqli_real_escape_string($db, $_POST["flyer"]);
    $missinginfo                  = mysqli_real_escape_string($db, $_POST["missinginfo"]);

    $SQLcommand .= "UPDATE contacts SET passport='$passport',expirydate='$expirydate',citizenship='$citizenship',badgename='$badgename',birthday='$birthday',martialstatus='$martialstatus',gender='$gender',payment='$payment',travelerid='$travelerid',flyer='$flyer',missinginfo='$missinginfo' WHERE id=$CustomerAccountBasicDataID;";
    $SQLcommand .= "INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','contact','$CustomerAccountBasicDataID','edited','$CustomerAccountBasicDataName');";

    if ($db->multi_query($SQLcommand) === TRUE) {
        echo "<h3>Success</h3> The customer account basic data ($CustomerAccountBasicDataName) has been updated successfully! The page will be refreshed in seconds.";
    } else {
        echo $SQLcommand . "<br>" . $db->error;
    }
}