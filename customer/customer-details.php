<?php include __DIR__ . "/../session.php";

include_once __DIR__ . '/../models/users.php' ;
include_once __DIR__ . '/../models/contacts.php' ;

include "header.php";

//To redirect the admin and users directly to the tab by using the url
if(isset($_GET['tab'])){
	$urlusertype = mysqli_real_escape_string($db, $_GET['tab']);
} else {
	$urlusertype = "contact";
}

?>
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->

<?php
if ( $DashboardActive == 'active' ) {
include __DIR__ . '/customer-dashboard.php' ;
}
else {
?>

<div class="row">
    <div class="col-md-9">
        <div class="card" style="overflow:hidden;">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs /*md-tabs*/" role="tablist">
                <li class="nav-item">
                    <a class="nav-link <?php echo $DetailsTabActive; ?>" href="customer-page-profile.php" >My Details</a>
                    <div class="slide"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo $SalesTabActive; ?>" href="customer-page-invoice.php" >My Invoices</a>
                    <div class="slide"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo $PaymentsTabActive; ?>" href="customer-page-payment.php" >My Payments</a>
                    <div class="slide"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-link <?php echo $AttachmentTabActive; ?>" href="customer-page-attachment.php" >Attachments</a>
                    <div class="slide"></div>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content card-block">
                <div class="tab-pane <?php echo $DetailsTabActive; ?>" id="contacttab" role="tabpanel">
                    <?php include __DIR__ . '/customer-form.php' ; ?>
                </div>
                <div class="tab-pane <?php echo $SalesTabActive; ?>" id="salestab" role="tabpanel">
                    <?php include __DIR__ . '/customer-invoices.php' ; ?>
                </div>
                <div class="tab-pane <?php echo $PaymentsTabActive; ?>" id="paymentstab" role="tabpanel">
                    <?php include __DIR__ . '/customer-payments.php' ; ?>
                </div>
                <div class="tab-pane <?php echo $AttachmentTabActive; ?>" id="attachmenttab" role="tabpanel">
                    <?php include __DIR__ . '/customer-attachment.php' ; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <?php include __DIR__  . '/customer-billing-status-box.php' ; ?>
    </div>
    <?php include __DIR__ . '/../inc/notificiations.php'; ?>

    <?php require('inc/metadata-js.php'); ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $("form:not(.not-common-form)").submit(function() {
                $('#results').html('');
                // Getting the form ID
                var formID = $(this).attr('id');
                var formDetails = $('#' + formID);
                //To not let the form of uploading attachments included in this
                if (formID != 'contact6') {
                    $.ajax({
                        type: "POST",
                        url: 'customer/customer-save.php',
                        data: formDetails.serialize(),
                        success: function(data) {
                            // Inserting html into the result div
                            $('#results').html(data);
                            $('.confirmation-message-result-area').html(data);
                            //$("form")[0].reset();
                            formmodified = 0;
                        },
                        error: function(jqXHR, text, error) {
                            // Displaying if there are any errors
                            $('#result').html(error);
                        }
                    });
                    return false;
                }
            });
        });

    </script>
    <!-- sweet alert js -->
    <script type="text/javascript" src="files/bower_components/sweetalert/js/sweetalert.min.js"></script>
    <!-- modalEffects js nifty modal window effects -->
    <script type="text/javascript" src="files/assets/js/modalEffects.js"></script>
    
    <?php
}?>
    <?php include "footer.php"; ?>
