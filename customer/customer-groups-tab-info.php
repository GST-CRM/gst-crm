<?php
    include_once __DIR__ . '/../models/groups.php' ;

    $tourId = $__REQUEST['GroupID'] ;
    $groupObj = new Groups();
    //tour details
    $loggedUser = Users::loggedUser() ;
    $userContactId = $loggedUser['contactid'] ;

    $tourData = $groupObj->customerTripDetail( $userContactId, $tourId) ;
    //group leader
    $leaders = $groupObj->getGroupLeadersNameString($tourId) ;
    //pax count
    $memberSummary = $groupObj->getMemberSummary($tourId) ;
    //remaining
    
    $diff = GUtils::dayDiff(Date('Y-m-d'), $tourData['startdate']) ;
    
?>
<div class="tab-pane active" id="info" role="tabpanel">
    <div class="row">
        <div class="col-sm-3">
            <img src="https://cdn.shopify.com/s/files/1/2092/5523/products/church_of_the_holy_sepulchre_105209441-W1000_530x@2x.jpg?v=1524254599" alt="" class="img-thumbnail img-fluid" />
        </div>
        <div class="col-sm-9 mb-3">
            <h4 style="margin-bottom: 10px;"><?php echo $tourData['tourname'] ;?></h4>
            <h6>Group Leader: <?php echo $leaders;?></h6>
            <h6>Group Pax: <?php echo ($memberSummary['customers'] - $memberSummary['canceled']) ; ?> participants</h6>
            <?php if( $diff > 0 ) { ?>
            <h6 class="alert background-success"> <?php echo $diff;?> days remaining!</h6>
            <?php } ?>
        </div>
        <div class="col-sm-12 text-center">
            <br />
        </div>
        <div class="col-sm-4">
            <b>Trip Date:</b> <?php echo GUtils::clientDate($tourData['startdate']) ; ?> to <?php echo GUtils::clientDate($tourData['enddate']) ;?><br />
            <b>Trip Price:</b> <?php echo GUtils::formatMoney($tourData['listprice']) ;?>
        </div>
        <div class="col-sm-4">
            <b>Room Type:</b> Double<br />
            <b>Travelling With:</b> None
        </div>
        <div class="col-sm-4">
            <b>Trip Destination:</b> <?php echo $tourData['destination'] ;?><br />
            <b>Departure City:</b> <?php echo $tourData['Departure_City'] ;?>
        </div>
    </div>
</div>