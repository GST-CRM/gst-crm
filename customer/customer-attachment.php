<?php 

$loggedUser = Users::loggedUser() ;
$leadid = $loggedUser['contactid'] ;

if( isset($__REQUEST['customer']) ) {
    $leadid = $__REQUEST['customer'] ;
}

if($_GET['action'] == "docedit") { 

    $mode = isset($_GET['mode']) ? $_GET['mode'] : "edit" ;

    $modeAttrib = '' ;
    if( $mode == 'edit' ) {
        $fileid = $_GET['fileid'];
        $con17 = new mysqli($servername, $username, $password, $dbname);
        if ($con17->connect_error) {die("Connection failed: " . $con17->connect_error);} 
        $sql17 = "SELECT * FROM media WHERE id=$fileid";
        $result17 = $con17->query($sql17);
        $docdata = $result17->fetch_assoc();
        $con17->close();
    }
    else if($mode == 'invoiceView' ) {
        $fileid = $_GET['fileid'];
        $sqla = "SELECT Customer_Invoice_Num, Customer_Invoice_Date, Attachment, Comments, Add_Date FROM customer_invoice 
            WHERE Customer_Account_Customer_ID=$leadid AND Customer_Invoice_Num = $fileid "; 
        $row = GDb::fetchRow($sqla) ;

        $docdata['mediatype'] = 'other' ;
        $docdata['name'] = 'Invoice_' . GUtils::clientDate($row["Customer_Invoice_Date"]);
        $docdata['id'] = 0 ;
        $docdata['mediadescription'] = $row['Comments'] ;
        $docdata['mediaurl'] = 'invattach/' .  $row["Attachment"];
        $modeAttrib = 'disabled'; 
    }
    else if($mode == 'paymentView' ) {
        $fileid = $_GET['fileid'];
            $sqla = "SELECT Customer_Payment_ID, Customer_Payment_Date, Attachment, Customer_Payment_Comments, Add_Date FROM customer_payments 
                WHERE Customer_Account_Customer_ID=$leadid AND Customer_Payment_ID = $fileid "; 
        
        $row = GDb::fetchRow($sqla) ;
        $docdata['mediatype'] = 'other' ;
        $docdata['name'] = 'Payment_' . GUtils::clientDate($row["Customer_Payment_Date"]);
        $docdata['id'] = 0 ;
        $docdata['mediadescription'] = $row['Customer_Payment_Comments'] ;
        $docdata['mediaurl'] = 'invattach/' .  $row["Attachment"];
        $modeAttrib = 'disabled'; 
    }
    else if($mode == 'refundView' ) {
        $fileid = $_GET['fileid'];
            $sqla = "SELECT Refund_ID, Refund_Date, Attachment, Cancelation_Reason, Add_Date FROM customer_refund 
                WHERE Customer_Account_Customer_ID=$leadid AND Refund_ID = $fileid "; 
        
        $row = GDb::fetchRow($sqla) ;
        $docdata['mediatype'] = 'other' ;
        $docdata['name'] = 'Refund_' . GUtils::clientDate($row["Refund_Date"]);
        $docdata['id'] = 0 ;
        $docdata['mediadescription'] = $row['Cancelation_Reason'] ;
        $docdata['mediaurl'] = 'invattach/' .  $row["Attachment"];
        $modeAttrib = 'disabled'; 
    }

?>

<form name="contact7" id="contact7" action="customer/customer-upload.php" method="POST" class="row">
	<div class="col-sm-12">
		<h6>Edit the Attachment</h6>
		<br />
	</div>
	<div class="col-sm-2" >
		<label class="float-label">Document Type</label>
		<select name="attachmenttype" class="form-control form-control-default" <?php echo $modeAttrib;?> >
			<option value="passport" <?php if($docdata['mediatype'] == "passport") { echo "selected";} ?>>Passport</option>
			<option value="payment" <?php if($docdata['mediatype'] == "payment") { echo "selected";} ?>>Payment</option>
			<option value="application" <?php if($docdata['mediatype'] == "application") { echo "selected";} ?>>Application</option>
			<option value="other" <?php if($docdata['mediatype'] == "other") { echo "selected";} ?>>Other</option>
		</select>
	</div>
	<div class="col-sm-4 ">
		<label class="float-label">Document Name</label>
		<input type="text" name="documentname" value="<?php echo $docdata['name']; ?>" class="form-control" required <?php echo $modeAttrib;?> >
		<input type="text" name="documentid" value="<?php echo $docdata['id']; ?>" class="form-control" hidden>
	</div>
	<div class="col-sm-6 ">
		<label class="float-label">Document Description</label>
		<input type="text" name="description" value="<?php echo $docdata['mediadescription']; ?>" class="form-control" <?php echo $modeAttrib;?> >
	</div>
	<div class="col-sm-12" style="text-align:center;">
	<br />
	<?php $filetype = array_pop(explode('.', $docdata['mediaurl']));
	if($filetype == "pdf" OR $filetype == "doc" OR $filetype == "docx") {?>
		<iframe src="https://docs.google.com/gview?url=<?php echo $crm_path . "uploads/" . $docdata['mediaurl']; ?>&embedded=true" style="width:100%; height:400px;" frameborder="1"></iframe>
	<?php } else { ?>
		<img src="uploads/<?php echo $docdata['mediaurl']; ?>" alt="<?php echo $docdata['name']; ?>" style="width:auto;height:400px;margin:auto;" />
	<?php } ?>
	</div>
	<div class="col-sm-12"><br /><br />
		<button <?php echo $modeAttrib;?> type="submit" name="updatedocument" value="submit" class=" <?php echo $modeAttrib;?> btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Update</button>
		<a href="customer-page-attachment.php?id=<?php echo $leadid; ?>&action=edit&usertype=attachments" class="btn waves-effect waves-light btn-inverse" style="margin-right:20px;"><i class="fas fa-ban"></i>Go back to the attachments list</a>
		<a  data-toggle="modal" data-target="#delete-attachment" href="javascript:void(0);" data-lead="<?php echo $leadid; ?>" data-file="<?php echo $docdata["id"]; ?>" onclick="return prepareDeleteAttachment(this)"
           class=" <?php echo $modeAttrib;?> btn waves-effect waves-light btn-danger" 
           style="margin-right:20px;"><i class="fas fa-trash"></i>Remove the attachment</a>
	</div>
</form>
<?php } else { ?>
<?php if($_GET['message'] != NULL) { ?>
<div style='margin-top:25px;' class='alert alert-danger background-danger'>
	<button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button>
	<strong><?php echo base64_decode($_GET['message']); ?></strong>
</div>
<?php } ?>

<?php
$TAB_DESTINATION = 'customer-page-attachment.php' ;
include __DIR__ . '/fragment-subtabs.php' ;
?>

<h5>Attachments</h5>
<br />
<table class="table table-hover table-striped table-bordered">
    <thead>
        <tr role="row">
            <th style="width:10%;">Type</th>
            <th>Name</th>
            <th class="d-none">Description</th>
            <th style="width:20%;" class="text-center">Upload Time</th>
            <th style="width:15%;" class="text-center">Actions</th>
        </tr>
    </thead>
    <tbody>
	<?php
        $hasRecords = false ;
	$sql = "select * from media WHERE userid='$leadid'"; $result = $db->query($sql);
	if ($result->num_rows > 0) {
            $hasRecords = true ;
            while($row = $result->fetch_assoc()) { ?>
        <tr>
			<td><?php echo $row["mediatype"]; ?></td>
			<td><a href="customer-page-attachment.php?id=<?php echo $leadid; ?>&usertype=attachments&fileid=<?php echo $row["id"]; ?>&action=docedit" style="color: #0000EE;"><?php echo $row["name"]; ?></a></td>
			<td class="d-none"><span data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $row['mediadescription']; ?>"><?php echo substr($row['mediadescription'], 0, 30)."..."; ?></span></td>
			<td><?php echo $row["createtime"]; ?></td>
			<td style="text-align:center;">
                <?php if( $leadid == $row['owner_contactid']) { ?>
                <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
					<a href="uploads/<?php echo $row["mediaurl"]; ?>" download><img src="files/assets/images/download.png" style="width:25px;height:auto;" alt="download" /></a>
                </span>
				<span data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete">
					<a data-toggle="modal" data-target="#delete-attachment" href="javascript:void(0);" data-lead="<?php echo $leadid; ?>" data-file="<?php echo $row["id"]; ?>" onclick="return prepareDeleteAttachment(this)"><img src="files/assets/images/remove.png" style="margin-left:5px;width:22px;height:auto;" alt="remove" /></a>
				</span>
                <?php } ?>
			</td>
        </tr>
	<?php }
            } 
                
            //Find attachments for this contact from all groups.
            $sqla = "SELECT Customer_Payment_ID, Customer_Payment_Date, Attachment, Customer_Payment_Comments, Add_Date FROM customer_payments 
                WHERE Customer_Account_Customer_ID=$leadid AND (Attachment IS NOT NULL AND LENGTH(TRIM(Attachment)) > 0)"; 
            $records = GDb::fetchRowSet($sqla) ;
            
            foreach( $records as $row ) { $hasRecords = true ; ?>
        <tr>
            <td>Payment</td>
            <td>
                <a href="customer-page-attachment.php?mode=paymentView&id=<?php echo $leadid; ?>&usertype=attachments&fileid=<?php echo $row["Customer_Payment_ID"]; ?>&action=docedit" style="color: #0000EE;">
                <?php echo $row["Customer_Payment_Comments"]; ?>
                </a>
            </td>
            <td class="d-none"><span data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $row['Customer_Payment_Comments']; ?>"><?php echo substr($row['Customer_Payment_Comments'], 0, 30)."..."; ?></span></td>
            <td><?php echo GUtils::clientDateTime($row["Add_Date"]); ?></td>
            <td style="text-align:center;">
                <?php /* Not applicable for customer login 
                <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                    <a href="uploads/invattach/<?php echo $row["Attachment"]; ?>" download><img src="files/assets/images/download.png" style="width:25px;height:auto;" alt="download" /></a>
                </span>
                <span data-toggle="tooltip" data-placement="top" title="" data-original-title="View">
                    <a href="<?php echo 'payments-add.php?payment=' . $row['Customer_Payment_ID'];?>" ><img src="files/assets/images/invoice.png" style="margin-left:5px;width:20px;height:auto;" alt="View" /></a>
                </span>
                 */ ?>
            </td>
        </tr>
	<?php }
        
        
        
            //Find attachments for this contact from all groups.
            $sqla = "SELECT Customer_Invoice_Num, Customer_Invoice_Date, Attachment, Comments, Add_Date FROM customer_invoice 
                WHERE Customer_Account_Customer_ID=$leadid AND (Attachment IS NOT NULL AND LENGTH(TRIM(Attachment)) > 0)"; 
            $recordsInv = GDb::fetchRowSet($sqla) ;
            
            foreach( $recordsInv as $row ) { $hasRecords = true ; ?>
        <tr>
            <td>Invoice</td>
            <td>
                <a href="customer-page-attachment.php?mode=invoiceView&id=<?php echo $leadid; ?>&usertype=attachments&fileid=<?php echo $row["Customer_Invoice_Num"]; ?>&action=docedit" style="color: #0000EE;">
                <?php echo 'Invoice_' . GUtils::clientDate($row["Customer_Invoice_Date"]); ?>
                </a>
            </td>
            <td class="d-none"><span data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $row['Comments']; ?>"><?php echo substr($row['Comments'], 0, 30)."..."; ?></span></td>
            <td><?php echo GUtils::clientDateTime($row["Add_Date"]); ?></td>
            <td style="text-align:center;">
                <?php /* Not applicable for customer login 
                <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                    <a href="uploads/invattach/<?php echo $row["Attachment"]; ?>" download><img src="files/assets/images/download.png" style="width:25px;height:auto;" alt="download" /></a>                 
                </span>
                <span data-toggle="tooltip" data-placement="top" title="" data-original-title="View Invoice">
                    <a href="<?php echo 'sales-invoice-add.php?invid=' . $row['Customer_Invoice_Num'];?>" ><img src="files/assets/images/invoice.png" style="margin-left:5px;width:20px;height:auto;" alt="View" /></a>
                </span>
                 */ ?>
            </td>
        </tr>
	<?php
            }
            
            

        
            //Find attachments for this contact from all groups.
            $sqla = "SELECT Refund_ID, Refund_Date, Attachment, Cancelation_Reason, Add_Date FROM customer_refund 
                WHERE Customer_Account_Customer_ID=$leadid AND (Attachment IS NOT NULL AND LENGTH(TRIM(Attachment)) > 0)"; 
            $recordsRefund = GDb::fetchRowSet($sqla) ;
            
            foreach( $recordsRefund as $row ) { $hasRecords = true ; ?>
        <tr>
            <td>Refund</td>
            <td>
                <a href="customer-page-attachment.php?mode=refundView&id=<?php echo $leadid; ?>&usertype=attachments&fileid=<?php echo $row["Refund_ID"]; ?>&action=docedit" style="color: #0000EE;">
                <?php echo 'Refund_' . GUtils::clientDate($row["Refund_Date"]); ?>
                </a>
            </td>
            <td class="d-none"><span data-toggle="tooltip" data-placement="top" title="" data-original-title="<?php echo $row['Cancelation_Reason']; ?>"><?php echo substr($row['Cancelation_Reason'], 0, 30)."..."; ?></span></td>
            <td><?php echo GUtils::clientDateTime($row["Add_Date"]); ?></td>
            <td style="text-align:center;">
                <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Download">
                    <a href="uploads/invattach/<?php echo $row["Attachment"]; ?>" download><img src="files/assets/images/download.png" style="width:25px;height:auto;" alt="download" /></a>                 
                </span>
            </td>
        </tr>
	<?php
            }

            if( ! $hasRecords ) { ?>
        <tr>
			<td colspan="5" style="text-align:center;">There are no documents or attachments for this person yet.</td>
        </tr>
	<?php } ?>
    </tbody>
</table>
<br /><br />
<form name="contact6" id="contact6" action="customer/customer-upload.php" method="POST" class="dropzone row" enctype="multipart/form-data">
	<div class="col-sm-12">
		<h6>Upload A New Document</h6>
		<br />
	</div>
	<div class="col-sm-2" style="display:none;">
		<label class="float-label">Customer ID#</label>
		<input type="text" name="leadidupload" value="<?php echo $leadid; ?>" class="form-control" required hidden>
	</div>
	<div class="col-sm-2">
		<label class="float-label">Document Type</label>
		<select name="attachmenttype" class="form-control form-control-default">
			<option value="opt1" disabled selected></option>
			<option value="passport">Passport</option>
			<option value="payment">Payment</option>
			<option value="application">Application</option>
			<option value="other">Other</option>
		</select>
	</div>
	<div class="col-sm-4">
		<label class="float-label">Document Name</label>
		<input type="text" name="documentname" class="form-control" required>
	</div>
	<div class="col-sm-6">
		<label class="float-label">Doccument Description</label>
		<input type="text" name="description" class="form-control">
	</div>
	<div class="col-sm-12">
		<br /><label class="float-label" for="Customer_Attachment">Select the document to upload</label>
        <div class="Attachment_Box bg-white">
            <input type="file" id="Customer_Attachment" name="fileToUpload" class="Attachment_Input" >
            <p id="Customer_Attachment_Text" class="Attachment_Text">Drag the file here or click in this area.</p>
        </div>
		<br /><input type="submit" value="Upload" class="btn btn-success" name="attachmentsubmit" data-toggle="modal" data-target="#uploadingmodal" onclick="uploaddisablealert()">
	</div>
</form>
<script>
//This is to make the attachment box update its content when a file is selected
$(document).ready(function(){
  $('#Customer_Attachment').change(function () {
    $('#Customer_Attachment_Text').text("A file has been selected");
  });
});
function uploaddisablealert() {
  formmodified=0;
}
</script>

<?php } ?>