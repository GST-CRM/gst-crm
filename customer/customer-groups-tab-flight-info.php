<?php
include_once __DIR__ . '/../models/customer_account.php' ;
$loggedUser = Users::loggedUser() ;
$CustomerID = $loggedUser['contactid'] ;

if( isset($__REQUEST['customer']) ){
    $CustomerID = $__REQUEST['customer'] ;
}
$CustomerList = (new CustomerAccount())->get(['contactid' => $CustomerID]) ;
$TourID = $CustomerList['tourid'] ;
?>
<div class="tab-pane " id="flight-info" role="tabpanel">
    <div class="row">
        <div class="col-sm-12 mb-3">
            <h5 style="margin-top: 10px;">Flight Information</h5>
        </div>
        <div class="col-sm-12">
            <table cellpadding="5" class="col-sm-12 table table-gst table-bordered table-condensed table-light">
                <tbody>
                    <tr>
                        <th class="font-weight-bold" colspan="2" bgcolor="#ffcfcf" color="#ffffff"><strong>Departure</strong></th>
                        <th class="font-weight-bold" colspan="2" bgcolor="#ffcfcf" color="#ffffff"><strong>Arrival</strong></th>
                    </tr>
                    <?php 
                        //If Statements to get the correct details of the Flights
                        if($CustomersList['hisownticket'] == 0) {$CustomerID = 0;}
                        if($CustomersList['hisownticket'] == 1 AND $CustomersList['Same_Itinerary']==0) {$CustomerID = $CustomersList['contactid'];}
                        if($CustomersList['hisownticket'] == 1 AND $CustomersList['Same_Itinerary']==1) {$CustomerID = 0;}

                        $FlightCounter = 1;
                        $FlightsListSQL = mysqli_query($db,"SELECT
                            ga.Airline_Name,
                            ga.Airline_Code,
                            gf.Flight_Airline_Number,
                            gf.Departure_Time,
                            gp.Airport_City AS Departure_Airport_City,
                            gp.Airport_State AS Departure_State,
                            gp.Airport_Code AS Departure_Airport,
                            gf.Arrival_Time,
                            gp2.Airport_City AS Arrival_Airport_City,
                            gp2.Airport_State AS Arrival_State,
                            gp2.Airport_Code AS Arrival_Airport
                        FROM groups_flights gf 
                        LEFT JOIN groups_airline ga
                        ON gf.Flight_Airline_ID=ga.Airline_ID
                        LEFT JOIN groups_airport gp
                        ON gf.Departure_Airport=gp.Airport_ID
                        LEFT JOIN groups_airport gp2
                        ON gf.Arrival_Airport=gp2.Airport_ID
                        WHERE gf.Group_ID=$TourID AND gf.Specific_Customer_ID=$CustomerID
                        ORDER BY gf.GF_Line_ID ASC");
                        while($FlightsDetails = mysqli_fetch_array($FlightsListSQL)) { ?>
                    <tr>
                        <td colspan="4" style="text-align: center;color: #ef1c00; " ><h6 class="notez" style="margin-top:5px;margin-bottom:5px;"><?php echo "Flight ".$FlightCounter." (".$FlightsDetails['Airline_Name']." (".$FlightsDetails['Airline_Code'].") ".$FlightsDetails['Flight_Airline_Number'].")"; ?></h6></td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Airport:</strong> <?php echo $FlightsDetails['Departure_Airport_City'].", ".$FlightsDetails['Departure_State']." (".$FlightsDetails['Departure_Airport'].")"; ?></td>
                        <td colspan="2"><strong>Airport:</strong> <?php echo $FlightsDetails['Arrival_Airport_City'].", ".$FlightsDetails['Arrival_State']." (".$FlightsDetails['Arrival_Airport'].")"; ?></td>
                    </tr>
                    <tr>
                        <td colspan="2"><strong>Scheduled Time:</strong> <?php echo date('F d, Y | h:i A', strtotime($FlightsDetails['Departure_Time'])); ?></td>
                        <td colspan="2"><strong>Scheduled Time:</strong> <?php echo date('F d, Y | h:i A', strtotime($FlightsDetails['Arrival_Time'])); ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>