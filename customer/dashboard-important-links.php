<div class="card exp-build-card">
            <div class="card-header">
                <h5>Important Links</h5>
            </div>
            <div class="pt-0 card-block accordion-block">
                <ul style="list-style: circle inside;">
                    <li><a href="#">The safety and security policy</a></li>
                    <li><a href="#">Frequently Asked Questions</a></li>
                    <li><a target="_blank" href="https://www.tourtheholylands.com/buy-travel-insurance">All about the insurance</a></li>
                    <li><a target="_blank" href="https://www.tourtheholylands.com">Our Main Website</a></li>
                    <li><a target="_blank" href="https://www.facebook.com/GoodshepherdTravel/">Our Facebook Channel</a></li>
                </ul>
            </div>
        </div>