<?php
    include_once __DIR__ . '/../models/customer_payment.php';
    include_once __DIR__ . '/../models/customer_account.php';
    include_once __DIR__ . '/../models/users.php';
    
    $loggedUser = Users::loggedUser() ;
    $userContactId = $loggedUser['contactid'] ;
    
    $invoices = (new CustomerPayment())->paymentListing(" AND contactid = $userContactId ", "" ) ;
    
?>
<div class="row">
    <div class="col-sm-12" style="margin-bottom:20px;">
        <h5 style="margin-top: 10px;">My Invoices</h5>
    </div>
    <div class="col-sm-12">
        <table class="table table-hover table-striped table-smallz" width="100%">
            <thead>
                <tr>
                    <th>Invoices</th>
                    <th>Due Date</th>
                    <th>Amount</th>
                    <th class="text-center"><i class="feather icon-layers"></i></th>
                </tr>
            </thead>
            <tbody>
                <?php
                if(is_array($invoices) && count($invoices) > 0 ) { 
                foreach( $invoices as $invoice ) {
                    $CUSTOMER_INVOICE_ID = $invoice['Customer_Invoice_Num'] ;
                    ?>
                <tr>
                    <td><a href="#"><b>#<?php echo $invoice['Customer_Invoice_Num'];?> -</b> <?php echo $invoice['tourname'];?></a></td>
                    <td><?php echo GUtils::clientDate( $invoice['Due_Date']);?></td>
                    <td><?php echo GUtils::formatMoney( $invoice['Invoice_Amount_Revised']);?></td>
                    <td><a href="customer-page-invoice.php?invoiceId=<?php echo $invoice['Customer_Invoice_Num'];?>" class="pt-0 pb-0 btn waves-effect waves-light btn-inverse btn-outline-inverse btn-block">Invoice</a></td>
                </tr>
                <?php } 
                }else {?>
                <tr>
                    <td align='center' colspan="6">There are no records !</td>
                </tr>
                <?php } ?>
                <tr style="border-top: double 4px #ccc;background: #f9f9f9;">
                    <td style="font-weight:bold;" class="text-center" colspan="4">You are added to <?php echo count($invoices);?> group.</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
<?php
if( isset($__REQUEST['invoiceId']) ) {
    $CUSTOMER_INVOICE_ID = $__REQUEST['invoiceId'] ;
}
include __DIR__ . '/customer-invoice-tab.php' ;
?>