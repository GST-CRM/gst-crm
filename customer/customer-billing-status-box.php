<?php
    include_once __DIR__ . '/../models/customer_payment.php';
    include_once __DIR__ . '/../models/customer_account.php';
    
    $invoiceData = (new CustomerAccount())->customerInvoices($userContactId) ;
    $invoices = array_column($invoiceData , 'Customer_Invoice_Num') ;
    
    $billingStatus = (new CustomerPayment())->CustomerBillingStatus($invoices) ;
    $OWED = $billingStatus['owed'] ;
    $PAID = $billingStatus['paid'] ;
    $REMAINING = $billingStatus['remaining'] ;
?>
<div class="card">
            <div class="card-header">
                <h5>Customer Billing Status</h5>
            </div>
            <div class="card-block row" style="padding-top: 0px;">
                <div class="col-sm-12">
                    <h6>Owed: <span style="font-weight:400;"><?php echo GUtils::formatMoney($OWED); ?></span></h6>
                    <h6>Paid: <span style="font-weight:400;"><?php echo GUtils::formatMoney($PAID); ?></span></h6>
                    <h6>Remaining: <span style="font-weight:400;"><?php echo GUtils::formatMoney($REMAINING); ?></span></h6>
                </div>
            </div>
        </div>