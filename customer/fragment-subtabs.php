<?php
    include_once __DIR__ . '/../../models/customer_account.php';
    include_once __DIR__ . '/../../models/customer_group.php';
    $loggedUser = Users::loggedUser() ;
    $userContactId = $loggedUser['contactid'] ;
    $selectedCustomer = $userContactId ;
    if( isset($__REQUEST['customer']) ) {
        $selectedCustomer = $__REQUEST['customer']  ;
    }

    $groups = (new CustomerAccount())->customerGroups($userContactId) ;
    
    $customerSet = [] ;
    //move current customer to the first of the list.
    $customerSet[$userContactId] = [] ;    
    foreach( $groups as $tripId ) {
        if( ! (new CustomerGroups())->isPrimaryTraveler($tripId, $userContactId) ) {
            continue ;
        }
        $customers = (new CustomerGroups())->getJointMemberDetails($tripId, $userContactId) ;
        if( $customers ) {
            foreach( $customers as $one ) {
                $customerSet[$one['contactid']] = $one ;
            }
        }
    }
    //hide tabs if there only one.
    if(count($customerSet) < 2 ) {
        return;
    }
?>

<ul class="nav nav-tabs inner-tabs /*md-tabs*/" role="tablist">
    <?php
    foreach( $customerSet as $set ) {
        $name = $set['fname'] . ' ' . $set['mname'] . ' ' . $set['lname'];
        $customerId = $set['contactid'] ;
        
        $active = '' ;
        if( $customerId == $selectedCustomer ) {
            $active = 'active' ;
        }
    ?>
    <li class="nav-item">
        <a class="nav-link <?php echo $active; ?>" href="<?php echo $TAB_DESTINATION;?>?customer=<?php echo $customerId; ?>" ><?php echo $name;?></a>
        <div class="slide"></div>
    </li>
    <?php 
    }
    ?>
</ul>