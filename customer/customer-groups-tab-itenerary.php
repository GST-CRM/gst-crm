<?php
    $groupId = $__REQUEST['GroupID'] ;
    include_once __DIR__ . '/../models/groups.php';
    
    $tourData = (new Groups())->get(['tourid' => $groupId]) ;
?>
<div class="tab-pane " id="itinerary" role="tabpanel">
    <div class="row">
        <div class="col-sm-12 mb-3">
            <h5 style="margin-top: 10px;">Itinerary Information</h5>
        </div>
        <div class="col-sm-12">
            <?php
            if( $tourData['itinerary'] ) {
                echo $tourData['itinerary']; 
            }
            else {
                echo '<p>Itinerary information not available! </p>' ;
            }
            ?>
        </div>
    </div>
</div>