<?php include __DIR__ .  "/../session.php";

include_once __DIR__ . '/../models/users.php' ;
include_once __DIR__ . '/../models/contacts.php' ;

$loggedUser = Users::loggedUser() ;
$leadid = $loggedUser['contactid'] ;

$ciObj = new Contacts() ;
$cidata = $ciObj->get( ['id' => $leadid] ) ;
$agentEmail = $cidata['email'] ;
$userObj = new Users() ;
$userData = $userObj->get(['Username' => $agentEmail]) ;
$tourId = $userData['tourid'] ;

//To collect the data of the above customer id, and show it in the fields below
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {die("Connection failed: " . $conn->connect_error);} 
$sql = "SELECT * FROM contacts WHERE id=$leadid";
$result = $conn->query($sql);
$data = $result->fetch_assoc();
$conn->close();

$PageTitle = "Welcome ".$data["title"]." ".$data["fname"]." ".$data["mname"]." ".$data["lname"];

include "header.php";


//Collect the full name of the contact to show it in the following notifications; Delete and Update
$contactname = $data['fname']." ".$data['mname']." ".$data['lname'];


//To redirect the admin and users directly to the tab by using the url
if(isset($_GET['tab'])){
	$urlusertype = mysqli_real_escape_string($db, $_GET['tab']);
} else {
	$urlusertype = "contact";
}

?>
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->

<div class="row">
    <div class="col-md-9">
        <div class="card" style="overflow:hidden;">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs /*md-tabs*/" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#info" role="tab">Group Info</a>
                    <div class="slide"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#itinerary" role="tab">Itinerary</a>
                    <div class="slide"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#flight-info" role="tab">Flight Info</a>
                    <div class="slide"></div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#land-info" role="tab">Land Info</a>
                    <div class="slide"></div>
                </li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content card-block">

                <?php
                include __DIR__ . '/customer-groups-tab-info.php' ;
                include __DIR__ . '/customer-groups-tab-itenerary.php' ;
                include __DIR__ . '/customer-groups-tab-flight-info.php' ;
                include __DIR__ . '/customer-groups-tab-land-info.php' ;
                ?>
                
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="card-block row">
                <div class="col-sm-12">
                    <a href="#" class="btn btn-block btn-inverse">Make a Payment</a>
                    <a href="#" class="btn btn-block btn-inverse">View Invoices</a>
                    <a href="#" class="btn btn-block btn-inverse">Download the Brochure</a>
                    <a href="#" class="btn btn-block btn-inverse">Submit a Testimonial</a>
                    <a target="_blank" href="<?php echo $crm_path;?>inc/tickets-action.php?GroupTicketID=<?php echo $tourId;?>&batch-invaction=P&TicketID[]=<?php echo $leadid;?>" class="btn btn-block btn-inverse">Print Ticket</a>
                </div>
            </div>
        </div>
    </div>
    <?php //include 'inc/notificiations.php'; ?>

    <?php require('inc/metadata-js.php'); ?>
    <script type="text/javascript">
        $(document).ready(function() {
            $("form:not(.not-common-form)").submit(function() {
                $('#results').html('');
                // Getting the form ID
                var formID = $(this).attr('id');
                var formDetails = $('#' + formID);
                //To not let the form of uploading attachments included in this
                if (formID != 'contact6') {
                    $.ajax({
                        type: "POST",
                        url: 'customer/customer-save.php',
                        data: formDetails.serialize(),
                        success: function(data) {
                            // Inserting html into the result div
                            $('#results').html(data);
                            $('.confirmation-message-result-area').html(data);
                            //$("form")[0].reset();
                            formmodified = 0;
                        },
                        error: function(jqXHR, text, error) {
                            // Displaying if there are any errors
                            $('#result').html(error);
                        }
                    });
                    return false;
                }
            });
        });

    </script>
    <!-- sweet alert js -->
    <script type="text/javascript" src="files/bower_components/sweetalert/js/sweetalert.min.js"></script>
    <!-- modalEffects js nifty modal window effects -->
    <script type="text/javascript" src="files/assets/js/modalEffects.js"></script>
    <?php include "footer.php"; ?>
