<div class="tab-pane " id="land-info" role="tabpanel">
    <div class="row">
        <div class="col-sm-12 mb-3">
            <h5 style="margin-top: 10px;">Land Information</h5>
        </div>
        <div class="col-sm-12">
            <table class="table table-hover table-striped table-smallz">
			<tbody>
				<?php 
				$parentidx2 = 1;
				$sql = "SELECT * FROM products WHERE parentid=$parentidx2 AND subproduct=1 AND status=1";
				$result = $db->query($sql);
				if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
				<tr>
					<td style="width:25px;"><img src="<?php echo $crm_images; ?>/<?php echo $row["producttype"]; ?>.png" alt="<?php echo $row["producttype"]; ?>" style="width:22px; height:auto;" /></td>
					<td><a href="products-edit.php?id=<?php echo $row["id"]; ?>&action=edit"><?php echo $row["name"]; ?></a></td>
					<td><?php echo $row["meta1"]; ?></td>
					<td><?php echo $row["meta2"]; ?></td>
					<td><?php echo $row["meta3"]; ?></td>
					<td><?php echo $row["meta4"]; ?></td>
				</tr>
				<?php } ?>
				<tr>
					<td colspan="6" style="text-align:center;"><button type="reset" name="addnew" class="btn waves-effect waves-light btn-inverse" style="padding-top: 5px;padding-bottom: 5px;" data-toggle="modal" data-target="#addnewsubproduct">Add A New Sub-Product</button></td>
				</tr>
				<?php } else { ?>
				<tr>
					<td colspan="5">There are no subproducts for this package!</td>
				</tr>
				<?php } ?>
			</tbody>
		</table>
        </div>
    </div>
</div>