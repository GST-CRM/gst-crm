<?php 
include "session.php";
$modalHtml 			= "";
if (!empty($_REQUEST['tourid']) && !empty($_REQUEST['type']))
{
	$tourid 	= $_REQUEST['tourid'];
	switch ($_REQUEST['type'])
	{
		case "with_ticket" :
			$modalHtml = include 'inc/group-tabs-filter-with-ticket.php';
			break;

		case "without_ticket" :
			$modalHtml = include 'inc/group-tabs-filter-without-ticket.php';
			break;
			
		case "single_room" :
			$modalHtml = include 'inc/group-tabs-filter-single-rooms.php';
			break;
			
		case "air_upgrade" :
			$modalHtml = include 'inc/group-tabs-filter-air-upgrades.php';
			break;
			
		case "land_upgrade" :
			$modalHtml = include 'inc/group-tabs-filter-land-upgrades.php';
			break;
			
		case "complimentary" :
			$modalHtml = include 'inc/group-tabs-filter-complimentary.php';
			break;
			
		case "need_transfer" :
			$modalHtml = include 'inc/group-tabs-filter-transfer.php';
			break;
			
		case "need_extension" :
			$modalHtml = include 'inc/group-tabs-filter-extension.php';
			break;
			
		case "need_insurance" :
			$modalHtml = include 'inc/group-tabs-filter-insurance.php';
			break;

	}
}
$modalHtml;
exit;
?>