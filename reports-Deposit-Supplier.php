<?php
include "session.php";

$PageTitle = "Airline & Land Products Deposits Report";
include "header.php"; 

// Get the values from the main page first
if(isset($_GET['CustomerSelected'])) {$CustomerSelected = mysqli_real_escape_string($db, $_GET['CustomerSelected']);}
if(isset($_GET['GroupSelected'])) {$GroupSelected = mysqli_real_escape_string($db, $_GET['GroupSelected']);}
if(isset($_GET['FromDate'])) {$FromDateFilter = mysqli_real_escape_string($db, $_GET['FromDate']);}
if(isset($_GET['UntilDate'])) {$UntilDateFilter = mysqli_real_escape_string($db, $_GET['UntilDate']);}
if(isset($_GET['type'])) {$type = mysqli_real_escape_string($db, $_GET['type']);}



if(isset($_POST['UpdateDepositData'])) {
    $productid = mysqli_real_escape_string($db, $_POST['productid']);
	$groupid = mysqli_real_escape_string($db, $_POST['groupid']);
	$depositamount = mysqli_real_escape_string($db, $_POST['depositamount']);
	$depositdate = mysqli_real_escape_string($db, $_POST['depositdate']);
	$returnedamount = mysqli_real_escape_string($db, $_POST['returnedamount']);
	$returneddate = mysqli_real_escape_string($db, $_POST['returneddate']);
    
	$UpdateDepositSQL = "UPDATE products SET Returned_Amount='$returnedamount',Deposit_Amount='$depositamount',Deposit_Return_Date='$returneddate',Deposit_Date='$depositdate' WHERE id=$productid;";
	if(GDb::execute($UpdateDepositSQL)) {
        GDb::execute("INSERT INTO logs (username, usertype, contactid, action, text) VALUES ('$login_session','product','$productid','updated','#$productid');");
		
        echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected supplier record has been updated successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error updating record: " . $db->error . "</strong></div>";
	}
    
    $SamePageURL = "reports-Deposit-Supplier.php?".$_SERVER['QUERY_STRING'];
    echo "<meta http-equiv='refresh' content='2;$SamePageURL'>";
}
?>
<style>
.select2-container--default .select2-selection--single .select2-selection__rendered {background:white;color:#333;padding:5px 30px 4px 20px;border-radius:4px;}
.select2-container--default .select2-selection--single .select2-selection__arrow {top:6px;}
.select2-container--default .select2-selection--single .select2-selection__arrow b {border-color:black transparent transparent transparent;}
div.dt-buttons { float:right !important; }
div.dataTables_wrapper div.dataTables_filter { text-align:left !important; }
</style>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
		<div class="card">
            <form class="row mb-3" action="reports-Deposit-Supplier.php" method="GET">
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;display:none;">
						<label class="float-label" for="cbBIRTH">Search for a specific Supplier</label>
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
					</div>
				</div>
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
						<label class="float-label" for="SpecificCustomer">Search for a specific Supplier</label>
						<select id="SpecificCustomer" name="CustomerSelected" class="col-sm-12">
							<?php
							//To show the selected customer if it was selected in the GET variable
							if(!empty($CustomerSelected)) { 
								$CustomerzData = explode(':', $CustomerSelected);
								$CustomerzDataID = $CustomerzData[0];
								$CustomerzDataName = $CustomerzData[1];
								echo "<option value='".$CustomerzDataID.":".$CustomerzDataName."' selected>".$CustomerzDataName."</option>";
							} else { 
								$CustomerzDataID = 0;
								echo "<option value='0'>-</option>";
							}
							?>
						</select>
				</div>
				<div class="col-md-3" style="margin-top:15px;">
						<label class="float-label" for="SpecificGroup">Search for a specific Group</label>
						<select id="SpecificGroup" name="GroupSelected" class="col-sm-12">
							<option value='0'>-</option>
							<?php
							//To show the selected customer if it was selected in the GET variable
							if(!empty($GroupSelected)) { 
								$GroupzData = explode(':', $GroupSelected);
								$GroupzDataID = $GroupzData[0];
								$GroupzDataName = $GroupzData[1];
								echo "<option value='".$GroupzDataID.":".$GroupzDataName."' selected>".$GroupzDataName."</option>";
							} else { 
								$GroupzDataID = 0;
								echo "<option value='0'>-</option>";
							}
							?>
						</select>
				</div>
				<div class="col-md-6" style="margin-top:15px;">
					<div class="col-12 row">
						<div class="form-group form-default form-static-label col-md-4">
							<label class="float-label" for="FromDate">From</label>
							<input class="form-control" id="FromDate" type="date" name="FromDate" value="<?php if(!empty($FromDateFilter)) {echo $FromDateFilter;} ?>">
						</div>
						<div class="form-group form-default form-static-label col-md-4">
							<label class="float-label" for="UntilDate">Until</label>
							<input class="form-control" id="UntilDate" type="date" name="UntilDate" value="<?php if(!empty($UntilDateFilter)) {echo $UntilDateFilter;} ?>">
						</div>
						<div class="form-group form-default form-static-label col-md-2 d-none">
							<label class="float-label" for="type">Type</label>
							<select id="type" name="type" class="form-control form-control-default fill">
								<option value="all" <?php if(!empty($type) AND $type == "all") {echo "selected";} ?>>All</option>
								<option value="Checks" <?php if(!empty($type) AND $type == "Checks") {echo "selected";} ?>>Checks</option>
								<option value="Cash" <?php if(!empty($type) AND $type == "Cash") {echo "selected";} ?>>Cash</option>
							</select>
						</div>
						<div class="form-group form-default form-static-label col-md-1">
							<label class="float-label" for="ClearFilterOptions">Clear</label>
							<button id="ClearFilterOptions" type="button" class="btn btn-inverse btn-block waves-effect waves-light" style="width:100%; padding: 5px 2px;"><i class="fas fa-eraser"></i></button>
						</div>
						<div class="form-group form-default form-static-label col-md-3 pr-0">
							<hr style="border: 0;height: 1px;background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0));margin-top: 14px;margin-bottom: 13px;" />
							<button type="submit" class="btn btn-info btn-block waves-effect waves-light" style="padding: 5px 13px;">Filter</button>
						</div>
					</div>
				</div>
			</form>
            <div class="card-block">
				<div class="dt-responsive table-responsive">
                    <table id="customers_list" class="table table-hover table-striped table-bordered responsive nowrap" style="width:100%;" data-page-length="20">
                        <thead>
                            <tr>
                                <th>Company Name</th>
                                <th>Supplier Name</th>
                                <th>Group Reference</th>
                                <th>Deposit Amount</th>
                                <th>Deposit Date</th>
                                <th>Confirmation #</th>
                                <th>Returned Amount</th>
                                <th class="d-none">Received Date</th>
                                <th width="30" class="text-center">Actions</th>
                            </tr>
                        </thead>
						<tbody>
<?php 
//Advanced Filters for the Deposit Report List Page
$DepositFilters = "";

//The Type of the payment: Check or Cash
if($type == "all") {
	$DepositFilters.= "";
}elseif($type == "Checks") {
	$DepositFilters.= " AND cp.Customer_Payment_Method='Check' ";
}elseif($type == "Cash") {
	$DepositFilters.= " AND cp.Customer_Payment_Method='Cash' ";
}

//The from date and the until date filters
if($FromDateFilter != 0) {
	$DepositFilters.= " AND pro.Deposit_Date >= '$FromDateFilter' ";
}
if($UntilDateFilter != 0) {
	$DepositFilters.= " AND pro.Deposit_Date <= '$UntilDateFilter' ";
}

//The customer ID filter
if($CustomerzDataID > 0) {
	$DepositFilters.= " AND pro.supplierid = $CustomerzDataID ";
}

//The Group ID filter
if($GroupzDataID > 0) {
	$DepositFilters.= " AND gro.tourid = $GroupzDataID ";
}
							
//The default search clause to not show the zero records
if($DepositFilters == "") {
	$DepositFilters = "HAVING (pro.Deposit_Amount-pro.Returned_Amount) != 0";
}
							
							
//The deposit get query
$GetDeposits = "SELECT
	pro.supplierid AS Supplier_ID,
    con.company AS Supplier_Company,
    CONCAT(con.fname,' ',con.lname) AS Supplier_Name,
    gro.tourid AS Group_ID,
    gro.tourdesc AS Group_Ref,
    pro.id AS Product_ID,
    pro.Deposit_Amount,
    pro.Deposit_Date,
    pro.Confirmation,
    pro.Returned_Amount,
    pro.Deposit_Return_Date
FROM groups gro
JOIN products pro
ON pro.id=gro.airline_productid
JOIN contacts con 
ON con.id=pro.supplierid
WHERE 1
$DepositFilters
ORDER BY pro.Deposit_Date DESC";
$DepositRecords = GDb::fetchRowSet($GetDeposits);
$TotalDepositsRecorded = 0;
$TotalReturnedDepositsRecorded = 0;
foreach ($DepositRecords AS $Deposit) {
?>
<tr>
	<td><?php echo $Deposit['Supplier_Company']; ?></td>
	<td><?php echo "<a href='contacts-edit.php?id=".$Deposit['Supplier_ID']."&action=edit&usertype=supplier' style='color: #0000EE;'>".$Deposit['Supplier_Name']."</a>"; ?></td>
	<td><?php echo "<a href='groups-edit.php?id=".$Deposit['Group_ID']."&action=edit&tab=airline' style='color: #0000EE;'>".$Deposit['Group_Ref']."</a>"; ?></td>
	<td><?php echo GUtils::formatMoney($Deposit['Deposit_Amount']); ?></td>
	<td><?php echo GUtils::clientDate($Deposit['Deposit_Date']); ?></td>
	<td><?php echo substr($Deposit['Confirmation'],0,10); ?></td>
    <td><?php echo GUtils::formatMoney($Deposit['Returned_Amount']); ?></td>
	<td class="d-none"><?php echo GUtils::clientDate($Deposit['Deposit_Return_Date']); ?></td>
	<td class="text-center">
		<a href="javascript:void(0)"
           class="letstryit btn btn-block btn-primary btn-sm pt-1 pb-1"
           data-toggle="modal"
           data-target="#UpdateDepositData"
           data-productid="<?php echo $Deposit['Product_ID']; ?>"
           data-groupid="<?php echo $Deposit['Group_ID']; ?>"
           data-groupreference="<?php echo $Deposit['Group_Ref']; ?>"
           data-depositamount="<?php echo $Deposit['Deposit_Amount']; ?>"
           data-depositdate="<?php echo $Deposit['Deposit_Date']; ?>"
           data-returnedamount="<?php echo $Deposit['Returned_Amount']; ?>"
           data-returneddate="<?php echo $Deposit['Deposit_Return_Date']; ?>"
           >Edit</a>
	</td>
</tr>
<?php $TotalDepositsRecorded += $Deposit['Deposit_Amount']; $TotalReturnedDepositsRecorded += $Deposit['Returned_Amount']; } ?>
						</tbody>
						<tfoot>
							<tr>
								<th></th>
								<th></th>
								<th style="text-align:right">Total Deposits:</th>
								<th><?php echo GUtils::formatMoney($TotalDepositsRecorded); ?></th>
                                <th></th>
								<th style="text-align:right">Total Returned Deposits:</th>
								<th><?php echo GUtils::formatMoney($TotalReturnedDepositsRecorded); ?></th>
								<th class="d-none"></th>
								<th></th>
							</tr>
						</tfoot>
					</table>
                </div>
            </div>
        </div>
	</div>
</div>

<script>
//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#customers_list').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
});

const formatter = new Intl.NumberFormat('en-US', {
	style: 'currency',
	currency: 'USD',
	minimumFractionDigits: 2
})


//To customize the datatables function
$( document ).ready(function() {
	$('#customers_list').DataTable({
		"lengthMenu": [ [10,20,50,100,1000,-1], [10,20,50,100,1000,"All"] ],
		//"order": [[ 1, "desc" ]],
		"ordering": false,
		dom: 'Bfrtip',
		buttons: ['csv',{
                    extend: 'excel',
                    footer: true,
                    title: 'Report',
                    filename: 'Report',
                }],
		"language": {
			"emptyTable": "There are no deposits available."
		},
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;
			// Remove the formatting to get integer data for summation
			var intVal = function ( i ) {
				return typeof i === 'string' ?
				i.replace(/[\$,]/g, '')*1 :
				typeof i === 'number' ?
				i : 0;
			};

			// Total over all pages
            total = api
                .column( 3 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 3, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 3 ).footer() ).html(
                formatter.format(pageTotal) +'<br /><small>(Total: '+ formatter.format(total) +')</small>' 
            );
        },
		columnDefs: [
			{ width: 110, targets: 0 },
			{ width: 300, targets: 1 },
			{ width: 200, targets: 3 },
			{ width: 150, targets: 4 }
		]
	});   
});

//To create select2 function for the customers dropdown
$(document).ready(function(){
   $("#SpecificCustomer").select2({
      ajax: {
        url: "inc/select2-customers.php",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
           return {
              searchTerm: params.term,
              DepositsFeature: 'active'
           };
        },
        processResults: function (response) {
           return {
              results: response
           };
        },
        cache: true
      }
   });
});

//To create select2 function for the groups dropdown
$(document).ready(function(){
   $("#SpecificGroup").select2({
      ajax: {
        url: "inc/select2-groups.php",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
           return {
              searchTerm: params.term
           };
        },
        processResults: function (response) {
           return {
              results: response
           };
        },
        cache: true
      }
   });
});

$('#ClearFilterOptions').click(function(){
	document.getElementById("FromDate").value = "0000-00-00";
	document.getElementById("UntilDate").value = "0000-00-00";
	$('#type').val('all');
	//$("#SpecificGroup").empty();
	$("#SpecificGroup").select2("val", "0");
	$("#SpecificCustomer").select2("val", "0");
});
    
    
$(document).on("click", ".letstryit", function () {
    var groupreference = $(this).data('groupreference');
    $(".DepositModalBody #productid").val( $(this).data('productid') );
    $(".DepositModalBody #groupid").val( $(this).data('groupid') );
    document.getElementById('groupreference').innerHTML = 'Group Reference: '+groupreference;
    $(".DepositModalBody #depositamount").val( $(this).data('depositamount') );
    $(".DepositModalBody #depositdate").val( $(this).data('depositdate') );
    $(".DepositModalBody #returnedamount").val( $(this).data('returnedamount') );
    $(".DepositModalBody #returneddate").val( $(this).data('returneddate') );
    // As pointed out in comments, 
    // it is unnecessary to have to manually call the modal.
    // $('#addBookDialog').modal('show');
});
</script>

<?php //This popup to show the details of the changes for a specific customer ?>
<div class="modal fade" id="UpdateDepositData" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" style="background:white;border-radius:10px;" role="document">
		<div class="sweet-alert showSweetAlert sweet-alert-customized" data-custom-class="" data-has-cancel-button="false" data-has-confirm-button="true" data-allow-outside-click="false" data-has-done-function="false" data-animation="pop" data-timer="null" style="display: block;">
            <h2 class="mb-0">Update the Deposits Details</h2>
            <h5 class="mb-4" id="groupreference" style="color:#666;">Group Reference</h5>
            
            <form class="DepositModalBody row" action="reports-Deposit-Supplier.php?<?php echo $_SERVER['QUERY_STRING']; ?>" method="POST">
                <div class="col-3 text-left d-none">
                    <label class="mb-0" for="productid">The product ID</label>
                    <input style="display:block;font-size:14px;" type="number" name="productid" id="productid" placeholder="N/A">
                </div>
                <div class="col-3 text-left d-none">
                    <label class="mb-0" for="productid">The Group ID</label>
                    <input style="display:block;font-size:14px;" type="number" name="groupid" id="groupid" placeholder="N/A">
                </div>
                <div class="col-3 text-left">
                    <label class="mb-0" for="productid">The Deposit Amount</label>
                    <input style="display:block;font-size:14px;" type="text" name="depositamount" id="depositamount" placeholder="N/A">
                </div>
                <div class="col-3 text-left">
                    <label class="mb-0" for="productid">The Deposit Date</label>
                    <input style="display:block;font-size:14px;" type="date" name="depositdate" id="depositdate" placeholder="N/A">
                </div>
                    <div class="col-3 text-left">
                    <label class="mb-0" for="productid">The Returned Amount</label>
                    <input style="display:block;font-size:14px;" type="text" name="returnedamount" id="returnedamount" placeholder="N/A">
                </div>
                <div class="col-3 text-left">
                    <label class="mb-0" for="productid">The Returned Date</label>
                    <input style="display:block;font-size:14px;" type="date" name="returneddate" id="returneddate" placeholder="N/A">
                </div>
                <div class="col-12 sa-button-container">
                    <button type="submit" name="UpdateDepositData" value="submit" class="btn btn-default waves-effect mt-2">Update</button>
                </div>
            </form>
            
        </div>
    </div>
</div>

<?php include "footer.php"; ?>