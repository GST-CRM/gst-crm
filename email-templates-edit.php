<?php
include "session.php";
$PageTitle = "Edit New Template";
include "header.php";

$allowed = array('edit');
$action = $_GET['action'];
if ( ! isset($_GET['action'])) {header("location: email-templates.php");die('Please go back to the main page.');}
$action = $_GET['action'];
if ( ! in_array($action, $allowed)) {header("location: email-templates.php");die('Please go back to the main page.');}


$template_id = $_GET["id"];
$conn = new mysqli($servername, $username, $password, $dbname);
if ($conn->connect_error) {die("Connection failed: " . $conn->connect_error);} 
$sql = "SELECT * FROM email_templates_new WHERE template_id=$template_id";
$result = $conn->query($sql);
$data = $result->fetch_assoc();
$conn->close();

$edit_template_name = mysqli_real_escape_string($db, $data["template_name"]);
$edit_template_type_id = mysqli_real_escape_string($db, $data["template_type_id"]);
$edit_template_subject = mysqli_real_escape_string($db, $data["template_subject"]);
$edit_template_description = mysqli_real_escape_string($db, $data["template_description"]);
$edit_template_body =  $data["template_body"];
$edit_status = mysqli_real_escape_string($db, $data["status"]);

?>
<!--<script src="https://cdn.ckeditor.com/4.12.1/standard-all/ckeditor.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.2.2/tinymce.min.js" integrity="sha256-MYEJOVodtWOmhHp5ueLNwfCwBBGKWJWUucfLvXzdles=" crossorigin="anonymous"></script>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h5>
					<?php echo 'Edit Template #' . $_GET['id']; ?>
                </h5>
            </div>
            <form name="templateEditForm" action="inc/email-template-functions.php" method="POST" id="contact1" class="card-block gst-block  row gst-no-right-p" >
                <div class="col-md-12 row">
                    <div class="form-group form-default form-static-label col-sm-8">
                        <input name="email_template_id" value="<?php echo $template_id; ?>" type="text" hidden>
                        <label class="float-label gst-label">Folder</label>
                        <select name="template_type_id" class="form-control" required>
                            <?php
                                $templateTypeResult = mysqli_query($db,"SELECT ett.type_id,ett.name FROM email_templates_type ett WHERE ett.status=1");
                                while($ResultData = mysqli_fetch_array($templateTypeResult))
                                {  
                                    if($ResultData['type_id'] == $edit_template_type_id) { $SelectedTemplate = "selected"; } else { $SelectedTemplate = ""; } 
                                    echo "<option value='".$ResultData['type_id']."' $SelectedTemplate>".$ResultData['name']."</option>";
                                }
                            ?>
                        </select>
                    </div>

                    <?php
                        $showAvailableForUse = 'style="display:block;"';
                        $disableEmailTemplateName = '';

                        if ($edit_template_name == 'Footer Email Template - Dont Delete') { 
                         $showAvailableForUse = 'style="display:none;"';
                         $disableEmailTemplateName = 'readonly';
                    }?>
                    <div class="form-group col-sm-4" <?php echo $showAvailableForUse ?>>
                        <label class="float-label gst-label">Available For Use</label>
                        <div class="can-toggle">
                          <input id="status" name="status" value="1" type="checkbox" <?php if ($edit_status == "1") { echo "checked"; } ?>>
                          <label for="status">
                            <div class="can-toggle__switch" data-checked="Active" data-unchecked="Disabled"></div>
                          </label>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 row">
                    <div class="form-group form-default form-static-label col-sm-6">
                        <label class="float-label gst-label">Email Template Name</label>
                        <input type="text" name="template_name" id="template_name" class="form-control" value="<?php echo $edit_template_name; ?>" style="padding-left: 10px" required <?php echo $disableEmailTemplateName ?>>
                    </div>

                    <div class="form-group form-default form-static-label col-sm-6">
                        <label class="float-label gst-label">Description</label>
                        <input type="text" name="template_description" id="template_description" class="form-control" value="<?php echo $edit_template_description; ?>" style="padding-left: 10px">
                    </div>
                </div>

                <div class="col-md-12 row">
                    <div class="form-group form-default form-static-label col-sm-12">
                        <label class="float-label gst-label">Subject</label>
                        <input type="text" name="template_subject" id="template_subject" class="form-control" value="<?php echo $edit_template_subject; ?>" style="padding-left: 10px" required>
                    </div>
                </div>

                <div class="col-md-12 row">
                    <div class="form-group form-default form-static-label col-sm-12">
                        <label class="float-label gst-label">Email Body</label>
                        <?php /* <textarea cols="80" id="template_body" class="ckeditor" name="template_body" rows="10" required data-sample-short><?php echo $edit_template_body; ?></textarea> */ ?>
                        <textarea cols="80" id="tinyMce" class="tinyMce" name="template_body" rows="10" 
                                  required data-sample-short><?php echo $edit_template_body; ?></textarea>
                    </div>
                </div>

                <div class="col-md-12 row">
                    <div class="form-group form-default form-static-label col-sm-12">
                        <button type="submit" name="submit" value="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Update</button>
                        <button type="reset" class="btn waves-effect waves-light btn-inverse" onclick="window.onbeforeunload = null; window.location.href = 'email-templates.php'"><i class="fas fa-ban"></i>Cancel</button>
                    </div>
                </div>
                <div class="col-md-12 row">
                    <div class="gst-spacer-10"></div>
                </div>
            </form>
        </div>
    </div>
</div>


<script language="javascript" type="text/javascript">
  tinyMCE.init({    
            selector: "#tinyMce",
            plugins: [
                "image","code","autoresize","media","link","imagetools"
            ],
            link_class_list: [
                {title: 'None', value: ''},
                {title: 'PDF', value: 'pdf'}
            ],
            default_link_target: "_blank",
            toolbar1: "insertfile undo | redo | styleselect | bold | italic | alignleft | aligncenter | alignright | alignjustify | bullist | numlist | outdent | indent | link | image | media | code",
            toolbar2: "",
            menubar : true,
            automatic_uploads: true,
            statusbar : false,
            convert_urls: false,
            content_css: 'editor-style.css',
            width: 800,
            height: 300,
            min_height: 300,
            images_upload_handler: function (blobInfo, success, failure) {
                var xhr, formData;

                xhr = new XMLHttpRequest();
                xhr.withCredentials = false;
                xhr.open('POST', 'inc/uploadImage.php');

                xhr.onload = function() {
                  var json;

                  if (xhr.status != 200) {
                    failure('HTTP Error: ' + xhr.status);
                    return;
                  }

                  json = JSON.parse(xhr.responseText);

                  if (!json || typeof json.location != 'string') {
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                  }

                  success(json.location);
                };

                formData = new FormData();
                formData.append('file', blobInfo.blob(), blobInfo.filename());

                xhr.send(formData);
              }
        });
</script>

<script type="text/javascript">
    //CKEDITOR.replace('template_body');
</script>
<!-- sweet alert js -->
<script type="text/javascript" src="files/bower_components/sweetalert/js/sweetalert.min.js"></script>
<!-- modalEffects js nifty modal window effects -->
<script type="text/javascript" src="files/assets/js/modalEffects.js"></script>
<?php include 'inc/notificiations.php'; ?>
<script type="text/javascript">
    $(document).ready(function() {
    $("form").submit(function() {
        $('#results').html('');
        // Getting the form ID
        var  formID = $(this).attr('id');
        var formDetails = $('#'+formID);
        //To not let the form of uploading attachments included in this
        if (formID != 'contact6') {
            $.ajax({
                type: "POST",
                url: 'inc/email-template-functions.php',
                data: formDetails.serialize(),
                success: function (data) {  
                    // Inserting html into the result div
                    $('#results').html(data);
                    //$("form")[0].reset();
                    formmodified = 0;
                },
                error: function(jqXHR, text, error){
                // Displaying if there are any errors
                    $('#result').html(error);           
                }
            });
            return false;
        }
    });

    // CKEDITOR FAILS TO SAVE FOR FIRST TIME WHILE UPDATE
//    $(function () {
//      setTimeout(function () {
//        function CK_jQ(instance) {
//          return function () {
//            CKEDITOR.instances[instance].updateElement();
//          };
//        }
//
//        $.each(CKEDITOR.instances, function (instance) {
//          CKEDITOR.instances[instance].on("keyup", CK_jQ(instance));
//          CKEDITOR.instances[instance].on("paste", CK_jQ(instance));
//          CKEDITOR.instances[instance].on("keypress", CK_jQ(instance));
//          CKEDITOR.instances[instance].on("blur", CK_jQ(instance));
//          CKEDITOR.instances[instance].on("change", CK_jQ(instance));
//        });
//      }, 0 /* 0 => To run after all */);
//    });
});
</script>
<?php include "footer.php"; ?>