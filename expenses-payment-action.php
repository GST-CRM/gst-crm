<?php include "session.php";

//To make several url options in the same page
$allowed = array('add', 'edit');
$action = mysqli_real_escape_string($db, $_GET['action']);
if ( ! isset($_GET['action'])) {header("location: expenses.php");die('Please go back to the main page.');}
if ( ! in_array($action, $allowed)) {header("location: expenses.php");die('Please go back to the main page.');}

if($action == "add") { $PageTitle = "Make a General Expense Payment"; } elseif($action == "edit") { $PageTitle = "Edit a General Expense Payment"; } else { $PageTitle = "General Expense Payment"; }
include "header.php";

$Expense_Num = mysqli_real_escape_string($db, $_GET["id"]);

//To collect the data of the above customer id, and show it in the fields below
$sql = "SELECT exp.*,gro.tourname,payee.Payee_ID,payee.Payee_Name FROM expenses exp LEFT JOIN groups gro ON exp.Group_ID=gro.tourid JOIN expenses_payee payee ON payee.Payee_ID=exp.Payee_ID WHERE Expense_Num=$Expense_Num";
$result = $db->query($sql);
$data = $result->fetch_assoc();


?>
  <!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />-->
<!--<script src="https://code.jquery.com/jquery-latest.js"></script>-->

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-block">
				<form name="contact1" action="inc/expenses-functions.php" method="POST" id="contact1" class="row">
                    <div class="form-default form-static-label col-sm-4">
                        <label class="float-label gst-label">Payee Reference</label>
                        <input type="text" name="ExpenseID" value="<?php echo $Expense_Num; ?>" hidden>
                        <input type="text" name="ExpenseAction" value="<?php echo $action; ?>" hidden>
                        <input type="text" name="Payee_ID" value="<?php echo $data['Payee_ID']; ?>" hidden>
				        <input class="form-control" type="text" name="PayeeReference" value="<?php echo $data['Payee_Name']; ?>" style="cursor:pointer;" readonly>
                    </div>
                    <div class="form-default form-static-label col-sm-4">
                        <label class="float-label gst-label">Group Reference</label>
						<?php if($data['Group_ID'] == 0) { ?>
							<input class="form-control" type="text" name="GroupReferenceID" value="0" hidden>
							<input class="form-control" type="text" name="GroupReferenceName" value="This expense isn't assigned to a group" readonly>
						<?php } else { ?>
                        <a href="groups-edit.php?id=<?php echo $data['Group_ID']; ?>&action=edit&tab=expenses">
							<input class="form-control" type="text" name="GroupReferenceID" value="<?php echo $data['Group_ID']; ?>" hidden>
							<input class="form-control" type="text" name="GroupReference" value="<?php echo $data['tourname']; ?>" style="cursor:pointer;" readonly>
						</a>
						<?php } ?>
                    </div>
                    <div class="form-default form-static-label col-sm-2 text-center">
					<?php if($data['Status']==0) { echo "<h1><i class='text-danger fas fa-exclamation-triangle'></i></h1>";} ?>
                    </div>
                    <div class="form-default form-static-label col-sm-2 text-center">
                        <h5>Amount</h5>
                        <h3 class="text-danger" id="ExpensesTotalText"><?php echo GUtils::formatMoney($data['Expense_Amount']); ?></h3>
                    </div>
                    <div class="form-group form-default form-static-label col-sm-2">
                        <label class="float-label gst-label">Payment Date</label>
                        <input type="date" name="Exp_Payment_Date" value="<?php echo GUtils::mysqlDate( Date('Y-m-d') ); ?>" class="form-control date-picker">
                    </div>
                    <div class="form-group form-default form-static-label col-sm-3">
            <label class="float-label gst-label">Payment Method</label>
            <select name="Exp_Payment_Method" id="idSelectPaymentMethod" class="form-control" required>
                <option value="">Select</option>
                <option <?php echo (($data['Exp_Payment_Method'] == 'Cash') ? 'selected' : '');?> value="Cash">Cash</option>
                <option <?php echo (($data['Exp_Payment_Method'] == 'Bank Transfer') ? 'selected' : '');?> value="Bank Transfer">Bank Transfer</option>
                <option <?php echo (($data['Exp_Payment_Method'] == 'Check') ? 'selected' : '');?> value="Check">Check</option>
                <option <?php echo (($data['Exp_Payment_Method'] == 'Credit Card') ? 'selected' : '');?> value="Credit Card">Credit Card</option>
                <option <?php echo (($data['Exp_Payment_Method'] == 'Online Payment') ? 'selected' : '');?> value="Online Payment">Online Payment</option>
            </select>
        </div>
        <div class="form-default form-static-label col-sm-3">
            <label class="float-label gst-label">From Account</label>
            <select name="From_Account_None" id="Accounts_None" class="form-control">
                <option value="0" disabled selected>Select Payment Method</option>
            </select>
            <select name="From_Account_Cash" id="Accounts_cash" class="form-control" style="display:none;">
                <option value="">Select</option>
                <?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name FROM checks_bank WHERE Bank_Type='cash' AND Bank_Status=1";
					$BanksData = GDb::fetchRowSet($GetBankAccountSQL);
					foreach ($BanksData AS $BankData) { ?>
                <option value="<?php echo $BankData['Bank_ID']; ?>"><?php echo $BankData['Bank_Name']; ?></option>
                <?php } ?>
            </select>
            <select name="From_Account_Bank" id="Accounts_bank" class="form-control" style="display:none;">
                <option value="" data-checknumber="N/A">Select</option>
                <?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name, Bank_Check_Number FROM checks_bank WHERE Bank_Type='bank' AND Bank_Status=1";
					$BanksData = GDb::fetchRowSet($GetBankAccountSQL);
					foreach ($BanksData AS $BankData) { ?>
                <option value="<?php echo $BankData['Bank_ID']; ?>" data-checknumber='<?php echo $BankData['Bank_Check_Number']; ?>'><?php echo $BankData['Bank_Name']; ?></option>
                <?php } ?>
            </select>
            <select name="From_Account_Card" id="Accounts_cards" class="form-control" style="display:none;">
                <option value="">Select</option>
                <?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name FROM checks_bank WHERE Bank_Type='cards' AND Bank_Status=1";
					$BanksData = GDb::fetchRowSet($GetBankAccountSQL);
					foreach ($BanksData AS $BankData) { ?>
                <option value="<?php echo $BankData['Bank_ID']; ?>"><?php echo $BankData['Bank_Name']; ?></option>
                <?php } ?>
            </select>
            <select name="From_Account_PayPal" id="Accounts_paypal" class="form-control" style="display:none;">
                <option value="">Select</option>
                <?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name FROM checks_bank WHERE Bank_Type='paypal' AND Bank_Status=1";
					$BanksData = GDb::fetchRowSet($GetBankAccountSQL);
					foreach ($BanksData AS $BankData) { ?>
                <option value="<?php echo $BankData['Bank_ID']; ?>"><?php echo $BankData['Bank_Name']; ?></option>
                <?php } ?>
            </select>
        </div>
        <div id="CheckNumberDiv" class="col-sm-4" style="display:none;">
            <label class="float-label gst-label">Check Number <small>(Below is the automatic number)</small></label>
            <input type="text" name="Check_Number" id='Check_Number' placeholder="Select a bank to get the check number" class="form-control">
        </div>
                    <div class="form-default form-static-label col-sm-2 text-center">
					<?php if($data['Status']==0) { echo "<h5 class='text-danger'>This record is</h5><h3 class='text-danger'>Voided</h3>";} ?>
                    </div>
                    <div class="form-default form-static-label col-sm-2 text-center">
					<?php /*if($data['Status']==0) { ?>
					<a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick = 'deleteConfirm( "expenses.php?action=unvoid&exp_id=<?php echo $data["Expense_Num"]; ?>", "Unvoid")' role="button">Unvoid</a>
					<?php } else { ?>
					<a class="btn btn-primary btn-sm" href="javascript:void(0);" onclick = 'deleteConfirm( "expenses.php?action=void&exp_id=<?php echo $data["Expense_Num"]; ?>", "Void")' role="button">Void</a>
					<?php }*/ ?>
                    </div>
                    <div class="col-sm-12"><br /></div>
                    <div class="col-sm-12">
	<table class="table table-hover table-striped table-bordered nowrap" id="item_table">
		<thead>
			<tr>
				<th width="30" class="text-center">#</th>
				<th width="20%">Expense Category</th>
				<th>Expense Description</th>
				<th width="15%">Amount</th>
			</tr>
		</thead>
		<tbody>
		<?php
		$sql = "SELECT el.Expense_Line, el.Expense_Line_category, gag.GL_Group_Name, el.Expense_Line_CoA, coa.GL_Account_Name, el.Expense_Line_Amount
		FROM expenses_line el
		JOIN gl_account_groups gag 
		ON gag.GL_Group_Num=el.Expense_Line_Category
		LEFT JOIN gl_chart_of_accounts coa
		ON coa.Account_ID=el.Expense_Line_CoA
		WHERE Expense_Num=$Expense_Num";
		$result = $db->query($sql);
		$ExpenseLine = 1;
		if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
			<tr>
				<td class="text-center p-2"><?php echo $ExpenseLine; ?></td>
				<td class="p-2"><?php echo $row['GL_Group_Name']; ?></td>
				<td class="p-2"><?php echo $row['GL_Account_Name']; ?></td>
				<td class="p-2"><?php echo GUtils::formatMoney($row['Expense_Line_Amount']); ?></td>
			</tr>

		<?php	
		$ExpenseTotal += $row['Expense_Line_Amount'];
		$ExpenseLine = ++$ExpenseLine;
		}} ?>
		</tbody>
		<tfoot>
			<tr style="background:#f9f9f9;border-top: double 3px #999;">
				<td colspan="3" class="text-right pt-3 p-2"><b>Total Expense Amount</b></td>
				<td class="p-2"><div class="dollar"><input type="text" id="ExpenseTotal" class="form-control ExpenseTotal" value="<?php echo $ExpenseTotal; ?>" readonly /></div></td>
			</tr>
			<tr style="background:#f2f2f2;">
				<td colspan="3" class="text-right pt-3 p-2"><b>New Payment Amount</b></td>
				<td class="p-2"><div class="dollar"><input type="text" id="PaymentAmount" name="Exp_Payment_Amount" onblur="findTotal()" class="form-control exp_amount" value="0.00" /></div></td>
			</tr>
		</tfoot>
	</table>
					</div>
					<div class="form-group form-default form-static-label col-md-12">
                        <label class="float-label">Payment Notes</label>
                        <textarea name="Exp_Payment_Comments" class="form-control"><?php echo $data["Exp_Payment_Comments"]; ?></textarea>
                    </div>
                    <div class="col-sm-12"><br /><br />
						<button type="submit" name="addcontact" value="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Add Expense Payment</button>
                        <button type="reset" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Clear</button>
					</div>
				</form>
			</div>
        </div>
    </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
    $('#idSelectPaymentMethod').on('change', function() {
      if ( this.value == 'Cash') {
        $("#Accounts_cash").show('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").hide('slow');
        $("#Accounts_bank").hide('slow');
        $("#Accounts_paypal").hide('slow');
        $("#CheckNumberDiv").hide('slow');
      } else if ( this.value == 'Bank Transfer') {
        $("#Accounts_cash").hide('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").hide('slow');
        $("#Accounts_bank").show('slow');
        $("#Accounts_paypal").hide('slow');
        $("#CheckNumberDiv").hide('slow');
      } else if ( this.value == 'Check') {
        $("#Accounts_cash").hide('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").hide('slow');
        $("#Accounts_bank").show('slow');
        $("#Accounts_paypal").hide('slow');
        $("#CheckNumberDiv").show('slow');
      } else if ( this.value == 'Credit Card') {
        $("#Accounts_cash").hide('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").show('slow');
        $("#Accounts_bank").hide('slow');
        $("#Accounts_paypal").hide('slow');
        $("#CheckNumberDiv").hide('slow');
      } else if ( this.value == 'Online Payment') {
        $("#Accounts_cash").hide('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").hide('slow');
        $("#Accounts_bank").hide('slow');
        $("#Accounts_paypal").show('slow');
        $("#CheckNumberDiv").hide('slow');
      }
    });
});


var selection = document.getElementById("Accounts_bank");
selection.onchange = function(event){
  var checknumber = event.target.options[event.target.selectedIndex].dataset.checknumber;
  document.getElementById("Check_Number").value = checknumber;
};
    
    
//To make the numbers as a currency with two decimels
const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD',
  minimumFractionDigits: 2
})

//To generate the total amount for the expense in the right top corner
function findTotal(){
    var expensetotal = document.getElementById('ExpenseTotal').value;
    var paymentamount = document.getElementById('PaymentAmount').value;
	var calculation = expensetotal - paymentamount;
	UpdatedTotal = formatter.format(calculation);
    document.getElementById('ExpensesTotalText').innerHTML = UpdatedTotal;
}

//To connect to expenses-functions.php and the database
$(document).ready(function() {
	$("form").submit(function() {
		// Getting the form ID
		var  formID = $(this).attr('id');
		var formDetails = $('#'+formID);
		$.ajax({
			type: "POST",
			url: 'inc/expenses-functions.php',
			data: formDetails.serialize(),
			success: function (data) {	
				// Inserting html into the result div
				$('#results').html(data);
				$("form")[0].reset();
			},
			error: function(jqXHR, text, error){
            // Displaying if there are any errors
            	$('#result').html(error);           
        }
    });
		return false;
	});
});
</script>
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>