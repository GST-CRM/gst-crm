<?php
include "session.php";
$PageTitle = "Edit Job";
include "header.php";

$allowed = array('edit');
$action = $_GET['action'];
if ( ! isset($_GET['action'])) {header("location: cron-schedular.php");die('Please go back to the main page.');}
$action = $_GET['action'];
if ( ! in_array($action, $allowed)) {header("location: cron-schedular.php");die('Please go back to the main page.');}


$Cron_ID = $_GET["id"];
$CronJobSQL = "SELECT * FROM cron_schedular WHERE Cron_ID=$Cron_ID;";
$CronJob = GDb::fetchRow($CronJobSQL);


$Cron_Loop = $CronJob["Cron_Loop"];
$Cron_Name = $CronJob["Cron_Name"];
$Email_Template_ID = $CronJob["Email_Template_ID"];
$Cron_Period = $CronJob["Cron_Period"];
$Cron_Days = $CronJob["Cron_Days"];
$Cron_Schedule_Date = $CronJob["Cron_Schedule_Date"];

if($Cron_Period != "After") {$Cron_Period = "Before";}
if($Cron_Loop != 99) { $Cron_Period = NULL; $Cron_Days = 0; }
if($Cron_Loop != 1) { $Cron_Schedule_Date = "0000-00-00"; }

?>
<div class="row">
	<div class="col-md-12">
		<div class="card">
			<form name="templateAddForm" action="inc/cron-schedular-functions.php" method="POST" id="contact1" class="card-block row">
				<input name="Cron_ID" value="<?php echo $Cron_ID; ?>" type="text" hidden>
				<div class="form-group form-default form-static-label col-sm-2">
					<label class="float-label gst-label">Job Loop</label>
					<select name="Cron_Loop" class="form-control" required onChange="CronLoop(this.value);">
						<option value="0" selected disabled>Select</option>
						<option value="99" <?php if($Cron_Loop == 99) { echo "selected"; } ?>>Before/After</option>
						<option value="1" <?php if($Cron_Loop == 1) { echo "selected"; } ?>>Specific Date</option>
						<option value="2" <?php if($Cron_Loop == 2) { echo "selected"; } ?>>Hourly</option>
						<option value="3" <?php if($Cron_Loop == 3) { echo "selected"; } ?>>Daily</option>
						<option value="4" <?php if($Cron_Loop == 4) { echo "selected"; } ?>>Weekly</option>
						<option value="5" <?php if($Cron_Loop == 5) { echo "selected"; } ?>>Monthly</option>
					</select>
				</div>
				<div class="form-group form-default form-static-label col-sm-4">
					<label class="float-label gst-label">Job Name</label>
					<input type="text" name="Cron_Name" id="Cron_Name" class="form-control" value="<?php echo $Cron_Name; ?>" style="padding-left: 10px" required>
				</div>
				<div class="form-group form-default form-static-label col-sm-6">
					<label class="float-label gst-label">Template Name</label>
					<select name="Email_Template_ID" class="form-control" required>
						<?php
						$templateTypeResult = mysqli_query($db,"SELECT etn.template_id,etn.template_name FROM email_templates_new etn WHERE etn.status=1");
						while($ResultData = mysqli_fetch_array($templateTypeResult)) {
							if($Email_Template_ID == $ResultData['template_id'] ) { $Selected = "selected"; } else { $Selected = ""; }
							echo "<option value='".$ResultData['template_id']."' $Selected>".$ResultData['template_name']."</option>";
						} ?>
					</select>
				</div>
				<div id="cron_scheduled_date"  <?php if($Cron_Loop != 1) { echo "style='display:none;'"; } ?> class="form-group form-default form-static-label col-sm-2">
					<label class="float-label gst-label">Add Date</label>
					<input type="date" name="Cron_Schedule_Date" class="form-control" value="<?php echo $Cron_Schedule_Date; ?>" <?php if($Cron_Loop != 1) { echo "disabled"; } ?>>
				</div>
				<div id="Cron_Days" <?php if($Cron_Loop != 99) { echo "style='display:none;'"; } ?> class="form-group form-default form-static-label col-sm-2">
					<label class="float-label gst-label">Number of Days</label>
					<input type="number" name="Cron_Days" class="form-control" value="<?php echo $Cron_Days; ?>">
				</div>
				<div id="Cron_AfterBefore" <?php if($Cron_Loop != 99) { echo "style='display:none;'"; } ?> class="col-sm-2">
				<style>.can-toggle label .can-toggle__switch {background: #70c767 !important;font-weight:bold;}</style>
					<label class="float-label gst-label">Before/After</label>
					<div class="can-toggle">
						<input id="Cron_Period" name="Cron_Period" value="After" type="checkbox" <?php if($Cron_Period == 1) { echo "checked"; } ?>>
						<label for="Cron_Period">
						<div class="can-toggle__switch" data-checked="After" data-unchecked="Before"></div>
						</label>
					</div>
				</div>
				<div class="col-md-12 mb-2 mt-3">
					<button type="submit" name="submit" value="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal"><i class="far fa-check-circle"></i>Update</button>
					<button type="reset" class="btn waves-effect waves-light btn-inverse" onclick="window.onbeforeunload = null; window.location.href = 'cron-schedular.php'"><i class="fas fa-ban"></i>Cancel</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
function CronLoop(value) {
	if (value == 1) {
		$("#cron_scheduled_date").show();
	} else {
		$("#cron_scheduled_date").hide();
	}
	if (value == 99) {
		$("#Cron_AfterBefore").show();
		$("#Cron_Days").show();
	} else {
		$("#Cron_AfterBefore").hide();
		$("#Cron_Days").hide();
	}
}


    $(document).ready(function() {
    $("form").submit(function() {
        $('#results').html('');
        // Getting the form ID
        var  formID = $(this).attr('id');
        var formDetails = $('#'+formID);
        //To not let the form of uploading attachments included in this
        if (formID != 'contact6') {
            $.ajax({
                type: "POST",
                url: 'inc/cron-schedular-functions.php',
                data: formDetails.serialize(),
                success: function (data) {  
                    // Inserting html into the result div
                    $('#results').html(data);
                    //$("form")[0].reset();
                    formmodified = 0;
                },
                error: function(jqXHR, text, error){
                // Displaying if there are any errors
                    $('#result').html(error);           
                }
            });
            return false;
        }
    });
});
</script>
<!-- sweet alert js -->
<script type="text/javascript" src="files/bower_components/sweetalert/js/sweetalert.min.js"></script>
<!-- modalEffects js nifty modal window effects -->
<script type="text/javascript" src="files/assets/js/modalEffects.js"></script>
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>