<?php
require_once( __DIR__ .  '/../files/pdf/tcpdf.php');

class MYPDF2 extends TCPDF {

    //Page header
    public function Header() {
        // get the current page break margin
        $bMargin         = $this->getBreakMargin();
        // get current auto-page-break mode
        $auto_page_break = $this->AutoPageBreak;
        // disable auto-page-break
        $this->SetAutoPageBreak(false, 0);
        // set bacground image
        $this->SetAutoPageBreak($auto_page_break, 25);
        $this->setPageMark();
    }

}
