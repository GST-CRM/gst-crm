<?php
include "session.php";
$PageTitle = "CRM Version Changelog";
include "header.php";
?>
<!-- [ page content ] start -->
	<div class="row">
		<div class="col-sm-1">
		</div>
		<div class="col-sm-10">
			<div class="card exp-build-card">
				<div class="card-header">
				<h5>GST CRM Software Changelog Details</h5>
				</div>
				<div class="card-block">
<pre style="word-wrap: break-word; white-space: pre-wrap;">
-----------------------------------------------------------------------------------------
Version 1.7.6 - September 2nd, 2020
-----------------------------------------------------------------------------------------
- IMPROVEMENT: Negative balance show in red - Pax listing page
- IMPROVEMENT: Removed the refunds column from the Financial Reports and added it to be deducted from the Amount received
- IMPROVEMENT: Showing the cancelled customers in red in the Financial Reports
- IMPROVEMENT: Added hidden columns for the financial reports that can be seen in the excel sheet for (Airline Cost, Land Cost, Agent Fees, Extra Upgrades Cost)
- IMPROVEMENT: Showing the Supplier Balance when adding or editing a supplier payment
- IMPROVEMENT: Separate tab for additional customers in customer access view
- IMPROVEMENT: Customer login - attachment editable by unloaded customer only
- IMPROVEMENT: Customer login - Hide sub tabs if theres only one customer
- IMPROVEMENT: Customer login - Itenary reading from tours
- IMPROVEMENT: The sales order now can be created manually, and convereted to invoice (Commit #4408c55)
- IMPROVEMENT: Added a field for the Denomination for contacts
- IMPROVEMENT: Switch additional to primary on primary cancellation
- FIXED: Removed the line through on cancelled customers in financial reports
- FIXED: Make the text red in footer in financial reports
- FIXED: Make the cancelled customer columns to be in red only for the name and the status
- FIXED: Show the additional options for primary customers in pax listing
- FIXED: Refund not proper if there is refund issued when he is active
- FIXED: Customer login - Incorrect ticket printing url
- FIXED: The agent commission won't be calculated in the financial reports if the customer is complimentary
- FIXED: The credit notes in the financial report is only connected to the current group
- FIXED: Hide refund amount section in cancellation box for addtional to primary switching
- FIXED: Sales invoice issue
- ADDED: Action button for "forgot password mail" to users
- ADDED: Switch primary while cancel
- ADDED: Customer login - Attachment feature
- ADDED: Customer login - print ticket
- ADDED: Customer login - Flight info & land info


-----------------------------------------------------------------------------------------
Version 1.7.5 - August 9th, 2020
-----------------------------------------------------------------------------------------
- IMPROVEMENT: Added the page X records for Agents List, Contacts List, Categories List, and Leaders List
- IMPROVEMENT: Removed the PLUS sign in all the phones fields for adding a contact
- IMPROVEMENT: Made the memo field in the checks edit & add pages limited to 50 characters only
- IMPROVEMENT: Organized the transactions table for each financial account, and added links for the several types of records (customers, agents, expenses, suppliers, ...)
- IMPROVEMENT: Show the description/comments for all the transactions in the Financial Accounts transactions table
- IMPROVEMENT: A remove button to delete any deposit or withdrawal record only in the financial accounts
- IMPROVEMENT: Customer Dashbaord tabs switched to URLs
- IMPROVEMENT: Enable cancelling and restore customer membership multiple times
- IMPROVEMENT: Show Issure refund button when balance is negative or zero
- IMPROVEMENT: Added the pax counter for each group in the monthly groups financial report
- IMPROVEMENT: Print invoice button for customers while their are logged in
- IMPROVEMENT: Added the agents fees to the group financial reports
- IMPROVEMENT: The name of the group in the Finanical Reports is now fully mentioned and clickable
- FIXED: Issues in the non-cancelled refund got fixed
- FIXED: The refund duplicated amount issue got fixed
- FIXED: When opening a current customer payment, you will be able to see the payment method and deposit to fields correctly
- FIXED: When edit a payment received from a customer it wasn't redirecting to the customer because the ID was missing
- FIXED: Customer billing status issues
- FIXED: Not showing payment amount in customer login dashboard
- FIXED: Incorrect amount in "Customer billing status" of canceled customers
- FIXED: Non canceled refund should not add to canceled refund balance
- FIXED: Issue taking cancellation charge from another group of same customer
- FIXED: Cancel primary customer
- FIXED: The names of the staff, agents, leaders in the group edit page are now full names and corrected
- FIXED: The name of the Payee is correctly displayed in the check print view
- FIXED: Attachments upload box was having issue of not being able to upload
- ADDED: The Group's Finanicial Reports Feature 
- ADDED: Supplier Balance Account (Over Payments) Feature 


-----------------------------------------------------------------------------------------
Version 1.7.4 - July 11th, 2020
-----------------------------------------------------------------------------------------
- HOTFIX: Error window when searching in Refund Receipts page
- IMPROVEMENT: Sped up the loading speed for the customers page
- IMPROVEMENT: Updated the Financial Accounts listing balances, and also the Account Overview Balances right box
- IMPROVEMENT: All the attachments upload fields have been improved and have an updated UI design
- FIXED: The invoices estimated balance is updated, but still not accurate, we should remove the refunds and some accounting
- FIXED: Agent permission issue
- FIXED: Remove contact edit link for non-admin users from confirmation listing
- FIXED: The link for the bank name wasn't working correctly
- FIXED: Pax lising buttons not showing
- FIXED: When adding a new check while creating a refund for a customer, the memo will be the cancelation reason, and the refund's comments will be the notes on the check
- FIXED: When adding a check from the refund receipt creation page it will also add the amount in words for the check


-----------------------------------------------------------------------------------------
Version 1.7.3 - July 6th, 2020
-----------------------------------------------------------------------------------------
- IMPROVEMENT: Modify email content to include forgot password when agent email address is change
- IMPROVEMENT: Agent login after email address change include forgot password link
- IMPROVEMENT: Remove max cancellation fee validation
- IMPROVEMENT: Cleaned up unnecessary tables and columns on the Database
- IMPROVEMENT: The users list page now has user type filter, and the layout improved to be similar like other pages
- IMPROVEMENT: Credit and debit balances screen for the banks uploaded and under progress
- IMPROVEMENT: Ability to see the supplier's company name when using the contacts dropdown in issue new check
- IMPROVEMENT: When cashing a check, it will record the date of check cashed automatically
- IMPROVEMENT: Show the refunds records in sales & payment tab for contact edit page
- IMPROVEMENT: Show all the records for the supplier deposit report as a default before filtering
- IMPROVEMENT: The favorite/bookmark navbar is showing the remove button with a colored star icon
- IMPROVEMENT: When adding a new account, it will give you tabs to choose the correct account type needed
- IMPROVEMENT: Adding a check is only allowed for account type "Banks"
- IMPROVEMENT: Editing a check is now only allowing the bank account types to be used for checks
- IMPROVEMENT: Added the logout button in the user corner menu, and hid the two icons for notifications for now only
- IMPROVEMENT: Editing the financial account page is enhanced now
- IMPROVEMENT: Enhanced the PHP code standards to be organized and easy to read for refund_receipt_add.php
- IMPROVEMENT: Cancellation charge of the additional travellers applied to the primary customer
- IMPROVEMENT: No redirect after removing a favorite page and stay in the same page
- IMPROVEMENT: Show "Issue a refund" button when partial refund applied
- IMPROVEMENT: Include label prefix "Refund" in receipt print
- IMPROVEMENT: The Airline & Land Deposits report is now hiding by default the records that has an amount of ZERO when deducting the returned amount from the deposit amount
- IMPROVEMENT: Removed the check cashed feature from the refund receipts list as we are now depending on the Check Feature itself for cashing
- IMPROVEMENT: Ability to record the finanicial account when adding a payment for supplier, agent, refund, customer and general expense
- IMPROVEMENT: The check will be created automatically if the payment for any user type was "check" as its method
- IMPROVEMENT: Increase the size of the font for the favorites menu
- IMPROVEMENT: Show cancellation option for primary customers
- FIXED: Fixed the attachments in the emails that contains the reports
- FIXED: the footer table for the subproducts tab in the group edit page wasn't balanced and accurate
- FIXED: Not being able to add a new general expense
- FIXED: Ability to select any contact from the database when issuing a new check
- FIXED: Removed the discount option in receive a payment screen
- FIXED: Refund date not listing in refunnd listing, balance not showing in pax listing
- FIXED: The customer deposits report was having issue in the footer table
- FIXED: Properly mark addtional travellers invoice status
- FIXED: Remove the contact/customer links in any screen for the agent view
- FIXED: "Refund created" status added with links
- FIXED: When adding an expense check, the payee name is shown incorrectly
- ADDED: Customer Login Screens uploaded and under progress
- ADDED: Ability to divide the customer refund to several payments
- ADDED: Suppliers Airline & Land Products Deposit Report
- ADDED: Customer login dashboard
- ADDED: Favorite Pages Feature
- ADDED: Financial Accounts feature
- ADDED: Deposits Feature for the Financial Accounts


-----------------------------------------------------------------------------------------
Version 1.7.2 - June 4th, 2020
-----------------------------------------------------------------------------------------
- HOTFIX: Issue with the email template submission form when there is an apostrophe
- IMPROVEMENT: Improved the print file for the checks to be accurate
- IMPROVEMENT: When a bank account is added, the user will be redirected to the bank accounts list page
- IMPROVEMENT: Security update change password hash algorith to sha256
- IMPROVEMENT: Replace every usage with sha256
- IMPROVEMENT: Added the function of "Check Cashed" for checks list
- IMPROVEMENT: Added the function of how many times this check has been printed
- FIXED: The logo in the print pdf preview has been fixed when using the batch action
- FIXED: Updating the company details was showing error or not updating


-----------------------------------------------------------------------------------------
Version 1.7.1 - May 30th, 2020
-----------------------------------------------------------------------------------------
- IMPROVEMENT: The refunds receipts list page has now more filters and the table got enhanced
- IMPROVEMENT: Change restore cancellation button style to "undo"
- IMPROVEMENT: Enhanced the view of the summary boxes in the Supplier Bills page
- IMPROVEMENT: "Undo cancellation" feature issue fixes and improvements
- IMPROVEMENT: Order the payments in the print view for the invoice from the newest to the oldest
- IMPROVEMENT: Updated the date and time format for the logs list page
- IMPROVEMENT: When adding a group note it will be tracked and captured in the logs list page
- IMPROVEMENT: Showing the date of the refund receipt and order it by the date
- IMPROVEMENT: Made the customer name clickable in the logs page
- IMPROVEMENT: Made the payments order in the print view to be from the oldest to the newest
- IMPROVEMENT: Update sales-order table with Sales_order_stop
- IMPROVEMENT: Organizing the filter section more in the refund receipt page
- IMPROVEMENT: Secured the GET variables in the add new supplier bill page
- IMPROVEMENT: If there is more than one person in the invoice, count them and update the calculation of the 2nd deposit payment in the invoice PDF
- IMPROVEMENT: Ability to clear the records from the advanced filter when searching in the deposit report page
- IMPROVEMENT: Added the Date filter in the supplier bills page
- IMPROVEMENT: The logs archive now has pagination to be able to view more logs records
- IMPROVEMENT: Disable invoice due date in sales and payment page
- IMPROVEMENT: Added the excel export button, customized file for the sales orders list
- IMPROVEMENT: Products list page enhanced
- IMPROVEMENT: The expenses payees page has been enhanced
- IMPROVEMENT: General expenses general variables secured
- IMPROVEMENT: Updated the labels of the add new product page
- IMPROVEMENT: Added the option of No group selection in the add new general expense page
- IMPROVEMENT: Some fixes on the undefined varialbles and indexes for the Sales Orders
- IMPROVEMENT: Removed all the extra-pages folder and organized the sub folders
- IMPROVEMENT: Uses CDN for tinymce instead of local files and removed the local files
- IMPROVEMENT: Showing the option to cashed or not cashed checks only when the status is “refund issued”
- IMPROVEMENT: Updated the GST logo photo size for the footer in emails
- IMPROVEMENT: When filtering cancelled recent customers, it will show the cancellation date in the recent registration report page
- IMPROVEMENT: MYPDF2 library upgraded to a class
- FIXED: Cancellation restore without restore joint membership
- FIXED: In the customer billing right box, the charges are added to the owed balance
- FIXED: Cancelled participant issue, Listing non canceled customers
- FIXED: Cancellation amount not currect
- FIXED: When removing a customer that has several groups not to get disabled
- FIXED: In Pax, List members based on account status ( instead of old tourMembership status)
- FIXED: Action button not properly showing
- FIXED: Include surcharge in the subtotal for refund due
- FIXED: Update modified date on reverse cancellation
- FIXED: Removed the unix timestamp 8 hours difference from the invoices page listing
- FIXED: Hide invoiced customer button in contact edit
- FIXED: Removed the notice warnings for empty values in the contacts list page
- FIXED: Added the due date to the sales order function when creating the invoice
- FIXED: Update login email address when login status changes
- FIXED: The sales order page to be shown in the menu
- FIXED: After payment redirect issue
- FIXED: Payment save redirect issue when save using invoice id
- FIXED: Issue with the invoice amount when creating a contact and making it a customer on the same time in group edit page
- FIXED: Added some filters when the general system search isn't used or filled for contacts
- ADDED: Custom sales orders interface pages and functionalities and Screens
- ADDED: Active / Inactive option for sending group email
- ADDED: Ability to trigger "cashed" flag in the refund receipts page and with filters
- ADDED: Sales Receipts Page and connected it to the accounting page
- ADDED: Send login enabled mail for agents (when activated from deactivated state)
- ADDED: New email trigger when email changes
- ADDED: TinyMCE Editor instead of CK Editor with image upload feature for email template section
- ADDED: Customer Checks Feature
- ADDED: Add to favorites dropdown


-----------------------------------------------------------------------------------------
Version 1.7.0 - April 9th, 2020
-----------------------------------------------------------------------------------------
- HOTFIX: Production issue - Land upgrade product doesn't reflect on the invoice correctly
- IMPROVEMENT: Showing the time span for the last time the invoice have been updated on Sales Invoices List Page
- IMPROVEMENT: Added the Group Reference in the Customer Payments List Page and Ordered it by Payment Newest Date
- IMPROVEMENT: Added links for the invoice column, and customer name column, added group reference column, and ordered the receipts by the newest to the oldest
- IMPROVEMENT: New filter option in refund receipt list "No Refund Due" depending on the Zero amount receipts
- IMPROVEMENT: Link added to refund create page from the Refunds Receipts list page
- IMPROVEMENT: Added the group name in the customers list page
- IMPROVEMENT: Refund details under contact sales & payment tab in the customer edit page
- IMPROVEMENT: Listing the payments records in the invoice tab in the customer page from the newest to the oldest
- IMPROVEMENT: Custom cancellation mesage on print screen
- IMPROVEMENT: Redirect back to refund receipt listing after adding a new refund receipt
- IMPROVEMENT: Conditionaly back to refund receipt list page or the customer edit page after issuing the refund
- IMPROVEMENT: Added advanced filters for the Deposits report page
- IMPROVEMENT: When the user selects "Refund" option as a payment method type, the amount of the money added will be in minus directly
- FIXED: Supplier Payable Report
- FIXED: The time of the changelog is now reflecting minus 8 hours difference to match Texas timing
- FIXED: Search field for contacts is working directly now, and when pressing on TAB button selects it directly
- FIXED: General Search field on the top is now showing the actual result in the search field
- FIXED: Invoice date not updates, Balance due not showing, Quatity into label from input text
- FIXED: Removed the comma from the pdf invoice file
- FIXED: Refund "Deleted Refund" filter not properly working
- FIXED: Refund created must not list 0 amount items
- ADDED: Custom refund amount
- ADDED: Refund Receipt has the ability to upload attachments
- ADDED: Deposits Report


-----------------------------------------------------------------------------------------
Version 1.6.9 - March 26th, 2020
-----------------------------------------------------------------------------------------
- IMPROVEMENT: Pay Bill link change to Supplier payments
- IMPROVEMENT: Void / Active expenses listing filter in Expense-CoA
- IMPROVEMENT: Showing "Joint" label in the Pax tab for all the additional travelers
- IMPROVEMENT: Updated the UI for Account Receivable Report Page
- IMPROVEMENT: The supplier bill payment is showing a popup confirmation when attempting to delete a line
- IMPROVEMENT: Added Group_ID column and improved the Supplier Bill Edit and Add functions
- IMPROVEMENT: Change title suppliers to Suppliers & Expenses in Accounting page
- IMPROVEMENT: Showing only the groups that are listed for the supplier in the Supplier Bill Add New Page
- IMPROVEMENT: Customer's tab in the contact edit page UI has been improved
- IMPROVEMENT: Customer selection changed from customer name to "Customer name - Tour name" in Create a New Refund Receipt page
- IMPROVEMENT: Update option to update the canceled invoice message in customer edit page
- IMPROVEMENT: Remove validation for fields due_date, address etc, when updating message
- IMPROVEMENT: Updated the label and UI of apply a credit to invoice in customer edit page
- IMPROVEMENT: Custom email option in the Tour Leader Report in group edit page
- IMPROVEMENT: Check the name of the new payee if duplicate before submitting it to the system
- IMPROVEMENT: Customer sales & payment data listing in the models and the framework
- IMPROVEMENT: Formatting contact edit sales & payments invoices listing
- IMPROVEMENT: Improved the General Expense Screens and General Expense Payments Screens
- FIXED: Moved all the dollar signs to be on the left for the Account Receivable Report Excel Export
- FIXED: The default time zone for the system has been updated to "America/Chicago"
- FIXED: Invoice mail not sent to recipients
- FIXED: Refund receipt add should list only refund created records
- FIXED: Badgename, passport, expirydate moved to contact table
- FIXED: Select All button is now working correctly for Confirmation List in group edit page
- ADDED: Tickets Confirmation Report for Groups and customers
- ADDED: Supplier Payable report added to the Reports Menu
- ADDED: Ability to choose if the Airline Credit to be applied and discounted or not in the customer edit page
- ADDED: Cron Jobs Feature - Phase #1 (Under Progress)
- ADDED: Tour leader report email option
- ADDED: General Expense Payments Feature


-----------------------------------------------------------------------------------------
Version 1.6.8 - March 4th, 2020
-----------------------------------------------------------------------------------------
- IMPROVEMENT: Tour leader default group listing set to all
- IMPROVEMENT: Supplier payment ui changes. Csv, Excel button added.
- IMPROVEMENT: Added the airports fields in the Supplier Flights Reports for the group reports tab
- IMPROVEMENT: Organized the code inside the contact edit page for the agent, supplier and group leader commands
- IMPROVEMENT: The button of the supplier on the sidebar to have disable and enable functionality
- IMPROVEMENT: Search by invoice feature, Load values on selection change
- IMPROVEMENT: Replace account receivable report with new query
- IMPROVEMENT: The ability to revert or choose normal row line trigger in the airline group edit page and the customer edit page
- IMPROVEMENT: Added the fields of "Meals Request" & "Allergies Request" to the Supplier Flight Report
- FIXED: Group category mail set to customers
- FIXED: The enable and disable agent button is now working correctly
- FIXED: Exclude action column from export data
- FIXED: Limit customers with invoice in credit note page
- FIXED: When updating the basic info for the group, the page will be refreshed


-----------------------------------------------------------------------------------------
Version 1.6.7 - March 2nd, 2020
-----------------------------------------------------------------------------------------
- IMPROVEMENT: Removed customer link in pax listing for group leaders and others
- IMPROVEMENT: Restrict selection of passport expiry and birth day date field
- IMPROVEMENT: Left side search box for category listing
- IMPROVEMENT: Validation for passport and birthday
- IMPROVEMENT: Categories listing without export buttons
- IMPROVEMENT: The agent's payment tab got improved for better user experience
- IMPROVEMENT: Account Receivable Reports to be exporting the footer also
- IMPROVEMENT: Add option to export to excel: Account Receivable report
- IMPROVEMENT: Updated the batch dropdown, search field and the labels updated in the Customer Payments page
- IMPROVEMENT: Better filters and labels got updated for the Supplier Payments page
- IMPROVEMENT: Updated the labels and the order of the filter and the interface for the Supplier Bills Page
- IMPROVEMENT: Extension & Special Request fields added to the Flight Info Supplier Report
- IMPROVEMENT: Flight Info Supplier Report order changed
- IMPROVEMENT: Updated the labels of the Sales Orders Page
- IMPROVEMENT: Categorizing the reports inside the group page per contact types
- IMPROVEMENT: Adding the transfer and extension fields in the manifest report
- IMPROVEMENT: The filter and the search box for Agents, Suppliers, Bills and Leaders is enhanced
- FIXED: Batch print not working
- FIXED: Add Credit Note Url
- FIXED: The filter for No Passport wasn't getting all the records
- FIXED: Updated the labels and fixed the datatable JS code for better performance
- FIXED: Group title change for tour leader & agents
- FIXED: Length check fo user name when used with group name caption
- FIXED: The session error for A/R report
- FIXED: Invoice amount as string search fix
- FIXED: Adding a new category or a current category will be saved correctly
- FIXED: Validating the email field before submitting a new contact entry
- FIXED: The accounting page >> Receive a Payment redirects to Add New Payment page
- FIXED: The flight airline information are now up to 10 rows and saving correctly
- ADDED: Advanced Flight Information Section Feature
- ADDED: Flights Feature >> Supplier Report under Reports tab in the Group's edit page
- ADDED: Flights Feature >> Implemented the feature for the customer as well
- ADDED: Category Mail Feature
- ADDED: Adding the online group link in the group edit page


-----------------------------------------------------------------------------------------
Version 1.6.6 - February 15th, 2020
-----------------------------------------------------------------------------------------
- IMPROVEMENT: Pax notification for "Passport Pending" interface updates
- IMPROVEMENT: Manifest report email & rooming list report email
- IMPROVEMENT: Enabled manifest email report to right person
- IMPROVEMENT: Rooming Report with contact email address
- IMPROVEMENT: Ability to delete a payment or a credit note directly from the invoice tab inside the customer edit page
- IMPROVEMENT: When adding a payment or a credit note to a customer, you will be redirected to the customer page again and added the ability to choose different redirections in the save button
- IMPROVEMENT: The contact's labels, the email field and the category dropdown are improved
- IMPROVEMENT: The group leader labels have been updated
- FIXED: UI Fixes to disable editing invoice products
- FIXED: Name correction of agent
- FIXED: Conflict fix between agent and tour leaders
- FIXED: Redirection not proper after manifest email report
- FIXED: Canceled contact edit not loading
- FIXED: Consider airline supplier in manifest/roomiglist email
- FIXED: When creating a customer from the contact edit page, it gets all the invoice and sales order details without missing any field
- FIXED: Issue with the excel exports for the Manifest report
- FIXED: Improved the group purchase order query when listing or editing a customer for a group
- FIXED: When a group doesn't have Pax, the search box will show an error window
- FIXED: When disabling an agent now, everything will be working correctly
- FIXED: The type & payment terms fields in the supplier tab is now showing N/A if nothing is chosen
- ADDED: Add passport pending flag in pax listing
- ADDED: Tour leader login
- ADDED: Meals & Allergies requests section has been added to the customer edit page and Rooming & Manifest reports
- ADDED: Dropbox Tickets shared folder field has been added to the Airline tab in group edit page
- ADDED: Added Date option for Schedular


-----------------------------------------------------------------------------------------
Version 1.6.5 - January 20th, 2020
-----------------------------------------------------------------------------------------
- IMPROVEMENT: Add notification for passport expiry in pax
- IMPROVEMENT: Speeding the Invoice Page - Listing the selected products only when the invoice is in Edit Mode
- IMPROVEMENT: All customer payments are now recording the Group_ID on the database records
- IMPROVEMENT: Updated the view of the Sales tab for each customer for better "Receive Payment" button for each invoice
- IMPROVEMENT: All products added to the customer details are now updating the invoice, its total amount and fixing all the records correctly
- IMPROVEMENT: Updated the interface of the Sales & Payments tab for the customers for better view of the invoices
- FIXED: Customer's profile got freeze
- FIXED: The invoice ID to be shown correctly when there are more than one group for the customer
- FIXED: the d-none position in invoice edit page
- FIXED: Issue with Invoice attachment
- FIXED: Download attachment buttion ui change
- FIXED: Groups that are disabled doesn't indicate that in the group edit page
- FIXED: Showing groups in the list depending on their expiry date not start date
- FIXED: Tracking the edits on the customer accounts and the contacts are more accurate
- FIXED: Customer Billing Status Wrong Information
- ADDED: Enable supplier login button
- ADDED: SQL Query to create table for Cron Schedular
- ADDED: New template type
- ADDED: Invoice add feature in contact edit page
- ADDED: Added Cron Schedular screens
- ADDED: Tracking the changes for groups (Basic Info, Agents commissions, Airline Information)


-----------------------------------------------------------------------------------------
Version 1.6.4 - January 8th, 2020
-----------------------------------------------------------------------------------------
- IMPROVEMENT: Moving the search field and add a customer button from PAX tab to the group edit page's sub-header
- IMPROVEMENT: show the pax tab directly when start searching
- IMPROVEMENT: Pax Tab got improved in UI
- IMPROVEMENT: Hiding the breadcrumb and the header sub-titles
- IMPROVEMENT: Moving the search field to be on the top left corner in the groups list page
- IMPROVEMENT: Agents, contacts, customers, leaders list pages UI enhanced
- IMPROVEMENT: Enhanced the UI suppliers list page
- IMPROVEMENT: General cancel button in contact edit and invoice listing page
- IMPROVEMENT: Tourname as toltip for Group #1 button in Contact Edit Page
- IMPROVEMENT: Refresh group invoice listing after cancelation
- IMPROVEMENT: The Refresh Button is now fully covering all the customers including the joint customers too
- IMPROVEMENT: Added a loader icon in the window that opens every time after we press UPDATE for better UI UX view
- IMPROVEMENT: Showing the cancelled customers in the manifest report
- IMPROVEMENT: Payment, Invoice Attachment Preview
- IMPROVEMENT: GST-234 - Sum up the total and display at the top in Account Receivable report
- FIXED: Credit note listing duplicate entries
- FIXED: Hide upload file contents, Show attachment link only if there is a file
- FIXED: Use htaccess to prevent file listing
- FIXED: "No fee cancellation" option under cancellation
- FIXED: Disable cancelation for primary customer in pax listing, invoice & contact edit
- FIXED: Show production description instead of product name in invoice
- FIXED: Issue with invoice not showing correct products
- FIXED: Showing wrong commission for agents
- FIXED: Manifest & Rooming List reports and view
- FIXED: Agent not listing customers
- ADDED: Payment attachments in attachment page
- ADDED: Recent registrations report for customers
- ADDED: Show invoice attachment in contact => attachment tab
- ADDED: Ability to see which GST Staff/User has added each customer in Recent Registration Report
- ADDED: Ability to add insurance as a product for each customer


-----------------------------------------------------------------------------------------
Version 1.6.3 - December 27th, 2019
-----------------------------------------------------------------------------------------
- IMPROVEMENT: Added the advanced filters for Customers List
- IMPROVEMENT: Corrected the calculations for the suppliers in the Expense Tab in group edit page
- IMPROVEMENT: Added cancelation date in pax removal page
- IMPROVEMENT: Added the general expenses total to the expenses tab and improved the tables for the expenses list and the agents commision list
- IMPROVEMENT: Decreased the space between the icon and the menu name in the header for better view in smaller screens
- IMPROVEMENT: Added the second address details in the contact info page, shipping labels preview page and PDF page
- IMPROVEMENT: When adding a new expense related to a group, added the button to save and go back to the group
- IMPROVEMENT: Ability to view the attachment files for the general expenses records on a new tab instead of downloading them
- IMPROVEMENT: Removed unused files for better performance and issue tracking
- IMPROVEMENT: Refund type field in refund page
- IMPROVEMENT: Refund type show in listing and in receipt/invoice
- IMPROVEMENT: Download customer payment attachment in invoice add page
- IMPROVEMENT: Refund Receipts menu added
- IMPROVEMENT: Showing the name of the extension product or the transfer product for primary or roommate contacts in the rooming list reports
- IMPROVEMENT: Showing the "Purchased Separate Ticket" field in the reports of the manifest
- IMPROVEMENT: Added a dropdown to easily access the main commands for each group from inside the customer edit page
- IMPROVEMENT: Show payment descrption in invoice add page
- IMPROVEMENT: Supplier's payments list is now ordered from the newest to the oldest and shows minimum 20 entries
- FIXED: Redirect issue in credit note
- FIXED: The order of the zip code to be after the state in the shipping labels lists
- FIXED: Not listing customers without invoice in pax tab
- FIXED: Position of payment attachment changed
- FIXED: Suppliers not listing groups
- FIXED: Multiple group sales order records affects invoice balance
- FIXED: Removed the profile images that they aren't exist on the system from the header
- FIXED: Supplier login limitations applied
- ADDED: Credit note listing and editing
- ADDED: Credit notes menu added
- ADDED: Supplier Login Feature
- ADDED: File attachment option for payment add screen


-----------------------------------------------------------------------------------------
Version 1.6.2 - December 13th, 2019
-----------------------------------------------------------------------------------------
- IMPROVEMENT: Redirect to pax page after cancelation
- IMPROVEMENT: Group Reference # Column in the groups list page
- IMPROVEMENT: Showing details for the extensions and transfers in the rooming list report
- IMPROVEMENT: Added the icons for the transfer and the extension products in the PAX tab
- IMPROVEMENT: Showing the recent 5 group notes in the Group Edit Page for Staff/users
- IMPROVEMENT: Changed some labels on the CRM software as Tony requested
- FIXED: Email Templates Wordwrap fix
- FIXED: Invoice page cancelation fix, seperate pax balance column
- FIXED: Hide canceled customers from shipping label
- FIXED: Show refund button for canceled
- FIXED: Balance corrected after refund issue
- FIXED: balance for canceld and no payment
- FIXED: Additional customers cancelation not working
- FIXED: List zero invoice pax in pax listing
- FIXED: Refund screen not showing canceled customer, since this page is used afer cancellation
- FIXED: Account showing disabled in pax listing
- FIXED: rooming list report for transfer and extension
- FIXED: Centered the notification label in the PAX table
- FIXED: Enabled the tour leader discount amount field to be editable
- FIXED: Cancelation outcome shown instead of "Cancellation charge"
- FIXED: Balance fixed even when the customer has not enough cancelation charge in their payment
- FIXED: Comments added for cancelation calculation
- FIXED: Adrress in tour leader attachment
- FIXED: Rooming list not getting updated after chosen as "TBD 999"
- FIXED: Added the primary customer also in the rooming list update queries
- FIXED: Adding the flags for the Extension, Transfer, Airline Credit, Joint Invoice, Tour Leader Discount, to the PAX query that Nithin updated
- ADDED: Tour leader report with address field


-----------------------------------------------------------------------------------------
Version 1.6.1 - November 23rd, 2019
-----------------------------------------------------------------------------------------
- IMPROVEMENT: Made the products dropdown fields in invoices searchable
- IMPROVEMENT: The buttons for Saving in the invoices are more organized and QuickBooks style
- IMPROVEMENT: Separating the Package Price from the Balance for participants in the Group's Pax tab
- IMPROVEMENT: Server side group listing in dashboard
- IMPROVEMENT: Increase the loading speed for the Group Edit Page
- IMPROVEMENT: Added the option "N/A" in the State field for the contacts edit page
- IMPROVEMENT: The rooming list preview tab is clickable now and get to the customer edit page directly
- IMPROVEMENT: Adding the note (debit and credit card payments subject to 4% fee) to the invoices pdf files
- IMPROVEMENT: Adding a note before showing any report in the groups edits page
- IMPROVEMENT: Show disabled customer's icon on the PAX tab in the group edit page
- IMPROVEMENT: Moved the emergency contacts fields to the contacts tab instead of the customer for less busy view
- IMPROVEMENT: Added the group dropdown for the extension window for better functionality when there are more than one group
- IMPROVEMENT: Showing the column of "Need Transfer?" for rooming list report
- FIXED: updated the style of the products fields in the invoice view
- FIXED: Rooming list keeps getting duplicated sometimes
- FIXED: List zero invoice in under sales tab
- FIXED: some queries not adding Group_ID for the customer invoices
- FIXED: Rooming report was showing disabled customers in the list
- FIXED: Report menu not showing
- FIXED: Customer search using first name, middle and last all together in pax add customer
- ADDED: Tour Leader Discount Feature
- ADDED: Extension Feature for customers
- ADDED: Transfer Feature for customers


-----------------------------------------------------------------------------------------
Version 1.6.0 - November 6th, 2019
-----------------------------------------------------------------------------------------
- IMPROVEMENT: Improving the listing of the groups to not show the errors or warning in the dashboard
- IMPROVEMENT: Manifest Report Change (GST-194)
- IMPROVEMENT: Requesting the Product Upgrade info only when needed in the contacts edit page
- IMPROVEMENT: Added 60 day rule for invoice refund
- FIXED: Create Refund Receipt list - Quick fix on filter
- FIXED: Invoice summary Incorrect for supplier login
- FIXED: Balance change for customer invoice
- FIXED: Sales tab remaining & owed
- FIXED: Method change for retrieving payment listing
- FIXED: Agent login not showing up data
- FIXED: Adding the variable $TourName for the airline tab in the groups edit page
- FIXED: Showing the invoice details in the contacts Sales tab for any additional traveler in the joint invoices
- FIXED: Tourleader discount not saving issue
- FIXED: Updated the database queries for Products_logisitics
- FIXED: Credit Discount value
- ADDED: Supplier login feature
- ADDED: Airline Flights info Feature (For group airline in general and for each customer in the group)
- ADDED: Add contactid in users table
- ADDED: Phase 1, Adding the ability for the Tour Leader Discount 100% in the receiving payments page for customers
- ADDED: Create logo and add in the email (GST-186)
- HOT FIX: Fixing the issue with the rooming list fields in the customers tab
- HOT FIX: Issue with the rooming list and roommates in the customer tab


-----------------------------------------------------------------------------------------
Version 1.5.9 - October 20th, 2019
-----------------------------------------------------------------------------------------
- IMPROVEMENT: Showing the Advanced Filter for groups by default in the Groups List Page
- IMPROVEMENT: Added the dollar sign in all the currency fields
- IMPROVEMENT: All dates in the system to reflect MM/DD/YYYY
- IMPROVEMENT: Updated the interface for the customers and listed all the groups in the customer tab
- IMPROVEMENT: Added the option of Proposal / Brochure in the Group's Attachement tab
- IMPROVEMENT: Improved the user interface when updating the password
- IMPROVEMENT: Improved the interface for the Sales & Payments in Contacts edits page
- IMPROVEMENT: Modifications to Land Sub Product in the group edit page
- IMPROVEMENT: Red coloring of canceled customers
- IMPROVEMENT: The customer's groups list is now more categorized (Active, Expired, Joint, Cancelled) in the contact edit page
- IMPROVEMENT: Add "Manage Chart of Accounts" in Accounting Menu
- IMPROVEMENT: Added the line for the total paid for each group in the Contacts Sales Tab
- IMPROVEMENT: Track the edits/changes of the contact information
- FIXED: Showing the cancelled invoices and customers in the PAX list in the group page
- FIXED: List the groups to be expired depending on the Ending Date not Starting Date
- FIXED: The name badges pdf template is decrease 0.2 inches
- FIXED: Cleaned the folders that aren't used in ASSETS folder
- FIXED: Adding the due date for invoices when adding current customer to the group
- FIXED: Login to GST is not working on Firefox
- FIXED: The fields in the add new contact page to be organized and not outside the box
- FIXED: Changed the rooming list queries and functions to not show duplicate records for a customer that has several groups
- FIXED: The print view for the customer invoices are correct
- FIXED: Fixed the search function when you are in the customers list page
- FIXED: Showing only one customer record in the customers list page
- FIXED: Invoices list tab and Pax List tab now show the current customer to the current group only
- FIXED: Remove strikethrough and add cancellation date for cancelled customers in group edit page
- FIXED: Correctly calculate the joint invoice entries in the invoice and the customer account tab
- FIXED: Customer refund properly while canceling
- FIXED: The payments list for the customers has a correct table layout now
- ADDED: The rooming list report is added to the Tour Leader Reports
- ADDED: Send an email to the login User who is sending Group Emails
- ADDED: One Customer to Many Groups Feature
- ADDED: Each group has its own Staff Representative and ability to filter the groups as well
- ADDED: Added Save button to redirect it to Add & Edit Invoice page
- ADDED: Canceled participants in a separate list
- ADDED: Primary member cancelation disable, all others can be canceled
- ADDED: Agent's Payments Feature
- ADDED: Agent's Payments List Feature
- ADDED: Leader Login page, Menu Toggle, and Account Switch Option
- ADDED: Account switching between leader and agents
- ADDED: Create email log for sent emails
- HOT FIX: The balances for the customers who joins several groups are corrected now in the PAX tab in the group edit page
- HOT FIX: The invoice ID for additional travelers in a joint invoice has the primary customer invoice id in the Sales Tab now


-----------------------------------------------------------------------------------------
Version 1.5.8 - September 9th, 2019
-----------------------------------------------------------------------------------------
- IMPROVEMENT: Showing all the participants list in the tour leader report in one page only
- IMPROVEMENT: Made the contacts edits logs box to be collapsed
- IMPROVEMENT: Added the price of the product next to the name in the customer edit page, upgrades section
- IMPROVEMENT: Added the price label for the land products upgrades
- IMPROVEMENT: The upgrade products field are now more advanced while searching in the customer edit page
- IMPROVEMENT: Ability to click on the roommate to open its own customer page
- IMPROVEMENT: The Chart of Accounts has the option to add new category directly from the list
- IMPROVEMENT: Ability to edit the quotes records
- FIXED: Tour leader report not showing correct balance amount
- FIXED: Added the ability to unvoid the disabled and voided customer invoices
- FIXED: Single rooming type update fixes
- FIXED: The room type issue in production that doesn't update or change
- FIXED: Changed the order of logs to be DESC in the customer edit page
- FIXED: Invoice_Line_Total mismatch, and Invoice_Amount sum mismatch
- FIXED: Invoiced_customer prevents credit card surchage
- FIXED: Sales invoice listing total & balance fix
- FIXED: Joint invoice multiplies total price with quantity in invoice_line_total, removed.
- FIXED: Removed the disabled customers from the rooming list report
- FIXED: The listing page for: Supplier Bills, Supplier Payments, General Expenses, Chart of Accounts, Expenses Payee
- FIXED: Payment page back button duplicate save issue , Invoice save &new button not working
- FIXED: Edit payment incorrect balance showing
- FIXED: Rooming List shows dublicate rooming records for the same particpants
- FIXED: Code mistake in the ACL permissions for the Settings Menu
- FIXED: Navigation corner dropdown menu not reopening after closing
- FIXED: Group cancel and pax count fix in group listing
- FIXED: Removed - Credit note remark prefix "Discount #"
- FIXED: Payment fields converted to input type number from text
- FIXED: Adding Customer to a group that does not have product produces an error
- FIXED: When creating a new contact and adding it to the group from the group itself, the invoice issue is now solved for them
- ADDED: Tracking the changes and updates in details for the customers
- ADDED: A new tab to list all the payments that been sent to the supplier
- ADDED: Proposal Quotes Feature
- ADDED: Email Templates Feature
- ADDED: Tour member Cancel Feature
- ADDED: Tour leader reports to include rooming list report
- ADDED: Adding the feature of advanced filtering for the groups listing
- HOT FIX: Some groups can't download the excel sheet for rooming list report
- HOT FIX: The issue with the trigger when there are no updates
- HOT FIX: Issue not being able to select customers to be added to the joint invoice fields


-----------------------------------------------------------------------------------------
Version 1.5.7 - August 15th, 2019
-----------------------------------------------------------------------------------------
- IMPROVEMENT: All inputs are using escape function for better security
- IMPROVEMENT: Receipt, invoice and remiders set to use common mail function
- IMPROVEMENT: Consdier credit note for  paymment balance on payment add page
- IMPROVEMENT: Added the ability to delete a customer from a group and made sure that all the tables and lines on the database are updated as needed
- IMPROVEMENT: Inserting the Group Price when adding a new customer to the customer invoice entry
- IMPROVEMENT: Airline dropdown autosuggest inside the Airline tab in group edit page
- IMPROVEMENT: Save & new, save & close button change in add bills,expenses,user,payment,product and group
- IMPROVEMENT: Joint Invoice flag changed to Group Invoice flag in the database
- IMPROVEMENT: The group notes are now more organized and added a notification counter of the number of notes created
- IMPROVEMENT: Reports moved to seperate tab in group edit page
- IMPROVEMENT: Type and create new airline entry, if not exists in airline database inside the Airline tab in group edit page
- IMPROVEMENT: Airline search delay added
- IMPROVEMENT: Return to edit group after "Save & Close" restored
- IMPROVEMENT: Success message added on payment, refund, credit note, invoice save action
- IMPROVEMENT: All the reports tabs are organized more when listed in the page
- IMPROVEMENT: Mark invoice sent after it is sent to a customer
- IMPROVEMENT: Show the changelog for each contact in the right sidebar
- IMPROVEMENT: Changing the interface design of the CRM to become more userfriendly and horizontally for more space
- IMPROVEMENT: Added the logout feature to the main menu and the CRM status line as well
- IMPROVEMENT: Updated the CRM status line to be shorter and clearer
- IMPROVEMENT: Added the Account Receivables Report to the main report menu
- IMPROVEMENT: All the reports view or download are now showing the full name of the customer
- IMPROVEMENT: The main menu is now mobile friendly
- IMPROVEMENT: Show a warning if the gender is not selected when updating the customer
- IMPROVEMENT: Added rules for the birthday dates in customer page for max date and min date
- IMPROVEMENT: Added filters for the groups' status for each agent
- IMPROVEMENT: Excluded non-invoiced customers from sales
- IMPROVEMENT: The Save & New Button and Save & Close Button have been improved
- IMPROVEMENT: The Joint Customers Payments are now shown as a sum together in the Print Invoice View
- IMPROVEMENT: The margins and the layout of the Name badges is modified to match the badge box
- IMPROVEMENT: Ordering the customer payments by Adding time in the invoice print view
- FIXED: Save & new and save & close button changes in contact,invoice and payment forms
- FIXED: Add user screen create users with admin privilege, not role id not mapped
- FIXED: Group invoice list all invoice when filtering
- FIXED: Redirect after credit note changed to invoice edit
- FIXED: Missing invoice in redict to payment page
- FIXED: Multiple time listing same credit note invoices
- FIXED: Group invoice payment include primary customer, duplicate payment for same customer
- FIXED: Email pdf issues on tour leader report
- FIXED: Customer ID check in invoice to payment, credit note, refund removed. Since single invoice can have multiple customer payment in joint invoice.
- FIXED: Fixing compatibility issues in bills, contacts, expenses, add payment, sales invoices and supplier bill payments
- FIXED: Fixing compatibility issues in groups add, products, and users
- FIXED: Group invoice credit charge not saving, payment info not showig in edit payment page
- FIXED: The layout of PAX tab in the groups edit page
- FIXED: Cancellation Refund Sl no issue in invoice
- FIXED: Agent login fixed based Sary's testing report
- FIXED: Updated the text of the rooming lists report
- FIXED: The expiry date for groups listing now depends on the end date order
- FIXED: Showing all the records in one page instead of each 20 records in one page
- FIXED: The date format for the reports in the groups is now M d, Y
- FIXED: The order of the customers list and editted the columns view
- FIXED: Changed the formate of the date for the customers list
- FIXED: Remove agent commision from contact page
- FIXED: The table code in the reports tab to be better responsive and formatted correctly
- FIXED: The flash of the menu everytime user loads a page in the CRM is now removed
- FIXED: Show the full name for each participant in the PAX tab in groups edit page
- FIXED: The gender field to be left blank if not selected
- FIXED: The buttons of the main menu are now working on the mobile devices screens
- FIXED: Missing token in password reset email
- FIXED: Password mailed as plain text intead of md5
- FIXED: Update role when other role already exists
- FIXED: Non-invoiced customers removed from account receivable reports
- FIXED: Changing "invoiced customer flag" to checkbox instead of dropdown
- FIXED: Adding the Send Payment button in the supplier bills
- FIXED: The bug of not showing the supplier info when the supplier bill is voided
- FIXED: Name badge field is correct in the reports
- FIXED: The roommates names are called correctly
- FIXED: Charges are calculated and sum up correctly
- FIXED: Showing the Single Package Price in the customer invoice print view
- FIXED: Removed the dublicate surcharge cell in the invoice print view
- ADDED: Airline table entry inside the Airline tab in group edit page
- ADDED: Distribute credit amout to all payments made on joint invoice
- ADDED: Comment added for a place where credit_note no need to distribute across each pyament
- ADDED: Token type for change password and forgot password
- ADDED: Group bulk email functionality
- ADDED: Multiple agent commission type
- ADDED: Adding agents will create new agent table entry, and will bring it in agent info box
- ADDED: Group email attachment
- ADDED: The search feature on the main menu is now working correctly
- ADDED: Added placeholders icons on the right corner of the menu (Notifications, messages, and settings)
- ADDED: The user profile feature
- ADDED: Add to leader table when a leader is created
- ADDED: "invoiced_customer" field in customer tab in contact edit page
- HOT FIX: Production Issue: Joint Invoices and Single Room Type conflict


-----------------------------------------------------------------------------------------
Version 1.5.6 - June 21th, 2019
-----------------------------------------------------------------------------------------
- IMPROVEMENT: The email sender system to use SMTP for free-spam performance
- IMPROVEMENT: Used Datatable searching issue in payment page
- IMPROVEMENT: Supplier tab is improved to calculate everything accurately
- IMPROVEMENT: Rooming and manifest report are now viewable before downloading
- IMPROVEMENT: Credit note formated in receipt, Payment page adjust credit note
- IMPROVEMENT: Credit note multiple add provided instead of single add/edit
- IMPROVEMENT: Credit note button in sales invoice page
- IMPROVEMENT: Delete credit note button in sales-invoice edit page
- IMPROVEMENT: Ajax participant filters in PAX tab
- IMPROVEMENT: The reports are in one tab in groups edit page
- IMPROVEMENT: The attachments are listed on the right sidebar
- FIXED: Sales invoice credit not fixed in summary box
- FIXED: Used domainUrl() method uses crm_path from config.php file
- FIXED: Bugs in Sales Invoice Page
- FIXED: Updating the customer invoice lines when clicking on Airline Product or Land Product Update Button
- FIXED: Supplier Payments Page Bugs and issues
- FIXED: Calendar issues
- FIXED: Invoice cancellation credit card charges issue
- FIXED: The customer list page not listing correctly and sometimes empty
- FIXED: Invoice, payment date issue in receipt
- FIXED: Credit note date issue
- FIXED: The customers total number in dashboard
- FIXED: Removed query echo on customer add page
- FIXED: Credit note design changes, Send mail mime header added
- FIXED: Credit note in invoice page alignment fix
- FIXED: When choosing a roommate now and that pax already has a single supplement, it will be deleted from the sales order
- FIXED: The link to the group page from the customer edit page redirects to the PAX tab now
- FIXED: Changed the Select query in the group list in the dashboard to get the contactid column instead of id column
- FIXED: Removed database.table format, since db name can change from invoice to invoice
- FIXED: Prevent credit note for cancelled invoices
- ADDED: Estimate cost for groups without products
- ADDED: Credit note in customer invoices
- ADDED: Credit note linked from accounting
- ADDED: Rooming List Feature
- ADDED: "Save & Close" and "Save & New" buttons in adding a new contact page


-----------------------------------------------------------------------------------------
Version 1.5.5 - June 8th, 2019 | Urgent Fixes
-----------------------------------------------------------------------------------------
- IMPROVEMENT: Speeded up the Sales Order List (All Sales) page to faster load from the server side
- IMPROVEMENT: When creating a new customer in the group PAX tab, show only the group members in the roommates list (Double & Triple)
- IMPROVEMENT: The functionality of the REFRESH button in the PAX tab is more accurate now
- IMPROVEMENT: Enhanced the list of PAX to highlight the joint invoice participants
- IMPROVEMENT: Speeded up all the tables list in all the CRM system pages
- FIXED: The invoice ID for the additional travelers of the Joint Invoice will get the correct primary customer invoice in their customer edit page
- FIXED: Invoice balance issue fix
- FIXED: Error when adding a customer to a group
- ADDED: Feature - Credit note model files
- ADDED: Two new titles in for the customer/contact (Dr. & Rev.)

-----------------------------------------------------------------------------------------
Version 1.5.4 - June 5th, 2019
-----------------------------------------------------------------------------------------

- IMPROVEMENT: Fixed some queries in the dashboard file
- IMPROVEMENT: The supplier tab edits and fixes
- IMPROVEMENT: Showing the balance of the supplier on the top right sidebar
- IMPROVEMENT: Showing the total paid for each group in the group lists in the supplier tab page
- IMPROVEMENT: Ordered the groups listed in the supplier tab regarding the Departure date
- IMPROVEMENT: Refresh the page when pressing on "update" in Airline tab in group edit page
- IMPROVEMENT: The "Add Payment" button in the expense tab will show the group for the user only
- IMPROVEMENT: Update the Supplier Bill Total Amount directly in the main supplier bill list page
- IMPROVEMENT: The calculations in the expenses tab in groups edit page are calculated correctly
- IMPROVEMENT: Added the Date of Registration for the participants in the PAX tab in Groups edit page
- IMPROVEMENT: Updated the SQL shared file (Removed three tables, we don't need them)
- IMPROVEMENT: Ability to see the disabled suppliers and active ones on the Suppliers Main List
- IMPROVEMENT: Changed the style of the tabs on the contacts page and the groups page to be more clear
- IMPROVEMENT: The view of the supplier page is more organized now
- IMPROVEMENT: Making the products groups list in Supplier page ordered by "Paid" or "Not Paid Yet"
- IMPROVEMENT: Showing the rows of the paid products for the supplier only when there are paid ones
- IMPROVEMENT: Show the type of the user in the Users List (Admin or Agent)
- IMPROVEMENT: The PAX number is now shown in the Agent List
- IMPROVEMENT: Updated the style of the tabs in Groups and Contacts
- IMPROVEMENT: Removed all the new window links and instead, when pressing BACK you will get back directly to the correct tab in Groups Edit page
- IMPROVEMENT: Supplier Bills improved 
- IMPROVEMENT: Used ajax for payment page datatable
- IMPROVEMENT: Showing the calendar on the sidebar
- IMPROVEMENT: Added ajax feature for sales invoice
- IMPROVEMENT: The agent view is more organized now
- IMPROVEMENT: The sidebar is more functional now when clicking on the main buttons "Contacts", "Sales" and "Expenses"
- IMPROVEMENT: The groups listed in the supplier tab page is now sorted depending on the balance "Paid, Not Paid, Paid more than needed"
- FIXED: The "Add Payment" button for suppliers in the group expense tab
- FIXED: Payment summary details not showing
- FIXED: The changelog.php file is now highlighted in the sidebar
- FIXED: Showing all the upgrades not only one inside the Supplier Page
- FIXED: Group wise total paid/unpaid for agent login dashboard
- FIXED: Customer balance calculation invoice and print
- FIXED: Refund amount must be negative
- FIXED: When cancellation charge & Refund amount issue
- FIXED: Long error message in the whole supplier tab page
- FIXED: Balance due issue in invoice add page
- FIXED: Supplier long error message bug and some improvements
- FIXED: Remove the disabled customers from the tour leader reports
- FIXED: Showing the number of active groups only in the admin dashboard
- FIXED: Showing the Land Special Request Data in the rooming list report
- FIXED: Changed monetary values in all pages to decimal 2
- FIXED: Removed the destination name from the Name Badge page and pdf file
- FIXED: Removed the two decimals from the dashboard
- FIXED: The joint invoice calculation issue
- FIXED: Group Invoice & Sales Invoice Ajax Data Table Completed
- FIXED: Showing a warning when there is no contact select in the add new contact to a group window
- FIXED: Not showing the additional travelers if the Joint Invoice button is turned off
- FIXED: Hiding the additional travelers boxes when its not active
- FIXED: When turning off the Joint invoice button, all records are removed
- ADDED: Charts of Accounts Page management (Add, edit, void, unvoid and delete)
- ADDED: Agent Login password screen
- ADDED: Agent login filtering, enable login feature
- ADDED: ACL limiting supplier info for agents, Email template table for agent welcome mail
- ADDED: Change pasword page for new users
- ADDED: Reset password option UI
- ADDED: Forgot password from login screen
- ADDED: Enable forgot password for admin users
- ADDED: Commission type included for agents in edit agent page
- ADDED: Events Calendar functionality
- ADDED: A column next to PAX column in supplier tab to show the Total Cost for each group


-----------------------------------------------------------------------------------------
Version 1.5.3 - May 19th, 2019
-----------------------------------------------------------------------------------------

- IMPROVEMENT: Refund issue page basic design
- IMPROVEMENT: Common script for all fixed datatable headers
- IMPROVEMENT: Category deactivation implemented in different pages
- IMPROVEMENT: Include helper.php via header.php instead of including it in every page
- IMPROVEMENT: Showing the general expenses for groups inside the Groups Edit page / Expenses tab
- IMPROVEMENT: Adding the button to add a new general expense in Accounting page
- IMPROVEMENT: Adding a Voided warning inside the expense record with a button to un-void
- IMPROVEMENT: Added the ability to add, edit, void and delete the payee with its own page
- IMPROVEMENT: When clicking on "Add Expense" from group tab, it will add the group directly to the field
- IMPROVEMENT: Added the the Chart of Accounts (Categories & Sub-categories) dropdown menus in General Expenses
- FIXED: Removed the word "Pilgrimage Package" from the PDF invoice
- FIXED: The batch action issues
- FIXED: Increase the name size in the badges & update the label of joint invoices in the customer page
- FIXED: Repeated results messages when updating the land product tab in groups
- FIXED: Shipping label corrections
- FIXED: The attachment field is now working correctly in General Expenses
- FIXED: bug in updating customers who doesn't have sales order for a reason
- FIXED: Invoice fixes email not sent, invalid balance in edit invoice
- FIXED: Tour Leader Report Changes
- FIXED: Invoices fixed Update product, total not shwoing
- FIXED: Group_ID is now added automatically to the customer_invoice records when adding new customers
- ADDED: Disable leader Feature
- ADDED: Purchase_Order queries and Purchase_Order_Line queries for groups and suppliers
- ADDED: Print button to the invoices edit page
- ADDED: Account Receivable Feature
- ADDED: Refund in Sales Invoices Feature
- ADDED: Supplier Bills & Payments & Purchase Orders
- ADDED: Category activation, deactivation feature in category page
- ADDED: Group Notes Feature
- ADDED: General Expenses Feature
- ADDED: Unvoiding feature is added to the General Expenses list
- ADDED: Agent login menu, group filter, inline hiding
- ADDED: Tour Leader Report
- ADDED: Payee Feature and Charts of Accounts Listing for General Expenses
- ADDED: Sql script sharing files between Developers


-----------------------------------------------------------------------------------------
Version 1.5.2 - April 28th, 2019
-----------------------------------------------------------------------------------------

- IMPROVEMENT: The crm is now on cloud and connected to a repositary
- IMPROVEMENT: A function to update all the sales order and customer invoice lines for each group
- IMPROVEMENT: Asset merge, CRM optimization
- IMPROVEMENT: When opening the contacts page, the search field will be selected automatically
- IMPROVEMENT: The view of the invoice print
- IMPROVEMENT: Reduce flickering on page load
- IMPROVEMENT: Seperated the config.php from the database information for better control
- IMPROVEMENT: Edited the group and invoice reference number instead of the description
- IMPROVEMENT: The config.php to be more dynamic in interaction with the two environment
- IMPROVEMENT: Updated the error message when uploading an attachment document
- IMPROVEMENT: Assigned a default timezone for the CRM software to read from
- IMPROVEMENT: The ability for the primary customer to edit the list of customer in Joint invoices only
- IMPROVEMENT: A function to update the column "groupinvoice" when adding customer to the joint invoice
- IMPROVEMENT: Filter group invoices
- IMPROVEMENT: Improved the Sub product feature when adding from the Land Product tab
- IMPROVEMENT: Disable group leader option & success message
- IMPROVEMENT: The primary customer will not show in the additional travelers dropdown now
- IMPROVEMENT: Additional travelers can't be duplicated now or chosen for the same group more than once
- IMPROVEMENT: Detailed list of Participants Popup
- IMPROVEMENT: Model structure and model classes, with ajax handlers
- IMPROVEMENT: Merged the the airline product and land product to be one product in the PDF invoice
- IMPROVEMENT: The participants list table in the PAX tab in group edit page has a search field
- FIXED: The sidebar.php to become active when opening the invoice edit or add page
- FIXED: The search issue with the contacts page to be able to search for first and middle and list name at once
- FIXED: Invoice cc to sary and tony
- FIXED: Invoice message changes and send reminder email not working
- FIXED: The expenses calculations in the Groups edit page
- FIXED: The label "This group is expired" to show after the Return Date occurs
- FIXED: The Twin room type field not showing the roomate option
- FIXED: Not showing the balance paid by the customer in the Participants tab
- FIXED: The groups warninig notification counter in the sidebar
- FIXED: The error in updating the airline product tab in the group page
- FIXED: The subproduct table in Land Product Tab
- FIXED: The total amount of the Joint Invoice is now updated correctly
- FIXED: The balance due issue that makes the amount x 10 times
- FIXED: The group_purchase_order table is now updated in the files
- ADDED: Invoice reminder popup
- ADDED: The accounting tab to the menu
- ADDED: Two fields to the group basic info (Deposit amount & 2nd payment amount)
- ADDED: The Gender field in the Manfiest Excel Report
- ADDED: The ability to search for the mobile number in the contacts list page
- ADDED: Payments page design, payment listing, style changes
- ADDED: A simple way to debug the load time for the pages
- ADDED: Customer Refund
- ADDED: Currency changes, Add a payment to Receive payment title change
- ADDED: Added "Deacon" is an option in the Title field
- ADDED: Payment summary in invoice listing page
- ADDED: Group invoice list
- ADDED: Joint Invoices Feature
- ADDED: Name Badges Preview Page
- ADDED: Name badges download/print
- ADDED: The Single Supplement is now a separate product and calculated individually
- ADDED: Made the Ticket discount a spereate product
- ADDED: Invoice & Credit card charges
- ADDED: Added Shipping Labels
- ADDED: Account Receivable Page
- ADDED: The ability to see the list of Additional Travelers in the Joint invoice of the primary customer


-----------------------------------------------------------------------------------------
Version 1.4.0 - February 26th, 2019
-----------------------------------------------------------------------------------------

- IMPROVEMENT: Removed the "groups_products" table and merged it with "products" table
- IMPROVEMENT: User can now edit the airline information and details in the edit product page directly
- IMPROVEMENT: The cost and the price fields of the airline product and the land product are now shown in the group tabs
- IMPROVEMENT: Removed the discount price of the ticket from the groups table and added it to the products table 
- FIXED: Removed the Groups Rooming List table and reverted back to the old interface
- FIXED: When adding new or current contacts to the group, they are automatically added to the Sales_order_line table as well
- FIXED: The wrong numbers for the total amount for each customer in the Participants tab in Groups
- FIXED: When selecting an Airline product or Land one, they are automatically added to the Sales_order_line table for all the customers of that group


-----------------------------------------------------------------------------------------
Version 1.3.9 - February 02nd, 2019
-----------------------------------------------------------------------------------------

- NEW: Created a tab for Expenses and in-depth Details numbers in the group page.
- IMPROVEMENT: Added the two fields for Church Name and Departure City in the Group edit page.
- IMPROVEMENT: Added the option for Complimentary Participants in the Sales and Payments Tab for the customers.
- IMPROVEMENT: Added a notification when trying to edit or update a customer and the birthday isn't filled up.
- IMPROVEMENT: Reformated the numbers of the dashboard widgets to have commas for clearer readings.
- IMPROVEMENT: Improved the loading speed for the customers list page when requesting all the data from the database.
- IMPROVEMENT: Reordered the groups list table to make the order depending on Departure Date.
- IMPROVEMENT: Added the option of "Discount" in Add a payment for the customer.
- IMPROVEMENT: When updating contacts info, any required field will show a better notification now.
- FIXED: Fixed the search problem in the contacts list page so you can search multiple names at once.
- FIXED: Fixed the problem were the balance of the customer isn't updated when the price of the group changes.
- FIXED: Fixed the error that gives you a corrupted and blank page when openning a group page.
- FIXED: Fixed the problem of not showing the payments occured by each customer in the group's participants tab.
- FIXED: Fixed when adding a new customer to a group, it won't be specified as a SINGLE Room type anymore.
- FIXED: Fixed the agents where they conflicting with the leaders ID and links and redirect you to the leader tab not the agent tab.
</pre>
				</div>
			</div>
		</div>
		<div class="col-sm-1">
		</div>
	</div>
<!-- [ page content ] end -->
<?php include "footer.php"; ?>