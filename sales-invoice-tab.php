<?php
if( ! defined('INVOICE_ADD_FRAGMENT_VIEW') ) {
	$PageTitle = "Customer Invoice";
    include "header.php";
}
include_once __DIR__ . '/models/customer_credit_note.php';
include_once __DIR__ . '/models/customer_invoice.php';
include_once __DIR__ . '/models/customer_invoice_line.php';
include_once __DIR__ . '/models/customer_payment.php';
include_once __DIR__ . '/models/customer_refund.php';
include_once __DIR__ . '/models/contacts.php';
include_once __DIR__ . '/models/groups.php';

global $GLOBAL_SCRIPT;
$SubmitAction = 'create';

$invoiceId =$__GET['invid'] ;

$ADD_URL = 'sales-invoice-add.php?invid='.$invoiceId ;

if( basename($_SERVER['SCRIPT_NAME']) == 'contacts-edit.php' ) {
    $contactIdForRedirectData = (new CustomerInvoice())->get(['Customer_Invoice_Num' => $invoiceId]) ;
    $contactIdForRedirect = $contactIdForRedirectData['Customer_Account_Customer_ID'] ;
    $ADD_URL = "contacts-edit.php?invid=$invoiceId&id=$contactIdForRedirect&action=edit&usertype=sales" ;
}

//Actions are handled in seprate file.
if ( isset($__REQUEST['submit']) && isset($__POST['hidInvoiceSave']) && $__POST['hidInvoiceSave'] == '1') {
    include __DIR__ . '/inc/sales-invoice-save.php';
    if ($__POST['submit'] == "create_new" || $__POST['submit'] == "update_new")
    {
        GUtils::redirect('sales-invoice-add.php');
    } 
    else if (($__POST['submit'] == "create_only") || ($__POST['submit'] == "update_only" || $__POST['submit'] == "update_message_only"))
    {
        GUtils::redirect($ADD_URL);
    }
    else
    {
        GUtils::redirect('sales-invoices.php');
    }
}
/**  * Delete note action ?  * { */
if( isset($__GET['noteid']) && isset($__GET['invid']) && $__GET['action'] == 'deletenote' ) {
$noteId = $__GET['noteid'] ;
 $obj = new CustomerCreditNote() ;
 $obj->delete(['Note_ID' => $noteId]) ;
 
 GUtils::redirect('sales-invoice-add.php?invid=' . $__GET['invid']);
 return; 
}
/**  }  */    


//Edit Mode {
//Invoice Number
$invoices     = [];
$invoiceLines = [];
$editId        = 0;
$editContactId = 0;
$paymentList    = [] ;
if (isset($__GET['invid'])) {

    $SubmitAction = 'update';
    $editId   = $__GET['invid'];
    //get invoice data.
    $invoices = (new CustomerPayment())->invoiceDetails($editId);
    
    //format billing address
    $invoices['address'] = GUtils::buildGSTAddress( false, $invoices, ",", "\n" ) ;

    $editContactId = $invoices['contactid'];
    
    //get invoice lines
    $invoiceLines = null ;
    $invoiceLines = (new CustomerInvoiceLine())->getInvoiceLines($editId) ;
    
    //get payments made
    $paymentList = (new CustomerPayment())->invoicePaymentsList($editId) ;
} else {

    //initialize wiht null string for add page. otherwise might cause undefined 
    $invoices['email']    = '';
    $invoices['Due_Date'] = '';
    $invoices['Comments'] = '';
    $invoices['paid'] = '';
    $invoices['due'] = '';
    $invoices['address'] = '';
    $invoices['tourid'] = '' ;
    $invoices['Invoice_Status'] = 0 ;
    $invoices['RefundStatus'] = 0 ;
    //TODO; for add, enable it. confirm with jyo {
    $invoices['invoiced_customer'] = 1 ;
    //}
}

//Credit Notes
$creditNotes = [] ;
if( $editId ) {
    $creditNotes = (new CustomerCreditNote())->getList(['Customer_Invoice_Num' => $editId, 'Status' => 1]);
}
$creditNoteTotal = 0 ;
foreach( $creditNotes as $one ) {
    $creditNoteTotal += $one['Credit_Amount'] ;
}

//if invalid due date in invoice table, recalculat the same
if( ! $invoices['Due_Date'] || strtotime($invoices['Due_Date']) == 0 ) {
    $tourId = $invoices['tourid'] ;
    $invoices['Due_Date'] = (new Groups())->getDueDate($tourId) ;
}

//} Edit Mode

$ccCharges = 0.0 ;
$numCharges = 0 ;
foreach( $paymentList as $one ) {
    if( floatval($one['Charges']) > 0 ) {
        $numCharges ++ ;
        $ccCharges += $one['Charges'] ;
    }
} 

$canceledInvoice = ($invoices['Invoice_Status'] == 4) ;
?>

<input type="hidden" id="invoiceDataEx" class="invoice-data" data-invoice-status="<?php echo $invoices['Invoice_Status'];?>" data-invoice-revised="<?php echo $invoices['Invoice_Amount_Revised'];?>" data-balance="<?php echo $invoices['balance'];?>" data-invoice-Amount="<?php echo $invoices['Invoice_Amount'];?>" data-credit-note="<?php echo $creditNoteTotal ;?>" />

<?php
if( $invoices['Invoice_Status'] == CustomerInvoice::$CANCELLED ) {
    $balanceToPrint= $invoices['balance'] ;
    $caption = 'Balance Due' ;
    
    if( $invoices['Refund_Status'] == CustomerRefund::$CREATED || 
        $invoices['Refund_Status'] == CustomerRefund::$ISSUED ||
        $invoices['Refund_Status'] == CustomerRefund::$PARTIALLY_ISSUED ) {
        
            $caption = 'Refund Due' ;
            $balanceToPrint= $invoices['Pending_Refund_Amount'] ;
    }
}
else {
    $caption = 'Balance Due' ;
    $balanceToPrint= $invoices['balance'] ;
}

//use invoice amount when canceled.
?>

<form class="not-common-form" name="invoiceAddForm" action="" method="POST" id="contact1" enctype="multipart/form-data">
    <div class="card">
        <div class="card-header">
            <h5 class="pt-2">View Invoice</h5>
            <?php if(isset($__GET['invid']) && $invoices['Invoice_Status'] == 0) { ?>
            <a href="javascript:void(0);" onclick='deleteConfirm( "sales-invoices.php?action=unvoid&InvoiceID=<?php echo $editId; ?>", "Unvoid")'>
                <input <?php echo (!$editId) ? 'disabled="disabled"' : ''; ?> class="btn waves-effect waves-light <?php echo (!$editId) ? 'btn-disabled' : ''; ?> btn-danger float-right pt-1 pb-1" type="button" value="Un-void" />
            </a>
            <?php } else { ?>
            <?php if( $editId && $invoices['Invoice_Status'] != 4 && $invoices['balance'] > 0 ) { ?>
            <a href="payments-add.php?invoice=<?php echo $invoices['Customer_Invoice_Num']; ?>&CustomerID=<?php echo $leadid; ?>">
                <input <?php echo (!$editId) ? 'disabled="disabled"' : ''; ?> class="btn m-l-5 waves-effect waves-light <?php echo (!$editId) ? 'btn-disabled' : ''; ?> btn-info float-right pt-1 pb-1" type="button" value="Receive Payment" />
            </a>
            <a href="credit_notes_add.php?invid=<?php echo $invoices['Customer_Invoice_Num']; ?>&CustomerID=<?php echo $leadid; ?>">
                <input <?php echo (!$editId) ? 'disabled="disabled"' : ''; ?> class="btn m-l-5 waves-effect waves-light <?php echo (!$editId) ? 'btn-disabled' : ''; ?> btn-success float-right pt-1 pb-1" type="button" value="Credit Note" />
            </a>
            <?php } else if( $editId &&
                ( ($invoices['Invoice_Status'] != 4 && $invoices['balance'] <= 0) ||
                ($invoices['Invoice_Status'] == 4 && ( $invoices['Refund_Status'] == 3 || $invoices['Refund_Status'] == 1) && $invoices['balance'] != 0.0) )
                ) { ?>
            <a href="refund_receipt_add.php?invid=<?php echo $invoices['Customer_Invoice_Num']; ?>&back=contact-edit">
                <input <?php echo (!$editId) ? 'disabled="disabled"' : ''; ?> class="btn m-l-5 waves-effect waves-light <?php echo (!$editId) ? 'btn-disabled' : ''; ?> btn-info float-right" type="button" value="Issue A Refund" />
            </a>
            <?php } ?>
            <?php } ?>
            <div class="clearfix"></div>
            <hr />
        </div>
        <div class="card-block pt-0 pb-4 row" style="flex-wrap: nowrap !important;">
            <div class="form-default form-static-label col-sm-4">
                <label class="float-label gst-label">Customer Info</label>
                <input value="<?php echo $editContactId; ?>" type="text" name="CurrentCustomerSelected" class="form-control" hidden required="">
                <h5><?php echo $invoices['fname'] . " " . $invoices['mname'] . " " . $invoices['lname']; ?></h5>
                <h6><?php echo $invoices['email']; ?></h6>
                <label><?php echo nl2br( $invoices['address'] ) ;?></label>
            </div>

            <div class="form-default form-static-label col-sm-8">
                <div class="row">
                    <div class="form-default form-static-label col-sm-4">
                        <label class="float-label gst-label">Invoice Date</label>
                        <input type="date" <?php echo (($canceledInvoice) ? '' : 'required="required"');?> name="invoice_date" id='id_invoice_date' value='<?php echo GUtils::mysqlDate($invoices['Add_Date']); ?>' class="form-control date-picker">
                    </div>
                    <div class="form-default form-static-label col-sm-4 text-center">
                        <?php if(isset($__GET['invid']) && $invoices['Invoice_Status'] == 0) { ?>
                        <h1 class="text-center"><i class="text-danger fas fa-exclamation-triangle"></i></h1>
                        <?php } ?>
                    </div>

                    <div class="form-default form-static-label col-sm-4 text-right">
                        <h5 style="margin-bottom:10px;"><?php echo $caption;?></h5>
                        <h3 class="text-danger" id='id_balance_due' data-refund-status="<?php echo $invoices['RefundStatus'];?>" data-paid="<?php echo $invoices['paid'];?>" data-balance="<?php echo $balanceToPrint ?>">
                            <?php echo GUtils::formatMoney($balanceToPrint); ?>
                        </h3>

                    </div>
                </div>

                <div class="row">
                    <div class="form-default form-static-label col-sm-4">
                        <label class="float-label gst-label">Due Date</label>
                        <h6 class="block"><?php echo GUtils::clientDate($invoices['Due_Date']); ?></h6>
                    </div>

                    <div class="form-default form-static-label col-sm-4">
                        <?php if(isset($__GET['invid']) && $invoices['Invoice_Status'] == 0) { ?>
                        <h5 class="text-center text-danger">The invoice is</h5>
                        <h3 class="text-center text-danger">Voided</h3>
                        <?php } else if(isset($__GET['invid']) && $invoices['Invoice_Status'] == CustomerInvoice::$CANCELLED) { ?>
                        <h5 class="text-center text-danger">The invoice is</h5>
                        <h3 class="text-center text-danger">Canceled</h3>
                        <?php } ?>
                    </div>
                    <div class="form-default form-static-label col-sm-4">

                    </div>
                </div>
            </div>
        </div>
        <div class="spacer"></div>
        <?php
				if($editContactId > 0) {
					$TheTourIDx = $invoices['tourid'];
					$JointCustomersSQL = "SELECT * FROM customer_groups WHERE Primary_Customer_ID=$editContactId AND Type='Group Invoice' AND Group_ID=$TheTourIDx;";
					$JointCustomersData = GDb::fetchRow($JointCustomersSQL);
					$AdditionalTravelers = "";
					if(is_array($JointCustomersData) AND count($JointCustomersData) > 0) {
					
					if($JointCustomersData['Additional_Traveler_ID_1'] > 0) {
						$JCustomer_ID = $JointCustomersData['Additional_Traveler_ID_1'];
						$JCsql = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
						$JCData = GDb::fetchRow($JCsql);
						$AdditionalTravelers .= "<a href='contacts-edit.php?id=".$JCData['id']."&action=edit&usertype=customer'>".$JCData['fname']." ".$JCData['lname']."</a>, ";
					}
					if($JointCustomersData['Additional_Traveler_ID_2'] > 0) {
						$JCustomer_ID = $JointCustomersData['Additional_Traveler_ID_2'];
						$JCsql = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
						$JCData = GDb::fetchRow($JCsql);
						$AdditionalTravelers .= "<a href='contacts-edit.php?id=".$JCData['id']."&action=edit&usertype=customer'>".$JCData['fname']." ".$JCData['lname']."</a>, ";
					}
					if($JointCustomersData['Additional_Traveler_ID_3'] > 0) {
						$JCustomer_ID = $JointCustomersData['Additional_Traveler_ID_3'];
						$JCsql = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
						$JCData = GDb::fetchRow($JCsql);
						$AdditionalTravelers .= "<a href='contacts-edit.php?id=".$JCData['id']."&action=edit&usertype=customer'>".$JCData['fname']." ".$JCData['lname']."</a>, ";
					}
					if($JointCustomersData['Additional_Traveler_ID_4'] > 0) {
						$JCustomer_ID = $JointCustomersData['Additional_Traveler_ID_4'];
						$JCsql = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
						$JCData = GDb::fetchRow($JCsql);
						$AdditionalTravelers .= "<a href='contacts-edit.php?id=".$JCData['id']."&action=edit&usertype=customer'>".$JCData['fname']." ".$JCData['lname']."</a>, ";
					}
					if($JointCustomersData['Additional_Traveler_ID_5'] > 0) {
						$JCustomer_ID = $JointCustomersData['Additional_Traveler_ID_5'];
						$JCsql = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
						$JCData = GDb::fetchRow($JCsql);
						$AdditionalTravelers .= "<a href='contacts-edit.php?id=".$JCData['id']."&action=edit&usertype=customer'>".$JCData['fname']." ".$JCData['lname']."</a>, ";
					}
					if($JointCustomersData['Additional_Traveler_ID_6'] > 0) {
						$JCustomer_ID = $JointCustomersData['Additional_Traveler_ID_6'];
						$JCsql = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
						$JCData = GDb::fetchRow($JCsql);
						$AdditionalTravelers .= "<a href='contacts-edit.php?id=".$JCData['id']."&action=edit&usertype=customer'>".$JCData['fname']." ".$JCData['lname']."</a>, ";
					}
					if($JointCustomersData['Additional_Traveler_ID_7'] > 0) {
						$JCustomer_ID = $JointCustomersData['Additional_Traveler_ID_7'];
						$JCsql = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
						$JCData = GDb::fetchRow($JCsql);
						$AdditionalTravelers .= "<a href='contacts-edit.php?id=".$JCData['id']."&action=edit&usertype=customer'>".$JCData['fname']." ".$JCData['lname']."</a>, ";
					}
					if($JointCustomersData['Additional_Traveler_ID_8'] > 0) {
						$JCustomer_ID = $JointCustomersData['Additional_Traveler_ID_8'];
						$JCsql = "SELECT id,fname,mname,lname FROM `contacts` WHERE id=$JCustomer_ID";
						$JCData = GDb::fetchRow($JCsql);
						$AdditionalTravelers .= "<a href='contacts-edit.php?id=".$JCData['id']."&action=edit&usertype=customer'>".$JCData['fname']." ".$JCData['lname']."</a>, ";
					}
				?>
        <div class="card-block pt-0 col-12">
            <label style="font-weight:bold;">Additional travelers: <?php echo substr($AdditionalTravelers,0,-2); ?>.</label>
        </div>
        <?php }} ?>

        <div class="card-block gst-block table-responsive">
            <table id="basic-btnzz" class="table table-hover table-striped table-bordered nowrap gst-table-margin" style="border: 0px;" data-page-length="20">
                <thead>
                    <tr>
                        <th>#</th>
                        <th width="65%">Product / Service</th>
                        <th width="10%">Quantity</th>
                        <th width="12%">Amount</th>
                        <th width="12%">Total</th>
                    </tr>
                </thead>
                <tbody id='idInvoiceLineSection'>
                    <!-- Your data here -->
                    <?php
$slno = 0 ;
$productAllTotal = 0 ;
foreach ($invoiceLines as $row) {
    $slno ++ ;
    $GLOBAL_SCRIPT .= "setInvoiceProductSelection(" . $row['Customer_Invoice_Line'] . ',' . $row['Product_Product_ID'] . ");";

$qty = (isset($row['Qty']) ? $row['Qty'] : 1);
$lineTotal = $row['Invoice_Line_Total'] * $qty ;
$productAllTotal += $lineTotal ; 
?>
                    <tr class='invoice-line-tr row-<?php echo $row['Customer_Invoice_Line'];?>'>
                        <td class='slno'><?php echo $slno;?></td>
                        <td width="25%">
                            <input type="hidden" name='product[<?php echo $row['Customer_Invoice_Line'];?>]' value="<?php echo $row['Product_Product_ID'];?>" />
                            <?php echo "<strong style='font-weight:bold' class='bold productSelect' id='idProdListInput_" . $row['Customer_Invoice_Line'] . "' data-quantity='" . $qty . "' data-description='" . $row['description'] . "' data-price='" . $row['Invoice_Line_Total'] . "' value ='" . $row["Product_Product_ID"] . "' >" . $row["Product_Name"] . "</strong>";?>
                            <div><?php echo $row['description'];?></div>
                        </td>
                        <td><span onchange="invoiceLineUpdatePrice(this)" class="quantity small-textbox" name="qty[<?php echo $row['Customer_Invoice_Line'];?>]"><?php echo $qty;?></span>
                        </td>
                        <td class="price"><?php echo GUtils::formatMoney($row['Invoice_Line_Total']);?></td>
                        <td data-price="<?php echo $lineTotal;?>" class="total"><?php echo GUtils::formatMoney($lineTotal);?></td>
                    </tr>
                    <?php 
}
?>
                </tbody>
                <tfoot class="gst-all-borderless gst-all-bold">
                    <?php 
                                
                                if( $ccCharges != 0 ) { ?>
                    <tr>
                        <td align="right" colspan="4">Credit Card Surcharge (4%) </td>
                        <td align="left"><?php echo GUtils::formatMoney($ccCharges); ?></td>
                    </tr>
                    <?php }
                                
                    if( $invoices['Refund_ID'] != null && $invoices['Cancellation_Charge'] > 0 ) { ?>
                    <tr>
                        <td align="right" colspan="4">Customer Cancellation</td>
                        <td class="invoice-cancelled" align="left"><?php echo GUtils::formatMoney($invoices['Invoice_Amount_Reverted']) ;?></td>
                    </tr>
                    <tr>
                        <td align="right" colspan="4">Cancellation Charge</td>
                        <td align="left" class="cancellation_charge" data-cancellation-charge="<?php echo floatval($invoices['Cancellation_Charge']) ;?>"><?php echo GUtils::formatMoney($invoices['Cancellation_Charge']) ;?></td>
                    </tr>
                    <?php
                                    }
                                    
                                    if( $invoices['Payee_Cancellation_Charge'] > 0 ) { ?>
                    <tr>
                        <td align="right" colspan="4">Additional Customer Cancellation</td>
                        <td align="left">
                            <span class="hidden payee_cancellation_charge" data-cancellation-charge="<?php echo floatval($invoices['Payee_Cancellation_Charge']) ;?>" /></span>
                            <?php echo GUtils::formatMoney($invoices['Payee_Cancellation_Charge']) ;?>
                        </td>
                    </tr>
                    <?php
                                    }
                                ?>
                    <tr>
                        <td colspan="2" class="align-right pl-0">
                            <?php if( $invoices['Invoice_Status'] != 4 ) { ?>
                            <input onclick="addInvoiceLine()" type="button" value="Add Line" class="<?php if( $editId ) { echo "d-none"; } ?> btn float-left waves-effect waves-light btn-info ml-0 mr-2 mt-2" />
                            <input onclick="clearInvoiceLines()" type="button" value="Clear All Lines" class="<?php if( $editId ) { echo "d-none"; } ?> btn float-left waves-effect waves-light btn-info mt-2" />
                            <?php } ?>
                        </td>
                        <td colspan="2" class="align-right">Total</td>
                        <td class="invoice-total" style="background: none;"><?php echo GUtils::formatMoney($productAllTotal);?></td>
                    </tr>
                </tfoot>
            </table>
        </div>

        <?php if( count($paymentList) > 0 || ( is_array($creditNotes) && count($creditNotes) > 0) ) { ?>
        <br />
        <div class="card-block pt-0 pb-0">
            <table id="basic-btnxx" class="table table-hover table-striped table-bordered nowrap gst-table-margin" style="border: 0px;" data-page-length="20">
                <thead>
                    <?php if(is_array($paymentList) && count($paymentList) > 0 ) { ?>
                    <tr>
                        <th width="15%">Payment Date</th>
                        <th>Payment Type</th>
                        <th>Description</th>
                        <th width="10%" class="text-center">Files</th>
                        <th width="10%">Amount</th>
                        <th width="10%">Actions</th>
                    </tr>
                    <?php } ?>
                </thead>
                <tbody id='idInvoiceLineSection'>
                    <?php 
                                $paid = 0.0 ;
                                $charges = 0.0 ;
                                if(is_array($paymentList) && count($paymentList) > 0 ) { 
                                foreach( $paymentList as $one ) {
                                    $paid += floatval( $one['Customer_Payment_Amount'] ) ;
                                    $charges = $one['Charges'] ;
                                    ?>
                    <tr>
                        <td><?php echo GUtils::clientDate($one['Customer_Payment_Date']);?></td>
                        <td><?php echo $one['Customer_Payment_Method'];?></td>
                        <td><?php echo $one['Customer_Payment_Comments'];?></td>
                        <td class="text-center">
                            <?php if( $one['Attachment'] ) { ?>
                            <a href="<?php echo GUtils::doDownload($one['Attachment'], 'invattach/');?>" target="_blank"><img src="files/assets/images/view-file.png" style="width:25px;height:auto;" alt="download"></a>
                            <?php } ?>
                        </td>
                        <td><?php echo GUtils::formatMoney( $one['Customer_Payment_Amount'] );?></td>
                        <td class="text-center">
                            <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Payment">
                                <a href="javascript:void(0);" onclick='deleteConfirm( "contacts-edit.php?id=<?php echo $leadid; ?>&action=DeletePayment&PaymentID=<?php echo $one["Customer_Payment_ID"]; ?>&usertype=sales", "Delete")'><img src="files/assets/images/remove.png" style="width:22px;height:auto;" alt="remove" /></a>
                            </span>
                        </td>
                    </tr>
                    <?php } 
                                }
                                
                                if(is_array($creditNotes) && count($creditNotes) > 0 ) { ?>
                    <tr>
                        <th width="15%">Credit Date</th>
                        <th colspan="3">Description</th>
                        <th width="10%">Amount</th>
                        <th width="10%">Action</th>
                    </tr>
                    <?php
                                }
                                $totalCredit = 0 ; 
                                foreach($creditNotes as $one ) {
                                if( $one['Credit_Amount'] != 0 ) {
                                    $totalCredit += $one['Credit_Amount'] ;
                                    ?>
                    <tr>
                        <td><?php echo GUtils::clientDate($one['Credit_Note_Date']); ?></td>
                        <td colspan="3"><?php echo CustomerCreditNote::printCreditNoteLine($one);?></td>
                        <td>
                            <?php echo GUtils::formatMoney($one['Credit_Amount']); ?>
                        </td>
                        <td class="text-center">
                            <?php if( $invoices['Invoice_Status'] == CustomerInvoice::$ACTIVE ) { ?>
                            <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete Credit Note">
                                <a href="javascript:void(0);" onclick='deleteConfirm( "contacts-edit.php?id=<?php echo $leadid; ?>&action=DeleteCreditNote&CreditNoteID=<?php echo $one["Note_ID"]; ?>&usertype=sales", "Delete")'><img src="files/assets/images/remove.png" style="width:22px;height:auto;" alt="remove" /></a>
                            </span>
                            <?php } ?>
                        </td>
                    </tr>
                    <?php 
                                }
                                }
                                if( $invoices['RefundStatus'] == 2 ) /* Refund issued */ { ?>
                    <tr>
                        <td><?php echo GUtils::clientDate($invoices['Refund_Date']);?></td>
                        <td><?php echo $invoices['Cancellation_Outcome'];?></th>
                        <td><?php echo GUtils::formatMoney( 0- $invoices['Refund_Amount'] );?></td>
                    </tr>
                    <?php } ?>
                </tbody>
                <tfoot class="gst-all-borderless gst-all-bold">
                    <?php if( $invoices['Invoice_Status'] != 4 ) { ?>
                    <tr>
                        <td colspan="4" class="align-right">Total Paid</td>
                        <td><?php echo GUtils::formatMoney( floatval($paid) );?></td>
                        <td></td>
                    </tr>
                    <?php }
                                $balCaption = 'Balance Due' ;
                                if( $invoices['Invoice_Status'] == CustomerInvoice::$CANCELLED && $invoices['Refund_Amount'] > 0 && $invoices['Refund_Status'] != 2 ) {
                                    $balCaption = 'Refund Due' ;
                                }
                                ?>
                    <tr>
                        <td colspan="4" class="align-right"><?php echo $balCaption;?></td>
                        <td id="idInvoiceBalanceElement" data-charges="<?php echo $ccCharges;?>" class="invoice-balance"><?php echo GUtils::formatMoney($balanceToPrint); ?></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <?php } ?>


        <?php 
                $refundList = (new CustomerRefund())->getList(['Customer_Invoice_Num' => $editId]) ;
                
                $hasOneCancellation = false ;
                foreach( $refundList as $one ) {
                    if( $one['Status'] == 0 || $one['Status'] == 4 ) {
                        continue;
                    }
                    $hasOneCancellation = true ;
                    break ;
                }
                if(is_array($refundList) && count($refundList) > 0 && $hasOneCancellation ) { ?>
        <br />
        <div class="card-block pt-0 pb-0">
            <table id="basic-btnxx" class="table table-hover table-striped table-bordered nowrap gst-table-margin" style="border: 0px;" data-page-length="20">
                <thead>
                    <tr>
                        <th width="15%">Cancellation Date</th>
                        <th>Reason</th>
                        <th>Comment</th>
                        <th width="10%">Amount</th>
                        <th width="10%">Balance</th>
                    </tr>
                </thead>
                <tbody id='idInvoiceLineSection'>
                    <?php 
                                foreach( $refundList as $one ) {
                                    if( $one['Status'] == 0 || $one['Status'] == 4 ) {
                                        continue;
                                    }
                                    $paid += floatval( $one['Customer_Payment_Amount'] ) ;
                                    $charges = $one['Charges'] ;
                                    ?>
                    <tr>
                        <td><?php echo GUtils::clientDate($one['Cancelled_On']);?></td>
                        <td><?php echo $one['Cancellation_Outcome'];?></td>
                        <td><?php echo $one['Cancelation_Reason'];?></td>
                        <td><?php echo GUtils::formatMoney( $one['Refund_Amount'] );?></td>
                        <td><?php echo GUtils::formatMoney( ($one['Refund_Amount'] - $one['Issued_Amount']) );?></td>
                    </tr>
                    <?php } ?>

                </tbody>
            </table>
        </div>
        <?php } ?>


        <?php
                //Issued Refunds
                $refundDetail = (new CustomerRefund())->invoiceRefundDetail($editId) ;
                
                foreach( $refundDetail as $k => $one ) {
                    if( ! $one['Amount']) {
                        unset($refundDetail[$k]) ;
                    }
                }
                if(is_array($refundDetail) && count($refundDetail) > 0 ) { ?>
        <br />
        <div class="card-block pt-0 pb-0">
            <table id="basic-btnxx" class="table table-hover table-striped table-bordered nowrap gst-table-margin" style="border: 0px;" data-page-length="20">
                <thead>
                    <tr>
                        <th width="15%">Refund Date</th>
                        <th>Comment</th>
                        <th>Payment Type</th>
                        <th width="10%" class="text-center">Files</th>
                        <th width="10%">Amount</th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                    foreach( $refundDetail as $one ) {
                                    ?>
                    <tr>
                        <td><?php echo GUtils::clientDate($one['Issue_Date']);?></td>
                        <td><?php echo $one['Description'];?></td>
                        <td><?php echo $one['Payment_Type'];?></td>
                        <td class="text-center">
                            <?php if( $one['Attachment'] ) { ?>
                            <a href="<?php echo GUtils::doDownload($one['Attachment'], 'invattach/');?>" target="_blank"><img src="files/assets/images/view-file.png" style="width:25px;height:auto;" alt="download"></a>
                            <?php } ?>
                        </td>
                        <td><?php echo GUtils::formatMoney( $one['Amount'] );?></td>
                    </tr>
                    <?php } ?>

                </tbody>
            </table>
        </div>
        <?php } ?>



        <div class="card-block gst-block  row">
            <div class="form-group form-default form-static-label col-sm-4">
                <label class="float-label gst-label">Message Displayed On Invoice</label>
                <textarea class="form-control" name="comments" rows="6"><?php echo $invoices['Comments']; ?></textarea>
            </div>
            <div class="form-group form-default form-static-label col-sm-5">
                <label class="float-label gst-label" for="Invoice_Attachment">Attachment <?php if ( strlen($invoices['Attachment']) > 0) { ?><a href='<?php echo GUtils::doDownload($invoices['Attachment'], 'invattach/'); ?>' target='_blank' style='margin-left:100px;' download><i class='fas fa-cloud-download-alt'></i> <small>Download the attachment</small></a><?php } ?></label>
                <div class="Attachment_Box">
                    <input type="file" id="Invoice_Attachment" name="filAttachment" class="Attachment_Input">
                    <p id="Invoice_Attachment_Text" class="Attachment_Text">Drag the file here or click in this area.</p>
                </div>
            </div>
        </div>

        <div class="card-block gst-block row">
            <div class="col-sm-12">
                <br />
                <input type="hidden" name="hidInvoiceSave" value="1" />
                <?php if( $invoices['Invoice_Status'] == 4 ) { ?>
                <button type="submit" onclick="formmodified = 0;onSaveInvoice();" value="update_message_only" name="submit" class="btn btn-success"><i class="far fa-check-circle"></i> Update Message</button>
                <?php } else { ?>
                <div class="btn-group mr-2">
                    <button type="submit" onclick="formmodified = 0;onSaveInvoice();" value="<?php echo $SubmitAction; ?>_only" name="submit" class="btn btn-success"><i class="far fa-check-circle"></i> Save </button>
                    <?php if( ! defined('INVOICE_ADD_FRAGMENT_VIEW') ) { ?>
                    <button type="button" class="btn btn-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu">
                        <button type="submit" onclick="formmodified = 0;onSaveInvoice();" value="<?php echo $SubmitAction; ?>_new" name="submit" class="dropdown-item">Save & New</button>
                        <button type="submit" onclick="formmodified = 0;onSaveInvoice();" value="<?php echo $SubmitAction; ?>_close" name="submit" class="dropdown-item" href="#">Save & Close</button>
                    </div>
                    <?php } ?>
                </div>
                <?php } ?>
                <?php if( $editId) { ?>
                <button type="button" onclick="window.open('sales-invoice-print.php?print=payment&invid=<?php echo $invoices['Customer_Invoice_Num']; ?>','_blank');" class="btn waves-effect waves-light btn-info mr-2"><i class="fas fa-print"></i>Print</button>
                <?php } 

                        if( ! defined('INVOICE_ADD_FRAGMENT_VIEW') ) {
                        ?>
                <button type="button" onclick="window.location.href = 'sales-invoices.php'" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Cancel</button>
                <?php } ?>
            </div>
        </div>
        <div class="card-block gst-block row">
            <div class="gst-spacer-10"></div>
        </div>

    </div>
</form>



<script type="text/javascript">
    function onSaveInvoice() {
        $('#contact1').submit(function(e) {


            $('#resultsmodal #results').html('Saving ...');
            $('#resultsmodal').modal('show');
            return true;
        });
    }

    //Notification if the form isnt saved before leaving the page
    $(document).ready(function() {
        formmodified = 0;
        $('form *').change(function() {
            formmodified = 1;
        });
        window.onbeforeunload = confirmExit;

        function confirmExit() {
            if (formmodified == 1) {
                return "New information not saved. Do you wish to leave the page?";
            }
        }
    });

    //This is to make the attachment box update its content when a file is selected
    $(document).ready(function() {
        $('#Invoice_Attachment').change(function() {
            $('#Invoice_Attachment_Text').text("A file has been selected");
        });
    });

</script>

<!-- sweet alert js -->
<script type="text/javascript" src="files/bower_components/sweetalert/js/sweetalert.min.js"></script>
<!-- modalEffects js nifty modal window effects -->
<script type="text/javascript" src="files/assets/js/modalEffects.js"></script>
<?php if( ! defined('INVOICE_ADD_FRAGMENT_VIEW') ) {
    include "footer.php"; 
}
?>
