<?php
include "session.php";
$PageTitle = "Refund Receipt";
include "header.php";

include_once __DIR__ . '/models/customer_refund.php' ;
include_once __DIR__ . '/models/customer_invoice.php' ;
include_once __DIR__ . '/models/customer_payment.php' ;

global $GLOBAL_SCRIPT;
$SubmitAction = 'create';

$BACK_URL = 'refund-receipts-list.php' ; 

//If post, then include the action file.
if (isset($__POST['hidInvoiceSave']) && $__POST['hidInvoiceSave'] == '1') {
    include __DIR__ . '/inc/refund-receipt-save.php';

    $customerInvoiceDetail = (new CustomerInvoice())->get(['Customer_Invoice_Num' => $invoiceId]) ;
    
    if( $__REQUEST['back'] == 'contact-edit' ) {
        $contacIdBack = $customerInvoiceDetail['Customer_Account_Customer_ID'] ;
        $BACK_URL = "contacts-edit.php?invid=$invoiceId&id=$contacIdBack&action=edit&usertype=sales" ;
    }
    else if( $__REQUEST['back'] != 'refund-receipt-list' ) {
        $groupIdOfInvoice = $customerInvoiceDetail['Group_ID'] ;
        $BACK_URL = "groups-edit.php?id=$groupIdOfInvoice&action=edit&tab=participants" ;
    }
        
    GUtils::redirect($BACK_URL);
    die;
}

$editContactId = 0 ;
$tourId = 0 ;

//this page can be loaded basd on customerId or invoice, in both case dynamically bulid curresponding sql condition.
if (isset($__GET['customerId'])) {
    $editContactId = $__GET['customerId'] ;
    $refundCond = " ci.Customer_Account_Customer_ID=" . $__GET['customerId'] ;
}
else if (isset($__GET['invid'])) {
    $invoiceId = $__GET['invid'] ;
    $refundCond = " ci.Customer_Invoice_Num=" . $invoiceId ;
    
    $customerInvoiceDetail = (new CustomerInvoice())->get(['Customer_Invoice_Num' => $invoiceId]) ;
    $tourId = $customerInvoiceDetail['Group_ID'] ;
}
else {
    $refundCond = " 1 = 0 " ;
}

//get the refund details.
$refunds = (new CustomerRefund())->refundAlways($refundCond) ;
$refundTotal = $refunds['Refund_Amount'] ;
$invoiceData = (new CustomerInvoice())->get(['Customer_Invoice_Num' => $invoiceId]) ;

//Edit Mode {

$balanceToRefund = 0 ;
$issuedWithActive = 0 ;
if( $refunds['Status'] === null || $refunds['Status'] == CustomerRefund::$CUSTOM_REFUND ) {
    $invoiceDetails =(new CustomerPayment())->customerInvoiceDetails($invoiceId) ;
    if( $invoiceDetails['balance'] <= 0 ) {
        $balanceToRefund = $invoiceDetails['balance'] * -1 ;
        $issuedWithActive = $refunds['Active_Refund'] ;
    }
}
else {
    $balanceToRefund = floatval($refunds["Refund_Amount"] - $refunds["Issued_Amount"]) ;
    $issuedWithActive = $refunds['Issued_Amount'] ;
}

    //get the refund and invoice details.
    $sqle = '' ;
    if( isset($__GET['invid']) ) {
        //by invoice id
        $sqle = "SELECT Invoice_Amount, contactid, fname, mname, lname, address1, address2, city, state, zipcode, email FROM contacts c 
                INNER JOIN customer_account ca ON ca.contactid = c.id -- AND ca.status=1
                LEFT JOIN customer_invoice ci ON ci.Customer_Account_Customer_ID=ca.contactid
                WHERE ci.Customer_Invoice_Num=" . $__GET['invid'] ;
    }
    else if( isset($__GET['customerId']) ) {
        //by cust id
        $sqle = "SELECT Invoice_Amount, contactid, fname, mname, lname, address1, address2, city, state, zipcode, email FROM contacts c 
                INNER JOIN customer_account ca ON ca.contactid = c.id -- AND ca.status=1 
                LEFT JOIN customer_invoice ci ON ci.Customer_Account_Customer_ID=ca.contactid
                WHERE c.id=" . $__GET['customerId'] ;
    }

    if( $sqle ) {
        $invoices = GDb::fetchRow($sqle);
    }
    else {
        $invoices = [] ;
    }
    $editContactId = isset($invoices['contactid']) ? $invoices['contactid'] : 0;
    
    //formate billing address.
    $invoices['address'] = GUtils::buildGSTAddress( false, $invoices, ",", "\n" ) ;
    
//} Edit Mode

//Support both add and edit
$intContactId = intval($editContactId) ;
//find all customers list for select box
$sqlCust  = "SELECT c.id,fname,mname,lname, g.tourname, g.tourid, ci.Customer_Invoice_Num FROM contacts c
            INNER JOIN customer_account ca ON c.id = ca.contactid 
            INNER JOIN `groups` g ON g.tourid=ca.tourid AND g.status=1 
            INNER JOIN customer_invoice ci ON ci.Customer_Account_Customer_ID=ca.contactid AND ci.Group_ID=ca.tourid
            LEFT JOIN customer_refund cr ON ci.Customer_Invoice_Num =cr.Customer_Invoice_Num
            WHERE 1 /*(ci.Status = 4) AND cr.Status IN(1,3) AND cr.Refund_Amount != 0 */
            ORDER BY c.fname, c.mname, c.lname ";
$custRows = GDb::fetchRowSet($sqlCust);

?>

<form name="invoiceAddForm" action="" method="POST" id="contact1" enctype="multipart/form-data">
	<div class="card row">
		<div class="card-header">
			<h5>Issue Refund</h5>
			<a href="refund-receipts-list.php" class="btn waves-effect waves-light btn-inverse" style="float:right; padding: 3px 13px;">Refund Receipt List</a>
		</div>
		<div class="card-block gst-block row">
			<div class="form-default form-static-label col-sm-5">
				<label class="float-label gst-label">Customer Name</label>
				<select onchange="return loadCustomerDetails(this.value)" required="required" name="CurrentCustomerSelected" class="gst-lazy-dropdown js-example-basic-multiple-limit col-sm-12" multiple="multiple">
					<?php
						foreach ($custRows as $row7) {
							$selected = '';
							if ($row7['Customer_Invoice_Num'] == $invoiceId && $row7['tourid'] == $tourId && $invoiceId && $tourId) {
								$selected = 'selected="selected"';
							}
                            $tourAppend = ( strlen($row7['tourname']) > 0 ) ? ' --- ' . $row7['tourname'] . '' : '' ;
                            $name = $row7['fname'] . " " . $row7['mname'] . " " . $row7['lname'] . $tourAppend ;
                            if( $row7['Status'] != 4 ) {
                                $name .= ' (ACTIVE)' ;
                            }
							echo "<option $selected value='" . $row7['Customer_Invoice_Num'] . "'>" . $name . "</option>";
						}
						?>
				</select>
			</div>
			<div class="form-default form-static-label col-sm-2">
				<label class="float-label gst-label">Email Address</label>
				<input value="<?php echo (isset($invoices['email']) ? $invoices['email'] : ''); ?>" type="text" name="email" id='id_email' class="form-control" required="">
			</div>
			<div class="form-group form-default form-static-label col-sm-2">
				<label class="float-label gst-label">Billing Address</label>
				<textarea name="billing_address" id='id_billing_address' class="form-control" rows="3" required="required"><?php echo $invoices['address'] ;?></textarea>
			</div>
			<div class="form-default form-static-label col-sm-3 text-right">
				<h5 style="margin-bottom:10px;">Amount</h5>
                <h3 class="text-danger" id='id_balance_due' data-balance="<?php echo GUtils::formatMoney($balanceToRefund) ; ?>"><?php echo GUtils::formatMoney($balanceToRefund); ?></h3>
			</div>
		</div>
		<div class="card-block pt-0 pb-0 row">
			<div class="form-group form-default form-static-label col-sm-3">
				<label class="float-label gst-label">Refund Date</label>
				<input type="date" required="required" name="refund_date" id='id_invoice_date' value='<?php echo GUtils::clientDate(Date('Y-m-d H:i:s'), 'Y-m-d'); ?>' class="form-control date-picker" required="">
			</div>
			<div class="form-group form-default form-static-label col-sm-3">
				<label class="float-label gst-label">Payment Method</label>
				<select name="payment_method" id="idSelectPaymentMethod" class="form-control" required>
					<option value="">Select</option>
					<option <?php echo (($refundType == 'Cash') ? 'selected' : '');?> value="Cash">Cash</option>
					<option <?php echo (($refundType == 'Bank Transfer') ? 'selected' : '');?> value="Bank Transfer">Bank Transfer</option>
					<option <?php echo (($refundType == 'Check') ? 'selected' : '');?> value="Check">Check</option>
					<option <?php echo (($refundType == 'Credit Card') ? 'selected' : '');?> value="Credit Card">Credit Card</option>
					<option <?php echo (($refundType == 'Online Payment') ? 'selected' : '');?> value="Online Payment">Online Payment</option>
				</select>
			</div>
			<div class="form-group form-default form-static-label col-sm-3">
				<label class="float-label gst-label">Refund From</label>
				<select name="refund_from_None" id="Accounts_None" class="form-control">
					<option value="0" disabled selected>Select Payment Method</option>
				</select>
				<select name="refund_from_Cash" id="Accounts_cash" class="form-control" style="display:none;">
					<option value="">Select</option>
					<?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name FROM checks_bank WHERE Bank_Type='cash' AND Bank_Status=1";
					$BanksData = GDb::fetchRowSet($GetBankAccountSQL);
					foreach ($BanksData AS $BankData) { ?>
						<option value="<?php echo $BankData['Bank_ID']; ?>"><?php echo $BankData['Bank_Name']; ?></option>
					<?php } ?>
				</select>
				<select name="refund_from_Bank" id="Accounts_bank" class="form-control" style="display:none;">
					<option value="" data-checknumber="N/A">Select</option>
					<?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name, Bank_Check_Number FROM checks_bank WHERE Bank_Type='bank' AND Bank_Status=1";
					$BanksData = GDb::fetchRowSet($GetBankAccountSQL);
					foreach ($BanksData AS $BankData) { ?>
						<option value="<?php echo $BankData['Bank_ID']; ?>" data-checknumber='<?php echo $BankData['Bank_Check_Number']; ?>'><?php echo $BankData['Bank_Name']; ?></option>
					<?php } ?>
				</select>
				<select name="refund_from_Card" id="Accounts_cards" class="form-control" style="display:none;">
					<option value="">Select</option>
					<?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name FROM checks_bank WHERE Bank_Type='cards' AND Bank_Status=1";
					$BanksData = GDb::fetchRowSet($GetBankAccountSQL);
					foreach ($BanksData AS $BankData) { ?>
						<option value="<?php echo $BankData['Bank_ID']; ?>"><?php echo $BankData['Bank_Name']; ?></option>
					<?php } ?>
				</select>
				<select name="refund_from_PayPal" id="Accounts_paypal" class="form-control" style="display:none;">
					<option value="">Select</option>
					<?php $GetBankAccountSQL = "SELECT Bank_ID, Bank_Name FROM checks_bank WHERE Bank_Type='paypal' AND Bank_Status=1";
					$BanksData = GDb::fetchRowSet($GetBankAccountSQL);
					foreach ($BanksData AS $BankData) { ?>
						<option value="<?php echo $BankData['Bank_ID']; ?>"><?php echo $BankData['Bank_Name']; ?></option>
					<?php } ?>
				</select>
			</div>
			<div id="CheckNumberDiv" class="form-group form-default form-static-label col-sm-3" style="display:none;">
				<label class="float-label gst-label">Check Number <small>(Below is the automatic number)</small></label>
				<input type="text" name="Check_Number" id='Check_Number' placeholder="Select a bank to get the check number" class="form-control">
			</div>
		</div>
		<div class="card-block gst-block">
			<table id="basic-btnzz" class="table table-hover table-striped table-bordered nowrap gst-table-margin" style="border: 0px;" data-page-length="20">
				<thead>
					<tr>
						<th width="15%">Invoice Num</th>
						<th>Reason</th>
						<th width="15%">Invoice Amount</th>
						<th width="15%">Refund Amount</th>
						<th width="15%">Balance</th>
						<th width="15%">Payment</th>
					</tr>
				</thead>
				<tbody id='idInvoiceLineSection'>
					<?php 
                            if(is_array($refunds) ) {
                            $row = $refunds ; ?>
					<tr class="refund-line-tr">
						<td>
							<input type="hidden" name="cbInvoice" value="<?php echo  $row['Customer_Invoice_Num'];?>" />
							<?php echo $row["Customer_Invoice_Num"]; ?>
						</td>
						<td><?php echo $row["Cancelation_Reason"]; ?></td>
						<td class="amount" data-price="<?php echo floatval($invoiceData["Invoice_Amount"]);?>"><?php echo GUtils::formatMoney( $invoiceData["Invoice_Amount"] ); ?></td>
						<td class="amount" data-price="<?php echo floatval($issuedWithActive);?>"><?php echo GUtils::formatMoney( $issuedWithActive ); ?></td>
						<td class="amount" data-price="<?php echo floatval($balanceToRefund);?>"><?php echo GUtils::formatMoney($balanceToRefund); ?></td>
						<td data-balance="<?php echo floatval($balanceToRefund);?>"><input type="text" id="refundPayment" name="payment" value="0" /></td>
					</tr>
					<?php }
                            else {
                                ?>
					<tr>
						<td class="text-center" colspan="5">No record found !</td>
					</tr>
					<?php
                            }
?>
				</tbody>

			</table>
		</div>

		<div class="card-block gst-block row">
			<div class="form-group form-default form-static-label col-sm-4">
				<label class="float-label gst-label">Message </label>
                <input type="text" name="Cancel_Reasonz" value="<?php echo $row["Cancelation_Reason"]; ?>" hidden>
                <input type="text" name="RefundPaymentText" id="RefundPaymentText" value="" hidden>
				<textarea class="form-control" name="comments" rows="6"><?php echo $invoices['Comments']; ?></textarea>
			</div>

            <div class="form-group form-default form-static-label col-sm-4">
                <label class="float-label gst-label" for="Refund_Attachment">Attachment <?php if ( strlen($paymentAttachment) > 0) { ?><a href='<?php echo GUtils::doDownload($paymentAttachment, 'invattach/'); ?>' target='_blank' style='margin-left:100px;' download><i class='fas fa-cloud-download-alt'></i> <small>Download the attachment</small></a><?php } ?></label>
                <div class="Attachment_Box">
                    <input <?php echo $DisabledFlag; ?> type="file" id="Refund_Attachment" name="filAttachment" class="Attachment_Input" >
                    <p id="Refund_Attachment_Text" class="Attachment_Text">Drag the file here or click in this area.</p>
                </div>
            </div>

		</div>

		<div class="card-block gst-block row">
			<div class="col-sm-12">
				<br />
				<input type="hidden" name="hidInvoiceSave" value="1" />
				<?php if( $refundTotal >= 0 && is_array($refunds) ) { ?>
				<button type="submit" onclick="formmodified = 0;onSaveInvoice();" value="<?php echo $SubmitAction; ?>" name="submit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;">
					<i class="far fa-check-circle"></i>Save</button>
				<?php } ?>
				<button type="button" onclick="window.location.href = '<?php echo $BACK_URL;?>'" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"></i>Cancel</button>
			</div>
		</div>
		<div class="card-block gst-block row">
			<div class="gst-spacer-10"></div>
		</div>

	</div>
</form>


<?php include 'inc/notificiations.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
    $('#idSelectPaymentMethod').on('change', function() {
      if ( this.value == 'Cash') {
        $("#Accounts_cash").show('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").hide('slow');
        $("#Accounts_bank").hide('slow');
        $("#Accounts_paypal").hide('slow');
        $("#CheckNumberDiv").hide('slow');
      } else if ( this.value == 'Bank Transfer') {
        $("#Accounts_cash").hide('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").hide('slow');
        $("#Accounts_bank").show('slow');
        $("#Accounts_paypal").hide('slow');
        $("#CheckNumberDiv").hide('slow');
      } else if ( this.value == 'Check') {
        $("#Accounts_cash").hide('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").hide('slow');
        $("#Accounts_bank").show('slow');
        $("#Accounts_paypal").hide('slow');
        $("#CheckNumberDiv").show('slow');
      } else if ( this.value == 'Credit Card') {
        $("#Accounts_cash").hide('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").show('slow');
        $("#Accounts_bank").hide('slow');
        $("#Accounts_paypal").hide('slow');
        $("#CheckNumberDiv").hide('slow');
      } else if ( this.value == 'Online Payment') {
        $("#Accounts_cash").hide('slow');
        $("#Accounts_None").hide('slow');
        $("#Accounts_cards").hide('slow');
        $("#Accounts_bank").hide('slow');
        $("#Accounts_paypal").show('slow');
        $("#CheckNumberDiv").hide('slow');
      }
    });
});

//This is to make the attachment box update its content when a file is selected
$(document).ready(function(){
  $('#Refund_Attachment').change(function () {
    $('#Refund_Attachment_Text').text("A file has been selected");
  });
});
    
function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = ''; words[1] = 'One'; words[2] = 'Two'; words[3] = 'Three'; words[4] = 'Four'; words[5] = 'Five'; words[6] = 'Six'; words[7] = 'Seven'; words[8] = 'Eight'; words[9] = 'Nine'; words[10] = 'Ten'; words[11] = 'Eleven'; words[12] = 'Twelve'; words[13] = 'Thirteen'; words[14] = 'Fourteen'; words[15] = 'Fifteen'; words[16] = 'Sixteen'; words[17] = 'Seventeen'; words[18] = 'Eighteen'; words[19] = 'Nineteen'; words[20] = 'Twenty'; words[30] = 'Thirty'; words[40] = 'Forty'; words[50] = 'Fifty'; words[60] = 'Sixty'; words[70] = 'Seventy'; words[80] = 'Eighty'; words[90] = 'Ninety';
    
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}
function ConvertToWords(n) {
    var nums = n.toString().split('.')
    var whole = convertNumberToWords(nums[0])
    if (nums.length == 2) {
        //var fraction = convertNumberToWords(nums[1])
        if(nums[1] > 0) { var decimalz = ' and ' + nums[1] + '/100' } else { var decimalz = '' }
        return whole + decimalz + '****';
    } else {
        return whole + '****';
    }
}
    
document.getElementById('refundPayment').onkeyup = function () {
    document.getElementById('RefundPaymentText').value = ConvertToWords(document.getElementById('refundPayment').value);
}; 


var selection = document.getElementById("Accounts_bank");
selection.onchange = function(event){
  var checknumber = event.target.options[event.target.selectedIndex].dataset.checknumber;
  document.getElementById("Check_Number").value = checknumber;
};
	
function onSaveInvoice() {
	$('#contact1').submit(function(e) {
		$('#resultsmodal #results').html('Saving ...');
		$('#resultsmodal').modal('show');
		return true;
	});
}


	//Notification if the form isnt saved before leaving the page
	$(document).ready(function() {
		formmodified = 0;
		$('form *').change(function() {
			formmodified = 1;
		});
		window.onbeforeunload = confirmExit;

		function confirmExit() {
			if (formmodified == 1) {
				return "New information not saved. Do you wish to leave the page?";
			}
		}
	});

	function updateTotal() {
		var total = 0;

		$('.refund-line-tr .amount').each(function() {
			total += parseFloat($(this).data('price'));
		});

		$('#id_balance_due').html(total);

	}

	function loadCustomerDetails(val) {

		//reset first
		$('#id_email').val('');
		$('#id_billing_address').val('');
		$('#id_due_date').val('');
		$('#id_received_amount').html(formatMoney(0.0));
		$('#id_received_amount').data('balance', '0.0');

		formmodified = 0;
		window.onbeforeunload = null;
		if (val) {
			window.location.href = 'refund_receipt_add.php?invid=' + val;
		}
		return false;

		//        --------------
		//        Ajax Loading...        
		//        --------------
		//        
		//        //reset first
		//        $('#id_email').val('');
		//        $('#id_billing_address').val('');
		//        $('#id_due_date').val('');
		//        $('#id_received_amount').html(formatMoney(0.0));
		//        $('#id_received_amount').data('balance', '0.0');
		//
		//        if (val !== undefined) {
		//            ajaxCall('inc/ajax.php', {'action': 'customer-details', 'id': val}, function (data) {
		//                if (typeof data === 'object') {
		//                    $('#id_email').val(data.email);
		//                    $('#id_billing_address').val(data.address);
		//                    $('#id_due_date').val(data.due_date);
		//                    $('#id_received_amount').html(formatMoney(data.balance));
		//                    $('#id_received_amount').data('balance', data.balance);
		//                }
		//            });
		//        }
		//        return false;
	}

</script>
<?php include "footer.php"; ?>
