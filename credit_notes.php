<?php

include "session.php";
include_once "inc/helpers.php";

// { Acl Condition
$contactId  = AclPermission::userId() ;
$ACL_CONDITION = "" ;
if( AclPermission::roleFlags() & AclRole::$FLAG_ONLY_MINE ) {
    $ACL_CONDITION = " AND Customer_Account_Customer_ID = $contactId " ;
}
// }

if( isset($__REQUEST['action']) && $__REQUEST['action'] == 'delete' ) {
    $editId = $__GET['noteid'] ;

    Model::begin() ;
        //Fidn transaction id
        $sqlts = "SELECT Transaction_Type_Transaction_ID FROM customer_credit_note WHERE Note_ID ='$editId'" ;
        $transactionId = GDb::fetchScalar($sqlts) ;
        //delete transaction id
        $sqltd =  "DELETE FROM transactions WHERE Transaction_ID='$transactionId' LIMIT 1" ;
        GDb::execute($sqltd) ;
        //payment
        $sqll =  "DELETE FROM customer_credit_note WHERE Note_ID = '$editId'" ;
        GDb::execute($sqll) ;
        //Todo delete other table too
    Model::commit() ;

    GUtils::redirect('credit_notes.php') ;
}
if( isset($__REQUEST['action']) && $__REQUEST['action'] == 'void' ) {
    $editId = $__GET['noteid'] ;

    Model::begin() ;
        //Fidn transaction id
        $sqlts = "SELECT Transaction_Type_Transaction_ID FROM customer_credit_note WHERE Note_ID ='$editId'" ;
        $transactionId = GDb::fetchScalar($sqlts) ;
        //delete transaction id
        $sqltd =  "UPDATE transactions WHERE Voided_Flag=1 WHERE Transaction_ID='$transactionId' LIMIT 1" ;
        GDb::execute($sqltd) ;
        //payment
        $sqll =  "UPDATE customer_credit_note SET Status=0 WHERE Note_ID = '$editId'" ;
        GDb::execute($sqll) ;
        //Todo delete other table too
    Model::commit() ;

    GUtils::redirect('credit_notes.php') ;
}
$PageTitle = "Credit Notes ";
include "header.php";

?>

    <form action="credit_notes.php" method="POST" name="customerCreditNoteListForm" >
        <!-- [ page content ] start -->
        <div class="row">
            <div class="col-sm-12">
                <!-- HTML5 Export Buttons table start -->
                <div class="card">
                    <div class="row">
						<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
							<div class="input-group input-group-sm mb-0">
								<span class="input-group-prepend mt-0">
									<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
								</span>
								<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
							</div>
						</div>
						<div class="col-md-9" style="margin-top:15px;">
									<?php if( AclPermission::actionAllowed('Add') ) { ?>
									<a href="credit_notes_add.php" class="btn waves-effect waves-light btn-success"
									   style="margin-right: 30px;float:right; padding: 3px 13px;"><i
												class="far fa-check-circle"></i>New Credit Note</a>
									<?php } ?>
						</div>
                    </div>
                    <div class="card-block">
                        <div class="dt-responsive table-responsive">
                            <table id="idCreditNoteTable" class="table gst-table table-hover table-striped table-bordered nowrap"
                                   data-page-length="10">
                                <thead>
                                <tr>
                                    <th width="1%">Invoice#</th>
                                    <th>Cust. Account</th>
                                    <th>Date</th>
                                    <th>Reason</th>
                                    <th>Amount</th>
                                    <th width="1%" class="gst-no-sort">Actions</th>
                                </tr>
                                </thead>
                                <!-- body here -->
                            </table>
                        </div>
                    </div>
                </div>
                <!-- HTML5 Export Buttons end -->
            </div>
        </div>
    </form>

<script type="text/javascript">
//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#idCreditNoteTable').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
});

   
    document.addEventListener('DOMContentLoaded', function () {
    $('#idCreditNoteTable').DataTable({
		 "bProcessing": true,
         "serverSide": true,
		 dom: 'Bfrtip',
		 sDom: 'lrtip', //To Hide the search box
		 "bLengthChange": false, //To hide the Show X entries dropdown
		 "columnDefs": [
                     {
                        "targets": 0,
                        "orderable": false
                        } 
		   ],
		 buttons: [],
         ajax : {
            url :"inc/datatable-credit-notes.php" // json datasource
          }
        }); 
    } ) ;
</script>

<?php include 'inc/notificiations.php'; ?>
    <!-- [ page content ] end -->
<?php include "footer.php"; ?>