<?php

include "session.php";
include_once "inc/helpers.php";
include_once 'models/customer_invoice.php' ;
include_once 'models/customer_refund.php' ;

$printTitle = 'Invoice' ;

$CALLING_PAGE = 'sales-invoices.php' ;
include __DIR__ . "/inc/sales-invoice-action.php";

include __DIR__ . "/inc/sales-invoice-query.php";

$PageTitle = "Customer Invoice List";
include "header.php";

//To show the Un-Void Message
if ($_GET['action'] == "unvoid") {
	$GetInvoiceID = $_GET['InvoiceID'];
	$InvoiceUnVoid = "UPDATE customer_invoice SET Status=1 WHERE Customer_Invoice_Num=$GetInvoiceID";
	if ($db->multi_query($InvoiceUnVoid) === TRUE) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected customer invoice record has been un-voided successfully. You will be redirected shortly to the invoice.</strong></div>";
		echo "<meta http-equiv='refresh' content='3;sales-invoice-add.php?invid=$GetInvoiceID'>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error un-voiding record: " . $db->error . "</strong></div>";
	}
}

?>
    <form action="sales-invoices.php" method="POST" name="saleInvoiceListForm" >
        <!-- [ page content ] start -->
                <!-- HTML5 Export Buttons table start -->
                <div class="col-sm-12 card">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card-header table-card-header">
                                <h5>Customer Invoices</h5>
                            </div>
                        </div>
                        <div class="col-md-3" style="margin-top:15px;">
                        </div>
                        <div class="col-md-6" style="margin-top:15px;">
                            <?php if( AclPermission::actionAllowed('Add') ) { ?>
                            <a href="sales-invoice-add.php" class="btn waves-effect waves-light btn-success"
                               style="display:none; float:right; padding: 3px 10px;">
                                <!-- hide the button till multple invoice feature is completely ready -->
                                <i class="far fa-check-circle"></i>New Invoice</a>
                            <?php }?>
                            
                            <a href="sales-invoices.php?showing=active" class="btn btn-mat waves-effect waves-light btn-inverse pr-2 pl-2 "
                               style="float:right; padding: 3px 10px; margin-right: 5px;">Active Invoices
                            </a>

                            <a href="sales-invoices.php?showing=canceled" class="btn btn-mat waves-effect waves-light btn-inverse pr-2 pl-2 "
                               style="float:right; padding: 3px 10px; margin-right: 5px;">Canceled Invoices
                            </a>
                            
                            <a href="sales-invoices.php" class="btn btn-mat waves-effect waves-light btn-inverse pr-2 pl-2  "
                               style="float:right; padding: 3px 10px; margin-right: 5px;">All Invoices
                            </a>
                            
                        </div>

                        <?php
                        include 'inc/sales-invoice-table.php' ;
                        ?>
                    </div>
                </div>
                <!-- HTML5 Export Buttons end -->
    </form>

<?php include 'inc/invoice-table-view.php'; ?>

<?php include 'inc/notificiations.php'; ?>
    <!-- [ page content ] end -->
<?php include "footer.php"; ?>
