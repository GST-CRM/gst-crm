<?php
include "session.php";
$PageTitle = "List of All Suppliers";
include "header.php"; 

?>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
        <div class="card">
            <div class="row">
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
					</div>
				</div>
				<div class="col-md-5" style="margin-top:15px;"></div>
				<div class="col-md-2" style="margin-top:15px;">
					<select id="status" name="status" class="form-control form-control-default fill" onChange="doReload(this.value);">
						<option value="">Status</option>
						<option value="active" <?php if(!empty($_GET['status']) AND $_GET['status'] == "active") {echo "selected";} ?>>Active Suppliers</option>
						<option value="disabled" <?php if(!empty($_GET['status']) AND $_GET['status'] == "disabled") {echo "selected";} ?>>Disabled Suppliers</option>
					</select>
				</div>
				<div class="col-md-2" style="margin-top:15px;padding-right:45px;">
					<a href="contacts-add.php" class="btn waves-effect float-right waves-light btn-success" style="padding: 3px 13px;"><i class="fas fa-plus-circle"></i>Add New Contact</a>
				</div>
			</div>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="TableWithNoButtons" class="table table-striped table-bordered nowrap">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Full Name</th>
                                <th style="display:none;">Description</th>
                                <th style="display:none;">Payment Terms</th>
                                <th style="text-align:center;"># of Products</th>
                                <th>Email</th>
                                <th style="display:none;">Birthday</th>
                                <th style="display:none;">Phone</th>
                                <th style="display:none;">Mobile</th>
                                <th style="display:none;">Address 1</th>
                                <th style="display:none;">Address 2</th>
                                <th style="display:none;">ZIP Code</th>
                                <th>City</th>
                                <th>State</th>
                                <th>Country</th>
                            </tr>
                        </thead>
                        <tbody>
						<?php
						//$sql = "select * from agents";
						if($_GET['status'] == "disabled") {$WhereClause = "sup.status=0";} else {$WhereClause = "sup.status=1";}
						$sql = "SELECT con.*,sup.payment_terms,sup.description,sup.contactid
								FROM suppliers sup 
								JOIN contacts con 
								ON con.id=sup.contactid
								WHERE $WhereClause";
						$result = $db->query($sql);
						if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
                            <tr>
                                <td><?php echo "S".$row["id"]; ?></td>
                                <td><a href="contacts-edit.php?id=<?php echo $contactidz=$row["contactid"]; ?>&action=edit&usertype=supplier" style="color: #0000EE;">
								<?php echo $row["fname"]." ".$row["mname"]." ".$row["lname"]; ?></a></td>
                                <td style="display:none;"><?php echo $row["description"]; ?></td>
                                <td style="display:none;"><?php echo $row["payment_terms"]; ?></td>
                                <td style="text-align:center;"><a href="products.php?supplierid=<?php echo $contactidz=$row["contactid"]; ?>" style="color: #0000EE;"><?php 
								$conn = new mysqli($servername, $username, $password, $dbname);
								if ($conn->connect_error) {die("Connection failed: " . $conn->connect_error);} 
								$sql2 = "SELECT * FROM products WHERE supplierid=$contactidz AND status=1 AND subproduct=0";
								$result2 = $conn->query($sql2);
								$row_cnt = $result2->num_rows;
								echo $row_cnt." Products";
								$conn->close();
								?></a></td>
                                <td><?php echo $row["email"]; ?></td>
                                <td style="display:none;"><?php echo $row["birthday"]; ?></td>
                                <td style="display:none;"><?php echo $row["phone"]; ?></td>
                                <td style="display:none;"><?php echo $row["mobile"]; ?></td>
                                <td style="display:none;"><?php echo $row["address1"]; ?></td>
                                <td style="display:none;"><?php echo $row["address2"]; ?></td>
                                <td style="display:none;"><?php echo $row["zipcode"]; ?></td>
                                <td><?php echo $row["city"]; ?></td>
                                <td>
								<?php $stateconn = new mysqli($servername, $username, $password, $dbname);
								$currentstate = $row["state"];
								$stateresult = mysqli_query($stateconn,"SELECT name, abbrev FROM states WHERE abbrev = '$currentstate' ");
								$state = mysqli_fetch_array($stateresult); echo $state['name']; ?>
								</td>
                                <td>
								<?php $countryconn = new mysqli($servername, $username, $password, $dbname);
								$currentcountry = $row["country"];
								$stateresult = mysqli_query($countryconn,"SELECT name, abbrev FROM countries WHERE abbrev = '$currentcountry' ");
								$country = mysqli_fetch_array($stateresult); echo $country['name']; ?>
								</td>
							</tr>
						<?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<script>
//Just to move the search box to be on the top of the card section
$('.outsideBorderSearch').on( 'keyup click', function () {
	$('#TableWithNoButtons').DataTable().search(
		$('.outsideBorderSearch').val()
	).draw();
});

$(document).ready(function() {
    $('#TableWithNoButtons').DataTable( {
        //"ordering": false, //To disable the ordering of the tables
		"bLengthChange": false, //To hide the Show X entries dropdown
		dom: 'Bfrtip',
		sDom: 'lrtip', //To Hide the search box
        buttons: [] //To hide the export buttons
    } );
} );

function doReload(status){
	document.location = 'suppliers.php?status=' + status;
}
</script>
<!-- [ page content ] end -->
<?php include "footer.php"; ?>