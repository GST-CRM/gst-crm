<?php
include "session.php";
$PageTitle = "Contact Categories";
include "header.php"; 

$CALLING_PAGE = 'categories.php' ;
include __DIR__ . '/inc/categories-action.php' ;

if ($_GET['action'] == "success") {
	$fullname = $_GET["fullname"];
	echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The agent account for ".$fullname." has been created successfully!</strong></div>";
}
if ($_GET['action'] == "update") {
	$fullname = $_GET["fullname"];
	echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The agent ".$fullname." has been updated successfully!</strong></div>";
}
if ($_GET['action'] == "delete") {
	$fullname = $_GET["fullname"];
	$contactid = $_GET["cid"];
	echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The agent ".$fullname." (id# ".$contactid.") has been deleted!</strong></div>";
}

?>
<!-- [ page content ] start -->
<div class="row">
    <div class="col-sm-12">
        <!-- HTML5 Export Buttons table start -->
        <div class="card">
            <div class="card-block">
                <div class="dt-responsive table-responsive">
                    <table id="category-table" class="table table-striped table-hover table-bordered nowrap" data-page-length="50">
                        <thead>
                            <tr>
                                <th width='1%'>ID</th>
                                <th>Category Name</th>
                                <th width="1%" class="gst-no-sort">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
						$sql = "SELECT * FROM metadata WHERE catid=1";
						$result = $db->query($sql);
						if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
                            <tr>
                                <?php 
                                
                                if( $row["Status"] == '1' ) {
                                    $btnColor = 'btn-info' ;
                                }
                                else {
                                    $btnColor = 'btn-danger' ;
                                }
                                
								/*$conn = new mysqli($servername, $username, $password, $dbname);
								if ($conn->connect_error) {die("Connection failed: " . $conn->connect_error);} 
								$CategoryID = $row["id"];
								$sql2 = "SELECT COUNT(*) as TotalContacts FROM contacts WHERE affiliation=$CategoryID";
								$result2 = $conn->query($sql2);
								$data = $result2->fetch_assoc();
								$conn->close();*/
								?>
                                <td><?php echo $row["id"]; ?></td>
                                <td><a href="categories-contacts.php?category=<?php echo $row["id"]; ?>" style="color: #0000EE;">
                                        <?php echo $row["meta"]; ?></a></td>
                                <?php //echo "<td>".$data["TotalContacts"]."</td>"; ?>


                                <td>
                                    <div style="width: 150px;" class="btn-group gst-btns ">


                                        <?php if( $row["Status"] == '1' ) { ?>
                                        <button onclick='deleteConfirm( "categories.php?action=deactivate&category=<?php echo $row["id"]; ?>", "Deactivate", "Please confirm category deactivation")' type="button" class="btn <?php echo $btnColor;?>">Deactivate
                                        </button>
                                        <?php } else if( $row["Status"] == '0' ) { ?>
                                        <button onclick='showConfirm( "categories.php?action=activate&category=<?php echo $row["id"]; ?>", "Activate", "Please confirm category activation")' type="button" class="btn <?php echo $btnColor;?>">Activate
                                        </button>
                                        <?php } ?>

                                        <button type="button" class="btn <?php echo $btnColor;?> dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu" style="white-space: nowrap">
                                            <li><a data-id="<?php echo $row['id'];?>" data-name="<?php echo $row['meta'];?>" href="javascript:void(0);" onclick="showCategoryMail(this)">Email</a></li>
                                        </ul>
                                    </div>
                                </td>

                            </tr>
                            <?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
    </div>
</div>
<!-- [ page content ] end -->

<style type="text/css">
    #category-table_filter {
        float: left;
    }
</style>
<?php include 'inc/notificiations.php'; ?>

<script>
    function showCategoryMail(obj) {

        groupid = $(obj).data('id');
        groupname = $(obj).data('name');

        $("#sendGroupEmail").modal("show");
        $(".gst-email-group-name").html(groupname);
        $(".gst-email-group-name-input").val(groupname);
        $(".gst-email-group-id").val(groupid);
        $(".gst-email-type").val('category');
    }

    $(document).on('change', '#email-template', function() {
        if ($(this).val() > 0) {
            $.ajax({
                type: "POST",
                url: 'inc/email-template-functions.php',
                data: {
                    'template_id': $(this).val(),
                    'previewTemplate': 'Yes'
                },
                dataType: "json",
                success: function(data) {
                    $('#email_subject').val(data.template_subject);
                    $('#email_message').html(data.template_body);
                    $('#email_message_text').val(data.template_body);
                    $('#sendGroupEmailBtn').attr('onclick', 'sendGroupEmail()');
                },
                error: function(jqXHR, text, error) {
                    // Displaying if there are any errors
                    $('#result').html(error);
                }
            });
        } else {
            $('#email_subject').val('');
            $('#email_message').html('');
            $('#email_message_text').val('');
            $('#sendGroupEmailBtn').removeAttr('onclick');
        }
    });


    $(document).ready(function() {
        $('#category-table').DataTable({
            dom: 'Bflrtip',
            buttons: [],
            bFilter: true
        });
    });

</script>


<?php include "footer.php"; ?>
