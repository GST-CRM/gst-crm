<?php
include "session.php";
$PageTitle = "List of Financial Accounts";
include "header.php"; 

if(isset($_GET['action'])) {
    $action = mysqli_real_escape_string($db, $_GET['action']);
} else {
    $action = "";
}

if(isset($_GET['status'])) {
    $StatusGET = mysqli_real_escape_string($db, $_GET['status']);
} else {
    $StatusGET = "";
}

//To show the Void Message
if ($action == "void") {
	$BankID = mysqli_real_escape_string($db, $_GET['id']);
	$BankVoid = "UPDATE checks_bank SET Bank_Status=0 WHERE Bank_ID=$BankID;";
	if(GDb::execute($BankVoid)) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected bank account has been voided successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error voiding record: " . $db->error . "</strong></div>";
	}
}

//To show the Un-Void Message
if ($action == "unvoid") {
	$BankID = mysqli_real_escape_string($db, $_GET['id']);
	$BankUnVoid = "UPDATE checks_bank SET Bank_Status=1 WHERE Bank_ID=$BankID;";
	if(GDb::execute($BankUnVoid)) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected bank account has been un-voided successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error un-voiding record: " . $db->error . "</strong></div>";
	}
}

//To show the Delete Message
if ($action == "delete") {
	$BankID = mysqli_real_escape_string($db, $_GET['id']);
	$BankDelete = "DELETE FROM checks_bank WHERE Bank_ID=$BankID;";
	if(GDb::execute($BankDelete)) {
		echo "<div style='margin-top:25px;' class='alert alert-success background-success'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>The selected bank account has been deleted successfully.</strong></div>";
	} else {
		echo "<div style='margin-top:25px;' class='alert alert-danger background-danger'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><i class='far fa-times-circle text-white'></i></button><strong>Error deleting record: " . $db->error . "</strong></div>";
	}
}

?>
<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
        <div class="card">
            <div class="row">
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input type="text" class="outsideBorderSearch form-control" placeholder="Search...">
					</div>
				</div>
				<div class="col-md-5" style="margin-top:15px;"></div>
				<div class="col-md-2" style="margin-top:15px;">
					<select id="status" name="status" class="form-control form-control-default fill" onChange="doReload(this.value);">
						<option value="0">Status</option>
						<option value="active" <?php if(!empty($StatusGET) AND $StatusGET == "active") {echo "selected";} ?>>Active Bank Accounts</option>
						<option value="voided" <?php if(!empty($StatusGET) AND $StatusGET == "voided") {echo "selected";} ?>>Voided Bank Accounts</option>
					</select>
				</div>
				<div class="col-md-2" style="margin-top:15px;padding-right:45px;">
					<a href="check-bank-add.php" class="btn waves-effect float-right waves-light btn-success" style="padding: 5px 13px;"><i class="fas fa-plus-circle"></i>Add New Account</a>
				</div>
			</div>
            <div class="card-block " >
                <div class="dt-responsive table-responsive " style="position: relative;">                    
                    <table id="TableWithNoButtons" class="table table-striped table-bordered nowrap" style="width:100%;">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Account Type</th>
                                <th>Account Name</th>
                                <th>Account Number</th>
                                <th>Credit Balance</th>
                                <th>Debit Balance</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
						<?php
						if ($StatusGET == "voided") {
							$WhereCondition = "AND Bank_Status=0 ";
						} elseif($StatusGET == "active") {
							$WhereCondition = "AND Bank_Status=1 ";
						} else {
							$WhereCondition = "AND Bank_Status=1  ";
						}
						$sql = "SELECT Bank_Type, Bank_ID, Bank_Name, Bank_Account_Number FROM checks_bank WHERE Bank_Type != 'supplier' $WhereCondition ORDER BY Bank_Mod_Time DESC;";
						$result = $db->query($sql);
						if ($result->num_rows > 0) { while($row = $result->fetch_assoc()) { ?>
                            <tr>
                                <td><a href="check-bank-edit.php?id=<?php echo $row["Bank_ID"]; ?>" style="color: #0000EE;">
								<?php $BankID = $row["Bank_ID"]; echo $BankID; ?></a></td>
                                <td><?php echo $row["Bank_Type"]; ?></td>
                                <td><a href="check-bank-edit.php?id=<?php echo $row["Bank_ID"]; ?>" style="color: #0000EE;">
								<?php echo $row["Bank_Name"]; ?></a></td>
                                <td><?php echo $row["Bank_Account_Number"]; ?></td>
                                <?php $GetAccountOverview = "
                                    SELECT  
                                        SUM(Credit) AS Credit,
                                        SUM(Debit) AS Debit
                                    FROM    (
                                    (SELECT
                                        'Refunds' AS Type, '0.00' AS Credit, SUM(crl.Amount) AS Debit
                                    FROM customer_refund_line crl
                                    WHERE crl.Payment_From_Account=$BankID)
                                    UNION
                                    (SELECT
                                        'General_Expenses' AS Type, '0.00' AS Credit, SUM(ep.Exp_Payment_Amount) AS Debit
                                    FROM expenses_payments ep
                                    WHERE ep.Exp_Payment_Account=$BankID)
                                    UNION
                                    (SELECT
                                        'Customer_Payments' AS Type, SUM(cp.Customer_Payment_Amount) AS Credit, '0.00' AS Debit
                                    FROM customer_payments cp
                                    WHERE cp.Customer_Payment_Deposit_To=$BankID)
                                    UNION
                                    (SELECT
                                        'Deposits' AS Type, SUM(de.Deposit_Amount) AS Credit, '0.00' AS Debit
                                    FROM checks_bank_deposits de
                                    WHERE de.Deposit_To=$BankID)
                                    UNION
                                    (SELECT
                                        'Withdraws' AS Type, '0.00' AS Credit, SUM(de.Deposit_Amount) AS Debit
                                    FROM checks_bank_deposits de
                                    WHERE de.Deposit_From=$BankID)
                                    UNION
                                    (SELECT
                                        'Agents' AS Type, '0.00' AS Credit, SUM(ap.Agent_Payment_Amount) AS Debit
                                    FROM agents_payments ap
                                    WHERE ap.Agent_Payment_From_Account=$BankID)
                                    UNION
                                    (SELECT
                                        'Suppliers' AS Type, '0.00' AS Credit, SUM(sp.Supplier_Payment_Amount) AS Debit
                                    FROM suppliers_payments sp
                                    WHERE sp.Supplier_Payment_From_Account=$BankID)
                                    ) t;";
                                    $AccountBalance = GDb::fetchRow($GetAccountOverview);
                                ?>
                                <td><?php echo GUtils::formatMoney($AccountBalance['Credit']); ?></td>
                                <td style="color:red;"><?php echo "-(".GUtils::formatMoney($AccountBalance['Debit']).")"; ?></td>
                                <td>
									<div class="btn-group">
										<?php if($StatusGET == "voided") { ?>
										<a class="btn btn-danger btn-sm" href="javascript:void(0);" onclick = 'deleteConfirm( "check-banks.php?action=unvoid&id=<?php echo $row["Bank_ID"]; ?>", "Unvoid")' role="button">Unvoid</a>
										<?php } else { ?>
										<a class="btn btn-primary btn-sm" href="check-bank-edit.php?id=<?php echo $row["Bank_ID"]; ?>" role="button" id="dropdownMenuLink">Edit</a>
										<button type="button" class="btn btn-sm btn-primary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<div class="dropdown-menu">
											<a class="dropdown-item" href="javascript:void(0);" onclick = 'deleteConfirm( "check-banks.php?action=delete&id=<?php echo $row["Bank_ID"]; ?>", "Delete")'>Delete</a>
											<a class="dropdown-item" href="javascript:void(0);" onclick = 'deleteConfirm( "check-banks.php?action=void&id=<?php echo $row["Bank_ID"]; ?>", "Void")'>Void</a>
										</div>
										<?php } ?>
									</div>
								</td>
							</tr>
						<?php }} ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<!-- [ page content ] end -->
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>

<style type="text/css">
#basic-btn_filter { display:none;}
#basic-btn_wrapper .dt-buttons {
    position: absolute;
    right: 10px;
    top: 10px;
}
</style>
<script>
$(document).ready(function() {

    //Just to move the search box to be on the top of the card section
    $('.outsideBorderSearch').on( 'keyup click', function () {
        $('#TableWithNoButtons').DataTable().search(
            $('.outsideBorderSearch').val()
        ).draw();
    });
});

$(document).ready(function() {
    $('#TableWithNoButtons').DataTable( {
        //"ordering": false, //To disable the ordering of the tables
		"bLengthChange": false, //To hide the Show X entries dropdown
		dom: 'Bfrtip',
		sDom: 'lrtip', //To Hide the search box
        buttons: [], //To hide the export buttons
		"order": [[ 0, "desc" ]]
    } );
} );

function doReload(status){
	document.location = 'check-banks.php?status=' + status;
}
</script>