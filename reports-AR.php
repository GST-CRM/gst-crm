<?php
include "session.php";
include_once "inc/helpers.php";

$printTitle = 'Account Receivable' ;

$PageTitle = "Account Receivable Report";
include "header.php";

// { Acl Condition
$GroupID = AclPermission::userGroup() ;
$ACL_CONDITION = "" ;
if( AclPermission::roleFlags() & AclRole::$FLAG_ONLY_GROUP ) {
    $ACL_CONDITION = " AND ca.tourid IN($GroupID) " ;
}
// }

$v_Input_Date = GUtils::mysqlDate( Date('Y-m-d') );
$groupId = $GroupID ;

$groupFilter = $ACL_CONDITION ;
if( $groupId ) {
    $groupFilter = " AND g.tourid IN('$groupId') " ;
}
$sqlCust  = "SELECT c.id,fname,mname,lname FROM contacts c
            INNER JOIN customer_account ca ON c.id = ca.contactid 
            INNER JOIN `groups` g ON g.tourid=ca.tourid AND g.status=1 
            WHERE ca.status=1 $groupFilter
            GROUP BY c.id
            ORDER BY c.fname, c.mname, c.lname ";
$custRows = GDb::fetchRowSet($sqlCust);



$sqlGroups  = "SELECT tourid AS id, tourname AS name FROM `groups` AS g WHERE g.status=1 $groupFilter ORDER BY g.tourid DESC ";
$groupRows = GDb::fetchRowSet($sqlGroups);

if( isset($__REQUEST['submit']) ) {

    if( isset($__REQUEST['invoice_date']) ) {
        $v_Input_Date = GUtils::mysqlDate( $__REQUEST['invoice_date'] ) ;
    }

    //Additional filters
    $filter = '' ;
    $customerId = 0 ;
    if( isset($__REQUEST['groupId']) ) {
        if( $__REQUEST['groupId'] ) {
            $groupId = $__REQUEST['groupId']  ;
            $filter .= " AND (g.tourid = '" . $groupId . "' $ACL_CONDITION) " ;
        }
    }

    if( isset($__REQUEST['customerId']) ) {
        if( $__REQUEST['customerId'] ) {
            $customerId = $__REQUEST['customerId']  ;
            $filter .= " AND (ca.contactid ='" . $customerId . "') " ;
        }
    }

    $sql = "
SELECT       
    g.tourid AS Group_Num,
    ci.Customer_Order_Num,
    CONCAT(c.title, ' ',c.fname,' ',c.mname,' ',c.lname) AS Customer_Name,
    ci.Customer_Invoice_Num,
    Customer_Invoice_Date,
    Invoice_Amount,
    Due_Date,
    DATEDIFF(Due_Date, '$v_Input_Date') AS Days_Due,
    (Invoice_Amount - IFNULL(payments.Customer_Payment_Amount,0)) AS Total_Open,
	IFNULL( payments.Customer_Payment_Amount, 0 ) AS Customer_Payment_Amount ,
	CASE WHEN Due_Date < '$v_Input_Date'  THEN (Invoice_Amount - IFNULL(payments.Customer_Payment_Amount,0)) ELSE 0 END AS Past_Due,
    CASE WHEN DATEDIFF(Due_Date, '$v_Input_Date' ) BETWEEN -999 AND 0 THEN (Invoice_Amount - IFNULL(payments.Customer_Payment_Amount,0)) ELSE 0 END AS Current_,
    CASE WHEN DATEDIFF(Due_Date, '$v_Input_Date' ) BETWEEN 1 AND 30 THEN (Invoice_Amount - IFNULL(payments.Customer_Payment_Amount,0)) ELSE 0 END AS Past_Due1_30,
    CASE WHEN DATEDIFF(Due_Date, '$v_Input_Date' ) BETWEEN 31 AND 60 THEN (Invoice_Amount - IFNULL(payments.Customer_Payment_Amount,0)) ELSE 0 END AS Past_Due31_60,
    CASE WHEN DATEDIFF(Due_Date, '$v_Input_Date' ) BETWEEN 61 AND 90 THEN (Invoice_Amount - IFNULL(payments.Customer_Payment_Amount,0)) ELSE 0 END AS Past_Due61_90,
    CASE WHEN DATEDIFF(Due_Date, '$v_Input_Date' ) >= 91 THEN (Invoice_Amount - IFNULL(payments.Customer_Payment_Amount,0)) ELSE 0 END AS Past_Due_over_90
    
    FROM customer_invoice ci
    
    JOIN customer_account ca ON ci.Customer_Account_Customer_ID = ca.contactid AND ci.Group_ID=ca.tourid
    join groups g on g.tourid = ci.group_id
    and g.tourid = ci.Group_ID 
    join contacts c on c.id = ca.contactid
              LEFT JOIN  (SELECT cp.Customer_Invoice_Num, SUM(cp.Customer_Payment_Amount) AS Customer_Payment_Amount 
            FROM  customer_payments cp 
            JOIN customer_invoice ci 
            ON cp.Customer_Invoice_Num = ci.Customer_Invoice_Num
            WHERE cp.status = 1 AND ci.status = 1
            AND ci.Due_Date <= '$v_Input_Date'
        GROUP BY ci.Customer_Invoice_Num
    ) payments ON payments.Customer_Invoice_Num = ci.Customer_Invoice_Num
	
    WHERE ci.status = 1 and ca.invoiced_customer = 1 
    and Customer_Invoice_Date <= '$v_Input_Date' 
    $filter
     HAVING (Invoice_Amount - Customer_Payment_Amount) <> 0;" ;
    
    $records = GDb::fetchRowSet($sql) ;


    $globalTotalOpen = $globalPaymentAount = $globalPastDue = $globalCurrentDue = $globalDue30Days = $globalDue60Days = $globalDue90Days = $globalDueOver90 = 0;

}


?>
    <form action="reports-AR.php" method="GET" name="accountReceivableFilter" >
                <div class="card">
                    <div class="row col-sm-12 mt-3 mb-2">
                        <div class="form-group form-default form-static-label col-sm-2">
                            <label class="float-label gst-label">As of Date</label>
                            <input type="date" required="required" name="invoice_date" id='id_invoice_date' value='<?php echo GUtils::clientDate(Date('Y-m-d', strtotime($v_Input_Date)), 'Y-m-d'); ?>' class="form-control date-picker" required="">
                        </div>
                        <div class="form-group form-default form-static-label col-sm-6">
                            <label class="float-label gst-label">Group ID</label>
                            <select onchange="return loadCustomerList(this.value)" name="groupId" class="js-example-basic-multiple-limit gst-lazy-dropdown col-sm-12" multiple="multiple" size="1" >
                            <?php
                            foreach ($groupRows as $row7) {
                                $selected = '';
                                if ($row7['id'] == $groupId && $groupId) {
                                    $selected = 'selected="selected"';
                                }
                                echo "<option $selected value='" . $row7['id'] . "'>" . $row7['name'] . "</option>";
                            }
                            ?>
                            </select>
                        </div>
                        <div class="form-group form-default form-static-label col-sm-2">
                            <label class="float-label gst-label">Customer Name</label>
                            <select id="idCustomerList" onchange="return false;" name="customerId" class="js-example-basic-multiple-limit gst-lazy-dropdown col-sm-12" multiple="multiple" size="1" >
                            <?php
                            foreach ($custRows as $row7) {
                                $selected = '';
                                if ($row7['id'] == $customerId && $customerId) {
                                    $selected = 'selected="selected"';
                                }
                                echo "<option $selected value='" . $row7['id'] . "'>" . $row7['fname'] . " " . $row7['mname'] . " " . $row7['lname'] . "</option>";
                            }
                            ?>
                            </select>
                        </div>
                        <div class="form-group form-default form-static-label col-sm-2">
                            <label class="float-label gst-label"><i class="fas fa-angle-double-right"></i></label><br />
                            <button type="submit" onclick="formmodified = 0; window.onbeforeunload = null;" value="save" name="submit" class="btn btn-block waves-effect waves-light btn-info" style="padding-top:6px;padding-bottom:7px;">
                                <i class="far fa-check-circle"></i> Run Report
                            </button>
                        </div>
                    </div>
                    
                    <?php if( isset($__REQUEST['submit']) ) { ?>

                    <div class="col-12"></div>
                    <div class="row col-12" style="padding-top: 0;justify-content: center;">
                        <div class="col-md-2" style="cursor: pointer;">
                            <div class="col-md-12 gst-summary-heading">Total Open</div>
                            <div  class="col-md-12 gst-summary-bg bg-silver">
                                <div class="bg-danger" style="z-index:1;position: absolute; bottom: 0; left: 0; height: 100%; width: 100%;"></div>
                                <div class="bg-danger" style="z-index:2;position: absolute; bottom: 0; left: <?php echo $first;?>%; width: 100%; height: 100%;"></div>
                                <span style="z-index:3; position: relative;" id="globalTotalOpen">$0</span>
                            </div>
                        </div>

                        <div class="col-md-2" style="cursor: pointer;">
                            <div class="col-md-12 gst-summary-heading">Payment Amount</div>
                            <div  class="col-md-12 gst-summary-bg bg-silver">
                                <div class="bg-success" style="z-index:1;position: absolute; bottom: 0; left: 0; height: 100%; width: 100%;"></div>
                                <div class="bg-success" style="z-index:2;position: absolute; bottom: 0; left: <?php echo $first;?>%; width: 100%; height: 100%;"></div>
                                <span style="z-index:3; position: relative;" id="globalPaymentAount">$0</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-2"></div>
                     <div class="row col-md-12 mb-5" style="padding-top: 0;justify-content: center;">
                        <div class="col-md-2" style="cursor: pointer;">
                            <div class="col-md-12 gst-summary-heading">Past Due</div>
                            <div  class="col-md-12 gst-summary-bg bg-silver">
                                <div class="bg-warning" style="z-index:1;position: absolute; bottom: 0; left: 0; height: 100%; width: 100%;"></div>
                                <div class="bg-warning" style="z-index:2;position: absolute; bottom: 0; left: <?php echo $first;?>%; width: 100%; height: 100%;"></div>
                                <span style="z-index:3; position: relative;" id="globalPastDue">$0</span>
                            </div>
                        </div>
                        <div class="col-md-2" style="cursor: pointer;">
                            <div class="col-md-12 gst-summary-heading">Current Due</div>
                            <div  class="col-md-12 gst-summary-bg bg-silver">
                                <div class="bg-danger" style="z-index:1;position: absolute; bottom: 0; left: 0; height: 100%; width: 100%;"></div>
                                <div class="bg-danger" style="z-index:2;position: absolute; bottom: 0; left: <?php echo $first;?>%; width: 100%; height: 100%;"></div>
                                <span style="z-index:3; position: relative;" id="globalCurrentDue">$0</span>
                            </div>
                        </div>
                        <div class="col-md-2" style="cursor: pointer;">
                            <div class="col-md-12 gst-summary-heading">Past Due 30 Days</div>
                            <div  class="col-md-12 gst-summary-bg bg-silver">
                                <div class="bg-success" style="z-index:1;position: absolute; bottom: 0; left: 0; height: 100%; width: 100%;"></div>
                                <div class="bg-success" style="z-index:2;position: absolute; bottom: 0; left: <?php echo $first;?>%; width: 100%; height: 100%;"></div>
                                <span style="z-index:3; position: relative;" id="globalDue30Days">$0</span>
                            </div>
                        </div>
                        <div class="col-md-2" style="cursor: pointer;">
                            <div class="col-md-12 gst-summary-heading">Past Due 60 Days</div>
                            <div  class="col-md-12 gst-summary-bg bg-silver">
                                <div class="bg-warning" style="z-index:1;position: absolute; bottom: 0; left: 0; height: 100%; width: 100%;"></div>
                                <div class="bg-warning" style="z-index:2;position: absolute; bottom: 0; left: <?php echo $first;?>%; width: 100%; height: 100%;"></div>
                                <span style="z-index:3; position: relative;" id="globalDue60Days">$0</span>
                            </div>
                        </div>
                        <div class="col-md-2" style="cursor: pointer;">
                            <div class="col-md-12 gst-summary-heading">Past Due 90 Days</div>
                            <div  class="col-md-12 gst-summary-bg bg-silver">
                                <div class="bg-danger" style="z-index:1;position: absolute; bottom: 0; left: 0; height: 100%; width: 100%;"></div>
                                <div class="bg-danger" style="z-index:2;position: absolute; bottom: 0; left: <?php echo $first;?>%; width: 100%; height: 100%;"></div>
                                <span style="z-index:3; position: relative;" id="globalDue90Days">$0</span>
                            </div>
                        </div>
                        <div class="col-md-2" style="cursor: pointer;">
                            <div class="col-md-12 gst-summary-heading">Past Due Over 90</div>
                            <div  class="col-md-12 gst-summary-bg bg-silver">
                                <div class="bg-warning" style="z-index:1;position: absolute; bottom: 0; left: 0; height: 100%; width: 100%;"></div>
                                <div class="bg-warning" style="z-index:2;position: absolute; bottom: 0; left: <?php echo $first;?>%; width: 100%; height: 100%;"></div>
                                <span style="z-index:3; position: relative;" id="globalDueOver90">$0</span>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                        <div class="dt-responsive table-responsive">
                            <table class="datatable-report table gst-table table-hover table-striped table-bordered nowrap responsive " data-page-length="20" style="width:100%;" id="reports_ar_datatable">
                                <thead>
                                <tr>
                                    <th>Group Num</th>
                                    <th>Order Num</th>
                                    <th>Customer Name</th>
                                    <th>Invoice Num</th>
                                    <th>Invoice Date</th>
                                    <th>Invoice Amount</th>
                                    <th>Due Date</th>
                                    <th>Days Due</th>
                                    <th>Total Open</th>
                                    <th>Payment Amount</th>
                                    <th>Past Due</th>
                                    <th>Current Due</th>
                                    <th>Past Due 30 Days</th>
                                    <th>Past Due 60 Days</th>
                                    <th>Past Due 90 Days</th>
                                    <th>Past Due Over 90 Days</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                    $foot = [
                                        'Invoice_Amount' => 0.0,
                                        'Total_Open' => 0.0,
                                        'Customer_Payment_Amount' => 0.0,
                                        'Past_Due' => 0.0,
                                        'Current_' => 0.0,
                                        'Past_Due1_30' => 0.0,
                                        'Past_Due31_60' => 0.0,
                                        'Past_Due61_90' => 0.0,
                                        'Past_Due_over_90' => 0.0,
                                    ]; 
                                    if( isset($records) && is_array($records) ) { 
                                        
                                        foreach( $records as $row ) { 
                                            $dueColor = '' ;
                                            if( $row['Days_Due'] < 0 ) {
                                                $dueColor = 'style="color:red"' ;
                                            }
                                        
                                            $foot['Invoice_Amount'] += $row['Invoice_Amount'] ;
                                            $foot['Total_Open'] += $row['Total_Open'] ;
                                            $foot['Customer_Payment_Amount'] += $row['Customer_Payment_Amount'] ;
                                            $foot['Past_Due'] += $row['Past_Due'] ;
                                            $foot['Current_'] += $row['Current_'] ;
                                            $foot['Past_Due1_30'] += $row['Past_Due1_30'] ;
                                            $foot['Past_Due31_60'] += $row['Past_Due31_60'] ;
                                            $foot['Past_Due61_90'] += $row['Past_Due61_90'] ;
                                            $foot['Past_Due_over_90'] += $row['Past_Due_over_90'] ;

                                        ?>
                                    <tr>
                                        <td><?php echo $row['Group_Num'];?></td>
                                        <td><?php echo $row['Customer_Order_Num'];?></td>
                                        <td><?php echo $row['Customer_Name'];?></td>
                                        <td><?php echo $row['Customer_Invoice_Num'];?></td>
                                        <td><?php echo GUtils::clientDate($row['Customer_Invoice_Date']);?></td>
                                        <td><?php echo GUtils::formatMoney( $row['Invoice_Amount'] );?></td>
                                        <td><?php echo GUtils::clientDate($row['Due_Date']);?></td>
                                        <td <?php echo $dueColor;?> ><?php echo $row['Days_Due'];?></td>
                                        <td><?php echo GUtils::formatMoney( $row['Total_Open'] );?></td>
                                        <td><?php echo GUtils::formatMoney( $row['Customer_Payment_Amount'] );?></td>
                                        <td><?php echo GUtils::formatMoney( $row['Past_Due'] );?></td>
                                        <td><?php echo GUtils::formatMoney( $row['Current_'] );?></td>
                                        <td><?php echo GUtils::formatMoney( $row['Past_Due1_30'] );?></td>
                                        <td><?php echo GUtils::formatMoney( $row['Past_Due31_60'] );?></td>
                                        <td><?php echo GUtils::formatMoney( $row['Past_Due61_90'] );?></td>
                                        <td><?php echo GUtils::formatMoney( $row['Past_Due_over_90'] );?></td>
                                    </tr>
                                    <?php } 
                                    }?>
                                    
                                </tbody>
                                <?php 
                                $numRecord = count($records) ;
                                if( isset($records) && is_array($records) && $numRecord > 0 ) { 
                                    $globalTotalOpen = "'".GUtils::formatMoney( $foot['Total_Open'])."'";
                                    $globalPaymentAount = "'".GUtils::formatMoney( $foot['Customer_Payment_Amount'])."'";
                                    $globalPastDue = "'".GUtils::formatMoney( $foot['Past_Due'])."'";
                                    $globalCurrentDue = "'".GUtils::formatMoney( $foot['Current_'])."'";
                                    $globalDue30Days = "'".GUtils::formatMoney( $foot['Past_Due1_30'])."'";
                                    $globalDue60Days = "'".GUtils::formatMoney( $foot['Past_Due31_60'])."'";
                                    $globalDue90Days = "'".GUtils::formatMoney( $foot['Past_Due61_90'])."'";
                                    $globalDueOver90 = "'".GUtils::formatMoney( $foot['Past_Due_over_90'])."'";
                                ?>
                                <tfoot>
                                        <th colspan="4" class="text-right" ></th>
                                        <th class="text-right" >Total</th>
                                        <th><?php echo GUtils::formatMoney( $foot['Invoice_Amount'] );?></th>
                                        <th colspan="2"></th>
                                        <th><?php echo GUtils::formatMoney( $foot['Total_Open']); ?></th>
                                        <th><?php echo GUtils::formatMoney( $foot['Customer_Payment_Amount']); ?></th>
                                        <th><?php echo GUtils::formatMoney( $foot['Past_Due']); ?></th>
                                        <th><?php echo GUtils::formatMoney( $foot['Current_']); ?></th>
                                        <th><?php echo GUtils::formatMoney( $foot['Past_Due1_30']); ?></th>
                                        <th><?php echo GUtils::formatMoney( $foot['Past_Due31_60']); ?></th>
                                        <th><?php echo GUtils::formatMoney( $foot['Past_Due61_90']); ?></th>
                                        <th><?php echo GUtils::formatMoney( $foot['Past_Due_over_90']); ?></th>
                                </tfoot>
                                <?php } ?>
                            </table>
                        </div>
                    </div>
                    <?php } ?>
                </div>
    </form>

<style>
.dt-buttons {
	float: right !important;
}
#reports_ar_datatable_filter {
	float: left !important;
}
button.dt-button, div.dt-button, a.dt-button {
	margin-right: 0 !important;
	margin-left: .333em;
}
</style>

<script type="text/javascript">

/*$(document).ready(function() {
    $('#reports_ar_datatable').DataTable( {
        //"ordering": false, //To disable the ordering of the tables
		"bLengthChange": false, //To hide the Show X entries dropdown
		dom: 'Bfrtip',
		//sDom: 'lrtip', //To Hide the search box
        buttons: [{ extend: 'csvHtml5', footer: true },{ extend: 'excelHtml5', footer: true }] 
    } );
} );*/

$(document).ready( function () {
  var table = $('#reports_ar_datatable').DataTable({
        //"ordering": false, //To disable the ordering of the tables
		"bLengthChange": false, //To hide the Show X entries dropdown
		dom: 'Bfrtip',
		//sDom: 'lrtip', //To Hide the search box
        buttons: [
            {
                extend: 'excelHtml5',
                footer: true,
                customize: function( xlsx ) {
                  var sheet = xlsx.xl.worksheets['sheet1.xml'];
                  var col = $('col', sheet);
                  var ocellXfs = $('cellXfs', xlsx.xl['styles.xml']);
                  ocellXfs.append('<xf numFmtId="170" fontId="0" fillId="0" borderId="0" xfId="0" applyFont="1" applyFill="1" applyBorder="1" applyAlignment="1">'+'</xf>');
                  ocellXfs.attr('count', ocellXfs.attr('count') +1 );

                  var numFmts = $('numFmts', xlsx.xl['styles.xml']);
                  numFmts.append('<numFmt formatCode="$ #,##0.00"  numFmtId="170" />');
                  numFmts.attr('count', numFmts.attr('count') +1 );

                  var oxf = $('xf', xlsx.xl['styles.xml']);
                  var styleIndex = oxf.length;
                  
				  $('row c[r^="F"]', sheet).attr( 's', styleIndex - 2 );
				  $('row c[r^="I"]', sheet).attr( 's', styleIndex - 2 );
				  $('row c[r^="J"]', sheet).attr( 's', styleIndex - 2 );
				  $('row c[r^="K"]', sheet).attr( 's', styleIndex - 2 );
				  $('row c[r^="L"]', sheet).attr( 's', styleIndex - 2 );
				  $('row c[r^="M"]', sheet).attr( 's', styleIndex - 2 );
				  $('row c[r^="N"]', sheet).attr( 's', styleIndex - 2 );
				  $('row c[r^="O"]', sheet).attr( 's', styleIndex - 2 );
				  $('row c[r^="P"]', sheet).attr( 's', styleIndex - 2 );
				  $('row:eq(0) c', sheet).attr( 's', '2' ); //This is for the first row in the sheet
				  },            
            },
        ],

          "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column(5)
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 5, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 5 ).footer() ).html(
                '$'+ total
            );
        }
  });
} );

setTimeout(function() {
    $('#globalTotalOpen').html(<?php echo $globalTotalOpen; ?>);
    $('#globalPaymentAount').html(<?php echo $globalPaymentAount; ?>);
    $('#globalPastDue').html(<?php echo $globalPastDue; ?>);
    $('#globalCurrentDue').html(<?php echo $globalCurrentDue; ?>);
    $('#globalDue30Days').html(<?php echo $globalDue30Days; ?>);
    $('#globalDue60Days').html(<?php echo $globalDue60Days; ?>);
    $('#globalDue90Days').html(<?php echo $globalDue90Days; ?>);
    $('#globalDueOver90').html(<?php echo $globalDueOver90; ?>);
}, 1000);

function loadCustomerList(val) {
	//reset first
	$('#idCustomerList').html('');
	var customerId = $('#idCustomerList').val() ;
		if (val !== undefined) {
			if( val.length > 0 ) {
				ajaxCall('inc/ajax.php', {'action': 'customer-list-html', 'groupId': val, 'customerId' : customerId }, function (data) {
				$('#idCustomerList').html(data);
				}, "html");
			}
		}
	return false;        
};
</script>
<?php include 'inc/notificiations.php'; ?>
    <!-- [ page content ] end -->
<?php include "footer.php"; ?>