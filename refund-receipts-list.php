<?php
include "session.php";
$PageTitle = "Refund Receipts";
include "header.php";

if(isset($_GET['status'])) {$ReceiptStatus = mysqli_real_escape_string($db, $_GET['status']);}
if(isset($_GET['FromDate'])) {$FromDateFilter = mysqli_real_escape_string($db, $_GET['FromDate']);} else { $FromDateFilter = ""; }
if(isset($_GET['UntilDate'])) {$UntilDateFilter = mysqli_real_escape_string($db, $_GET['UntilDate']);} else { $UntilDateFilter = ""; }




?>

<!-- [ page content ] start -->
<div class="row">
	<div class="col-sm-12">
		<!-- HTML5 Export Buttons table start -->
		<div class="card">
            <form class="row" action="refund-receipts-list.php" method="GET">
				<div class="col-md-3" style="margin-top:15px;padding-left:45px;">
					<label class="float-label" for="searchbox">Custom Search</label>
					<div class="input-group input-group-sm mb-0">
						<span class="input-group-prepend mt-0">
							<label class="input-group-text pt-2 pb-2"><i class="fas fa-search"></i></label>
						</span>
						<input type="text" id="searchbox" class="outsideBorderSearch form-control" placeholder="Search...">
					</div>
				</div>
				<div class="col-md-2" style="margin-top:15px;">
					<label class="float-label" for="FromDate">From Date</label>
					<input class="form-control" id="FromDate" type="date" name="FromDate" value="<?php if(!empty($FromDateFilter)) {echo $FromDateFilter;} ?>">
				</div>
				<div class="col-md-2" style="margin-top:15px;">
					<label class="float-label" for="UntilDate">Until Date</label>
					<input class="form-control" id="UntilDate" type="date" name="UntilDate" value="<?php if(!empty($UntilDateFilter)) {echo $UntilDateFilter;} ?>">
				</div>
				<div class="col-md-1" style="margin-top:15px;padding-right:20px;">
					<label class="float-label" for="status">Status</label>
					<select id="status" name="status" class="form-control form-control-default fill">
						<option value="-1">All</option>
						<?php if($ReceiptStatus == "") { $ReceiptStatus = -1; } ?>
						<option value="4" <?php if(!empty($ReceiptStatus) AND $ReceiptStatus == 4) {echo "selected";} ?>>Deleted Receipts</option>
						<option value="1" <?php if(!empty($ReceiptStatus) AND $ReceiptStatus == 1) {echo "selected";} ?>>Refund Created</option>
						<option value="2" <?php if(!empty($ReceiptStatus) AND $ReceiptStatus == 2) {echo "selected";} ?>>Refund Issued</option>
						<option value="3" <?php if(!empty($ReceiptStatus) AND $ReceiptStatus == 3) {echo "selected";} ?>>No Refund Due</option>
					</select>
				</div>
				<div class="col-md-1" style="margin-top:15px;">
					<label class="float-label">Show Results</label>
					<button type="submit" class="btn btn-info btn-block waves-effect waves-light" style="padding: 5px 13px;">Filter</button>
				</div>
				<div class="col-md-3" style="margin-top:15px;padding-right:45px;">
					<hr style="border: 0;height: 1px;background-image: linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.25), rgba(0, 0, 0, 0));margin-top: 14px;margin-bottom: 13px;" />
					<a href="refund_receipt_add.php?back=refund-receipt-list" class="btn btn-block waves-effect float-right waves-light btn-success" style="padding: 5px 13px;"><i class="fas fa-plus-circle"></i>Create New Receipt</a>
				</div>
			</form>
            <div class="card-block">
                <div class="dt-responsive table-responsive">
				<!-- basic-btn -->
                    	<table id="refund_receipts_grid" width="100%" class="table table-hover table-striped table-bordered responsive nowrap" data-page-length="10">
                        <thead>
                            <tr>
                                <th>Invoice #</th>
                                <th>Customer Name</th>
                                <th>Group Reference #</th>
                                <th>Cancellation Charge</th>
                                <th>Reason</th>
                                <?php //<th>Cancellation Reason</th> ?>
                                <th>Type</th>
                                <?php //<th>Refund Type</th> ?>
                                <th>Refund Date</th>
                                <th>Refund Amount</th>
                                <th>Balance</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        	<?php
                        	$refundStatus = 'N/A';
                        	$refundStyle = '';
							$WhereStatusConditions = '';
								
							if(isset($ReceiptStatus) OR isset($FromDateFilter) OR isset($UntilDateFilter)) {
								
								$WhereStatusConditions = ' WHERE 1 ';
								
								//If the filter contains conditions related to STATUS
                        		if(($ReceiptStatus == 1 || $ReceiptStatus == 2)) {
									$WhereStatusConditions .= " AND cr.Refund_Amount!=0 AND cr.Status = ".$ReceiptStatus." ";
                                } else if($ReceiptStatus == 3) {
                                    $WhereStatusConditions .= " AND cr.Status != 0 AND cr.Refund_Amount=0 ";
                                } else if($ReceiptStatus == 4) {
									$WhereStatusConditions .= " AND cr.Status = 0 AND cr.Refund_Amount=0 ";
								}
								
								//If the filter contains conditions related to FROM DATE
                        		if($FromDateFilter != "") {
									$WhereStatusConditions .= " AND cr.Refund_Date >= '$FromDateFilter' ";
                                }								
								
								//If the filter contains conditions related to UNTIL DATE
                        		if($UntilDateFilter != "") {
									$WhereStatusConditions .= " AND cr.Refund_Date <= '$UntilDateFilter' ";
                                }								
							
							}

                        		$sql = "SELECT
											/* cr.Refund_Type, */ '' AS Refund_Type,
											cr.Refund_ID,
											cr.Customer_Invoice_Num,
											cr.Cancellation_Charge,
											cr.Refund_Amount,
                                                                                        cr.Active_Refund,  
											cr.Refund_Date,
											cr.Cancelation_Reason,
											(cr.Refund_Amount - cr.Issued_Amount) AS balance,
											cr.Status,
											c.title,
											c.fname,
											c.mname,
											c.lname,
											cr.Customer_Account_Customer_ID,
											gr.tourid,
											gr.tourdesc
										FROM customer_refund cr
										JOIN customer_invoice ci 
											ON ci.Customer_Invoice_Num=cr.Customer_Invoice_Num 
										JOIN contacts c
											ON c.id = ci.Customer_Account_Customer_ID
										JOIN groups gr
											ON gr.tourid=ci.Group_ID
										$WhereStatusConditions
										ORDER BY Refund_Date DESC, Refund_ID DESC";
                        		$result = $db->query($sql);
								if ($result->num_rows > 0) { 
									while($row = $result->fetch_assoc()) { 
                                        //Delete first priority
                                        if ($row['Status'] == 0) {
                                            $refundStatus = 'Deleted';
                                            $refundStyle = 'style="color:ff0000"';
                                        }
                                        //No refund due second priority
                                        else if( floatval($row['Refund_Amount']) == 0.0 ) {
                                            $refundStatus = 'No Refund Due';
                                            $refundStyle = 'style="color:777777"';
                                        }
                                        else {
                                            if ($row['Status'] == 1 || $row['Status'] == 3) {
                                                $refundStatus = '<a class="text-underline " href="refund_receipt_add.php?back=refund-receipt-list&invid=' . $row['Customer_Invoice_Num'] . '">Refund Created</a>';
                                                $refundStyle = 'style="color:#ff0000"';
                                            }
                                            else if ($row['Status'] == 2) {
                                                $refundStatus = 'Refund Issued';
                                                $refundStyle = 'style="color:#006600"';
                                            }
                                        }
									?>
									<tr>
										<td><a style="color: #0000EE;" href="contacts-edit.php?id=<?php echo $row['Customer_Account_Customer_ID']; ?>&action=edit&usertype=sales"><?php echo $row['Customer_Invoice_Num']; ?></a></td>
										<td><a style="color: #0000EE;" href="contacts-edit.php?id=<?php echo $row['Customer_Account_Customer_ID']; ?>&action=edit&usertype=sales"><?php echo $row['title']." ".$row['fname']." ".$row['mname']." ".$row['lname']; ?></a></td>
										<td><a style="color: #0000EE;" href="groups-edit.php?id=<?php echo $row['tourid']; ?>&action=edit&tab=participants"><?php echo $row['tourdesc']; ?></a></td>
										<td><?php echo GUtils::formatMoney( $row['Cancellation_Charge'] ); ?></td>
										<td><?php if($row['Cancelation_Reason']=="") { echo "N/A"; } else { echo wordwrap($row['Cancelation_Reason'], 55, "<br>\n", true); } ?></td>
										<td><?php if($row['Refund_Type']=="") { echo "N/A"; } else { echo $row['Refund_Type']; } ?></td>
                                                                                <td><?php echo GUtils::clientDate($row['Refund_Date']); ?></td>
                                        <td><?php echo GUtils::formatMoney( $row['Refund_Amount'] + $row['Active_Refund'] ); ?></td>
                                        <td><?php echo GUtils::formatMoney( $row['balance'] ); ?></td>
										<td <?php echo $refundStyle; ?>><?php echo $refundStatus; ?></td>
									</tr>
								<?php } 
								}
                        	?>
                        </tbody>
						<tfoot>
							<th colspan="7" class="text-right">Total Amount:</th>
							<th colspan="3"></th>
						</tfoot>
                    </table>
                </div>
            </div>
        </div>
        <!-- HTML5 Export Buttons end -->
	</div>
</div>
<!-- [ page content ] end -->
<script>
const formatter = new Intl.NumberFormat('en-US', {
	style: 'currency',
	currency: 'USD',
	minimumFractionDigits: 2
})


$(document).ready(function() {
	//Just to move the search box to be on the top of the card section
    $('.outsideBorderSearch').on( 'keyup click', function () {
        $('#refund_receipts_grid').DataTable().search(
            $('.outsideBorderSearch').val()
        ).draw();
    });
	
    $('#refund_receipts_grid').DataTable( {
        "ordering": false,
		"bLengthChange": false,
		dom: 'Bfrtip',
		sDom: 'lrtip',
        buttons: [],
		"footerCallback": function ( row, data, start, end, display ) {
			var api = this.api(), data;
			// Remove the formatting to get integer data for summation
			var intVal = function ( i ) {
				return typeof i === 'string' ?
				i.replace(/[\$,]/g, '')*1 :
				typeof i === 'number' ?
				i : 0;
			};

			// Total over all pages
            total = api
                .column( 7 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 7 ).footer() ).html(
                formatter.format(total) 
            );
        },
    } );
} );
</script>
<?php include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>