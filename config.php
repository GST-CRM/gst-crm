<?php
    $IS_PRODUCTION = false ;
    $HOST_NAME = trim($_SERVER['HTTP_HOST'], "/") ;

    if( stripos( $HOST_NAME, 'goodshepherdtravel.net') === 0 ) {
        $IS_PRODUCTION = true ;
    }

    //if production server?
    if( $IS_PRODUCTION ) {
        require __DIR__ . '/../crm-connect/config-db.php';
        $crm_status_text="Production";
    }
    else {
        require 'config-db.php';
        $crm_status_text="Development";
    }
	
	define('DB_SERVER', $servername);
	define('DB_USERNAME', $username);
	define('DB_PASSWORD', $password);
	define('DB_DATABASE', $dbname);
	$db = mysqli_connect(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
	date_default_timezone_set('America/Chicago');
    
    require_once __DIR__ . '/inc/helpers-sanitize.php';

	$Company_Name = "Good Shepherd Travels";
	$Company_Address = "9021 Washington Ln<br />Lantana, Texas 76226";
	$Company_Email = "info@goodshepherdtravel.com";
	$crm_path = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://". $_SERVER['HTTP_HOST'].dirname($_SERVER['REQUEST_URI'])."/";
	$crm_images = "files/assets/images";
	$crm_version = "1.7.6";
	$crm_last_update = "09/02/2020";
    $Current_Page = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
    
    $GST_CRM_Version = "GST CRM ".$crm_status_text." v".$crm_version." <small>(Updated on ".$crm_last_update.")</small>";    
?>