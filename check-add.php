<?php
include "session.php";
$PageTitle = "Issue New Check";
include "header.php";


$CompanyDataSQL = 'SELECT Company_Name, Company_Address, Company_Address2, Company_Phone, Company_Email, Company_Logo FROM checks_company WHERE Company_ID=1';
$CompanyData = GDb::fetchRow($CompanyDataSQL);

?>

<style>
@font-face {
  font-family: MICRfont;
  src: url(files/assets/fonts/micrenc.ttf);
}
.select2-container--default .select2-selection--single .select2-selection__rendered {background:white;color:#333;padding:2px 30px 3px 20px;border-radius:4px;}
.select2-container--default .select2-selection--single .select2-selection__arrow {top:4px;}
.select2-container--default .select2-selection--single .select2-selection__arrow b {border-color:black transparent transparent transparent;}
.select2-selection {height:35px;padding:0px !important;}
</style>

<form name="CheckSetupForm" action="" method="POST" id="contact1">
<div class="row">
    <div class="col-sm-5">
        <div class="card">
            <div class="card-header pb-3">
                <h5>Check Information</h5>
            </div>
            <div class="card-block row pt-0">
                <div class="form-group form-default form-static-label col-sm-8">
                    <label class="float-label">Bank Account</label>
                    <input type="hidden" name="AddNewCheck" value="yes">
                    <select id="TheBankSelection" name="BankID" class="form-control">
                        <option value="0" disabled selected></option>
                        <?php $BankDataSQL = 'SELECT Bank_ID, Bank_Name, Bank_Address, Bank_Transit, Bank_Routing, Bank_Account_Number, Bank_Check_Number FROM checks_bank WHERE Bank_Type="bank" AND Bank_Status=1';
                        $BanksData = GDb::fetchRowSet($BankDataSQL);
                        foreach ($BanksData AS $BankData) {
                            echo "<option value='".$BankData['Bank_ID']."' data-name='".$BankData['Bank_Name']."' data-address='".$BankData['Bank_Address']."' data-transit='".$BankData['Bank_Transit']."' data-routing='".$BankData['Bank_Routing']."' data-account='".$BankData['Bank_Account_Number']."' data-checknumber='".$BankData['Bank_Check_Number']."'>".$BankData['Bank_Name']."</option>";
                        } ?>
                    </select>
                </div>
                <div class="form-group form-default form-static-label col-sm-4">
                    <label class="float-label">Check Number</label>
                    <input type="number" name="CheckNum" id="CheckNumValue" value="" class="form-control" readonly>
                </div>
				<div class="form-group form-default form-static-label col-sm-4">
                    <label class="float-label">Check Type</label><br />
					<div class="form-radio">
						<div class="radio radiofill radio-primary radio-inline">
							<label class="mb-0"><input type="radio" name="Check_Type" id="Check_Type"onclick="CheckTypeFire(this);" value="1"><i class="helper"></i>Contact</label>
							<label class="mb-0"><input type="radio" name="Check_Type" id="Check_Type"onclick="CheckTypeFire(this);" value="2"><i class="helper"></i>Expense</label>
						</div>
                    </div>
                </div>
                <div id="EmptySelectBox" class="form-group form-default form-static-label col-sm-8">
				</div>
                <div id="CustomerSelectBox" class="form-group form-default form-static-label col-sm-8" style="display:none;">
                    <label class="float-label">Customer Name</label>
                    <select type="text" name="CustomerID" id="CustomerName" class="form-control" disabled>
                        <option value="0">-</option>
                    </select>
                </div>
                <div id="ExpenseSelectBox" class="form-group form-default form-static-label col-sm-8" style="display:none;">
                    <label class="float-label">Payee Name</label>
                    <select type="text" name="PayeeID" id="PayeeName" class="form-control" disabled>
                        <option value="0">-</option>
                    </select>
                </div>
                <div class="form-group form-default form-static-label col-sm-6">
                    <label class="float-label">Check Amount</label>
                    <input type="text" name="CheckAmount" id="CheckAmount" oninput="CheckAmountFunc()" class="form-control" disabled>
                    <input type="text" name="CheckAmountWordText" id="CheckAmountWordText" class="form-control" hidden>
                </div>
                <div class="form-group form-default form-static-label col-sm-6">
                    <label class="float-label">Check Date</label>
                    <input type="date" name="CheckDate" id="CheckDate" value="<?php echo date("Y-m-d"); ?>" oninput="CheckDateFunc()" class="form-control" disabled>
                </div>
                <div class="form-group form-default form-static-label col-sm-12">
                    <label class="float-label">Check Memo</label>
                    <input type="text" name="CheckMemo" id="CheckMemo" oninput="CheckMemoFunc()" class="form-control" maxlength="50" disabled>
                </div>
                <div class="form-group form-default form-static-label col-sm-12">
                    <label class="float-label">Check Notes <i>(For GST staff only)</i></label>
                    <input type="text" name="CheckNotes" id="CheckNotes" class="form-control" disabled>
                </div>
                <div class="col-sm-12">
                    <hr />
                    <button type="submit" name="AddNewBankSubmit" class="btn waves-effect waves-light btn-success" style="margin-right:20px;" data-toggle="modal" data-target="#resultsmodal" style="margin-right:20px;" ><i class="far fa-check-circle"></i>Save</button>
                    <button type="button" onclick="window.location.href = 'check-banks.php'" class="btn waves-effect waves-light btn-inverse"><i class="fas fa-ban"> </i>Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-7">
        <div class="card">
            <div class="card-header">
                <h5>Preview</h5>
            </div>
            <div class="card-block row pt-0 no-gutters text-center">
                <div class="col-12 row no-gutters p-3" style="background:#f9f9f9 url(files/assets/images/cream-pixels.png);border: solid 1px #333; border-radius: 15px;">
                    <div class="col-2" style="line-height:13px;">
                        <?php if($CompanyData['Company_Logo'] != '') {
                            $LogoPath = "uploads/".$CompanyData['Company_Logo'];
                        } else { 
                            $LogoPath = $crm_images."/logo-Placeholder.png";
                        } ?>
                        <img src="<?php echo $LogoPath; ?>" class="img-fluid" style="height:60px;" alt="<?php echo $CompanyData['Company_Name']; ?>" />
                    </div>
                    <div class="col-4" style="line-height:13px;">
                        <b><?php echo $CompanyData['Company_Name']; ?></b>
                        <br />
                        <small>
                            <?php echo $CompanyData['Company_Address']; ?>
                            <br />
                            <?php echo $CompanyData['Company_Address2']; ?>
                            <br />
                            <?php echo $CompanyData['Company_Phone']; ?>
                            <br />
                            <?php echo $CompanyData['Company_Email']; ?>
                        </small>
                    </div>
                    <div class="col-4">
                        <b id="BankNameText">Your Bank Name</b><br /><small id="BankAddressText">Bank City, State, Zip Code</small>
                    </div>
                    <div class="col-2 text-center">
                        <h4 class="mb-0" id="CheckNumText">No. -</h4>
                        <h6 id="TransitCodeText"></h6>
                    </div>
                    <div class="col-10"></div>
                    <div class="col-2 text-center pb-2">
                        <p style="color:black">
                            <b>Date:</b> <span id="CheckDateText">__ / __ / ____</span>
                        </p>
                    </div>
                    <div class="col-10 text-left" style="border-bottom:solid 2px #333;">
                        <b>Pay To The Order Of:</b> <span id="CustomerNameText">-</span>
                    </div>
                    <div class="col-2 text-center">
                        <h5 id="CheckAmountText">$ _____ . __</h5>
                    </div>
                    <div class="col-12 pt-2 text-left" id="CustomerAddressText"></div>
                    <div class="col-12 pt-2 text-left" style="border-bottom:solid 2px #333;" id="CheckAmountWords">
                        <b>Amount in words</b>
                    </div>
                    
                    <div class="col-7 pt-3 text-left">
                        <b>Memo:</b> <u id="CheckMemoText"></u>
                    </div>
                    <div class="col-5 pt-3" style="border-bottom:solid 2px #333;"></div>
                    <div class="col-12 text-center pt-2 pb-0"><h4 class="mb-0" style="font-family:MICRfont;">C00<span id="CheckNum2Text">-</span>C A<span id="RoutingNumText">67-76890</span>A <span id="AccountNumText">111000025</span>C</h4></div>
                </div>
                <br/>
                <label class="mt-2" style="margin:auto;font-size:12px;color:#333;">*The info on this check is for sample purposes only.*</label>
            </div>
        </div>
    </div>
</div>
</form>


<div class="modal fade" id="resultsmodal" tabindex="-1" role="dialog" style="top:50px;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align:center;">
                <div id="results" class="confirmation-message-result-area">
					<div class="upload-loader"></div>
					<p>Prcoessing...</p>
				</div>
            </div>
            <div class="modal-footer2">
                <button type="button" class="btn btn-success waves-effect waves-light" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>

<script>
var selection = document.getElementById("TheBankSelection");
selection.onchange = function(event){
    /*$('#CheckNum').removeAttr("disabled");*/
    $('#CustomerName').removeAttr("disabled");
    $('#PayeeName').removeAttr("disabled");
    $('#CheckAmount').removeAttr("disabled");
    $('#CheckDate').removeAttr("disabled");
    $('#CheckMemo').removeAttr("disabled");
    $('#CheckNotes').removeAttr("disabled");
  var name = event.target.options[event.target.selectedIndex].dataset.name;
  var address = event.target.options[event.target.selectedIndex].dataset.address;
  var transit = event.target.options[event.target.selectedIndex].dataset.transit;
  var routing = event.target.options[event.target.selectedIndex].dataset.routing;
  var account = event.target.options[event.target.selectedIndex].dataset.account;
  var checknumber = event.target.options[event.target.selectedIndex].dataset.checknumber;
  document.getElementById("BankNameText").innerHTML = name;
  document.getElementById("BankAddressText").innerHTML = address;
  document.getElementById("TransitCodeText").innerHTML = transit;
  document.getElementById("RoutingNumText").innerHTML = routing;
  document.getElementById("AccountNumText").innerHTML = account;
  document.getElementById("CheckNumValue").value = checknumber;
  document.getElementById("CheckNumText").innerHTML = 'No. ' + checknumber;
  document.getElementById("CheckNum2Text").innerHTML = checknumber;
};
function CheckMemoFunc() {
    var x = document.getElementById("CheckMemo").value;
    document.getElementById("CheckMemoText").innerHTML = x;
}
function CheckAmountFunc() {
    var x = document.getElementById("CheckAmount").value;
    document.getElementById("CheckAmountText").innerHTML = formatMoney(x);
}
function CheckDateFunc() {
    var x = document.getElementById("CheckDate").value;
    var today  = new Date(x);
    document.getElementById("CheckDateText").innerHTML = today.toLocaleDateString("en-US");
} 


function CheckTypeFire(myRadio) {
	if(myRadio.value == 1) {
		$("#EmptySelectBox").hide();
		$("#CustomerSelectBox").show();
		$("#ExpenseSelectBox").hide();
	} else if (myRadio.value == 2) {
		$("#EmptySelectBox").hide();
		$("#CustomerSelectBox").hide();
		$("#ExpenseSelectBox").show();
	}
}


function convertNumberToWords(amount) {
    var words = new Array();
    words[0] = ''; words[1] = 'One'; words[2] = 'Two'; words[3] = 'Three'; words[4] = 'Four'; words[5] = 'Five'; words[6] = 'Six'; words[7] = 'Seven'; words[8] = 'Eight'; words[9] = 'Nine'; words[10] = 'Ten'; words[11] = 'Eleven'; words[12] = 'Twelve'; words[13] = 'Thirteen'; words[14] = 'Fourteen'; words[15] = 'Fifteen'; words[16] = 'Sixteen'; words[17] = 'Seventeen'; words[18] = 'Eighteen'; words[19] = 'Nineteen'; words[20] = 'Twenty'; words[30] = 'Thirty'; words[40] = 'Forty'; words[50] = 'Fifty'; words[60] = 'Sixty'; words[70] = 'Seventy'; words[80] = 'Eighty'; words[90] = 'Ninety';
    
    amount = amount.toString();
    var atemp = amount.split(".");
    var number = atemp[0].split(",").join("");
    var n_length = number.length;
    var words_string = "";
    if (n_length <= 9) {
        var n_array = new Array(0, 0, 0, 0, 0, 0, 0, 0, 0);
        var received_n_array = new Array();
        for (var i = 0; i < n_length; i++) {
            received_n_array[i] = number.substr(i, 1);
        }
        for (var i = 9 - n_length, j = 0; i < 9; i++, j++) {
            n_array[i] = received_n_array[j];
        }
        for (var i = 0, j = 1; i < 9; i++, j++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                if (n_array[i] == 1) {
                    n_array[j] = 10 + parseInt(n_array[j]);
                    n_array[i] = 0;
                }
            }
        }
        value = "";
        for (var i = 0; i < 9; i++) {
            if (i == 0 || i == 2 || i == 4 || i == 7) {
                value = n_array[i] * 10;
            } else {
                value = n_array[i];
            }
            if (value != 0) {
                words_string += words[value] + " ";
            }
            if ((i == 1 && value != 0) || (i == 0 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Crores ";
            }
            if ((i == 3 && value != 0) || (i == 2 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Lakhs ";
            }
            if ((i == 5 && value != 0) || (i == 4 && value != 0 && n_array[i + 1] == 0)) {
                words_string += "Thousand ";
            }
            if (i == 6 && value != 0 && (n_array[i + 1] != 0 && n_array[i + 2] != 0)) {
                words_string += "Hundred and ";
            } else if (i == 6 && value != 0) {
                words_string += "Hundred ";
            }
        }
        words_string = words_string.split("  ").join(" ");
    }
    return words_string;
}
function ConvertToWords(n) {
    var nums = n.toString().split('.')
    var whole = convertNumberToWords(nums[0])
    if (nums.length == 2) {
        //var fraction = convertNumberToWords(nums[1])
        if(nums[1] > 0) { var decimalz = ' and ' + nums[1] + '/100' } else { var decimalz = '' }
        return whole + decimalz + '****';
    } else {
        return whole + '****';
    }
}

               
document.getElementById('CheckAmount').onkeyup = function () {
    document.getElementById('CheckAmountWords').innerHTML = ConvertToWords(document.getElementById('CheckAmount').value);
    document.getElementById('CheckAmountWordText').value = ConvertToWords(document.getElementById('CheckAmount').value);
};  
    
$(document).ready(function() {
	$("form").submit(function() {
		// Getting the form ID
		var  formID = $(this).attr('id');
		var formDetails = $('#'+formID);
		//To not let the form of uploading attachments included in this
		if (formID != 'contact6' && formID != 'idGroupEmailForm') {
			$.ajax({
				type: "POST",
				url: 'inc/checks-functions.php',
				data: formDetails.serialize(),
				success: function (data) {	
					// Inserting html into the result div
					$('#results').html(data);
					//$("form")[0].reset();
				},
				error: function(jqXHR, text, error){
				// Displaying if there are any errors
					$('#result').html(error);           
				}
			});
			return false;
		}
	});
});
    

//To create select2 function for the customers dropdown
$(document).ready(function(){
   $("#CustomerName").select2({
      ajax: {
        url: "inc/select2-customers.php",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
           return {
              searchTerm: params.term,
              ChecksFeature: 'active'
           };
        },
        processResults: function (response) {
           return {
              results: response
           };
        },
        cache: true
      }
   }).on('change', function(e) {
       // Access to full data
       document.getElementById("CustomerNameText").innerHTML = $(this).select2('data')[0].CustomerName;
       document.getElementById("CustomerAddressText").innerHTML = $(this).select2('data')[0].CustomerAddress;
   });
});

//To create select2 function for the payees dropdown
$(document).ready(function(){
   $("#PayeeName").select2({
      ajax: {
        url: "inc/select2-payee.php",
        type: "post",
        dataType: 'json',
        delay: 250,
        data: function (params) {
           return {
              searchTerm: params.term,
              ChecksFeature: 'active'
           };
        },
        processResults: function (response) {
           return {
              results: response
           };
        },
        cache: true
      }
   }).on('change', function(e) {
       // Access to full data
       document.getElementById("CustomerNameText").innerHTML = $(this).select2('data')[0].CustomerName;
       document.getElementById("CustomerAddressText").innerHTML = $(this).select2('data')[0].CustomerAddress;
   });
});

    
</script>


<?php //include 'inc/notificiations.php'; ?>
<?php include "footer.php"; ?>
